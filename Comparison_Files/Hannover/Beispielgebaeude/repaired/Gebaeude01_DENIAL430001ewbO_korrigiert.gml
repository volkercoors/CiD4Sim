<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<CityModel xmlns="http://www.opengis.net/citygml/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:core="http://www.opengis.net/citygml/base/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:tex="http://www.opengis.net/citygml/textures/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/citygml/2.0 http://schemas.opengis.net/citygml/2.0/cityGMLBase.xsd http://www.opengis.net/citygml/appearance/2.0  http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd">

  <gml:description>Exported by RhinoCity(TM) software - (c)Rhinoterrain 2010-2018 : www.rhinoterrain.com</gml:description>

  <gml:name>RhinoCity(TM)</gml:name>

  <gml:boundedBy>
    <gml:Envelope srsDimension="3" srsName="crs:ETRS89_UTM32*DE_DHHN92_NH">
      <gml:lowerCorner>552662.750000 5799975.000000 57.119995</gml:lowerCorner>
      <gml:upperCorner>552674.812500 5799987.000000 66.129372</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>

  <cityObjectMember>
    <bldg:Building gml:id="DENIAL430001ewbO">
      <gen:doubleAttribute name="Volume">
        <gen:value>743.802</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="TotalRoofArea">
        <gen:value>103.99</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="TotalWallArea">
        <gen:value>295.43</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Authoring_Software">
        <gen:value>RhinoCity</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="OSKA">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ALKISID">
        <gen:value>DENIAL430001ewbO</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="KACHEL_ID">
        <gen:value>11_04_Building</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="COUNT">
        <gen:value>1281</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="DESC">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCK">
        <gen:value>20</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCKNR">
        <gen:value>051020</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STATBEZNR">
        <gen:value>051</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="MBZNR">
        <gen:value>0502</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="WBZNR">
        <gen:value>0502</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNAM">
        <gen:value>Waldhausen</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNR">
        <gen:value>05</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNAM">
        <gen:value>Döhren-Wülfel</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNR">
        <gen:value>08</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEMEINDE">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SCALE">
        <gen:value>5000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_overlaps">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_xmax">
        <gen:value>552674.7580000004</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_xmin">
        <gen:value>552662.8090000004</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_ymax">
        <gen:value>5799986.460999999</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_ymin">
        <gen:value>5799975.484999999</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_zmax">
        <gen:value>57.758955999990924</gen:value>
      </gen:stringAttribute>
      <gen:doubleAttribute name="ZMIN">
        <gen:value>57.120000</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="THICKNESS">
        <gen:value>0.100000</gen:value>
      </gen:doubleAttribute>
      <gen:intAttribute name="TEXTURE">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="BUILDINGID">
        <gen:value>DENIAL430001ewbO</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ARTBEZEICH">
        <gen:value>Gebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Join_Cou_1">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="FUNKTION_T">
        <gen:value>Wohngebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADT">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="OVERHANG">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="OBJFLAECHE">
        <gen:value>94.93</gen:value>
      </gen:stringAttribute>
      <bldg:measuredHeight uom="#m">12.182</bldg:measuredHeight>
      <bldg:lod2Solid>
        <gml:Solid gml:id="b33db5fa-6cf1-4ff4-b666-1b147204c695">
          <gml:exterior>
            <gml:CompositeSurface gml:id="0d8525b3-27f1-492a-ab23-503ca6f3523a">
              <gml:surfaceMember xlink:href="#d9b66aa5-0bde-417d-9ef5-5bf8887dc71a"/>
              <gml:surfaceMember xlink:href="#abce1e5f-16f5-4b50-a0a7-75d16ce1b63a"/>
              <gml:surfaceMember xlink:href="#da352f02-9357-4e51-81a6-6116a7010203"/>
              <gml:surfaceMember xlink:href="#db9f31ca-3ed4-4a38-8012-a020cc692edd"/>
              <gml:surfaceMember xlink:href="#b871f450-dcbf-44d6-b932-56505b9ed08a"/>
              <gml:surfaceMember xlink:href="#a44c70a2-cc43-47b1-897e-ceee7118907d"/>
              <gml:surfaceMember xlink:href="#b8005e1e-dd13-4b28-8ab0-9e9a1e3fb4aa"/>
              <gml:surfaceMember xlink:href="#4a393e46-d31b-4f43-b877-a26c1d42b43d"/>
              <gml:surfaceMember xlink:href="#d7429457-d68d-433f-849f-5f470cc17350"/>
              <gml:surfaceMember xlink:href="#7a62d04d-a8cf-4582-b199-7f61bbf8a0a5"/>
              <gml:surfaceMember xlink:href="#c93eed19-c2ca-4dfd-9b0f-8cf070fde68c"/>
              <gml:surfaceMember xlink:href="#def45baf-c560-42d7-965a-d855da80b81c"/>
              <gml:surfaceMember xlink:href="#a289ab5d-706f-4602-bff9-499cf9253728"/>
              <gml:surfaceMember xlink:href="#5402b289-c7ee-4faf-8e2f-9f28d099e5d2"/>
              <gml:surfaceMember xlink:href="#af9b71d4-9dc7-4cf2-ac9c-b99620862813"/>
              <gml:surfaceMember xlink:href="#f580dd78-b214-4d27-b891-f49009570552"/>
              <gml:surfaceMember xlink:href="#c910634a-a2ed-44c0-b02a-d315327b8f49"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="b16073df-e707-4938-a85c-34e341b9d679">
          <gen:stringAttribute name="Type">
            <gen:value>Ground</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>94.93</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="cb110346-ea54-492c-97d0-f2b863d8b747" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="d9b66aa5-0bde-417d-9ef5-5bf8887dc71a">
                  <gml:exterior>
                    <gml:LinearRing gml:id="a69b4913-095d-4832-adb9-962bafba56ae">
                      <gml:posList>552674.024882543600000000 5799981.677221176200000000 57.119999999999997000 552674.758000000380000000 5799976.969000000500000000 57.120000000000005000 552665.225999999790000000 5799975.484999999400000000 57.119999999999997000 552664.558217657500000000 5799979.824628517000000000 57.119999999999990000 552664.527999999930000000 5799980.020999999700000000 57.119999999999997000 552664.370679647890000000 5799979.996551566800000000 57.120000000000005000 552663.048000000420000000 5799979.790999999300000000 57.120000000000005000 552662.809000000360000000 5799981.342000000200000000 57.119999999999997000 552664.126462846990000000 5799981.546326672700000000 57.120000000000005000 552664.292000000370000000 5799981.572000000600000000 57.119999999999997000 552664.259268710510000000 5799981.784878312600000000 57.120000000000005000 552663.768000000160000000 5799984.980000000400000000 57.119999999999997000 552673.280000000260000000 5799986.460999999200000000 57.119999999999983000 552674.024882543600000000 5799981.677221176200000000 57.119999999999997000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="524b96eb-e6e6-432e-bc11-869f2b35379b">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>261.26</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="0ac20c0f-961a-4e88-a2a3-4f73372b1106" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="abce1e5f-16f5-4b50-a0a7-75d16ce1b63a">
                  <gml:exterior>
                    <gml:LinearRing gml:id="e938211b-4022-4850-bd24-7eb381a07966">
                      <gml:posList>552664.292000000370000000 5799981.572000000600000000 60.804459982244595000 552664.259268710510000000 5799981.784878312600000000 60.804340817728516000 552664.259268710510000000 5799981.784878312600000000 57.120000000000005000 552664.292000000370000000 5799981.572000000600000000 57.119999999999997000 552664.292000000370000000 5799981.572000000600000000 60.804459982244595000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="a73c144a-896c-43df-a533-fe0c13a4c265">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.58</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>171.17</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="1ff1cb2d-44b2-447a-90a4-42bfbf3c841f" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="da352f02-9357-4e51-81a6-6116a7010203">
                  <gml:exterior>
                    <gml:LinearRing gml:id="c01dffed-2544-4e56-89be-37df4c16e319">
                      <gml:posList>552664.527999999930000000 5799980.020999999700000000 60.804219992706138000 552664.370679647890000000 5799979.996551566800000000 60.732119115593463000 552664.370679647890000000 5799979.996551566800000000 57.120000000000005000 552664.527999999930000000 5799980.020999999700000000 57.119999999999997000 552664.527999999930000000 5799980.020999999700000000 60.804219992706138000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="3195b183-d8b7-4b32-8198-dc3c7c493298">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>1.40</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>351.18</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="a693ca9a-997d-48fc-b715-d03ba9828924" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="db9f31ca-3ed4-4a38-8012-a020cc692edd">
                  <gml:exterior>
                    <gml:LinearRing gml:id="bf1b32f2-5e38-45aa-90a0-6b66f0595063">
                      <gml:posList>552664.292000000370000000 5799981.572000000600000000 60.804459982244595000 552664.292000000370000000 5799981.572000000600000000 57.119999999999997000 552664.126462846990000000 5799981.546326672700000000 57.120000000000005000 552664.126462846990000000 5799981.546326672700000000 60.728596848052170000 552664.126462846990000000 5799981.546326672700000000 65.498224695512548000 552664.292000000370000000 5799981.572000000600000000 65.498406832002075000 552664.292000000370000000 5799981.572000000600000000 60.804459982244595000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="daf21d3c-0bcc-4b25-b8a9-afa172176865">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>25.44</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>261.26</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="35236a79-5d96-44b9-afb4-6655558fde49" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="b871f450-dcbf-44d6-b932-56505b9ed08a">
                  <gml:exterior>
                    <gml:LinearRing gml:id="2740a068-d4d0-48c1-8971-35b8457adc25">
                      <gml:posList>552664.292000000370000000 5799981.572000000600000000 60.804459982244595000 552664.292000000370000000 5799981.572000000600000000 65.498406832002075000 552663.768000000160000000 5799984.980000000400000000 63.956655605168265000 552663.768000000160000000 5799984.980000000400000000 57.119999999999997000 552664.259268710510000000 5799981.784878312600000000 57.120000000000005000 552664.259268710510000000 5799981.784878312600000000 60.804340817728516000 552664.292000000370000000 5799981.572000000600000000 60.804459982244595000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="93f3a12b-bdef-423a-a234-2c85ce1f73b8">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>65.85</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>351.15</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="689f73b8-6d5a-41fa-81a9-65ff87c0a78e" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="a44c70a2-cc43-47b1-897e-ceee7118907d">
                  <gml:exterior>
                    <gml:LinearRing gml:id="1ac9fc5e-cfec-4130-bd13-35efa693c729">
                      <gml:posList>552673.280000000260000000 5799986.460999999200000000 57.119999999999983000 552663.768000000160000000 5799984.980000000400000000 57.119999999999997000 552663.768000000160000000 5799984.980000000400000000 63.956655605168265000 552673.280000000260000000 5799986.460999999200000000 63.964571063253551000 552673.280000000260000000 5799986.460999999200000000 57.119999999999983000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="94f42597-b23f-43c1-9b14-5f8dd8bffcf5">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>76.23</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>81.15</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="5bdb74f5-c8a7-49a4-ab57-ba31929f5300" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="b8005e1e-dd13-4b28-8ab0-9e9a1e3fb4aa">
                  <gml:exterior>
                    <gml:LinearRing gml:id="2130f754-942c-48e4-850e-7b3af44741f6">
                      <gml:posList>552674.758000000380000000 5799976.969000000500000000 57.120000000000005000 552674.024882543600000000 5799981.677221176200000000 57.119999999999997000 552673.280000000260000000 5799986.460999999200000000 57.119999999999983000 552673.280000000260000000 5799986.460999999200000000 63.964571063253551000 552674.024882543600000000 5799981.677221176200000000 66.129364013671875000 552674.758000000380000000 5799976.969000000500000000 63.998762999507179000 552674.758000000380000000 5799976.969000000500000000 57.120000000000005000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="a5d7c368-a69e-445e-99d7-12c0ae59a25a">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>66.40</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>171.15</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="da994c78-dfb5-4261-b5d5-9f7b7ae39cbb" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="4a393e46-d31b-4f43-b877-a26c1d42b43d">
                  <gml:exterior>
                    <gml:LinearRing gml:id="28076e60-c028-4cc5-b708-838e81e9f43e">
                      <gml:posList>552674.758000000380000000 5799976.969000000500000000 63.998762999507179000 552665.225999999790000000 5799975.484999999400000000 64.006745435257443000 552665.225999999790000000 5799975.484999999400000000 57.119999999999997000 552674.758000000380000000 5799976.969000000500000000 57.120000000000005000 552674.758000000380000000 5799976.969000000500000000 63.998762999507179000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="1b237d68-5445-402f-95aa-3d9456091b56">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>36.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>261.25</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="714ba260-336a-4763-b4c4-909fd08c0bf6" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="d7429457-d68d-433f-849f-5f470cc17350">
                  <gml:exterior>
                    <gml:LinearRing gml:id="05c172b1-d611-48bf-8a96-43e5555e1060">
                      <gml:posList>552665.225999999790000000 5799975.484999999400000000 64.006745435257443000 552664.527999999930000000 5799980.020999999700000000 66.058833816220798000 552664.527999999930000000 5799980.020999999700000000 60.804219992706138000 552664.527999999930000000 5799980.020999999700000000 57.119999999999997000 552664.558217657500000000 5799979.824628517000000000 57.119999999999990000 552665.225999999790000000 5799975.484999999400000000 57.119999999999997000 552665.225999999790000000 5799975.484999999400000000 64.006745435257443000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="e5d80b87-a12a-4134-be17-8700b20c21a5">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.84</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>171.17</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="3cccc78b-f48a-45f9-b098-7d23f330694f" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="7a62d04d-a8cf-4582-b199-7f61bbf8a0a5">
                  <gml:exterior>
                    <gml:LinearRing gml:id="6ea96ee2-e282-410e-93fd-24ac32e70cd9">
                      <gml:posList>552664.527999999930000000 5799980.020999999700000000 66.058833816220798000 552664.370679647890000000 5799979.996551566800000000 66.058985067128646000 552664.370679647890000000 5799979.996551566800000000 60.732119115593463000 552664.527999999930000000 5799980.020999999700000000 60.804219992706138000 552664.527999999930000000 5799980.020999999700000000 66.058833816220798000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="a3c8a73f-81fa-46be-ae90-d836457af0e3">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>8.02</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>261.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="d3ff895d-622d-4d2f-b92a-cd17c483d9ea" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="c93eed19-c2ca-4dfd-9b0f-8cf070fde68c">
                  <gml:exterior>
                    <gml:LinearRing gml:id="8c33c250-b031-4ce9-99f6-8dfca0302941">
                      <gml:posList>552664.370679647890000000 5799979.996551566800000000 66.058985067128646000 552664.346178901380000000 5799980.152030824700000000 66.129364013671875000 552664.126462846990000000 5799981.546326672700000000 65.498224695512548000 552664.126462846990000000 5799981.546326672700000000 60.728596848052170000 552664.346178901380000000 5799980.152030824700000000 60.731765748487497000 552664.370679647890000000 5799979.996551566800000000 60.732119115593463000 552664.370679647890000000 5799979.996551566800000000 66.058985067128646000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="ce19ee00-3dcc-465e-9a77-c2e964bc238e">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>4.43</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>171.17</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="8fecdfa1-4c76-489a-b504-5248cd473fc1" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="def45baf-c560-42d7-965a-d855da80b81c">
                  <gml:exterior>
                    <gml:LinearRing gml:id="2963bd70-5b91-4ddd-b3b3-e5c0de9ca743">
                      <gml:posList>552664.370679647890000000 5799979.996551566800000000 60.732119115593463000 552663.048000000420000000 5799979.790999999300000000 60.125926963796545000 552663.048000000420000000 5799979.790999999300000000 57.120000000000005000 552664.370679647890000000 5799979.996551566800000000 57.120000000000005000 552664.370679647890000000 5799979.996551566800000000 60.732119115593463000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="c47ae0c7-cee0-4c1d-b405-69eb48d66809">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>4.41</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>351.18</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="65030cb3-e86f-4c04-8da5-bfc1be23ec9e" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="a289ab5d-706f-4602-bff9-499cf9253728">
                  <gml:exterior>
                    <gml:LinearRing gml:id="7f09d97b-6ece-4a4c-8fff-8131ec60060b">
                      <gml:posList>552664.126462846990000000 5799981.546326672700000000 57.120000000000005000 552662.809000000360000000 5799981.342000000200000000 57.119999999999997000 552662.809000000360000000 5799981.342000000200000000 60.124823866215849000 552664.126462846990000000 5799981.546326672700000000 60.728596848052170000 552664.126462846990000000 5799981.546326672700000000 57.120000000000005000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="0c0add54-178d-4239-b940-9497e0f92d9a">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>4.72</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>261.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="f8a94363-289f-433c-aac4-1309302a1798" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="5402b289-c7ee-4faf-8e2f-9f28d099e5d2">
                  <gml:exterior>
                    <gml:LinearRing gml:id="e9109d8a-5b08-4ea5-b27a-75863e2bccd0">
                      <gml:posList>552663.048000000420000000 5799979.790999999300000000 60.125926963796545000 552662.809000000360000000 5799981.342000000200000000 60.124823866215849000 552662.809000000360000000 5799981.342000000200000000 57.119999999999997000 552663.048000000420000000 5799979.790999999300000000 57.120000000000005000 552663.048000000420000000 5799979.790999999300000000 60.125926963796545000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="2b9e3603-a9e6-4088-916f-03e76b8bb02b">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001ewbO_C:28816</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>50.26</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>171.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>24.09</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="82b4d191-e694-4c78-b37a-5dee690f0296" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="af9b71d4-9dc7-4cf2-ac9c-b99620862813">
                  <gml:exterior>
                    <gml:LinearRing gml:id="37e8ef9f-9f5e-4b01-98af-0e9b891c7f0f">
                      <gml:posList>552674.758000000380000000 5799976.969000000500000000 63.998762999507179000 552674.024882543600000000 5799981.677221176200000000 66.129364013671875000 552664.346178901380000000 5799980.152030824700000000 66.129364013671875000 552664.370679647890000000 5799979.996551566800000000 66.058985067128646000 552664.527999999930000000 5799980.020999999700000000 66.058833816220798000 552665.225999999790000000 5799975.484999999400000000 64.006745435257443000 552674.758000000380000000 5799976.969000000500000000 63.998762999507179000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="535b4316-eeb1-4c84-a05e-b3e16d903c89">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001ewbO_C:28816</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>51.43</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>351.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>24.09</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="efd9ddf3-f754-4bfe-b45a-f8993f39201e" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="f580dd78-b214-4d27-b891-f49009570552">
                  <gml:exterior>
                    <gml:LinearRing gml:id="b764af7b-a3f0-4206-a1a9-dea68fe2791d">
                      <gml:posList>552674.024882543600000000 5799981.677221176200000000 66.129364013671875000 552673.280000000260000000 5799986.460999999200000000 63.964571063253551000 552663.768000000160000000 5799984.980000000400000000 63.956655605168265000 552664.292000000370000000 5799981.572000000600000000 65.498406832002075000 552664.126462846990000000 5799981.546326672700000000 65.498224695512548000 552664.346178901380000000 5799980.152030824700000000 66.129364013671875000 552674.024882543600000000 5799981.677221176200000000 66.129364013671875000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="ab1459b2-e1be-47b2-bae5-5218f417ae87">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001ewbO_C:28815</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>2.30</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>261.33</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>24.36</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="36304728-5462-4d55-a6f6-df261fe33b92" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="c910634a-a2ed-44c0-b02a-d315327b8f49">
                  <gml:exterior>
                    <gml:LinearRing gml:id="172bed49-b0b6-46b1-923b-2f8bdb48625d">
                      <gml:posList>552664.346178901380000000 5799980.152030824700000000 60.731765748487497000 552664.126462846990000000 5799981.546326672700000000 60.728596848052170000 552662.809000000360000000 5799981.342000000200000000 60.124823866215849000 552663.048000000420000000 5799979.790999999300000000 60.125926963796545000 552664.370679647890000000 5799979.996551566800000000 60.732119115593463000 552664.346178901380000000 5799980.152030824700000000 60.731765748487497000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </cityObjectMember>

</CityModel>
