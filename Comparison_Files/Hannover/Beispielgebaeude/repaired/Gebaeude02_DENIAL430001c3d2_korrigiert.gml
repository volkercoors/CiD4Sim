<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<CityModel xmlns="http://www.opengis.net/citygml/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:core="http://www.opengis.net/citygml/base/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:tex="http://www.opengis.net/citygml/textures/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/citygml/2.0 http://schemas.opengis.net/citygml/2.0/cityGMLBase.xsd http://www.opengis.net/citygml/appearance/2.0  http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd">

  <gml:description>Exported by RhinoCity(TM) software - (c)Rhinoterrain 2010-2018 : www.rhinoterrain.com</gml:description>

  <gml:name>RhinoCity(TM)</gml:name>

  <gml:boundedBy>
    <gml:Envelope srsDimension="3" srsName="crs:ETRS89_UTM32*DE_DHHN92_NH">
      <gml:lowerCorner>552789.812500 5805468.000000 53.749996</gml:lowerCorner>
      <gml:upperCorner>552810.812500 5805484.000000 62.672176</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>

  <cityObjectMember>
    <bldg:Building gml:id="DENIAL430001c3d2">
      <gen:doubleAttribute name="Volume">
        <gen:value>1165.532</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="TotalRoofArea">
        <gen:value>194.55</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="TotalWallArea">
        <gen:value>430.90</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Authoring_Software">
        <gen:value>RhinoCity</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="OSKA">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ALKISID">
        <gen:value>DENIAL430001c3d2</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="KACHEL_ID">
        <gen:value>11_10_Building</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="COUNT">
        <gen:value>850</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="DESC">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCK">
        <gen:value>12</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCKNR">
        <gen:value>104012</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STATBEZNR">
        <gen:value>104</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="MBZNR">
        <gen:value>1031</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="WBZNR">
        <gen:value>1030</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNAM">
        <gen:value>List</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNR">
        <gen:value>10</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNAM">
        <gen:value>Vahrenwald-List</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNR">
        <gen:value>02</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEMEINDE">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SCALE">
        <gen:value>5000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_overlaps">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_xmax">
        <gen:value>552810.756</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_xmin">
        <gen:value>552789.8250000002</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_ymax">
        <gen:value>5805483.767999999</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_ymin">
        <gen:value>5805468.116</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_zmax">
        <gen:value>54.3384440000616</gen:value>
      </gen:stringAttribute>
      <gen:doubleAttribute name="ZMIN">
        <gen:value>53.750000</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="THICKNESS">
        <gen:value>0.100000</gen:value>
      </gen:doubleAttribute>
      <gen:intAttribute name="TEXTURE">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="BUILDINGID">
        <gen:value>DENIAL430001c3d2</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ARTBEZEICH">
        <gen:value>Gebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Join_Cou_1">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="FUNKTION_T">
        <gen:value>Wohngebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADT">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="OVERHANG">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="OBJFLAECHE">
        <gen:value>162.63</gen:value>
      </gen:stringAttribute>
      <bldg:measuredHeight uom="#m">14.267</bldg:measuredHeight>
      <bldg:lod2Solid>
        <gml:Solid gml:id="911369c6-63fc-4ec9-b786-99680372306e">
          <gml:exterior>
            <gml:CompositeSurface gml:id="3b2bccf0-7dd7-477a-a6a7-e5746f0b3766">
              <gml:surfaceMember xlink:href="#69856e08-0d5d-4e29-886c-45e201f8f923"/>
              <gml:surfaceMember xlink:href="#8c27eb45-72af-4bbe-a1d9-76daafe33bde"/>
              <gml:surfaceMember xlink:href="#79163004-6391-4270-bf16-41a2cb081180"/>
              <gml:surfaceMember xlink:href="#241c4a3e-6531-41d5-b293-a27d5550eac8"/>
              <gml:surfaceMember xlink:href="#de8e6bfb-476d-40ff-a62a-15feacf21c39"/>
              <gml:surfaceMember xlink:href="#4f039765-795a-4ff4-a76a-7c71d5957e25"/>
              <gml:surfaceMember xlink:href="#33d8bceb-02d4-4860-ac23-088fca81991f"/>
              <gml:surfaceMember xlink:href="#ec1a7209-ecf5-48eb-b983-d9dae387153c"/>
              <gml:surfaceMember xlink:href="#b110c17e-50ac-442f-8b21-e9f9e8b4e98a"/>
              <gml:surfaceMember xlink:href="#0cffcb47-4ef7-4343-9440-6a45d43aeeda"/>
              <gml:surfaceMember xlink:href="#f50311a5-d057-4aad-80fc-e9c0603250c3"/>
              <gml:surfaceMember xlink:href="#b642ad1a-072a-45c8-923a-5209b3bb52e3"/>
              <gml:surfaceMember xlink:href="#c10ea5e7-aaec-45a0-a97f-877b90785106"/>
              <gml:surfaceMember xlink:href="#2d997224-5ca6-48b0-9186-1d372dbed0a9"/>
              <gml:surfaceMember xlink:href="#6f5294bb-2254-4f49-b92d-ea07467ba403"/>
              <gml:surfaceMember xlink:href="#37a2f96e-4b46-4321-a3ee-ce19dde0cb94"/>
              <gml:surfaceMember xlink:href="#acaa16d7-81f5-4863-9251-088d497116d2"/>
              <gml:surfaceMember xlink:href="#8085e57a-20a3-4e28-94ad-27511f8833e6"/>
              <gml:surfaceMember xlink:href="#41926223-a6bf-46a0-b5f9-a45e74767012"/>
              <gml:surfaceMember xlink:href="#3d47088a-5e84-4f05-86cd-979031d7b310"/>
              <gml:surfaceMember xlink:href="#f4ac8768-099a-49a7-a0cb-0dfd2affd24d"/>
              <gml:surfaceMember xlink:href="#0f423cd0-b89b-427b-8048-975e07306a3d"/>
              <gml:surfaceMember xlink:href="#8073a748-1a6e-489a-8cc8-048be6d0d7b6"/>
              <gml:surfaceMember xlink:href="#f665870c-08ac-4778-9837-98d9c0f68769"/>
              <gml:surfaceMember xlink:href="#6971feed-7e92-4eed-a84c-d7c7699b4673"/>
              <gml:surfaceMember xlink:href="#4ac67d59-c489-43da-bedc-38db59f149b0"/>
              <gml:surfaceMember xlink:href="#ac9adf65-1551-436c-a896-9d4a886741e8"/>
              <gml:surfaceMember xlink:href="#db6bd8f8-f051-44b6-aeab-b8088dbbaa5a"/>
              <gml:surfaceMember xlink:href="#f454f62c-512b-42c0-a1eb-b6a9dd385b78"/>
              <gml:surfaceMember xlink:href="#1807eb0d-adf3-42d4-9de0-83bd9012c0f0"/>
              <gml:surfaceMember xlink:href="#2e5dff48-867d-42f3-80d6-72a5056e8601"/>
              <gml:surfaceMember xlink:href="#8ef0308d-10f1-49e2-a53a-e7d187b0a818"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="febaa161-d1d3-4f9c-b8f5-fb8becf30917">
          <gen:stringAttribute name="Type">
            <gen:value>Ground</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>162.63</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="fcef5725-8062-4aeb-99b5-b21399421664" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="69856e08-0d5d-4e29-886c-45e201f8f923">
                  <gml:exterior>
                    <gml:LinearRing gml:id="64ac95fe-e7c0-41c0-8e14-4dbc82fe6dbe">
                      <gml:posList>552802.779295607940000000 5805470.715565425300000000 53.750000000000007000 552801.459631668170000000 5805469.993388736600000000 53.750000000000000000 552798.029000000100000000 5805468.116000000400000000 53.750000000000000000 552797.809569380940000000 5805468.516900039300000000 53.750000000000000000 552796.554999999700000000 5805470.809000000400000000 53.750000000000000000 552795.918882989790000000 5805471.971132600700000000 53.750000000000007000 552794.929067623450000000 5805473.779442532900000000 53.750000000000000000 552793.939252257230000000 5805475.587752466100000000 53.750000000000000000 552793.672000000250000000 5805476.075999999400000000 53.750000000000000000 552793.564364697320000000 5805476.017283975100000000 53.750000000000000000 552792.002000000330000000 5805475.164999999100000000 53.750000000000000000 552789.825000000190000000 5805479.140000000600000000 53.750000000000000000 552791.377879375590000000 5805479.989796116000000000 53.750000000000000000 552794.897464812270000000 5805481.915850437200000000 53.750000000000000000 552798.281999999660000000 5805483.767999999200000000 53.750000000000000000 552799.074705763140000000 5805482.319873966300000000 53.749999999999993000 552801.809999999590000000 5805477.323000000800000000 53.750000000000000000 552801.959936687260000000 5805477.405017553800000000 53.750000000000000000 552806.064000000250000000 5805479.650000000400000000 53.750000000000014000 552810.756000000050000000 5805471.083000000600000000 53.750000000000028000 552806.169999999930000000 5805468.571000000500000000 53.750000000000014000 552804.612626252700000000 5805471.415864329800000000 53.750000000000000000 552804.485000000340000000 5805471.649000000200000000 53.750000000000014000 552802.779295607940000000 5805470.715565425300000000 53.750000000000007000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="f38fc2ae-47c8-4efd-9f7e-348d0d717c58">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>7.35</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>241.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="50b2903f-b010-4e3a-a963-fcacc22a9767" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="8c27eb45-72af-4bbe-a1d9-76daafe33bde">
                  <gml:exterior>
                    <gml:LinearRing gml:id="a88184cc-7dd3-4c89-85b9-b465fe3359fa">
                      <gml:posList>552793.939252257110000000 5805475.587752466100000000 60.329868316650391000 552793.672000000250000000 5805476.075999999400000000 60.329868316650391000 552792.048565865030000000 5805479.041985026600000000 60.329868316650391000 552792.048565865030000000 5805479.041985026600000000 58.468727553464952000 552793.672000000250000000 5805476.075999999400000000 58.460137008310802000 552793.939252257230000000 5805475.587752466100000000 58.458731980315122000 552793.939252257110000000 5805475.587752466100000000 60.329868316650391000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="a8f52ce7-21f7-41f9-8c09-211038ca27bc">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>1.60</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>331.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="a7fc8c88-3002-4449-a404-ce9b2d7fc602" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="79163004-6391-4270-bf16-41a2cb081180">
                  <gml:exterior>
                    <gml:LinearRing gml:id="947ad877-9d37-43a5-a4ff-64c3851df684">
                      <gml:posList>552793.556140601290000000 5805479.867161382000000000 60.329868316650405000 552792.048565865030000000 5805479.041985026600000000 58.468727553464952000 552792.048565865030000000 5805479.041985026600000000 60.329868316650391000 552793.556140601290000000 5805479.867161382000000000 60.329868316650405000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="9f9e92b4-c9fb-43a1-ac67-3fd06a87f2c4">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>1.65</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>151.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="b46744be-5bf7-42f8-8367-b207f13aa476" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="241c4a3e-6531-41d5-b293-a27d5550eac8">
                  <gml:exterior>
                    <gml:LinearRing gml:id="0cc66948-0d4e-4e94-bdc5-a73a816f8bab">
                      <gml:posList>552799.341767877690000000 5805469.355554297600000000 60.329868316650419000 552797.809569380940000000 5805468.516900039300000000 60.329868316650391000 552797.809569380940000000 5805468.516900039300000000 58.438328871951825000 552799.341767877690000000 5805469.355554297600000000 60.329868316650419000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="20246c8e-124b-46cb-80a1-4bb7ce664aab">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>7.43</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>241.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="2432dad5-b416-453d-94b2-75c22b508984" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="de8e6bfb-476d-40ff-a62a-15feacf21c39">
                  <gml:exterior>
                    <gml:LinearRing gml:id="acaca406-78fc-484a-8d36-87b875612bab">
                      <gml:posList>552797.809569380940000000 5805468.516900039300000000 60.329868316650391000 552796.554999999700000000 5805470.809000000400000000 60.329868316650391000 552795.918882988860000000 5805471.971132599700000000 60.329868316650391000 552795.918882989790000000 5805471.971132600700000000 58.448324447628636000 552796.554999999700000000 5805470.809000000400000000 58.444980183239757000 552797.809569380940000000 5805468.516900039300000000 58.438328871951825000 552797.809569380940000000 5805468.516900039300000000 60.329868316650391000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="57436d5d-3012-4e61-b3db-e6fe457eb4f0">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>11.99</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>241.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="5fac4f83-9ca3-471b-a8de-6b2ac2805702" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="4f039765-795a-4ff4-a76a-7c71d5957e25">
                  <gml:exterior>
                    <gml:LinearRing gml:id="7e91e705-28c1-4f1a-8414-01e568696f2c">
                      <gml:posList>552795.918882988860000000 5805471.971132599700000000 60.329868316650391000 552794.929067622990000000 5805473.779442532900000000 62.392517089843750000 552793.939252257110000000 5805475.587752466100000000 60.329868316650391000 552793.939252257230000000 5805475.587752466100000000 58.458731980315122000 552794.929067623450000000 5805473.779442532900000000 58.453528213971872000 552795.918882989790000000 5805471.971132600700000000 58.448324447628636000 552795.918882988860000000 5805471.971132599700000000 60.329868316650391000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="719e3167-7179-41e1-9c5f-f8f3e9550fb1">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>1.66</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>330.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="afdbe05e-3eae-4a14-a0d6-f0c456a3c997" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="33d8bceb-02d4-4860-ac23-088fca81991f">
                  <gml:exterior>
                    <gml:LinearRing gml:id="313781a6-7aac-4bdf-8096-a2b5332ac167">
                      <gml:posList>552799.074705763140000000 5805482.319873966300000000 60.394882202148437000 552799.074705763140000000 5805482.319873966300000000 58.498202210685612000 552797.545942239700000000 5805481.465134386900000000 60.394882202148423000 552799.074705763140000000 5805482.319873966300000000 60.394882202148437000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="2090bc00-f006-4d51-a15b-3fa1bda95810">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>10.76</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>61.30</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="b87bf1bd-f6ef-4e97-a6e7-39f197feab5b" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="ec1a7209-ecf5-48eb-b983-d9dae387153c">
                  <gml:exterior>
                    <gml:LinearRing gml:id="e4f9754a-6ff6-4e62-b3ab-eab04257e7b6">
                      <gml:posList>552801.809999999590000000 5805477.323000000800000000 58.512436316964383000 552799.074705763140000000 5805482.319873966300000000 58.498202210685612000 552799.074705763140000000 5805482.319873966300000000 60.394882202148437000 552801.809999999590000000 5805477.323000000800000000 60.394882202148437000 552801.809999999590000000 5805477.323000000800000000 58.512436316964383000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="03e675e3-9be7-432d-a5cc-d8dd64abdf91">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>7.68</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>151.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="dc759b0c-30c8-467f-9507-21fc1a491cb2" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="b110c17e-50ac-442f-8b21-e9f9e8b4e98a">
                  <gml:exterior>
                    <gml:LinearRing gml:id="7f2f861c-f8ec-4351-91bc-95c5c1b87301">
                      <gml:posList>552793.564364697320000000 5805476.017283975100000000 58.327362060546875000 552792.002000000330000000 5805475.164999999100000000 57.806973158026942000 552792.002000000330000000 5805475.164999999100000000 53.750000000000000000 552793.564364697320000000 5805476.017283975100000000 53.750000000000000000 552793.564364697320000000 5805476.017283975100000000 58.327362060546875000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="a7d80339-f67d-4509-835f-db17ed6ae767">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>18.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>241.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="1fc00caa-20dd-4c6d-aba8-da08b1e3dda4" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="0cffcb47-4ef7-4343-9440-6a45d43aeeda">
                  <gml:exterior>
                    <gml:LinearRing gml:id="ddd1dcd4-d855-46ee-84c6-c6f0fa1ff190">
                      <gml:posList>552792.002000000330000000 5805475.164999999100000000 57.806973158026942000 552789.825000000190000000 5805479.140000000600000000 57.809753732080281000 552789.825000000190000000 5805479.140000000600000000 53.750000000000000000 552792.002000000330000000 5805475.164999999100000000 53.750000000000000000 552792.002000000330000000 5805475.164999999100000000 57.806973158026942000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="89043347-c8e8-4e63-acdb-3d026f8a5137">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>7.64</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>331.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="78b69498-72c9-4bfa-b9fe-0ac9c6767759" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="f50311a5-d057-4aad-80fc-e9c0603250c3">
                  <gml:exterior>
                    <gml:LinearRing gml:id="e922b4f1-c925-486d-b23c-6f25cccf1939">
                      <gml:posList>552791.377879375590000000 5805479.989796116000000000 53.750000000000000000 552789.825000000190000000 5805479.140000000600000000 53.750000000000000000 552789.825000000190000000 5805479.140000000600000000 57.809753732080281000 552791.377879375590000000 5805479.989796116000000000 58.327362060546875000 552791.377879375590000000 5805479.989796116000000000 53.750000000000000000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="35b4483a-9071-45cd-9866-907d38c296bd">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.80</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>331.32</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="2ea98097-1b1e-44fa-b0dd-5c42d1dcd796" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="b642ad1a-072a-45c8-923a-5209b3bb52e3">
                  <gml:exterior>
                    <gml:LinearRing gml:id="a28c4bc2-fe80-4c5d-b667-8acdc8d91103">
                      <gml:posList>552801.959936687260000000 5805477.405017553800000000 53.750000000000000000 552801.809999999590000000 5805477.323000000800000000 53.750000000000000000 552801.809999999590000000 5805477.323000000800000000 58.512436316964383000 552801.959936687260000000 5805477.405017553800000000 58.327362060546875000 552801.959936687260000000 5805477.405017553800000000 53.750000000000000000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="4900f235-87f7-4112-b20f-55da5da94340">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>35.50</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>331.32</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="338ceafa-449f-48e7-8f3a-6393b63cdcef" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="c10ea5e7-aaec-45a0-a97f-877b90785106">
                  <gml:exterior>
                    <gml:LinearRing gml:id="e8d2849b-86d2-4dbf-86f4-2deb8a21b2bb">
                      <gml:posList>552801.959936687260000000 5805477.405017553800000000 53.750000000000000000 552801.959936687260000000 5805477.405017553800000000 58.327362060546875000 552801.809999999590000000 5805477.323000000800000000 58.512436316964383000 552801.809999999590000000 5805477.323000000800000000 60.394882202148437000 552800.284948886600000000 5805476.488774814600000000 60.394882202148445000 552799.992905954250000000 5805476.329023074400000000 60.755365212579946000 552799.803125814650000000 5805476.225210572600000000 60.989620208740213000 552799.992905954250000000 5805476.329023074400000000 60.989620208740213000 552801.809999999590000000 5805477.323000000800000000 60.989620208740220000 552806.064000000250000000 5805479.650000000400000000 60.989620208740234000 552806.064000000250000000 5805479.650000000400000000 53.750000000000014000 552801.959936687260000000 5805477.405017553800000000 53.750000000000000000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="291d283e-f75e-4bd4-a5a3-abff36be64e3">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>25.40</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>241.30</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="69b6c9b8-1909-4e2f-a895-dc9d50be12fc" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="2d997224-5ca6-48b0-9186-1d372dbed0a9">
                  <gml:exterior>
                    <gml:LinearRing gml:id="154fa731-7242-4928-96ed-2d9de4d2fd6a">
                      <gml:posList>552806.169999999930000000 5805468.571000000500000000 60.989620208740234000 552804.485000000340000000 5805471.649000000200000000 60.989620208740234000 552804.485000000340000000 5805471.649000000200000000 58.937450380600858000 552804.485000000340000000 5805471.649000000200000000 53.750000000000014000 552804.612626252700000000 5805471.415864329800000000 53.750000000000000000 552806.169999999930000000 5805468.571000000500000000 53.750000000000014000 552806.169999999930000000 5805468.571000000500000000 60.989620208740234000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="6f3934f4-2b6e-4955-9b73-c5a55ad9830c">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>14.08</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>151.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="f5e7dcd6-7917-413f-a13d-9dbd1b57095f" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="6f5294bb-2254-4f49-b92d-ea07467ba403">
                  <gml:exterior>
                    <gml:LinearRing gml:id="8ff0348a-77e0-40f3-8cf7-a0a727bf75fc">
                      <gml:posList>552804.485000000340000000 5805471.649000000200000000 60.989620208740234000 552802.822608707240000000 5805470.739268209800000000 60.989620208740213000 552802.779295607940000000 5805470.715565425300000000 60.989620208740234000 552802.779295607940000000 5805470.715565425300000000 53.750000000000007000 552804.485000000340000000 5805471.649000000200000000 53.750000000000014000 552804.485000000340000000 5805471.649000000200000000 58.937450380600858000 552804.485000000340000000 5805471.649000000200000000 60.989620208740234000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="ede672a7-b7af-4b52-95d3-91f886945569">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>70.71</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>61.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="0b18f2cd-357f-4999-9b40-1a23a8c7575c" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="37a2f96e-4b46-4321-a3ee-ce19dde0cb94">
                  <gml:exterior>
                    <gml:LinearRing gml:id="a78d6d08-fd25-4906-84b4-5500e6737186">
                      <gml:posList>552810.756000000050000000 5805471.083000000600000000 53.750000000000028000 552806.064000000250000000 5805479.650000000400000000 53.750000000000014000 552806.064000000250000000 5805479.650000000400000000 60.989620208740234000 552810.756000000050000000 5805471.083000000600000000 60.989620208740234000 552810.756000000050000000 5805471.083000000600000000 53.750000000000028000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="402be083-7bca-4651-9e16-c747814e51d5">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>37.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>151.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="ab6d4061-0777-4e0f-8e78-38e681fd2c2b" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="acaa16d7-81f5-4863-9251-088d497116d2">
                  <gml:exterior>
                    <gml:LinearRing gml:id="81a7afd5-afe1-4774-a5e7-75c9ad54a948">
                      <gml:posList>552810.756000000050000000 5805471.083000000600000000 60.989620208740234000 552806.169999999930000000 5805468.571000000500000000 60.989620208740234000 552806.169999999930000000 5805468.571000000500000000 53.750000000000014000 552810.756000000050000000 5805471.083000000600000000 53.750000000000028000 552810.756000000050000000 5805471.083000000600000000 60.989620208740234000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="4669ac73-c25e-4214-8c60-e38112e54bbc">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>38.81</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>151.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="0f44a7db-0b5f-42d0-924b-b85668bcb997" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="8085e57a-20a3-4e28-94ad-27511f8833e6">
                  <gml:exterior>
                    <gml:LinearRing gml:id="89a64759-e789-40e6-ae93-e69cca132c59">
                      <gml:posList>552802.779295607940000000 5805470.715565425300000000 60.989620208740234000 552802.822608707240000000 5805470.739268209800000000 60.989620208740213000 552801.459631668170000000 5805469.993388736600000000 62.672172546386719000 552798.029000000100000000 5805468.116000000400000000 58.437165523494450000 552798.029000000100000000 5805468.116000000400000000 53.750000000000000000 552801.459631668170000000 5805469.993388736600000000 53.750000000000000000 552802.779295607940000000 5805470.715565425300000000 53.750000000000007000 552802.779295607940000000 5805470.715565425300000000 60.989620208740234000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="9830ea97-2344-4edf-92af-fecc418b960c">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>2.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>241.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="c314f69c-3119-483a-a67f-bbc32ce9d08c" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="41926223-a6bf-46a0-b5f9-a45e74767012">
                  <gml:exterior>
                    <gml:LinearRing gml:id="36bc2870-d21e-4cab-9f49-0cfc5d82a98a">
                      <gml:posList>552798.029000000100000000 5805468.116000000400000000 58.437165523494450000 552797.809569380940000000 5805468.516900039300000000 58.438328871951825000 552797.809569380940000000 5805468.516900039300000000 53.750000000000000000 552798.029000000100000000 5805468.116000000400000000 53.750000000000000000 552798.029000000100000000 5805468.116000000400000000 58.437165523494450000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="7cea18a6-fab1-40ea-bef3-d1dd1a43e13a">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>40.50</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>241.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="a3d318d9-1bcd-418b-acbb-84e7ed1936ce" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="3d47088a-5e84-4f05-86cd-979031d7b310">
                  <gml:exterior>
                    <gml:LinearRing gml:id="2c1c9c24-7e67-4ce7-86de-f1cc118f02ec">
                      <gml:posList>552797.809569380940000000 5805468.516900039300000000 58.438328871951825000 552796.554999999700000000 5805470.809000000400000000 58.444980183239757000 552795.918882989790000000 5805471.971132600700000000 58.448324447628636000 552794.929067623450000000 5805473.779442532900000000 58.453528213971872000 552793.939252257230000000 5805475.587752466100000000 58.458731980315122000 552793.672000000250000000 5805476.075999999400000000 58.460137008310802000 552793.672000000250000000 5805476.075999999400000000 53.750000000000000000 552793.939252257230000000 5805475.587752466100000000 53.750000000000000000 552794.929067623450000000 5805473.779442532900000000 53.750000000000000000 552795.918882989790000000 5805471.971132600700000000 53.750000000000007000 552796.554999999700000000 5805470.809000000400000000 53.750000000000000000 552797.809569380940000000 5805468.516900039300000000 53.750000000000000000 552797.809569380940000000 5805468.516900039300000000 58.438328871951825000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="0e657a1d-695e-4c73-b7f5-b62855f369c1">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.57</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>151.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="a5cf183d-6ed4-480e-a162-6e46adc233fd" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="f4ac8768-099a-49a7-a0cb-0dfd2affd24d">
                  <gml:exterior>
                    <gml:LinearRing gml:id="f93b4e5f-a28c-477c-8b15-04f882f647c5">
                      <gml:posList>552793.672000000250000000 5805476.075999999400000000 58.460137008310802000 552793.564364697320000000 5805476.017283975100000000 58.327362060546875000 552793.564364697320000000 5805476.017283975100000000 53.750000000000000000 552793.672000000250000000 5805476.075999999400000000 53.750000000000000000 552793.672000000250000000 5805476.075999999400000000 58.460137008310802000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="1523d126-cf11-452d-aefd-9162b13bed77">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>53.44</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>331.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="7c03504a-9fc5-4ce6-afcb-ae701bb20778" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="0f423cd0-b89b-427b-8048-975e07306a3d">
                  <gml:exterior>
                    <gml:LinearRing gml:id="a3f226cf-7738-4440-a7a7-92cdded4b518">
                      <gml:posList>552798.281999999660000000 5805483.767999999200000000 53.750000000000000000 552794.897464812270000000 5805481.915850437200000000 53.750000000000000000 552791.377879375590000000 5805479.989796116000000000 53.750000000000000000 552791.377879375590000000 5805479.989796116000000000 58.327362060546875000 552794.897464812270000000 5805481.915850437200000000 62.672172546386719000 552798.281999999660000000 5805483.767999999200000000 58.494077075651049000 552798.281999999660000000 5805483.767999999200000000 53.750000000000000000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="2735373a-4ea4-42ae-9255-656c3c325727">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>34.92</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>61.30</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="0f524031-bc43-4987-8a05-37fb5e2de9e3" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="8073a748-1a6e-489a-8cc8-048be6d0d7b6">
                  <gml:exterior>
                    <gml:LinearRing gml:id="a2972e72-85ef-4135-803e-833a32cb94fe">
                      <gml:posList>552801.809999999590000000 5805477.323000000800000000 53.750000000000000000 552799.074705763140000000 5805482.319873966300000000 53.749999999999993000 552798.281999999660000000 5805483.767999999200000000 53.750000000000000000 552798.281999999660000000 5805483.767999999200000000 58.494077075651049000 552799.074705763140000000 5805482.319873966300000000 58.498202210685612000 552801.809999999590000000 5805477.323000000800000000 58.512436316964383000 552801.809999999590000000 5805477.323000000800000000 53.750000000000000000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="3bdee761-2ac0-4a22-96f1-bd6990c9bdbf">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001c3d2_C:28879</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>62.98</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="b49ad3bd-5ef7-4672-8901-30b903cfc49f" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="f665870c-08ac-4778-9837-98d9c0f68769">
                  <gml:exterior>
                    <gml:LinearRing gml:id="2b2aa886-b140-47b3-91dd-dde5bc178b6f">
                      <gml:posList>552810.756000000050000000 5805471.083000000600000000 60.989620208740234000 552806.064000000250000000 5805479.650000000400000000 60.989620208740234000 552801.809999999590000000 5805477.323000000800000000 60.989620208740220000 552799.992905954250000000 5805476.329023074400000000 60.989620208740213000 552799.803125814650000000 5805476.225210572600000000 60.989620208740213000 552802.822608707240000000 5805470.739268209800000000 60.989620208740213000 552804.485000000340000000 5805471.649000000200000000 60.989620208740234000 552806.169999999930000000 5805468.571000000500000000 60.989620208740234000 552810.756000000050000000 5805471.083000000600000000 60.989620208740234000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="314a607d-996b-487f-a286-3e95cf5e0359">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001c3d2_C:28875</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>6.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="2d48120d-0e4c-45a1-b807-792908740e56" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="6971feed-7e92-4eed-a84c-d7c7699b4673">
                  <gml:exterior>
                    <gml:LinearRing gml:id="0653c25d-42ff-4755-88c5-ce9c8dd5220b">
                      <gml:posList>552793.939252257110000000 5805475.587752466100000000 60.329868316650391000 552795.454923680280000000 5805476.417360572100000000 60.329868316650405000 552793.556140601290000000 5805479.867161382000000000 60.329868316650405000 552792.048565865030000000 5805479.041985026600000000 60.329868316650391000 552793.672000000250000000 5805476.075999999400000000 60.329868316650391000 552793.939252257110000000 5805475.587752466100000000 60.329868316650391000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="d98bb3a1-f440-47ae-af33-850c48c1a649">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001c3d2_C:28874</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>6.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="8cf7fe58-c821-4819-a737-08df6bfbc2b9" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="4ac67d59-c489-43da-bedc-38db59f149b0">
                  <gml:exterior>
                    <gml:LinearRing gml:id="61630d48-3cd3-4584-97a9-0c46e042db8a">
                      <gml:posList>552799.341767877690000000 5805469.355554297600000000 60.329868316650419000 552797.442984798690000000 5805472.805355107400000000 60.329868316650398000 552795.918882988860000000 5805471.971132599700000000 60.329868316650391000 552796.554999999700000000 5805470.809000000400000000 60.329868316650391000 552797.809569380940000000 5805468.516900039300000000 60.329868316650391000 552799.341767877690000000 5805469.355554297600000000 60.329868316650419000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="7562ba94-07e8-409f-b440-fc85e429d693">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001c3d2_C:28873</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>7.82</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>331.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>45.02</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="cc369980-a715-4b42-a929-59af0416abe7" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="ac9adf65-1551-436c-a896-9d4a886741e8">
                  <gml:exterior>
                    <gml:LinearRing gml:id="ffb9bb23-ab21-4994-b907-f69ad5eebf0c">
                      <gml:posList>552795.454923680280000000 5805476.417360572100000000 60.329868316650405000 552793.939252257110000000 5805475.587752466100000000 60.329868316650391000 552794.929067622990000000 5805473.779442532900000000 62.392517089843750000 552798.119733165720000000 5805475.525918464200000000 62.392517089843750000 552795.454923680280000000 5805476.417360572100000000 60.329868316650405000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="a4d8e3f7-8af1-4493-8e6c-2955ea897f98">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001c3d2_C:28873</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>7.84</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>151.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>45.02</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="6afd52a1-dc2b-413b-b8ff-cbd3ebafa6ca" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="db6bd8f8-f051-44b6-aeab-b8088dbbaa5a">
                  <gml:exterior>
                    <gml:LinearRing gml:id="8277c892-b82b-4598-a230-3172cdae3d8b">
                      <gml:posList>552798.119733165720000000 5805475.525918464200000000 62.392517089843750000 552794.929067622990000000 5805473.779442532900000000 62.392517089843750000 552795.918882988860000000 5805471.971132599700000000 60.329868316650391000 552797.442984798690000000 5805472.805355107400000000 60.329868316650398000 552798.119733165720000000 5805475.525918464200000000 62.392517089843750000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="93aa0df4-481e-4443-bd61-ccf154e383b6">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001c3d2_C:28876</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>9.93</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="e39fe812-c211-4c89-9997-96f722d60c77" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="f454f62c-512b-42c0-a1eb-b6a9dd385b78">
                  <gml:exterior>
                    <gml:LinearRing gml:id="6b9d67e0-fb5d-42df-9676-45380174a3af">
                      <gml:posList>552801.809999999590000000 5805477.323000000800000000 60.394882202148437000 552799.074705763140000000 5805482.319873966300000000 60.394882202148437000 552797.545942239700000000 5805481.465134386900000000 60.394882202148423000 552800.284948886600000000 5805476.488774814600000000 60.394882202148445000 552801.809999999590000000 5805477.323000000800000000 60.394882202148437000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="0d345677-04bb-408a-a28a-051a9ec9910d">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001c3d2_C:28877</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>8.38</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>241.17</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>16.30</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="0f3e1691-9c04-4c21-a16f-5bbacbda0c24" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="1807eb0d-adf3-42d4-9de0-83bd9012c0f0">
                  <gml:exterior>
                    <gml:LinearRing gml:id="8139965f-abb4-4c5f-b753-4d01e8fea44a">
                      <gml:posList>552793.564364697320000000 5805476.017283975100000000 58.327362060546875000 552791.377879375590000000 5805479.989796116000000000 58.327362060546875000 552789.825000000190000000 5805479.140000000600000000 57.809753732080281000 552792.002000000330000000 5805475.164999999100000000 57.806973158026942000 552793.564364697320000000 5805476.017283975100000000 58.327362060546875000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="057ea2af-568f-4826-b86d-00c34ba98596">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001c3d2_C:28878</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>41.40</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>61.17</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>47.28</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="7d187cea-12d8-427e-8971-2bc2d4be6d9a" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="2e5dff48-867d-42f3-80d6-72a5056e8601">
                  <gml:exterior>
                    <gml:LinearRing gml:id="011dff1d-809e-45be-acff-36cc148f4d53">
                      <gml:posList>552799.992905954250000000 5805476.329023074400000000 60.755365212579946000 552800.284948886600000000 5805476.488774814600000000 60.394882202148445000 552797.545942239700000000 5805481.465134386900000000 60.394882202148423000 552799.074705763140000000 5805482.319873966300000000 58.498202210685612000 552798.281999999660000000 5805483.767999999200000000 58.494077075651049000 552794.897464812270000000 5805481.915850437200000000 62.672172546386719000 552801.459631668170000000 5805469.993388736600000000 62.672172546386719000 552802.822608707240000000 5805470.739268209800000000 60.989620208740213000 552799.803125814650000000 5805476.225210572600000000 60.989620208740213000 552799.992905954250000000 5805476.329023074400000000 60.755365212579946000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="dbbf332e-1fa1-4a1d-abc7-2ad9fff5c6b7">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001c3d2_C:28878</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>42.55</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>241.17</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>47.28</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="4baac4b1-dbbf-4d33-9e59-4e6f6e1a9868" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="8ef0308d-10f1-49e2-a53a-e7d187b0a818">
                  <gml:exterior>
                    <gml:LinearRing gml:id="58145cab-cfee-40df-bca2-630babe30af2">
                      <gml:posList>552797.442984798690000000 5805472.805355107400000000 60.329868316650398000 552799.341767877690000000 5805469.355554297600000000 60.329868316650419000 552797.809569380940000000 5805468.516900039300000000 58.438328871951825000 552798.029000000100000000 5805468.116000000400000000 58.437165523494450000 552801.459631668170000000 5805469.993388736600000000 62.672172546386719000 552794.897464812270000000 5805481.915850437200000000 62.672172546386719000 552791.377879375590000000 5805479.989796116000000000 58.327362060546875000 552793.564364697320000000 5805476.017283975100000000 58.327362060546875000 552793.672000000250000000 5805476.075999999400000000 58.460137008310802000 552792.048565865030000000 5805479.041985026600000000 58.468727553464952000 552793.556140601290000000 5805479.867161382000000000 60.329868316650405000 552795.454923680280000000 5805476.417360572100000000 60.329868316650405000 552798.119733165720000000 5805475.525918464200000000 62.392517089843750000 552797.442984798690000000 5805472.805355107400000000 60.329868316650398000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </cityObjectMember>

</CityModel>
