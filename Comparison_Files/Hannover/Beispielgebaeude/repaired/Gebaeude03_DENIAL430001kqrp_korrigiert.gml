<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<CityModel xmlns="http://www.opengis.net/citygml/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:core="http://www.opengis.net/citygml/base/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:tex="http://www.opengis.net/citygml/textures/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/citygml/2.0 http://schemas.opengis.net/citygml/2.0/cityGMLBase.xsd http://www.opengis.net/citygml/appearance/2.0  http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd">

  <gml:description>Exported by RhinoCity(TM) software - (c)Rhinoterrain 2010-2018 : www.rhinoterrain.com</gml:description>

  <gml:name>RhinoCity(TM)</gml:name>

  <gml:boundedBy>
    <gml:Envelope srsDimension="3" srsName="crs:ETRS89_UTM32*DE_DHHN92_NH">
      <gml:lowerCorner>552620.875000 5806290.500000 51.839996</gml:lowerCorner>
      <gml:upperCorner>552636.375000 5806323.000000 63.921291</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>

  <cityObjectMember>
    <bldg:Building gml:id="DENIAL430001kqrp">
      <gen:doubleAttribute name="Volume">
        <gen:value>3540.486</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="TotalRoofArea">
        <gen:value>450.83</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="TotalWallArea">
        <gen:value>769.35</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Authoring_Software">
        <gen:value>RhinoCity</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="OSKA">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ALKISID">
        <gen:value>DENIAL430001kqrp</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="KACHEL_ID">
        <gen:value>11_11_Building</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="COUNT">
        <gen:value>1021</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="DESC">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCK">
        <gen:value>02</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCKNR">
        <gen:value>103002</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STATBEZNR">
        <gen:value>103</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="MBZNR">
        <gen:value>1023</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="WBZNR">
        <gen:value>1023</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNAM">
        <gen:value>List</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNR">
        <gen:value>10</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNAM">
        <gen:value>Vahrenwald-List</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNR">
        <gen:value>02</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEMEINDE">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SCALE">
        <gen:value>5000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_overlaps">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_xmax">
        <gen:value>552636.3370000003</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_xmin">
        <gen:value>552620.9029999999</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_ymax">
        <gen:value>5806322.85</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_ymin">
        <gen:value>5806290.933</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_zmax">
        <gen:value>52.38067000003718</gen:value>
      </gen:stringAttribute>
      <gen:doubleAttribute name="ZMIN">
        <gen:value>51.840000</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="THICKNESS">
        <gen:value>0.100000</gen:value>
      </gen:doubleAttribute>
      <gen:intAttribute name="TEXTURE">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="BUILDINGID">
        <gen:value>DENIAL430001kqrp</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ARTBEZEICH">
        <gen:value>Gebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Join_Cou_1">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="FUNKTION_T">
        <gen:value>Wohngebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADT">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="OVERHANG">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="OBJFLAECHE">
        <gen:value>359.63</gen:value>
      </gen:stringAttribute>
      <bldg:measuredHeight uom="#m">17.783</bldg:measuredHeight>
      <bldg:lod2Solid>
        <gml:Solid gml:id="46419a28-edad-4ca5-a2d8-b9a8109c30ad">
          <gml:exterior>
            <gml:CompositeSurface gml:id="bb34e492-c6c1-461e-9a3c-fcdb80f7e5a6">
              <gml:surfaceMember xlink:href="#c8a5c45b-7bc2-4a8f-b74e-8462d6dc16f1"/>
              <gml:surfaceMember xlink:href="#5b0a5946-04c1-4dac-b38e-4ab7a050fe02"/>
              <gml:surfaceMember xlink:href="#8f4a20e3-9faf-4dfe-9b9a-11bf7c438ef2"/>
              <gml:surfaceMember xlink:href="#c144706d-481d-4e25-b315-58311b7ac5e9"/>
              <gml:surfaceMember xlink:href="#d5a641d4-d7e9-40e2-b949-51db8f98de79"/>
              <gml:surfaceMember xlink:href="#9bf9c17a-cc3c-442d-912e-5c3968fbf919"/>
              <gml:surfaceMember xlink:href="#2f87f2c5-9243-4b5f-9d32-0fd0e116cb4b"/>
              <gml:surfaceMember xlink:href="#3855da8b-4ba9-4e83-9db4-bb48386bf660"/>
              <gml:surfaceMember xlink:href="#d793daf5-023c-451c-a77a-211aa135e6f5"/>
              <gml:surfaceMember xlink:href="#e25d1fa8-060a-4af6-b5f2-4a86996997a5"/>
              <gml:surfaceMember xlink:href="#32947210-797a-4475-a575-a766b00f5a85"/>
              <gml:surfaceMember xlink:href="#806badee-6ecf-43ad-a2ae-69017ab7b1f7"/>
              <gml:surfaceMember xlink:href="#b710a256-5425-4940-bdbc-16f845403089"/>
              <gml:surfaceMember xlink:href="#966d850e-6d54-4391-8543-2bcd37dd6629"/>
              <gml:surfaceMember xlink:href="#8420d558-521b-431e-acee-511f65fd519f"/>
              <gml:surfaceMember xlink:href="#1ac3e042-4b43-46bc-aba9-2e82bf2e3738"/>
              <gml:surfaceMember xlink:href="#b70a13c0-2357-4175-ac04-c1cc6307b4c5"/>
              <gml:surfaceMember xlink:href="#6b782620-bb3a-4552-9ae7-af209a53c579"/>
              <gml:surfaceMember xlink:href="#843f8d79-dcc0-447b-8179-d50d92f3553e"/>
              <gml:surfaceMember xlink:href="#5f518a1a-5499-4b3c-9754-c7f418e1e94d"/>
              <gml:surfaceMember xlink:href="#81c5faf6-f4b7-4d2b-9aec-bcf43b5d1861"/>
              <gml:surfaceMember xlink:href="#ca9905bf-0bf3-465f-a358-b37af30eaf83"/>
              <gml:surfaceMember xlink:href="#5f28feaa-ac64-4ca3-adf9-dd9858047be7"/>
              <gml:surfaceMember xlink:href="#337fe559-1ec5-41df-8952-01e79ea18a2e"/>
              <gml:surfaceMember xlink:href="#17080a44-4774-4b8f-8826-5fbd95bf0dec"/>
              <gml:surfaceMember xlink:href="#8b6d1d0b-6ad6-49a2-9477-e782df6438b1"/>
              <gml:surfaceMember xlink:href="#b3e3b66a-388e-4cf2-8e67-dc66e6045a44"/>
              <gml:surfaceMember xlink:href="#855a5935-6fa0-481d-9ee1-65278ee694b4"/>
              <gml:surfaceMember xlink:href="#6a788127-a447-4e78-9693-8b7e380317b9"/>
              <gml:surfaceMember xlink:href="#a6e302f2-0f3a-463b-bbef-6301dabce96a"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="cd3322e9-7fc1-4402-b337-15e8f99bcd8a">
          <gen:stringAttribute name="Type">
            <gen:value>Ground</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>359.63</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="e42d5504-f73f-47a9-925d-f63049af44da" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="c8a5c45b-7bc2-4a8f-b74e-8462d6dc16f1">
                  <gml:exterior>
                    <gml:LinearRing gml:id="e392a12b-9016-40d7-a560-89f0302c3f33">
                      <gml:posList>552635.149000000210000000 5806306.640000000600000000 51.840000000000011000 552635.406000000420000000 5806304.770999999700000000 51.840000000000011000 552634.794683739540000000 5806304.686866828200000000 51.840000000000003000 552634.614000000060000000 5806304.662000000500000000 51.840000000000011000 552636.307000000030000000 5806292.405999999500000000 51.840000000000011000 552630.991412530300000000 5806291.670318299000000000 51.840000000000003000 552625.663999999870000000 5806290.933000000200000000 51.840000000000011000 552625.622999999670000000 5806291.230000000400000000 51.840000000000003000 552624.243077282210000000 5806301.177066044000000000 51.839999999999989000 552624.222000000070000000 5806301.328999999900000000 51.840000000000011000 552624.031764661780000000 5806301.302591762500000000 51.840000000000011000 552622.241000000390000000 5806301.053999999500000000 51.839999999996103000 552620.902999999930000000 5806310.697000000600000000 51.840000000000011000 552622.687994687820000000 5806310.944915927900000000 51.840000000000003000 552622.883000000380000000 5806310.971999999100000000 51.840000000000011000 552622.872539071020000000 5806311.047406798200000000 51.840000000000003000 552621.481999999840000000 5806321.071000000500000000 51.840000000000003000 552621.440999999640000000 5806321.368000000700000000 51.840000000000011000 552626.749734129870000000 5806322.106805895500000000 51.840000000000003000 552632.089999999850000000 5806322.849999999600000000 51.840000000000011000 552633.792999999600000000 5806310.615000000200000000 51.840000000000011000 552633.965221886520000000 5806310.638733758600000000 51.840000000000003000 552635.774000000210000000 5806310.888000000300000000 51.840000000000011000 552636.337000000290000000 5806306.803999999500000000 51.840000000000011000 552635.149000000210000000 5806306.640000000600000000 51.840000000000011000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="2475ab56-8838-426e-89af-31946f3190a1">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>8.63</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>352.15</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="9bcf2a82-0138-4e97-92bf-bcd9e361fe15" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="5b0a5946-04c1-4dac-b38e-4ab7a050fe02">
                  <gml:exterior>
                    <gml:LinearRing gml:id="0e6a5640-5029-4441-a7a1-9ff5fc626030">
                      <gml:posList>552635.774000000210000000 5806310.888000000300000000 51.840000000000011000 552633.965221886520000000 5806310.638733758600000000 51.840000000000003000 552633.965221886520000000 5806310.638733758600000000 56.564559936523438000 552635.774000000210000000 5806310.888000000300000000 56.564559936523438000 552635.774000000210000000 5806310.888000000300000000 51.840000000000011000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="2a7ccb32-6bee-4e27-84a4-3d1fc079ea30">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>19.48</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>82.15</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="4197c971-389c-44ca-b530-b5f63b8a3307" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="8f4a20e3-9faf-4dfe-9b9a-11bf7c438ef2">
                  <gml:exterior>
                    <gml:LinearRing gml:id="77a8f30d-f1d8-4355-8ba1-4be97b95a663">
                      <gml:posList>552636.337000000290000000 5806306.803999999500000000 51.840000000000011000 552635.774000000210000000 5806310.888000000300000000 51.840000000000011000 552635.774000000210000000 5806310.888000000300000000 56.564559936523438000 552636.337000000290000000 5806306.803999999500000000 56.564559936523438000 552636.337000000290000000 5806306.803999999500000000 51.840000000000011000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="aab98f16-ade3-4a56-8e88-ab34576fe9d0">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>5.67</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>172.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="5d5c5cb5-8dce-4d13-9b64-07cf6d198430" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="c144706d-481d-4e25-b315-58311b7ac5e9">
                  <gml:exterior>
                    <gml:LinearRing gml:id="feb71676-3da8-4ebe-b46f-282dd44f791c">
                      <gml:posList>552636.337000000290000000 5806306.803999999500000000 56.564559936523438000 552635.149000000210000000 5806306.640000000600000000 56.564559936523438000 552635.149000000210000000 5806306.640000000600000000 51.840000000000011000 552636.337000000290000000 5806306.803999999500000000 51.840000000000011000 552636.337000000290000000 5806306.803999999500000000 56.564559936523438000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="c32f9efd-97da-43b9-8906-e379099bf06a">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>8.91</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>82.17</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="d184b38e-87f5-4cdc-81b5-32d9299e74b1" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="d5a641d4-d7e9-40e2-b949-51db8f98de79">
                  <gml:exterior>
                    <gml:LinearRing gml:id="e69c5111-e916-4966-b4e4-9054e0ce0ac8">
                      <gml:posList>552635.406000000420000000 5806304.770999999700000000 51.840000000000011000 552635.149000000210000000 5806306.640000000600000000 51.840000000000011000 552635.149000000210000000 5806306.640000000600000000 56.564559936523438000 552635.406000000420000000 5806304.770999999700000000 56.564559936523438000 552635.406000000420000000 5806304.770999999700000000 51.840000000000011000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="72d25e61-9fa6-472e-883e-00d279cdfc89">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>2.92</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>172.16</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="915e78c1-9109-457c-bb1b-b41d12e6b43b" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="9bf9c17a-cc3c-442d-912e-5c3968fbf919">
                  <gml:exterior>
                    <gml:LinearRing gml:id="b88388e7-730f-4db1-b5a7-366f297707bb">
                      <gml:posList>552635.406000000420000000 5806304.770999999700000000 56.564559936523438000 552634.794683739540000000 5806304.686866828200000000 56.564559936523438000 552634.794683739540000000 5806304.686866828200000000 51.840000000000003000 552635.406000000420000000 5806304.770999999700000000 51.840000000000011000 552635.406000000420000000 5806304.770999999700000000 56.564559936523438000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="ea584639-b96f-4823-8f12-0ecd8f01c6e7">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>1.47</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>352.09</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="586377f6-9b7c-4753-89db-4ae5aeb60245" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="2f87f2c5-9243-4b5f-9d32-0fd0e116cb4b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="60bd518b-be58-425f-8b3b-b7774784550f">
                      <gml:posList>552622.883000000380000000 5806310.971999999100000000 51.840000000000011000 552622.687994687820000000 5806310.944915927900000000 51.840000000000003000 552622.687994687820000000 5806310.944915927900000000 59.219917297363281000 552622.883000000380000000 5806310.971999999100000000 59.386285653270100000 552622.883000000380000000 5806310.971999999100000000 51.840000000000011000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bee76692-a1ef-446d-a835-2db360b66bb4">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>76.97</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>262.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="8bef7966-45a3-4fed-be71-f16a77ad93c3" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="3855da8b-4ba9-4e83-9db4-bb48386bf660">
                  <gml:exterior>
                    <gml:LinearRing gml:id="2a3a82fd-ed62-4076-bb00-ed8c339e504a">
                      <gml:posList>552622.883000000380000000 5806310.971999999100000000 59.386285653270100000 552622.872539071020000000 5806311.047406798200000000 59.386325730339017000 552621.481999999840000000 5806321.071000000500000000 59.391653051510552000 552621.481999999840000000 5806321.071000000500000000 51.840000000000003000 552622.872539071020000000 5806311.047406798200000000 51.840000000000003000 552622.883000000380000000 5806310.971999999100000000 51.840000000000011000 552622.883000000380000000 5806310.971999999100000000 59.386285653270100000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="6b2be3da-2af4-4509-b7c2-c65e3d4e963e">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>2.26</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>262.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="d59e4e84-d08a-4f2a-9ab8-7da1904ad080" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="d793daf5-023c-451c-a77a-211aa135e6f5">
                  <gml:exterior>
                    <gml:LinearRing gml:id="596d6395-d581-4eb5-afb0-9528378ac6b6">
                      <gml:posList>552621.481999999840000000 5806321.071000000500000000 59.391653051510552000 552621.440999999640000000 5806321.368000000700000000 59.391979798321941000 552621.440999999640000000 5806321.368000000700000000 51.840000000000011000 552621.481999999840000000 5806321.071000000500000000 51.840000000000003000 552621.481999999840000000 5806321.071000000500000000 59.391653051510552000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="0eacf5ff-b5f0-47a3-a573-4a355c6bdfe0">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>105.47</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>352.08</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="0af5956f-d7cc-4e6b-a47b-593fdb3efe8a" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="e25d1fa8-060a-4af6-b5f2-4a86996997a5">
                  <gml:exterior>
                    <gml:LinearRing gml:id="1a015d71-9912-429f-9558-e169ae1a47dc">
                      <gml:posList>552632.089999999850000000 5806322.849999999600000000 51.840000000000011000 552626.749734129870000000 5806322.106805895500000000 51.840000000000003000 552621.440999999640000000 5806321.368000000700000000 51.840000000000011000 552621.440999999640000000 5806321.368000000700000000 59.391979798321941000 552626.749734129870000000 5806322.106805895500000000 63.921287536621094000 552632.089999999850000000 5806322.849999999600000000 59.365077537042453000 552632.089999999850000000 5806322.849999999600000000 51.840000000000011000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="b4a2e3d8-168a-476e-bad3-9673fd336b98">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>92.97</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>82.08</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="cd0cd256-6ba5-4e07-92f5-10c3e21e4a43" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="32947210-797a-4475-a575-a766b00f5a85">
                  <gml:exterior>
                    <gml:LinearRing gml:id="7f8a323f-5255-40f7-b3ff-55fb16647b79">
                      <gml:posList>552633.792999999600000000 5806310.615000000200000000 56.564559936523438000 552633.792999999600000000 5806310.615000000200000000 51.840000000000011000 552632.089999999850000000 5806322.849999999600000000 51.840000000000011000 552632.089999999850000000 5806322.849999999600000000 59.365077537042453000 552633.792999999600000000 5806310.615000000200000000 59.366826334904651000 552633.792999999600000000 5806310.615000000200000000 56.564559936523438000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="24cda2cd-545c-4bf1-8452-3e880378af86">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>1.30</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>352.15</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="27b20222-0430-40e8-9f72-3daf2c78ec53" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="806badee-6ecf-43ad-a2ae-69017ab7b1f7">
                  <gml:exterior>
                    <gml:LinearRing gml:id="d09c6fdf-cb44-4314-8f8b-58c94f807d6f">
                      <gml:posList>552633.965221886520000000 5806310.638733758600000000 56.564559936523438000 552633.965221886520000000 5806310.638733758600000000 51.840000000000003000 552633.792999999600000000 5806310.615000000200000000 51.840000000000011000 552633.792999999600000000 5806310.615000000200000000 56.564559936523438000 552633.792999999600000000 5806310.615000000200000000 59.366826334904651000 552633.965221886520000000 5806310.638733758600000000 59.219917297363281000 552633.965221886520000000 5806310.638733758600000000 56.564559936523438000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="9aec0f9b-4ae5-4a65-8484-51ef6f827315">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>15.96</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>82.07</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="dee44519-825c-434d-a702-c1d8699dd853" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="b710a256-5425-4940-bdbc-16f845403089">
                  <gml:exterior>
                    <gml:LinearRing gml:id="9ca4222d-43d4-40be-9129-03bde56c13f0">
                      <gml:posList>552634.794683739540000000 5806304.686866828200000000 56.564559936523438000 552633.965221886520000000 5806310.638733758600000000 56.564559936523438000 552633.965221886520000000 5806310.638733758600000000 59.219917297363281000 552634.794683739540000000 5806304.686866828200000000 59.219917297363281000 552634.794683739540000000 5806304.686866828200000000 56.564559936523438000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="3b2039bb-0fca-42bb-b649-aebf78815b32">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>1.36</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>172.16</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="e9838ddc-e804-48a7-93c9-2b81280e0ece" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="966d850e-6d54-4391-8543-2bcd37dd6629">
                  <gml:exterior>
                    <gml:LinearRing gml:id="b128cc37-875b-4367-9fb9-3e368f8d3569">
                      <gml:posList>552634.794683739540000000 5806304.686866828200000000 59.219917297363281000 552634.614000000060000000 5806304.662000000500000000 59.374040625160738000 552634.614000000060000000 5806304.662000000500000000 56.564559936523438000 552634.614000000060000000 5806304.662000000500000000 51.840000000000011000 552634.794683739540000000 5806304.686866828200000000 51.840000000000003000 552634.794683739540000000 5806304.686866828200000000 56.564559936523438000 552634.794683739540000000 5806304.686866828200000000 59.219917297363281000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="02d59012-cb64-47f1-9bf6-8f8d87efecbe">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>93.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>82.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="bcfacc3d-d913-447a-b147-66e1dad36e38" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="8420d558-521b-431e-acee-511f65fd519f">
                  <gml:exterior>
                    <gml:LinearRing gml:id="38fdab7c-840d-4202-bdf2-80c6ab234100">
                      <gml:posList>552636.307000000030000000 5806292.405999999500000000 59.386608310525688000 552636.307000000030000000 5806292.405999999500000000 51.840000000000011000 552634.614000000060000000 5806304.662000000500000000 51.840000000000011000 552634.614000000060000000 5806304.662000000500000000 56.564559936523438000 552634.614000000060000000 5806304.662000000500000000 59.374040625160738000 552636.307000000030000000 5806292.405999999500000000 59.386608310525688000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="cc52ccb6-6608-4e0a-a27c-3eac6e5e68a1">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>105.42</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>172.12</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="a2689230-2c98-4330-9892-397d70cf55b1" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="1ac3e042-4b43-46bc-aba9-2e82bf2e3738">
                  <gml:exterior>
                    <gml:LinearRing gml:id="91628611-9466-4fe1-b183-e2c4c01223e1">
                      <gml:posList>552636.307000000030000000 5806292.405999999500000000 59.386608310525688000 552630.991412530300000000 5806291.670318299000000000 63.921287536621094000 552625.663999999870000000 5806290.933000000200000000 59.376520458962517000 552625.663999999870000000 5806290.933000000200000000 51.840000000000011000 552630.991412530300000000 5806291.670318299000000000 51.840000000000003000 552636.307000000030000000 5806292.405999999500000000 51.840000000000011000 552636.307000000030000000 5806292.405999999500000000 59.386608310525688000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="45ee3082-e3ee-4977-8c5e-fd599e300482">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>2.26</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>262.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="959cb5ef-985f-41b1-a764-5ba806adf63b" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="b70a13c0-2357-4175-ac04-c1cc6307b4c5">
                  <gml:exterior>
                    <gml:LinearRing gml:id="02417016-af3c-44a4-a639-4e4e116894bd">
                      <gml:posList>552625.663999999870000000 5806290.933000000200000000 59.376520458962517000 552625.622999999670000000 5806291.230000000400000000 59.376847205773899000 552625.622999999670000000 5806291.230000000400000000 51.840000000000003000 552625.663999999870000000 5806290.933000000200000000 51.840000000000011000 552625.663999999870000000 5806290.933000000200000000 59.376520458962517000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="dd141836-9a4a-4186-9e09-2e0e618a00f7">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>76.87</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>262.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="3422e2b0-ef87-4c21-a63e-f0ef9894c704" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="6b782620-bb3a-4552-9ae7-af209a53c579">
                  <gml:exterior>
                    <gml:LinearRing gml:id="e921f892-ae9f-42fd-b521-5f36ca20b297">
                      <gml:posList>552625.622999999670000000 5806291.230000000400000000 59.376847205773899000 552624.243077282210000000 5806301.177066044000000000 59.382133854984062000 552624.222000000070000000 5806301.328999999900000000 59.382214604576568000 552624.222000000070000000 5806301.328999999900000000 51.840000000000011000 552624.243077282210000000 5806301.177066044000000000 51.839999999999989000 552625.622999999670000000 5806291.230000000400000000 51.840000000000003000 552625.622999999670000000 5806291.230000000400000000 59.376847205773899000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="d4b7bac8-663f-412a-bb2d-d8c03e6c3ce9">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>1.43</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>172.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="68058747-cb46-4ba3-a7d1-654e5e9b35a0" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="843f8d79-dcc0-447b-8179-d50d92f3553e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="3b5caed1-ec44-409f-9df5-18d39b3535ef">
                      <gml:posList>552624.222000000070000000 5806301.328999999900000000 59.382214604576568000 552624.031764661780000000 5806301.302591762500000000 59.219917297363281000 552624.031764661780000000 5806301.302591762500000000 51.840000000000011000 552624.222000000070000000 5806301.328999999900000000 51.840000000000011000 552624.222000000070000000 5806301.328999999900000000 59.382214604576568000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="6df28264-e28a-4bca-83e5-fb2f24094b72">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>5.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>171.64</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="e37cb06d-0b17-419e-a098-97dd32b5a599" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="5f518a1a-5499-4b3c-9754-c7f418e1e94d">
                  <gml:exterior>
                    <gml:LinearRing gml:id="f1c5ab26-5f43-4524-927c-fb191ebe7d7b">
                      <gml:posList>552628.012347705430000000 5806301.730751342100000000 62.601398299056093000 552624.243077282210000000 5806301.177066044000000000 62.135161374497045000 552624.243077282210000000 5806301.177066044000000000 59.382133854984062000 552628.012347705430000000 5806301.730751342100000000 62.601398299056093000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="9f0e0303-05b7-4194-9614-62367af92c09">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.42</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>262.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="eb752d6f-be5a-4076-8214-dc468863cb66" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="81c5faf6-f4b7-4d2b-9aec-bcf43b5d1861">
                  <gml:exterior>
                    <gml:LinearRing gml:id="e20753d4-d274-429d-a248-dccfe8d7eb34">
                      <gml:posList>552624.243077282210000000 5806301.177066044000000000 62.135161374497045000 552624.222000000070000000 5806301.328999999900000000 62.135161869818972000 552624.222000000070000000 5806301.328999999900000000 59.382214604576568000 552624.243077282210000000 5806301.177066044000000000 59.382133854984062000 552624.243077282210000000 5806301.177066044000000000 62.135161374497045000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="b453a550-2287-41f2-af11-113f1f2b271c">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>18.91</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>172.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="ffebf47b-bd01-41ab-910f-33c2e55540a8" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="ca9905bf-0bf3-465f-a358-b37af30eaf83">
                  <gml:exterior>
                    <gml:LinearRing gml:id="d2c3ce39-b7c1-4eca-86e7-6fe52343107d">
                      <gml:posList>552624.222000000070000000 5806301.328999999900000000 59.382214604576568000 552624.222000000070000000 5806301.328999999900000000 62.135161869818972000 552622.241000000390000000 5806301.053999999500000000 61.890392732287211000 552622.241000000390000000 5806301.053999999500000000 51.839999999996103000 552624.031764661780000000 5806301.302591762500000000 51.840000000000011000 552624.031764661780000000 5806301.302591762500000000 59.219917297363281000 552624.222000000070000000 5806301.328999999900000000 59.382214604576568000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="29a97577-71a3-48c4-8973-20b9dd8f6305">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>18.87</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>352.09</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="89d40da6-dd35-4007-af19-9f8acdea1192" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="5f28feaa-ac64-4ca3-adf9-dd9858047be7">
                  <gml:exterior>
                    <gml:LinearRing gml:id="de730998-f4dd-45cd-a2f0-a37201d87789">
                      <gml:posList>552622.687994687820000000 5806310.944915927900000000 59.219917297363281000 552622.687994687820000000 5806310.944915927900000000 51.840000000000003000 552620.902999999930000000 5806310.697000000600000000 51.840000000000011000 552620.902999999930000000 5806310.697000000600000000 61.890392732295012000 552622.883000000380000000 5806310.971999999100000000 62.135040646478863000 552622.883000000380000000 5806310.971999999100000000 59.386285653270100000 552622.687994687820000000 5806310.944915927900000000 59.219917297363281000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="81cd528d-6569-4b21-977b-9670e475b79d">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.21</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>262.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="69e72bda-43e9-49ef-bc62-8eda42aa359f" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="337fe559-1ec5-41df-8952-01e79ea18a2e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="76583758-9a71-4149-9aba-d2a96ffc668d">
                      <gml:posList>552622.883000000380000000 5806310.971999999100000000 62.135040646478863000 552622.872539071020000000 5806311.047406798200000000 62.135040892312986000 552622.872539071020000000 5806311.047406798200000000 59.386325730339017000 552622.883000000380000000 5806310.971999999100000000 59.386285653270100000 552622.883000000380000000 5806310.971999999100000000 62.135040646478863000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="99999d3f-6763-422e-a202-7d86bec477f3">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>5.23</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>351.64</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="160cc94d-cdb3-4f52-81d6-506c46364209" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="17080a44-4774-4b8f-8826-5fbd95bf0dec">
                  <gml:exterior>
                    <gml:LinearRing gml:id="2323e06f-a6fd-4717-93a8-c8447ae3d958">
                      <gml:posList>552626.635905288160000000 5806311.600224801300000000 62.600547500773693000 552622.872539071020000000 5806311.047406798200000000 59.386325730339017000 552622.872539071020000000 5806311.047406798200000000 62.135040892312986000 552626.635905288160000000 5806311.600224801300000000 62.600547500773693000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="d70be35e-e262-4349-b12f-24802695ffa9">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>97.84</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>262.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="2daa8c31-c6c7-4bab-adda-382d1b31bffe" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="8b6d1d0b-6ad6-49a2-9477-e782df6438b1">
                  <gml:exterior>
                    <gml:LinearRing gml:id="7c682641-2d7f-4a64-9641-de927cc37b9f">
                      <gml:posList>552622.241000000390000000 5806301.053999999500000000 61.890392732287211000 552620.902999999930000000 5806310.697000000600000000 61.890392732295012000 552620.902999999930000000 5806310.697000000600000000 51.840000000000011000 552622.241000000390000000 5806301.053999999500000000 51.839999999996103000 552622.241000000390000000 5806301.053999999500000000 61.890392732287211000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="3c44722b-131e-4013-8a6f-c4ac77239ea0">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001kqrp_C:28976</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>217.99</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>82.07</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>40.20</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="25c728fd-67ce-470a-b4ab-43dac7487fb9" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="b3e3b66a-388e-4cf2-8e67-dc66e6045a44">
                  <gml:exterior>
                    <gml:LinearRing gml:id="25741357-45ac-48c7-b9aa-94da62a7e895">
                      <gml:posList>552633.965221886520000000 5806310.638733758600000000 59.219917297363281000 552633.792999999600000000 5806310.615000000200000000 59.366826334904651000 552632.089999999850000000 5806322.849999999600000000 59.365077537042453000 552626.749734129870000000 5806322.106805895500000000 63.921287536621094000 552630.991412530300000000 5806291.670318299000000000 63.921287536621094000 552636.307000000030000000 5806292.405999999500000000 59.386608310525688000 552634.614000000060000000 5806304.662000000500000000 59.374040625160738000 552634.794683739540000000 5806304.686866828200000000 59.219917297363281000 552633.965221886520000000 5806310.638733758600000000 59.219917297363281000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="b42e7cda-1ef0-463a-809c-c300cf37007a">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001kqrp_C:28976</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>166.34</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>262.07</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>40.20</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="b067623f-441c-4fb1-b3ec-84c93d40d113" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="855a5935-6fa0-481d-9ee1-65278ee694b4">
                  <gml:exterior>
                    <gml:LinearRing gml:id="dbda9c3e-ab95-4413-ae5f-bca584b21c59">
                      <gml:posList>552625.622999999670000000 5806291.230000000400000000 59.376847205773899000 552625.663999999870000000 5806290.933000000200000000 59.376520458962517000 552630.991412530300000000 5806291.670318299000000000 63.921287536621094000 552626.749734129870000000 5806322.106805895500000000 63.921287536621094000 552621.440999999640000000 5806321.368000000700000000 59.391979798321941000 552621.481999999840000000 5806321.071000000500000000 59.391653051510552000 552622.872539071020000000 5806311.047406798200000000 59.386325730339017000 552626.635905288160000000 5806311.600224801300000000 62.600547500773693000 552628.012347705430000000 5806301.730751342100000000 62.601398299056093000 552624.243077282210000000 5806301.177066044000000000 59.382133854984062000 552625.622999999670000000 5806291.230000000400000000 59.376847205773899000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="44826730-3e38-48bf-a561-2cfe3f8b7649">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001kqrp_C:28974</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>8.68</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="cb1c1de6-d7d7-4a77-be4e-858e3af3d383" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="6a788127-a447-4e78-9693-8b7e380317b9">
                  <gml:exterior>
                    <gml:LinearRing gml:id="76e08557-28db-4299-8793-18568852ee0c">
                      <gml:posList>552636.337000000290000000 5806306.803999999500000000 56.564559936523438000 552635.774000000210000000 5806310.888000000300000000 56.564559936523438000 552633.965221886520000000 5806310.638733758600000000 56.564559936523438000 552634.794683739540000000 5806304.686866828200000000 56.564559936523438000 552635.406000000420000000 5806304.770999999700000000 56.564559936523438000 552635.149000000210000000 5806306.640000000600000000 56.564559936523438000 552636.337000000290000000 5806306.803999999500000000 56.564559936523438000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="d7dc41f2-eacb-4093-a1f9-3033ebd58d61">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001kqrp_C:28975</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>57.83</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>262.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>6.98</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="baa7d599-8876-475c-ad3d-6b8ffab44ddc" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="a6e302f2-0f3a-463b-bbef-6301dabce96a">
                  <gml:exterior>
                    <gml:LinearRing gml:id="aa1114f1-9738-4f22-9775-aacf6e846991">
                      <gml:posList>552626.635905288160000000 5806311.600224801300000000 62.600547500773693000 552622.872539071020000000 5806311.047406798200000000 62.135040892312986000 552622.883000000380000000 5806310.971999999100000000 62.135040646478863000 552620.902999999930000000 5806310.697000000600000000 61.890392732295012000 552622.241000000390000000 5806301.053999999500000000 61.890392732287211000 552624.222000000070000000 5806301.328999999900000000 62.135161869818972000 552624.243077282210000000 5806301.177066044000000000 62.135161374497045000 552628.012347705430000000 5806301.730751342100000000 62.601398299056093000 552626.635905288160000000 5806311.600224801300000000 62.600547500773693000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </cityObjectMember>

</CityModel>
