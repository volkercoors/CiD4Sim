<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<CityModel xmlns="http://www.opengis.net/citygml/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:core="http://www.opengis.net/citygml/base/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:tex="http://www.opengis.net/citygml/textures/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/citygml/2.0 http://schemas.opengis.net/citygml/2.0/cityGMLBase.xsd http://www.opengis.net/citygml/appearance/2.0  http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd">

  <gml:description>Exported by RhinoCity(TM) software - (c)Rhinoterrain 2010-2018 : www.rhinoterrain.com</gml:description>

  <gml:name>RhinoCity(TM)</gml:name>

  <gml:boundedBy>
    <gml:Envelope srsDimension="3" srsName="crs:ETRS89_UTM32*DE_DHHN92_NH">
      <gml:lowerCorner>552710.187500 5807839.000000 50.929996</gml:lowerCorner>
      <gml:upperCorner>552720.312500 5807852.000000 59.400913</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>

  <cityObjectMember>
    <bldg:Building gml:id="DENIAL4300017Dxt">
      <gen:doubleAttribute name="Volume">
        <gen:value>514.371</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="TotalRoofArea">
        <gen:value>124.94</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="TotalWallArea">
        <gen:value>208.93</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Authoring_Software">
        <gen:value>RhinoCity</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="OSKA">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ALKISID">
        <gen:value>DENIAL4300017Dxt</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="KACHEL_ID">
        <gen:value>11_12_Building</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="COUNT">
        <gen:value>1645</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="DESC">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCK">
        <gen:value>31</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCKNR">
        <gen:value>212031</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STATBEZNR">
        <gen:value>212</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="MBZNR">
        <gen:value>2109</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="WBZNR">
        <gen:value>2109</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNAM">
        <gen:value>Sahlkamp</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNR">
        <gen:value>21</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNAM">
        <gen:value>Bothfeld-Vahrenheide</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNR">
        <gen:value>03</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEMEINDE">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SCALE">
        <gen:value>5000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_overlaps">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_xmax">
        <gen:value>552720.3039999995</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_xmin">
        <gen:value>552710.2319999998</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_ymax">
        <gen:value>5807851.529999999</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_ymin">
        <gen:value>5807839.169</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_zmax">
        <gen:value>51.09987600000203</gen:value>
      </gen:stringAttribute>
      <gen:doubleAttribute name="ZMIN">
        <gen:value>50.930000</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="THICKNESS">
        <gen:value>0.100000</gen:value>
      </gen:doubleAttribute>
      <gen:intAttribute name="TEXTURE">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="BUILDINGID">
        <gen:value>DENIAL4300017Dxt</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ARTBEZEICH">
        <gen:value>Gebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Join_Cou_1">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="FUNKTION_T">
        <gen:value>Wohngebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADT">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="OVERHANG">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="OBJFLAECHE">
        <gen:value>96.82</gen:value>
      </gen:stringAttribute>
      <bldg:measuredHeight uom="#m">14.555</bldg:measuredHeight>
      <bldg:lod2Solid>
        <gml:Solid gml:id="df4ec0ad-541f-4c07-88ab-940a4485a424">
          <gml:exterior>
            <gml:CompositeSurface gml:id="5cd64910-ea4e-46ca-8d02-667b991e4b31">
              <gml:surfaceMember xlink:href="#74537e92-bf59-49f1-9825-fa0abe30627c"/>
              <gml:surfaceMember xlink:href="#75920dcd-05fa-4ccd-8ef8-a4269522765e"/>
              <gml:surfaceMember xlink:href="#1b9ec4a5-cfe7-4e42-9525-6f479b3bac98"/>
              <gml:surfaceMember xlink:href="#b2ec9e0d-64e0-4860-9130-e6129ca5a79e"/>
              <gml:surfaceMember xlink:href="#e1b322f5-6f74-48d1-af8f-e1099c2a7f0b"/>
              <gml:surfaceMember xlink:href="#d334cfd6-1982-44b1-8ab8-c9f4c021e426"/>
              <gml:surfaceMember xlink:href="#97541e5e-d9df-4170-9110-4eec5e960a6b"/>
              <gml:surfaceMember xlink:href="#87223b25-d0f7-40af-ac7b-5b20993b5e35"/>
              <gml:surfaceMember xlink:href="#d3f3d1bb-d760-4a5a-ad89-a071d2eded24"/>
              <gml:surfaceMember xlink:href="#5cd0b84d-2c35-4b4a-ada9-a4266999add8"/>
              <gml:surfaceMember xlink:href="#5873c32a-50c3-4020-b9c3-5475fbf3e31c"/>
              <gml:surfaceMember xlink:href="#c9367a5b-d6dd-4f40-8e74-b6da9678098f"/>
              <gml:surfaceMember xlink:href="#bf659b00-dcd5-4efd-a226-763e49d2e49d"/>
              <gml:surfaceMember xlink:href="#8e616506-5bfa-4410-bc25-9617dd1dcc9b"/>
              <gml:surfaceMember xlink:href="#b6832793-2f1c-45f8-a833-78e7d924b9b1"/>
              <gml:surfaceMember xlink:href="#1088b0ad-b0b4-41d4-9cdb-c5fd3b975c90"/>
              <gml:surfaceMember xlink:href="#6f154a8b-8c56-412a-8d8e-508978a2adbe"/>
              <gml:surfaceMember xlink:href="#c08ee561-4755-41ae-a9f3-e3a22c10e2b4"/>
              <gml:surfaceMember xlink:href="#4e9a04e0-b0dc-41c4-be4e-613a5f5bd933"/>
              <gml:surfaceMember xlink:href="#c2f5b821-de98-4b5b-8f1c-f73c9d70a890"/>
              <gml:surfaceMember xlink:href="#87e2a15a-4691-4bbe-8ce4-48751ca9c389"/>
              <gml:surfaceMember xlink:href="#6e59735d-93d0-4a71-82fc-6247a887dc49"/>
              <gml:surfaceMember xlink:href="#88c9439f-faf9-4604-aab6-5dbfcd9ad40f"/>
              <gml:surfaceMember xlink:href="#eed23c8b-203f-489b-bcaa-d9783276e859"/>
              <gml:surfaceMember xlink:href="#91cc5a05-4653-40af-9327-0673abcad43a"/>
              <gml:surfaceMember xlink:href="#5070c4c0-86a6-4e38-a036-52275bb15e28"/>
              <gml:surfaceMember xlink:href="#bf409da9-4a04-473d-ad05-513cda13eee3"/>
              <gml:surfaceMember xlink:href="#ca879f72-e0b5-4b22-82cc-ae34ffde9c41"/>
              <gml:surfaceMember xlink:href="#d0dc7f6c-f1a0-4277-b350-76148d0506a9"/>
              <gml:surfaceMember xlink:href="#cdd41205-b307-410d-bf12-d5d3606bb738"/>
              <gml:surfaceMember xlink:href="#72dc44a1-8446-4288-8937-ed5e62295e33"/>
              <gml:surfaceMember xlink:href="#01b6ff25-096c-45a3-a866-83fd6df8aaf0"/>
              <gml:surfaceMember xlink:href="#25eedd33-b326-4d56-8bd5-e328488b7a16"/>
              <gml:surfaceMember xlink:href="#ec200bce-839b-488f-ad1c-57bf3f649c01"/>
              <gml:surfaceMember xlink:href="#4a2f324b-6752-4a90-bd97-676dc3ccfd47"/>
              <gml:surfaceMember xlink:href="#a340f16e-4219-44e2-8f1e-95a3d4788edd"/>
              <gml:surfaceMember xlink:href="#3ab50c9d-cb78-42ec-9132-5458d44cd36a"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="11faa55f-a925-451b-8cc8-41640cd0d9e4">
          <gen:stringAttribute name="Type">
            <gen:value>Ground</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>96.82</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="65414c72-c368-41cd-ad8d-94a35c35728c" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="74537e92-bf59-49f1-9825-fa0abe30627c">
                  <gml:exterior>
                    <gml:LinearRing gml:id="5083c5ee-505c-4dbc-9006-63fd20a919c8">
                      <gml:posList>552717.468326951140000000 5807847.645471150100000000 50.929999999999993000 552718.948148671650000000 5807847.686872590300000000 50.929999999999993000 552720.025000000370000000 5807847.717000000200000000 50.929999999999993000 552720.163534285380000000 5807843.614597613000000000 50.930000000000000000 552720.303999999540000000 5807839.455000000100000000 50.929999999999993000 552718.668663174610000000 5807839.400779462400000000 50.929999999999993000 552713.477291024870000000 5807839.228656530400000000 50.929999999999993000 552711.678000000310000000 5807839.168999999800000000 50.929999999999993000 552711.566434851030000000 5807843.340739675800000000 50.929999999999971000 552711.509999999780000000 5807845.450999999400000000 50.930000000000000000 552711.379205986040000000 5807845.446815449700000000 50.929999999999993000 552710.291000000200000000 5807845.412000000500000000 50.930000000000000000 552710.231999999840000000 5807847.129000000700000000 50.929999999999993000 552710.300999999980000000 5807847.130999999100000000 50.930000000000000000 552711.324521318780000000 5807847.163506684800000000 50.929999999999993000 552711.466000000010000000 5807847.167999999600000000 50.930000000000007000 552711.459132624440000000 5807847.358760441700000000 50.929999999999993000 552711.457000000400000000 5807847.417999999600000000 50.930000000000000000 552711.387000000100000000 5807847.414999999100000000 50.930000000000000000 552711.380613521090000000 5807847.607078193700000000 50.929999999999993000 552711.254999999890000000 5807851.384999999800000000 50.930000000000000000 552717.263000000270000000 5807851.529999999300000000 50.930000000000007000 552717.375333300210000000 5807847.798038133400000000 50.929999999999993000 552717.379999999890000000 5807847.642999999200000000 50.929999999999993000 552717.468326951140000000 5807847.645471150100000000 50.929999999999993000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="240e3195-d1f1-4be9-af58-04ebb3358e04">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>2.22</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>358.18</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="d920d549-fb92-4d56-ab2b-c241c46531f1" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="75920dcd-05fa-4ccd-8ef8-a4269522765e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="0015dad9-bdf0-4de2-a45a-0deaa4378af4">
                      <gml:posList>552711.324521318780000000 5807847.163506684800000000 50.929999999999993000 552710.300999999980000000 5807847.130999999100000000 50.930000000000000000 552710.300999999980000000 5807847.130999999100000000 53.095531463623047000 552711.324521318780000000 5807847.163506684800000000 53.095531463623047000 552711.324521318780000000 5807847.163506684800000000 50.929999999999993000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="9bdcc9c3-43d3-426e-b3e9-ea9da921f478">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>2.36</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>178.17</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="047154d5-672e-42fa-be34-3ca999dd0d1d" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="1b9ec4a5-cfe7-4e42-9525-6f479b3bac98">
                  <gml:exterior>
                    <gml:LinearRing gml:id="19a927ae-abbb-45ef-983f-f11506217832">
                      <gml:posList>552711.379205986040000000 5807845.446815449700000000 53.095531463623047000 552710.291000000200000000 5807845.412000000500000000 53.095531463623047000 552710.291000000200000000 5807845.412000000500000000 50.930000000000000000 552711.379205986040000000 5807845.446815449700000000 50.929999999999993000 552711.379205986040000000 5807845.446815449700000000 53.095531463623047000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="2a656ee0-0f45-4703-b4cd-1c4260b6b99e">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>3.72</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>268.03</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="a461ffd7-5ce8-46a0-b891-98012ca87140" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="b2ec9e0d-64e0-4860-9130-e6129ca5a79e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="59a3bccc-906b-4de8-8902-afa12b3e03be">
                      <gml:posList>552710.291000000200000000 5807845.412000000500000000 53.095531463623047000 552710.231999999840000000 5807847.129000000700000000 53.095531463623047000 552710.231999999840000000 5807847.129000000700000000 50.929999999999993000 552710.291000000200000000 5807845.412000000500000000 50.930000000000000000 552710.291000000200000000 5807845.412000000500000000 53.095531463623047000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="2ef9f459-cc7a-4af1-a81b-6f9424a99df1">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.15</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>358.34</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="cdb299a3-a1ef-4ab9-93df-3bd0a1e27d09" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="e1b322f5-6f74-48d1-af8f-e1099c2a7f0b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="537d5a1c-99e5-4075-a912-3df407f5a54e">
                      <gml:posList>552710.300999999980000000 5807847.130999999100000000 50.930000000000000000 552710.231999999840000000 5807847.129000000700000000 50.929999999999993000 552710.231999999840000000 5807847.129000000700000000 53.095531463623047000 552710.300999999980000000 5807847.130999999100000000 53.095531463623047000 552710.300999999980000000 5807847.130999999100000000 50.930000000000000000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="91e2e5cd-ab54-4ee4-a70e-17e25ddfb6aa">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>2.28</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>87.90</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="04834e8c-5bcb-4e1a-b1dc-348b18e76978" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="d334cfd6-1982-44b1-8ab8-c9f4c021e426">
                  <gml:exterior>
                    <gml:LinearRing gml:id="47feb5c1-25f8-4548-b9be-dbe484d5bff9">
                      <gml:posList>552718.668663174730000000 5807839.400779459600000000 55.931259155273438000 552718.668663174610000000 5807839.400779462400000000 54.443112053919734000 552718.556399772410000000 5807842.466706536700000000 58.095835562749258000 552718.668663174730000000 5807839.400779459600000000 55.931259155273438000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="46c8e6ca-9841-4eaa-a8d0-4cf3d5861283">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>7.75</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>178.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="ac611d34-975b-4d63-8d22-7d83349eb82d" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="97541e5e-d9df-4170-9110-4eec5e960a6b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="f030800c-38f5-4fa1-aacf-996cd1fe3a2b">
                      <gml:posList>552718.668663174730000000 5807839.400779459600000000 55.931259155273438000 552713.477291025100000000 5807839.228656524800000000 55.931259155273438000 552713.477291024990000000 5807839.228656527600000000 54.435075494323925000 552718.668663174610000000 5807839.400779462400000000 54.443112053919734000 552718.668663174730000000 5807839.400779459600000000 55.931259155273438000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="5e5dc864-d866-4e96-8696-d541c0b11661">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>2.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>267.90</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="65be1725-36c5-4abc-a00e-ab4dff814d53" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="87223b25-d0f7-40af-ac7b-5b20993b5e35">
                  <gml:exterior>
                    <gml:LinearRing gml:id="f49d7e01-40b6-456e-8fee-b80b9374f581">
                      <gml:posList>552713.477291025100000000 5807839.228656524800000000 55.931259155273438000 552713.364421357750000000 5807842.311140772000000000 58.107525097339106000 552713.477291024990000000 5807839.228656527600000000 54.435075494323925000 552713.477291025100000000 5807839.228656524800000000 55.931259155273438000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="17c785eb-e0a8-4135-a87f-d0e747fe48d0">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>4.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>358.18</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="d668a67f-7169-42e4-99ac-e5b5894bb842" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="d3f3d1bb-d760-4a5a-ad89-a071d2eded24">
                  <gml:exterior>
                    <gml:LinearRing gml:id="0e3d6607-cd1f-49be-ad81-c42c3ccbffa8">
                      <gml:posList>552717.375333300210000000 5807847.798038134400000000 53.436967226894623000 552712.390749571260000000 5807847.639255764900000000 53.436283487299249000 552712.390749571260000000 5807847.639255764900000000 54.316902160644531000 552717.375333300210000000 5807847.798038133400000000 54.316902160644531000 552717.375333300210000000 5807847.798038134400000000 53.436967226894623000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="365b1010-b884-4c9e-a9d7-6e7965b8b0c8">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.28</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>178.17</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="6c90ca33-c8bf-4699-b752-dc8fb792a3af" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="5cd0b84d-2c35-4b4a-ada9-a4266999add8">
                  <gml:exterior>
                    <gml:LinearRing gml:id="3c8736d5-9285-44a3-9d72-c7b94a2d9c2d">
                      <gml:posList>552711.509999999780000000 5807845.450999999400000000 53.095531463623047000 552711.379205986040000000 5807845.446815449700000000 53.095531463623047000 552711.379205986040000000 5807845.446815449700000000 50.929999999999993000 552711.509999999780000000 5807845.450999999400000000 50.930000000000000000 552711.509999999780000000 5807845.450999999400000000 53.095531463623047000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="0dee6034-9164-48d2-af72-9bcc92664544">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.55</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>358.18</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="8b1b4f21-2ebe-4b03-a50f-5651041bcb87" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="5873c32a-50c3-4020-b9c3-5475fbf3e31c">
                  <gml:exterior>
                    <gml:LinearRing gml:id="ade8f0cd-3785-4953-b194-a3f189ca97b5">
                      <gml:posList>552711.466000000010000000 5807847.167999999600000000 53.095531463623047000 552711.466000000010000000 5807847.167999999600000000 50.930000000000007000 552711.324521318780000000 5807847.163506684800000000 50.929999999999993000 552711.324521318780000000 5807847.163506684800000000 53.095531463623047000 552711.324521318780000000 5807847.163506684800000000 54.842628081720484000 552711.466000000010000000 5807847.167999999600000000 54.842644081501689000 552711.466000000010000000 5807847.167999999600000000 53.095531463623047000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="0e13ec50-4b45-47e1-b685-6290b9408e72">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.94</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>267.94</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="c3502e9b-f343-4194-b696-7b8bd20f032a" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="c9367a5b-d6dd-4f40-8e74-b6da9678098f">
                  <gml:exterior>
                    <gml:LinearRing gml:id="2ff1d395-7e55-43fa-b2d9-19200633f99c">
                      <gml:posList>552711.466000000010000000 5807847.167999999600000000 53.095531463623047000 552711.466000000010000000 5807847.167999999600000000 54.842644081501689000 552711.457000000400000000 5807847.417999999600000000 54.544801717815020000 552711.457000000400000000 5807847.417999999600000000 53.457751267503937000 552711.457000000400000000 5807847.417999999600000000 50.930000000000000000 552711.459132624440000000 5807847.358760441700000000 50.929999999999993000 552711.466000000010000000 5807847.167999999600000000 50.930000000000007000 552711.466000000010000000 5807847.167999999600000000 53.095531463623047000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="e06b20ff-faca-426b-934b-469afd456579">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.25</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>177.55</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="e459ba14-44a8-43ca-bd0c-93f59ff053c6" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bf659b00-dcd5-4efd-a226-763e49d2e49d">
                  <gml:exterior>
                    <gml:LinearRing gml:id="b54e5673-660c-4b77-b42b-b25bed62fb79">
                      <gml:posList>552711.457000000400000000 5807847.417999999600000000 54.544801717815020000 552711.387000000100000000 5807847.414999999100000000 54.545718226401448000 552711.387000000100000000 5807847.414999999100000000 53.457828514305845000 552711.387000000100000000 5807847.414999999100000000 50.930000000000000000 552711.457000000400000000 5807847.417999999600000000 50.930000000000000000 552711.457000000400000000 5807847.417999999600000000 53.457751267503937000 552711.457000000400000000 5807847.417999999600000000 54.544801717815020000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="4a499396-83ac-4d5c-9148-c9f3eb023a33">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.67</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>268.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="b3c131a0-6c1e-4ea4-b0cf-2543a793ea7d" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="8e616506-5bfa-4410-bc25-9617dd1dcc9b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="6b7b3db8-5896-451f-880a-dd497c7c77b0">
                      <gml:posList>552711.387000000100000000 5807847.414999999100000000 54.545718226401448000 552711.380613521090000000 5807847.607078193700000000 54.316902160644531000 552711.380613521090000000 5807847.607078193700000000 53.436144926176404000 552711.380613521090000000 5807847.607078193700000000 50.929999999999993000 552711.387000000100000000 5807847.414999999100000000 50.930000000000000000 552711.387000000100000000 5807847.414999999100000000 53.457828514305845000 552711.387000000100000000 5807847.414999999100000000 54.545718226401448000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="a4753797-897d-4be3-80c8-c6477512235f">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.89</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>358.18</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="98ddf285-daf0-411d-bc7f-3e271fc5398b" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="b6832793-2f1c-45f8-a833-78e7d924b9b1">
                  <gml:exterior>
                    <gml:LinearRing gml:id="74af3212-e0fc-48e9-a325-175d2c4bf844">
                      <gml:posList>552712.390749571260000000 5807847.639255764900000000 53.436283487299249000 552711.380613521090000000 5807847.607078193700000000 53.436144926176404000 552711.380613521090000000 5807847.607078193700000000 54.316902160644531000 552712.390749571260000000 5807847.639255764900000000 54.316902160644531000 552712.390749571260000000 5807847.639255764900000000 53.436283487299249000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="0cd08bfa-8dc5-40d0-a232-dff796827c0e">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.54</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>88.28</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="be2421c9-de0d-43bc-933c-1d67313a7090" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="1088b0ad-b0b4-41d4-9cdb-c5fd3b975c90">
                  <gml:exterior>
                    <gml:LinearRing gml:id="b2d6baf2-eb62-4dc7-b0ca-0accda19133f">
                      <gml:posList>552717.379999999890000000 5807847.642999999200000000 53.454467565013559000 552717.379999999890000000 5807847.642999999200000000 50.929999999999993000 552717.375333300210000000 5807847.798038133400000000 50.929999999999993000 552717.375333300210000000 5807847.798038134400000000 53.436967226894623000 552717.375333300210000000 5807847.798038133400000000 54.316902160644531000 552717.379999999890000000 5807847.642999999200000000 54.501575185703700000 552717.379999999890000000 5807847.642999999200000000 53.454467565013559000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="776d79ff-14dd-4133-a6da-03bde9ddebf4">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>9.47</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>358.40</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="98a88efb-3b6e-487e-86ad-88ca8b248a2d" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="6f154a8b-8c56-412a-8d8e-508978a2adbe">
                  <gml:exterior>
                    <gml:LinearRing gml:id="b594c446-bd48-4b66-9562-78a95b33ae17">
                      <gml:posList>552720.025000000370000000 5807847.717000000200000000 50.929999999999993000 552718.948148671650000000 5807847.686872590300000000 50.929999999999993000 552717.468326951140000000 5807847.645471150100000000 50.929999999999993000 552717.379999999890000000 5807847.642999999200000000 50.929999999999993000 552717.379999999890000000 5807847.642999999200000000 53.454467565013559000 552717.379999999890000000 5807847.642999999200000000 54.501575185703700000 552718.948148671650000000 5807847.686872590300000000 54.508810770517144000 552720.025000000370000000 5807847.717000000200000000 54.513779463384239000 552720.025000000370000000 5807847.717000000200000000 50.929999999999993000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="9685a2eb-916d-49e9-b843-88d48f6db652">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>24.74</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>88.07</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="5c40abc2-0613-4f28-b54d-049992f1f417" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="c08ee561-4755-41ae-a9f3-e3a22c10e2b4">
                  <gml:exterior>
                    <gml:LinearRing gml:id="4fbdfabb-82e9-45be-bfa9-85426b13cac7">
                      <gml:posList>552720.163534285380000000 5807843.614597613000000000 50.930000000000000000 552720.025000000370000000 5807847.717000000200000000 50.929999999999993000 552720.025000000370000000 5807847.717000000200000000 54.513779463384239000 552720.163534285380000000 5807843.614597613000000000 59.400909423828125000 552720.163534285380000000 5807843.614597613000000000 50.930000000000000000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="41f11ab3-d17c-4396-af8b-50eed9485ad5">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>24.94</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>88.07</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="7397e864-06a5-4c80-a210-ddb755af49be" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="4e9a04e0-b0dc-41c4-be4e-613a5f5bd933">
                  <gml:exterior>
                    <gml:LinearRing gml:id="a2ad9742-4889-45f4-b1d0-434d268fb300">
                      <gml:posList>552720.303999999540000000 5807839.455000000100000000 50.929999999999993000 552720.163534285380000000 5807843.614597613000000000 50.930000000000000000 552720.163534285380000000 5807843.614597613000000000 59.400909423828125000 552720.303999999540000000 5807839.455000000100000000 54.445643654713251000 552720.303999999540000000 5807839.455000000100000000 50.929999999999993000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="29a172a5-0ea7-4a23-944d-a1f65afd1ffe">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>30.28</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>178.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="24ace81b-2dc2-4876-83d8-61d6f14a374c" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="c2f5b821-de98-4b5b-8f1c-f73c9d70a890">
                  <gml:exterior>
                    <gml:LinearRing gml:id="dd36eee7-952c-4392-a154-7e2e3546bab5">
                      <gml:posList>552720.303999999540000000 5807839.455000000100000000 54.445643654713251000 552718.668663174610000000 5807839.400779462400000000 54.443112053919734000 552713.477291024990000000 5807839.228656527600000000 54.435075494323925000 552711.678000000310000000 5807839.168999999800000000 54.432290082465016000 552711.678000000310000000 5807839.168999999800000000 50.929999999999993000 552713.477291024870000000 5807839.228656530400000000 50.929999999999993000 552718.668663174610000000 5807839.400779462400000000 50.929999999999993000 552720.303999999540000000 5807839.455000000100000000 50.929999999999993000 552720.303999999540000000 5807839.455000000100000000 54.445643654713251000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="42aeddd4-fb5a-4117-925c-21aca5c8594c">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>40.21</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>268.47</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="220efdf3-2051-4e91-91aa-b015756ce20a" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="87e2a15a-4691-4bbe-8ce4-48751ca9c389">
                  <gml:exterior>
                    <gml:LinearRing gml:id="7800de9d-04f5-4a2c-bba8-c8a3e7ced92d">
                      <gml:posList>552711.678000000310000000 5807839.168999999800000000 54.432290082465016000 552711.566434851030000000 5807843.340739675800000000 59.400909423828125000 552711.509999999780000000 5807845.450999999400000000 56.887550231956283000 552711.509999999780000000 5807845.450999999400000000 53.095531463623047000 552711.509999999780000000 5807845.450999999400000000 50.930000000000000000 552711.566434851030000000 5807843.340739675800000000 50.929999999999971000 552711.678000000310000000 5807839.168999999800000000 50.929999999999993000 552711.678000000310000000 5807839.168999999800000000 54.432290082465016000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="e8bf2c9c-3610-4a67-a8a4-4e1a6c35269d">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.50</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>178.17</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="f8764ae6-019b-4ce2-a806-f3b2bafefcc1" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="6e59735d-93d0-4a71-82fc-6247a887dc49">
                  <gml:exterior>
                    <gml:LinearRing gml:id="e5d29844-f049-448f-ab43-2cf8d8ee6083">
                      <gml:posList>552711.509999999780000000 5807845.450999999400000000 56.887550231956283000 552711.379205986040000000 5807845.446815449700000000 56.887571827201810000 552711.379205986040000000 5807845.446815449700000000 53.095531463623047000 552711.509999999780000000 5807845.450999999400000000 53.095531463623047000 552711.509999999780000000 5807845.450999999400000000 56.887550231956283000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="2834660a-c3bf-490f-892b-1febd7d4b371">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>4.76</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>268.18</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="8687e7be-c314-4fd8-a1f6-e64ea10f585a" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="88c9439f-faf9-4604-aab6-5dbfcd9ad40f">
                  <gml:exterior>
                    <gml:LinearRing gml:id="59130570-4326-49cf-9bc1-96ef0660c443">
                      <gml:posList>552711.379205986040000000 5807845.446815449700000000 56.887571827201810000 552711.324521318780000000 5807847.163506684800000000 54.842628081720484000 552711.324521318780000000 5807847.163506684800000000 53.095531463623047000 552711.379205986040000000 5807845.446815449700000000 53.095531463623047000 552711.379205986040000000 5807845.446815449700000000 56.887571827201810000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="13d9a4e9-f7cd-424f-a3bf-fe9459b14f8c">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>8.67</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>268.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="efc2d7d5-d278-42f2-a127-39b2ed4ec882" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="eed23c8b-203f-489b-bcaa-d9783276e859">
                  <gml:exterior>
                    <gml:LinearRing gml:id="70b707b7-9adc-4aa9-879c-acd65b1baa4a">
                      <gml:posList>552711.380613521090000000 5807847.607078193700000000 53.436144926176404000 552711.254999999890000000 5807851.384999999800000000 53.009657678236714000 552711.254999999890000000 5807851.384999999800000000 50.930000000000000000 552711.380613521090000000 5807847.607078193700000000 50.929999999999993000 552711.380613521090000000 5807847.607078193700000000 53.436144926176404000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="d6d4face-b075-4592-b780-6d58105436b8">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>12.52</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>358.62</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="30b85854-d5d0-4171-9342-c0cad1737acc" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="91cc5a05-4653-40af-9327-0673abcad43a">
                  <gml:exterior>
                    <gml:LinearRing gml:id="df5fc6a2-596c-47c7-bb7c-7539888df4a2">
                      <gml:posList>552717.263000000270000000 5807851.529999999300000000 50.930000000000007000 552711.254999999890000000 5807851.384999999800000000 50.930000000000000000 552711.254999999890000000 5807851.384999999800000000 53.009657678236714000 552717.263000000270000000 5807851.529999999300000000 53.015712194975826000 552717.263000000270000000 5807851.529999999300000000 50.930000000000007000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="d1d0134c-e2e7-4259-a694-d9dcb9a8d30d">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>8.57</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>88.28</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="6dafc9f5-b325-4d88-9bba-9c84323d5b97" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="5070c4c0-86a6-4e38-a036-52275bb15e28">
                  <gml:exterior>
                    <gml:LinearRing gml:id="fa2083e7-8bec-482e-a72a-9dbf68fcedd8">
                      <gml:posList>552717.375333300210000000 5807847.798038133400000000 50.929999999999993000 552717.263000000270000000 5807851.529999999300000000 50.930000000000007000 552717.263000000270000000 5807851.529999999300000000 53.015712194975826000 552717.375333300210000000 5807847.798038134400000000 53.436967226894623000 552717.375333300210000000 5807847.798038133400000000 50.929999999999993000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="a277dbf3-69c7-4e47-be4e-343a2bf53069">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>2.26</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>268.18</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="cbda90ca-b112-469e-a4f9-8318833864f2" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bf409da9-4a04-473d-ad05-513cda13eee3">
                  <gml:exterior>
                    <gml:LinearRing gml:id="f95c309f-5d9e-42fb-8428-d6fd8eb9526e">
                      <gml:posList>552712.480028882270000000 5807844.836550385700000000 57.655519329351755000 552712.390749571260000000 5807847.639255764900000000 55.931259155273438000 552712.390749571260000000 5807847.639255764900000000 54.316902160644531000 552712.480028882270000000 5807844.836550385700000000 57.655519329351755000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="7c5afb9d-6169-46f5-902d-dd45f5a48c20">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>8.05</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>358.18</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="d441ae01-f213-413e-9dc6-4823e3bb6faa" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="ca879f72-e0b5-4b22-82cc-ae34ffde9c41">
                  <gml:exterior>
                    <gml:LinearRing gml:id="a4065f13-9f70-42e7-8f42-ee1abcb53718">
                      <gml:posList>552717.375333300210000000 5807847.798038133400000000 54.316902160644531000 552712.390749571260000000 5807847.639255764900000000 54.316902160644531000 552712.390749571260000000 5807847.639255764900000000 55.931259155273438000 552717.375333300210000000 5807847.798038134400000000 55.931259155273438000 552717.375333300210000000 5807847.798038133400000000 54.316902160644531000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="74fd9e38-2668-4885-9fd9-0e7364c5a4d6">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>88.28</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="b3838c02-850b-431c-b5b2-9c8a7d163191" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="d0dc7f6c-f1a0-4277-b350-76148d0506a9">
                  <gml:exterior>
                    <gml:LinearRing gml:id="2d82d22e-adee-41d0-bd7b-660ece04e6b8">
                      <gml:posList>552717.379999999890000000 5807847.642999999200000000 54.501575185703700000 552717.375333300210000000 5807847.798038133400000000 54.316902160644531000 552717.375333300210000000 5807847.798038134400000000 55.931259155273438000 552717.379999999890000000 5807847.642999999200000000 56.026635272689603000 552717.379999999890000000 5807847.642999999200000000 54.501575185703700000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="35b50d09-856e-47a9-bd4d-24f51e437426">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>2.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>358.40</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="2fc5533d-9596-414e-b9b6-4fca0a6a40dd" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="cdd41205-b307-410d-bf12-d5d3606bb738">
                  <gml:exterior>
                    <gml:LinearRing gml:id="0702ddbd-3554-43df-9690-7513d32e623d">
                      <gml:posList>552718.948148671650000000 5807847.686872590300000000 54.508810770517144000 552717.379999999890000000 5807847.642999999200000000 54.501575185703700000 552717.379999999890000000 5807847.642999999200000000 56.026635272689603000 552718.948148671650000000 5807847.686872590300000000 56.030372158308786000 552718.948148671650000000 5807847.686872590300000000 54.508810770517144000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="1c0c91c2-7676-4a0e-aafe-fcaaf08dd734">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>2.01</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>88.18</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="1120fc06-33a6-404f-bd3b-9fbebbccb12b" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="72dc44a1-8446-4288-8937-ed5e62295e33">
                  <gml:exterior>
                    <gml:LinearRing gml:id="f03f371f-7030-4cda-8134-9040fb0858e4">
                      <gml:posList>552719.032296077000000000 5807845.045270825700000000 57.655519329351755000 552718.948148671650000000 5807847.686872590300000000 54.508810770517144000 552718.948148671650000000 5807847.686872590300000000 56.030372158308786000 552719.032296077000000000 5807845.045270825700000000 57.655519329351755000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="8d97d4d3-52a7-40fd-a44e-717e4caf87a8">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL4300017Dxt_C:29024</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>30.99</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>178.18</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>49.97</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="ac7f1dd9-967c-4eb1-970b-f7ec08d54f7b" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="01b6ff25-096c-45a3-a866-83fd6df8aaf0">
                  <gml:exterior>
                    <gml:LinearRing gml:id="ed179daa-adba-4b9a-ac36-300fcd3d1f2f">
                      <gml:posList>552718.668663174610000000 5807839.400779462400000000 54.443112053919734000 552720.303999999540000000 5807839.455000000100000000 54.445643654713251000 552720.163534285380000000 5807843.614597613000000000 59.400909423828125000 552711.566434851030000000 5807843.340739675800000000 59.400909423828125000 552711.678000000310000000 5807839.168999999800000000 54.432290082465016000 552713.477291024990000000 5807839.228656527600000000 54.435075494323925000 552713.364421357750000000 5807842.311140772000000000 58.107525097339106000 552718.556399772410000000 5807842.466706536700000000 58.095835562749258000 552718.668663174610000000 5807839.400779462400000000 54.443112053919734000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="91f6d7c9-8e24-4ec9-b3dc-98c285476b90">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL4300017Dxt_C:29024</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>28.53</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>358.18</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>49.97</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="94fdd4b7-50c2-43cd-b56e-7363fe4b78b8" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="25eedd33-b326-4d56-8bd5-e328488b7a16">
                  <gml:exterior>
                    <gml:LinearRing gml:id="33fea341-cb7a-42b2-bc65-b13925577e9f">
                      <gml:posList>552712.480028882270000000 5807844.836550385700000000 57.655519329351755000 552712.390749571260000000 5807847.639255764900000000 54.316902160644531000 552711.380613521090000000 5807847.607078193700000000 54.316902160644531000 552711.387000000100000000 5807847.414999999100000000 54.545718226401448000 552711.457000000400000000 5807847.417999999600000000 54.544801717815020000 552711.466000000010000000 5807847.167999999600000000 54.842644081501689000 552711.324521318780000000 5807847.163506684800000000 54.842628081720484000 552711.379205986040000000 5807845.446815449700000000 56.887571827201810000 552711.509999999780000000 5807845.450999999400000000 56.887550231956283000 552711.566434851030000000 5807843.340739675800000000 59.400909423828125000 552720.163534285380000000 5807843.614597613000000000 59.400909423828125000 552720.025000000370000000 5807847.717000000200000000 54.513779463384239000 552718.948148671650000000 5807847.686872590300000000 54.508810770517144000 552719.032296077000000000 5807845.045270825700000000 57.655519329351755000 552712.480028882270000000 5807844.836550385700000000 57.655519329351755000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="380e0b0a-e859-4a8f-9693-efb4699e8007">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL4300017Dxt_C:29020</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>1.87</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="6e301eb4-e249-4fae-bdb1-c21a27bcdd04" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="ec200bce-839b-488f-ad1c-57bf3f649c01">
                  <gml:exterior>
                    <gml:LinearRing gml:id="bdc9fc63-2373-46b9-8458-4c50776ef431">
                      <gml:posList>552711.324521318780000000 5807847.163506684800000000 53.095531463623047000 552710.300999999980000000 5807847.130999999100000000 53.095531463623047000 552710.231999999840000000 5807847.129000000700000000 53.095531463623047000 552710.291000000200000000 5807845.412000000500000000 53.095531463623047000 552711.379205986040000000 5807845.446815449700000000 53.095531463623047000 552711.324521318780000000 5807847.163506684800000000 53.095531463623047000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="adc8fce5-5a8c-41e1-8fe7-2b529c1b6f1f">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL4300017Dxt_C:29022</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>19.56</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>178.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>35.20</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="29f4b2e5-21a6-4f96-8331-b154c4887776" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="4a2f324b-6752-4a90-bd97-676dc3ccfd47">
                  <gml:exterior>
                    <gml:LinearRing gml:id="dee9d011-b053-439c-b5fa-da8363095b7f">
                      <gml:posList>552718.556399772410000000 5807842.466706536700000000 58.095835562749258000 552713.364421357750000000 5807842.311140772000000000 58.107525097339106000 552713.477291025100000000 5807839.228656524800000000 55.931259155273438000 552718.668663174730000000 5807839.400779459600000000 55.931259155273438000 552718.556399772410000000 5807842.466706536700000000 58.095835562749258000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="23e5722e-f1e6-4765-ab55-20b1c88d3f45">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL4300017Dxt_C:29021</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>21.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>358.18</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>31.59</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="b57028ab-bf19-4651-bcd0-6f94c168bcba" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="a340f16e-4219-44e2-8f1e-95a3d4788edd">
                  <gml:exterior>
                    <gml:LinearRing gml:id="65702f10-5ed0-47ee-ba2e-dd01ca2322c1">
                      <gml:posList>552717.379999999890000000 5807847.642999999200000000 56.026635272689603000 552717.375333300210000000 5807847.798038134400000000 55.931259155273438000 552712.390749571260000000 5807847.639255764900000000 55.931259155273438000 552712.480028882270000000 5807844.836550385700000000 57.655519329351755000 552719.032296077000000000 5807845.045270825700000000 57.655519329351755000 552718.948148671650000000 5807847.686872590300000000 56.030372158308786000 552717.379999999890000000 5807847.642999999200000000 56.026635272689603000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="0d0a03af-ed05-4ad6-8af3-7ac06990b7eb">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL4300017Dxt_C:29023</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>22.70</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>358.11</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>6.44</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="a40026df-3396-44bd-8c26-09654b88d084" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="3ab50c9d-cb78-42ec-9132-5458d44cd36a">
                  <gml:exterior>
                    <gml:LinearRing gml:id="b5f70d09-8c68-45ba-91e0-762c7c9401c4">
                      <gml:posList>552717.375333300210000000 5807847.798038134400000000 53.436967226894623000 552717.263000000270000000 5807851.529999999300000000 53.015712194975826000 552711.254999999890000000 5807851.384999999800000000 53.009657678236714000 552711.380613521090000000 5807847.607078193700000000 53.436144926176404000 552712.390749571260000000 5807847.639255764900000000 53.436283487299249000 552717.375333300210000000 5807847.798038134400000000 53.436967226894623000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </cityObjectMember>

</CityModel>
