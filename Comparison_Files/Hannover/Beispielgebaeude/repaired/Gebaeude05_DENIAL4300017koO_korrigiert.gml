<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<CityModel xmlns="http://www.opengis.net/citygml/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:core="http://www.opengis.net/citygml/base/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:tex="http://www.opengis.net/citygml/textures/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/citygml/2.0 http://schemas.opengis.net/citygml/2.0/cityGMLBase.xsd http://www.opengis.net/citygml/appearance/2.0  http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd">

  <gml:description>Exported by RhinoCity(TM) software - (c)Rhinoterrain 2010-2018 : www.rhinoterrain.com</gml:description>

  <gml:name>RhinoCity(TM)</gml:name>

  <gml:boundedBy>
    <gml:Envelope srsDimension="3" srsName="crs:ETRS89_UTM32*DE_DHHN92_NH">
      <gml:lowerCorner>552535.625000 5807993.000000 51.199997</gml:lowerCorner>
      <gml:upperCorner>552545.812500 5808011.500000 61.512882</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>

  <cityObjectMember>
    <bldg:Building gml:id="DENIAL4300017koO">
      <gen:doubleAttribute name="Volume">
        <gen:value>1249.670</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="TotalRoofArea">
        <gen:value>222.47</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="TotalWallArea">
        <gen:value>360.74</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Authoring_Software">
        <gen:value>RhinoCity</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="OSKA">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ALKISID">
        <gen:value>DENIAL4300017koO</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="KACHEL_ID">
        <gen:value>11_13_Building</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="COUNT">
        <gen:value>843</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="DESC">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCK">
        <gen:value>24</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCKNR">
        <gen:value>212024</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STATBEZNR">
        <gen:value>212</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="MBZNR">
        <gen:value>2109</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="WBZNR">
        <gen:value>2109</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNAM">
        <gen:value>Sahlkamp</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNR">
        <gen:value>21</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNAM">
        <gen:value>Bothfeld-Vahrenheide</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNR">
        <gen:value>03</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEMEINDE">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SCALE">
        <gen:value>5000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_overlaps">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_xmax">
        <gen:value>552545.7630000003</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_xmin">
        <gen:value>552535.6440000003</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_ymax">
        <gen:value>5808011.245999999</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_ymin">
        <gen:value>5807993.312000001</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_zmax">
        <gen:value>51.28532599999011</gen:value>
      </gen:stringAttribute>
      <gen:doubleAttribute name="ZMIN">
        <gen:value>51.200000</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="THICKNESS">
        <gen:value>0.100000</gen:value>
      </gen:doubleAttribute>
      <gen:intAttribute name="TEXTURE">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="BUILDINGID">
        <gen:value>DENIAL4300017koO</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ARTBEZEICH">
        <gen:value>Gebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Join_Cou_1">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="FUNKTION_T">
        <gen:value>Wohngebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADT">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="OVERHANG">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="OBJFLAECHE">
        <gen:value>155.9</gen:value>
      </gen:stringAttribute>
      <bldg:measuredHeight uom="#m">17.126</bldg:measuredHeight>
      <bldg:lod2Solid>
        <gml:Solid gml:id="810cdcc5-2ff5-49e8-830a-dc7c6e23a1f7">
          <gml:exterior>
            <gml:CompositeSurface gml:id="0e3898d2-6bb3-4b69-8a0a-c3c7cdfd9f54">
              <gml:surfaceMember xlink:href="#588aa7f6-f348-4567-b767-e4c59833126f"/>
              <gml:surfaceMember xlink:href="#b8fbd88f-84ea-4ab1-a076-9a37eb896d15"/>
              <gml:surfaceMember xlink:href="#69f87a5b-b084-47fe-96bd-075cb984ad8b"/>
              <gml:surfaceMember xlink:href="#7dc461bb-a1d9-411f-a5bb-c8e3bcc4018e"/>
              <gml:surfaceMember xlink:href="#a0183d68-4d6d-43f5-bcb3-eb85a69a4691"/>
              <gml:surfaceMember xlink:href="#abe50299-1484-462f-8198-7bda8bf4ecee"/>
              <gml:surfaceMember xlink:href="#1d7fd079-fb6a-4b2d-ac01-198abc482fc5"/>
              <gml:surfaceMember xlink:href="#4be3bead-1a29-461c-9925-a810381d0746"/>
              <gml:surfaceMember xlink:href="#e60a6e72-6319-456a-b80b-f2337e91ca39"/>
              <gml:surfaceMember xlink:href="#b12bb840-89fb-496c-9de4-a3ad05e3bb10"/>
              <gml:surfaceMember xlink:href="#57422808-78d6-4e8e-8e00-7b5acc48facf"/>
              <gml:surfaceMember xlink:href="#fa73d3f6-a5bc-450a-b646-b3625fdec01d"/>
              <gml:surfaceMember xlink:href="#f0eec04b-7f7c-41ec-96ff-6ad429f62493"/>
              <gml:surfaceMember xlink:href="#7cf1f0f7-7d16-49ec-8833-9d7795e3a078"/>
              <gml:surfaceMember xlink:href="#83427f93-7616-450c-b8f4-b3db691e3f0c"/>
              <gml:surfaceMember xlink:href="#fd2e89bc-e3f0-4b7d-a985-d7d95eeed9d6"/>
              <gml:surfaceMember xlink:href="#85863f9b-e6b1-4114-9a2b-601380e50ab4"/>
              <gml:surfaceMember xlink:href="#01171fa7-566e-434a-9056-a813e05aa2d0"/>
              <gml:surfaceMember xlink:href="#4c5997e0-e192-4f3c-8ec0-40d726972731"/>
              <gml:surfaceMember xlink:href="#77f89a60-3dfc-4bec-8b5c-e5409ee49a52"/>
              <gml:surfaceMember xlink:href="#5d2c41ec-f150-4405-9c59-dd9082ee84b5"/>
              <gml:surfaceMember xlink:href="#c7c9e4a0-3108-405a-8e09-60ccdd13b2d9"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="b6f2e17a-1d21-4530-8544-abfefee8fa96">
          <gen:stringAttribute name="Type">
            <gen:value>Ground</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>155.90</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="63b52b90-0292-4561-a414-cdaa9dc9569b" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="588aa7f6-f348-4567-b767-e4c59833126f">
                  <gml:exterior>
                    <gml:LinearRing gml:id="02574c74-0a22-4c80-b3c0-368c97f17737">
                      <gml:posList>552545.464999999850000000 5808003.073999999100000000 51.200000000000003000 552545.763000000270000000 5807993.493000000700000000 51.200000000000003000 552541.705261185650000000 5807993.401718528900000000 51.200000000000003000 552537.717000000180000000 5807993.312000000800000000 51.200000000000003000 552537.582999999630000000 5807998.287000000500000000 51.200000000000003000 552535.870000000110000000 5807998.222999999300000000 51.200000000000003000 552535.644000000320000000 5808006.196000000500000000 51.200000000000003000 552537.307000000030000000 5808006.262000000100000000 51.200000000000003000 552537.175999999980000000 5808010.966000000000000000 51.200000000000003000 552541.095228011140000000 5808011.102643486100000000 51.200000000000003000 552545.207000000400000000 5808011.245999999300000000 51.200000000000003000 552545.218999999580000000 5808010.905999999500000000 51.200000000000003000 552545.464999999850000000 5808003.073999999100000000 51.200000000000003000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="811bba48-c8df-4dc3-be1f-82e97e9a68e9">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>1.94</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>87.98</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="71ad01d5-62e7-4a85-abab-75da00276b68" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="b8fbd88f-84ea-4ab1-a076-9a37eb896d15">
                  <gml:exterior>
                    <gml:LinearRing gml:id="04a21822-75fa-40fb-89a0-12fcf08b252c">
                      <gml:posList>552545.218999999580000000 5808010.905999999500000000 51.200000000000003000 552545.207000000400000000 5808011.245999999300000000 51.200000000000003000 552545.207000000400000000 5808011.245999999300000000 56.907867526791080000 552545.218999999580000000 5808010.905999999500000000 56.907551563028690000 552545.218999999580000000 5808010.905999999500000000 51.200000000000003000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="d844b83b-a270-4ebd-9f1e-835540295ad1">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>44.83</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>88.20</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="f4b5f69d-4b98-4b9b-9fa0-4de1cf4e45fd" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="69f87a5b-b084-47fe-96bd-075cb984ad8b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="fbefdd9d-5a80-4c63-b54c-01608f76b2d0">
                      <gml:posList>552545.464999999850000000 5808003.073999999100000000 51.200000000000003000 552545.218999999580000000 5808010.905999999500000000 51.200000000000003000 552545.218999999580000000 5808010.905999999500000000 56.907551563028690000 552545.464999999850000000 5808003.073999999100000000 56.934305399058971000 552545.464999999850000000 5808003.073999999100000000 51.200000000000003000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="e1dacc95-de6b-4b9a-a118-30b3b61ef3e7">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>55.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>88.22</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="04539333-7e9d-49c6-9921-fd0752d94384" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="7dc461bb-a1d9-411f-a5bb-c8e3bcc4018e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="e9a93ff6-351d-4e8e-9085-666603be851e">
                      <gml:posList>552545.763000000270000000 5807993.493000000700000000 51.200000000000003000 552545.464999999850000000 5808003.073999999100000000 51.200000000000003000 552545.464999999850000000 5808003.073999999100000000 56.934305399058971000 552545.763000000270000000 5807993.493000000700000000 56.970317328297327000 552545.763000000270000000 5807993.493000000700000000 51.200000000000003000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="7b8bb89a-a562-46d5-a198-02f24053b039">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>65.62</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>178.71</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="85d417cf-6801-4ab3-a0da-af8012ac29aa" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="a0183d68-4d6d-43f5-bcb3-eb85a69a4691">
                  <gml:exterior>
                    <gml:LinearRing gml:id="918932a5-fe4f-4e31-9a1f-360ed385b910">
                      <gml:posList>552545.763000000270000000 5807993.493000000700000000 56.970317328297327000 552541.705261185650000000 5807993.401718528900000000 61.512878417968750000 552537.717000000180000000 5807993.312000000800000000 57.420286476846826000 552537.717000000180000000 5807993.312000000800000000 51.200000000000003000 552541.705261185650000000 5807993.401718528900000000 51.200000000000003000 552545.763000000270000000 5807993.493000000700000000 51.200000000000003000 552545.763000000270000000 5807993.493000000700000000 56.970317328297327000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="f8260689-7c20-4b03-870d-131d17eb8aa7">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>31.05</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>268.46</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="514ebabc-abb3-4681-b333-6b99156cfdfb" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="abe50299-1484-462f-8198-7bda8bf4ecee">
                  <gml:exterior>
                    <gml:LinearRing gml:id="fe05f5e1-a527-4355-aa9f-c113007eb5a5">
                      <gml:posList>552537.717000000180000000 5807993.312000000800000000 57.420286476846826000 552537.582999999630000000 5807998.287000000500000000 57.458691675511290000 552537.582999999630000000 5807998.287000000500000000 51.200000000000003000 552537.717000000180000000 5807993.312000000800000000 51.200000000000003000 552537.717000000180000000 5807993.312000000800000000 57.420286476846826000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="f5e1dfd4-5362-42e2-860e-0815d0fbd5e5">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>9.22</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>177.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="057f88b1-06ea-4c72-9730-c5b1d3309d91" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="1d7fd079-fb6a-4b2d-ac01-198abc482fc5">
                  <gml:exterior>
                    <gml:LinearRing gml:id="2018e308-c4fa-4fb2-98ad-d9d86c193ed5">
                      <gml:posList>552537.582999999630000000 5807998.287000000500000000 57.458691675511290000 552535.870000000110000000 5807998.222999999300000000 55.699980654785094000 552535.870000000110000000 5807998.222999999300000000 51.200000000000003000 552537.582999999630000000 5807998.287000000500000000 51.200000000000003000 552537.582999999630000000 5807998.287000000500000000 57.458691675511290000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="fa6041a8-d602-4488-a948-1486b9a18ebf">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>36.09</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>268.38</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="4d3bdf2e-25f4-4ae5-828c-a51fa6ea728a" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="4be3bead-1a29-461c-9925-a810381d0746">
                  <gml:exterior>
                    <gml:LinearRing gml:id="9c6aaaea-885e-41f6-872d-c4e94a70058e">
                      <gml:posList>552535.870000000110000000 5807998.222999999300000000 55.699980654785094000 552535.644000000320000000 5808006.196000000500000000 55.749994132847561000 552535.644000000320000000 5808006.196000000500000000 51.200000000000003000 552535.870000000110000000 5807998.222999999300000000 51.200000000000003000 552535.870000000110000000 5807998.222999999300000000 55.699980654785094000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="7188c16b-ea4f-46f6-b03a-4991606c463a">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>8.99</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>357.73</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="a5283777-69be-4e81-bdfd-26e5a5a30a7b" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="e60a6e72-6319-456a-b80b-f2337e91ca39">
                  <gml:exterior>
                    <gml:LinearRing gml:id="3aa68bde-7a0a-429f-b93d-aaedbeb15b81">
                      <gml:posList>552537.307000000030000000 5808006.262000000100000000 51.200000000000003000 552535.644000000320000000 5808006.196000000500000000 51.200000000000003000 552535.644000000320000000 5808006.196000000500000000 55.749994132847561000 552537.307000000030000000 5808006.262000000100000000 57.457507602743064000 552537.307000000030000000 5808006.262000000100000000 51.200000000000003000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="3ca862d3-bef6-4ab3-a347-0e91fc02fc21">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>29.52</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>268.40</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="095ed772-5e39-4850-9023-8f507e586d0a" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="b12bb840-89fb-496c-9de4-a3ad05e3bb10">
                  <gml:exterior>
                    <gml:LinearRing gml:id="8646dd0a-3a32-4361-97fb-b1d92f480ef5">
                      <gml:posList>552537.307000000030000000 5808006.262000000100000000 57.457507602743064000 552537.175999999980000000 5808010.966000000000000000 57.489412433943052000 552537.175999999980000000 5808010.966000000000000000 51.200000000000003000 552537.307000000030000000 5808006.262000000100000000 51.200000000000003000 552537.307000000030000000 5808006.262000000100000000 57.457507602743064000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="8cd4df20-bb91-4ef9-8689-0bf8d4d87291">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>32.55</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>358.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="67e2f047-631c-4f17-a085-31b65745976c" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="57422808-78d6-4e8e-8e00-7b5acc48facf">
                  <gml:exterior>
                    <gml:LinearRing gml:id="0a2134c5-b6aa-4205-a8ea-0a6a12d5e275">
                      <gml:posList>552541.095228011140000000 5808011.102643486100000000 51.200000000000003000 552537.175999999980000000 5808010.966000000000000000 51.200000000000003000 552537.175999999980000000 5808010.966000000000000000 57.489412433943052000 552541.095228011140000000 5808011.102643486100000000 61.512878417968750000 552541.095228011140000000 5808011.102643486100000000 51.200000000000003000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="acfa28f3-93f5-4471-be7f-da87d1261dea">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>32.96</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>358.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="65e57896-e6ff-4a2c-9ddc-d9f5ccc3b064" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="fa73d3f6-a5bc-450a-b646-b3625fdec01d">
                  <gml:exterior>
                    <gml:LinearRing gml:id="d09135c6-29bd-4fcb-9d80-7b87202def6b">
                      <gml:posList>552545.207000000400000000 5808011.245999999300000000 51.200000000000003000 552541.095228011140000000 5808011.102643486100000000 51.200000000000003000 552541.095228011140000000 5808011.102643486100000000 61.512878417968750000 552545.207000000400000000 5808011.245999999300000000 56.907867526791080000 552545.207000000400000000 5808011.245999999300000000 51.200000000000003000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="340d4734-31e3-45c0-ab13-5440b45ee7f4">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.56</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>178.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="23e60cda-b6b3-458d-bb74-ba4ffabd80ab" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="f0eec04b-7f7c-41ec-96ff-6ad429f62493">
                  <gml:exterior>
                    <gml:LinearRing gml:id="9adb3206-b2e8-431f-8727-c7e802b2adb7">
                      <gml:posList>552541.513689201090000000 5807998.960434783300000000 61.512878417968750000 552537.826259561000000000 5807998.856481975900000000 58.029945373535156000 552537.826259561000000000 5807998.856481975900000000 57.728245465972876000 552541.513689201090000000 5807998.960434783300000000 61.512878417968750000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="fa9b81c9-7bd2-41f7-a70d-e5ad10eff753">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>2.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>268.03</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="cf237cc2-c864-49c2-bc8d-8dc49c6223fd" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="7cf1f0f7-7d16-49ec-8833-9d7795e3a078">
                  <gml:exterior>
                    <gml:LinearRing gml:id="7c93227e-06cf-4281-a44f-93b157d6cd06">
                      <gml:posList>552537.826259561000000000 5807998.856481975900000000 58.029945373535156000 552537.586338813650000000 5808005.818101969500000000 58.029945373535156000 552537.586338813650000000 5808005.818101969500000000 57.728245465989673000 552537.826259561000000000 5807998.856481975900000000 57.728245465972876000 552537.826259561000000000 5807998.856481975900000000 58.029945373535156000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="3fdbc590-9a4a-4715-89d5-eda3fb9ff501">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.56</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>358.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="91f5590a-68f0-4c1c-8e7e-8f97a9bc709d" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="83427f93-7616-450c-b8f4-b3db691e3f0c">
                  <gml:exterior>
                    <gml:LinearRing gml:id="ff4f11ff-e0ee-401c-bfa8-27b9fdb7a7b7">
                      <gml:posList>552541.273768453740000000 5808005.922054776900000000 61.512878417968750000 552537.586338813650000000 5808005.818101969500000000 57.728245465989673000 552537.586338813650000000 5808005.818101969500000000 58.029945373535156000 552541.273768453740000000 5808005.922054776900000000 61.512878417968750000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="97130a4a-0423-4347-86ca-6a9b20aaca94">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>1.72</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>358.40</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="556899e5-0fdd-4b6f-a5dc-49f2e113a0a5" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="fd2e89bc-e3f0-4b7d-a985-d7d95eeed9d6">
                  <gml:exterior>
                    <gml:LinearRing gml:id="0a18827e-9b1f-4691-92ed-fcff70919c21">
                      <gml:posList>552545.173970193950000000 5808006.030861678500000000 58.029945373535156000 552545.173970193950000000 5808006.030861678500000000 57.145864826318729000 552541.273768453740000000 5808005.922054776900000000 61.512878417968750000 552545.173970193950000000 5808006.030861678500000000 58.029945373535156000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="cf4349e7-0f89-489a-82b1-ce500d0fad6e">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>6.16</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>88.03</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="ed68b95d-c532-4798-9b5d-b0e7c92c883d" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="85863f9b-e6b1-4114-9a2b-601380e50ab4">
                  <gml:exterior>
                    <gml:LinearRing gml:id="2635e00d-7be5-4bb8-8ad3-d234ad7c6c13">
                      <gml:posList>552545.413890941300000000 5807999.069241684900000000 57.145864826337061000 552545.173970193950000000 5808006.030861678500000000 57.145864826318729000 552545.173970193950000000 5808006.030861678500000000 58.029945373535156000 552545.413890941300000000 5807999.069241684900000000 58.029945373535156000 552545.413890941300000000 5807999.069241684900000000 57.145864826337061000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="4a929bc5-df95-4911-9f70-37349efff91c">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>1.72</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>178.40</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="7b2dac9f-625a-414a-b256-47018f95adfb" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="01171fa7-566e-434a-9056-a813e05aa2d0">
                  <gml:exterior>
                    <gml:LinearRing gml:id="7f69b6e0-cd0f-4120-b05c-619a7c8d0ba0">
                      <gml:posList>552545.413890941300000000 5807999.069241684900000000 58.029945373535156000 552541.513689201090000000 5807998.960434783300000000 61.512878417968750000 552545.413890941300000000 5807999.069241684900000000 57.145864826337061000 552545.413890941300000000 5807999.069241684900000000 58.029945373535156000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="cd5ad62e-46c8-449b-be4e-97c12b758598">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL4300017koO_C:29127</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>68.03</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>88.03</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>48.22</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="6d25c8c4-1d5d-4aa0-95f0-bf6ddb3d6987" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="4c5997e0-e192-4f3c-8ec0-40d726972731">
                  <gml:exterior>
                    <gml:LinearRing gml:id="5fc5557c-b257-40ef-953e-9904b3225ffa">
                      <gml:posList>552545.763000000270000000 5807993.493000000700000000 56.970317328297327000 552545.464999999850000000 5808003.073999999100000000 56.934305399058971000 552545.218999999580000000 5808010.905999999500000000 56.907551563028690000 552545.207000000400000000 5808011.245999999300000000 56.907867526791080000 552541.095228011140000000 5808011.102643486100000000 61.512878417968750000 552541.273768453740000000 5808005.922054776900000000 61.512878417968750000 552545.173970193950000000 5808006.030861678500000000 57.145864826318729000 552545.413890941300000000 5807999.069241684900000000 57.145864826337061000 552541.513689201090000000 5807998.960434783300000000 61.512878417968750000 552541.705261185650000000 5807993.401718528900000000 61.512878417968750000 552545.763000000270000000 5807993.493000000700000000 56.970317328297327000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bb855dbb-9bd1-474b-9a36-65954a9ff9c9">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL4300017koO_C:29127</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>82.67</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>268.03</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>45.73</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="18c64b93-6a5f-4419-9dde-17a27ad2ac4e" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="77f89a60-3dfc-4bec-8b5c-e5409ee49a52">
                  <gml:exterior>
                    <gml:LinearRing gml:id="470eded3-9c6a-4f87-bcc2-88b8031cabd9">
                      <gml:posList>552537.586338813650000000 5808005.818101969500000000 57.728245465989673000 552541.273768453740000000 5808005.922054776900000000 61.512878417968750000 552541.095228011140000000 5808011.102643486100000000 61.512878417968750000 552537.175999999980000000 5808010.966000000000000000 57.489412433943052000 552537.307000000030000000 5808006.262000000100000000 57.457507602743064000 552535.644000000320000000 5808006.196000000500000000 55.749994132847561000 552535.870000000110000000 5807998.222999999300000000 55.699980654785094000 552537.582999999630000000 5807998.287000000500000000 57.458691675511290000 552537.717000000180000000 5807993.312000000800000000 57.420286476846826000 552541.705261185650000000 5807993.401718528900000000 61.512878417968750000 552541.513689201090000000 5807998.960434783300000000 61.512878417968750000 552537.826259561000000000 5807998.856481975900000000 57.728245465972876000 552537.586338813650000000 5808005.818101969500000000 57.728245465989673000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="56178d8d-0f91-4c17-a6d9-eecdc1f81042">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL4300017koO_C:29126</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>35.34</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>268.03</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>43.36</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="b5246d49-33ad-4407-8f3f-e357f4713952" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="5d2c41ec-f150-4405-9c59-dd9082ee84b5">
                  <gml:exterior>
                    <gml:LinearRing gml:id="ae1222ba-f997-4202-b061-2cd6e20f609a">
                      <gml:posList>552541.273768453740000000 5808005.922054776900000000 61.512878417968750000 552537.586338813650000000 5808005.818101969500000000 58.029945373535156000 552537.826259561000000000 5807998.856481975900000000 58.029945373535156000 552541.513689201090000000 5807998.960434783300000000 61.512878417968750000 552541.273768453740000000 5808005.922054776900000000 61.512878417968750000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="8c8c1055-28e0-4791-962e-96e76453514c">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL4300017koO_C:29126</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>36.43</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>88.03</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>41.75</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="305cf42e-75bb-48ef-ad06-6b97343b1c6d" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="c7c9e4a0-3108-405a-8e09-60ccdd13b2d9">
                  <gml:exterior>
                    <gml:LinearRing gml:id="c340d4e4-7181-4619-9e6e-3ba6e4e258b1">
                      <gml:posList>552545.173970193950000000 5808006.030861678500000000 58.029945373535156000 552541.273768453740000000 5808005.922054776900000000 61.512878417968750000 552541.513689201090000000 5807998.960434783300000000 61.512878417968750000 552545.413890941300000000 5807999.069241684900000000 58.029945373535156000 552545.173970193950000000 5808006.030861678500000000 58.029945373535156000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </cityObjectMember>

</CityModel>
