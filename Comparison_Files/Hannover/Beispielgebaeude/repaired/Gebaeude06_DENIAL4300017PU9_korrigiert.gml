<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<CityModel xmlns="http://www.opengis.net/citygml/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:core="http://www.opengis.net/citygml/base/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:tex="http://www.opengis.net/citygml/textures/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/citygml/2.0 http://schemas.opengis.net/citygml/2.0/cityGMLBase.xsd http://www.opengis.net/citygml/appearance/2.0  http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd">

  <gml:description>Exported by RhinoCity(TM) software - (c)Rhinoterrain 2010-2018 : www.rhinoterrain.com</gml:description>

  <gml:name>RhinoCity(TM)</gml:name>

  <gml:boundedBy>
    <gml:Envelope srsDimension="3" srsName="crs:ETRS89_UTM32*DE_DHHN92_NH">
      <gml:lowerCorner>552264.937500 5808299.000000 50.259998</gml:lowerCorner>
      <gml:upperCorner>552277.687500 5808311.000000 58.777325</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>

  <cityObjectMember>
    <bldg:Building gml:id="DENIAL4300017PU9">
      <gen:doubleAttribute name="Volume">
        <gen:value>622.933</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="TotalRoofArea">
        <gen:value>167.25</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="TotalWallArea">
        <gen:value>188.45</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Authoring_Software">
        <gen:value>RhinoCity</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="OSKA">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ALKISID">
        <gen:value>DENIAL4300017PU9</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="KACHEL_ID">
        <gen:value>11_13_Building</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="COUNT">
        <gen:value>843</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="DESC">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCK">
        <gen:value>11</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCKNR">
        <gen:value>212011</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STATBEZNR">
        <gen:value>212</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="MBZNR">
        <gen:value>2107</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="WBZNR">
        <gen:value>2107</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNAM">
        <gen:value>Sahlkamp</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNR">
        <gen:value>21</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNAM">
        <gen:value>Bothfeld-Vahrenheide</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNR">
        <gen:value>03</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEMEINDE">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SCALE">
        <gen:value>5000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_overlaps">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_xmax">
        <gen:value>552277.6770000001</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_xmin">
        <gen:value>552264.9400000004</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_ymax">
        <gen:value>5808310.7129999995</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_ymin">
        <gen:value>5808299.528000001</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_zmax">
        <gen:value>50.35514799997956</gen:value>
      </gen:stringAttribute>
      <gen:doubleAttribute name="ZMIN">
        <gen:value>50.260000</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="THICKNESS">
        <gen:value>0.100000</gen:value>
      </gen:doubleAttribute>
      <gen:intAttribute name="TEXTURE">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="BUILDINGID">
        <gen:value>DENIAL4300017PU9</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ARTBEZEICH">
        <gen:value>Gebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Join_Cou_1">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="FUNKTION_T">
        <gen:value>Wohngebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADT">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="OVERHANG">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="OBJFLAECHE">
        <gen:value>119.9</gen:value>
      </gen:stringAttribute>
      <bldg:measuredHeight uom="#m">15.216</bldg:measuredHeight>
      <bldg:lod2Solid>
        <gml:Solid gml:id="c67146f3-e613-40ad-b5da-9bc7a487f191">
          <gml:exterior>
            <gml:CompositeSurface gml:id="ad2e20ba-c412-4e49-830b-58d696f4addb">
              <gml:surfaceMember xlink:href="#335f2da6-2c94-4cb1-a879-fce6b6f37e61"/>
              <gml:surfaceMember xlink:href="#0e95389f-0e72-4a39-836f-7f15d6f21bf7"/>
              <gml:surfaceMember xlink:href="#035cefa8-d5c4-4718-8123-826cdb367740"/>
              <gml:surfaceMember xlink:href="#6dd3fa34-e868-4a96-999e-1c7278e6b3b1"/>
              <gml:surfaceMember xlink:href="#f40467fe-74a4-4e47-bfd9-f397be5e4e3e"/>
              <gml:surfaceMember xlink:href="#9e28c110-55a9-4992-a817-8866f51d7958"/>
              <gml:surfaceMember xlink:href="#04598544-8845-4228-aa9a-601b955ec307"/>
              <gml:surfaceMember xlink:href="#3f1c8ce4-51e7-4e11-a91b-d57d825703fd"/>
              <gml:surfaceMember xlink:href="#4caea0b8-fde3-4402-b289-3a2bbba40aa8"/>
              <gml:surfaceMember xlink:href="#89d96045-5fd6-4589-ab5a-d85b70a3f9b2"/>
              <gml:surfaceMember xlink:href="#bc91e840-c8df-4806-9d06-a90ff50fbbaf"/>
              <gml:surfaceMember xlink:href="#07967a36-5acd-4874-bc7f-c20e96e31a7e"/>
              <gml:surfaceMember xlink:href="#af7ddc8e-38f0-46cd-8c3d-5ccdde0acc0a"/>
              <gml:surfaceMember xlink:href="#46e3bf53-34f9-4544-bfb2-a84e0fbd5b89"/>
              <gml:surfaceMember xlink:href="#6f0d116f-ae68-4543-b2be-e01e4b9b71ab"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="5aa11c74-2e3e-4862-8cf0-43c5e1d7bcd3">
          <gen:stringAttribute name="Type">
            <gen:value>Ground</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>119.90</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="851585ed-7eb4-43e6-aa0a-125d6c0627f7" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="335f2da6-2c94-4cb1-a879-fce6b6f37e61">
                  <gml:exterior>
                    <gml:LinearRing gml:id="58ea8b4c-969a-4e50-9c7f-7b733ca82bb3">
                      <gml:posList>552274.003063968150000000 5808302.310635389800000000 50.260000000000005000 552273.882000000220000000 5808302.303999999500000000 50.260000000000005000 552274.014000000430000000 5808300.041999999400000000 50.260000000000005000 552265.553000000310000000 5808299.528000000900000000 50.260000000000005000 552265.308334250000000000 5808303.698094224600000000 50.260000000000005000 552264.940000000410000000 5808309.975999999800000000 50.260000000000012000 552273.554924150230000000 5808310.493836970100000000 50.259999999999998000 552277.201000000350000000 5808310.712999999500000000 50.260000000000012000 552277.330088561280000000 5808308.488934266400000000 50.260000000000005000 552277.677000000140000000 5808302.512000000100000000 50.260000000000005000 552274.003063968150000000 5808302.310635389800000000 50.260000000000005000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="3509d05c-a903-4632-930e-a90fe853c1b4">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>10.26</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>356.56</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="e5e2946f-754d-4d37-b7e8-e5b329ad8432" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="0e95389f-0e72-4a39-836f-7f15d6f21bf7">
                  <gml:exterior>
                    <gml:LinearRing gml:id="b2fa9a41-ba95-4a3e-bbe7-f49ffdc9f70b">
                      <gml:posList>552277.201000000350000000 5808310.712999999500000000 50.260000000000012000 552273.554924150230000000 5808310.493836970100000000 50.259999999999998000 552273.554924150230000000 5808310.493836970100000000 53.078356709673102000 552277.201000000350000000 5808310.712999999500000000 53.060847334913007000 552277.201000000350000000 5808310.712999999500000000 50.260000000000012000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="576ecab0-b399-4bd5-9c99-e3d1e57f7727">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>8.47</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>86.68</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="d6c6a762-bf98-4cf2-ac50-b1689dd5c8dd" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="035cefa8-d5c4-4718-8123-826cdb367740">
                  <gml:exterior>
                    <gml:LinearRing gml:id="19de3d16-279a-4d73-91aa-aaac56aa0130">
                      <gml:posList>552277.330088561280000000 5808308.488934266400000000 50.260000000000005000 552277.201000000350000000 5808310.712999999500000000 50.260000000000012000 552277.201000000350000000 5808310.712999999500000000 53.060847334913007000 552277.330088561280000000 5808308.488934266400000000 55.065086389933555000 552277.330088561280000000 5808308.488934266400000000 50.260000000000005000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="62f723d3-9bec-4687-a6dd-ba0ffe2ea7a3">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>22.06</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>86.68</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="a54f99e5-cdfa-454c-a63e-75f7d44ec2e1" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="6dd3fa34-e868-4a96-999e-1c7278e6b3b1">
                  <gml:exterior>
                    <gml:LinearRing gml:id="0fe69c88-17c2-4c5d-81a3-540f35441c27">
                      <gml:posList>552277.677000000140000000 5808302.512000000100000000 50.260000000000005000 552277.330088561280000000 5808308.488934266400000000 50.260000000000005000 552277.330088561280000000 5808308.488934266400000000 55.065086389933555000 552277.677000000140000000 5808302.512000000100000000 52.822786102309948000 552277.677000000140000000 5808302.512000000100000000 50.260000000000005000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="f50478f5-a087-4f1d-b266-24f8670e186f">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>9.43</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>176.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="76a56a78-486b-4f54-a48b-69822f2973a7" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="f40467fe-74a4-4e47-bfd9-f397be5e4e3e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="49b89ae6-a258-4010-bef8-8bf22be877bc">
                      <gml:posList>552277.677000000140000000 5808302.512000000100000000 52.822786102309948000 552274.003063969080000000 5808302.310635389800000000 52.822723877211914000 552274.003063968150000000 5808302.310635389800000000 50.260000000000005000 552277.677000000140000000 5808302.512000000100000000 50.260000000000005000 552277.677000000140000000 5808302.512000000100000000 52.822786102309948000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="e09d50c7-fa8b-41d8-9810-cd0b5ff15f48">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.73</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>176.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="5202657e-a91f-42dd-9a48-3e0be1ed32ca" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="9e28c110-55a9-4992-a817-8866f51d7958">
                  <gml:exterior>
                    <gml:LinearRing gml:id="27b91fb4-fe2c-44cb-b662-92fb18336f62">
                      <gml:posList>552274.003063969080000000 5808302.310635389800000000 52.822723877211914000 552274.003063968150000000 5808302.310635389800000000 56.252960438525825000 552273.882000000220000000 5808302.303999999500000000 56.252952964875718000 552273.882000000220000000 5808302.303999999500000000 50.260000000000005000 552274.003063968150000000 5808302.310635389800000000 50.260000000000005000 552274.003063969080000000 5808302.310635389800000000 52.822723877211914000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="ac42bd87-dbe7-4bd5-be3e-1be263b1d798">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>10.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>86.66</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="e262f716-c4f0-4087-9828-e95aa5938315" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="04598544-8845-4228-aa9a-601b955ec307">
                  <gml:exterior>
                    <gml:LinearRing gml:id="e3cd1e3a-c199-4c7c-96b6-6235ee21d758">
                      <gml:posList>552274.014000000430000000 5808300.041999999400000000 50.260000000000005000 552273.882000000220000000 5808302.303999999500000000 50.260000000000005000 552273.882000000220000000 5808302.303999999500000000 56.252952964875718000 552274.014000000430000000 5808300.041999999400000000 53.179162754960196000 552274.014000000430000000 5808300.041999999400000000 50.260000000000005000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="705ef252-f247-4e92-a298-ddcb0b7b6b19">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>24.45</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>176.52</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="4472e52f-025e-4955-8848-ad27a57f1297" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="3f1c8ce4-51e7-4e11-a91b-d57d825703fd">
                  <gml:exterior>
                    <gml:LinearRing gml:id="c8da65f5-a671-495b-9c81-93cfde7f3151">
                      <gml:posList>552274.014000000430000000 5808300.041999999400000000 53.179162754960196000 552265.553000000310000000 5808299.528000000900000000 53.110558719025391000 552265.553000000310000000 5808299.528000000900000000 50.260000000000005000 552274.014000000430000000 5808300.041999999400000000 50.260000000000005000 552274.014000000430000000 5808300.041999999400000000 53.179162754960196000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="9c503d5c-1477-4fbc-ac88-fea468410d58">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>59.52</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>266.64</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="6976e070-2c78-4c97-9297-1b6adc9748d5" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="4caea0b8-fde3-4402-b289-3a2bbba40aa8">
                  <gml:exterior>
                    <gml:LinearRing gml:id="2d22b2f1-03c0-4b09-b0f0-631e9359a6c3">
                      <gml:posList>552265.553000000310000000 5808299.528000000900000000 53.110558719025391000 552265.308334250000000000 5808303.698094224600000000 58.777320861816406000 552264.940000000410000000 5808309.975999999800000000 53.119727748032325000 552264.940000000410000000 5808309.975999999800000000 50.260000000000012000 552265.308334250000000000 5808303.698094224600000000 50.260000000000005000 552265.553000000310000000 5808299.528000000900000000 50.260000000000005000 552265.553000000310000000 5808299.528000000900000000 53.110558719025391000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="3eb70d7c-340c-4c2e-9c9f-3ab35c8c8a21">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>24.50</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>356.56</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="a4c3b6b9-6615-4ea5-abf4-3cc450911852" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="89d96045-5fd6-4589-ab5a-d85b70a3f9b2">
                  <gml:exterior>
                    <gml:LinearRing gml:id="e9e9b2b8-0be4-415a-b68b-e5bea84fb272">
                      <gml:posList>552273.554924150230000000 5808310.493836970100000000 50.259999999999998000 552264.940000000410000000 5808309.975999999800000000 50.260000000000012000 552264.940000000410000000 5808309.975999999800000000 53.119727748032325000 552273.554924150230000000 5808310.493836970100000000 53.078356709673102000 552273.554924150230000000 5808310.493836970100000000 50.259999999999998000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="75137132-1bf2-407a-b614-818e526f6aff">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>18.93</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>86.87</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="d0e678b2-715f-4dc9-8424-489581b029b3" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bc91e840-c8df-4806-9d06-a90ff50fbbaf">
                  <gml:exterior>
                    <gml:LinearRing gml:id="9ff3c90d-6d69-4f3e-b575-0f2db5462a2a">
                      <gml:posList>552274.003063969080000000 5808302.310635389800000000 52.822723877211914000 552273.901311444580000000 5808304.168674741900000000 53.519659191306971000 552273.675679086360000000 5808308.288806431900000000 55.065086778835834000 552273.901311444000000000 5808304.168674741900000000 58.777320861816406000 552274.003063968150000000 5808302.310635389800000000 56.252960438525825000 552274.003063969080000000 5808302.310635389800000000 52.822723877211914000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="12c677ff-f6bd-4f31-8566-90ebee6c423d">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL4300017PU9_C:29142</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>73.17</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>356.87</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>41.98</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="3cfd1387-d34f-44ea-877a-3241a0d54f1c" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="07967a36-5acd-4874-bc7f-c20e96e31a7e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="237cbbe7-a546-4f9b-856d-8b11fd341ee2">
                      <gml:posList>552273.675679086360000000 5808308.288806431900000000 55.065086778835834000 552273.554924150230000000 5808310.493836970100000000 53.078356709673102000 552264.940000000410000000 5808309.975999999800000000 53.119727748032325000 552265.308334250000000000 5808303.698094224600000000 58.777320861816406000 552273.901311444000000000 5808304.168674741900000000 58.777320861816406000 552273.675679086360000000 5808308.288806431900000000 55.065086778835834000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="53d52c9d-8569-420f-9654-808e962f5fb0">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL4300017PU9_C:29142</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>59.71</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>176.87</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>53.60</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="e1995f97-8742-4084-bf76-b9c21fdcc960" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="af7ddc8e-38f0-46cd-8c3d-5ccdde0acc0a">
                  <gml:exterior>
                    <gml:LinearRing gml:id="0d55d767-2a08-44fc-8180-3bcdec30ad0d">
                      <gml:posList>552274.003063968150000000 5808302.310635389800000000 56.252960438525825000 552273.901311444000000000 5808304.168674741900000000 58.777320861816406000 552265.308334250000000000 5808303.698094224600000000 58.777320861816406000 552265.553000000310000000 5808299.528000000900000000 53.110558719025391000 552274.014000000430000000 5808300.041999999400000000 53.179162754960196000 552273.882000000220000000 5808302.303999999500000000 56.252952964875718000 552274.003063968150000000 5808302.310635389800000000 56.252960438525825000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="4d70e7a6-1805-4966-b726-3dedfdd77468">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL4300017PU9_C:29141</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>23.46</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>176.87</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>20.53</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="6b2d73ae-934c-4b21-8f23-e2601895fc22" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="46e3bf53-34f9-4544-bfb2-a84e0fbd5b89">
                  <gml:exterior>
                    <gml:LinearRing gml:id="8bfae71e-47d3-4d1c-8611-7d9e637b6fd0">
                      <gml:posList>552277.677000000140000000 5808302.512000000100000000 52.822786102309948000 552277.330088561280000000 5808308.488934266400000000 55.065086389933555000 552273.675679086360000000 5808308.288806431900000000 55.065086778835834000 552273.901311444580000000 5808304.168674741900000000 53.519659191306971000 552274.003063969080000000 5808302.310635389800000000 52.822723877211914000 552277.677000000140000000 5808302.512000000100000000 52.822786102309948000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="c48f38ec-4715-41bc-884d-d7b1f54e22dc">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL4300017PU9_C:29141</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>10.91</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>356.87</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>41.98</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="f9b2fedf-997c-41ad-8cde-b17cb707bf74" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="6f0d116f-ae68-4543-b2be-e01e4b9b71ab">
                  <gml:exterior>
                    <gml:LinearRing gml:id="4b7c5e1b-5bce-491a-be14-681192bd9f58">
                      <gml:posList>552277.201000000350000000 5808310.712999999500000000 53.060847334913007000 552273.554924150230000000 5808310.493836970100000000 53.078356709673102000 552273.675679086360000000 5808308.288806431900000000 55.065086778835834000 552277.330088561280000000 5808308.488934266400000000 55.065086389933555000 552277.201000000350000000 5808310.712999999500000000 53.060847334913007000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </cityObjectMember>

</CityModel>
