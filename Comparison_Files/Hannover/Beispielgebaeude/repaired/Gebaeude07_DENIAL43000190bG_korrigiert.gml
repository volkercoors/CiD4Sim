<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<CityModel xmlns="http://www.opengis.net/citygml/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:core="http://www.opengis.net/citygml/base/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:tex="http://www.opengis.net/citygml/textures/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/citygml/2.0 http://schemas.opengis.net/citygml/2.0/cityGMLBase.xsd http://www.opengis.net/citygml/appearance/2.0  http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd">

  <gml:description>Exported by RhinoCity(TM) software - (c)Rhinoterrain 2010-2018 : www.rhinoterrain.com</gml:description>

  <gml:name>RhinoCity(TM)</gml:name>

  <gml:boundedBy>
    <gml:Envelope srsDimension="3" srsName="crs:ETRS89_UTM32*DE_DHHN92_NH">
      <gml:lowerCorner>552236.625000 5808360.000000 50.539997</gml:lowerCorner>
      <gml:upperCorner>552252.125000 5808371.500000 60.520531</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>

  <cityObjectMember>
    <bldg:Building gml:id="DENIAL43000190bG">
      <gen:doubleAttribute name="Volume">
        <gen:value>1042.252</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="TotalRoofArea">
        <gen:value>195.04</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="TotalWallArea">
        <gen:value>322.75</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Authoring_Software">
        <gen:value>RhinoCity</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="OSKA">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ALKISID">
        <gen:value>DENIAL43000190bG</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="KACHEL_ID">
        <gen:value>11_13_Building</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="COUNT">
        <gen:value>843</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="DESC">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCK">
        <gen:value>11</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCKNR">
        <gen:value>212011</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STATBEZNR">
        <gen:value>212</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="MBZNR">
        <gen:value>2107</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="WBZNR">
        <gen:value>2107</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNAM">
        <gen:value>Sahlkamp</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNR">
        <gen:value>21</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNAM">
        <gen:value>Bothfeld-Vahrenheide</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNR">
        <gen:value>03</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEMEINDE">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SCALE">
        <gen:value>5000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_overlaps">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_xmax">
        <gen:value>552252.0870000003</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_xmin">
        <gen:value>552236.6789999995</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_ymax">
        <gen:value>5808371.194</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_ymin">
        <gen:value>5808360.2190000005</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_zmax">
        <gen:value>50.67976399999857</gen:value>
      </gen:stringAttribute>
      <gen:doubleAttribute name="ZMIN">
        <gen:value>50.540000</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="THICKNESS">
        <gen:value>0.100000</gen:value>
      </gen:doubleAttribute>
      <gen:intAttribute name="TEXTURE">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="BUILDINGID">
        <gen:value>DENIAL43000190bG</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ARTBEZEICH">
        <gen:value>Gebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Join_Cou_1">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="FUNKTION_T">
        <gen:value>Wohngebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADT">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="OVERHANG">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="OBJFLAECHE">
        <gen:value>144.52</gen:value>
      </gen:stringAttribute>
      <bldg:measuredHeight uom="#m">15.916</bldg:measuredHeight>
      <bldg:lod2Solid>
        <gml:Solid gml:id="64ea6ac6-996e-41b4-a28e-0c7f92a3a94f">
          <gml:exterior>
            <gml:CompositeSurface gml:id="5be8226d-a7f9-4ccd-9b55-2a8af5fee373">
              <gml:surfaceMember xlink:href="#db85ad1c-1671-4814-a723-a5d87dc66e74"/>
              <gml:surfaceMember xlink:href="#31c8e8d4-305d-49cc-b49e-90c946d98d0b"/>
              <gml:surfaceMember xlink:href="#428edc26-a3d4-4dc3-b138-ad4068610120"/>
              <gml:surfaceMember xlink:href="#0641ad53-c889-4fa8-8fec-75dff1bc001e"/>
              <gml:surfaceMember xlink:href="#856b2a2d-102c-49ce-be2f-29a6c916234c"/>
              <gml:surfaceMember xlink:href="#a0b1df1f-0d91-480d-a372-8360820c836e"/>
              <gml:surfaceMember xlink:href="#818a55d3-8679-4f2f-9723-1c0e6a37c471"/>
              <gml:surfaceMember xlink:href="#5d6e2f87-6bbf-4b03-a52c-a0f198a5bf07"/>
              <gml:surfaceMember xlink:href="#bcbb41a8-c5a4-49c5-b147-4dbea5d43b15"/>
              <gml:surfaceMember xlink:href="#efea1066-3422-48eb-bfe9-3214b74ece52"/>
              <gml:surfaceMember xlink:href="#10e85135-2036-41be-a647-a67fad6322c4"/>
              <gml:surfaceMember xlink:href="#489d487f-8d29-431b-9796-efba06d1bf11"/>
              <gml:surfaceMember xlink:href="#4883bfc8-6ceb-4733-9871-9d8b9c8920c8"/>
              <gml:surfaceMember xlink:href="#e987bec1-b90b-4187-bc3d-31f8b16d23d0"/>
              <gml:surfaceMember xlink:href="#39e5c469-3fc6-4435-8e7b-7feb2c3edfca"/>
              <gml:surfaceMember xlink:href="#520115a7-2717-4c0c-ba7d-257357172e37"/>
              <gml:surfaceMember xlink:href="#3f0c74f2-2906-47f3-9a16-a1468f49acda"/>
              <gml:surfaceMember xlink:href="#3eda62f6-5058-4a45-ad3a-3082b49c3846"/>
              <gml:surfaceMember xlink:href="#a8cd2a1b-cf52-49cf-800a-7d57b1ccb623"/>
              <gml:surfaceMember xlink:href="#d714c0a6-81c8-4167-9107-50d2aa9e9d5c"/>
              <gml:surfaceMember xlink:href="#6fee1919-af01-4a16-b6bf-55369350aefb"/>
              <gml:surfaceMember xlink:href="#564ca6f2-f87f-456c-97f7-a74b3ee73b39"/>
              <gml:surfaceMember xlink:href="#2a4d3fea-f5d7-4295-9782-91f30d9c92e0"/>
              <gml:surfaceMember xlink:href="#7f764d81-2a00-4660-90c1-2a11163602ea"/>
              <gml:surfaceMember xlink:href="#4cd500ec-2a08-4fb5-adbc-40a9cde555ce"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="ad590712-8941-4062-9496-abf28ef05abb">
          <gen:stringAttribute name="Type">
            <gen:value>Ground</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>144.52</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="9f0a976a-cedf-41cb-b3d3-9102550d02ab" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="db85ad1c-1671-4814-a723-a5d87dc66e74">
                  <gml:exterior>
                    <gml:LinearRing gml:id="d0946e5e-af6e-4b9e-8581-7f95e22b1410">
                      <gml:posList>552247.236427880360000000 5808369.568057778300000000 50.539999999999999000 552251.987999999900000000 5808369.634999999800000000 50.539999999999999000 552252.022477926100000000 5808366.431338974300000000 50.539999999999992000 552252.058502694010000000 5808363.083946664800000000 50.539999999999999000 552252.087000000290000000 5808360.436000000700000000 50.539999999999999000 552247.358652588450000000 5808360.368972864900000000 50.540000000000006000 552236.779000000100000000 5808360.219000000500000000 50.540000000000006000 552236.729135237520000000 5808364.806558134000000000 50.539999999999978000 552236.678999999540000000 5808369.418999999800000000 50.539999999999992000 552240.069000000130000000 5808369.467000000200000000 50.540000000000006000 552242.609000000170000000 5808369.503000000500000000 50.539999999999992000 552242.606251531050000000 5808369.693102445500000000 50.539999999999992000 552242.584999999960000000 5808371.163000000600000000 50.539999999999999000 552244.794999999930000000 5808371.194000000100000000 50.539999999999999000 552244.816275187880000000 5808369.722466181000000000 50.539999999999999000 552244.819000000130000000 5808369.534000000000000000 50.539999999999999000 552247.236427880360000000 5808369.568057778300000000 50.539999999999999000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="04705b49-b136-4105-89d6-240377bd7625">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>6.30</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>269.17</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="dca94ac3-345a-433a-9807-581e87c0dacd" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="31c8e8d4-305d-49cc-b49e-90c946d98d0b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="8d48b400-9b95-4cfe-a6fb-bc1ee01b7331">
                      <gml:posList>552242.606251531050000000 5808369.693102444500000000 55.000000000000000000 552242.584999999960000000 5808371.163000000600000000 54.657099249069567000 552242.584999999960000000 5808371.163000000600000000 50.539999999999999000 552242.606251531050000000 5808369.693102445500000000 50.539999999999992000 552242.606251531050000000 5808369.693102444500000000 55.000000000000000000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="7fb98fe5-91d0-489a-b819-640a8baab27b">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>9.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>359.20</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="2f0547fb-2c42-490e-9dc9-9118290e6d09" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="428edc26-a3d4-4dc3-b138-ad4068610120">
                  <gml:exterior>
                    <gml:LinearRing gml:id="25ecbe11-7171-4f6d-aae5-d91da08aff86">
                      <gml:posList>552244.794999999930000000 5808371.194000000100000000 50.539999999999999000 552242.584999999960000000 5808371.163000000600000000 50.539999999999999000 552242.584999999960000000 5808371.163000000600000000 54.657099249069567000 552244.794999999930000000 5808371.194000000100000000 54.656717537983965000 552244.794999999930000000 5808371.194000000100000000 50.539999999999999000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="3af1e889-8976-4eab-943b-bad08715a37d">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>6.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>89.17</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="24e8e052-a43b-457a-be42-ec09e112fbba" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="0641ad53-c889-4fa8-8fec-75dff1bc001e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="6994f2c8-140e-493b-91b1-67a3b511791e">
                      <gml:posList>552244.816275187880000000 5808369.722466181000000000 50.539999999999999000 552244.794999999930000000 5808371.194000000100000000 50.539999999999999000 552244.794999999930000000 5808371.194000000100000000 54.656717537983965000 552244.816275187880000000 5808369.722466180100000000 55.000000000000000000 552244.816275187880000000 5808369.722466181000000000 50.539999999999999000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="e8c877ab-8b69-474f-8874-a039e59268cf">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>24.88</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>359.19</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="e794275c-544f-45b5-9a2f-5ab6c34bcc16" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="856b2a2d-102c-49ce-be2f-29a6c916234c">
                  <gml:exterior>
                    <gml:LinearRing gml:id="ed7a61db-ee2a-49eb-a2ad-b3d1f0367db7">
                      <gml:posList>552251.987999999900000000 5808369.634999999800000000 50.539999999999999000 552247.236427880360000000 5808369.568057778300000000 50.539999999999999000 552247.236427880360000000 5808369.568057778300000000 55.776555685584569000 552251.987999999900000000 5808369.634999999800000000 55.772645770047305000 552251.987999999900000000 5808369.634999999800000000 50.539999999999999000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="f6d2e4ec-eba9-453d-b1a4-45f1c6d2c017">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>48.02</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>89.38</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="f0a2e581-c62d-4bfa-8f4c-b75f398576ad" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="a0b1df1f-0d91-480d-a372-8360820c836e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="7f3ceb60-03ea-4f9f-b751-9999ac51cf0a">
                      <gml:posList>552252.058502694010000000 5808363.083946664800000000 55.122966766357422000 552252.058502694010000000 5808363.083946664800000000 50.539999999999999000 552252.022477926100000000 5808366.431338974300000000 50.539999999999992000 552251.987999999900000000 5808369.634999999800000000 50.539999999999999000 552251.987999999900000000 5808369.634999999800000000 55.772645770047305000 552252.022477926100000000 5808366.431338974300000000 59.061938414928754000 552252.058502694010000000 5808363.083946664800000000 57.545051574707031000 552252.058502694010000000 5808363.083946664800000000 55.122966766357422000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="6b4ccbf6-248c-4f58-86fa-e81daf818a55">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>11.47</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>179.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="0c07eca4-20b7-485c-bae4-e572dad166d5" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="818a55d3-8679-4f2f-9723-1c0e6a37c471">
                  <gml:exterior>
                    <gml:LinearRing gml:id="4a9f6ca6-e8c4-4278-b19a-872d41bcc46c">
                      <gml:posList>552252.058502694010000000 5808363.083946664800000000 57.545051574707031000 552247.323415679740000000 5808363.021033382000000000 57.545051574707031000 552247.323415678930000000 5808363.021033382000000000 55.122966766357422000 552252.058502694010000000 5808363.083946664800000000 55.122966766357422000 552252.058502694010000000 5808363.083946664800000000 57.545051574707031000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="f78dcb3f-35d9-40b0-b422-7ee2cbfd6424">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>21.67</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>179.19</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="e568b834-18ea-43c3-8216-57ef3a7a8411" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="5d6e2f87-6bbf-4b03-a52c-a0f198a5bf07">
                  <gml:exterior>
                    <gml:LinearRing gml:id="e8782ad0-d3b7-445f-9c21-5e785f4b9bae">
                      <gml:posList>552252.087000000290000000 5808360.436000000700000000 55.122966766357422000 552247.358652588450000000 5808360.368972864900000000 55.122966766357422000 552247.358652588450000000 5808360.368972864900000000 50.540000000000006000 552252.087000000290000000 5808360.436000000700000000 50.539999999999999000 552252.087000000290000000 5808360.436000000700000000 55.122966766357422000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="531cb158-c17e-4341-a613-4a838d0d3d3f">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>12.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>89.38</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="0a13325d-d506-4d0d-b2b3-0a19e071d1cb" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bcbb41a8-c5a4-49c5-b147-4dbea5d43b15">
                  <gml:exterior>
                    <gml:LinearRing gml:id="dae675dc-880b-4bbf-b139-7a01fb90ad65">
                      <gml:posList>552252.087000000290000000 5808360.436000000700000000 50.539999999999999000 552252.058502694010000000 5808363.083946664800000000 50.539999999999999000 552252.058502694010000000 5808363.083946664800000000 55.122966766357422000 552252.087000000290000000 5808360.436000000700000000 55.122966766357422000 552252.087000000290000000 5808360.436000000700000000 50.539999999999999000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="d0520d56-0895-421b-9b88-b75c7cead527">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>1.49</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>89.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="e7269ca8-f9e6-4937-ab2d-cdd00919a574" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="efea1066-3422-48eb-bfe9-3214b74ece52">
                  <gml:exterior>
                    <gml:LinearRing gml:id="0a7e7405-1749-4fd4-b50d-a0fa231a5096">
                      <gml:posList>552247.297826362540000000 5808364.946980274300000000 58.417519973705751000 552247.278941654600000000 5808366.368313429900000000 59.061394562254407000 552247.297826362310000000 5808364.946980274300000000 60.520526885986328000 552247.297826362540000000 5808364.946980274300000000 58.417519973705751000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="e3ff74ee-a1a3-4f92-bacf-65503d2a4b24">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>55.82</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>179.19</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="7ca86af0-67c6-4f86-b51f-69952e8e4993" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="10e85135-2036-41be-a647-a67fad6322c4">
                  <gml:exterior>
                    <gml:LinearRing gml:id="44a66f21-53d0-4b3e-91d2-6534924483ad">
                      <gml:posList>552247.358652588450000000 5808360.368972864900000000 55.122966766357422000 552247.358652588450000000 5808360.368972864900000000 55.820771194151980000 552236.779000000100000000 5808360.219000000500000000 55.811117693938513000 552236.779000000100000000 5808360.219000000500000000 50.540000000000006000 552247.358652588450000000 5808360.368972864900000000 50.540000000000006000 552247.358652588450000000 5808360.368972864900000000 55.122966766357422000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="5523ba2e-8b17-4c84-8d62-b1554a91d88d">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>70.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>269.38</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="2c2cd8c0-4834-41d7-a0dc-5e15791cdb4e" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="489d487f-8d29-431b-9796-efba06d1bf11">
                  <gml:exterior>
                    <gml:LinearRing gml:id="878b7230-4ce1-41cb-9086-690514613f71">
                      <gml:posList>552236.779000000100000000 5808360.219000000500000000 55.811117693938513000 552236.729135237520000000 5808364.806558134000000000 60.520526885986328000 552236.678999999540000000 5808369.418999999800000000 55.785573020498589000 552236.678999999540000000 5808369.418999999800000000 50.539999999999992000 552236.729135237520000000 5808364.806558134000000000 50.539999999999978000 552236.779000000100000000 5808360.219000000500000000 50.540000000000006000 552236.779000000100000000 5808360.219000000500000000 55.811117693938513000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="53071056-963d-49a3-a83e-ff1abfa39433">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>17.78</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>359.19</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="61ef50c3-fdec-4058-9329-c630de6518f2" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="4883bfc8-6ceb-4733-9871-9d8b9c8920c8">
                  <gml:exterior>
                    <gml:LinearRing gml:id="ee1b5f0a-9ff1-4cae-bf89-c3ccad71874e">
                      <gml:posList>552240.069000000130000000 5808369.467000000200000000 50.540000000000006000 552236.678999999540000000 5808369.418999999800000000 50.539999999999992000 552236.678999999540000000 5808369.418999999800000000 55.785573020498589000 552240.069000000130000000 5808369.467000000200000000 55.782536508729827000 552240.069000000130000000 5808369.467000000200000000 50.540000000000006000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="9bfc8aa8-ece1-442c-a8bf-6aa1ea725d35">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>13.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>359.19</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="7a973cdd-e2da-4937-8cf0-3d57fa510dee" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="e987bec1-b90b-4187-bc3d-31f8b16d23d0">
                  <gml:exterior>
                    <gml:LinearRing gml:id="10878a04-d2e4-48e8-a918-3af3a64f19be">
                      <gml:posList>552242.609000000170000000 5808369.503000000500000000 50.539999999999992000 552240.069000000130000000 5808369.467000000200000000 50.540000000000006000 552240.069000000130000000 5808369.467000000200000000 55.782536508729827000 552242.609000000170000000 5808369.503000000500000000 55.780225031010403000 552242.609000000170000000 5808369.503000000500000000 50.539999999999992000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="58108015-9d4c-4c8d-b76b-e95ea73c74f2">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.98</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>269.17</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="2d6e141e-2373-4709-b1ed-818716f6bf4d" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="39e5c469-3fc6-4435-8e7b-7feb2c3edfca">
                  <gml:exterior>
                    <gml:LinearRing gml:id="a8534426-8e28-4421-8581-59da785185e3">
                      <gml:posList>552242.609000000170000000 5808369.503000000500000000 55.780225031010403000 552242.606251531050000000 5808369.693102445500000000 55.585063934326172000 552242.606251531050000000 5808369.693102444500000000 55.000000000000000000 552242.606251531050000000 5808369.693102445500000000 50.539999999999992000 552242.609000000170000000 5808369.503000000500000000 50.539999999999992000 552242.609000000170000000 5808369.503000000500000000 55.780225031010403000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="547115ad-4a9b-4f61-a026-5f74f6e886ec">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>1.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>359.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="9b2d9ffe-09c1-4a2c-bb9e-8b8c60a5ffeb" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="520115a7-2717-4c0c-ba7d-257357172e37">
                  <gml:exterior>
                    <gml:LinearRing gml:id="1536b7d1-a95c-46f4-b84b-1240ac0b039f">
                      <gml:posList>552244.816275187880000000 5808369.722466180100000000 55.000000000000000000 552242.606251531050000000 5808369.693102444500000000 55.000000000000000000 552242.606251531050000000 5808369.693102445500000000 55.585063934326172000 552244.816275187880000000 5808369.722466181000000000 55.585063934326179000 552244.816275187880000000 5808369.722466180100000000 55.000000000000000000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="8a886b1e-6a15-4157-b2af-a7303150c8f4">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.97</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>89.17</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="09706bae-272b-41d4-b7f8-7a3b6a7a8a56" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="3f0c74f2-2906-47f3-9a16-a1468f49acda">
                  <gml:exterior>
                    <gml:LinearRing gml:id="bb518b66-a37e-4536-b24a-dadd588b8289">
                      <gml:posList>552244.819000000130000000 5808369.534000000000000000 55.778545225853605000 552244.819000000130000000 5808369.534000000000000000 50.539999999999999000 552244.816275187880000000 5808369.722466181000000000 50.539999999999999000 552244.816275187880000000 5808369.722466180100000000 55.000000000000000000 552244.816275187880000000 5808369.722466181000000000 55.585063934326179000 552244.819000000130000000 5808369.534000000000000000 55.778545225853605000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="28eddfc9-18fa-49da-b870-9c84b2a28edc">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>12.66</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>359.19</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="eda320a5-85bd-4772-8be2-ed5674753680" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="3eda62f6-5058-4a45-ad3a-3082b49c3846">
                  <gml:exterior>
                    <gml:LinearRing gml:id="2150af40-5c0e-48fd-b8e5-3c9e93038104">
                      <gml:posList>552247.236427880360000000 5808369.568057778300000000 50.539999999999999000 552244.819000000130000000 5808369.534000000000000000 50.539999999999999000 552244.819000000130000000 5808369.534000000000000000 55.778545225853605000 552247.236427880360000000 5808369.568057778300000000 55.776555685584569000 552247.236427880360000000 5808369.568057778300000000 50.539999999999999000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="e3ae3c66-9fe9-46ce-9b4c-6bb92d8930b6">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>8.45</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>89.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="e5cf9e41-ca0b-4d9a-bba8-7477ff2f4366" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="a8cd2a1b-cf52-49cf-800a-7d57b1ccb623">
                  <gml:exterior>
                    <gml:LinearRing gml:id="1ce1ea6d-8075-4555-a126-104b8b5adb33">
                      <gml:posList>552247.323415678930000000 5808363.021033382000000000 55.122966766357422000 552247.323415679740000000 5808363.021033382000000000 57.545051574707031000 552247.297826362540000000 5808364.946980274300000000 58.417519973705751000 552247.297826362310000000 5808364.946980274300000000 60.520526885986328000 552247.358652588450000000 5808360.368972864900000000 55.820771194151980000 552247.358652588450000000 5808360.368972864900000000 55.122966766357422000 552247.323415678930000000 5808363.021033382000000000 55.122966766357422000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="823874e4-4652-46a9-9885-c7556747538f">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL43000190bG_C:29123</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>70.50</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>359.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>45.75</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="e802cfac-30b4-4ad5-90f5-548d460d4a75" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="d714c0a6-81c8-4167-9107-50d2aa9e9d5c">
                  <gml:exterior>
                    <gml:LinearRing gml:id="be6bb665-0fae-42cb-9d9b-3df5a35c4dee">
                      <gml:posList>552244.816275187880000000 5808369.722466181000000000 55.585063934326179000 552242.606251531050000000 5808369.693102445500000000 55.585063934326172000 552242.609000000170000000 5808369.503000000500000000 55.780225031010403000 552240.069000000130000000 5808369.467000000200000000 55.782536508729827000 552236.678999999540000000 5808369.418999999800000000 55.785573020498589000 552236.729135237520000000 5808364.806558134000000000 60.520526885986328000 552247.297826362310000000 5808364.946980274300000000 60.520526885986328000 552247.278941654600000000 5808366.368313429900000000 59.061394562254407000 552247.236427880360000000 5808369.568057778300000000 55.776555685584569000 552244.819000000130000000 5808369.534000000000000000 55.778545225853605000 552244.816275187880000000 5808369.722466181000000000 55.585063934326179000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="8a26298b-9c45-4562-acb9-e842a6516978">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL43000190bG_C:29123</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>69.46</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>179.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>45.75</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="f559b7d5-3833-407b-aa70-fdb430e403bb" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="6fee1919-af01-4a16-b6bf-55369350aefb">
                  <gml:exterior>
                    <gml:LinearRing gml:id="3ec1ad30-7247-46f0-864a-87df9625fc8b">
                      <gml:posList>552247.297826362310000000 5808364.946980274300000000 60.520526885986328000 552236.729135237520000000 5808364.806558134000000000 60.520526885986328000 552236.779000000100000000 5808360.219000000500000000 55.811117693938513000 552247.358652588450000000 5808360.368972864900000000 55.820771194151980000 552247.297826362310000000 5808364.946980274300000000 60.520526885986328000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="ed3d6081-eb95-4bdc-a6ee-864292a6b0b5">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL43000190bG_C:29120</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>3.34</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>359.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>13.13</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="5638d7f6-7fae-4a95-aa31-6ebfb3c846da" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="564ca6f2-f87f-456c-97f7-a74b3ee73b39">
                  <gml:exterior>
                    <gml:LinearRing gml:id="6014c059-9ce8-4342-8ce2-8163379beb5b">
                      <gml:posList>552244.794999999930000000 5808371.194000000100000000 54.656717537983965000 552242.584999999960000000 5808371.163000000600000000 54.657099249069567000 552242.606251531050000000 5808369.693102444500000000 55.000000000000000000 552244.816275187880000000 5808369.722466180100000000 55.000000000000000000 552244.794999999930000000 5808371.194000000100000000 54.656717537983965000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="e22a53ed-61b0-4aca-a452-f55d629e1dc8">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL43000190bG_C:29122</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>17.42</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>179.25</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>24.37</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="3214e7a1-daf2-442d-8617-ce10b7c23fd0" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="2a4d3fea-f5d7-4295-9782-91f30d9c92e0">
                  <gml:exterior>
                    <gml:LinearRing gml:id="6f6928ed-2e84-4b67-98f3-c215f311707e">
                      <gml:posList>552252.058502694010000000 5808363.083946664800000000 57.545051574707031000 552252.022477926100000000 5808366.431338974300000000 59.061938414928754000 552247.278941654600000000 5808366.368313429900000000 59.061394562254407000 552247.297826362540000000 5808364.946980274300000000 58.417519973705751000 552247.323415679740000000 5808363.021033382000000000 57.545051574707031000 552252.058502694010000000 5808363.083946664800000000 57.545051574707031000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="73616cdd-4cf5-44da-a1dc-4ac03224577d">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL43000190bG_C:29122</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>21.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>359.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>45.75</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="acc6c874-1104-4edb-8a9b-759007307fcb" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="7f764d81-2a00-4660-90c1-2a11163602ea">
                  <gml:exterior>
                    <gml:LinearRing gml:id="96a6eebe-5239-4f11-b9f8-a8d06eec5a2a">
                      <gml:posList>552251.987999999900000000 5808369.634999999800000000 55.772645770047305000 552247.236427880360000000 5808369.568057778300000000 55.776555685584569000 552247.278941654600000000 5808366.368313429900000000 59.061394562254407000 552252.022477926100000000 5808366.431338974300000000 59.061938414928754000 552251.987999999900000000 5808369.634999999800000000 55.772645770047305000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="5f7bfe3d-0c12-4119-80d1-e600ad2d3e6c">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL43000190bG_C:29121</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>12.54</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="31740d64-cec1-4142-9563-c7876f7ff545" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="4cd500ec-2a08-4fb5-adbc-40a9cde555ce">
                  <gml:exterior>
                    <gml:LinearRing gml:id="c579cef9-c519-4dd6-818a-f6e1691d620a">
                      <gml:posList>552252.058502694010000000 5808363.083946664800000000 55.122966766357422000 552247.323415678930000000 5808363.021033382000000000 55.122966766357422000 552247.358652588450000000 5808360.368972864900000000 55.122966766357422000 552252.087000000290000000 5808360.436000000700000000 55.122966766357422000 552252.058502694010000000 5808363.083946664800000000 55.122966766357422000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </cityObjectMember>

</CityModel>
