<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<CityModel xmlns="http://www.opengis.net/citygml/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:core="http://www.opengis.net/citygml/base/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:tex="http://www.opengis.net/citygml/textures/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/citygml/2.0 http://schemas.opengis.net/citygml/2.0/cityGMLBase.xsd http://www.opengis.net/citygml/appearance/2.0  http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd">

  <gml:description>Exported by RhinoCity(TM) software - (c)Rhinoterrain 2010-2018 : www.rhinoterrain.com</gml:description>

  <gml:name>RhinoCity(TM)</gml:name>

  <gml:boundedBy>
    <gml:Envelope srsDimension="3" srsName="crs:ETRS89_UTM32*DE_DHHN92_NH">
      <gml:lowerCorner>551996.812500 5801768.500000 54.649998</gml:lowerCorner>
      <gml:upperCorner>552033.625000 5801825.000000 63.350368</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>

  <cityObjectMember>
    <bldg:Building gml:id="DENIAL430001ls7C">
      <gen:doubleAttribute name="Volume">
        <gen:value>6645.379</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="TotalRoofArea">
        <gen:value>784.99</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="TotalWallArea">
        <gen:value>1350.96</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Authoring_Software">
        <gen:value>RhinoCity</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="OSKA">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ALKISID">
        <gen:value>DENIAL430001ls7C</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="KACHEL_ID">
        <gen:value>11_06_Building</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="COUNT">
        <gen:value>843</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="DESC">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCK">
        <gen:value>24</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCKNR">
        <gen:value>043024</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STATBEZNR">
        <gen:value>043</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="MBZNR">
        <gen:value>0417</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="WBZNR">
        <gen:value>0416</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNAM">
        <gen:value>Südstadt</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNR">
        <gen:value>04</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNAM">
        <gen:value>Südstadt-Bult</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNR">
        <gen:value>07</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEMEINDE">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SCALE">
        <gen:value>5000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_overlaps">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_xmax">
        <gen:value>552033.5700000003</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_xmin">
        <gen:value>551996.875</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_ymax">
        <gen:value>5801824.67</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_ymin">
        <gen:value>5801768.879000001</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_zmax">
        <gen:value>55.709176000021394</gen:value>
      </gen:stringAttribute>
      <gen:doubleAttribute name="ZMIN">
        <gen:value>54.650000</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="THICKNESS">
        <gen:value>0.100000</gen:value>
      </gen:doubleAttribute>
      <gen:intAttribute name="TEXTURE">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="BUILDINGID">
        <gen:value>DENIAL430001ls7C</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ARTBEZEICH">
        <gen:value>Gebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Join_Cou_1">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="FUNKTION_T">
        <gen:value>Wohngebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADT">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="OVERHANG">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="OBJFLAECHE">
        <gen:value>784.99</gen:value>
      </gen:stringAttribute>
      <bldg:measuredHeight uom="#m">9.700</bldg:measuredHeight>
      <bldg:lod2Solid>
        <gml:Solid gml:id="d88205a1-43f9-4b8d-b6ca-f5065b4bf6d1">
          <gml:exterior>
            <gml:CompositeSurface gml:id="b44e07c3-4059-4189-8677-a44e8d84f00d">
              <gml:surfaceMember xlink:href="#9caf6ff2-ae8f-47ec-be54-a43ffadfbcb6"/>
              <gml:surfaceMember xlink:href="#79ea9de2-9be4-4c07-b7b7-6c3120b2ea71"/>
              <gml:surfaceMember xlink:href="#e69fd1b8-6d6d-4974-b17c-0636b0d95d56"/>
              <gml:surfaceMember xlink:href="#3880ca2b-6c06-4b73-b4df-1f25be3203b2"/>
              <gml:surfaceMember xlink:href="#29a0840d-78f5-45ac-9e54-37abdc22f95a"/>
              <gml:surfaceMember xlink:href="#f2505a3b-47a7-4933-a293-2524ef19630e"/>
              <gml:surfaceMember xlink:href="#4d286961-7b59-4b13-a38e-333ceb6a148a"/>
              <gml:surfaceMember xlink:href="#4a724bd8-9524-449c-bf99-2e50d47a4601"/>
              <gml:surfaceMember xlink:href="#10be7b6b-dc74-4bc7-ac91-62ffa6a31403"/>
              <gml:surfaceMember xlink:href="#2d9bc1cd-cc10-4b6a-a713-2cb5a764c7c8"/>
              <gml:surfaceMember xlink:href="#9474bf85-8f54-42d3-99ca-4ff6a2b4e94f"/>
              <gml:surfaceMember xlink:href="#c02ec2c5-9a98-421c-9394-19fd245375b5"/>
              <gml:surfaceMember xlink:href="#0c8b281b-a1b9-4168-96b6-be9224dcc784"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="548c20a9-cbcc-41ab-838e-24633c1e06db">
          <gen:stringAttribute name="Type">
            <gen:value>Ground</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>784.99</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="f7be98f7-d244-4dd8-b05f-f0fb732bd59c" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="9caf6ff2-ae8f-47ec-be54-a43ffadfbcb6">
                  <gml:exterior>
                    <gml:LinearRing gml:id="73799291-9667-4660-9dbb-7a8069c3bf29">
                      <gml:posList>552013.895999999720000000 5801805.848999999500000000 54.650000000000006000 552033.379999999890000000 5801778.455000000100000000 54.650000000000006000 552031.809999999590000000 5801777.337999999500000000 54.650000000000006000 552033.570000000300000000 5801774.755999999100000000 54.650000000000006000 552024.912999999710000000 5801768.879000000700000000 54.650000000000006000 551996.875000000000000000 5801810.042999999600000000 54.650000000000006000 552021.018000000160000000 5801824.669999999900000000 54.650000000000006000 552027.610999999570000000 5801814.947000000600000000 54.650000000000006000 552024.964999999850000000 5801813.202999999700000000 54.650000000000006000 552024.873999999840000000 5801813.335000000900000000 54.650000000000006000 552022.025000000370000000 5801811.392000000900000000 54.649999999999991000 552013.895999999720000000 5801805.848999999500000000 54.650000000000006000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="9f173be0-8ee2-4d06-bfdc-46bd2446f170">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>102.21</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>55.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="e1df772d-4f22-48cd-81c6-9f676d7be58c" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="79ea9de2-9be4-4c07-b7b7-6c3120b2ea71">
                  <gml:exterior>
                    <gml:LinearRing gml:id="b01ecc06-34a3-4140-b431-c04013062eee">
                      <gml:posList>552027.610999999570000000 5801814.947000000600000000 54.650000000000006000 552021.018000000160000000 5801824.669999999900000000 54.650000000000006000 552021.018000000160000000 5801824.669999999900000000 63.350364685058594000 552027.610999999570000000 5801814.947000000600000000 63.350364685058594000 552027.610999999570000000 5801814.947000000600000000 54.650000000000006000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="80059876-1a84-4198-8e3f-6d35c4b5cdcd">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>27.57</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>146.61</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="6cced058-72ba-45be-9307-30e03b91a2db" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="e69fd1b8-6d6d-4974-b17c-0636b0d95d56">
                  <gml:exterior>
                    <gml:LinearRing gml:id="39c2ad7a-01e6-4581-9b4d-9526ccafbe2f">
                      <gml:posList>552027.610999999570000000 5801814.947000000600000000 63.350364685058594000 552024.964999999850000000 5801813.202999999700000000 63.350364685058594000 552024.964999999850000000 5801813.202999999700000000 54.650000000000006000 552027.610999999570000000 5801814.947000000600000000 54.650000000000006000 552027.610999999570000000 5801814.947000000600000000 63.350364685058594000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="1ae16ab8-76f1-4bf8-abfc-148fa34e92a2">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>1.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>235.42</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="310c9a85-8d76-46ea-9b13-b4243b4f8d33" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="3880ca2b-6c06-4b73-b4df-1f25be3203b2">
                  <gml:exterior>
                    <gml:LinearRing gml:id="848a843f-802c-4b23-8888-840cd67fd1a3">
                      <gml:posList>552024.964999999850000000 5801813.202999999700000000 63.350364685058594000 552024.873999999840000000 5801813.335000000900000000 63.350364685058594000 552024.873999999840000000 5801813.335000000900000000 54.650000000000006000 552024.964999999850000000 5801813.202999999700000000 54.650000000000006000 552024.964999999850000000 5801813.202999999700000000 63.350364685058594000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="6fc2c705-2fbd-422b-a459-f5ec391d0980">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>119.93</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>145.71</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="84f6fa3d-f1b7-42fd-9236-8643f3ddec83" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="29a0840d-78f5-45ac-9e54-37abdc22f95a">
                  <gml:exterior>
                    <gml:LinearRing gml:id="617bc47e-6711-4c9b-a534-a3c76bd66953">
                      <gml:posList>552024.873999999840000000 5801813.335000000900000000 54.650000000000006000 552024.873999999840000000 5801813.335000000900000000 63.350364685058594000 552022.025000000370000000 5801811.392000000900000000 63.350364685058594000 552013.895999999720000000 5801805.848999999500000000 63.350364685058594000 552012.858110078260000000 5801805.141283941500000000 63.350364685058594000 552012.858110078260000000 5801805.141283941500000000 59.906620025634766000 552013.895999999720000000 5801805.848999999500000000 59.906620025634766000 552013.895999999720000000 5801805.848999999500000000 54.650000000000006000 552022.025000000370000000 5801811.392000000900000000 54.649999999999991000 552024.873999999840000000 5801813.335000000900000000 54.650000000000006000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="5cef5403-5de1-485c-a95d-89490a2908b8">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>143.06</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>55.72</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="71beeb36-93be-4dfb-9d49-128ed8393a2f" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="f2505a3b-47a7-4933-a293-2524ef19630e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="7b899ecb-684b-400c-8d2c-72e307937300">
                      <gml:posList>552033.570000000300000000 5801774.755999999100000000 54.650000000000006000 552031.809999999590000000 5801777.337999999500000000 54.650000000000006000 552031.809999999590000000 5801777.337999999500000000 59.906620025634766000 552012.858110078260000000 5801805.141283941500000000 59.906620025634766000 552012.858110078260000000 5801805.141283941500000000 63.350364685058594000 552031.809999999590000000 5801777.337999999500000000 63.350364685058594000 552033.570000000300000000 5801774.755999999100000000 63.350364685058594000 552033.570000000300000000 5801774.755999999100000000 54.650000000000006000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="51ba3842-20f3-488e-a162-ee2d6d1eebb4">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>91.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>145.83</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="6cbaedf1-d465-4c2e-9eb0-2b7147e10d9c" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="4d286961-7b59-4b13-a38e-333ceb6a148a">
                  <gml:exterior>
                    <gml:LinearRing gml:id="e8546cd1-6111-4d63-a10e-c069da8324d9">
                      <gml:posList>552033.570000000300000000 5801774.755999999100000000 63.350364685058594000 552024.912999999710000000 5801768.879000000700000000 63.350364685058594000 552024.912999999710000000 5801768.879000000700000000 54.650000000000006000 552033.570000000300000000 5801774.755999999100000000 54.650000000000006000 552033.570000000300000000 5801774.755999999100000000 63.350364685058594000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="d65ab564-09fa-4989-97be-df802595ba9a">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>433.33</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>235.74</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="4c0c0db4-f43f-45b0-9a60-e0a9cf28ed4c" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="4a724bd8-9524-449c-bf99-2e50d47a4601">
                  <gml:exterior>
                    <gml:LinearRing gml:id="818eba2a-04c6-4010-8b57-b55929825d9f">
                      <gml:posList>552024.912999999710000000 5801768.879000000700000000 63.350364685058594000 551996.875000000000000000 5801810.042999999600000000 63.350364685058594000 551996.875000000000000000 5801810.042999999600000000 54.650000000000006000 552024.912999999710000000 5801768.879000000700000000 54.650000000000006000 552024.912999999710000000 5801768.879000000700000000 63.350364685058594000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="1bdd6cdd-40a2-460e-bb34-7daba49247c6">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>245.60</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>328.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="672c4f3e-af92-4bbc-994f-c3b235e1802c" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="10be7b6b-dc74-4bc7-ac91-62ffa6a31403">
                  <gml:exterior>
                    <gml:LinearRing gml:id="d79113c2-b18d-4eaf-81cf-52fb253fe115">
                      <gml:posList>552021.018000000160000000 5801824.669999999900000000 54.650000000000006000 551996.875000000000000000 5801810.042999999600000000 54.650000000000006000 551996.875000000000000000 5801810.042999999600000000 63.350364685058594000 552021.018000000160000000 5801824.669999999900000000 63.350364685058594000 552021.018000000160000000 5801824.669999999900000000 54.650000000000006000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="570aa5d0-9728-4ee3-8b62-171195ec529b">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>176.71</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>54.58</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="4b6fee04-15ab-4691-9845-6466523a3c20" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="2d9bc1cd-cc10-4b6a-a713-2cb5a764c7c8">
                  <gml:exterior>
                    <gml:LinearRing gml:id="6a728fb7-e4a5-4b31-8e55-87645878e2f3">
                      <gml:posList>552033.379999999890000000 5801778.455000000100000000 54.650000000000006000 552013.895999999720000000 5801805.848999999500000000 54.650000000000006000 552013.895999999720000000 5801805.848999999500000000 59.906620025634766000 552033.379999999890000000 5801778.455000000100000000 59.906620025634766000 552033.379999999890000000 5801778.455000000100000000 54.650000000000006000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="4282c6a4-a875-405e-b5ff-943001bea16b">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>10.13</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>144.57</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="d7f1590c-03bc-4e99-a665-54e2785361d7" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="9474bf85-8f54-42d3-99ca-4ff6a2b4e94f">
                  <gml:exterior>
                    <gml:LinearRing gml:id="5a51a286-0e7e-4793-a115-df6788670892">
                      <gml:posList>552033.379999999890000000 5801778.455000000100000000 59.906620025634766000 552031.809999999590000000 5801777.337999999500000000 59.906620025634766000 552031.809999999590000000 5801777.337999999500000000 54.650000000000006000 552033.379999999890000000 5801778.455000000100000000 54.650000000000006000 552033.379999999890000000 5801778.455000000100000000 59.906620025634766000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="6cfb5d90-cd31-49b9-8e03-2b8522000bea">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001ls7C_C:29176</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>731.47</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="f46e4817-96bc-429b-9aa1-71cc579a83e9" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="c02ec2c5-9a98-421c-9394-19fd245375b5">
                  <gml:exterior>
                    <gml:LinearRing gml:id="89d33450-d211-4be2-a83b-dae5ded62368">
                      <gml:posList>552013.895999999720000000 5801805.848999999500000000 63.350364685058594000 552022.025000000370000000 5801811.392000000900000000 63.350364685058594000 552024.873999999840000000 5801813.335000000900000000 63.350364685058594000 552024.964999999850000000 5801813.202999999700000000 63.350364685058594000 552027.610999999570000000 5801814.947000000600000000 63.350364685058594000 552021.018000000160000000 5801824.669999999900000000 63.350364685058594000 551996.875000000000000000 5801810.042999999600000000 63.350364685058594000 552024.912999999710000000 5801768.879000000700000000 63.350364685058594000 552033.570000000300000000 5801774.755999999100000000 63.350364685058594000 552031.809999999590000000 5801777.337999999500000000 63.350364685058594000 552012.858110078260000000 5801805.141283941500000000 63.350364685058594000 552013.895999999720000000 5801805.848999999500000000 63.350364685058594000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="4eeaf711-3014-40d9-9bc5-69ab1cd9c34c">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001ls7C_C:29175</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>53.52</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="9b699b15-aa64-471a-aea3-be6bb2a65ecf" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="0c8b281b-a1b9-4168-96b6-be9224dcc784">
                  <gml:exterior>
                    <gml:LinearRing gml:id="76b9ddd1-bd8d-4b6a-97dd-35c3157f81ab">
                      <gml:posList>552033.379999999890000000 5801778.455000000100000000 59.906620025634766000 552013.895999999720000000 5801805.848999999500000000 59.906620025634766000 552012.858110078260000000 5801805.141283941500000000 59.906620025634766000 552031.809999999590000000 5801777.337999999500000000 59.906620025634766000 552033.379999999890000000 5801778.455000000100000000 59.906620025634766000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </cityObjectMember>

</CityModel>
