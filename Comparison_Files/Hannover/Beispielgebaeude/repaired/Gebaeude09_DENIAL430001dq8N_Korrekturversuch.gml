<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<CityModel xmlns="http://www.opengis.net/citygml/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:core="http://www.opengis.net/citygml/base/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:tex="http://www.opengis.net/citygml/textures/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/citygml/2.0 http://schemas.opengis.net/citygml/2.0/cityGMLBase.xsd http://www.opengis.net/citygml/appearance/2.0  http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd">

  <gml:description>Exported by RhinoCity(TM) software - (c)Rhinoterrain 2010-2018 : www.rhinoterrain.com</gml:description>

  <gml:name>RhinoCity(TM)</gml:name>

  <gml:boundedBy>
    <gml:Envelope srsDimension="3" srsName="crs:ETRS89_UTM32*DE_DHHN92_NH">
      <gml:lowerCorner>552165.375000 5805804.500000 52.849998</gml:lowerCorner>
      <gml:upperCorner>552227.437500 5805849.000000 79.874008</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>

  <cityObjectMember>
    <bldg:Building gml:id="DENIAL430001dq8N">
      <gen:doubleAttribute name="Volume">
        <gen:value>19590.378</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="TotalRoofArea">
        <gen:value>867.40</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="TotalWallArea">
        <gen:value>4156.05</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Authoring_Software">
        <gen:value>RhinoCity</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="OSKA">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ALKISID">
        <gen:value>DENIAL430001dq8N</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="KACHEL_ID">
        <gen:value>11_10_Building</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="COUNT">
        <gen:value>850</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="DESC">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCK">
        <gen:value>45</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCKNR">
        <gen:value>103045</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STATBEZNR">
        <gen:value>103</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="MBZNR">
        <gen:value>1022</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="WBZNR">
        <gen:value>1022</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNAM">
        <gen:value>List</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNR">
        <gen:value>10</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNAM">
        <gen:value>Vahrenwald-List</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNR">
        <gen:value>02</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEMEINDE">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SCALE">
        <gen:value>5000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_overlaps">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_xmax">
        <gen:value>552227.3949999996</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_xmin">
        <gen:value>552165.4019999998</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_ymax">
        <gen:value>5805848.7190000005</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_ymin">
        <gen:value>5805804.960000001</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_zmax">
        <gen:value>53.50074200000242</gen:value>
      </gen:stringAttribute>
      <gen:doubleAttribute name="ZMIN">
        <gen:value>52.850000</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="THICKNESS">
        <gen:value>0.100000</gen:value>
      </gen:doubleAttribute>
      <gen:intAttribute name="TEXTURE">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="BUILDINGID">
        <gen:value>DENIAL430001dq8N</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ARTBEZEICH">
        <gen:value>Gebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Join_Cou_1">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="FUNKTION_T">
        <gen:value>Wohngebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADT">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="OVERHANG">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="OBJFLAECHE">
        <gen:value>867.4</gen:value>
      </gen:stringAttribute>
      <bldg:measuredHeight uom="#m">28.024</bldg:measuredHeight>
      <bldg:lod2Solid>
        <gml:Solid gml:id="4d52b3f2-192b-4e0e-b344-48ed5a1f2cd8">
          <gml:exterior>
            <gml:CompositeSurface gml:id="a9c87bdf-bf44-4895-a9f3-da1fdfdb025c">
              <gml:surfaceMember xlink:href="#2011f315-2001-4838-ac5d-488337677e1b"/>
              <gml:surfaceMember xlink:href="#2c75009c-ad1d-423e-95af-77aa830f06a5"/>
              <gml:surfaceMember xlink:href="#4a6c8fca-6cdc-468e-b036-19111e407c77"/>
              <gml:surfaceMember xlink:href="#574cfcb0-8852-4c17-b6b9-486fd69c99d8"/>
              <gml:surfaceMember xlink:href="#127276c9-38df-423a-88a0-a1100ea3606a"/>
              <gml:surfaceMember xlink:href="#385d6155-107d-4a5a-b2ac-43991414d920"/>
              <gml:surfaceMember xlink:href="#41e2be95-78ea-4027-8a82-eda9d626f007"/>
              <gml:surfaceMember xlink:href="#e1454db0-c26a-435a-8ae1-d68c8e565c2a"/>
              <gml:surfaceMember xlink:href="#8cca5956-9d72-4c41-8127-5958f4f05792"/>
              <gml:surfaceMember xlink:href="#e7c85a7b-c488-49cc-9f92-1bde7384cc9f"/>
              <gml:surfaceMember xlink:href="#f2de26f5-a94b-4dfc-a10a-469fee05cd62"/>
              <gml:surfaceMember xlink:href="#afb1dd01-25e5-4d17-9edb-08d5edee35db"/>
              <gml:surfaceMember xlink:href="#a872d72b-baa5-429c-bd14-4e100d745747"/>
              <gml:surfaceMember xlink:href="#f9b88f5b-6a7d-4789-8305-52aba5e952a4"/>
              <gml:surfaceMember xlink:href="#7d8d0d9c-39fa-44d7-a3b3-2dfd7f1c3aaf"/>
              <gml:surfaceMember xlink:href="#61ae5107-4642-4583-9e06-34dc9717960d"/>
              <gml:surfaceMember xlink:href="#409d82e5-bf41-47a9-a61e-649f49ec641f"/>
              <gml:surfaceMember xlink:href="#0a9202f2-8652-4be4-a63d-5b1e98e1e727"/>
              <gml:surfaceMember xlink:href="#db58787f-75bc-4da6-9903-f6e874de67a8"/>
              <gml:surfaceMember xlink:href="#68486285-e694-49f8-b44e-761586fdff4d"/>
              <gml:surfaceMember xlink:href="#840d9df4-56af-42be-9c41-0bf503d25114"/>
              <gml:surfaceMember xlink:href="#fe397264-9517-479d-9e08-6b55b0f5ca06"/>
              <gml:surfaceMember xlink:href="#5ec1f7de-fbb1-4df8-8118-98c383bf7050"/>
              <gml:surfaceMember xlink:href="#8904fa24-b4fe-40b3-b22b-cb0ec81b9f5b"/>
              <gml:surfaceMember xlink:href="#ba5c5081-2dbd-473d-84b3-e343f77a9cff"/>
              <gml:surfaceMember xlink:href="#b521c023-7f81-4776-bdff-d2c56fa8d3b0"/>
              <gml:surfaceMember xlink:href="#0192ef8c-e131-4390-8402-4b4162608d21"/>
              <gml:surfaceMember xlink:href="#8d1c7e1e-15a7-41e2-a370-b50e1b1293c8"/>
              <gml:surfaceMember xlink:href="#711779d1-83e0-4aba-aa21-0828e52fac69"/>
              <gml:surfaceMember xlink:href="#ec69a1ac-7752-44f2-9203-782d103f577a"/>
              <gml:surfaceMember xlink:href="#27cdd860-8e82-48b6-bb67-cc720e5e3c84"/>
              <gml:surfaceMember xlink:href="#bee263b7-16ba-408e-9471-f53cc372b3fc"/>
              <gml:surfaceMember xlink:href="#ad9a0eca-7fcf-428e-8ecb-33fdaf3a4a3d"/>
              <gml:surfaceMember xlink:href="#83f64f59-5bc1-4c20-bf5a-420ab08f0ae2"/>
              <gml:surfaceMember xlink:href="#6def544a-1fe8-4d00-a986-3ec36c70c2b5"/>
              <gml:surfaceMember xlink:href="#4a2751ed-b4ac-430d-b47e-ac6efea2d1de"/>
              <gml:surfaceMember xlink:href="#933cf182-ad9d-49b8-82e8-cb4910ddfb8e"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="919287e1-de8b-480f-9f36-87d40ac29b6b">
          <gen:stringAttribute name="Type">
            <gen:value>Ground</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>867.40</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="addcd464-fb3b-4aae-bbf4-f96ff5c9d49e" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="2011f315-2001-4838-ac5d-488337677e1b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="c60deceb-ae30-48ec-99b6-c053a4e889e0">
                      <gml:posList>552221.025000000370000000 5805835.562999999200000000 52.850000000000001000 552212.580000000070000000 5805831.680999999900000000 52.850000000000001000 552208.311999999920000000 5805829.720000000700000000 52.850000000000001000 552207.688256629160000000 5805829.433311147600000000 52.850000000000001000 552202.317999999970000000 5805826.964999999900000000 52.850000000000001000 552195.598000000230000000 5805823.877000000300000000 52.850000000000001000 552193.236999999730000000 5805822.790999999300000000 52.850000000000001000 552189.605000000450000000 5805821.121999999500000000 52.850000000000001000 552190.656000000420000000 5805818.855000000400000000 52.850000000000001000 552192.442999999970000000 5805814.995999999300000000 52.850000000000001000 552176.605779273550000000 5805807.700611723600000000 52.849999999999994000 552172.913999999870000000 5805806.000000000000000000 52.850000000000001000 552170.638000000270000000 5805804.960000000900000000 52.850000000000001000 552168.303999999540000000 5805810.036000000300000000 52.850000000000001000 552170.575000000190000000 5805811.084000000700000000 52.850000000000001000 552168.304092888840000000 5805816.021346274800000000 52.850000000000001000 552165.401999999770000000 5805822.331000000200000000 52.850000000000001000 552184.275999999610000000 5805831.019999999600000000 52.850000000000009000 552184.920243055680000000 5805831.316534666300000000 52.850000000000001000 552188.997000000440000000 5805833.193000000000000000 52.850000000000001000 552190.449000000020000000 5805833.860999999600000000 52.850000000000001000 552195.169999999930000000 5805836.035000000100000000 52.850000000000001000 552202.913254528070000000 5805839.599564497400000000 52.850000000000001000 552215.141999999990000000 5805845.229000000300000000 52.849999999999994000 552219.862999999900000000 5805847.402000000700000000 52.849999999999994000 552220.770999999720000000 5805847.820000000300000000 52.849999999999994000 552220.992719214060000000 5805847.923438284500000000 52.850000000000001000 552222.697999999860000000 5805848.719000000500000000 52.850000000000001000 552227.394999999550000000 5805838.507999999400000000 52.850000000000001000 552225.563000000080000000 5805837.649000000200000000 52.850000000000001000 552225.475999999790000000 5805837.607999999100000000 52.850000000000001000 552225.292999999600000000 5805837.525000000400000000 52.850000000000001000 552221.025000000370000000 5805835.562999999200000000 52.850000000000001000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="e95c4baf-581e-4f77-ae2d-a243b7330e66">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>272.71</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>155.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="4ac3fbc3-eed4-4dc6-95b8-8d20d34b8ed4" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="2c75009c-ad1d-423e-95af-77aa830f06a5">
                  <gml:exterior>
                    <gml:LinearRing gml:id="6ef1740a-7164-42df-a34c-2ae22ceb755b">
                      <gml:posList>552207.688256629160000000 5805829.433311147600000000 69.996627807617188000 552202.317999999970000000 5805826.964999999900000000 69.996627807617188000 552195.598000000230000000 5805823.877000000300000000 69.996627807617188000 552193.236999999730000000 5805822.790999999300000000 69.996627807617188000 552193.236999999730000000 5805822.790999999300000000 52.850000000000001000 552195.598000000230000000 5805823.877000000300000000 52.850000000000001000 552202.317999999970000000 5805826.964999999900000000 52.850000000000001000 552207.688256629160000000 5805829.433311147600000000 52.850000000000001000 552207.688256629160000000 5805829.433311147600000000 69.996627807617188000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="c5909eb9-1753-4f9a-8e3b-72826f24b29c">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>68.54</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>155.32</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="6e2f0233-3dc1-439a-85c4-1550e5dfc56a" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="4a6c8fca-6cdc-468e-b036-19111e407c77">
                  <gml:exterior>
                    <gml:LinearRing gml:id="b45fc04d-a9d3-410e-a1fe-35a422b74b94">
                      <gml:posList>552193.236999999730000000 5805822.790999999300000000 69.996627807617188000 552189.605000000450000000 5805821.121999999500000000 69.996627807617188000 552189.605000000450000000 5805821.121999999500000000 52.850000000000001000 552193.236999999730000000 5805822.790999999300000000 52.850000000000001000 552193.236999999730000000 5805822.790999999300000000 69.996627807617188000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="82d3eb0b-1b8f-416a-aafd-9701584036d9">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>339.64</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>335.28</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="5edf6a34-ce55-4441-840a-227fd031344d" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="574cfcb0-8852-4c17-b6b9-486fd69c99d8">
                  <gml:exterior>
                    <gml:LinearRing gml:id="eed8904e-7d97-4773-aee4-d8f4271bd654">
                      <gml:posList>552202.913254528070000000 5805839.599564497400000000 52.850000000000001000 552195.169999999930000000 5805836.035000000100000000 52.850000000000001000 552190.449000000020000000 5805833.860999999600000000 52.850000000000001000 552188.997000000440000000 5805833.193000000000000000 52.850000000000001000 552184.920243055680000000 5805831.316534666300000000 52.850000000000001000 552184.920243055680000000 5805831.316534666300000000 69.996627807617188000 552188.997000000440000000 5805833.193000000000000000 69.996627807617188000 552190.449000000020000000 5805833.860999999600000000 69.996627807617188000 552195.169999999930000000 5805836.035000000100000000 69.996627807617188000 552202.913254528070000000 5805839.599564497400000000 69.996627807617188000 552202.913254528070000000 5805839.599564497400000000 52.850000000000001000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="836ffe13-1e19-4aab-b6ed-00b47e28c95e">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>69.05</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>244.84</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="66151fc5-1ecd-4819-a5a0-2d96b1f144ce" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="127276c9-38df-423a-88a0-a1100ea3606a">
                  <gml:exterior>
                    <gml:LinearRing gml:id="c5561dbd-70a8-4e0b-9c94-1c5d820e6e57">
                      <gml:posList>552205.885178024760000000 5805833.272168913900000000 76.928344726562500000 552205.885178024760000000 5805833.272168913900000000 79.874000549316406000 552202.913254528070000000 5805839.599564497400000000 79.874000549316406000 552202.913254528070000000 5805839.599564497400000000 69.996627807617188000 552205.885178024760000000 5805833.272168913900000000 69.996627807617188000 552205.885178024760000000 5805833.272168913900000000 76.928344726562500000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="a1f933c4-1e78-40bf-bae8-7d8d0acafcec">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>58.63</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>154.84</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="0033339a-78b9-4c4a-93fe-462760037e7b" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="385d6155-107d-4a5a-b2ac-43991414d920">
                  <gml:exterior>
                    <gml:LinearRing gml:id="9a8b257c-51ee-462e-a897-cb9dec040447">
                      <gml:posList>552223.899965960180000000 5805841.733560469900000000 79.874000549316406000 552205.885178024760000000 5805833.272168913900000000 79.874000549316406000 552205.885178024760000000 5805833.272168913900000000 76.928344726562500000 552223.899965960180000000 5805841.733560469900000000 76.928344726562500000 552223.899965960180000000 5805841.733560469900000000 79.874000549316406000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="701ed9e8-d0dd-4eb2-aaf0-2f0576569acf">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>531.26</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>335.28</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="aba972d4-be51-43d8-8a59-db9ccbc31f43" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="41e2be95-78ea-4027-8a82-eda9d626f007">
                  <gml:exterior>
                    <gml:LinearRing gml:id="b3c75aa2-eefd-4bc7-872f-19ffd84abcc5">
                      <gml:posList>552220.770999999720000000 5805847.820000000300000000 79.874000549316406000 552220.770999999720000000 5805847.820000000300000000 52.849999999999994000 552219.862999999900000000 5805847.402000000700000000 52.849999999999994000 552215.141999999990000000 5805845.229000000300000000 52.849999999999994000 552202.913254528070000000 5805839.599564497400000000 52.850000000000001000 552202.913254528070000000 5805839.599564497400000000 69.996627807617188000 552202.913254528070000000 5805839.599564497400000000 79.874000549316406000 552215.141999999990000000 5805845.229000000300000000 79.874000549316406000 552219.862999999900000000 5805847.402000000700000000 79.874000549316406000 552220.770999999720000000 5805847.820000000300000000 79.874000549316406000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="5ea7d01b-0e4e-4f2e-9846-5da1fdc67a6b">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>6.61</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>334.99</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="cb695111-c4da-4d87-9723-f42eeb0ba26e" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="e1454db0-c26a-435a-8ae1-d68c8e565c2a">
                  <gml:exterior>
                    <gml:LinearRing gml:id="65164e4b-724a-42d3-903a-ea27dd0cbc08">
                      <gml:posList>552220.992719214060000000 5805847.923438284500000000 76.928344726562500000 552220.992719214060000000 5805847.923438284500000000 52.850000000000001000 552220.770999999720000000 5805847.820000000300000000 52.849999999999994000 552220.770999999720000000 5805847.820000000300000000 79.874000549316406000 552220.992719214060000000 5805847.923438284500000000 79.874000549316406000 552220.992719214060000000 5805847.923438284500000000 76.928344726562500000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="2297fd54-57e3-454b-8f24-7778554cc8f7">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>20.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>64.84</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="0a1f758f-ec5e-4177-942e-3c5cbebaae23" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="8cca5956-9d72-4c41-8127-5958f4f05792">
                  <gml:exterior>
                    <gml:LinearRing gml:id="ec23c80b-9d29-45be-9436-4cff2a564115">
                      <gml:posList>552223.899965960180000000 5805841.733560469900000000 76.928344726562500000 552220.992719214060000000 5805847.923438284500000000 76.928344726562500000 552220.992719214060000000 5805847.923438284500000000 79.874000549316406000 552223.899965960180000000 5805841.733560469900000000 79.874000549316406000 552223.899965960180000000 5805841.733560469900000000 76.928344726562500000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="d528d120-c9bf-495c-af68-f512bd6bfb3a">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>29.40</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>244.84</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="2a21048f-9145-461d-9689-d5f4ce7fc0fd" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="e7c85a7b-c488-49cc-9f92-1bde7384cc9f">
                  <gml:exterior>
                    <gml:LinearRing gml:id="85f7d1db-75f5-4b3c-b7b0-8f4c2228246d">
                      <gml:posList>552207.688256629160000000 5805829.433311147600000000 76.928344726562500000 552205.885178024760000000 5805833.272168913900000000 76.928344726562500000 552205.885178024760000000 5805833.272168913900000000 69.996627807617188000 552207.688256629160000000 5805829.433311147600000000 69.996627807617188000 552207.688256629160000000 5805829.433311147600000000 76.928344726562500000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="5d88d88c-3dc0-4bf7-84a2-ab4157098c64">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>48.72</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>154.88</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="5c913377-948a-4d31-bb61-e6ce6bed323f" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="f2de26f5-a94b-4dfc-a10a-469fee05cd62">
                  <gml:exterior>
                    <gml:LinearRing gml:id="ad1ab493-e0f0-4632-9ff1-f7592ecb9f6a">
                      <gml:posList>552227.394999999550000000 5805838.507999999400000000 76.928344726562500000 552225.563000000080000000 5805837.649000000200000000 76.928344726562500000 552225.563000000080000000 5805837.649000000200000000 52.850000000000001000 552227.394999999550000000 5805838.507999999400000000 52.850000000000001000 552227.394999999550000000 5805838.507999999400000000 76.928344726562500000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="659b2d0c-cc4a-4998-baed-ccf7664316ed">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>2.32</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>154.77</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="e88cb81e-eb74-4388-a227-a60449146122" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="afb1dd01-25e5-4d17-9edb-08d5edee35db">
                  <gml:exterior>
                    <gml:LinearRing gml:id="79dfcfaf-2e5c-42c0-a246-43f5429f5f2e">
                      <gml:posList>552225.563000000080000000 5805837.649000000200000000 76.928344726562500000 552225.475999999790000000 5805837.607999999100000000 76.928344726562500000 552225.475999999790000000 5805837.607999999100000000 52.850000000000001000 552225.563000000080000000 5805837.649000000200000000 52.850000000000001000 552225.563000000080000000 5805837.649000000200000000 76.928344726562500000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="7b9f667b-581f-4f44-b985-35b017f00786">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>4.84</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>155.60</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="a0360233-4a24-4cdd-887e-1e77c5675366" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="a872d72b-baa5-429c-bd14-4e100d745747">
                  <gml:exterior>
                    <gml:LinearRing gml:id="cd384e50-89df-4da2-9303-c32519f35960">
                      <gml:posList>552225.475999999790000000 5805837.607999999100000000 76.928344726562500000 552225.292999999600000000 5805837.525000000400000000 76.928344726562500000 552225.292999999600000000 5805837.525000000400000000 52.850000000000001000 552225.475999999790000000 5805837.607999999100000000 52.850000000000001000 552225.475999999790000000 5805837.607999999100000000 76.928344726562500000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="de7b3ee3-a455-4a05-a316-8547047d94a2">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>466.53</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>155.32</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="0872bf84-0257-41d0-b3ab-e4966b438091" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="f9b88f5b-6a7d-4789-8305-52aba5e952a4">
                  <gml:exterior>
                    <gml:LinearRing gml:id="4dd571b7-486c-4a61-8230-a2575c2ffc4c">
                      <gml:posList>552225.292999999600000000 5805837.525000000400000000 76.928344726562500000 552221.025000000370000000 5805835.562999999200000000 76.928344726562500000 552212.580000000070000000 5805831.680999999900000000 76.928344726562500000 552208.311999999920000000 5805829.720000000700000000 76.928344726562500000 552207.688256629160000000 5805829.433311147600000000 76.928344726562500000 552207.688256629160000000 5805829.433311147600000000 69.996627807617188000 552207.688256629160000000 5805829.433311147600000000 52.850000000000001000 552208.311999999920000000 5805829.720000000700000000 52.850000000000001000 552212.580000000070000000 5805831.680999999900000000 52.850000000000001000 552221.025000000370000000 5805835.562999999200000000 52.850000000000001000 552225.292999999600000000 5805837.525000000400000000 52.850000000000001000 552225.292999999600000000 5805837.525000000400000000 76.928344726562500000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="5b1f036d-c4bf-4696-ab1f-a18ed42982a1">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>45.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>334.99</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="dfd3d2de-19df-4f79-8f83-6eac2d52bd22" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="7d8d0d9c-39fa-44d7-a3b3-2dfd7f1c3aaf">
                  <gml:exterior>
                    <gml:LinearRing gml:id="15e009ae-3c90-4d9b-be6b-0703da1e222c">
                      <gml:posList>552222.697999999860000000 5805848.719000000500000000 52.850000000000001000 552220.992719214060000000 5805847.923438284500000000 52.850000000000001000 552220.992719214060000000 5805847.923438284500000000 76.928344726562500000 552222.697999999860000000 5805848.719000000500000000 76.928344726562500000 552222.697999999860000000 5805848.719000000500000000 52.850000000000001000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="5a21ea6b-423b-42f0-8fbe-1324eb5a0a42">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>270.63</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>65.30</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="5d160bff-2a5d-4da5-b275-4e89155d106a" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="61ae5107-4642-4583-9e06-34dc9717960d">
                  <gml:exterior>
                    <gml:LinearRing gml:id="6831bda5-80a1-4576-a0c9-1003b9cf6c95">
                      <gml:posList>552227.394999999550000000 5805838.507999999400000000 52.850000000000001000 552222.697999999860000000 5805848.719000000500000000 52.850000000000001000 552222.697999999860000000 5805848.719000000500000000 76.928344726562500000 552227.394999999550000000 5805838.507999999400000000 76.928344726562500000 552227.394999999550000000 5805838.507999999400000000 52.850000000000001000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="92b8a107-65c9-44b0-8f42-bad102bb77ac">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>72.03</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>155.27</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="9a4e7a95-195b-4259-84b8-4f37116ade17" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="409d82e5-bf41-47a9-a61e-649f49ec641f">
                  <gml:exterior>
                    <gml:LinearRing gml:id="d62cb86a-6b0c-4584-9190-9ec57adee21e">
                      <gml:posList>552176.605779273550000000 5805807.700611723600000000 70.571426391601562000 552172.913999999870000000 5805806.000000000000000000 70.571426391601562000 552172.913999999870000000 5805806.000000000000000000 52.850000000000001000 552176.605779273550000000 5805807.700611723600000000 52.849999999999994000 552176.605779273550000000 5805807.700611723600000000 70.571426391601562000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="c6edc3d2-bac8-4240-91e5-cd28bdd990b5">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>44.35</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>155.44</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="cbd40050-ce88-42b1-857f-5cc68c3c39e8" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="0a9202f2-8652-4be4-a63d-5b1e98e1e727">
                  <gml:exterior>
                    <gml:LinearRing gml:id="34f8bed9-77c2-4ecf-a0e2-fc26265bc26f">
                      <gml:posList>552172.913999999870000000 5805806.000000000000000000 70.571426391601562000 552170.638000000270000000 5805804.960000000900000000 70.571426391601562000 552170.638000000270000000 5805804.960000000900000000 52.850000000000001000 552172.913999999870000000 5805806.000000000000000000 52.850000000000001000 552172.913999999870000000 5805806.000000000000000000 70.571426391601562000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="f1b5b899-3306-4c3b-8c5c-52bf5d1fab17">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>99.01</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>245.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="3b11e2c2-eb3b-4347-a5c2-13ebd483b0c0" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="db58787f-75bc-4da6-9903-f6e874de67a8">
                  <gml:exterior>
                    <gml:LinearRing gml:id="2d4d19bd-fbcb-44ac-b61a-5a5ad277bb61">
                      <gml:posList>552170.638000000270000000 5805804.960000000900000000 70.571426391601562000 552168.303999999540000000 5805810.036000000300000000 70.571426391601562000 552168.303999999540000000 5805810.036000000300000000 52.850000000000001000 552170.638000000270000000 5805804.960000000900000000 52.850000000000001000 552170.638000000270000000 5805804.960000000900000000 70.571426391601562000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="a9d1ab2a-facb-4ca5-a3c2-c13f9bbeb1b4">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>44.32</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>335.23</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="5480ddfe-aff2-4248-b230-5518ee933a9a" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="68486285-e694-49f8-b44e-761586fdff4d">
                  <gml:exterior>
                    <gml:LinearRing gml:id="a5eab118-4319-4b3f-9a78-757a620aeb29">
                      <gml:posList>552170.575000000190000000 5805811.084000000700000000 52.850000000000001000 552168.303999999540000000 5805810.036000000300000000 52.850000000000001000 552168.303999999540000000 5805810.036000000300000000 70.571426391601562000 552170.575000000190000000 5805811.084000000700000000 70.571426391601562000 552170.575000000190000000 5805811.084000000700000000 52.850000000000001000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="c2ea2800-2441-4686-9916-5619fa05d18e">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>96.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>245.30</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="a8fac5e9-d0ab-437a-ab3d-fa3a4c8fb102" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="840d9df4-56af-42be-9c41-0bf503d25114">
                  <gml:exterior>
                    <gml:LinearRing gml:id="f6b88c35-a9ad-4e87-b03f-5ef6da90471f">
                      <gml:posList>552170.575000000190000000 5805811.084000000700000000 70.571426391601562000 552168.304092888840000000 5805816.021346274800000000 70.571426391601562000 552168.304092888840000000 5805816.021346274800000000 52.850000000000001000 552170.575000000190000000 5805811.084000000700000000 52.850000000000001000 552170.575000000190000000 5805811.084000000700000000 70.571426391601562000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="fadd0a2f-cea6-4c26-9af6-acf9fafdf541">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>25.73</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>65.32</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="6fb363e3-7648-4655-8048-508754f10290" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="fe397264-9517-479d-9e08-6b55b0f5ca06">
                  <gml:exterior>
                    <gml:LinearRing gml:id="3f071598-ef0a-42ed-9e1e-00bacd59fd1b">
                      <gml:posList>552189.605000000450000000 5805821.121999999500000000 69.996627807617188000 552187.841453038740000000 5805824.959667747800000000 69.996627807617188000 552187.841453038740000000 5805824.959667747800000000 76.087593078613281000 552189.605000000450000000 5805821.121999999500000000 76.087593078613281000 552189.605000000450000000 5805821.121999999500000000 69.996627807617188000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="70fec09b-0061-4ba6-af65-db1477fcd3d8">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>60.63</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>245.32</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="22b20b6e-77ab-44ca-9310-d4104d1e4ca6" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="5ec1f7de-fbb1-4df8-8118-98c383bf7050">
                  <gml:exterior>
                    <gml:LinearRing gml:id="de3db5c6-37ac-464b-8976-08770f094ca0">
                      <gml:posList>552176.605779273550000000 5805807.700611723600000000 76.087593078613281000 552172.016271251950000000 5805817.687425050000000000 76.087593078613281000 552172.016271251950000000 5805817.687425050000000000 70.571426391601562000 552176.605779273550000000 5805807.700611723600000000 70.571426391601562000 552176.605779273550000000 5805807.700611723600000000 76.087593078613281000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="94a95ae3-4fa4-45f9-bef3-c7b44b1ec9cd">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>58.07</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>65.13</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="ee400027-d753-4e40-8a14-63347f44950f" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="8904fa24-b4fe-40b3-b22b-cb0ec81b9f5b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="56521011-97f6-4401-8414-aed6421e4683">
                      <gml:posList>552190.656000000420000000 5805818.855000000400000000 76.087593078613281000 552190.656000000420000000 5805818.855000000400000000 52.850000000000001000 552189.605000000450000000 5805821.121999999500000000 52.850000000000001000 552189.605000000450000000 5805821.121999999500000000 69.996627807617188000 552189.605000000450000000 5805821.121999999500000000 76.087593078613281000 552190.656000000420000000 5805818.855000000400000000 76.087593078613281000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="c121aea5-7634-4812-80d8-866f3213795c">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>98.82</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>65.15</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="8c8271da-3a42-4dcc-a018-80bf1219b42d" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="ba5c5081-2dbd-473d-84b3-e343f77a9cff">
                  <gml:exterior>
                    <gml:LinearRing gml:id="b666d15a-2828-4c0a-a1d8-fcf5c6b7b215">
                      <gml:posList>552192.442999999970000000 5805814.995999999300000000 52.850000000000001000 552190.656000000420000000 5805818.855000000400000000 52.850000000000001000 552190.656000000420000000 5805818.855000000400000000 76.087593078613281000 552192.442999999970000000 5805814.995999999300000000 76.087593078613281000 552192.442999999970000000 5805814.995999999300000000 52.850000000000001000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="900be6c1-a7ea-4497-88d0-da47912cdacb">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>405.19</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>155.27</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="5b0874b7-20c1-4553-9248-8c08593ba3f5" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="b521c023-7f81-4776-bdff-d2c56fa8d3b0">
                  <gml:exterior>
                    <gml:LinearRing gml:id="3449b463-bc1b-4516-8a9c-66c2b14a4254">
                      <gml:posList>552192.442999999970000000 5805814.995999999300000000 76.087593078613281000 552176.605779273550000000 5805807.700611723600000000 76.087593078613281000 552176.605779273550000000 5805807.700611723600000000 70.571426391601562000 552176.605779273550000000 5805807.700611723600000000 52.849999999999994000 552192.442999999970000000 5805814.995999999300000000 52.850000000000001000 552192.442999999970000000 5805814.995999999300000000 76.087593078613281000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="3a54a59b-3692-4b57-9fd8-2ce18b6485e5">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>66.16</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>65.32</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="f5bab7b8-747c-47ee-bde5-c5e9f1b106e2" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="0192ef8c-e131-4390-8402-4b4162608d21">
                  <gml:exterior>
                    <gml:LinearRing gml:id="92bf234f-e92b-4db3-8d9d-ba3e21376fe3">
                      <gml:posList>552187.841453038740000000 5805824.959667747800000000 76.087593078613281000 552187.841453038740000000 5805824.959667747800000000 69.996627807617188000 552184.920243055680000000 5805831.316534666300000000 69.996627807617188000 552184.920243055680000000 5805831.316534666300000000 79.452941894531250000 552187.841453038740000000 5805824.959667747800000000 79.452941894531250000 552187.841453038740000000 5805824.959667747800000000 76.087593078613281000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="86f7c112-26ce-4990-8b07-76c2657bb19e">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>58.61</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>155.32</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="3f50f0ab-0906-456f-9c72-97d1c88e6cc7" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="8d1c7e1e-15a7-41e2-a370-b50e1b1293c8">
                  <gml:exterior>
                    <gml:LinearRing gml:id="a32fb877-ff7d-4891-8228-1ac2eb746dc8">
                      <gml:posList>552187.841453038740000000 5805824.959667747800000000 79.452941894531250000 552172.016271251950000000 5805817.687425050000000000 79.452941894531250000 552172.016271251950000000 5805817.687425050000000000 76.087593078613281000 552187.841453038740000000 5805824.959667747800000000 76.087593078613281000 552187.841453038740000000 5805824.959667747800000000 79.452941894531250000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="6b415756-a984-43f5-871d-c7b8df03fb9e">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>36.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>155.83</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="15f0de78-c1d5-43eb-b2c8-3c3554a1f5cd" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="711779d1-83e0-4aba-aa21-0828e52fac69">
                  <gml:exterior>
                    <gml:LinearRing gml:id="f082f28e-d0f8-49db-b13a-d4e77f2f1130">
                      <gml:posList>552172.016271251950000000 5805817.687425050000000000 76.087593078613281000 552172.016271251950000000 5805817.687425050000000000 79.452941894531250000 552168.304092888840000000 5805816.021346274800000000 79.452941894531250000 552168.304092888840000000 5805816.021346274800000000 70.571426391601562000 552172.016271251950000000 5805817.687425050000000000 70.571426391601562000 552172.016271251950000000 5805817.687425050000000000 76.087593078613281000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="26f29b03-54e6-45b3-b714-47bbb946bdbf">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>184.76</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>245.30</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="d7f530d2-cb4d-4288-9596-8c9f169df1a4" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="ec69a1ac-7752-44f2-9203-782d103f577a">
                  <gml:exterior>
                    <gml:LinearRing gml:id="c02e500e-39c1-4fd7-9c2a-76895c004342">
                      <gml:posList>552168.304092888840000000 5805816.021346274800000000 70.571426391601562000 552168.304092888840000000 5805816.021346274800000000 79.452941894531250000 552165.401999999770000000 5805822.331000000200000000 79.452941894531250000 552165.401999999770000000 5805822.331000000200000000 52.850000000000001000 552168.304092888840000000 5805816.021346274800000000 52.850000000000001000 552168.304092888840000000 5805816.021346274800000000 70.571426391601562000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="39fa545e-73ea-47a6-84b4-567c253fc12e">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>571.62</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>335.28</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="575dd2af-6a58-452a-8f7e-6bbe8aaeb98e" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="27cdd860-8e82-48b6-bb67-cc720e5e3c84">
                  <gml:exterior>
                    <gml:LinearRing gml:id="fb31da42-772e-4315-86df-581bbbb14688">
                      <gml:posList>552184.920243055680000000 5805831.316534666300000000 69.996627807617188000 552184.920243055680000000 5805831.316534666300000000 52.850000000000001000 552184.275999999610000000 5805831.019999999600000000 52.850000000000009000 552165.401999999770000000 5805822.331000000200000000 52.850000000000001000 552165.401999999770000000 5805822.331000000200000000 79.452941894531250000 552184.275999999610000000 5805831.019999999600000000 79.452941894531250000 552184.920243055680000000 5805831.316534666300000000 79.452941894531250000 552184.920243055680000000 5805831.316534666300000000 69.996627807617188000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="a38553bc-a60b-4b5f-9055-7d65ae40dbc6">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001dq8N_C:29313</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>222.88</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="624ce928-4a83-46cd-a26e-4c82a00e63f2" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bee263b7-16ba-408e-9471-f53cc372b3fc">
                  <gml:exterior>
                    <gml:LinearRing gml:id="3f2e2244-8f32-4bb8-ad28-e8c88474fbf1">
                      <gml:posList>552205.885178024760000000 5805833.272168913900000000 69.996627807617188000 552202.913254528070000000 5805839.599564497400000000 69.996627807617188000 552195.169999999930000000 5805836.035000000100000000 69.996627807617188000 552190.449000000020000000 5805833.860999999600000000 69.996627807617188000 552188.997000000440000000 5805833.193000000000000000 69.996627807617188000 552184.920243055680000000 5805831.316534666300000000 69.996627807617188000 552187.841453038740000000 5805824.959667747800000000 69.996627807617188000 552189.605000000450000000 5805821.121999999500000000 69.996627807617188000 552193.236999999730000000 5805822.790999999300000000 69.996627807617188000 552195.598000000230000000 5805823.877000000300000000 69.996627807617188000 552202.317999999970000000 5805826.964999999900000000 69.996627807617188000 552207.688256629160000000 5805829.433311147600000000 69.996627807617188000 552205.885178024760000000 5805833.272168913900000000 69.996627807617188000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="e6da73df-d7b1-44e4-beb3-9eb2625c694e">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001dq8N_C:29310</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>137.61</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="0e8f4f91-ee63-4273-878d-5d8d52b1cce5" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="ad9a0eca-7fcf-428e-8ecb-33fdaf3a4a3d">
                  <gml:exterior>
                    <gml:LinearRing gml:id="37f2a160-9fa0-49e7-9834-540315b2a3d9">
                      <gml:posList>552220.770999999720000000 5805847.820000000300000000 79.874000549316406000 552219.862999999900000000 5805847.402000000700000000 79.874000549316406000 552215.141999999990000000 5805845.229000000300000000 79.874000549316406000 552202.913254528070000000 5805839.599564497400000000 79.874000549316406000 552205.885178024760000000 5805833.272168913900000000 79.874000549316406000 552223.899965960180000000 5805841.733560469900000000 79.874000549316406000 552220.992719214060000000 5805847.923438284500000000 79.874000549316406000 552220.770999999720000000 5805847.820000000300000000 79.874000549316406000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="57835dcf-dc34-4fda-9861-309a40f74700">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001dq8N_C:29309</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>106.71</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="87c00ba3-7802-40c2-90f9-25b82c487705" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="83f64f59-5bc1-4c20-bf5a-420ab08f0ae2">
                  <gml:exterior>
                    <gml:LinearRing gml:id="4f19502a-f71b-4596-9257-6f7cc78de198">
                      <gml:posList>552225.563000000080000000 5805837.649000000200000000 76.928344726562500000 552227.394999999550000000 5805838.507999999400000000 76.928344726562500000 552222.697999999860000000 5805848.719000000500000000 76.928344726562500000 552220.992719214060000000 5805847.923438284500000000 76.928344726562500000 552223.899965960180000000 5805841.733560469900000000 76.928344726562500000 552205.885178024760000000 5805833.272168913900000000 76.928344726562500000 552207.688256629160000000 5805829.433311147600000000 76.928344726562500000 552208.311999999920000000 5805829.720000000700000000 76.928344726562500000 552212.580000000070000000 5805831.680999999900000000 76.928344726562500000 552221.025000000370000000 5805835.562999999200000000 76.928344726562500000 552225.292999999600000000 5805837.525000000400000000 76.928344726562500000 552225.475999999790000000 5805837.607999999100000000 76.928344726562500000 552225.563000000080000000 5805837.649000000200000000 76.928344726562500000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="7a42fd71-7b7a-48a3-afaa-bc93589aa0d6">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001dq8N_C:29312</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>191.35</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="9d2b2cff-b2fb-4a57-a105-b137cfa7c4af" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="6def544a-1fe8-4d00-a986-3ec36c70c2b5">
                  <gml:exterior>
                    <gml:LinearRing gml:id="259ed756-4b42-4bc7-9d93-26652eff6772">
                      <gml:posList>552189.605000000450000000 5805821.121999999500000000 76.087593078613281000 552187.841453038740000000 5805824.959667747800000000 76.087593078613281000 552172.016271251950000000 5805817.687425050000000000 76.087593078613281000 552176.605779273550000000 5805807.700611723600000000 76.087593078613281000 552192.442999999970000000 5805814.995999999300000000 76.087593078613281000 552190.656000000420000000 5805818.855000000400000000 76.087593078613281000 552189.605000000450000000 5805821.121999999500000000 76.087593078613281000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="b18d5a21-e756-4a3f-bdbf-f72373b05a40">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001dq8N_C:29311</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>150.08</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="7d1d9b31-38ca-42d6-97b9-fa57fd3481d9" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="4a2751ed-b4ac-430d-b47e-ac6efea2d1de">
                  <gml:exterior>
                    <gml:LinearRing gml:id="571498fc-06a0-40a1-b581-26f08f980cdb">
                      <gml:posList>552184.920243055680000000 5805831.316534666300000000 79.452941894531250000 552184.275999999610000000 5805831.019999999600000000 79.452941894531250000 552165.401999999770000000 5805822.331000000200000000 79.452941894531250000 552168.304092888840000000 5805816.021346274800000000 79.452941894531250000 552172.016271251950000000 5805817.687425050000000000 79.452941894531250000 552187.841453038740000000 5805824.959667747800000000 79.452941894531250000 552184.920243055680000000 5805831.316534666300000000 79.452941894531250000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="8091da34-6f2f-442d-8104-f30dde709a53">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001dq8N_C:29308</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>58.77</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="833bd3bf-1ba3-4c5f-96de-03642df0f5a5" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="933cf182-ad9d-49b8-82e8-cb4910ddfb8e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="15a36c3b-e835-4a01-bd0d-c3f2fb0d5a57">
                      <gml:posList>552176.605779273550000000 5805807.700611723600000000 70.571426391601562000 552172.016271251950000000 5805817.687425050000000000 70.571426391601562000 552168.304092888840000000 5805816.021346274800000000 70.571426391601562000 552170.575000000190000000 5805811.084000000700000000 70.571426391601562000 552168.303999999540000000 5805810.036000000300000000 70.571426391601562000 552170.638000000270000000 5805804.960000000900000000 70.571426391601562000 552172.913999999870000000 5805806.000000000000000000 70.571426391601562000 552176.605779273550000000 5805807.700611723600000000 70.571426391601562000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </cityObjectMember>

</CityModel>
