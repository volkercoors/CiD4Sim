<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<CityModel xmlns="http://www.opengis.net/citygml/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:core="http://www.opengis.net/citygml/base/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:tex="http://www.opengis.net/citygml/textures/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/citygml/2.0 http://schemas.opengis.net/citygml/2.0/cityGMLBase.xsd http://www.opengis.net/citygml/appearance/2.0  http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd">

  <gml:description>Exported by RhinoCity(TM) software - (c)Rhinoterrain 2010-2018 : www.rhinoterrain.com</gml:description>

  <gml:name>RhinoCity(TM)</gml:name>

  <gml:boundedBy>
    <gml:Envelope srsDimension="3" srsName="crs:ETRS89_UTM32*DE_DHHN92_NH">
      <gml:lowerCorner>552662.750000 5799975.000000 57.119995</gml:lowerCorner>
      <gml:upperCorner>552674.812500 5799987.000000 66.129372</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>

  <cityObjectMember>
    <bldg:Building gml:id="DENIAL430001ewbO">
      <gen:doubleAttribute name="Volume">
        <gen:value>743.900</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="TotalRoofArea">
        <gen:value>104.03</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="TotalWallArea">
        <gen:value>295.45</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Authoring_Software">
        <gen:value>RhinoCity</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="OSKA">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ALKISID">
        <gen:value>DENIAL430001ewbO</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="KACHEL_ID">
        <gen:value>11_04_Building</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="COUNT">
        <gen:value>1281</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="DESC">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCK">
        <gen:value>20</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCKNR">
        <gen:value>051020</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STATBEZNR">
        <gen:value>051</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="MBZNR">
        <gen:value>0502</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="WBZNR">
        <gen:value>0502</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNAM">
        <gen:value>Waldhausen</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNR">
        <gen:value>05</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNAM">
        <gen:value>Döhren-Wülfel</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNR">
        <gen:value>08</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEMEINDE">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SCALE">
        <gen:value>5000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_overlaps">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_xmax">
        <gen:value>552674.7580000004</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_xmin">
        <gen:value>552662.8090000004</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_ymax">
        <gen:value>5799986.460999999</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_ymin">
        <gen:value>5799975.484999999</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_zmax">
        <gen:value>57.758955999990924</gen:value>
      </gen:stringAttribute>
      <gen:doubleAttribute name="ZMIN">
        <gen:value>57.120000</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="THICKNESS">
        <gen:value>0.100000</gen:value>
      </gen:doubleAttribute>
      <gen:intAttribute name="TEXTURE">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="BUILDINGID">
        <gen:value>DENIAL430001ewbO</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ARTBEZEICH">
        <gen:value>Gebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Join_Cou_1">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="FUNKTION_T">
        <gen:value>Wohngebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADT">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="OVERHANG">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="OBJFLAECHE">
        <gen:value>94.93</gen:value>
      </gen:stringAttribute>
      <bldg:measuredHeight uom="#m">12.182</bldg:measuredHeight>
      <bldg:lod2Solid>
        <gml:Solid gml:id="c194fe44-e136-4c5b-b3cc-45c4493cba77">
          <gml:exterior>
            <gml:CompositeSurface gml:id="5b340694-abbe-4343-9f26-0a2ce6874878">
              <gml:surfaceMember xlink:href="#88ff8c29-7b42-4125-8f12-414b3442eb84"/>
              <gml:surfaceMember xlink:href="#e397a809-673a-4da3-b94c-909b2661a145"/>
              <gml:surfaceMember xlink:href="#284614ed-9f72-4561-aca4-1e1e51167d78"/>
              <gml:surfaceMember xlink:href="#045839e2-6863-427d-a598-7c05353a36e5"/>
              <gml:surfaceMember xlink:href="#a42091c7-f5f9-4989-a5f4-8711a00b34bf"/>
              <gml:surfaceMember xlink:href="#bccfe04c-56c1-4de0-891e-651731797444"/>
              <gml:surfaceMember xlink:href="#92480b9c-bc4a-45b4-bcd2-0be3c9a98e1c"/>
              <gml:surfaceMember xlink:href="#f0af8538-c154-4205-a913-f2070d3f4ee5"/>
              <gml:surfaceMember xlink:href="#5ec3d01f-d331-4c16-a4e2-3e2103e31dcc"/>
              <gml:surfaceMember xlink:href="#60f40c7b-1e8b-4759-bcc2-e92b2ce7c277"/>
              <gml:surfaceMember xlink:href="#5acb6ab4-28bf-47ff-97a4-b94102d385e7"/>
              <gml:surfaceMember xlink:href="#ccbd7556-5d67-44a9-8fea-e2bd03ad5483"/>
              <gml:surfaceMember xlink:href="#f45fcf57-9694-4bef-b8e8-be50ac72fd40"/>
              <gml:surfaceMember xlink:href="#632dac39-7aba-471e-bd60-d5c55f883502"/>
              <gml:surfaceMember xlink:href="#20ee8688-2b1e-44ef-b993-7ff86bc3661b"/>
              <gml:surfaceMember xlink:href="#06ec8928-b333-4bd8-babe-d3ae1f3a2e72"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bc7a25d4-0c87-40f0-a409-5602116a1784">
          <gen:stringAttribute name="Type">
            <gen:value>Ground</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>94.93</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="95f4cc50-cb54-4d75-950b-2e8a116c6fda" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="88ff8c29-7b42-4125-8f12-414b3442eb84">
                  <gml:exterior>
                    <gml:LinearRing gml:id="497e4a3e-df5e-42b4-b894-724bbc36afc4">
                      <gml:posList>552674.024882543600000000 5799981.677221176200000000 57.119999999999997000 552674.758000000380000000 5799976.969000000500000000 57.120000000000005000 552665.225999999790000000 5799975.484999999400000000 57.119999999999997000 552664.558166357920000000 5799979.824961890500000000 57.119999999999976000 552664.527999999930000000 5799980.020999999700000000 57.119999999999997000 552664.370679647890000000 5799979.996551566800000000 57.119999999999997000 552663.048000000420000000 5799979.790999999300000000 57.119999999999997000 552662.809000000360000000 5799981.342000000200000000 57.119999999999983000 552664.126462846990000000 5799981.546326672700000000 57.119999999999997000 552664.291739570090000000 5799981.571959610100000000 57.119999999999997000 552664.292000000370000000 5799981.572000000600000000 57.119999999999997000 552663.768000000160000000 5799984.980000000400000000 57.119999999999983000 552673.280000000260000000 5799986.460999999200000000 57.120000000000005000 552674.024882543600000000 5799981.677221176200000000 57.119999999999997000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="c56a8abc-4d21-4e87-bc21-4ce236f54630">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.59</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>171.17</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="40a5564a-c689-41c7-bd50-2880f16c3340" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="e397a809-673a-4da3-b94c-909b2661a145">
                  <gml:exterior>
                    <gml:LinearRing gml:id="39876187-c7ab-41e0-b76e-b3375fbe8ae1">
                      <gml:posList>552664.527999999930000000 5799980.020999999700000000 60.889542043842646000 552664.370679647890000000 5799979.996551566800000000 60.809995381900762000 552664.370679647890000000 5799979.996551566800000000 57.119999999999997000 552664.527999999930000000 5799980.020999999700000000 57.119999999999997000 552664.527999999930000000 5799980.020999999700000000 60.889542043842646000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="19cfe6a1-f92e-47b7-8d97-decb4cea2d9d">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>1.40</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>351.18</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="138e7d53-0733-4896-810f-f95a256f8691" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="284614ed-9f72-4561-aca4-1e1e51167d78">
                  <gml:exterior>
                    <gml:LinearRing gml:id="3567ae6a-328e-4dd6-8e67-a758d13e9e79">
                      <gml:posList>552664.291739570090000000 5799981.571959610100000000 60.889675140380859000 552664.292000000370000000 5799981.572000000600000000 57.119999999999997000 552664.291739570090000000 5799981.571959610100000000 57.119999999999997000 552664.126462846990000000 5799981.546326672700000000 57.119999999999997000 552664.126462846990000000 5799981.546326672700000000 60.806109373325143000 552664.126462846990000000 5799981.546326672700000000 65.498224695512533000 552664.292000000370000000 5799981.572000000600000000 65.498406832001905000 552664.291739570090000000 5799981.571959610100000000 60.889675140380859000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="97e655d5-0530-440c-9b10-1b76e81f64a3">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>26.23</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>261.26</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="89530762-2d40-4b74-8f11-8cfdfbec5451" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="045839e2-6863-427d-a598-7c05353a36e5">
                  <gml:exterior>
                    <gml:LinearRing gml:id="b0bd6da1-cf65-44dd-8d48-6ed044ef406d">
                      <gml:posList>552664.292000000370000000 5799981.572000000600000000 65.498406832001905000 552663.768000000160000000 5799984.980000000400000000 63.956655605168294000 552663.768000000160000000 5799984.980000000400000000 57.119999999999983000 552664.292000000370000000 5799981.572000000600000000 57.119999999999997000 552664.292000000370000000 5799981.572000000600000000 65.498406832001905000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="7dace92d-46bb-4b39-98d4-82b6a0ead7ba">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>65.85</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>351.15</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="48f772ff-3a33-4bc7-8cf2-e0ccae719a26" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="a42091c7-f5f9-4989-a5f4-8711a00b34bf">
                  <gml:exterior>
                    <gml:LinearRing gml:id="ec1c169c-7024-4890-8709-608259a6f60f">
                      <gml:posList>552673.280000000260000000 5799986.460999999200000000 57.120000000000005000 552663.768000000160000000 5799984.980000000400000000 57.119999999999983000 552663.768000000160000000 5799984.980000000400000000 63.956655605168294000 552673.280000000260000000 5799986.460999999200000000 63.964571063253565000 552673.280000000260000000 5799986.460999999200000000 57.120000000000005000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="0cbd26a6-3da0-4b5d-8b42-9fe01d972e5b">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>76.23</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>81.15</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="78ed5d07-e443-4211-b729-334909076bda" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bccfe04c-56c1-4de0-891e-651731797444">
                  <gml:exterior>
                    <gml:LinearRing gml:id="1805d1d2-0aee-4cc7-8c3e-eb7a489c3438">
                      <gml:posList>552674.758000000380000000 5799976.969000000500000000 57.120000000000005000 552674.024882543600000000 5799981.677221176200000000 57.119999999999997000 552673.280000000260000000 5799986.460999999200000000 57.120000000000005000 552673.280000000260000000 5799986.460999999200000000 63.964571063253565000 552674.024882543600000000 5799981.677221176200000000 66.129364013671875000 552674.758000000380000000 5799976.969000000500000000 63.998762999507136000 552674.758000000380000000 5799976.969000000500000000 57.120000000000005000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="f6ab356f-73ad-47cd-a84b-5d101d83678a">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>66.40</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>171.15</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="dd24c4da-9f1e-4a1c-84ff-10f8d299e323" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="92480b9c-bc4a-45b4-bcd2-0be3c9a98e1c">
                  <gml:exterior>
                    <gml:LinearRing gml:id="c8eae380-ead2-4789-a714-1a07c64a54da">
                      <gml:posList>552674.758000000380000000 5799976.969000000500000000 63.998762999507136000 552665.225999999790000000 5799975.484999999400000000 64.006745435257329000 552665.225999999790000000 5799975.484999999400000000 57.119999999999997000 552674.758000000380000000 5799976.969000000500000000 57.120000000000005000 552674.758000000380000000 5799976.969000000500000000 63.998762999507136000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="d1e14a99-26f7-4784-b062-4f8cc0e76cb2">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>36.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>261.25</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="caba97ba-3af7-4c13-aff6-bf09948318bc" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="f0af8538-c154-4205-a913-f2070d3f4ee5">
                  <gml:exterior>
                    <gml:LinearRing gml:id="77075987-cda0-4e90-a021-70563eb70947">
                      <gml:posList>552665.225999999790000000 5799975.484999999400000000 64.006745435257329000 552664.527999999930000000 5799980.020999999700000000 66.058833816220641000 552664.527999999930000000 5799980.020999999700000000 60.889542043842646000 552664.527999999930000000 5799980.020999999700000000 57.119999999999997000 552664.558166357920000000 5799979.824961890500000000 57.119999999999976000 552665.225999999790000000 5799975.484999999400000000 57.119999999999997000 552665.225999999790000000 5799975.484999999400000000 64.006745435257329000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="5db54ca5-1465-4a8b-89e7-20b984a99ebf">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.83</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>171.17</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="8ea40ccc-5452-4d67-8da9-fc407379d5a0" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="5ec3d01f-d331-4c16-a4e2-3e2103e31dcc">
                  <gml:exterior>
                    <gml:LinearRing gml:id="633ac33f-94d8-494a-81b8-6943baa55ecf">
                      <gml:posList>552664.527999999930000000 5799980.020999999700000000 66.058833816220641000 552664.370679647890000000 5799979.996551566800000000 66.058985067128589000 552664.370679647890000000 5799979.996551566800000000 60.809995381900762000 552664.527999999930000000 5799980.020999999700000000 60.889542043842646000 552664.527999999930000000 5799980.020999999700000000 66.058833816220641000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="f8ceecd3-b697-48ab-889b-8a0d0ca35cfe">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>7.90</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>261.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="e6bb8ef8-90c7-458a-a2e6-14a2ea9b20df" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="60f40c7b-1e8b-4759-bcc2-e92b2ce7c277">
                  <gml:exterior>
                    <gml:LinearRing gml:id="e4804a36-cf6d-4f14-a5e9-00f66cebdc60">
                      <gml:posList>552664.370679647890000000 5799979.996551566800000000 66.058985067128589000 552664.346178901380000000 5799980.152030824700000000 66.129364013671875000 552664.126462846990000000 5799981.546326672700000000 65.498224695512533000 552664.126462846990000000 5799981.546326672700000000 60.806109373325143000 552664.346178901380000000 5799980.152030824700000000 60.809605522911710000 552664.370679647890000000 5799979.996551566800000000 60.809995381900762000 552664.370679647890000000 5799979.996551566800000000 66.058985067128589000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="fa8f2351-3e1b-4e88-9439-31a202519998">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>4.49</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>171.17</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="5f24b995-1310-463a-b494-fcfd1b772200" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="5acb6ab4-28bf-47ff-97a4-b94102d385e7">
                  <gml:exterior>
                    <gml:LinearRing gml:id="e8ccb6d1-6863-4a89-9f61-40459760cee9">
                      <gml:posList>552664.370679647890000000 5799979.996551566800000000 60.809995381900762000 552663.048000000420000000 5799979.790999999300000000 60.141202378064804000 552663.048000000420000000 5799979.790999999300000000 57.119999999999997000 552664.370679647890000000 5799979.996551566800000000 57.119999999999997000 552664.370679647890000000 5799979.996551566800000000 60.809995381900762000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="efbb2b97-d7ad-4cc7-8d72-cfa0c4d6e1bc">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>4.47</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>351.18</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="210d3314-5f10-40b8-bb60-a8965ac8d641" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="ccbd7556-5d67-44a9-8fea-e2bd03ad5483">
                  <gml:exterior>
                    <gml:LinearRing gml:id="26d5c39a-7e40-48de-a5d1-a8495f87725d">
                      <gml:posList>552664.126462846990000000 5799981.546326672700000000 57.119999999999997000 552662.809000000360000000 5799981.342000000200000000 57.119999999999983000 552662.809000000360000000 5799981.342000000200000000 60.139985364709744000 552664.126462846990000000 5799981.546326672700000000 60.806109373325143000 552664.126462846990000000 5799981.546326672700000000 57.119999999999997000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="b11dd5b5-4fbd-4799-a31d-e80ee0eb3093">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>4.74</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>261.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="c38638a5-0948-42e6-a496-d9316d5a76d9" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="f45fcf57-9694-4bef-b8e8-be50ac72fd40">
                  <gml:exterior>
                    <gml:LinearRing gml:id="0247b429-6c07-4420-8265-a072bd3ffab7">
                      <gml:posList>552663.048000000420000000 5799979.790999999300000000 60.141202378064804000 552662.809000000360000000 5799981.342000000200000000 60.139985364709744000 552662.809000000360000000 5799981.342000000200000000 57.119999999999983000 552663.048000000420000000 5799979.790999999300000000 57.119999999999997000 552663.048000000420000000 5799979.790999999300000000 60.141202378064804000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="06454410-76a3-4aa1-a6ba-24c48d97f885">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001ewbO_C:28911</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>51.43</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>351.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>24.09</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="7fe29777-63dd-43e7-bb4d-ec43d830334a" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="632dac39-7aba-471e-bd60-d5c55f883502">
                  <gml:exterior>
                    <gml:LinearRing gml:id="78994c2c-3143-40bf-8e67-b625a3a9ca92">
                      <gml:posList>552674.024882543600000000 5799981.677221176200000000 66.129364013671875000 552673.280000000260000000 5799986.460999999200000000 63.964571063253565000 552663.768000000160000000 5799984.980000000400000000 63.956655605168294000 552664.292000000370000000 5799981.572000000600000000 65.498406832001905000 552664.126462846990000000 5799981.546326672700000000 65.498224695512533000 552664.346178901380000000 5799980.152030824700000000 66.129364013671875000 552674.024882543600000000 5799981.677221176200000000 66.129364013671875000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="b1fd3d1f-7561-4ed3-8abb-b293224be444">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001ewbO_C:28911</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>50.26</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>171.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>24.09</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="48724609-0ede-4b6b-b57b-f056bc9e0766" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="20ee8688-2b1e-44ef-b993-7ff86bc3661b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="0a5800e7-d280-4e69-8649-27816b4678e9">
                      <gml:posList>552674.758000000380000000 5799976.969000000500000000 63.998762999507136000 552674.024882543600000000 5799981.677221176200000000 66.129364013671875000 552664.346178901380000000 5799980.152030824700000000 66.129364013671875000 552664.370679647890000000 5799979.996551566800000000 66.058985067128589000 552664.527999999930000000 5799980.020999999700000000 66.058833816220641000 552665.225999999790000000 5799975.484999999400000000 64.006745435257329000 552674.758000000380000000 5799976.969000000500000000 63.998762999507136000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="eef6ee63-c902-464a-a0d5-ce74259d0aa5">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001ewbO_C:28910</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>2.34</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>261.33</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>26.55</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="252c6a44-84b5-47b3-bb20-65c1c35060da" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="06ec8928-b333-4bd8-babe-d3ae1f3a2e72">
                  <gml:exterior>
                    <gml:LinearRing gml:id="cebd29a5-acd9-44b7-b87d-63e93217e813">
                      <gml:posList>552664.346178901380000000 5799980.152030824700000000 60.809605522911710000 552664.126462846990000000 5799981.546326672700000000 60.806109373325143000 552662.809000000360000000 5799981.342000000200000000 60.139985364709744000 552663.048000000420000000 5799979.790999999300000000 60.141202378064804000 552664.370679647890000000 5799979.996551566800000000 60.809995381900762000 552664.346178901380000000 5799980.152030824700000000 60.809605522911710000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </cityObjectMember>

</CityModel>
