<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<CityModel xmlns="http://www.opengis.net/citygml/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:core="http://www.opengis.net/citygml/base/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:tex="http://www.opengis.net/citygml/textures/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/citygml/2.0 http://schemas.opengis.net/citygml/2.0/cityGMLBase.xsd http://www.opengis.net/citygml/appearance/2.0  http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd">

  <gml:description>Exported by RhinoCity(TM) software - (c)Rhinoterrain 2010-2018 : www.rhinoterrain.com</gml:description>

  <gml:name>RhinoCity(TM)</gml:name>

  <gml:boundedBy>
    <gml:Envelope srsDimension="3" srsName="crs:ETRS89_UTM32*DE_DHHN92_NH">
      <gml:lowerCorner>552789.812500 5805468.000000 53.749996</gml:lowerCorner>
      <gml:upperCorner>552810.812500 5805484.000000 62.672176</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>

  <cityObjectMember>
    <bldg:Building gml:id="DENIAL430001c3d2">
      <gen:doubleAttribute name="Volume">
        <gen:value>1167.247</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="TotalRoofArea">
        <gen:value>193.86</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="TotalWallArea">
        <gen:value>430.87</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Authoring_Software">
        <gen:value>RhinoCity</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="OSKA">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ALKISID">
        <gen:value>DENIAL430001c3d2</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="KACHEL_ID">
        <gen:value>11_10_Building</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="COUNT">
        <gen:value>850</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="DESC">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCK">
        <gen:value>12</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCKNR">
        <gen:value>104012</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STATBEZNR">
        <gen:value>104</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="MBZNR">
        <gen:value>1031</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="WBZNR">
        <gen:value>1030</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNAM">
        <gen:value>List</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNR">
        <gen:value>10</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNAM">
        <gen:value>Vahrenwald-List</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNR">
        <gen:value>02</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEMEINDE">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SCALE">
        <gen:value>5000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_overlaps">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_xmax">
        <gen:value>552810.756</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_xmin">
        <gen:value>552789.8250000002</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_ymax">
        <gen:value>5805483.767999999</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_ymin">
        <gen:value>5805468.116</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_zmax">
        <gen:value>54.3384440000616</gen:value>
      </gen:stringAttribute>
      <gen:doubleAttribute name="ZMIN">
        <gen:value>53.750000</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="THICKNESS">
        <gen:value>0.100000</gen:value>
      </gen:doubleAttribute>
      <gen:intAttribute name="TEXTURE">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="BUILDINGID">
        <gen:value>DENIAL430001c3d2</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ARTBEZEICH">
        <gen:value>Gebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Join_Cou_1">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="FUNKTION_T">
        <gen:value>Wohngebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADT">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="OVERHANG">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="OBJFLAECHE">
        <gen:value>162.63</gen:value>
      </gen:stringAttribute>
      <bldg:measuredHeight uom="#m">14.267</bldg:measuredHeight>
      <bldg:lod2Solid>
        <gml:Solid gml:id="d36ad9c3-c568-4cb7-bdb6-09f6606d09f2">
          <gml:exterior>
            <gml:CompositeSurface gml:id="f7b772f8-11aa-43e8-b838-732dda31ae48">
              <gml:surfaceMember xlink:href="#da945ae2-ea34-4e34-97aa-a1ed2a05cf02"/>
              <gml:surfaceMember xlink:href="#763b3cba-f054-442e-9b75-e1dea73ade09"/>
              <gml:surfaceMember xlink:href="#0489dd78-9320-4f4b-94c5-613ed00fabe3"/>
              <gml:surfaceMember xlink:href="#ccba90ca-e715-4dff-a9c6-c7d4a8e39b9e"/>
              <gml:surfaceMember xlink:href="#e7ad9580-29e3-4093-922e-94f425a6186a"/>
              <gml:surfaceMember xlink:href="#637cc748-e461-4481-a9e3-b2cbd3a19145"/>
              <gml:surfaceMember xlink:href="#6c3be137-1fbd-47c8-88bb-ed051d4c143b"/>
              <gml:surfaceMember xlink:href="#2d4d90d5-8a81-4dad-89e1-7faa111dcc9c"/>
              <gml:surfaceMember xlink:href="#0a8c676e-5f66-44c4-a2e1-8215bf032d22"/>
              <gml:surfaceMember xlink:href="#ce937e6c-96e7-4e3f-b2be-734ecf4d3e7d"/>
              <gml:surfaceMember xlink:href="#ec5fdb80-72e2-4888-95a5-4eab828ad307"/>
              <gml:surfaceMember xlink:href="#aad620b0-406a-4a10-885f-55004d1c6506"/>
              <gml:surfaceMember xlink:href="#5e78d7dd-6131-4b1a-bc4e-094b485ab251"/>
              <gml:surfaceMember xlink:href="#26ba7287-ab1d-4d03-8ba6-9a159b2124e7"/>
              <gml:surfaceMember xlink:href="#b0dedef4-84c0-4e3c-982d-249be90edf9e"/>
              <gml:surfaceMember xlink:href="#5ecb7abb-c40f-4988-86fd-7715e0cd132a"/>
              <gml:surfaceMember xlink:href="#b78b00f3-580e-4389-b312-1441ddce9552"/>
              <gml:surfaceMember xlink:href="#df0d48db-e1c8-4928-a7b6-30cefd1b51cc"/>
              <gml:surfaceMember xlink:href="#e666cb9e-8a7e-45c6-99a4-879bc4887d99"/>
              <gml:surfaceMember xlink:href="#b08e92cd-b1ed-4902-a308-c2837d109b39"/>
              <gml:surfaceMember xlink:href="#d59024b3-e542-4158-b309-9a31c58a7cf3"/>
              <gml:surfaceMember xlink:href="#eb8dc911-180b-4db6-9ecf-2b6e61f88ddc"/>
              <gml:surfaceMember xlink:href="#ad84d983-e0ee-408b-a984-0b9879b8753c"/>
              <gml:surfaceMember xlink:href="#54214e89-d083-47bf-a6f2-d37ab5650a41"/>
              <gml:surfaceMember xlink:href="#4f730a1b-c2f6-4844-b415-e51d3debb03d"/>
              <gml:surfaceMember xlink:href="#99e451e1-83bb-4d23-93c6-7ed86d444aa9"/>
              <gml:surfaceMember xlink:href="#c8bd20ef-7210-44cd-afe2-8fb7be27f125"/>
              <gml:surfaceMember xlink:href="#29f8dabf-018f-4409-9ac2-cda0f6ef287c"/>
              <gml:surfaceMember xlink:href="#a3ad8245-e869-47af-b25c-a34ec7b96ca1"/>
              <gml:surfaceMember xlink:href="#f0552e4e-6d3f-411b-8dd6-40d13cf82781"/>
              <gml:surfaceMember xlink:href="#4423f923-b7f6-4e1d-bf36-ad36b493cd91"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="c3d8efb7-8538-4435-b97d-8b941452673b">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001c3d2_C:29257</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>42.55</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>241.17</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>47.28</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="e81dc8dc-c99a-499d-b870-278c9013a0db" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="da945ae2-ea34-4e34-97aa-a1ed2a05cf02">
                  <gml:exterior>
                    <gml:LinearRing gml:id="08166b57-375a-44e8-bfe8-9fb0ebbeff46">
                      <gml:posList>552797.442984798690000000 5805472.805355107400000000 60.329868316650312000 552799.341767877690000000 5805469.355554297600000000 60.329868316650241000 552797.809569380940000000 5805468.516900039300000000 58.438328871951455000 552798.029000000100000000 5805468.116000000400000000 58.437165523494073000 552801.459631668170000000 5805469.993388736600000000 62.672172546386719000 552794.897464812270000000 5805481.915850437200000000 62.672172546386733000 552791.377879375590000000 5805479.989796116000000000 58.327362060546875000 552793.564364697320000000 5805476.017283975100000000 58.327362060546875000 552793.672000000250000000 5805476.075999999400000000 58.460137008310582000 552792.048565865030000000 5805479.041985026600000000 58.468727553464959000 552793.556140601290000000 5805479.867161382000000000 60.329868316650405000 552795.454923680280000000 5805476.417360572100000000 60.329868316650362000 552798.119733165720000000 5805475.525918464200000000 62.392517089843693000 552797.442984798690000000 5805472.805355107400000000 60.329868316650312000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="37610fb4-f231-4962-99f7-534f504df98c">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001c3d2_C:29257</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>38.72</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>61.17</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>47.28</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="b615ebb9-6e48-4d03-a340-e05dc3c26026" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="763b3cba-f054-442e-9b75-e1dea73ade09">
                  <gml:exterior>
                    <gml:LinearRing gml:id="0407c0a0-0242-4e93-8d35-35064913540d">
                      <gml:posList>552800.002566434440000000 5805476.333440670700000000 60.743893439905321000 552797.262520077640000000 5805481.306651673300000000 60.746523928628235000 552799.083647620170000000 5805482.303538867300000000 58.498248742891619000 552798.281999999660000000 5805483.767999999200000000 58.494077075650843000 552794.897464812270000000 5805481.915850437200000000 62.672172546386733000 552801.459631668170000000 5805469.993388736600000000 62.672172546386719000 552802.822608707240000000 5805470.739268209800000000 60.989620208740234000 552799.803125814530000000 5805476.225210572600000000 60.989620208740128000 552800.002566434440000000 5805476.333440670700000000 60.743893439905321000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="7f413ff8-de6d-4777-877b-82829a185b91">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001c3d2_C:29256</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>8.38</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>241.17</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>16.30</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="d9543fff-62bf-437a-a59c-2fff720dac11" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="0489dd78-9320-4f4b-94c5-613ed00fabe3">
                  <gml:exterior>
                    <gml:LinearRing gml:id="aa8ca5ff-04be-4a0c-9e72-16896d2634a1">
                      <gml:posList>552793.564364697320000000 5805476.017283975100000000 58.327362060546875000 552791.377879375590000000 5805479.989796116000000000 58.327362060546875000 552789.825000000190000000 5805479.140000000600000000 57.809753732080253000 552792.002000000330000000 5805475.164999999100000000 57.806973158026935000 552793.564364697320000000 5805476.017283975100000000 58.327362060546875000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="be0edac2-1813-4881-bec4-ba919f696e11">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001c3d2_C:29255</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>11.91</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>61.30</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>9.61</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="dd9d39f6-5ce6-4225-bd36-6947f5dd31ab" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="ccba90ca-e715-4dff-a9c6-c7d4a8e39b9e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="e1a8155c-c11e-4e99-8761-af5c97c662a4">
                      <gml:posList>552801.810070867300000000 5805477.322870560000000000 60.394882202148437000 552799.083647624940000000 5805482.303538870100000000 60.394882202148437000 552797.262520077640000000 5805481.306651673300000000 60.746523928628235000 552800.002566434440000000 5805476.333440670700000000 60.743893439905321000 552801.810070867300000000 5805477.322870560000000000 60.394882202148437000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="d12a4647-e53b-42e0-8f57-09e9795bbd12">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001c3d2_C:29252</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>7.84</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>151.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>45.02</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="d8eec036-a20f-4741-ba0f-3afb285f54ea" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="e7ad9580-29e3-4093-922e-94f425a6186a">
                  <gml:exterior>
                    <gml:LinearRing gml:id="4b5874e4-84a0-46d1-bfb7-ca7fd2d86060">
                      <gml:posList>552798.119733165720000000 5805475.525918464200000000 62.392517089843693000 552794.929067622990000000 5805473.779442532900000000 62.392517089843750000 552795.918882988860000000 5805471.971132599700000000 60.329868316650391000 552797.442984798690000000 5805472.805355107400000000 60.329868316650312000 552798.119733165720000000 5805475.525918464200000000 62.392517089843693000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="4bafb361-97bf-4a11-bf31-edf32b339f0b">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001c3d2_C:29252</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>7.82</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>331.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>45.02</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="8db00a1e-4007-4ec8-8883-6c01989cef58" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="637cc748-e461-4481-a9e3-b2cbd3a19145">
                  <gml:exterior>
                    <gml:LinearRing gml:id="5b3f41f0-f909-4aa7-8f17-8881bc894982">
                      <gml:posList>552795.454923680280000000 5805476.417360572100000000 60.329868316650362000 552793.939252257110000000 5805475.587752466100000000 60.329868316650391000 552794.929067622990000000 5805473.779442532900000000 62.392517089843750000 552798.119733165720000000 5805475.525918464200000000 62.392517089843693000 552795.454923680280000000 5805476.417360572100000000 60.329868316650362000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="c1faf968-e730-4e5e-853d-4f0296c5f261">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001c3d2_C:29253</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>6.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="169f5db8-f787-4520-86d8-4559e8e96db2" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="6c3be137-1fbd-47c8-88bb-ed051d4c143b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="dc134333-d9dc-48bf-a99c-408ba64b4bb2">
                      <gml:posList>552799.341767877690000000 5805469.355554297600000000 60.329868316650241000 552797.442984798690000000 5805472.805355107400000000 60.329868316650312000 552795.918882988860000000 5805471.971132599700000000 60.329868316650391000 552796.554999999700000000 5805470.809000000400000000 60.329868316650391000 552797.809569380940000000 5805468.516900039300000000 60.329868316650391000 552799.341767877690000000 5805469.355554297600000000 60.329868316650241000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="53e42bd6-6f8e-4460-ba39-67a6ceaa053e">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001c3d2_C:29254</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>6.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="98677de8-8db8-46da-81cb-66e5443ce596" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="2d4d90d5-8a81-4dad-89e1-7faa111dcc9c">
                  <gml:exterior>
                    <gml:LinearRing gml:id="b1d20b52-7838-4e9c-9b4e-782414518985">
                      <gml:posList>552793.939252257110000000 5805475.587752466100000000 60.329868316650391000 552795.454923680280000000 5805476.417360572100000000 60.329868316650362000 552793.556140601290000000 5805479.867161382000000000 60.329868316650405000 552792.048565865030000000 5805479.041985026600000000 60.329868316650391000 552793.672000000250000000 5805476.075999999400000000 60.329868316650391000 552793.939252257110000000 5805475.587752466100000000 60.329868316650391000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="dfb1770f-2a3d-4030-819c-91960f85c50e">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001c3d2_C:29258</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>62.98</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="696e5fce-eee3-47cb-a9dd-f09693fa474d" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="0a8c676e-5f66-44c4-a2e1-8215bf032d22">
                  <gml:exterior>
                    <gml:LinearRing gml:id="14809827-ec33-411c-944a-568670c91485">
                      <gml:posList>552806.169999999930000000 5805468.571000000500000000 60.989620208740234000 552810.756000000050000000 5805471.083000000600000000 60.989620208740234000 552806.064000000250000000 5805479.650000000400000000 60.989620208740234000 552801.809999999590000000 5805477.323000000800000000 60.989620208740163000 552799.803125814530000000 5805476.225210572600000000 60.989620208740128000 552802.822608707240000000 5805470.739268209800000000 60.989620208740234000 552804.485000000340000000 5805471.649000000200000000 60.989620208740234000 552806.169999999930000000 5805468.571000000500000000 60.989620208740234000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="db9427f4-6347-44fa-b5a5-ab3d3d24fcd1">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>34.92</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>61.30</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="88dd496e-a050-4468-8a2b-2f1351259e11" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="ce937e6c-96e7-4e3f-b2be-734ecf4d3e7d">
                  <gml:exterior>
                    <gml:LinearRing gml:id="b628fb2e-3c48-495d-b824-b6296168d28d">
                      <gml:posList>552801.809999999590000000 5805477.323000000800000000 53.750000000000000000 552799.083647620170000000 5805482.303538867300000000 53.749999999999986000 552798.281999999660000000 5805483.767999999200000000 53.750000000000000000 552798.281999999660000000 5805483.767999999200000000 58.494077075650843000 552799.083647620170000000 5805482.303538867300000000 58.498248742891619000 552801.809999999590000000 5805477.323000000800000000 58.512436316964134000 552801.809999999590000000 5805477.323000000800000000 53.750000000000000000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="89d573b5-1dcd-44cc-a99a-ccdfb06d61a8">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>53.44</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>331.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="b8341797-9908-4da7-94fb-c7f416b1c633" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="ec5fdb80-72e2-4888-95a5-4eab828ad307">
                  <gml:exterior>
                    <gml:LinearRing gml:id="cc6f435f-e9d1-4185-95d4-f882e634b679">
                      <gml:posList>552798.281999999660000000 5805483.767999999200000000 53.750000000000000000 552794.897464812270000000 5805481.915850437200000000 53.750000000000000000 552791.377879375590000000 5805479.989796116000000000 53.750000000000000000 552791.377879375590000000 5805479.989796116000000000 58.327362060546875000 552794.897464812270000000 5805481.915850437200000000 62.672172546386733000 552798.281999999660000000 5805483.767999999200000000 58.494077075650843000 552798.281999999660000000 5805483.767999999200000000 53.750000000000000000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="9f6da7ab-d193-4824-9e3f-bcb1193133b0">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.57</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>151.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="99ea8e0d-1090-4450-8d2c-4d812a65a36d" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="aad620b0-406a-4a10-885f-55004d1c6506">
                  <gml:exterior>
                    <gml:LinearRing gml:id="ff601d52-6a04-4c37-9038-d395a6198dcd">
                      <gml:posList>552793.672000000250000000 5805476.075999999400000000 58.460137008310582000 552793.564364697320000000 5805476.017283975100000000 58.327362060546875000 552793.564364697320000000 5805476.017283975100000000 53.750000000000000000 552793.672000000250000000 5805476.075999999400000000 53.750000000000000000 552793.672000000250000000 5805476.075999999400000000 58.460137008310582000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="da336c74-34dd-400e-91af-e6c7875065c2">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>40.50</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>241.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="421d2d54-e657-4e37-876e-cafbc746b467" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="5e78d7dd-6131-4b1a-bc4e-094b485ab251">
                  <gml:exterior>
                    <gml:LinearRing gml:id="5e765cc7-7a05-4afb-aa03-e6122711ff59">
                      <gml:posList>552797.809569380940000000 5805468.516900039300000000 58.438328871951455000 552796.554999999700000000 5805470.809000000400000000 58.444980183239529000 552795.918882989790000000 5805471.971132600700000000 58.448324447628394000 552794.929067623450000000 5805473.779442532900000000 58.453528213971651000 552793.939252257230000000 5805475.587752466100000000 58.458731980314894000 552793.672000000250000000 5805476.075999999400000000 58.460137008310582000 552793.672000000250000000 5805476.075999999400000000 53.750000000000000000 552793.939252257230000000 5805475.587752466100000000 53.750000000000014000 552794.929067623450000000 5805473.779442532900000000 53.750000000000014000 552795.918882989790000000 5805471.971132600700000000 53.750000000000000000 552796.554999999700000000 5805470.809000000400000000 53.750000000000000000 552797.809569380940000000 5805468.516900039300000000 53.750000000000000000 552797.809569380940000000 5805468.516900039300000000 58.438328871951455000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="c6142700-ffc3-4a94-a89e-6d36acfc68c4">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>2.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>241.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="ca7476be-077e-46bd-8648-ba9bdf3306ec" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="26ba7287-ab1d-4d03-8ba6-9a159b2124e7">
                  <gml:exterior>
                    <gml:LinearRing gml:id="85e015c8-f425-4c26-a5b3-bed12f9c199b">
                      <gml:posList>552798.029000000100000000 5805468.116000000400000000 58.437165523494073000 552797.809569380940000000 5805468.516900039300000000 58.438328871951455000 552797.809569380940000000 5805468.516900039300000000 53.750000000000000000 552798.029000000100000000 5805468.116000000400000000 53.750000000000000000 552798.029000000100000000 5805468.116000000400000000 58.437165523494073000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="0c722f39-b168-4aac-9b1c-c25bfd4ebb18">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>38.81</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>151.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="cfaa7c64-19ad-46a9-84b5-d9e9c2f46854" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="b0dedef4-84c0-4e3c-982d-249be90edf9e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="2d988746-9bf1-4ba3-996b-a0db7021a24b">
                      <gml:posList>552802.779295607940000000 5805470.715565425300000000 60.989620208740234000 552802.822608707240000000 5805470.739268209800000000 60.989620208740234000 552801.459631668170000000 5805469.993388736600000000 62.672172546386719000 552798.029000000100000000 5805468.116000000400000000 58.437165523494073000 552798.029000000100000000 5805468.116000000400000000 53.750000000000000000 552801.459631668170000000 5805469.993388736600000000 53.750000000000000000 552802.779295607940000000 5805470.715565425300000000 53.750000000000007000 552802.779295607940000000 5805470.715565425300000000 60.989620208740234000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="7458b063-ce3d-4d09-8f5a-c1b9365e7248">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>37.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>151.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="0b2bc2c0-79f9-445d-a131-d75c5fd0554c" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="5ecb7abb-c40f-4988-86fd-7715e0cd132a">
                  <gml:exterior>
                    <gml:LinearRing gml:id="a954d0f1-2de3-40d1-ac09-2cf3ed8898e1">
                      <gml:posList>552810.756000000050000000 5805471.083000000600000000 60.989620208740234000 552806.169999999930000000 5805468.571000000500000000 60.989620208740234000 552806.169999999930000000 5805468.571000000500000000 53.750000000000014000 552810.756000000050000000 5805471.083000000600000000 53.750000000000028000 552810.756000000050000000 5805471.083000000600000000 60.989620208740234000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="1ea8d8cc-329d-4454-8d13-0c20f1c802e3">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>70.71</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>61.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="226393d0-1a2d-45e9-bf76-29f711a139d8" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="b78b00f3-580e-4389-b312-1441ddce9552">
                  <gml:exterior>
                    <gml:LinearRing gml:id="5ca73b75-c205-42a4-975c-f9b72950449e">
                      <gml:posList>552810.756000000050000000 5805471.083000000600000000 53.750000000000028000 552806.064000000250000000 5805479.650000000400000000 53.750000000000014000 552806.064000000250000000 5805479.650000000400000000 60.989620208740234000 552810.756000000050000000 5805471.083000000600000000 60.989620208740234000 552810.756000000050000000 5805471.083000000600000000 53.750000000000028000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="a2ee9c64-edf8-491c-bbf7-24314ec00e1d">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>14.08</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>151.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="1bb4d980-21d0-495e-bfd4-ccc21c1bf8b9" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="df0d48db-e1c8-4928-a7b6-30cefd1b51cc">
                  <gml:exterior>
                    <gml:LinearRing gml:id="9c802911-61be-4c29-b308-c6a3d4692d52">
                      <gml:posList>552804.485000000340000000 5805471.649000000200000000 60.989620208740234000 552802.822608707240000000 5805470.739268209800000000 60.989620208740234000 552802.779295607940000000 5805470.715565425300000000 60.989620208740234000 552802.779295607940000000 5805470.715565425300000000 53.750000000000007000 552804.485000000340000000 5805471.649000000200000000 53.750000000000014000 552804.485000000340000000 5805471.649000000200000000 58.937450380600595000 552804.485000000340000000 5805471.649000000200000000 60.989620208740234000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="7f1362a6-84a2-45e7-98d6-d881162358b1">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>25.40</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>241.30</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="5f4c70ee-70c4-4cf7-8183-52b078c6354c" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="e666cb9e-8a7e-45c6-99a4-879bc4887d99">
                  <gml:exterior>
                    <gml:LinearRing gml:id="5d28efb1-4785-4673-8cbf-ec2db2969e89">
                      <gml:posList>552806.169999999930000000 5805468.571000000500000000 60.989620208740234000 552804.485000000340000000 5805471.649000000200000000 60.989620208740234000 552804.485000000340000000 5805471.649000000200000000 58.937450380600595000 552804.485000000340000000 5805471.649000000200000000 53.750000000000014000 552804.612626252700000000 5805471.415864329800000000 53.749999999999993000 552806.169999999930000000 5805468.571000000500000000 53.750000000000014000 552806.169999999930000000 5805468.571000000500000000 60.989620208740234000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="9e63bc1a-52c1-4c8e-9f23-4a2eb792246b">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>36.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>331.32</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="e38cbac8-b124-471e-958c-abccef76621f" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="b08e92cd-b1ed-4902-a308-c2837d109b39">
                  <gml:exterior>
                    <gml:LinearRing gml:id="1ff576e6-597d-416d-89b2-e99ac5818d03">
                      <gml:posList>552801.809999999590000000 5805477.323000000800000000 53.750000000000000000 552801.809999999590000000 5805477.323000000800000000 58.512436316964134000 552801.810070867300000000 5805477.322870560000000000 60.394882202148437000 552800.002566434440000000 5805476.333440670700000000 60.743893439905321000 552799.803125814530000000 5805476.225210572600000000 60.989620208740128000 552806.064000000250000000 5805479.650000000400000000 60.989620208740234000 552806.064000000250000000 5805479.650000000400000000 53.750000000000014000 552801.959936687260000000 5805477.405017553800000000 53.750000000000000000 552801.809999999590000000 5805477.323000000800000000 53.750000000000000000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="6f6ff7a1-c95d-405d-8862-fd93b0c3d6a8">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>7.64</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>331.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="8909cc98-f38e-40ec-8167-1e89e6598b80" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="d59024b3-e542-4158-b309-9a31c58a7cf3">
                  <gml:exterior>
                    <gml:LinearRing gml:id="180c0b71-ecc7-4ee9-a808-029dea0a19c6">
                      <gml:posList>552791.377879375590000000 5805479.989796116000000000 53.750000000000000000 552789.825000000190000000 5805479.140000000600000000 53.750000000000000000 552789.825000000190000000 5805479.140000000600000000 57.809753732080253000 552791.377879375590000000 5805479.989796116000000000 58.327362060546875000 552791.377879375590000000 5805479.989796116000000000 53.750000000000000000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="a6803ce7-b56a-4b09-9f2f-47ea0ff6cee7">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>18.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>241.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="1e2eda28-a9b7-41f7-9f10-92d23d652212" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="eb8dc911-180b-4db6-9ecf-2b6e61f88ddc">
                  <gml:exterior>
                    <gml:LinearRing gml:id="dbbe7cae-9832-4e27-bbfb-1a3a9a22c487">
                      <gml:posList>552792.002000000330000000 5805475.164999999100000000 57.806973158026935000 552789.825000000190000000 5805479.140000000600000000 57.809753732080253000 552789.825000000190000000 5805479.140000000600000000 53.750000000000000000 552792.002000000330000000 5805475.164999999100000000 53.750000000000000000 552792.002000000330000000 5805475.164999999100000000 57.806973158026935000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="dc53fba1-0ff0-49a3-9f86-d1b597059cd1">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>7.68</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>151.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="d355ba96-b710-4ee2-8503-1fcf079a8dfc" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="ad84d983-e0ee-408b-a984-0b9879b8753c">
                  <gml:exterior>
                    <gml:LinearRing gml:id="b57de2ad-2681-4ce1-805e-fee6c996fd2f">
                      <gml:posList>552793.564364697320000000 5805476.017283975100000000 58.327362060546875000 552792.002000000330000000 5805475.164999999100000000 57.806973158026935000 552792.002000000330000000 5805475.164999999100000000 53.750000000000000000 552793.564364697320000000 5805476.017283975100000000 53.750000000000000000 552793.564364697320000000 5805476.017283975100000000 58.327362060546875000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="d26b6ea8-b8ae-4a02-b88f-62edc04ad3fd">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>10.73</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>61.30</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="75b04e40-2ca0-422e-b6dd-c10e31f9dd60" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="54214e89-d083-47bf-a6f2-d37ab5650a41">
                  <gml:exterior>
                    <gml:LinearRing gml:id="2a4316c1-317a-4e37-b1b0-b8354d49e518">
                      <gml:posList>552801.809999999590000000 5805477.323000000800000000 58.512436316964134000 552799.083647620170000000 5805482.303538867300000000 58.498248742891619000 552799.083647624940000000 5805482.303538870100000000 60.394882202148437000 552801.810070867300000000 5805477.322870560000000000 60.394882202148437000 552801.809999999590000000 5805477.323000000800000000 58.512436316964134000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="ab9827fe-5495-4b76-be92-bf7f8720085d">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>1.97</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>331.30</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="0215aba3-d29e-4fed-b616-47a5088c1d27" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="4f730a1b-c2f6-4844-b415-e51d3debb03d">
                  <gml:exterior>
                    <gml:LinearRing gml:id="1488fe1c-7c44-45fc-91bf-63e12925769f">
                      <gml:posList>552799.083647624940000000 5805482.303538870100000000 60.394882202148437000 552799.083647620170000000 5805482.303538867300000000 58.498248742891619000 552797.262520077640000000 5805481.306651673300000000 60.746523928628235000 552799.083647624940000000 5805482.303538870100000000 60.394882202148437000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="4384ce83-105d-4356-8afd-4d01fbf21a03">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>11.99</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>241.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="c121dc65-3ca8-4f14-945c-e4fda2b6d034" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="99e451e1-83bb-4d23-93c6-7ed86d444aa9">
                  <gml:exterior>
                    <gml:LinearRing gml:id="705769f1-0957-4fd3-b2f1-069ff9106ef0">
                      <gml:posList>552795.918882988860000000 5805471.971132599700000000 60.329868316650391000 552794.929067622990000000 5805473.779442532900000000 62.392517089843750000 552793.939252257110000000 5805475.587752466100000000 60.329868316650391000 552793.939252257230000000 5805475.587752466100000000 58.458731980314894000 552794.929067623450000000 5805473.779442532900000000 58.453528213971651000 552795.918882989790000000 5805471.971132600700000000 58.448324447628394000 552795.918882988860000000 5805471.971132599700000000 60.329868316650391000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="a779cf2a-7be8-48b5-9235-99233f605aea">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>7.43</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>241.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="3c24ed70-11d5-4e46-b421-f31b305f2cce" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="c8bd20ef-7210-44cd-afe2-8fb7be27f125">
                  <gml:exterior>
                    <gml:LinearRing gml:id="0fa0bb11-5fe0-427e-bace-1d35e3c216c7">
                      <gml:posList>552797.809569380940000000 5805468.516900039300000000 60.329868316650391000 552796.554999999700000000 5805470.809000000400000000 60.329868316650391000 552795.918882988860000000 5805471.971132599700000000 60.329868316650391000 552795.918882989790000000 5805471.971132600700000000 58.448324447628394000 552796.554999999700000000 5805470.809000000400000000 58.444980183239529000 552797.809569380940000000 5805468.516900039300000000 58.438328871951455000 552797.809569380940000000 5805468.516900039300000000 60.329868316650391000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="7ab3156a-c270-45e3-b4d4-3f339effc74f">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>1.65</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>151.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="f4d6e59a-d4fc-46af-8bf1-86bb72926b33" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="29f8dabf-018f-4409-9ac2-cda0f6ef287c">
                  <gml:exterior>
                    <gml:LinearRing gml:id="b7f68155-386b-49fb-a637-8ee9ad76d3f4">
                      <gml:posList>552799.341767877690000000 5805469.355554297600000000 60.329868316650241000 552797.809569380940000000 5805468.516900039300000000 60.329868316650391000 552797.809569380940000000 5805468.516900039300000000 58.438328871951455000 552799.341767877690000000 5805469.355554297600000000 60.329868316650241000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="e2370b02-97d2-4c48-abd9-ddb488b3c801">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>1.60</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>331.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="b666f6cc-6128-41ce-a63f-ebbb83e41889" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="a3ad8245-e869-47af-b25c-a34ec7b96ca1">
                  <gml:exterior>
                    <gml:LinearRing gml:id="186ef208-3253-4274-8be0-82ffa4bbe8bf">
                      <gml:posList>552793.556140601290000000 5805479.867161382000000000 60.329868316650405000 552792.048565865030000000 5805479.041985026600000000 58.468727553464959000 552792.048565865030000000 5805479.041985026600000000 60.329868316650391000 552793.556140601290000000 5805479.867161382000000000 60.329868316650405000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="2163bdd3-a8a8-4aab-ae58-471d1df184bf">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>7.35</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>241.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="7a6a48de-c5c6-45ad-860d-78fd26d20921" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="f0552e4e-6d3f-411b-8dd6-40d13cf82781">
                  <gml:exterior>
                    <gml:LinearRing gml:id="c94aef4a-60f4-4e77-9f0e-f8c0e2bee47d">
                      <gml:posList>552793.939252257110000000 5805475.587752466100000000 60.329868316650391000 552793.672000000250000000 5805476.075999999400000000 60.329868316650391000 552792.048565865030000000 5805479.041985026600000000 60.329868316650391000 552792.048565865030000000 5805479.041985026600000000 58.468727553464959000 552793.672000000250000000 5805476.075999999400000000 58.460137008310582000 552793.939252257230000000 5805475.587752466100000000 58.458731980314894000 552793.939252257110000000 5805475.587752466100000000 60.329868316650391000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="3f180faa-2462-4a03-970c-92d126df6959">
          <gen:stringAttribute name="Type">
            <gen:value>Ground</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>162.63</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="f2fc2fa6-0a90-496e-b7f0-7f6280613e20" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="4423f923-b7f6-4e1d-bf36-ad36b493cd91">
                  <gml:exterior>
                    <gml:LinearRing gml:id="fc3642f5-7758-4d4b-8476-3575e08eb066">
                      <gml:posList>552802.779295607940000000 5805470.715565425300000000 53.750000000000007000 552801.459631668170000000 5805469.993388736600000000 53.750000000000000000 552798.029000000100000000 5805468.116000000400000000 53.750000000000000000 552797.809569380940000000 5805468.516900039300000000 53.750000000000000000 552796.554999999700000000 5805470.809000000400000000 53.750000000000000000 552795.918882989790000000 5805471.971132600700000000 53.750000000000000000 552794.929067623450000000 5805473.779442532900000000 53.750000000000014000 552793.939252257230000000 5805475.587752466100000000 53.750000000000014000 552793.672000000250000000 5805476.075999999400000000 53.750000000000000000 552793.564364697320000000 5805476.017283975100000000 53.750000000000000000 552792.002000000330000000 5805475.164999999100000000 53.750000000000000000 552789.825000000190000000 5805479.140000000600000000 53.750000000000000000 552791.377879375590000000 5805479.989796116000000000 53.750000000000000000 552794.897464812270000000 5805481.915850437200000000 53.750000000000000000 552798.281999999660000000 5805483.767999999200000000 53.750000000000000000 552799.083647620170000000 5805482.303538867300000000 53.749999999999986000 552801.809999999590000000 5805477.323000000800000000 53.750000000000000000 552801.959936687260000000 5805477.405017553800000000 53.750000000000000000 552806.064000000250000000 5805479.650000000400000000 53.750000000000014000 552810.756000000050000000 5805471.083000000600000000 53.750000000000028000 552806.169999999930000000 5805468.571000000500000000 53.750000000000014000 552804.612626252700000000 5805471.415864329800000000 53.749999999999993000 552804.485000000340000000 5805471.649000000200000000 53.750000000000014000 552802.779295607940000000 5805470.715565425300000000 53.750000000000007000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </cityObjectMember>

</CityModel>
