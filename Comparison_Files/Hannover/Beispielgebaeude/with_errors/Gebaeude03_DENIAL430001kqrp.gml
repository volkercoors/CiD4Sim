<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<CityModel xmlns="http://www.opengis.net/citygml/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:core="http://www.opengis.net/citygml/base/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:tex="http://www.opengis.net/citygml/textures/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/citygml/2.0 http://schemas.opengis.net/citygml/2.0/cityGMLBase.xsd http://www.opengis.net/citygml/appearance/2.0  http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd">

  <gml:description>Exported by RhinoCity(TM) software - (c)Rhinoterrain 2010-2018 : www.rhinoterrain.com</gml:description>

  <gml:name>RhinoCity(TM)</gml:name>

  <gml:boundedBy>
    <gml:Envelope srsDimension="3" srsName="crs:ETRS89_UTM32*DE_DHHN92_NH">
      <gml:lowerCorner>552620.875000 5806290.500000 51.839996</gml:lowerCorner>
      <gml:upperCorner>552636.375000 5806323.000000 63.921291</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>

  <cityObjectMember>
    <bldg:Building gml:id="DENIAL430001kqrp">
      <gen:doubleAttribute name="Volume">
        <gen:value>3541.915</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="TotalRoofArea">
        <gen:value>450.69</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="TotalWallArea">
        <gen:value>769.66</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Authoring_Software">
        <gen:value>RhinoCity</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="OSKA">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ALKISID">
        <gen:value>DENIAL430001kqrp</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="KACHEL_ID">
        <gen:value>11_11_Building</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="COUNT">
        <gen:value>1021</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="DESC">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCK">
        <gen:value>02</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCKNR">
        <gen:value>103002</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STATBEZNR">
        <gen:value>103</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="MBZNR">
        <gen:value>1023</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="WBZNR">
        <gen:value>1023</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNAM">
        <gen:value>List</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNR">
        <gen:value>10</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNAM">
        <gen:value>Vahrenwald-List</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNR">
        <gen:value>02</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEMEINDE">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SCALE">
        <gen:value>5000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_overlaps">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_xmax">
        <gen:value>552636.3370000003</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_xmin">
        <gen:value>552620.9029999999</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_ymax">
        <gen:value>5806322.85</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_ymin">
        <gen:value>5806290.933</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_zmax">
        <gen:value>52.38067000003718</gen:value>
      </gen:stringAttribute>
      <gen:doubleAttribute name="ZMIN">
        <gen:value>51.840000</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="THICKNESS">
        <gen:value>0.100000</gen:value>
      </gen:doubleAttribute>
      <gen:intAttribute name="TEXTURE">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="BUILDINGID">
        <gen:value>DENIAL430001kqrp</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ARTBEZEICH">
        <gen:value>Gebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Join_Cou_1">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="FUNKTION_T">
        <gen:value>Wohngebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADT">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="OVERHANG">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="OBJFLAECHE">
        <gen:value>359.63</gen:value>
      </gen:stringAttribute>
      <bldg:measuredHeight uom="#m">17.783</bldg:measuredHeight>
      <bldg:lod2Solid>
        <gml:Solid gml:id="6c3fcac4-dda5-45be-8b92-4ddfb8d4b7e2">
          <gml:exterior>
            <gml:CompositeSurface gml:id="becbf0e4-8ed4-4ee6-a508-93646a9ec3d1">
              <gml:surfaceMember xlink:href="#a1bcc4a4-6dc4-41a9-9dbe-92dcd868f2d6"/>
              <gml:surfaceMember xlink:href="#158f8c8b-a389-4b3a-8daa-b005026bf08a"/>
              <gml:surfaceMember xlink:href="#17cd48ac-5e71-45bc-960f-4f6225d3c13b"/>
              <gml:surfaceMember xlink:href="#345202f5-0cf3-4478-b675-eb51a5e317f7"/>
              <gml:surfaceMember xlink:href="#16c188c9-3836-46bf-88f7-bee1d9fdccd0"/>
              <gml:surfaceMember xlink:href="#abdf394b-6140-4e3f-9a24-3628037ef862"/>
              <gml:surfaceMember xlink:href="#f3d73020-f177-4eb7-b6bf-bd3671439984"/>
              <gml:surfaceMember xlink:href="#da259e82-a5e5-4656-a128-72c4f1d2a2c5"/>
              <gml:surfaceMember xlink:href="#d6ff9a39-3d9e-4248-b7b5-e4c612754b4d"/>
              <gml:surfaceMember xlink:href="#472da61f-4e8f-4211-87e0-058355e31292"/>
              <gml:surfaceMember xlink:href="#ceecd2c8-32bf-44fa-b55d-4a1a8dc6e831"/>
              <gml:surfaceMember xlink:href="#6882bab2-69c9-4467-88bf-ac08be8ad9e2"/>
              <gml:surfaceMember xlink:href="#14385764-55fa-43f3-b952-5619b0a60bed"/>
              <gml:surfaceMember xlink:href="#0a60cbc5-2466-4299-aaa1-219b4a943c4a"/>
              <gml:surfaceMember xlink:href="#abff5654-5b1b-4626-a5d5-a225d7e8d790"/>
              <gml:surfaceMember xlink:href="#14b03272-88d0-4920-830d-d2e42b22d6b1"/>
              <gml:surfaceMember xlink:href="#1811c2c4-f32a-4214-acb3-7c329af88c7e"/>
              <gml:surfaceMember xlink:href="#f7fa4228-a6bb-4409-be8a-004eac834053"/>
              <gml:surfaceMember xlink:href="#6b99075c-2130-4d01-b60a-11c090967e31"/>
              <gml:surfaceMember xlink:href="#bb8fd881-a5f9-492a-be03-01425161ff5f"/>
              <gml:surfaceMember xlink:href="#cc489525-b847-427c-92e1-e92107797f8c"/>
              <gml:surfaceMember xlink:href="#6473701a-0d6a-46b4-a0a3-7a571b2b7d3a"/>
              <gml:surfaceMember xlink:href="#4cb0005f-fa3b-4c4e-8191-2d63ea182751"/>
              <gml:surfaceMember xlink:href="#d4243ccd-e37b-4d9f-9ca7-8273ad5c3ac3"/>
              <gml:surfaceMember xlink:href="#91b2226d-a781-4c52-8d90-a3f2ad9e334b"/>
              <gml:surfaceMember xlink:href="#92862c70-41f5-4e67-9d79-c4dab8a2345f"/>
              <gml:surfaceMember xlink:href="#71b79a5c-b444-4cb2-b8f6-652de4f31878"/>
              <gml:surfaceMember xlink:href="#610eada9-6af1-4c23-9c6d-4343344e0193"/>
              <gml:surfaceMember xlink:href="#2d6d4320-3555-432a-87ed-7b38b31a5b62"/>
              <gml:surfaceMember xlink:href="#1d94e430-1dd5-4420-88af-2687a46a7de0"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="0c8f886d-fc5b-411d-9e58-e50773e04e65">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001kqrp_C:29190</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>58.55</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>262.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>7.42</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="67db533d-6f2f-4944-9670-fdb07c482ecf" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="a1bcc4a4-6dc4-41a9-9dbe-92dcd868f2d6">
                  <gml:exterior>
                    <gml:LinearRing gml:id="59fc8811-5232-482f-bafc-4e1adb1eea79">
                      <gml:posList>552627.396109029070000000 5806306.626807765100000000 62.656706927860228000 552626.707846409990000000 5806311.561566745900000000 62.656249465758883000 552622.874989314350000000 5806311.029744409000000000 62.152479357583623000 552622.883000000380000000 5806310.971999999100000000 62.152479157328770000 552620.902999999930000000 5806310.697000000600000000 61.892233817216876000 552622.241000000390000000 5806301.053999999500000000 61.892233817208577000 552624.222000000070000000 5806301.328999999900000000 62.152608109212473000 552624.245528604840000000 5806301.159395875400000000 62.152607521032039000 552628.084371648150000000 5806301.692048783400000000 62.657164389961196000 552627.740240338610000000 5806304.159428274300000000 62.656935658910839000 552627.396109029070000000 5806306.626807765100000000 62.656706927860228000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="b2aa5a24-e1c3-4123-806e-673d4f073aae">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001kqrp_C:29189</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>8.68</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="3b439a4e-57f9-43b9-806f-80b98cf1cfae" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="158f8c8b-a389-4b3a-8daa-b005026bf08a">
                  <gml:exterior>
                    <gml:LinearRing gml:id="ce988943-c4e7-430e-8f0a-eba1ce384825">
                      <gml:posList>552636.337000000290000000 5806306.803999999500000000 56.564559936523438000 552635.774000000210000000 5806310.888000000300000000 56.564559936523438000 552633.965221886520000000 5806310.638733758600000000 56.564559936523438000 552634.794683739540000000 5806304.686866828200000000 56.564559936523438000 552635.406000000420000000 5806304.770999999700000000 56.564559936523438000 552635.149000000210000000 5806306.640000000600000000 56.564559936523438000 552636.337000000290000000 5806306.803999999500000000 56.564559936523438000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="4527fa41-387d-4017-aae1-d99ce8143610">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001kqrp_C:29191</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>165.48</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>262.07</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>40.20</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="677ef2e0-33e7-458e-a448-b63cc05e3bec" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="17cd48ac-5e71-45bc-960f-4f6225d3c13b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="f9fa3b91-89a7-46c9-8ad9-28a4707be5d4">
                      <gml:posList>552624.245528604840000000 5806301.159395875400000000 59.382124463673605000 552625.622999999670000000 5806291.230000000400000000 59.376847205773629000 552625.663999999870000000 5806290.933000000200000000 59.376520458962247000 552630.991412530300000000 5806291.670318299000000000 63.921287536621094000 552626.749734129870000000 5806322.106805895500000000 63.921287536621094000 552621.440999999640000000 5806321.368000000700000000 59.391979798321913000 552621.481999999840000000 5806321.071000000500000000 59.391653051510573000 552622.874989314350000000 5806311.029744409000000000 59.386316343164495000 552626.707846409990000000 5806311.561566745900000000 62.656249465758890000 552627.740240338610000000 5806304.159428274300000000 62.656935658910839000 552628.084371648150000000 5806301.692048783400000000 62.657164389961196000 552624.245528604840000000 5806301.159395875400000000 59.382124463673605000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="39174167-c7a8-491a-979e-6329cbf7e42b">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001kqrp_C:29191</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>217.99</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>82.07</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>40.20</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="3480afc2-64cb-4de1-a59f-92db78b8043f" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="345202f5-0cf3-4478-b675-eb51a5e317f7">
                  <gml:exterior>
                    <gml:LinearRing gml:id="956640e4-1e91-4a13-8311-25d2bba227c5">
                      <gml:posList>552633.965221886520000000 5806310.638733758600000000 59.219917297363281000 552633.792999999600000000 5806310.615000000200000000 59.366826334904381000 552632.089999999850000000 5806322.849999999600000000 59.365077537042112000 552626.749734129870000000 5806322.106805895500000000 63.921287536621094000 552630.991412530300000000 5806291.670318299000000000 63.921287536621094000 552636.307000000030000000 5806292.405999999500000000 59.386608310525602000 552634.614000000060000000 5806304.662000000500000000 59.374040625160561000 552634.794683739540000000 5806304.686866828200000000 59.219917297363281000 552633.965221886520000000 5806310.638733758600000000 59.219917297363281000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="2ab2ce12-fa29-4294-917f-03e562eca242">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>97.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>262.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="73c04df5-86bc-4532-a71b-02ce6eb2d8da" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="16c188c9-3836-46bf-88f7-bee1d9fdccd0">
                  <gml:exterior>
                    <gml:LinearRing gml:id="21433775-20c9-4c1a-b509-1d25671ad4e9">
                      <gml:posList>552622.241000000390000000 5806301.053999999500000000 61.892233817208577000 552620.902999999930000000 5806310.697000000600000000 61.892233817216876000 552620.902999999930000000 5806310.697000000600000000 51.840000000000011000 552622.241000000390000000 5806301.053999999500000000 51.839999999995854000 552622.241000000390000000 5806301.053999999500000000 61.892233817208577000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="e7545b77-0b01-479a-8ddc-0583167c098e">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>5.35</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>352.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="d3bf2c2a-2713-4c12-aa32-54caa3dfb12d" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="abdf394b-6140-4e3f-9a24-3628037ef862">
                  <gml:exterior>
                    <gml:LinearRing gml:id="b56e4b26-814a-4096-98ab-cd60acbc94d2">
                      <gml:posList>552626.707846409990000000 5806311.561566745900000000 62.656249465758890000 552622.874989314350000000 5806311.029744409000000000 59.386316343164495000 552622.874989314350000000 5806311.029744409000000000 62.152479357583623000 552626.707846409990000000 5806311.561566745900000000 62.656249465758890000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="dd52a557-6d7f-43a3-85d9-845af2dc9d7a">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.16</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>262.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="32056d6d-d72c-41c8-9e5e-31ec1429f70a" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="f3d73020-f177-4eb7-b6bf-bd3671439984">
                  <gml:exterior>
                    <gml:LinearRing gml:id="4a5b2091-3180-4f20-9e6c-6bc3c82cb243">
                      <gml:posList>552622.883000000380000000 5806310.971999999100000000 62.152479157328770000 552622.874989314350000000 5806311.029744409000000000 62.152479357583623000 552622.874989314350000000 5806311.029744409000000000 59.386316343164495000 552622.883000000380000000 5806310.971999999100000000 59.386285653270022000 552622.883000000380000000 5806310.971999999100000000 62.152479157328770000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="2ac0c0e1-3f0b-49ce-a5a5-98039d514251">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>18.89</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>352.09</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="9bbbbcc6-560a-4f5e-9d54-6405a6802fad" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="da259e82-a5e5-4656-a128-72c4f1d2a2c5">
                  <gml:exterior>
                    <gml:LinearRing gml:id="40a77710-0f2e-4d2c-955d-3d155927f1e2">
                      <gml:posList>552622.687994687820000000 5806310.944915927900000000 59.219917297363281000 552622.687994687820000000 5806310.944915927900000000 51.840000000000003000 552620.902999999930000000 5806310.697000000600000000 51.840000000000011000 552620.902999999930000000 5806310.697000000600000000 61.892233817216876000 552622.883000000380000000 5806310.971999999100000000 62.152479157328770000 552622.883000000380000000 5806310.971999999100000000 59.386285653270022000 552622.687994687820000000 5806310.944915927900000000 59.219917297363281000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="2fda16e4-dc4e-44f8-8b6f-5f86e673640d">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>18.93</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>172.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="14234f2c-1464-42c6-8e82-ee0d27e47996" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="d6ff9a39-3d9e-4248-b7b5-e4c612754b4d">
                  <gml:exterior>
                    <gml:LinearRing gml:id="0c8990d9-cd31-4079-b31d-1aa0508172f2">
                      <gml:posList>552624.222000000070000000 5806301.328999999900000000 59.382214604576376000 552624.222000000070000000 5806301.328999999900000000 62.152608109212473000 552622.241000000390000000 5806301.053999999500000000 61.892233817208577000 552622.241000000390000000 5806301.053999999500000000 51.839999999995854000 552624.031764661780000000 5806301.302591762500000000 51.840000000000011000 552624.031764661780000000 5806301.302591762500000000 59.219917297363281000 552624.222000000070000000 5806301.328999999900000000 59.382214604576376000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="e044a2c2-9a16-408d-b8af-14b87be23d1d">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.47</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>262.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="0a2d1d66-fb75-4968-ad60-69b3573913e6" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="472da61f-4e8f-4211-87e0-058355e31292">
                  <gml:exterior>
                    <gml:LinearRing gml:id="fb56206b-cbf3-4521-bee0-fd740c47133b">
                      <gml:posList>552624.245528604840000000 5806301.159395875400000000 62.152607521032039000 552624.222000000070000000 5806301.328999999900000000 62.152608109212473000 552624.222000000070000000 5806301.328999999900000000 59.382214604576376000 552624.245528604840000000 5806301.159395875400000000 59.382124463673605000 552624.245528604840000000 5806301.159395875400000000 62.152607521032039000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="6020c0d0-42f6-4565-9265-8786691a1a20">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>5.37</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>172.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="52dff559-d55d-47aa-aadf-299e23e2c196" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="ceecd2c8-32bf-44fa-b55d-4a1a8dc6e831">
                  <gml:exterior>
                    <gml:LinearRing gml:id="470a4e91-bd2c-4755-893c-a5ae5cc0800b">
                      <gml:posList>552628.084371648150000000 5806301.692048783400000000 62.657164389961196000 552624.245528604840000000 5806301.159395875400000000 62.152607521032039000 552624.245528604840000000 5806301.159395875400000000 59.382124463673605000 552628.084371648150000000 5806301.692048783400000000 62.657164389961196000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="83123d40-cd5b-4bee-8a1d-b6ef1ad68447">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>1.43</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>172.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="eda7146d-03ff-40bc-91e8-86a47c4ffc28" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="6882bab2-69c9-4467-88bf-ac08be8ad9e2">
                  <gml:exterior>
                    <gml:LinearRing gml:id="a92d7159-67de-4d11-8e77-c929ba64745b">
                      <gml:posList>552624.222000000070000000 5806301.328999999900000000 59.382214604576376000 552624.031764661780000000 5806301.302591762500000000 59.219917297363281000 552624.031764661780000000 5806301.302591762500000000 51.840000000000011000 552624.222000000070000000 5806301.328999999900000000 51.840000000000011000 552624.222000000070000000 5806301.328999999900000000 59.382214604576376000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="97cde23f-9b37-4289-bf64-acbe4ffbdf79">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>76.87</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>262.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="f1799a39-3670-4216-88f7-837a51246195" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="14385764-55fa-43f3-b952-5619b0a60bed">
                  <gml:exterior>
                    <gml:LinearRing gml:id="d89fb733-71a8-41dd-8048-23a452f5f3ee">
                      <gml:posList>552625.622999999670000000 5806291.230000000400000000 59.376847205773629000 552624.245528604840000000 5806301.159395875400000000 59.382124463673605000 552624.222000000070000000 5806301.328999999900000000 59.382214604576376000 552624.222000000070000000 5806301.328999999900000000 51.840000000000011000 552624.245528604840000000 5806301.159395875400000000 51.840000000000018000 552625.622999999670000000 5806291.230000000400000000 51.840000000000003000 552625.622999999670000000 5806291.230000000400000000 59.376847205773629000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="30b986ec-aacb-4a7b-95a6-a7052aa99e72">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>2.26</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>262.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="1a172d63-3fdf-499f-ad9f-db7f6d7a455d" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="0a60cbc5-2466-4299-aaa1-219b4a943c4a">
                  <gml:exterior>
                    <gml:LinearRing gml:id="d190bc48-a28b-4ea1-a9f5-f7d20ce43823">
                      <gml:posList>552625.663999999870000000 5806290.933000000200000000 59.376520458962247000 552625.622999999670000000 5806291.230000000400000000 59.376847205773629000 552625.622999999670000000 5806291.230000000400000000 51.840000000000003000 552625.663999999870000000 5806290.933000000200000000 51.840000000000011000 552625.663999999870000000 5806290.933000000200000000 59.376520458962247000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="820d8dd6-bc43-42b0-8dde-0b94f4b606cd">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>105.42</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>172.12</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="61399fd6-234f-4b38-ade3-7fe90732c7c4" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="abff5654-5b1b-4626-a5d5-a225d7e8d790">
                  <gml:exterior>
                    <gml:LinearRing gml:id="60f933cc-2897-4d41-8baa-1f71b68e49a7">
                      <gml:posList>552636.307000000030000000 5806292.405999999500000000 59.386608310525602000 552630.991412530300000000 5806291.670318299000000000 63.921287536621094000 552625.663999999870000000 5806290.933000000200000000 59.376520458962247000 552625.663999999870000000 5806290.933000000200000000 51.840000000000011000 552630.991412530300000000 5806291.670318299000000000 51.840000000000003000 552636.307000000030000000 5806292.405999999500000000 51.840000000000011000 552636.307000000030000000 5806292.405999999500000000 59.386608310525602000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="570ca4a7-45d2-468a-8f2e-0360ef339ed1">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>93.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>82.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="42144e95-bf83-45d1-a00a-a529b77fc92b" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="14b03272-88d0-4920-830d-d2e42b22d6b1">
                  <gml:exterior>
                    <gml:LinearRing gml:id="2268b22b-efe1-42d3-871a-4bb9735d0139">
                      <gml:posList>552636.307000000030000000 5806292.405999999500000000 59.386608310525602000 552636.307000000030000000 5806292.405999999500000000 51.840000000000011000 552634.614000000060000000 5806304.662000000500000000 51.840000000000011000 552634.614000000060000000 5806304.662000000500000000 56.564559936523438000 552634.614000000060000000 5806304.662000000500000000 59.374040625160561000 552636.307000000030000000 5806292.405999999500000000 59.386608310525602000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bbad1900-ab61-4781-89cd-44bbe2c7a8ea">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>1.36</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>172.16</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="aec6ae54-dd66-4f2c-b79f-910c1ba7c681" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="1811c2c4-f32a-4214-acb3-7c329af88c7e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="5af35b23-6b80-477d-aacc-1f9c438bf7d2">
                      <gml:posList>552634.794683739540000000 5806304.686866828200000000 59.219917297363281000 552634.614000000060000000 5806304.662000000500000000 59.374040625160561000 552634.614000000060000000 5806304.662000000500000000 56.564559936523438000 552634.614000000060000000 5806304.662000000500000000 51.840000000000011000 552634.794683739540000000 5806304.686866828200000000 51.840000000000003000 552634.794683739540000000 5806304.686866828200000000 56.564559936523438000 552634.794683739540000000 5806304.686866828200000000 59.219917297363281000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="b2c6f6d9-619d-4943-b1b9-94b8983d2fc1">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>15.96</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>82.07</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="eae24747-89a2-41b0-8c74-d5340e192d69" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="f7fa4228-a6bb-4409-be8a-004eac834053">
                  <gml:exterior>
                    <gml:LinearRing gml:id="141fa966-0229-4414-965e-4e1133958063">
                      <gml:posList>552634.794683739540000000 5806304.686866828200000000 56.564559936523438000 552633.965221886520000000 5806310.638733758600000000 56.564559936523438000 552633.965221886520000000 5806310.638733758600000000 59.219917297363281000 552634.794683739540000000 5806304.686866828200000000 59.219917297363281000 552634.794683739540000000 5806304.686866828200000000 56.564559936523438000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="11fbb682-4fbe-44cb-99c2-54fc8153838f">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>1.30</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>352.15</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="1bbf1249-2ba5-4e4a-b44b-ff1b55f7f125" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="6b99075c-2130-4d01-b60a-11c090967e31">
                  <gml:exterior>
                    <gml:LinearRing gml:id="d55f6110-bcba-45c4-9b47-fd6df3756672">
                      <gml:posList>552633.965221886520000000 5806310.638733758600000000 56.564559936523438000 552633.965221886520000000 5806310.638733758600000000 51.840000000000003000 552633.792999999600000000 5806310.615000000200000000 51.840000000000011000 552633.792999999600000000 5806310.615000000200000000 56.564559936523438000 552633.792999999600000000 5806310.615000000200000000 59.366826334904381000 552633.965221886520000000 5806310.638733758600000000 59.219917297363281000 552633.965221886520000000 5806310.638733758600000000 56.564559936523438000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="c00a73fa-ba4d-4b4d-92f7-30c5a1514094">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>92.97</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>82.08</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="7e33f8f6-449b-4c95-8872-e2686b949320" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bb8fd881-a5f9-492a-be03-01425161ff5f">
                  <gml:exterior>
                    <gml:LinearRing gml:id="f607169c-0fe7-47cb-88c5-83929d4bc62f">
                      <gml:posList>552633.792999999600000000 5806310.615000000200000000 56.564559936523438000 552633.792999999600000000 5806310.615000000200000000 51.840000000000011000 552632.089999999850000000 5806322.849999999600000000 51.840000000000011000 552632.089999999850000000 5806322.849999999600000000 59.365077537042112000 552633.792999999600000000 5806310.615000000200000000 59.366826334904381000 552633.792999999600000000 5806310.615000000200000000 56.564559936523438000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="7e0e3123-bca4-4868-8d48-15030005bba2">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>105.47</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>352.08</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="2c93cac9-38fa-43d6-a114-915e09c10aaa" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="cc489525-b847-427c-92e1-e92107797f8c">
                  <gml:exterior>
                    <gml:LinearRing gml:id="38ac9d53-3ca0-4be1-8658-da30e87efd94">
                      <gml:posList>552632.089999999850000000 5806322.849999999600000000 51.840000000000011000 552626.749734129870000000 5806322.106805895500000000 51.840000000000003000 552621.440999999640000000 5806321.368000000700000000 51.840000000000011000 552621.440999999640000000 5806321.368000000700000000 59.391979798321913000 552626.749734129870000000 5806322.106805895500000000 63.921287536621094000 552632.089999999850000000 5806322.849999999600000000 59.365077537042112000 552632.089999999850000000 5806322.849999999600000000 51.840000000000011000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="0d94f708-c055-4e36-8353-93d45c6c60a6">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>2.26</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>262.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="87ba1bcd-8339-42ac-81b8-a9cb2f63b8cd" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="6473701a-0d6a-46b4-a0a3-7a571b2b7d3a">
                  <gml:exterior>
                    <gml:LinearRing gml:id="94e5f971-7220-408c-ac2f-b275bf811adc">
                      <gml:posList>552621.481999999840000000 5806321.071000000500000000 59.391653051510573000 552621.440999999640000000 5806321.368000000700000000 59.391979798321913000 552621.440999999640000000 5806321.368000000700000000 51.840000000000011000 552621.481999999840000000 5806321.071000000500000000 51.840000000000003000 552621.481999999840000000 5806321.071000000500000000 59.391653051510573000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="10e1dbb2-09c2-433b-9b30-93f8869b28a4">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>76.97</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>262.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="489bce8b-83f8-4cee-9bc1-d8e0910fab25" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="4cb0005f-fa3b-4c4e-8191-2d63ea182751">
                  <gml:exterior>
                    <gml:LinearRing gml:id="d95320f5-51d8-4bea-9765-85c1449c1649">
                      <gml:posList>552622.883000000380000000 5806310.971999999100000000 59.386285653270022000 552622.874989314350000000 5806311.029744409000000000 59.386316343164495000 552621.481999999840000000 5806321.071000000500000000 59.391653051510573000 552621.481999999840000000 5806321.071000000500000000 51.840000000000003000 552622.874989314350000000 5806311.029744409000000000 51.839999999999989000 552622.883000000380000000 5806310.971999999100000000 51.840000000000011000 552622.883000000380000000 5806310.971999999100000000 59.386285653270022000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="e8c43df3-7f4f-415a-b344-2a19cb8e14d9">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>1.47</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>352.09</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="49e62612-f9c7-4811-b6f3-46e2e3e910b7" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="d4243ccd-e37b-4d9f-9ca7-8273ad5c3ac3">
                  <gml:exterior>
                    <gml:LinearRing gml:id="95f6b10c-6a6b-4cd5-92c7-1a178443c026">
                      <gml:posList>552622.883000000380000000 5806310.971999999100000000 51.840000000000011000 552622.687994687820000000 5806310.944915927900000000 51.840000000000003000 552622.687994687820000000 5806310.944915927900000000 59.219917297363281000 552622.883000000380000000 5806310.971999999100000000 59.386285653270022000 552622.883000000380000000 5806310.971999999100000000 51.840000000000011000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="ca3c0a44-4feb-437b-baec-94c3081294bf">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>2.92</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>172.16</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="ea8ba5fb-d2c8-4a76-9486-35efb313d8b5" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="91b2226d-a781-4c52-8d90-a3f2ad9e334b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="7572ebf8-a3c3-4997-b0bd-bcb4a64926ea">
                      <gml:posList>552635.406000000420000000 5806304.770999999700000000 56.564559936523438000 552634.794683739540000000 5806304.686866828200000000 56.564559936523438000 552634.794683739540000000 5806304.686866828200000000 51.840000000000003000 552635.406000000420000000 5806304.770999999700000000 51.840000000000011000 552635.406000000420000000 5806304.770999999700000000 56.564559936523438000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="72ea190e-f294-40fa-a58a-bf9dff607725">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>8.91</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>82.17</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="834c63da-6b9d-4b85-ad40-34b8b649b39a" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="92862c70-41f5-4e67-9d79-c4dab8a2345f">
                  <gml:exterior>
                    <gml:LinearRing gml:id="0a7f3cf9-8529-4470-bfe3-c01ec907e727">
                      <gml:posList>552635.406000000420000000 5806304.770999999700000000 51.840000000000011000 552635.149000000210000000 5806306.640000000600000000 51.840000000000011000 552635.149000000210000000 5806306.640000000600000000 56.564559936523438000 552635.406000000420000000 5806304.770999999700000000 56.564559936523438000 552635.406000000420000000 5806304.770999999700000000 51.840000000000011000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="4008b444-c39a-4d96-bc3b-788b4fed889d">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>5.67</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>172.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="2acfb94a-fd35-46fa-b68e-56a1d05fedf1" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="71b79a5c-b444-4cb2-b8f6-652de4f31878">
                  <gml:exterior>
                    <gml:LinearRing gml:id="df730d17-a28f-454e-afe0-adbba11640d6">
                      <gml:posList>552636.337000000290000000 5806306.803999999500000000 56.564559936523438000 552635.149000000210000000 5806306.640000000600000000 56.564559936523438000 552635.149000000210000000 5806306.640000000600000000 51.840000000000011000 552636.337000000290000000 5806306.803999999500000000 51.840000000000011000 552636.337000000290000000 5806306.803999999500000000 56.564559936523438000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="00295618-218e-4322-a00c-0cd6ca032a2e">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>19.48</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>82.15</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="28ed2012-8c3c-4abf-a40d-f23f1bd6f593" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="610eada9-6af1-4c23-9c6d-4343344e0193">
                  <gml:exterior>
                    <gml:LinearRing gml:id="ac21bf20-f0c6-4d9e-890b-46bb6d593db7">
                      <gml:posList>552636.337000000290000000 5806306.803999999500000000 51.840000000000011000 552635.774000000210000000 5806310.888000000300000000 51.840000000000011000 552635.774000000210000000 5806310.888000000300000000 56.564559936523438000 552636.337000000290000000 5806306.803999999500000000 56.564559936523438000 552636.337000000290000000 5806306.803999999500000000 51.840000000000011000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="20a6a0fb-960a-4909-9a29-aad40b9b1264">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>8.63</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>352.15</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="a748aff8-3e08-4fd3-9f43-eade399cee21" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="2d6d4320-3555-432a-87ed-7b38b31a5b62">
                  <gml:exterior>
                    <gml:LinearRing gml:id="2c63c37f-1746-4451-bde6-ab4d9e1c769b">
                      <gml:posList>552635.774000000210000000 5806310.888000000300000000 51.840000000000011000 552633.965221886520000000 5806310.638733758600000000 51.840000000000003000 552633.965221886520000000 5806310.638733758600000000 56.564559936523438000 552635.774000000210000000 5806310.888000000300000000 56.564559936523438000 552635.774000000210000000 5806310.888000000300000000 51.840000000000011000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="e61945e7-3f8b-4562-8511-2131961abfb3">
          <gen:stringAttribute name="Type">
            <gen:value>Ground</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>359.63</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="077313f1-f9d4-472f-9d58-2010a1c1a1e8" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="1d94e430-1dd5-4420-88af-2687a46a7de0">
                  <gml:exterior>
                    <gml:LinearRing gml:id="795a9fcd-fbf7-4373-8669-15e001a39cc4">
                      <gml:posList>552635.149000000210000000 5806306.640000000600000000 51.840000000000011000 552635.406000000420000000 5806304.770999999700000000 51.840000000000011000 552634.794683739540000000 5806304.686866828200000000 51.840000000000003000 552634.614000000060000000 5806304.662000000500000000 51.840000000000011000 552636.307000000030000000 5806292.405999999500000000 51.840000000000011000 552630.991412530300000000 5806291.670318299000000000 51.840000000000003000 552625.663999999870000000 5806290.933000000200000000 51.840000000000011000 552625.622999999670000000 5806291.230000000400000000 51.840000000000003000 552624.245528604840000000 5806301.159395875400000000 51.840000000000018000 552624.222000000070000000 5806301.328999999900000000 51.840000000000011000 552624.031764661780000000 5806301.302591762500000000 51.840000000000011000 552622.241000000390000000 5806301.053999999500000000 51.839999999995854000 552620.902999999930000000 5806310.697000000600000000 51.840000000000011000 552622.687994687820000000 5806310.944915927900000000 51.840000000000003000 552622.883000000380000000 5806310.971999999100000000 51.840000000000011000 552622.874989314350000000 5806311.029744409000000000 51.839999999999989000 552621.481999999840000000 5806321.071000000500000000 51.840000000000003000 552621.440999999640000000 5806321.368000000700000000 51.840000000000011000 552626.749734129870000000 5806322.106805895500000000 51.840000000000003000 552632.089999999850000000 5806322.849999999600000000 51.840000000000011000 552633.792999999600000000 5806310.615000000200000000 51.840000000000011000 552633.965221886520000000 5806310.638733758600000000 51.840000000000003000 552635.774000000210000000 5806310.888000000300000000 51.840000000000011000 552636.337000000290000000 5806306.803999999500000000 51.840000000000011000 552635.149000000210000000 5806306.640000000600000000 51.840000000000011000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </cityObjectMember>

</CityModel>
