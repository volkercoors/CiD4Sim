<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<CityModel xmlns="http://www.opengis.net/citygml/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:core="http://www.opengis.net/citygml/base/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:tex="http://www.opengis.net/citygml/textures/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/citygml/2.0 http://schemas.opengis.net/citygml/2.0/cityGMLBase.xsd http://www.opengis.net/citygml/appearance/2.0  http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd">

  <gml:description>Exported by RhinoCity(TM) software - (c)Rhinoterrain 2010-2018 : www.rhinoterrain.com</gml:description>

  <gml:name>RhinoCity(TM)</gml:name>

  <gml:boundedBy>
    <gml:Envelope srsDimension="3" srsName="crs:ETRS89_UTM32*DE_DHHN92_NH">
      <gml:lowerCorner>552710.187500 5807839.000000 50.929996</gml:lowerCorner>
      <gml:upperCorner>552720.312500 5807852.000000 59.400913</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>

  <cityObjectMember>
    <bldg:Building gml:id="DENIAL4300017Dxt">
      <gen:doubleAttribute name="Volume">
        <gen:value>515.753</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="TotalRoofArea">
        <gen:value>124.87</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="TotalWallArea">
        <gen:value>209.48</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Authoring_Software">
        <gen:value>RhinoCity</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="OSKA">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ALKISID">
        <gen:value>DENIAL4300017Dxt</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="KACHEL_ID">
        <gen:value>11_12_Building</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="COUNT">
        <gen:value>1645</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="DESC">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCK">
        <gen:value>31</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCKNR">
        <gen:value>212031</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STATBEZNR">
        <gen:value>212</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="MBZNR">
        <gen:value>2109</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="WBZNR">
        <gen:value>2109</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNAM">
        <gen:value>Sahlkamp</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNR">
        <gen:value>21</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNAM">
        <gen:value>Bothfeld-Vahrenheide</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNR">
        <gen:value>03</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEMEINDE">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SCALE">
        <gen:value>5000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_overlaps">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_xmax">
        <gen:value>552720.3039999995</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_xmin">
        <gen:value>552710.2319999998</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_ymax">
        <gen:value>5807851.529999999</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_ymin">
        <gen:value>5807839.169</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_zmax">
        <gen:value>51.09987600000203</gen:value>
      </gen:stringAttribute>
      <gen:doubleAttribute name="ZMIN">
        <gen:value>50.930000</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="THICKNESS">
        <gen:value>0.100000</gen:value>
      </gen:doubleAttribute>
      <gen:intAttribute name="TEXTURE">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="BUILDINGID">
        <gen:value>DENIAL4300017Dxt</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ARTBEZEICH">
        <gen:value>Gebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Join_Cou_1">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="FUNKTION_T">
        <gen:value>Wohngebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADT">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="OVERHANG">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="OBJFLAECHE">
        <gen:value>96.82</gen:value>
      </gen:stringAttribute>
      <bldg:measuredHeight uom="#m">14.555</bldg:measuredHeight>
      <bldg:lod2Solid>
        <gml:Solid gml:id="b02fa677-5312-433b-9f97-ebb76a3f78df">
          <gml:exterior>
            <gml:CompositeSurface gml:id="f43b743a-fade-41d5-adf9-421f90888b5b">
              <gml:surfaceMember xlink:href="#2eeba77c-da0c-43cf-9a01-785827e457bc"/>
              <gml:surfaceMember xlink:href="#dc4d5097-d53c-41db-b452-2509fab59028"/>
              <gml:surfaceMember xlink:href="#8b348b82-8d7c-491c-911c-a21789ff2240"/>
              <gml:surfaceMember xlink:href="#552ca70f-9a18-49ed-aa42-9b270a367388"/>
              <gml:surfaceMember xlink:href="#ce1bf88d-a073-49ea-b9d0-d2abec91cfc6"/>
              <gml:surfaceMember xlink:href="#6ac55291-bb72-4719-ba6b-350957603980"/>
              <gml:surfaceMember xlink:href="#db07545c-8bea-4d24-9f0b-0873dfc196b7"/>
              <gml:surfaceMember xlink:href="#e2193cd3-3a6e-4127-8591-335c4b8c3f41"/>
              <gml:surfaceMember xlink:href="#4b15f9f2-c03c-4269-9f98-26d7638ec6cc"/>
              <gml:surfaceMember xlink:href="#6c082ae7-d633-4a2a-8a3c-a1f39bc28ffe"/>
              <gml:surfaceMember xlink:href="#31295b38-2310-471b-b113-d05066b11ba1"/>
              <gml:surfaceMember xlink:href="#9f9a082d-9aae-48d8-b551-5c3cc57a291d"/>
              <gml:surfaceMember xlink:href="#66e1ffc5-8a0f-42be-871b-1cb381a77bd2"/>
              <gml:surfaceMember xlink:href="#ad2d63e5-7d78-410a-8026-38461e98fe11"/>
              <gml:surfaceMember xlink:href="#027bdfa1-1e6d-421e-952b-0938cca225f7"/>
              <gml:surfaceMember xlink:href="#0d657a2c-51ac-4a9e-9c1e-cbacd3eaabf4"/>
              <gml:surfaceMember xlink:href="#8f265e05-3fcc-4b29-9afc-505d2c330395"/>
              <gml:surfaceMember xlink:href="#b1231234-10e5-42fd-927f-d3b971a8ede0"/>
              <gml:surfaceMember xlink:href="#dfca81a7-6be5-42cf-aa7e-e64d98a9ff02"/>
              <gml:surfaceMember xlink:href="#fb259837-5c19-4f6f-bc22-dbe48717b9e5"/>
              <gml:surfaceMember xlink:href="#93fb2d64-20eb-4f2e-8630-c8a9077c3b24"/>
              <gml:surfaceMember xlink:href="#29ddc067-bb1b-4b1b-942d-8d5a60550529"/>
              <gml:surfaceMember xlink:href="#415faf96-d115-4d22-97db-c6d025513892"/>
              <gml:surfaceMember xlink:href="#10850b95-764a-41db-a45a-9a8302405d79"/>
              <gml:surfaceMember xlink:href="#062b5ea3-cea2-4ed7-b420-550f4347df56"/>
              <gml:surfaceMember xlink:href="#aa6a6745-e276-4af4-8840-66de2017bd1f"/>
              <gml:surfaceMember xlink:href="#e6c46cf6-3ca2-4695-bfca-93e4e1dcfeff"/>
              <gml:surfaceMember xlink:href="#7b0e1184-9c23-49ce-80e7-ed1856b78472"/>
              <gml:surfaceMember xlink:href="#5b035169-711d-4623-be16-4bec2e079923"/>
              <gml:surfaceMember xlink:href="#771c62ca-b1b1-4cba-9cc3-adc860f3e80d"/>
              <gml:surfaceMember xlink:href="#3afee05d-65c1-4b04-abb3-b6332539abad"/>
              <gml:surfaceMember xlink:href="#73aaf192-4df5-4ccc-acb8-f6406e521ba3"/>
              <gml:surfaceMember xlink:href="#0184c322-d545-4162-a14b-1582b54358c7"/>
              <gml:surfaceMember xlink:href="#4666a976-d5e8-48dd-b1aa-a212d4c360f9"/>
              <gml:surfaceMember xlink:href="#1f7815d7-4e46-47cd-bebc-06bae0d45890"/>
              <gml:surfaceMember xlink:href="#dfa71f1b-2eaf-4747-8897-8914c3d0cd90"/>
              <gml:surfaceMember xlink:href="#862dcaf5-1b39-43c6-bcd6-23817f99ac36"/>
              <gml:surfaceMember xlink:href="#143083d5-560c-41b5-a620-ba41715de182"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="11fb82a2-7d38-4fdd-8c45-404e839e2fd8">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL4300017Dxt_C:29116</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>22.70</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>358.11</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>6.44</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="83c12591-a158-46d7-8dff-c50c399061ef" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="2eeba77c-da0c-43cf-9a01-785827e457bc">
                  <gml:exterior>
                    <gml:LinearRing gml:id="030ed2e5-0ef3-4d84-981a-bb8b5256f036">
                      <gml:posList>552717.375333300210000000 5807847.798038134400000000 53.436967226894701000 552717.263000000270000000 5807851.529999999300000000 53.015712194975848000 552711.254999999890000000 5807851.384999999800000000 53.009657678236721000 552711.380613521090000000 5807847.607078193700000000 53.436144926176453000 552712.390749571260000000 5807847.639255764900000000 53.436283487299278000 552717.375333300210000000 5807847.798038134400000000 53.436967226894701000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="a80de77d-70b3-476f-8b3c-f63f763ea9e9">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL4300017Dxt_C:29115</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>21.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>358.18</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>31.59</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="5d9efad1-b21e-4ecd-b5e2-88219da90944" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="dc4d5097-d53c-41db-b452-2509fab59028">
                  <gml:exterior>
                    <gml:LinearRing gml:id="c9747bdf-1bad-4a05-9454-e97b0e0bca00">
                      <gml:posList>552717.379999999890000000 5807847.642999999200000000 56.026635272689546000 552717.375333300210000000 5807847.798038134400000000 55.931259155273438000 552712.390749571260000000 5807847.639255764900000000 55.931259155273438000 552712.480028882270000000 5807844.836550385700000000 57.655519329351755000 552719.032296077000000000 5807845.045270825700000000 57.655519329351755000 552718.948148671650000000 5807847.686872590300000000 56.030372158308964000 552717.379999999890000000 5807847.642999999200000000 56.026635272689546000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="5f3e32db-4278-4dd9-b1db-cf795134dcd0">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL4300017Dxt_C:29114</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>22.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>178.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>37.09</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="ecfc9fda-4c73-4cd2-bfa7-a09965ce7f5a" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="8b348b82-8d7c-491c-911c-a21789ff2240">
                  <gml:exterior>
                    <gml:LinearRing gml:id="46d0ad57-d030-42f9-a5ce-f5842d76225b">
                      <gml:posList>552718.555214422640000000 5807842.822489009200000000 58.519263344677071000 552713.363842256950000000 5807842.650366559600000000 58.519263711565117000 552713.477291025100000000 5807839.228656524800000000 55.931259155273438000 552718.668663174730000000 5807839.400779459600000000 55.931259155273438000 552718.555214422640000000 5807842.822489009200000000 58.519263344677071000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="552c5a1f-800f-4acd-9e57-2d52c021ccea">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL4300017Dxt_C:29113</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>1.87</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="4d6a3cb6-1cdf-427d-9beb-ec99a5e3b7cb" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="552ca70f-9a18-49ed-aa42-9b270a367388">
                  <gml:exterior>
                    <gml:LinearRing gml:id="4dfddd4e-669b-4991-9504-0824dbead7c1">
                      <gml:posList>552711.324521318780000000 5807847.163506684800000000 53.095531463623047000 552710.300999999980000000 5807847.130999999100000000 53.095531463623047000 552710.231999999840000000 5807847.129000000700000000 53.095531463623047000 552710.291000000200000000 5807845.412000000500000000 53.095531463623047000 552711.379205986040000000 5807845.446815449700000000 53.095531463623047000 552711.324521318780000000 5807847.163506684800000000 53.095531463623047000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="74eec0b1-b21a-429f-9710-d270205a6383">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL4300017Dxt_C:29117</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>28.53</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>358.18</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>49.97</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="d5301120-3720-40d4-a4fa-5733d278ac7d" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="ce1bf88d-a073-49ea-b9d0-d2abec91cfc6">
                  <gml:exterior>
                    <gml:LinearRing gml:id="941ca319-96d0-40dc-adc7-8889ae716f0e">
                      <gml:posList>552712.480028882270000000 5807844.836550385700000000 57.655519329351755000 552712.390749571260000000 5807847.639255764900000000 54.316902160644531000 552711.380613521090000000 5807847.607078193700000000 54.316902160644531000 552711.387000000100000000 5807847.414999999100000000 54.545718226401988000 552711.457000000400000000 5807847.417999999600000000 54.544801717815432000 552711.466000000010000000 5807847.167999999600000000 54.842644081501973000 552711.324521318780000000 5807847.163506684800000000 54.842628081720648000 552711.379205986040000000 5807845.446815449700000000 56.887571827201327000 552711.509999999780000000 5807845.450999999400000000 56.887550231957192000 552711.566434851030000000 5807843.340739675800000000 59.400909423828125000 552720.163534285380000000 5807843.614597613000000000 59.400909423828125000 552720.025000000370000000 5807847.717000000200000000 54.513779463384068000 552718.948148671650000000 5807847.686872590300000000 54.508810770517186000 552719.032296077000000000 5807845.045270825700000000 57.655519329351755000 552712.480028882270000000 5807844.836550385700000000 57.655519329351755000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="e7fa7516-6836-43ee-9fcd-c2b750d31106">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL4300017Dxt_C:29117</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>28.18</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>178.18</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>49.97</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="d75c5c7d-4651-4f97-9bfd-66b785cbc88b" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="6ac55291-bb72-4719-ba6b-350957603980">
                  <gml:exterior>
                    <gml:LinearRing gml:id="80bbf614-09ec-47ad-ae33-69e7a97abe6f">
                      <gml:posList>552718.555214422640000000 5807842.822489009200000000 58.519263344677071000 552718.668663174610000000 5807839.400779462400000000 54.443112053919648000 552720.303999999540000000 5807839.455000000100000000 54.445643654713180000 552720.163534285380000000 5807843.614597613000000000 59.400909423828125000 552711.566434851030000000 5807843.340739675800000000 59.400909423828125000 552711.678000000310000000 5807839.168999999800000000 54.432290082464789000 552713.477291024990000000 5807839.228656527600000000 54.435075494323726000 552713.363842256950000000 5807842.650366559600000000 58.511227363029434000 552717.257473592300000000 5807842.779461786200000000 58.517254507484409000 552718.555214422640000000 5807842.822489009200000000 58.519263344677071000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="cc0bd310-7e1b-4b67-90ab-2e68ce4e62b5">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>2.01</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>88.18</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="85a7caf3-64e9-46c9-a9de-4a8ffa1e305e" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="db07545c-8bea-4d24-9f0b-0873dfc196b7">
                  <gml:exterior>
                    <gml:LinearRing gml:id="782244eb-db82-4bb8-8d59-d88a22aa4f88">
                      <gml:posList>552719.032296077000000000 5807845.045270825700000000 57.655519329351755000 552718.948148671650000000 5807847.686872590300000000 54.508810770517186000 552718.948148671650000000 5807847.686872590300000000 56.030372158308964000 552719.032296077000000000 5807845.045270825700000000 57.655519329351755000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="9aacd986-19c0-4983-8dae-b7773eacac0c">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>2.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>358.40</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="37771b96-8551-4370-a85c-c11f80df449b" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="e2193cd3-3a6e-4127-8591-335c4b8c3f41">
                  <gml:exterior>
                    <gml:LinearRing gml:id="7f135a8a-3cd2-474e-84d0-0add60d1d8b0">
                      <gml:posList>552718.948148671650000000 5807847.686872590300000000 54.508810770517186000 552717.379999999890000000 5807847.642999999200000000 54.501575185704105000 552717.379999999890000000 5807847.642999999200000000 56.026635272689546000 552718.948148671650000000 5807847.686872590300000000 56.030372158308964000 552718.948148671650000000 5807847.686872590300000000 54.508810770517186000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="96694bfc-6f22-4492-9e60-ac4a928daf01">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>88.28</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="d33ef16e-54b9-4582-bb11-f2c44ff6721b" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="4b15f9f2-c03c-4269-9f98-26d7638ec6cc">
                  <gml:exterior>
                    <gml:LinearRing gml:id="7e2d24a0-0501-4a72-81be-98171e9e4794">
                      <gml:posList>552717.379999999890000000 5807847.642999999200000000 54.501575185704105000 552717.375333300210000000 5807847.798038133400000000 54.316902160644531000 552717.375333300210000000 5807847.798038134400000000 55.931259155273438000 552717.379999999890000000 5807847.642999999200000000 56.026635272689546000 552717.379999999890000000 5807847.642999999200000000 54.501575185704105000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="77993cbf-de81-41bd-8d77-85cd2ab55144">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>8.05</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>358.18</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="e574ef2e-20e2-4bf7-80b4-d2a8ed278444" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="6c082ae7-d633-4a2a-8a3c-a1f39bc28ffe">
                  <gml:exterior>
                    <gml:LinearRing gml:id="1750b043-755a-42e2-8836-087d28ae4dba">
                      <gml:posList>552717.375333300210000000 5807847.798038133400000000 54.316902160644531000 552712.390749571260000000 5807847.639255764900000000 54.316902160644531000 552712.390749571260000000 5807847.639255764900000000 55.931259155273438000 552717.375333300210000000 5807847.798038134400000000 55.931259155273438000 552717.375333300210000000 5807847.798038133400000000 54.316902160644531000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="eef58ca2-1055-449c-a0ff-afae622d07c6">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>2.26</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>268.18</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="b3911070-16cc-4ada-80b5-20e48cd9b609" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="31295b38-2310-471b-b113-d05066b11ba1">
                  <gml:exterior>
                    <gml:LinearRing gml:id="46279fc5-7e04-4af2-b310-707ba25a53c0">
                      <gml:posList>552712.480028882270000000 5807844.836550385700000000 57.655519329351755000 552712.390749571260000000 5807847.639255764900000000 55.931259155273438000 552712.390749571260000000 5807847.639255764900000000 54.316902160644531000 552712.480028882270000000 5807844.836550385700000000 57.655519329351755000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="5c3707a5-0bf6-4b88-b130-7b2fc83ea50b">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>8.57</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>88.28</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="d9d71f4e-171d-42e2-8299-abd3d5129007" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="9f9a082d-9aae-48d8-b551-5c3cc57a291d">
                  <gml:exterior>
                    <gml:LinearRing gml:id="554e7dad-dbf2-40e0-a6ad-f67279c4e87f">
                      <gml:posList>552717.375333300210000000 5807847.798038133400000000 50.929999999999993000 552717.263000000270000000 5807851.529999999300000000 50.930000000000000000 552717.263000000270000000 5807851.529999999300000000 53.015712194975848000 552717.375333300210000000 5807847.798038134400000000 53.436967226894701000 552717.375333300210000000 5807847.798038133400000000 50.929999999999993000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="f550eca5-2686-46ff-ba6d-41ac1bd1a746">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>12.52</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>358.62</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="3e7615c9-af25-4f35-ab68-c84969ea8a90" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="66e1ffc5-8a0f-42be-871b-1cb381a77bd2">
                  <gml:exterior>
                    <gml:LinearRing gml:id="f988b167-c21e-4566-b24a-9646e5952071">
                      <gml:posList>552717.263000000270000000 5807851.529999999300000000 50.930000000000000000 552711.254999999890000000 5807851.384999999800000000 50.930000000000000000 552711.254999999890000000 5807851.384999999800000000 53.009657678236721000 552717.263000000270000000 5807851.529999999300000000 53.015712194975848000 552717.263000000270000000 5807851.529999999300000000 50.930000000000000000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="0c784c2a-7db9-423b-bb79-c5b12db5690b">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>8.67</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>268.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="9d14fed8-1703-4341-b350-27bbd8bd97ec" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="ad2d63e5-7d78-410a-8026-38461e98fe11">
                  <gml:exterior>
                    <gml:LinearRing gml:id="bcdbbf71-1378-460c-a474-baa876bf0929">
                      <gml:posList>552711.380613521090000000 5807847.607078193700000000 53.436144926176453000 552711.254999999890000000 5807851.384999999800000000 53.009657678236721000 552711.254999999890000000 5807851.384999999800000000 50.930000000000000000 552711.380613521090000000 5807847.607078193700000000 50.929999999999993000 552711.380613521090000000 5807847.607078193700000000 53.436144926176453000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="6fdc07f1-575b-4907-8c14-f3f0f9dd316b">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>4.76</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>268.18</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="2e3e0c98-fb9e-445d-b146-81cce07b14a1" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="027bdfa1-1e6d-421e-952b-0938cca225f7">
                  <gml:exterior>
                    <gml:LinearRing gml:id="cf893a5f-eee3-4fe1-b73f-dd8d89338665">
                      <gml:posList>552711.379205986040000000 5807845.446815449700000000 56.887571827201327000 552711.324521318780000000 5807847.163506684800000000 54.842628081720648000 552711.324521318780000000 5807847.163506684800000000 53.095531463623047000 552711.379205986040000000 5807845.446815449700000000 53.095531463623047000 552711.379205986040000000 5807845.446815449700000000 56.887571827201327000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="0fe898ef-9422-436c-8712-b962387a0d29">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.50</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>178.17</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="28316c58-fc8f-4c52-9718-fb29b7c0be61" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="0d657a2c-51ac-4a9e-9c1e-cbacd3eaabf4">
                  <gml:exterior>
                    <gml:LinearRing gml:id="36837b1f-7213-45f4-b1ef-fe7f65c7d50c">
                      <gml:posList>552711.509999999780000000 5807845.450999999400000000 56.887550231957192000 552711.379205986040000000 5807845.446815449700000000 56.887571827201327000 552711.379205986040000000 5807845.446815449700000000 53.095531463623047000 552711.509999999780000000 5807845.450999999400000000 53.095531463623047000 552711.509999999780000000 5807845.450999999400000000 56.887550231957192000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="b24c54a1-2655-4957-8fce-88af593908a3">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>40.21</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>268.47</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="aacc701e-13db-45d2-bb51-b133f0cc9343" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="8f265e05-3fcc-4b29-9afc-505d2c330395">
                  <gml:exterior>
                    <gml:LinearRing gml:id="64d6bcac-6480-4ba9-a161-95e0ad37973f">
                      <gml:posList>552711.678000000310000000 5807839.168999999800000000 54.432290082464789000 552711.566434851030000000 5807843.340739675800000000 59.400909423828125000 552711.509999999780000000 5807845.450999999400000000 56.887550231957192000 552711.509999999780000000 5807845.450999999400000000 53.095531463623047000 552711.509999999780000000 5807845.450999999400000000 50.930000000000007000 552711.566434851030000000 5807843.340739675800000000 50.930000000000000000 552711.678000000310000000 5807839.168999999800000000 50.930000000000000000 552711.678000000310000000 5807839.168999999800000000 54.432290082464789000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="4eba6f32-6826-4ed0-be69-3064e9ab01fc">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>30.28</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>178.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="49435c31-7a9b-400a-b3fa-df9e71db4b85" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="b1231234-10e5-42fd-927f-d3b971a8ede0">
                  <gml:exterior>
                    <gml:LinearRing gml:id="092ace64-dbfa-4310-885f-6f6c06c67586">
                      <gml:posList>552720.303999999540000000 5807839.455000000100000000 54.445643654713180000 552718.668663174610000000 5807839.400779462400000000 54.443112053919648000 552713.477291024990000000 5807839.228656527600000000 54.435075494323726000 552711.678000000310000000 5807839.168999999800000000 54.432290082464789000 552711.678000000310000000 5807839.168999999800000000 50.930000000000000000 552713.477291024870000000 5807839.228656530400000000 50.930000000000007000 552718.668663174610000000 5807839.400779462400000000 50.930000000000007000 552720.303999999540000000 5807839.455000000100000000 50.929999999999993000 552720.303999999540000000 5807839.455000000100000000 54.445643654713180000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="177c2254-2535-42f9-8bee-28ca0d89df29">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>24.94</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>88.07</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="7ca42e60-964a-4451-8347-c3b83f1f7e35" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="dfca81a7-6be5-42cf-aa7e-e64d98a9ff02">
                  <gml:exterior>
                    <gml:LinearRing gml:id="ff8cb360-8f00-49ee-bb08-5363016eac15">
                      <gml:posList>552720.303999999540000000 5807839.455000000100000000 50.929999999999993000 552720.163534285380000000 5807843.614597613000000000 50.930000000000007000 552720.163534285380000000 5807843.614597613000000000 59.400909423828125000 552720.303999999540000000 5807839.455000000100000000 54.445643654713180000 552720.303999999540000000 5807839.455000000100000000 50.929999999999993000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="2696e731-2452-4da7-ae94-89153c7c56b5">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>24.74</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>88.07</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="aef0f502-267f-4690-bf6c-50b8556d02b8" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="fb259837-5c19-4f6f-bc22-dbe48717b9e5">
                  <gml:exterior>
                    <gml:LinearRing gml:id="e94c4e14-2b8e-4e1f-af2b-110ccf96be6c">
                      <gml:posList>552720.163534285380000000 5807843.614597613000000000 50.930000000000007000 552720.025000000370000000 5807847.717000000200000000 50.930000000000000000 552720.025000000370000000 5807847.717000000200000000 54.513779463384068000 552720.163534285380000000 5807843.614597613000000000 59.400909423828125000 552720.163534285380000000 5807843.614597613000000000 50.930000000000007000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="28f57519-5c11-4149-be9a-eb635ef74296">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>9.47</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>358.40</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="160dff3a-72fd-4967-a874-04fe99bef646" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="93fb2d64-20eb-4f2e-8630-c8a9077c3b24">
                  <gml:exterior>
                    <gml:LinearRing gml:id="0bf73c77-bdfb-458b-a4be-0ecffcea5a52">
                      <gml:posList>552720.025000000370000000 5807847.717000000200000000 50.930000000000000000 552718.948148671650000000 5807847.686872590300000000 50.930000000000000000 552717.468326951140000000 5807847.645471150100000000 50.929999999999986000 552717.379999999890000000 5807847.642999999200000000 50.929999999999993000 552717.379999999890000000 5807847.642999999200000000 53.454467565013573000 552717.379999999890000000 5807847.642999999200000000 54.501575185704105000 552718.948148671650000000 5807847.686872590300000000 54.508810770517186000 552720.025000000370000000 5807847.717000000200000000 54.513779463384068000 552720.025000000370000000 5807847.717000000200000000 50.930000000000000000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="c15f32d6-60d1-43ca-b95c-36715ddef909">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.54</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>88.28</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="1623da99-cb96-41aa-aa02-c5ae0065637b" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="29ddc067-bb1b-4b1b-942d-8d5a60550529">
                  <gml:exterior>
                    <gml:LinearRing gml:id="e82bbc25-063d-41a2-b927-e8fff7c22f62">
                      <gml:posList>552717.379999999890000000 5807847.642999999200000000 53.454467565013573000 552717.379999999890000000 5807847.642999999200000000 50.929999999999993000 552717.375333300210000000 5807847.798038133400000000 50.929999999999993000 552717.375333300210000000 5807847.798038134400000000 53.436967226894701000 552717.375333300210000000 5807847.798038133400000000 54.316902160644531000 552717.379999999890000000 5807847.642999999200000000 54.501575185704105000 552717.379999999890000000 5807847.642999999200000000 53.454467565013573000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="8f29bf50-743e-439d-88f1-1f8c3835bd2e">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.89</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>358.18</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="15314432-139f-4b1d-8697-778967393f03" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="415faf96-d115-4d22-97db-c6d025513892">
                  <gml:exterior>
                    <gml:LinearRing gml:id="ee4894ed-7265-48ce-aae6-d75417f55fb4">
                      <gml:posList>552712.390749571260000000 5807847.639255764900000000 53.436283487299278000 552711.380613521090000000 5807847.607078193700000000 53.436144926176453000 552711.380613521090000000 5807847.607078193700000000 54.316902160644531000 552712.390749571260000000 5807847.639255764900000000 54.316902160644531000 552712.390749571260000000 5807847.639255764900000000 53.436283487299278000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="3cb27cc4-f563-43aa-abe9-9b8c0964d03c">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.67</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>268.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="11e67524-e5a4-45ab-bb74-d05004fc1e8d" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="10850b95-764a-41db-a45a-9a8302405d79">
                  <gml:exterior>
                    <gml:LinearRing gml:id="cc95c7e7-5107-4144-aeca-85010864d2b1">
                      <gml:posList>552711.387000000100000000 5807847.414999999100000000 54.545718226401988000 552711.380613521090000000 5807847.607078193700000000 54.316902160644531000 552711.380613521090000000 5807847.607078193700000000 53.436144926176453000 552711.380613521090000000 5807847.607078193700000000 50.929999999999993000 552711.387000000100000000 5807847.414999999100000000 50.930000000000007000 552711.387000000100000000 5807847.414999999100000000 53.457828514305874000 552711.387000000100000000 5807847.414999999100000000 54.545718226401988000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="8ad5f1af-17ee-4ddb-b42c-431e9a04eb78">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.25</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>177.55</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="a2e8e3ef-f88f-4499-8f7d-1e0eff4db7b4" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="062b5ea3-cea2-4ed7-b420-550f4347df56">
                  <gml:exterior>
                    <gml:LinearRing gml:id="81c67b3f-c3d9-48f6-995c-9f3ba1226320">
                      <gml:posList>552711.457000000400000000 5807847.417999999600000000 54.544801717815432000 552711.387000000100000000 5807847.414999999100000000 54.545718226401988000 552711.387000000100000000 5807847.414999999100000000 53.457828514305874000 552711.387000000100000000 5807847.414999999100000000 50.930000000000007000 552711.457000000400000000 5807847.417999999600000000 50.930000000000000000 552711.457000000400000000 5807847.417999999600000000 53.457751267503937000 552711.457000000400000000 5807847.417999999600000000 54.544801717815432000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="f85859d3-2b83-49f6-8821-9297d116c5f1">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.94</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>267.94</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="235b4231-0d8d-4c02-aea7-c080fa10947e" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="aa6a6745-e276-4af4-8840-66de2017bd1f">
                  <gml:exterior>
                    <gml:LinearRing gml:id="d75ce052-0176-48c1-bb90-9d11b49422a8">
                      <gml:posList>552711.466000000010000000 5807847.167999999600000000 53.095531463623047000 552711.466000000010000000 5807847.167999999600000000 54.842644081501973000 552711.457000000400000000 5807847.417999999600000000 54.544801717815432000 552711.457000000400000000 5807847.417999999600000000 53.457751267503937000 552711.457000000400000000 5807847.417999999600000000 50.930000000000000000 552711.459132624440000000 5807847.358760441700000000 50.930000000000007000 552711.466000000010000000 5807847.167999999600000000 50.930000000000007000 552711.466000000010000000 5807847.167999999600000000 53.095531463623047000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="e3249280-19e5-4746-bb7c-c8f5134ad1a9">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.55</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>358.18</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="483c8daf-7b26-4e0e-be58-433ef9ea5d58" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="e6c46cf6-3ca2-4695-bfca-93e4e1dcfeff">
                  <gml:exterior>
                    <gml:LinearRing gml:id="2d965b36-2fb4-4b5d-a1ab-25e7c8cd30b8">
                      <gml:posList>552711.466000000010000000 5807847.167999999600000000 53.095531463623047000 552711.466000000010000000 5807847.167999999600000000 50.930000000000007000 552711.324521318780000000 5807847.163506684800000000 50.930000000000007000 552711.324521318780000000 5807847.163506684800000000 53.095531463623047000 552711.324521318780000000 5807847.163506684800000000 54.842628081720648000 552711.466000000010000000 5807847.167999999600000000 54.842644081501973000 552711.466000000010000000 5807847.167999999600000000 53.095531463623047000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="f04cb629-34b1-4a7b-a6ef-ad8d48f92525">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.28</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>178.17</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="a7a588b4-3b88-4099-a505-29c3daaddccb" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="7b0e1184-9c23-49ce-80e7-ed1856b78472">
                  <gml:exterior>
                    <gml:LinearRing gml:id="2901bcac-46c4-4f28-bb73-5ff2b10a92a1">
                      <gml:posList>552711.509999999780000000 5807845.450999999400000000 53.095531463623047000 552711.379205986040000000 5807845.446815449700000000 53.095531463623047000 552711.379205986040000000 5807845.446815449700000000 50.929999999999993000 552711.509999999780000000 5807845.450999999400000000 50.930000000000007000 552711.509999999780000000 5807845.450999999400000000 53.095531463623047000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="ab63a33e-aec0-4784-a18c-6ac4404b7afc">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>4.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>358.18</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="5301a3b6-5887-4265-8e97-8e98576eed9f" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="5b035169-711d-4623-be16-4bec2e079923">
                  <gml:exterior>
                    <gml:LinearRing gml:id="1587faec-ec32-482d-82a9-7c86c2b5892f">
                      <gml:posList>552717.375333300210000000 5807847.798038134400000000 53.436967226894701000 552712.390749571260000000 5807847.639255764900000000 53.436283487299278000 552712.390749571260000000 5807847.639255764900000000 54.316902160644531000 552717.375333300210000000 5807847.798038133400000000 54.316902160644531000 552717.375333300210000000 5807847.798038134400000000 53.436967226894701000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="b0c7f801-64b0-4a42-9159-ce7c126fda37">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>2.57</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>268.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="c8cf7ccc-b118-4bfb-b0f2-40197eebf18e" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="771c62ca-b1b1-4cba-9cc3-adc860f3e80d">
                  <gml:exterior>
                    <gml:LinearRing gml:id="0e6fc288-c971-44bf-ab51-30e48649c385">
                      <gml:posList>552713.477291025100000000 5807839.228656524800000000 55.931259155273438000 552713.363842256950000000 5807842.650366559600000000 58.519263711565117000 552713.363842256950000000 5807842.650366559600000000 58.511227363029434000 552713.477291024990000000 5807839.228656527600000000 54.435075494323726000 552713.477291025100000000 5807839.228656524800000000 55.931259155273438000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="cbb3691c-7ac9-4f55-a4c7-9b56cb1939d7">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>7.75</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>178.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="a7c953b3-c877-41b0-bd1e-6035087d8713" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="3afee05d-65c1-4b04-abb3-b6332539abad">
                  <gml:exterior>
                    <gml:LinearRing gml:id="5e608caa-0c57-4a37-94b4-d0f71a676783">
                      <gml:posList>552718.668663174730000000 5807839.400779459600000000 55.931259155273438000 552713.477291025100000000 5807839.228656524800000000 55.931259155273438000 552713.477291024990000000 5807839.228656527600000000 54.435075494323726000 552718.668663174610000000 5807839.400779462400000000 54.443112053919648000 552718.668663174730000000 5807839.400779459600000000 55.931259155273438000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="39a74535-5d8b-41ff-a84d-b0442a12e2ec">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>2.55</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>88.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="ccaa45e2-d6d6-4cd0-9cd9-94481a26676c" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="73aaf192-4df5-4ccc-acb8-f6406e521ba3">
                  <gml:exterior>
                    <gml:LinearRing gml:id="81891edf-02f8-4524-8d4c-b373a6de3b8c">
                      <gml:posList>552718.668663174730000000 5807839.400779459600000000 55.931259155273438000 552718.668663174610000000 5807839.400779462400000000 54.443112053919648000 552718.555214422640000000 5807842.822489009200000000 58.519263344677071000 552718.668663174730000000 5807839.400779459600000000 55.931259155273438000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="741ac702-f493-42d6-8f07-c55da9270e02">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.02</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>358.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="18990013-39a5-4793-934b-7e4d7440cb1b" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="0184c322-d545-4162-a14b-1582b54358c7">
                  <gml:exterior>
                    <gml:LinearRing gml:id="7f982404-8acc-44af-a9c1-ebb68407081a">
                      <gml:posList>552717.257473592300000000 5807842.779461786200000000 58.517254507484409000 552715.959596480480000000 5807842.736430043400000000 58.515245459332867000 552713.363842256950000000 5807842.650366559600000000 58.511227363029427000 552713.363842256950000000 5807842.650366559600000000 58.519263711565117000 552718.555214422640000000 5807842.822489009200000000 58.519263344677071000 552717.257473592300000000 5807842.779461786200000000 58.517254507484409000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="3328fe0e-b3cb-46f7-8a83-8402ecc6a05f">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.15</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>358.34</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="4cb19616-6c07-49ec-b37b-1818ce2335ff" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="4666a976-d5e8-48dd-b1aa-a212d4c360f9">
                  <gml:exterior>
                    <gml:LinearRing gml:id="8bb99e35-fd18-4e2c-86a7-991e4f123608">
                      <gml:posList>552710.300999999980000000 5807847.130999999100000000 50.930000000000000000 552710.231999999840000000 5807847.129000000700000000 50.930000000000007000 552710.231999999840000000 5807847.129000000700000000 53.095531463623047000 552710.300999999980000000 5807847.130999999100000000 53.095531463623047000 552710.300999999980000000 5807847.130999999100000000 50.930000000000000000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="c69df1f0-4d25-4446-bddd-3c037b03f56e">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>3.72</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>268.03</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="27f7ca01-9b25-46f8-b341-9648f99fd52f" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="1f7815d7-4e46-47cd-bebc-06bae0d45890">
                  <gml:exterior>
                    <gml:LinearRing gml:id="85844211-f0a5-435c-ba13-26da1ef91538">
                      <gml:posList>552710.291000000200000000 5807845.412000000500000000 53.095531463623047000 552710.231999999840000000 5807847.129000000700000000 53.095531463623047000 552710.231999999840000000 5807847.129000000700000000 50.930000000000007000 552710.291000000200000000 5807845.412000000500000000 50.930000000000000000 552710.291000000200000000 5807845.412000000500000000 53.095531463623047000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="2b01d1cb-1e1a-4bbc-a6cc-649226318d8c">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>2.36</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>178.17</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="02fbfdd1-ed61-4cb0-8858-2f0f82a70272" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="dfa71f1b-2eaf-4747-8897-8914c3d0cd90">
                  <gml:exterior>
                    <gml:LinearRing gml:id="cc805a22-80ac-40f4-876c-b0c43f93bad6">
                      <gml:posList>552711.379205986040000000 5807845.446815449700000000 53.095531463623047000 552710.291000000200000000 5807845.412000000500000000 53.095531463623047000 552710.291000000200000000 5807845.412000000500000000 50.930000000000000000 552711.379205986040000000 5807845.446815449700000000 50.929999999999993000 552711.379205986040000000 5807845.446815449700000000 53.095531463623047000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="2ca62341-fa36-4f91-9945-6fe14c56a782">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>2.22</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>358.18</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="bc50c6c9-4fb7-4dd2-a949-1e5c00b68866" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="862dcaf5-1b39-43c6-bcd6-23817f99ac36">
                  <gml:exterior>
                    <gml:LinearRing gml:id="4c336c1b-1f46-4be5-add1-9c33b0601f7d">
                      <gml:posList>552711.324521318780000000 5807847.163506684800000000 50.930000000000007000 552710.300999999980000000 5807847.130999999100000000 50.930000000000000000 552710.300999999980000000 5807847.130999999100000000 53.095531463623047000 552711.324521318780000000 5807847.163506684800000000 53.095531463623047000 552711.324521318780000000 5807847.163506684800000000 50.930000000000007000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="b92ef887-58cf-4666-bf3a-3393ee848200">
          <gen:stringAttribute name="Type">
            <gen:value>Ground</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>96.82</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="3f1be2f1-4a1f-4376-b317-9095cd0f3d4c" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="143083d5-560c-41b5-a620-ba41715de182">
                  <gml:exterior>
                    <gml:LinearRing gml:id="4cbeb382-239c-41ca-91ee-65bb8c4090d8">
                      <gml:posList>552717.468326951140000000 5807847.645471150100000000 50.929999999999986000 552718.948148671650000000 5807847.686872590300000000 50.930000000000000000 552720.025000000370000000 5807847.717000000200000000 50.930000000000000000 552720.163534285380000000 5807843.614597613000000000 50.930000000000007000 552720.303999999540000000 5807839.455000000100000000 50.929999999999993000 552718.668663174610000000 5807839.400779462400000000 50.930000000000007000 552713.477291024870000000 5807839.228656530400000000 50.930000000000007000 552711.678000000310000000 5807839.168999999800000000 50.930000000000000000 552711.566434851030000000 5807843.340739675800000000 50.930000000000000000 552711.509999999780000000 5807845.450999999400000000 50.930000000000007000 552711.379205986040000000 5807845.446815449700000000 50.929999999999993000 552710.291000000200000000 5807845.412000000500000000 50.930000000000000000 552710.231999999840000000 5807847.129000000700000000 50.930000000000007000 552710.300999999980000000 5807847.130999999100000000 50.930000000000000000 552711.324521318780000000 5807847.163506684800000000 50.930000000000007000 552711.466000000010000000 5807847.167999999600000000 50.930000000000007000 552711.459132624440000000 5807847.358760441700000000 50.930000000000007000 552711.457000000400000000 5807847.417999999600000000 50.930000000000000000 552711.387000000100000000 5807847.414999999100000000 50.930000000000007000 552711.380613521090000000 5807847.607078193700000000 50.929999999999993000 552711.254999999890000000 5807851.384999999800000000 50.930000000000000000 552717.263000000270000000 5807851.529999999300000000 50.930000000000000000 552717.375333300210000000 5807847.798038133400000000 50.929999999999993000 552717.379999999890000000 5807847.642999999200000000 50.929999999999993000 552717.468326951140000000 5807847.645471150100000000 50.929999999999986000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </cityObjectMember>

</CityModel>
