<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<CityModel xmlns="http://www.opengis.net/citygml/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:core="http://www.opengis.net/citygml/base/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:tex="http://www.opengis.net/citygml/textures/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/citygml/2.0 http://schemas.opengis.net/citygml/2.0/cityGMLBase.xsd http://www.opengis.net/citygml/appearance/2.0  http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd">

  <gml:description>Exported by RhinoCity(TM) software - (c)Rhinoterrain 2010-2018 : www.rhinoterrain.com</gml:description>

  <gml:name>RhinoCity(TM)</gml:name>

  <gml:boundedBy>
    <gml:Envelope srsDimension="3" srsName="crs:ETRS89_UTM32*DE_DHHN92_NH">
      <gml:lowerCorner>552535.625000 5807993.000000 51.199997</gml:lowerCorner>
      <gml:upperCorner>552545.812500 5808011.500000 61.512882</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>

  <cityObjectMember>
    <bldg:Building gml:id="DENIAL4300017koO">
      <gen:doubleAttribute name="Volume">
        <gen:value>1259.293</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="TotalRoofArea">
        <gen:value>219.60</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="TotalWallArea">
        <gen:value>367.83</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Authoring_Software">
        <gen:value>RhinoCity</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="OSKA">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ALKISID">
        <gen:value>DENIAL4300017koO</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="KACHEL_ID">
        <gen:value>11_13_Building</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="COUNT">
        <gen:value>843</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="DESC">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCK">
        <gen:value>24</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCKNR">
        <gen:value>212024</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STATBEZNR">
        <gen:value>212</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="MBZNR">
        <gen:value>2109</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="WBZNR">
        <gen:value>2109</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNAM">
        <gen:value>Sahlkamp</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNR">
        <gen:value>21</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNAM">
        <gen:value>Bothfeld-Vahrenheide</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNR">
        <gen:value>03</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEMEINDE">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SCALE">
        <gen:value>5000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_overlaps">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_xmax">
        <gen:value>552545.7630000003</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_xmin">
        <gen:value>552535.6440000003</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_ymax">
        <gen:value>5808011.245999999</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_ymin">
        <gen:value>5807993.312000001</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_zmax">
        <gen:value>51.28532599999011</gen:value>
      </gen:stringAttribute>
      <gen:doubleAttribute name="ZMIN">
        <gen:value>51.200000</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="THICKNESS">
        <gen:value>0.100000</gen:value>
      </gen:doubleAttribute>
      <gen:intAttribute name="TEXTURE">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="BUILDINGID">
        <gen:value>DENIAL4300017koO</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ARTBEZEICH">
        <gen:value>Gebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Join_Cou_1">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="FUNKTION_T">
        <gen:value>Wohngebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADT">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="OVERHANG">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="OBJFLAECHE">
        <gen:value>155.9</gen:value>
      </gen:stringAttribute>
      <bldg:measuredHeight uom="#m">17.126</bldg:measuredHeight>
      <bldg:lod2Solid>
        <gml:Solid gml:id="3c5e9314-4595-4b93-8490-c3efeed54364">
          <gml:exterior>
            <gml:CompositeSurface gml:id="fe0a5b64-45ac-4094-a979-d868782696cf">
              <gml:surfaceMember xlink:href="#4f5946cc-fc55-4cd8-b680-9b53156de953"/>
              <gml:surfaceMember xlink:href="#06632654-84f0-4355-9699-848a9b37e41c"/>
              <gml:surfaceMember xlink:href="#18299755-4ed2-47bf-9599-064a47f117eb"/>
              <gml:surfaceMember xlink:href="#acabc956-16be-4283-b7c5-b33aa15deac1"/>
              <gml:surfaceMember xlink:href="#0b0a06c6-9521-4fc0-9619-386dfc3b9e56"/>
              <gml:surfaceMember xlink:href="#9feca2e0-6aaa-4739-84e1-6c2547dd9164"/>
              <gml:surfaceMember xlink:href="#0d0c7c91-cc12-437b-bff9-6c7ac9a32c6b"/>
              <gml:surfaceMember xlink:href="#2584cd5a-6f34-4fa4-9ef6-e59a004a3c89"/>
              <gml:surfaceMember xlink:href="#c094d746-c10f-497f-a009-87793fd56475"/>
              <gml:surfaceMember xlink:href="#6e2835e3-9b77-48d7-a9ca-4e45e00b749a"/>
              <gml:surfaceMember xlink:href="#92efe960-6afc-4a53-8fba-db744be6b19e"/>
              <gml:surfaceMember xlink:href="#8ecf003b-a530-447c-9247-3003c53dd6bc"/>
              <gml:surfaceMember xlink:href="#732958aa-6e00-4ae3-8272-83997c0a5f3e"/>
              <gml:surfaceMember xlink:href="#651fbfa4-5bb4-449d-bdbd-0b3f2c9b79aa"/>
              <gml:surfaceMember xlink:href="#f060156f-1901-4453-a92a-8595e3a6b806"/>
              <gml:surfaceMember xlink:href="#4175950d-6f00-4256-ac4e-3c2c88983505"/>
              <gml:surfaceMember xlink:href="#ff78b98a-da5e-466d-91c5-7f73ca74c44e"/>
              <gml:surfaceMember xlink:href="#f6352be8-df41-4436-a9d5-505c63738b44"/>
              <gml:surfaceMember xlink:href="#79cd30b2-d555-46be-8ee0-c5f02e6b8e5a"/>
              <gml:surfaceMember xlink:href="#561bb512-ce48-408d-9ebb-917a60b34e6e"/>
              <gml:surfaceMember xlink:href="#262f5e53-2dd5-4964-bbff-080c7c924948"/>
              <gml:surfaceMember xlink:href="#8e9569fe-ef88-491f-b135-22773fb02890"/>
              <gml:surfaceMember xlink:href="#cfc09814-9665-4892-a9a2-d77332994e63"/>
              <gml:surfaceMember xlink:href="#71990748-fb8e-4aa1-b5f8-268beccf8c07"/>
              <gml:surfaceMember xlink:href="#50d60db7-8e14-4743-a0bb-83ce1f5beb0e"/>
              <gml:surfaceMember xlink:href="#0e42c593-2edb-4610-903a-42abbdbf60c7"/>
              <gml:surfaceMember xlink:href="#92cf154f-86c5-40da-bc5b-58aa4421dd49"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="e817d6c9-2c56-4855-8fe3-a7a8415d5a51">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL4300017koO_C:29056</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>36.88</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>88.02</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>39.75</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="f8815769-1b90-4a67-9c21-414dd2fe5e82" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="4f5946cc-fc55-4cd8-b680-9b53156de953">
                  <gml:exterior>
                    <gml:LinearRing gml:id="34e6fa95-7eb8-4eaa-8802-42c74b9de4e8">
                      <gml:posList>552545.464999999850000000 5808003.073999999100000000 58.110687370031798000 552545.371590011520000000 5808006.047931006200000000 58.102781866632313000 552541.277215746580000000 5808005.906232285300000000 61.509632204750403000 552541.483763974390000000 5807999.839280718900000000 61.512472642500910000 552541.513270765890000000 5807998.972576239100000000 61.512878417968750000 552545.588181260620000000 5807999.113598464100000000 58.122223593088663000 552545.464999999850000000 5808003.073999999100000000 58.110687370031798000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="98694a73-cf2d-412a-9a6a-24111af0961f">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL4300017koO_C:29056</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>88.02</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>39.75</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="2a842a69-fc67-44bb-80e0-b29d7c54fd03" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="06632654-84f0-4355-9699-848a9b37e41c">
                  <gml:exterior>
                    <gml:LinearRing gml:id="f210067c-ab1e-4ff8-b125-77a1e3852ad7">
                      <gml:posList>552541.513270765890000000 5807998.972576239100000000 61.512878417968750000 552541.273868286630000000 5808005.906116436200000000 61.512417561995079000 552541.273314427000000000 5808005.906097267800000000 61.512878417968750000 552541.513270765890000000 5807998.972576239100000000 61.512878417968750000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="419c2192-8ffb-434f-8c73-b9a24a5e60aa">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL4300017koO_C:29057</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>108.51</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>268.03</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>45.73</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="228a19be-75dd-4c58-b806-d8400bb82bc3" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="18299755-4ed2-47bf-9599-064a47f117eb">
                  <gml:exterior>
                    <gml:LinearRing gml:id="467046ed-02d4-4ab3-94a1-af831ac76e06">
                      <gml:posList>552537.717000000180000000 5807993.312000000800000000 57.420286476846691000 552537.582999999630000000 5807998.287000000500000000 57.458691675511197000 552537.502858725840000000 5807998.833780404200000000 57.395839507411537000 552537.262902287070000000 5808005.767304319900000000 57.394810065653161000 552537.307000000030000000 5808006.262000000100000000 57.457507602743007000 552537.175999999980000000 5808010.966000000000000000 57.489412433943031000 552541.095228011140000000 5808011.102643486100000000 61.512878417968750000 552541.273314427000000000 5808005.906097267800000000 61.511848976639541000 552537.262902287070000000 5808005.767304319900000000 57.394810065653161000 552535.644000000320000000 5808006.196000000500000000 55.749994132847533000 552535.870000000110000000 5807998.222999999300000000 55.699980654785008000 552537.582999999630000000 5807998.287000000500000000 57.458691675511197000 552541.513270765890000000 5807998.972576239100000000 61.512878417968750000 552541.273868286630000000 5808005.906116436200000000 61.512417561995079000 552541.273314427000000000 5808005.906097267800000000 61.511848976639541000 552537.307000000030000000 5808006.262000000100000000 57.457507602743007000 552535.644000000320000000 5808006.196000000500000000 55.749994132847533000 552537.502858725840000000 5807998.833780404200000000 57.395839507411537000 552541.513270765890000000 5807998.972576239100000000 61.512878417968750000 552541.705261185650000000 5807993.401718528900000000 61.512878417968750000 552537.717000000180000000 5807993.312000000800000000 57.420286476846691000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="85054aa3-3cfb-40b5-9077-24103d66341e">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL4300017koO_C:29057</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>34.18</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>88.03</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>48.22</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="9f810848-641d-4d64-b5bb-cf05c0bd94ff" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="acabc956-16be-4283-b7c5-b33aa15deac1">
                  <gml:exterior>
                    <gml:LinearRing gml:id="dc1f9df0-4bb6-45e1-a75a-a03b97c31dc8">
                      <gml:posList>552545.588181260620000000 5807999.113598464100000000 56.949191287781062000 552541.513270765890000000 5807998.972576239100000000 61.512878417968750000 552541.705261185650000000 5807993.401718528900000000 61.512878417968750000 552545.763000000270000000 5807993.493000000700000000 56.970317328297327000 552545.588181260620000000 5807999.113598464100000000 56.949191287781062000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="dbf7ac1c-6d48-446d-b60c-5d82ea120828">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL4300017koO_C:29057</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>32.08</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>88.03</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>48.22</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="08f664e0-a3fc-496c-9d48-628676d968cc" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="0b0a06c6-9521-4fc0-9619-386dfc3b9e56">
                  <gml:exterior>
                    <gml:LinearRing gml:id="4a353011-bcd4-4a8c-91c4-98da6e798537">
                      <gml:posList>552545.371590011520000000 5808006.047931006200000000 56.924146555529482000 552545.218999999580000000 5808010.905999999500000000 56.907551563028868000 552545.207000000400000000 5808011.245999999300000000 56.907867526791293000 552541.095228011140000000 5808011.102643486100000000 61.512878417968750000 552541.513270765890000000 5807998.972576239100000000 61.512878417968750000 552541.483763974390000000 5807999.839280718900000000 61.512472642500910000 552541.454257084760000000 5808000.705988085800000000 61.512066865679387000 552541.277215746580000000 5808005.906232285300000000 61.509632204750403000 552545.371590011520000000 5808006.047931006200000000 56.924146555529482000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="ba34708d-cc29-421e-aaf0-ac6b6162b1ff">
          <gen:stringAttribute name="Index">
            <gen:value>IR_P:DENIAL4300017koO_C:29055</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>InstallationRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>36.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>268.02</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>40.96</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="72002621-1dc9-499a-8e88-7e6561d252eb" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="9feca2e0-6aaa-4739-84e1-6c2547dd9164">
                  <gml:exterior>
                    <gml:LinearRing gml:id="e5369a7f-ca3c-48c6-9a75-02e48ecba700">
                      <gml:posList>552541.273314427000000000 5808005.906097267800000000 61.512878417968750000 552537.262902287070000000 5808005.767304319900000000 58.029945373535156000 552537.502858725840000000 5807998.833780404200000000 58.029945373535156000 552541.513270765890000000 5807998.972576239100000000 61.512878417968750000 552541.273314427000000000 5808005.906097267800000000 61.512878417968750000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="02d83c80-c945-410b-b400-b6cccc108fb7">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>2.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>178.02</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="42fdebf2-ccf2-4ad8-8110-89fea5b68174" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="0d0c7c91-cc12-437b-bff9-6c7ac9a32c6b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="3bb2f694-f0be-4b4a-9ddb-e61eac86b970">
                      <gml:posList>552545.588181260620000000 5807999.113598464100000000 58.122223593088663000 552541.513270765890000000 5807998.972576239100000000 61.512878417968750000 552545.588181260620000000 5807999.113598464100000000 56.949191287781062000 552545.588181260620000000 5807999.113598464100000000 58.122223593088663000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="3ded6ca4-9512-4a46-b324-6ff0f8e9195e">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>4.65</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>88.22</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="31b211c9-0c93-45e6-86aa-23a5d8625315" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="2584cd5a-6f34-4fa4-9ef6-e59a004a3c89">
                  <gml:exterior>
                    <gml:LinearRing gml:id="faa85d05-d53b-4ac0-b6a6-7e6016ad2e0d">
                      <gml:posList>552545.588181260620000000 5807999.113598464100000000 56.949191287781062000 552545.464999999850000000 5808003.073999999100000000 56.934305399059085000 552545.464999999850000000 5808003.073999999100000000 58.110687370031798000 552545.588181260620000000 5807999.113598464100000000 58.122223593088663000 552545.588181260620000000 5807999.113598464100000000 56.949191287781062000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="5c276197-3b88-45e8-8501-2259037182c7">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>3.50</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>88.20</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="340d4b65-39cc-483f-b5de-8f79a20b8166" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="c094d746-c10f-497f-a009-87793fd56475">
                  <gml:exterior>
                    <gml:LinearRing gml:id="34657391-6572-4833-81af-88096f16e95e">
                      <gml:posList>552545.464999999850000000 5808003.073999999100000000 56.934305399059085000 552545.371590011520000000 5808006.047931006200000000 56.924146555529482000 552545.371590011520000000 5808006.047931006200000000 58.102781866632313000 552545.464999999850000000 5808003.073999999100000000 58.110687370031798000 552545.464999999850000000 5808003.073999999100000000 56.934305399059085000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="ff853468-21a0-49ae-9f6e-8fe949e0ce6b">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>358.02</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="ec1bb019-4060-4230-84c3-198cc5a3679d" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="6e2835e3-9b77-48d7-a9ca-4e45e00b749a">
                  <gml:exterior>
                    <gml:LinearRing gml:id="b47042e5-e530-49bd-a5fe-4843956ce17d">
                      <gml:posList>552541.273868286630000000 5808005.906116436200000000 61.512417561995079000 552541.273314427000000000 5808005.906097267800000000 61.511848976639541000 552541.273314427000000000 5808005.906097267800000000 61.512878417968750000 552541.273868286630000000 5808005.906116436200000000 61.512417561995079000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bcb32333-e638-4fef-bcbf-09ebebb3f4f9">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>2.41</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>358.02</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="d966ea56-1153-4f9e-8a98-7e8e991fb9e9" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="92efe960-6afc-4a53-8fba-db744be6b19e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="4106d12c-b81b-460a-ae05-35b90e0037d5">
                      <gml:posList>552545.371590011520000000 5808006.047931006200000000 58.102781866632313000 552545.371590011520000000 5808006.047931006200000000 56.924146555529482000 552541.277215746580000000 5808005.906232285300000000 61.509632204750403000 552545.371590011520000000 5808006.047931006200000000 58.102781866632313000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="353b2bfd-b8ea-44a8-aff0-ff40cf4646e5">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>32.96</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>358.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="a77600c3-20ca-4139-92f0-0b3ea8f00fb1" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="8ecf003b-a530-447c-9247-3003c53dd6bc">
                  <gml:exterior>
                    <gml:LinearRing gml:id="f8b32c09-1497-46c5-b9b3-5f77d9e86653">
                      <gml:posList>552545.207000000400000000 5808011.245999999300000000 51.200000000000003000 552541.095228011140000000 5808011.102643486100000000 51.200000000000003000 552541.095228011140000000 5808011.102643486100000000 61.512878417968750000 552545.207000000400000000 5808011.245999999300000000 56.907867526791293000 552545.207000000400000000 5808011.245999999300000000 51.200000000000003000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="7114bbf1-972c-4f6e-867f-6926aa1d41ba">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>32.55</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>358.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="8529191e-08b6-4309-8a6e-309748ebd744" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="732958aa-6e00-4ae3-8272-83997c0a5f3e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="30dfb3cb-1580-4fd6-9da2-84474a04ce27">
                      <gml:posList>552541.095228011140000000 5808011.102643486100000000 51.200000000000003000 552537.175999999980000000 5808010.966000000000000000 51.200000000000003000 552537.175999999980000000 5808010.966000000000000000 57.489412433943031000 552541.095228011140000000 5808011.102643486100000000 61.512878417968750000 552541.095228011140000000 5808011.102643486100000000 51.200000000000003000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="779b5ad6-52e6-424f-b09c-ce0e970e7a16">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>29.52</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>268.40</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="44f8b129-3e8d-4b72-b248-a6f41a43f69e" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="651fbfa4-5bb4-449d-bdbd-0b3f2c9b79aa">
                  <gml:exterior>
                    <gml:LinearRing gml:id="019ae0da-428e-48aa-b919-7caa32d3b225">
                      <gml:posList>552537.307000000030000000 5808006.262000000100000000 57.457507602743007000 552537.175999999980000000 5808010.966000000000000000 57.489412433943031000 552537.175999999980000000 5808010.966000000000000000 51.200000000000003000 552537.307000000030000000 5808006.262000000100000000 51.200000000000003000 552537.307000000030000000 5808006.262000000100000000 57.457507602743007000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="c9996c10-f3e7-4fe4-bf32-b5f17bf2a8a8">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>8.99</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>357.73</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="ba171579-02c4-49ff-a45a-c0c461ab01a7" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="f060156f-1901-4453-a92a-8595e3a6b806">
                  <gml:exterior>
                    <gml:LinearRing gml:id="4a09edb7-4b68-4a83-8812-e1237c43b7fd">
                      <gml:posList>552537.307000000030000000 5808006.262000000100000000 51.200000000000003000 552535.644000000320000000 5808006.196000000500000000 51.200000000000003000 552535.644000000320000000 5808006.196000000500000000 55.749994132847533000 552537.307000000030000000 5808006.262000000100000000 57.457507602743007000 552537.307000000030000000 5808006.262000000100000000 51.200000000000003000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="e1356036-1b9b-48ce-aac9-f61d9066ca43">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>36.09</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>268.38</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="410e6dea-9717-4fc1-a253-4b8e6f55879b" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="4175950d-6f00-4256-ac4e-3c2c88983505">
                  <gml:exterior>
                    <gml:LinearRing gml:id="9bd8d451-abfb-4dd0-b5b6-6f5f51e08bf5">
                      <gml:posList>552535.870000000110000000 5807998.222999999300000000 55.699980654785008000 552535.644000000320000000 5808006.196000000500000000 55.749994132847533000 552535.644000000320000000 5808006.196000000500000000 51.200000000000003000 552535.870000000110000000 5807998.222999999300000000 51.200000000000003000 552535.870000000110000000 5807998.222999999300000000 55.699980654785008000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="e0242d01-8a6a-4f54-8447-381c4b988c89">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>9.22</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>177.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="5b721cca-2a76-4362-9854-5af68ca2a7b1" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="ff78b98a-da5e-466d-91c5-7f73ca74c44e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="10be40c4-e3ba-4ca2-bff3-2dab0fa62978">
                      <gml:posList>552537.582999999630000000 5807998.287000000500000000 57.458691675511197000 552535.870000000110000000 5807998.222999999300000000 55.699980654785008000 552535.870000000110000000 5807998.222999999300000000 51.200000000000003000 552537.582999999630000000 5807998.287000000500000000 51.200000000000003000 552537.582999999630000000 5807998.287000000500000000 57.458691675511197000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="1908e0b0-c37b-4ac4-ae65-f552e9513f12">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>31.05</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>268.46</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="820dac27-7802-4acb-8624-330c2e6cfb3e" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="f6352be8-df41-4436-a9d5-505c63738b44">
                  <gml:exterior>
                    <gml:LinearRing gml:id="79b7a66b-769b-487c-a0c7-9404cbaa9f18">
                      <gml:posList>552537.717000000180000000 5807993.312000000800000000 57.420286476846691000 552537.582999999630000000 5807998.287000000500000000 57.458691675511197000 552537.582999999630000000 5807998.287000000500000000 51.200000000000003000 552537.717000000180000000 5807993.312000000800000000 51.200000000000003000 552537.717000000180000000 5807993.312000000800000000 57.420286476846691000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="9df7992f-f546-414e-afed-6be2b62a1074">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>65.62</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>178.71</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="32730f01-78f2-436b-adfc-6e5e9de32184" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="79cd30b2-d555-46be-8ee0-c5f02e6b8e5a">
                  <gml:exterior>
                    <gml:LinearRing gml:id="7323d3b7-69b1-4bb7-a6dc-2b48784a141d">
                      <gml:posList>552545.763000000270000000 5807993.493000000700000000 56.970317328297327000 552541.705261185650000000 5807993.401718528900000000 61.512878417968750000 552537.717000000180000000 5807993.312000000800000000 57.420286476846691000 552537.717000000180000000 5807993.312000000800000000 51.200000000000003000 552541.705261185650000000 5807993.401718528900000000 51.200000000000003000 552545.763000000270000000 5807993.493000000700000000 51.200000000000003000 552545.763000000270000000 5807993.493000000700000000 56.970317328297327000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="75f40c21-6ad9-42f6-af33-dd82faa7cba4">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>55.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>88.22</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="84d7b8f2-41d3-4343-aa6a-98729f599543" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="561bb512-ce48-408d-9ebb-917a60b34e6e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="fbad7efa-5902-4d59-84ea-65eb8ac7dc1e">
                      <gml:posList>552545.763000000270000000 5807993.493000000700000000 51.200000000000003000 552545.588181260620000000 5807999.113598464100000000 51.200000000000003000 552545.464999999850000000 5808003.073999999100000000 51.200000000000003000 552545.464999999850000000 5808003.073999999100000000 56.934305399059085000 552545.588181260620000000 5807999.113598464100000000 56.949191287781062000 552545.763000000270000000 5807993.493000000700000000 56.970317328297327000 552545.763000000270000000 5807993.493000000700000000 51.200000000000003000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="e0758c97-421b-4207-867e-c9caa10887de">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>27.78</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>88.20</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="8e0f7ec1-ade5-4a25-9054-7c52675c304e" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="262f5e53-2dd5-4964-bbff-080c7c924948">
                  <gml:exterior>
                    <gml:LinearRing gml:id="25a72ccd-353f-45b8-8931-7d1b36834e2d">
                      <gml:posList>552545.371590011520000000 5808006.047931006200000000 51.200000000000003000 552545.218999999580000000 5808010.905999999500000000 51.200000000000003000 552545.218999999580000000 5808010.905999999500000000 56.907551563028868000 552545.371590011520000000 5808006.047931006200000000 56.924146555529482000 552545.371590011520000000 5808006.047931006200000000 51.200000000000003000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="a56a5947-3f96-4d7f-951f-b0530810b266">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>1.94</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>87.98</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="2466b560-8f3d-4ab8-b748-7ce8465ac6de" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="8e9569fe-ef88-491f-b135-22773fb02890">
                  <gml:exterior>
                    <gml:LinearRing gml:id="7642ceb5-ed1f-414b-aa75-dbfa45f0e59b">
                      <gml:posList>552545.218999999580000000 5808010.905999999500000000 51.200000000000003000 552545.207000000400000000 5808011.245999999300000000 51.200000000000003000 552545.207000000400000000 5808011.245999999300000000 56.907867526791293000 552545.218999999580000000 5808010.905999999500000000 56.907551563028868000 552545.218999999580000000 5808010.905999999500000000 51.200000000000003000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="b1c9e29e-114e-4764-ad6d-8d713519a1c6">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>17.05</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>88.20</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="667456c5-61e5-48d1-9ef7-69a77ae7ec1f" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="cfc09814-9665-4892-a9a2-d77332994e63">
                  <gml:exterior>
                    <gml:LinearRing gml:id="0c048c8e-6707-44fe-ac0b-1d1eea432f84">
                      <gml:posList>552545.464999999850000000 5808003.073999999100000000 51.200000000000003000 552545.371590011520000000 5808006.047931006200000000 51.200000000000003000 552545.371590011520000000 5808006.047931006200000000 56.924146555529482000 552545.464999999850000000 5808003.073999999100000000 56.934305399059085000 552545.464999999850000000 5808003.073999999100000000 51.200000000000003000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="82c36fa8-e083-4559-a3c0-9931aee5bbd2">
          <gen:stringAttribute name="Type">
            <gen:value>InstallationWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>1.28</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>358.02</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="c6733114-75f2-462f-ab75-b532cb7bf393" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="71990748-fb8e-4aa1-b5f8-268beccf8c07">
                  <gml:exterior>
                    <gml:LinearRing gml:id="6f5620cc-260a-4c00-b2c4-74f9e44a092e">
                      <gml:posList>552541.273314427000000000 5808005.906097267800000000 61.511848976639541000 552537.262902287070000000 5808005.767304319900000000 57.394810065653161000 552537.262902287070000000 5808005.767304319900000000 58.029945373535156000 552541.273314427000000000 5808005.906097267800000000 61.512878417968750000 552541.273314427000000000 5808005.906097267800000000 61.511848976639541000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="0eeec392-b542-495c-830c-81d92cfd1a6b">
          <gen:stringAttribute name="Type">
            <gen:value>InstallationWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>4.40</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>268.02</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="bb703c30-e569-4344-9e1f-21f339a81551" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="50d60db7-8e14-4743-a0bb-83ce1f5beb0e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="f9bee1a5-aaaa-4af2-8e96-c9d47634af0a">
                      <gml:posList>552537.502858725840000000 5807998.833780404200000000 58.029945373535156000 552537.262902287070000000 5808005.767304319900000000 58.029945373535156000 552537.262902287070000000 5808005.767304319900000000 57.394810065653161000 552537.502858725840000000 5807998.833780404200000000 57.395839507411537000 552537.502858725840000000 5807998.833780404200000000 58.029945373535156000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="ae65e7b0-2de6-4314-b174-71a6f06e067d">
          <gen:stringAttribute name="Type">
            <gen:value>InstallationWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>1.27</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>178.02</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="aa02dee5-c8a3-40ef-a886-81c17163be97" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="0e42c593-2edb-4610-903a-42abbdbf60c7">
                  <gml:exterior>
                    <gml:LinearRing gml:id="973834ae-a563-4d6b-a96d-d6db63b6142f">
                      <gml:posList>552541.513270765890000000 5807998.972576239100000000 61.512878417968750000 552537.502858725840000000 5807998.833780404200000000 58.029945373535156000 552537.502858725840000000 5807998.833780404200000000 57.395839507411537000 552541.513270765890000000 5807998.972576239100000000 61.512878417968750000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="4c5afff1-d6aa-4f32-86a5-08ee5afdd415">
          <gen:stringAttribute name="Type">
            <gen:value>Ground</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>155.90</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="800e0ff5-49dd-4306-bcc9-3284959ada41" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="92cf154f-86c5-40da-bc5b-58aa4421dd49">
                  <gml:exterior>
                    <gml:LinearRing gml:id="02ae0c07-9308-4d0c-be52-f05461f902c6">
                      <gml:posList>552545.464999999850000000 5808003.073999999100000000 51.200000000000003000 552545.588181260620000000 5807999.113598464100000000 51.200000000000003000 552545.763000000270000000 5807993.493000000700000000 51.200000000000003000 552541.705261185650000000 5807993.401718528900000000 51.200000000000003000 552537.717000000180000000 5807993.312000000800000000 51.200000000000003000 552537.582999999630000000 5807998.287000000500000000 51.200000000000003000 552535.870000000110000000 5807998.222999999300000000 51.200000000000003000 552535.644000000320000000 5808006.196000000500000000 51.200000000000003000 552537.307000000030000000 5808006.262000000100000000 51.200000000000003000 552537.175999999980000000 5808010.966000000000000000 51.200000000000003000 552541.095228011140000000 5808011.102643486100000000 51.200000000000003000 552545.207000000400000000 5808011.245999999300000000 51.200000000000003000 552545.218999999580000000 5808010.905999999500000000 51.200000000000003000 552545.371590011520000000 5808006.047931006200000000 51.200000000000003000 552545.464999999850000000 5808003.073999999100000000 51.200000000000003000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </cityObjectMember>

</CityModel>
