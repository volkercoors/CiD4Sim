<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<CityModel xmlns="http://www.opengis.net/citygml/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:core="http://www.opengis.net/citygml/base/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:tex="http://www.opengis.net/citygml/textures/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/citygml/2.0 http://schemas.opengis.net/citygml/2.0/cityGMLBase.xsd http://www.opengis.net/citygml/appearance/2.0  http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd">

  <gml:description>Exported by RhinoCity(TM) software - (c)Rhinoterrain 2010-2018 : www.rhinoterrain.com</gml:description>

  <gml:name>RhinoCity(TM)</gml:name>

  <gml:boundedBy>
    <gml:Envelope srsDimension="3" srsName="crs:ETRS89_UTM32*DE_DHHN92_NH">
      <gml:lowerCorner>552264.937500 5808299.000000 50.259998</gml:lowerCorner>
      <gml:upperCorner>552277.687500 5808311.000000 58.777325</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>

  <cityObjectMember>
    <bldg:Building gml:id="DENIAL4300017PU9">
      <gen:doubleAttribute name="Volume">
        <gen:value>622.819</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="TotalRoofArea">
        <gen:value>167.23</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="TotalWallArea">
        <gen:value>205.21</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Authoring_Software">
        <gen:value>RhinoCity</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="OSKA">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ALKISID">
        <gen:value>DENIAL4300017PU9</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="KACHEL_ID">
        <gen:value>11_13_Building</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="COUNT">
        <gen:value>843</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="DESC">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCK">
        <gen:value>11</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCKNR">
        <gen:value>212011</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STATBEZNR">
        <gen:value>212</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="MBZNR">
        <gen:value>2107</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="WBZNR">
        <gen:value>2107</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNAM">
        <gen:value>Sahlkamp</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNR">
        <gen:value>21</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNAM">
        <gen:value>Bothfeld-Vahrenheide</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNR">
        <gen:value>03</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEMEINDE">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SCALE">
        <gen:value>5000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_overlaps">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_xmax">
        <gen:value>552277.6770000001</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_xmin">
        <gen:value>552264.9400000004</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_ymax">
        <gen:value>5808310.7129999995</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_ymin">
        <gen:value>5808299.528000001</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_zmax">
        <gen:value>50.35514799997956</gen:value>
      </gen:stringAttribute>
      <gen:doubleAttribute name="ZMIN">
        <gen:value>50.260000</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="THICKNESS">
        <gen:value>0.100000</gen:value>
      </gen:doubleAttribute>
      <gen:intAttribute name="TEXTURE">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="BUILDINGID">
        <gen:value>DENIAL4300017PU9</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ARTBEZEICH">
        <gen:value>Gebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Join_Cou_1">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="FUNKTION_T">
        <gen:value>Wohngebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADT">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="OVERHANG">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="OBJFLAECHE">
        <gen:value>119.9</gen:value>
      </gen:stringAttribute>
      <bldg:measuredHeight uom="#m">15.216</bldg:measuredHeight>
      <bldg:lod2Solid>
        <gml:Solid gml:id="7e9cc618-0e13-48a4-b30d-838a9caa7c23">
          <gml:exterior>
            <gml:CompositeSurface gml:id="ce7ee7a8-462a-4a7f-82c9-a9af07d90a26">
              <gml:surfaceMember xlink:href="#e74f5ab2-afbe-4679-bc50-0fc4ee6ab269"/>
              <gml:surfaceMember xlink:href="#a80494c8-cd57-4ddb-b8fb-5bef49878bdb"/>
              <gml:surfaceMember xlink:href="#6e2db5fe-e443-4d7b-8763-c6bff2b9d990"/>
              <gml:surfaceMember xlink:href="#324884fc-901e-4a3d-b9fe-adb721cea606"/>
              <gml:surfaceMember xlink:href="#f6554f95-fc51-437d-9218-26a030456d1b"/>
              <gml:surfaceMember xlink:href="#6d47ecd7-1ada-42ae-8a9e-6765e6351ad2"/>
              <gml:surfaceMember xlink:href="#fba610b7-65c6-4937-ae43-459b9b8c09f5"/>
              <gml:surfaceMember xlink:href="#3aaea612-ea12-46a3-873c-469345897d72"/>
              <gml:surfaceMember xlink:href="#cf048d81-a06b-4f8d-b7be-74cc6e663e17"/>
              <gml:surfaceMember xlink:href="#c1546e14-253d-41d6-bbde-aa221fa013e6"/>
              <gml:surfaceMember xlink:href="#8650a3dc-5828-42ae-b9ed-6782cafa05a0"/>
              <gml:surfaceMember xlink:href="#99740a7d-ee0e-498b-9d2c-d77a77a30d26"/>
              <gml:surfaceMember xlink:href="#cba36d19-34f5-45f0-9b55-5050cf093904"/>
              <gml:surfaceMember xlink:href="#5338b030-0dc1-469e-aa64-bd16f595aa69"/>
              <gml:surfaceMember xlink:href="#173c4298-1e96-45f2-a3d9-6adf6efe030c"/>
              <gml:surfaceMember xlink:href="#dbb88ed4-c4b4-42cc-bf24-91718cf01fb5"/>
              <gml:surfaceMember xlink:href="#46187a01-c4a7-4065-8c55-d3151e30e99a"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="e4f2515e-35b1-4e30-917c-5d6462bc88d2">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL4300017PU9_C:29071</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>10.87</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>356.87</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>41.97</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="1a614ecf-84f1-4ed9-9dc9-f124f4e28827" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="e74f5ab2-afbe-4679-bc50-0fc4ee6ab269">
                  <gml:exterior>
                    <gml:LinearRing gml:id="61d293ce-62a5-4377-a9ea-52b6625ac1a3">
                      <gml:posList>552277.329681890430000000 5808308.495940796100000000 55.058000000000007000 552277.201000000350000000 5808310.712999999500000000 53.060751360141523000 552273.554924150230000000 5808310.493836970100000000 53.078356709673102000 552273.556349180870000000 5808310.467815377700000000 53.101802135623089000 552273.675284811300000000 5808308.296006048100000000 55.058213546942241000 552277.329681890430000000 5808308.495940796100000000 55.058000000000007000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="96558a3f-8c74-4b45-9856-6caf87428f9f">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL4300017PU9_C:29071</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>23.48</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>176.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>20.45</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="cabf19bb-266e-4bfc-8217-6b77bad1d31a" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="a80494c8-cd57-4ddb-b8fb-5bef49878bdb">
                  <gml:exterior>
                    <gml:LinearRing gml:id="a29e89bf-aea8-42f3-b22f-ac1af6a83630">
                      <gml:posList>552277.677000000140000000 5808302.512000000100000000 52.822594495321852000 552277.329681890430000000 5808308.495940796100000000 55.058000000000007000 552273.675284811300000000 5808308.296006048100000000 55.058213546942241000 552273.901311444700000000 5808304.168674741900000000 53.516510948554142000 552274.003063969080000000 5808302.310635389800000000 52.822532144297156000 552277.677000000140000000 5808302.512000000100000000 52.822594495321852000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="efe188d9-c2ad-47c5-8e6b-4ef69b66e612">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL4300017PU9_C:29072</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>59.71</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>176.87</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>53.60</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="fc51e2ff-8c21-431a-b3cb-9f1f733537bf" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="6e2db5fe-e443-4d7b-8763-c6bff2b9d990">
                  <gml:exterior>
                    <gml:LinearRing gml:id="bd374034-4868-481d-a6ee-58c02ba48016">
                      <gml:posList>552274.003063968150000000 5808302.310635389800000000 56.252960438526742000 552273.901311444000000000 5808304.168674741900000000 58.777320861816406000 552265.308334250000000000 5808303.698094224600000000 58.777320861816406000 552265.553000000310000000 5808299.528000000900000000 53.110558719026358000 552274.014000000430000000 5808300.041999999400000000 53.179162754961880000 552273.882000000220000000 5808302.303999999500000000 56.252952964876989000 552274.003063968150000000 5808302.310635389800000000 56.252960438526742000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bee12a32-2c00-4dca-af4c-7df25ef9c84a">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL4300017PU9_C:29072</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>73.17</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>356.87</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>41.98</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="c3456ead-00cb-4adb-9316-442a56f1ed37" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="324884fc-901e-4a3d-b9fe-adb721cea606">
                  <gml:exterior>
                    <gml:LinearRing gml:id="adc58501-c7b5-4d3d-95f0-5289030eaa7f">
                      <gml:posList>552273.556349180870000000 5808310.467815377700000000 53.101802135623089000 552273.554924150230000000 5808310.493836970100000000 53.078356709673102000 552264.940000000410000000 5808309.975999999800000000 53.119727748033021000 552265.308334250000000000 5808303.698094224600000000 58.777320861816406000 552273.901311444000000000 5808304.168674741900000000 58.777320861816406000 552273.675284811300000000 5808308.296006048100000000 55.058213546942241000 552273.556349180870000000 5808310.467815377700000000 53.101802135623089000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="f70f1aba-da02-45a8-b2a0-d73a3a6dccaf">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>18.96</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>86.87</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="b0b596ba-dd68-42ea-a813-1aa406c80d1b" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="f6554f95-fc51-437d-9218-26a030456d1b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="d8310a34-bf99-440a-8e5d-fd83feaf01b4">
                      <gml:posList>552274.003063969080000000 5808302.310635389800000000 52.822532144297156000 552273.901311444700000000 5808304.168674741900000000 53.516510948554142000 552273.675284811300000000 5808308.296006048100000000 55.058213546942241000 552273.901311444000000000 5808304.168674741900000000 58.777320861816406000 552274.003063968150000000 5808302.310635389800000000 56.252960438526742000 552274.003063969080000000 5808302.310635389800000000 52.822532144297156000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="073d7dd9-d9a4-492a-8145-dffcbf79b9a6">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>8.38</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>86.87</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="2b922b9c-2a88-48cf-ab92-1f6d6691f7c4" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="6d47ecd7-1ada-42ae-8a9e-6765e6351ad2">
                  <gml:exterior>
                    <gml:LinearRing gml:id="a4361768-b754-4f11-86c2-63519a5e63fd">
                      <gml:posList>552273.675284811300000000 5808308.296006048100000000 55.058213546942241000 552273.675295316500000000 5808308.295814218000000000 50.260000000000005000 552273.554924150230000000 5808310.493836970100000000 50.259999999999998000 552273.554924150230000000 5808310.493836970100000000 53.078356709673102000 552273.556349180870000000 5808310.467815377700000000 53.101802135623089000 552273.675284811300000000 5808308.296006048100000000 55.058213546942241000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bdf03808-ae56-4011-b4b5-7ea123b5aacc">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>24.50</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>356.56</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="d204188e-dfda-43d2-8813-d6fd67e43c43" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="fba610b7-65c6-4937-ae43-459b9b8c09f5">
                  <gml:exterior>
                    <gml:LinearRing gml:id="cb4575d5-2698-4d0f-b27c-bc7eea27db8f">
                      <gml:posList>552273.554924150230000000 5808310.493836970100000000 50.259999999999998000 552264.940000000410000000 5808309.975999999800000000 50.260000000000005000 552264.940000000410000000 5808309.975999999800000000 53.119727748033021000 552273.554924150230000000 5808310.493836970100000000 53.078356709673102000 552273.554924150230000000 5808310.493836970100000000 50.259999999999998000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="12240ff5-1882-40c6-94ca-7864c40f3a89">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>59.52</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>266.64</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="dee9147f-dd3c-49ba-aecc-3d534a46bbf9" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="3aaea612-ea12-46a3-873c-469345897d72">
                  <gml:exterior>
                    <gml:LinearRing gml:id="2a5fb1b2-f0a3-460b-a94f-3d08c9c7ee55">
                      <gml:posList>552265.553000000310000000 5808299.528000000900000000 53.110558719026358000 552265.308334250000000000 5808303.698094224600000000 58.777320861816406000 552264.940000000410000000 5808309.975999999800000000 53.119727748033021000 552264.940000000410000000 5808309.975999999800000000 50.260000000000005000 552265.308334250000000000 5808303.698094224600000000 50.260000000000005000 552265.553000000310000000 5808299.528000000900000000 50.260000000000012000 552265.553000000310000000 5808299.528000000900000000 53.110558719026358000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="9f8e8e8a-eec8-447a-a9f9-231f1d9cf265">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>24.45</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>176.52</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="116ebd60-7260-4024-b661-f7d7c8c1e30c" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="cf048d81-a06b-4f8d-b7be-74cc6e663e17">
                  <gml:exterior>
                    <gml:LinearRing gml:id="6c655945-b9e7-40fc-b6a9-3d4db600d252">
                      <gml:posList>552274.014000000430000000 5808300.041999999400000000 53.179162754961880000 552265.553000000310000000 5808299.528000000900000000 53.110558719026358000 552265.553000000310000000 5808299.528000000900000000 50.260000000000012000 552274.014000000430000000 5808300.041999999400000000 50.260000000000005000 552274.014000000430000000 5808300.041999999400000000 53.179162754961880000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="9e2bfa31-f98c-45e9-96f9-ef2675d55640">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>10.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>86.66</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="54c2ffda-2f43-4511-969a-9a76e93913a4" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="c1546e14-253d-41d6-bbde-aa221fa013e6">
                  <gml:exterior>
                    <gml:LinearRing gml:id="b01b3f2d-5856-4c92-a62c-5cde0acd0a18">
                      <gml:posList>552274.014000000430000000 5808300.041999999400000000 50.260000000000005000 552273.882000000220000000 5808302.303999999500000000 50.259999999999991000 552273.882000000220000000 5808302.303999999500000000 56.252952964876989000 552274.014000000430000000 5808300.041999999400000000 53.179162754961880000 552274.014000000430000000 5808300.041999999400000000 50.260000000000005000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="f0e3c4c2-c632-415c-ab04-0927846ca203">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.73</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>176.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="2e00f37a-feea-4977-9039-589a4de54edf" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="8650a3dc-5828-42ae-b9ed-6782cafa05a0">
                  <gml:exterior>
                    <gml:LinearRing gml:id="9f05c650-66f0-4266-b8bc-47302eb2ecba">
                      <gml:posList>552274.003063969080000000 5808302.310635389800000000 52.822532144297156000 552274.003063968150000000 5808302.310635389800000000 56.252960438526742000 552273.882000000220000000 5808302.303999999500000000 56.252952964876989000 552273.882000000220000000 5808302.303999999500000000 50.259999999999991000 552274.003063968150000000 5808302.310635389800000000 50.259999999999998000 552274.003063969080000000 5808302.310635389800000000 52.822532144297156000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="194357ac-4272-4941-af0b-52d590552008">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>8.38</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>266.87</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="b9fe86c6-9272-4e5c-8261-bf20d0f5e0b1" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="99740a7d-ee0e-498b-9d2c-d77a77a30d26">
                  <gml:exterior>
                    <gml:LinearRing gml:id="e80d0355-ec4f-489e-98f0-355569a58e68">
                      <gml:posList>552273.675284811300000000 5808308.296006048100000000 55.058213546942241000 552273.556349180870000000 5808310.467815377700000000 53.101802135623089000 552273.554924150230000000 5808310.493836970100000000 53.078356709673102000 552273.554924150230000000 5808310.493836970100000000 50.259999999999998000 552273.675295316500000000 5808308.295814218000000000 50.260000000000005000 552273.675284811300000000 5808308.296006048100000000 55.058213546942241000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="9de9f64b-2133-4184-933c-de2e101f6431">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>9.43</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>176.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="4c027d49-b437-4aae-b668-b7e3746cde9a" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="cba36d19-34f5-45f0-9b55-5050cf093904">
                  <gml:exterior>
                    <gml:LinearRing gml:id="5eaa9ffa-47dc-4c79-9fd6-edd95d7cc13a">
                      <gml:posList>552277.677000000140000000 5808302.512000000100000000 52.822594495321852000 552274.003063969080000000 5808302.310635389800000000 52.822532144297156000 552274.003063968150000000 5808302.310635389800000000 50.259999999999998000 552277.677000000140000000 5808302.512000000100000000 50.260000000000012000 552277.677000000140000000 5808302.512000000100000000 52.822594495321852000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="09fa1c51-8f36-4ef5-8c8a-d191057aa03b">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>22.06</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>86.68</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="b8e47587-6f38-4e13-94a0-d940d9796ec3" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="5338b030-0dc1-469e-aa64-bd16f595aa69">
                  <gml:exterior>
                    <gml:LinearRing gml:id="d429340e-b422-4da3-8fa0-95934216a897">
                      <gml:posList>552277.677000000140000000 5808302.512000000100000000 50.260000000000012000 552277.329681890430000000 5808308.495940796100000000 50.260000000000005000 552277.329681890430000000 5808308.495940796100000000 55.058000000000007000 552277.677000000140000000 5808302.512000000100000000 52.822594495321852000 552277.677000000140000000 5808302.512000000100000000 50.260000000000012000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="aacfbc22-5da4-4ee2-ac11-1e8553f789e2">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>8.44</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>86.68</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="326cc39e-d7b4-4f6d-9052-aaaff54361a3" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="173c4298-1e96-45f2-a3d9-6adf6efe030c">
                  <gml:exterior>
                    <gml:LinearRing gml:id="c6b644f2-3913-42f7-aba9-7d909c76bca7">
                      <gml:posList>552277.329681890430000000 5808308.495940796100000000 50.260000000000005000 552277.201000000350000000 5808310.712999999500000000 50.260000000000005000 552277.201000000350000000 5808310.712999999500000000 53.060751360141523000 552277.329681890430000000 5808308.495940796100000000 55.058000000000007000 552277.329681890430000000 5808308.495940796100000000 50.260000000000005000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="d9e47688-1b97-41e2-8b08-320459272943">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>10.26</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>356.56</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="6770692c-6155-4412-b482-c90c7c58f338" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="dbb88ed4-c4b4-42cc-bf24-91718cf01fb5">
                  <gml:exterior>
                    <gml:LinearRing gml:id="7b81a4d1-32c0-44b8-bae3-1a73c7650465">
                      <gml:posList>552277.201000000350000000 5808310.712999999500000000 50.260000000000005000 552273.554924150230000000 5808310.493836970100000000 50.259999999999998000 552273.554924150230000000 5808310.493836970100000000 53.078356709673102000 552277.201000000350000000 5808310.712999999500000000 53.060751360141523000 552277.201000000350000000 5808310.712999999500000000 50.260000000000005000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="74c63c82-6e3c-4b86-98ae-5cd9a51938ee">
          <gen:stringAttribute name="Type">
            <gen:value>Ground</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>119.90</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="cc073290-fb25-497a-b1c4-3cd4f3701824" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="46187a01-c4a7-4065-8c55-d3151e30e99a">
                  <gml:exterior>
                    <gml:LinearRing gml:id="3488aaee-79c5-4d77-8b36-142a4e04c577">
                      <gml:posList>552274.003063968150000000 5808302.310635389800000000 50.259999999999998000 552273.882000000220000000 5808302.303999999500000000 50.259999999999991000 552274.014000000430000000 5808300.041999999400000000 50.260000000000005000 552265.553000000310000000 5808299.528000000900000000 50.260000000000012000 552265.308334250000000000 5808303.698094224600000000 50.260000000000005000 552264.940000000410000000 5808309.975999999800000000 50.260000000000005000 552273.554924150230000000 5808310.493836970100000000 50.259999999999998000 552277.201000000350000000 5808310.712999999500000000 50.260000000000005000 552277.329681890430000000 5808308.495940796100000000 50.260000000000005000 552277.677000000140000000 5808302.512000000100000000 50.260000000000012000 552274.003063968150000000 5808302.310635389800000000 50.259999999999998000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </cityObjectMember>

</CityModel>
