<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<CityModel xmlns="http://www.opengis.net/citygml/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:core="http://www.opengis.net/citygml/base/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:tex="http://www.opengis.net/citygml/textures/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/citygml/2.0 http://schemas.opengis.net/citygml/2.0/cityGMLBase.xsd http://www.opengis.net/citygml/appearance/2.0  http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd">

  <gml:description>Exported by RhinoCity(TM) software - (c)Rhinoterrain 2010-2018 : www.rhinoterrain.com</gml:description>

  <gml:name>RhinoCity(TM)</gml:name>

  <gml:boundedBy>
    <gml:Envelope srsDimension="3" srsName="crs:ETRS89_UTM32*DE_DHHN92_NH">
      <gml:lowerCorner>552236.625000 5808360.000000 50.539997</gml:lowerCorner>
      <gml:upperCorner>552252.125000 5808371.500000 60.520531</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>

  <cityObjectMember>
    <bldg:Building gml:id="DENIAL43000190bG">
      <gen:doubleAttribute name="Volume">
        <gen:value>1042.030</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="TotalRoofArea">
        <gen:value>194.96</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="TotalWallArea">
        <gen:value>322.76</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Authoring_Software">
        <gen:value>RhinoCity</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="OSKA">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ALKISID">
        <gen:value>DENIAL43000190bG</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="KACHEL_ID">
        <gen:value>11_13_Building</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="COUNT">
        <gen:value>843</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="DESC">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCK">
        <gen:value>11</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCKNR">
        <gen:value>212011</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STATBEZNR">
        <gen:value>212</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="MBZNR">
        <gen:value>2107</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="WBZNR">
        <gen:value>2107</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNAM">
        <gen:value>Sahlkamp</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNR">
        <gen:value>21</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNAM">
        <gen:value>Bothfeld-Vahrenheide</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNR">
        <gen:value>03</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEMEINDE">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SCALE">
        <gen:value>5000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_overlaps">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_xmax">
        <gen:value>552252.0870000003</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_xmin">
        <gen:value>552236.6789999995</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_ymax">
        <gen:value>5808371.194</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_ymin">
        <gen:value>5808360.2190000005</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_zmax">
        <gen:value>50.67976399999857</gen:value>
      </gen:stringAttribute>
      <gen:doubleAttribute name="ZMIN">
        <gen:value>50.540000</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="THICKNESS">
        <gen:value>0.100000</gen:value>
      </gen:doubleAttribute>
      <gen:intAttribute name="TEXTURE">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="BUILDINGID">
        <gen:value>DENIAL43000190bG</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ARTBEZEICH">
        <gen:value>Gebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Join_Cou_1">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="FUNKTION_T">
        <gen:value>Wohngebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADT">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="OVERHANG">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="OBJFLAECHE">
        <gen:value>144.52</gen:value>
      </gen:stringAttribute>
      <bldg:measuredHeight uom="#m">15.916</bldg:measuredHeight>
      <bldg:lod2Solid>
        <gml:Solid gml:id="89cdbd7d-a82b-478d-bf75-b72a1ef42e6b">
          <gml:exterior>
            <gml:CompositeSurface gml:id="a99b1bf1-5112-46ed-b961-c750e3a70c15">
              <gml:surfaceMember xlink:href="#f9fad0ad-b9cc-4fd7-93d7-a56dfe038448"/>
              <gml:surfaceMember xlink:href="#83469785-244d-4bf8-b85d-f82bbc6ba882"/>
              <gml:surfaceMember xlink:href="#6935ccc3-df68-4bb3-81fc-5e431deba9c7"/>
              <gml:surfaceMember xlink:href="#a051f404-d115-4137-a307-df1319331b2c"/>
              <gml:surfaceMember xlink:href="#35a72019-2d7f-4818-9698-db7cc2fd1650"/>
              <gml:surfaceMember xlink:href="#b8121577-094a-4a7b-98ac-55ff2ec6f652"/>
              <gml:surfaceMember xlink:href="#9718fa91-6516-45cb-be75-64c333060b00"/>
              <gml:surfaceMember xlink:href="#6305e1ab-b967-493f-9628-53bb4bbed509"/>
              <gml:surfaceMember xlink:href="#03f68c63-8aa3-44fd-90f1-b0a2b0e7695e"/>
              <gml:surfaceMember xlink:href="#9325b7f6-d384-44b7-8109-a481f6a8270b"/>
              <gml:surfaceMember xlink:href="#d5b0fc4f-ce05-4080-b43e-f7729c451ba7"/>
              <gml:surfaceMember xlink:href="#554ac7a0-5fb3-4cd5-a873-fd1305157560"/>
              <gml:surfaceMember xlink:href="#557cf279-e675-4072-bc4f-bfe65c8176ce"/>
              <gml:surfaceMember xlink:href="#ab824606-3a6a-4ef3-ade0-08df4055f4e4"/>
              <gml:surfaceMember xlink:href="#e7e64047-da72-4289-879b-07433dab3dd8"/>
              <gml:surfaceMember xlink:href="#07b9b6de-8a09-458b-a6f4-72d7c1bbb9ae"/>
              <gml:surfaceMember xlink:href="#81ed5845-febc-4468-9b76-db6f7ef0371d"/>
              <gml:surfaceMember xlink:href="#0093ece7-3e4d-4d8c-b11a-8655f38bb5be"/>
              <gml:surfaceMember xlink:href="#cebbee33-739b-4472-a757-0416a4a35ecb"/>
              <gml:surfaceMember xlink:href="#39ff4176-e22f-4f88-a68a-5750b6f5b167"/>
              <gml:surfaceMember xlink:href="#b874c022-7709-467e-9d86-097b285e6360"/>
              <gml:surfaceMember xlink:href="#3f866bc9-f47d-4db6-ac83-27f2edffd203"/>
              <gml:surfaceMember xlink:href="#c98067f8-ca40-4905-a225-b8fe4350678e"/>
              <gml:surfaceMember xlink:href="#7a044ede-8470-49fa-bcd5-4274f16ccf91"/>
              <gml:surfaceMember xlink:href="#5973cae1-49ce-4c31-8e51-7d3b7bfa5ed6"/>
              <gml:surfaceMember xlink:href="#1034a8dc-95c1-493e-aba3-1f18b2f7ba26"/>
              <gml:surfaceMember xlink:href="#e4240289-a3af-4161-98df-3b50599a1010"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="b4befeeb-190d-4983-9a75-b711f041e508">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL43000190bG_C:29050</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>12.54</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="ec8995eb-c9eb-48c5-bff9-6acac3293ecf" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="f9fad0ad-b9cc-4fd7-93d7-a56dfe038448">
                  <gml:exterior>
                    <gml:LinearRing gml:id="e57d923c-6761-4769-919f-883d1d574a64">
                      <gml:posList>552252.058502694010000000 5808363.083946664800000000 55.122966766357422000 552247.323415678930000000 5808363.021033382000000000 55.122966766357422000 552247.358652588450000000 5808360.368972864900000000 55.122966766357422000 552252.087000000290000000 5808360.436000000700000000 55.122966766357422000 552252.058502694010000000 5808363.083946664800000000 55.122966766357422000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="d774c489-f7a8-48ee-b494-4e1aff3ebb0a">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL43000190bG_C:29051</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>17.47</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>179.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>23.97</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="de243040-c51f-496d-a61e-40634b3a2a7b" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="83469785-244d-4bf8-b85d-f82bbc6ba882">
                  <gml:exterior>
                    <gml:LinearRing gml:id="c3c777b1-566c-444e-8c36-1ad6c8d9599e">
                      <gml:posList>552252.058502694010000000 5808363.083946664800000000 57.545051574707031000 552252.022268083880000000 5808366.450837335500000000 59.042000000000009000 552247.278682596400000000 5808366.387811136400000000 59.042000000000009000 552247.278688210530000000 5808366.387388582300000000 59.041812122439111000 552247.297826362540000000 5808364.946980274300000000 58.401372774937130000 552247.323415679740000000 5808363.021033382000000000 57.545051574707031000 552252.058502694010000000 5808363.083946664800000000 57.545051574707031000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="acd4ffc7-8874-4fdf-b8c0-dcc36c512eff">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL43000190bG_C:29051</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>21.66</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>359.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>45.75</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="cb5645e8-3a7f-4c0b-9351-bd88c801ace9" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="6935ccc3-df68-4bb3-81fc-5e431deba9c7">
                  <gml:exterior>
                    <gml:LinearRing gml:id="a5061a30-1615-4135-acce-476e051a03f2">
                      <gml:posList>552251.987999999900000000 5808369.634999999800000000 55.772678887095125000 552247.236427880360000000 5808369.568057778300000000 55.776555685585194000 552247.278682596400000000 5808366.387811136400000000 59.042000000000009000 552252.022268083880000000 5808366.450837335500000000 59.042000000000009000 552251.987999999900000000 5808369.634999999800000000 55.772678887095125000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="1981690a-4d1b-42e0-bb66-d7561ca69b80">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL43000190bG_C:29049</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>3.34</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>359.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>13.13</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="b0cea742-54e7-4da7-a081-4b4842b11b10" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="a051f404-d115-4137-a307-df1319331b2c">
                  <gml:exterior>
                    <gml:LinearRing gml:id="e35f6898-7a8a-4dae-91a9-2bcc3a14b0e0">
                      <gml:posList>552244.794999999930000000 5808371.194000000100000000 54.656717537983809000 552242.584999999960000000 5808371.163000000600000000 54.657099249069574000 552242.606251531050000000 5808369.693102444500000000 55.000000000000000000 552244.816275187880000000 5808369.722466180100000000 55.000000000000000000 552244.794999999930000000 5808371.194000000100000000 54.656717537983809000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="b8e6c8bc-137e-4b2f-ac9f-bbef1637a78f">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL43000190bG_C:29052</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>69.46</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>179.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>45.75</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="74409469-daae-401c-b765-d3021796e9df" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="35a72019-2d7f-4818-9698-db7cc2fd1650">
                  <gml:exterior>
                    <gml:LinearRing gml:id="599fe53b-70bb-45f4-aa5d-d92a6642521c">
                      <gml:posList>552247.297826362310000000 5808364.946980274300000000 60.520526885986328000 552236.729135237520000000 5808364.806558134000000000 60.520526885986328000 552236.779000000100000000 5808360.219000000500000000 55.811117693938414000 552247.358652588450000000 5808360.368972864900000000 55.820771194151732000 552247.297826362310000000 5808364.946980274300000000 60.520526885986328000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="c1b6b04a-fb9d-4f4d-b121-1b45e6848764">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL43000190bG_C:29052</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>70.50</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>359.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>45.75</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="d8d2c9ec-53ec-4d5e-a81f-3e20acb20afb" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="b8121577-094a-4a7b-98ac-55ff2ec6f652">
                  <gml:exterior>
                    <gml:LinearRing gml:id="f5a22356-023e-49b1-98c9-97842f329d28">
                      <gml:posList>552244.819000000130000000 5808369.534000000000000000 55.778545225854209000 552244.816275187880000000 5808369.722466181000000000 55.585063934326172000 552242.606251531050000000 5808369.693102445500000000 55.585063934326172000 552242.609000000170000000 5808369.503000000500000000 55.780225031010531000 552240.069000000130000000 5808369.467000000200000000 55.782536508729834000 552236.678999999540000000 5808369.418999999800000000 55.785573020498973000 552236.729135237520000000 5808364.806558134000000000 60.520526885986328000 552247.297826362310000000 5808364.946980274300000000 60.520526885986328000 552247.278688210530000000 5808366.387388582300000000 59.041812122439111000 552247.278682596170000000 5808366.387811136400000000 59.041378330661296000 552247.236427880360000000 5808369.568057778300000000 55.776555685585194000 552244.819000000130000000 5808369.534000000000000000 55.778545225854209000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="ddc15875-49a1-4352-a230-77599155e121">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>11.47</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>179.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="851cde09-0b79-4a33-a533-4a3bcbaa5402" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="9718fa91-6516-45cb-be75-64c333060b00">
                  <gml:exterior>
                    <gml:LinearRing gml:id="5724bf7f-f5df-4225-9c1e-2f0a59e62b46">
                      <gml:posList>552252.058502694010000000 5808363.083946664800000000 57.545051574707031000 552247.323415679740000000 5808363.021033382000000000 57.545051574707031000 552247.323415678930000000 5808363.021033382000000000 55.122966766357422000 552252.058502694010000000 5808363.083946664800000000 55.122966766357422000 552252.058502694010000000 5808363.083946664800000000 57.545051574707031000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="31355547-1ba5-45bd-b3ce-795da1c5a787">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>47.97</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>89.38</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="5d688312-baf0-4241-98f3-c6cc317b2784" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="6305e1ab-b967-493f-9628-53bb4bbed509">
                  <gml:exterior>
                    <gml:LinearRing gml:id="7775243f-34f6-49d9-8ff8-ad91c374324c">
                      <gml:posList>552252.058502694010000000 5808363.083946664800000000 55.122966766357422000 552252.058502694010000000 5808363.083946664800000000 50.540000000000006000 552252.022268083880000000 5808366.450837335500000000 50.539999999999999000 552251.987999999900000000 5808369.634999999800000000 50.539999999999999000 552251.987999999900000000 5808369.634999999800000000 55.772678887095125000 552252.022268083880000000 5808366.450837335500000000 59.042000000000009000 552252.058502694010000000 5808363.083946664800000000 57.545051574707031000 552252.058502694010000000 5808363.083946664800000000 55.122966766357422000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="ef3e234c-8bf0-4aa0-b6ed-4a9410c8d089">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>24.88</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>359.19</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="e4188fa9-548c-4c4c-97dd-7569ccee6aa7" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="03f68c63-8aa3-44fd-90f1-b0a2b0e7695e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="9639671e-7279-423c-ac88-f20cbf3030dd">
                      <gml:posList>552251.987999999900000000 5808369.634999999800000000 50.539999999999999000 552247.236427880360000000 5808369.568057778300000000 50.539999999999999000 552247.236427880360000000 5808369.568057778300000000 55.776555685585194000 552251.987999999900000000 5808369.634999999800000000 55.772678887095125000 552251.987999999900000000 5808369.634999999800000000 50.539999999999999000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="f5df4653-0622-4bd7-ab59-59d5faae6541">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>269.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="0b1c47d5-c4cf-4466-b0d2-b7047c2e2f84" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="9325b7f6-d384-44b7-8109-a481f6a8270b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="b9c493fb-ed0f-4456-b7cc-076b3ba9e446">
                      <gml:posList>552247.278688210530000000 5808366.387388582300000000 59.041812122439111000 552247.278682596400000000 5808366.387811136400000000 59.042000000000009000 552247.278682596170000000 5808366.387811136400000000 59.041378330661296000 552247.278688210530000000 5808366.387388582300000000 59.041812122439111000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bedac8da-9f4f-4e7d-a8bf-0e03c8036b51">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>269.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="2f52c320-fbb4-4cb4-a499-cff46ce32988" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="d5b0fc4f-ce05-4080-b43e-f7729c451ba7">
                  <gml:exterior>
                    <gml:LinearRing gml:id="f739b11b-def4-44e8-baea-cf601944fa62">
                      <gml:posList>552247.278682596400000000 5808366.387811136400000000 59.042000000000009000 552247.236427880360000000 5808369.568057778300000000 55.776555685585194000 552247.278682596170000000 5808366.387811136400000000 59.041378330661296000 552247.278682596400000000 5808366.387811136400000000 59.042000000000009000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="73b3781f-e857-425c-b8f4-80a0d36ebb8c">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>8.46</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>89.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="37e87c63-4539-4dc0-afb7-cb32b41886b4" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="554ac7a0-5fb3-4cd5-a873-fd1305157560">
                  <gml:exterior>
                    <gml:LinearRing gml:id="cdbdc149-3870-4abc-aa1b-36152783a4f9">
                      <gml:posList>552247.323415678930000000 5808363.021033382000000000 55.122966766357422000 552247.323415679740000000 5808363.021033382000000000 57.545051574707031000 552247.297826362540000000 5808364.946980274300000000 58.401372774937130000 552247.297826362310000000 5808364.946980274300000000 60.520526885986328000 552247.358652588450000000 5808360.368972864900000000 55.820771194151732000 552247.358652588450000000 5808360.368972864900000000 55.122966766357422000 552247.323415678930000000 5808363.021033382000000000 55.122966766357422000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="8e5633b8-39be-4876-be15-49590bac6134">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>12.66</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>359.19</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="69901814-4ebc-4469-b9c2-371fa108a2fd" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="557cf279-e675-4072-bc4f-bfe65c8176ce">
                  <gml:exterior>
                    <gml:LinearRing gml:id="f5d1a79a-84ae-4fbf-ae27-bf006c461750">
                      <gml:posList>552247.236427880360000000 5808369.568057778300000000 50.539999999999999000 552244.819000000130000000 5808369.534000000000000000 50.539999999999992000 552244.819000000130000000 5808369.534000000000000000 55.778545225854209000 552247.236427880360000000 5808369.568057778300000000 55.776555685585194000 552247.236427880360000000 5808369.568057778300000000 50.539999999999999000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="491200c5-a3a1-46ba-a054-c3d1744f9995">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.97</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>89.17</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="e7bdcc3a-2bb5-40b7-8e19-1e9cb0834307" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="ab824606-3a6a-4ef3-ade0-08df4055f4e4">
                  <gml:exterior>
                    <gml:LinearRing gml:id="a2cb6c93-3c36-4ba8-b262-01772aa3061e">
                      <gml:posList>552244.819000000130000000 5808369.534000000000000000 55.778545225854209000 552244.819000000130000000 5808369.534000000000000000 50.539999999999992000 552244.816275187880000000 5808369.722466181000000000 50.539999999999992000 552244.816275187880000000 5808369.722466180100000000 55.000000000000000000 552244.816275187880000000 5808369.722466181000000000 55.585063934326172000 552244.819000000130000000 5808369.534000000000000000 55.778545225854209000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="a4ce4c08-154d-4695-9276-dd3c447840e1">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>1.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>359.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="47ae19a9-4e2f-43c8-bded-e7f779cb1073" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="e7e64047-da72-4289-879b-07433dab3dd8">
                  <gml:exterior>
                    <gml:LinearRing gml:id="8ed07943-a8c3-4275-a79b-f7e593566411">
                      <gml:posList>552244.816275187880000000 5808369.722466180100000000 55.000000000000000000 552242.606251531050000000 5808369.693102444500000000 55.000000000000000000 552242.606251531050000000 5808369.693102445500000000 55.585063934326172000 552244.816275187880000000 5808369.722466181000000000 55.585063934326172000 552244.816275187880000000 5808369.722466180100000000 55.000000000000000000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="4552c1e7-2bb5-48e2-a88e-5ff53dcc4f06">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>0.98</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>269.17</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="5f997e66-6187-4e0e-9023-4cf1b6282bfc" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="07b9b6de-8a09-458b-a6f4-72d7c1bbb9ae">
                  <gml:exterior>
                    <gml:LinearRing gml:id="d74c6fc4-f3a5-422f-a46f-a69b18106c82">
                      <gml:posList>552242.609000000170000000 5808369.503000000500000000 55.780225031010531000 552242.606251531050000000 5808369.693102445500000000 55.585063934326172000 552242.606251531050000000 5808369.693102444500000000 55.000000000000000000 552242.606251531050000000 5808369.693102445500000000 50.539999999999999000 552242.609000000170000000 5808369.503000000500000000 50.539999999999999000 552242.609000000170000000 5808369.503000000500000000 55.780225031010531000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="6ef81ecb-9e12-44d4-9e18-d16c2f5c4e09">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>13.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>359.19</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="0c417112-5059-4ea0-b55e-c4fdafc5517a" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="81ed5845-febc-4468-9b76-db6f7ef0371d">
                  <gml:exterior>
                    <gml:LinearRing gml:id="e2128461-4ab8-4c5e-afae-b98e13790710">
                      <gml:posList>552242.609000000170000000 5808369.503000000500000000 50.539999999999999000 552240.069000000130000000 5808369.467000000200000000 50.540000000000006000 552240.069000000130000000 5808369.467000000200000000 55.782536508729834000 552242.609000000170000000 5808369.503000000500000000 55.780225031010531000 552242.609000000170000000 5808369.503000000500000000 50.539999999999999000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="23200790-1f60-45ae-98e9-34d945edda11">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>17.78</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>359.19</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="7ec65b87-d058-48a8-9f22-d1340abfffee" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="0093ece7-3e4d-4d8c-b11a-8655f38bb5be">
                  <gml:exterior>
                    <gml:LinearRing gml:id="07b47102-34be-454d-b8db-417d21bd260b">
                      <gml:posList>552240.069000000130000000 5808369.467000000200000000 50.540000000000006000 552236.678999999540000000 5808369.418999999800000000 50.539999999999999000 552236.678999999540000000 5808369.418999999800000000 55.785573020498973000 552240.069000000130000000 5808369.467000000200000000 55.782536508729834000 552240.069000000130000000 5808369.467000000200000000 50.540000000000006000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="8a4802c2-de6e-49b0-8809-66feca2e227d">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>70.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>269.38</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="8ae47aeb-0941-4b13-8b72-bb7b3793c38b" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="cebbee33-739b-4472-a757-0416a4a35ecb">
                  <gml:exterior>
                    <gml:LinearRing gml:id="9b2c18a8-90c7-4051-b1c0-96c6a75e1b0b">
                      <gml:posList>552236.779000000100000000 5808360.219000000500000000 55.811117693938414000 552236.729135237520000000 5808364.806558134000000000 60.520526885986328000 552236.678999999540000000 5808369.418999999800000000 55.785573020498973000 552236.678999999540000000 5808369.418999999800000000 50.539999999999999000 552236.729135237520000000 5808364.806558134000000000 50.539999999999992000 552236.779000000100000000 5808360.219000000500000000 50.539999999999999000 552236.779000000100000000 5808360.219000000500000000 55.811117693938414000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="d208ecd6-07f4-47e2-b89e-aefd6e94fa56">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>55.82</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>179.19</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="3edc442d-6953-4a73-a530-35f14ac5c3de" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="39ff4176-e22f-4f88-a68a-5750b6f5b167">
                  <gml:exterior>
                    <gml:LinearRing gml:id="2b462b71-b989-4360-a79f-a1cdc8f56b3e">
                      <gml:posList>552247.358652588450000000 5808360.368972864900000000 55.122966766357422000 552247.358652588450000000 5808360.368972864900000000 55.820771194151732000 552236.779000000100000000 5808360.219000000500000000 55.811117693938414000 552236.779000000100000000 5808360.219000000500000000 50.539999999999999000 552247.358652588450000000 5808360.368972864900000000 50.540000000000006000 552247.358652588450000000 5808360.368972864900000000 55.122966766357422000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="b8378fc9-1bc3-45f0-be7d-e8a01138d42a">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>1.53</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>89.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="5cf13969-e813-45cf-bb04-2d9b32bab84f" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="b874c022-7709-467e-9d86-097b285e6360">
                  <gml:exterior>
                    <gml:LinearRing gml:id="905a666f-e559-4607-b7b3-71c468b54329">
                      <gml:posList>552247.297826362540000000 5808364.946980274300000000 58.401372774937130000 552247.278688210530000000 5808366.387388582300000000 59.041812122439111000 552247.297826362310000000 5808364.946980274300000000 60.520526885986328000 552247.297826362540000000 5808364.946980274300000000 58.401372774937130000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="cca454ec-bc04-4e46-9371-2117466c2281">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>12.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>89.38</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="8013b837-7650-4d92-a9c0-34e3ec4202cf" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="3f866bc9-f47d-4db6-ac83-27f2edffd203">
                  <gml:exterior>
                    <gml:LinearRing gml:id="d6cf9401-5c59-4e99-8a02-c2e94d345408">
                      <gml:posList>552252.087000000290000000 5808360.436000000700000000 50.539999999999999000 552252.058502694010000000 5808363.083946664800000000 50.540000000000006000 552252.058502694010000000 5808363.083946664800000000 55.122966766357422000 552252.087000000290000000 5808360.436000000700000000 55.122966766357422000 552252.087000000290000000 5808360.436000000700000000 50.539999999999999000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="269b5332-bd93-418b-9210-7c61f5fea192">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>21.67</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>179.19</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="df7914a8-5452-44dc-8ed0-10c57be34497" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="c98067f8-ca40-4905-a225-b8fe4350678e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="5869364a-2448-4626-91a7-439542c621ba">
                      <gml:posList>552252.087000000290000000 5808360.436000000700000000 55.122966766357422000 552247.358652588450000000 5808360.368972864900000000 55.122966766357422000 552247.358652588450000000 5808360.368972864900000000 50.540000000000006000 552252.087000000290000000 5808360.436000000700000000 50.539999999999999000 552252.087000000290000000 5808360.436000000700000000 55.122966766357422000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="3cc26375-5247-4268-87b0-72d09bc52871">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>6.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>89.17</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="276f0dbd-5019-467f-aa90-57dfbba6ec70" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="7a044ede-8470-49fa-bcd5-4274f16ccf91">
                  <gml:exterior>
                    <gml:LinearRing gml:id="876dbced-3e36-4e94-a66f-c09d4fade820">
                      <gml:posList>552244.816275187880000000 5808369.722466181000000000 50.539999999999992000 552244.794999999930000000 5808371.194000000100000000 50.539999999999999000 552244.794999999930000000 5808371.194000000100000000 54.656717537983809000 552244.816275187880000000 5808369.722466180100000000 55.000000000000000000 552244.816275187880000000 5808369.722466181000000000 50.539999999999992000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="43d7cf68-f82d-4041-885e-05cf2220e10f">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>9.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>359.20</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="b0caa6eb-6db7-43a4-a71c-11c7fd8cc72e" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="5973cae1-49ce-4c31-8e51-7d3b7bfa5ed6">
                  <gml:exterior>
                    <gml:LinearRing gml:id="b7861211-f179-47bf-9800-8dd26102bf20">
                      <gml:posList>552244.794999999930000000 5808371.194000000100000000 50.539999999999999000 552242.584999999960000000 5808371.163000000600000000 50.540000000000006000 552242.584999999960000000 5808371.163000000600000000 54.657099249069574000 552244.794999999930000000 5808371.194000000100000000 54.656717537983809000 552244.794999999930000000 5808371.194000000100000000 50.539999999999999000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="39e4b836-4054-4cdd-94d0-3612e643ebb7">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>6.30</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>269.17</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="0f260500-cc3f-49dd-a907-1a3638ea3868" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="1034a8dc-95c1-493e-aba3-1f18b2f7ba26">
                  <gml:exterior>
                    <gml:LinearRing gml:id="1618d61f-5dc8-4a7a-9025-6c106a5b7c3e">
                      <gml:posList>552242.606251531050000000 5808369.693102444500000000 55.000000000000000000 552242.584999999960000000 5808371.163000000600000000 54.657099249069574000 552242.584999999960000000 5808371.163000000600000000 50.540000000000006000 552242.606251531050000000 5808369.693102445500000000 50.539999999999999000 552242.606251531050000000 5808369.693102444500000000 55.000000000000000000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="7065e8f4-6b61-44c3-8ab3-27c5af8958b6">
          <gen:stringAttribute name="Type">
            <gen:value>Ground</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>144.52</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="b1c2252b-34ce-4a86-8a42-977c022ec026" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="e4240289-a3af-4161-98df-3b50599a1010">
                  <gml:exterior>
                    <gml:LinearRing gml:id="c28c2882-7fd8-4848-a5c2-c034b4708879">
                      <gml:posList>552247.236427880360000000 5808369.568057778300000000 50.539999999999999000 552251.987999999900000000 5808369.634999999800000000 50.539999999999999000 552252.022268083880000000 5808366.450837335500000000 50.539999999999999000 552252.058502694010000000 5808363.083946664800000000 50.540000000000006000 552252.087000000290000000 5808360.436000000700000000 50.539999999999999000 552247.358652588450000000 5808360.368972864900000000 50.540000000000006000 552236.779000000100000000 5808360.219000000500000000 50.539999999999999000 552236.729135237520000000 5808364.806558134000000000 50.539999999999992000 552236.678999999540000000 5808369.418999999800000000 50.539999999999999000 552240.069000000130000000 5808369.467000000200000000 50.540000000000006000 552242.609000000170000000 5808369.503000000500000000 50.539999999999999000 552242.606251531050000000 5808369.693102445500000000 50.539999999999999000 552242.584999999960000000 5808371.163000000600000000 50.540000000000006000 552244.794999999930000000 5808371.194000000100000000 50.539999999999999000 552244.816275187880000000 5808369.722466181000000000 50.539999999999992000 552244.819000000130000000 5808369.534000000000000000 50.539999999999992000 552247.236427880360000000 5808369.568057778300000000 50.539999999999999000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </cityObjectMember>

</CityModel>
