<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<CityModel xmlns="http://www.opengis.net/citygml/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:core="http://www.opengis.net/citygml/base/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:tex="http://www.opengis.net/citygml/textures/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/citygml/2.0 http://schemas.opengis.net/citygml/2.0/cityGMLBase.xsd http://www.opengis.net/citygml/appearance/2.0  http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd">

  <gml:description>Exported by RhinoCity(TM) software - (c)Rhinoterrain 2010-2018 : www.rhinoterrain.com</gml:description>

  <gml:name>RhinoCity(TM)</gml:name>

  <gml:boundedBy>
    <gml:Envelope srsDimension="3" srsName="crs:ETRS89_UTM32*DE_DHHN92_NH">
      <gml:lowerCorner>551996.812500 5801768.500000 54.649998</gml:lowerCorner>
      <gml:upperCorner>552033.625000 5801825.000000 63.350368</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>

  <cityObjectMember>
    <bldg:Building gml:id="DENIAL430001ls7C">
      <gen:doubleAttribute name="Volume">
        <gen:value>6645.379</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="TotalRoofArea">
        <gen:value>784.99</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="TotalWallArea">
        <gen:value>1350.96</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Authoring_Software">
        <gen:value>RhinoCity</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="OSKA">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ALKISID">
        <gen:value>DENIAL430001ls7C</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="KACHEL_ID">
        <gen:value>11_06_Building</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="COUNT">
        <gen:value>843</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="DESC">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCK">
        <gen:value>24</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCKNR">
        <gen:value>043024</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STATBEZNR">
        <gen:value>043</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="MBZNR">
        <gen:value>0417</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="WBZNR">
        <gen:value>0416</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNAM">
        <gen:value>Südstadt</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNR">
        <gen:value>04</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNAM">
        <gen:value>Südstadt-Bult</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNR">
        <gen:value>07</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEMEINDE">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SCALE">
        <gen:value>5000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_overlaps">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_xmax">
        <gen:value>552033.5700000003</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_xmin">
        <gen:value>551996.875</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_ymax">
        <gen:value>5801824.67</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_ymin">
        <gen:value>5801768.879000001</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_zmax">
        <gen:value>55.709176000021394</gen:value>
      </gen:stringAttribute>
      <gen:doubleAttribute name="ZMIN">
        <gen:value>54.650000</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="THICKNESS">
        <gen:value>0.100000</gen:value>
      </gen:doubleAttribute>
      <gen:intAttribute name="TEXTURE">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="BUILDINGID">
        <gen:value>DENIAL430001ls7C</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ARTBEZEICH">
        <gen:value>Gebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Join_Cou_1">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="FUNKTION_T">
        <gen:value>Wohngebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADT">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="OVERHANG">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="OBJFLAECHE">
        <gen:value>784.99</gen:value>
      </gen:stringAttribute>
      <bldg:measuredHeight uom="#m">9.700</bldg:measuredHeight>
      <bldg:lod2Solid>
        <gml:Solid gml:id="9e05d5e6-6378-4082-b076-fd6f5d816546">
          <gml:exterior>
            <gml:CompositeSurface gml:id="e287472d-882d-4f74-8b75-458d900236a5">
              <gml:surfaceMember xlink:href="#498a6860-82c4-47d6-bf92-3671d6a87c85"/>
              <gml:surfaceMember xlink:href="#f706e183-9e6b-4060-bc8c-9a584be183e7"/>
              <gml:surfaceMember xlink:href="#508a315b-dabe-4186-9c21-bf5bc01a7e3c"/>
              <gml:surfaceMember xlink:href="#a02c5491-5278-4546-8081-5575d60998b7"/>
              <gml:surfaceMember xlink:href="#16286586-1c51-487c-aa5b-f6f286152585"/>
              <gml:surfaceMember xlink:href="#24a4bd83-b10e-4dd3-b021-fe199f7bd1d7"/>
              <gml:surfaceMember xlink:href="#d892889f-8245-4642-a92a-bd2a65291b27"/>
              <gml:surfaceMember xlink:href="#1a294d76-7f27-45c5-8439-184e7ac74603"/>
              <gml:surfaceMember xlink:href="#3a6e05ad-648b-4a86-9a63-fdbb22d68ef9"/>
              <gml:surfaceMember xlink:href="#4e8aeca0-5924-44d1-9ea1-2052de634435"/>
              <gml:surfaceMember xlink:href="#6582b07f-1a45-445b-80db-33a03e034a8d"/>
              <gml:surfaceMember xlink:href="#1d544675-8143-4f81-8ce5-18d6fec4520d"/>
              <gml:surfaceMember xlink:href="#7fe29166-e885-461b-9f61-2a05dbbce13d"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="a311aa82-a673-4cc3-bfa0-2250bbc1d9f9">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001ls7C_C:28831</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>53.52</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="ff9674e6-a375-462e-9109-3df20a7fe3f0" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="498a6860-82c4-47d6-bf92-3671d6a87c85">
                  <gml:exterior>
                    <gml:LinearRing gml:id="cc4fb44b-6aa0-4be0-a30d-3eaa394df47f">
                      <gml:posList>552033.379999999890000000 5801778.455000000100000000 59.906620025634766000 552013.895999999720000000 5801805.848999999500000000 59.906620025634766000 552012.858110085360000000 5801805.141283946100000000 59.906620025634766000 552031.809999999590000000 5801777.337999999500000000 59.906620025634766000 552033.379999999890000000 5801778.455000000100000000 59.906620025634766000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="7ec1d36d-5e3a-4030-af83-591ef8fb33ab">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001ls7C_C:28832</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>731.47</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="bc2a93ce-988e-4245-a870-711625dc0773" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="f706e183-9e6b-4060-bc8c-9a584be183e7">
                  <gml:exterior>
                    <gml:LinearRing gml:id="78ab62f5-13f8-43fd-8065-e8b57fe5e09b">
                      <gml:posList>552013.895999999720000000 5801805.848999999500000000 63.350364685058594000 552022.025000000370000000 5801811.392000000900000000 63.350364685058594000 552024.873999999840000000 5801813.335000000900000000 63.350364685058594000 552024.964999999850000000 5801813.202999999700000000 63.350364685058594000 552027.610999999570000000 5801814.947000000600000000 63.350364685058594000 552021.018000000160000000 5801824.669999999900000000 63.350364685058594000 551996.875000000000000000 5801810.042999999600000000 63.350364685058594000 552024.912999999710000000 5801768.879000000700000000 63.350364685058594000 552033.570000000300000000 5801774.755999999100000000 63.350364685058594000 552031.809999999590000000 5801777.337999999500000000 63.350364685058594000 552012.858110085360000000 5801805.141283946100000000 63.350364685058594000 552013.895999999720000000 5801805.848999999500000000 63.350364685058594000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="47ded866-8d9f-4c58-8c3d-ed6e9ebb9eab">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>245.60</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>328.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="b494e0ca-0ad9-4531-960b-7d0dfa2bbe04" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="508a315b-dabe-4186-9c21-bf5bc01a7e3c">
                  <gml:exterior>
                    <gml:LinearRing gml:id="bb855639-a93e-47d1-b8b7-469ad4eeaa66">
                      <gml:posList>552021.018000000160000000 5801824.669999999900000000 54.650000000000006000 551996.875000000000000000 5801810.042999999600000000 54.650000000000006000 551996.875000000000000000 5801810.042999999600000000 63.350364685058594000 552021.018000000160000000 5801824.669999999900000000 63.350364685058594000 552021.018000000160000000 5801824.669999999900000000 54.650000000000006000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="f9cad6ca-5a4a-4e32-ab0a-ab8b624ba519">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>433.33</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>235.74</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="2fd96381-bf43-4537-a0a1-550de7c27826" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="a02c5491-5278-4546-8081-5575d60998b7">
                  <gml:exterior>
                    <gml:LinearRing gml:id="8e724f15-e61e-46bc-a991-95dc1535c094">
                      <gml:posList>552024.912999999710000000 5801768.879000000700000000 63.350364685058594000 551996.875000000000000000 5801810.042999999600000000 63.350364685058594000 551996.875000000000000000 5801810.042999999600000000 54.650000000000006000 552024.912999999710000000 5801768.879000000700000000 54.650000000000006000 552024.912999999710000000 5801768.879000000700000000 63.350364685058594000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="2b7f0c1f-6146-47b2-a717-48ea199fdaef">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>91.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>145.83</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="41becacd-8798-4a01-a8d7-45ff2c31ce3b" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="16286586-1c51-487c-aa5b-f6f286152585">
                  <gml:exterior>
                    <gml:LinearRing gml:id="7b11fb34-85eb-4fc2-98f3-db3199f348df">
                      <gml:posList>552033.570000000300000000 5801774.755999999100000000 63.350364685058594000 552024.912999999710000000 5801768.879000000700000000 63.350364685058594000 552024.912999999710000000 5801768.879000000700000000 54.650000000000006000 552033.570000000300000000 5801774.755999999100000000 54.650000000000006000 552033.570000000300000000 5801774.755999999100000000 63.350364685058594000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="5c258de3-3c4f-4aac-96cb-18aa79676e88">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>119.93</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>145.71</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="1b26a000-40c0-44ec-a8d5-5416709c25ed" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="24a4bd83-b10e-4dd3-b021-fe199f7bd1d7">
                  <gml:exterior>
                    <gml:LinearRing gml:id="543d3b01-0e6f-4c06-bec8-b3263e710fe6">
                      <gml:posList>552024.873999999840000000 5801813.335000000900000000 54.650000000000006000 552024.873999999840000000 5801813.335000000900000000 63.350364685058594000 552022.025000000370000000 5801811.392000000900000000 63.350364685058594000 552013.895999999720000000 5801805.848999999500000000 63.350364685058594000 552012.858110085360000000 5801805.141283946100000000 63.350364685058594000 552012.858110085360000000 5801805.141283946100000000 59.906620025634766000 552013.895999999720000000 5801805.848999999500000000 59.906620025634766000 552013.895999999720000000 5801805.848999999500000000 54.650000000000006000 552022.025000000370000000 5801811.392000000900000000 54.649999999999991000 552024.873999999840000000 5801813.335000000900000000 54.650000000000006000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="f50c232f-9f2b-436e-b7d8-b6ba2be1e1f1">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>1.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>235.42</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="6b6ca1c0-00e2-4cee-b8bd-8e4bcfb103da" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="d892889f-8245-4642-a92a-bd2a65291b27">
                  <gml:exterior>
                    <gml:LinearRing gml:id="49d45343-142a-4711-9f06-6dad1edfc23c">
                      <gml:posList>552024.964999999850000000 5801813.202999999700000000 63.350364685058594000 552024.873999999840000000 5801813.335000000900000000 63.350364685058594000 552024.873999999840000000 5801813.335000000900000000 54.650000000000006000 552024.964999999850000000 5801813.202999999700000000 54.650000000000006000 552024.964999999850000000 5801813.202999999700000000 63.350364685058594000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="73dc36e3-8b2c-4db9-b5d2-5c0afbbe2eb3">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>27.57</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>146.61</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="e20a2e4c-0f8e-4947-8463-50f417128998" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="1a294d76-7f27-45c5-8439-184e7ac74603">
                  <gml:exterior>
                    <gml:LinearRing gml:id="c9e29d7d-1e6b-4bdf-b18d-9df9a2981a6a">
                      <gml:posList>552027.610999999570000000 5801814.947000000600000000 63.350364685058594000 552024.964999999850000000 5801813.202999999700000000 63.350364685058594000 552024.964999999850000000 5801813.202999999700000000 54.650000000000006000 552027.610999999570000000 5801814.947000000600000000 54.650000000000006000 552027.610999999570000000 5801814.947000000600000000 63.350364685058594000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="46f42bce-43ac-4cee-ad6e-45a365b488cb">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>102.21</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>55.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="8ec610f6-2f09-4cb8-9de4-c25917ec9e8c" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="3a6e05ad-648b-4a86-9a63-fdbb22d68ef9">
                  <gml:exterior>
                    <gml:LinearRing gml:id="011f1c62-c1a0-439c-9294-2bbde45e8985">
                      <gml:posList>552027.610999999570000000 5801814.947000000600000000 54.650000000000006000 552021.018000000160000000 5801824.669999999900000000 54.650000000000006000 552021.018000000160000000 5801824.669999999900000000 63.350364685058594000 552027.610999999570000000 5801814.947000000600000000 63.350364685058594000 552027.610999999570000000 5801814.947000000600000000 54.650000000000006000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="e9237eb7-187e-40d5-bcc1-5f3311d1762c">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>143.06</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>55.72</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="dd839d60-ae9f-445a-95cc-502aae98ad46" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="4e8aeca0-5924-44d1-9ea1-2052de634435">
                  <gml:exterior>
                    <gml:LinearRing gml:id="ae9006de-325d-4364-b4c8-e7bcd3fe11d3">
                      <gml:posList>552033.570000000300000000 5801774.755999999100000000 54.650000000000006000 552031.809999999590000000 5801777.337999999500000000 54.650000000000006000 552031.809999999590000000 5801777.337999999500000000 59.906620025634766000 552012.858110085360000000 5801805.141283946100000000 59.906620025634766000 552012.858110085360000000 5801805.141283946100000000 63.350364685058594000 552031.809999999590000000 5801777.337999999500000000 63.350364685058594000 552033.570000000300000000 5801774.755999999100000000 63.350364685058594000 552033.570000000300000000 5801774.755999999100000000 54.650000000000006000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="dcf80ce7-aee6-40b6-9188-28eed340f72b">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>10.13</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>144.57</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="0c0fa4dc-2c16-4018-a8c6-1aed669cec4b" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="6582b07f-1a45-445b-80db-33a03e034a8d">
                  <gml:exterior>
                    <gml:LinearRing gml:id="83417056-5ab4-435c-b644-fb5d9e9a3445">
                      <gml:posList>552033.379999999890000000 5801778.455000000100000000 59.906620025634766000 552031.809999999590000000 5801777.337999999500000000 59.906620025634766000 552031.809999999590000000 5801777.337999999500000000 54.650000000000006000 552033.379999999890000000 5801778.455000000100000000 54.650000000000006000 552033.379999999890000000 5801778.455000000100000000 59.906620025634766000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="3dde43c3-3f98-45cb-a4b0-f87a1dc85609">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>176.71</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>54.58</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="757d4c46-db71-49e2-8992-32c9c176a5fe" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="1d544675-8143-4f81-8ce5-18d6fec4520d">
                  <gml:exterior>
                    <gml:LinearRing gml:id="3746da17-bd92-4d80-a432-f051a7bd261b">
                      <gml:posList>552033.379999999890000000 5801778.455000000100000000 54.650000000000006000 552013.895999999720000000 5801805.848999999500000000 54.650000000000006000 552013.895999999720000000 5801805.848999999500000000 59.906620025634766000 552033.379999999890000000 5801778.455000000100000000 59.906620025634766000 552033.379999999890000000 5801778.455000000100000000 54.650000000000006000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="97786810-bdd8-4b3e-8c43-2c939d7e0a26">
          <gen:stringAttribute name="Type">
            <gen:value>Ground</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>784.99</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="4a515869-b4d1-4f61-9517-546ea26c88eb" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="7fe29166-e885-461b-9f61-2a05dbbce13d">
                  <gml:exterior>
                    <gml:LinearRing gml:id="8e0ae01e-3944-45b4-999c-dfe9bc38d1d0">
                      <gml:posList>552013.895999999720000000 5801805.848999999500000000 54.650000000000006000 552033.379999999890000000 5801778.455000000100000000 54.650000000000006000 552031.809999999590000000 5801777.337999999500000000 54.650000000000006000 552033.570000000300000000 5801774.755999999100000000 54.650000000000006000 552024.912999999710000000 5801768.879000000700000000 54.650000000000006000 551996.875000000000000000 5801810.042999999600000000 54.650000000000006000 552021.018000000160000000 5801824.669999999900000000 54.650000000000006000 552027.610999999570000000 5801814.947000000600000000 54.650000000000006000 552024.964999999850000000 5801813.202999999700000000 54.650000000000006000 552024.873999999840000000 5801813.335000000900000000 54.650000000000006000 552022.025000000370000000 5801811.392000000900000000 54.649999999999991000 552013.895999999720000000 5801805.848999999500000000 54.650000000000006000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </cityObjectMember>

</CityModel>
