<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<CityModel xmlns="http://www.opengis.net/citygml/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:core="http://www.opengis.net/citygml/base/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:tex="http://www.opengis.net/citygml/textures/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/citygml/2.0 http://schemas.opengis.net/citygml/2.0/cityGMLBase.xsd http://www.opengis.net/citygml/appearance/2.0  http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd">

  <gml:description>Exported by RhinoCity(TM) software - (c)Rhinoterrain 2010-2018 : www.rhinoterrain.com</gml:description>

  <gml:name>RhinoCity(TM)</gml:name>

  <gml:boundedBy>
    <gml:Envelope srsDimension="3" srsName="crs:ETRS89_UTM32*DE_DHHN92_NH">
      <gml:lowerCorner>552165.375000 5805804.500000 52.849998</gml:lowerCorner>
      <gml:upperCorner>552227.437500 5805849.000000 79.874008</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>

  <cityObjectMember>
    <bldg:Building gml:id="DENIAL430001dq8N">
      <gen:doubleAttribute name="Volume">
        <gen:value>19576.632</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="TotalRoofArea">
        <gen:value>867.40</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="TotalWallArea">
        <gen:value>4166.59</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Authoring_Software">
        <gen:value>RhinoCity</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="OSKA">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ALKISID">
        <gen:value>DENIAL430001dq8N</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="KACHEL_ID">
        <gen:value>11_10_Building</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="COUNT">
        <gen:value>850</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="DESC">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCK">
        <gen:value>45</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCKNR">
        <gen:value>103045</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STATBEZNR">
        <gen:value>103</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="MBZNR">
        <gen:value>1022</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="WBZNR">
        <gen:value>1022</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNAM">
        <gen:value>List</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNR">
        <gen:value>10</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNAM">
        <gen:value>Vahrenwald-List</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNR">
        <gen:value>02</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEMEINDE">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SCALE">
        <gen:value>5000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_overlaps">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_xmax">
        <gen:value>552227.3949999996</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_xmin">
        <gen:value>552165.4019999998</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_ymax">
        <gen:value>5805848.7190000005</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_ymin">
        <gen:value>5805804.960000001</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="_zmax">
        <gen:value>53.50074200000242</gen:value>
      </gen:stringAttribute>
      <gen:doubleAttribute name="ZMIN">
        <gen:value>52.850000</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="THICKNESS">
        <gen:value>0.100000</gen:value>
      </gen:doubleAttribute>
      <gen:intAttribute name="TEXTURE">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="BUILDINGID">
        <gen:value>DENIAL430001dq8N</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ARTBEZEICH">
        <gen:value>Gebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Join_Cou_1">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="FUNKTION_T">
        <gen:value>Wohngebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADT">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="OVERHANG">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="OBJFLAECHE">
        <gen:value>867.4</gen:value>
      </gen:stringAttribute>
      <bldg:measuredHeight uom="#m">28.024</bldg:measuredHeight>
      <bldg:lod2Solid>
        <gml:Solid gml:id="2d98da34-e289-42c1-a723-82a4ebee0d0f">
          <gml:exterior>
            <gml:CompositeSurface gml:id="843dd28d-1830-4ffa-be96-2810daf9b67d">
              <gml:surfaceMember xlink:href="#d0bb2d9a-d8ed-44b3-9d25-4790ad92f0d3"/>
              <gml:surfaceMember xlink:href="#6b41f37d-0686-4dda-96fc-951c8f362e1f"/>
              <gml:surfaceMember xlink:href="#ff494e5f-62e6-4353-ab3f-3eef91867ee8"/>
              <gml:surfaceMember xlink:href="#576bd863-5185-46dc-9085-72e7bb6bf306"/>
              <gml:surfaceMember xlink:href="#f4fedaf5-f29c-45e8-bea8-7540b427665f"/>
              <gml:surfaceMember xlink:href="#7ac26882-5d0d-4a14-88d8-f377eacf101f"/>
              <gml:surfaceMember xlink:href="#7bff9a41-b2b0-49c9-9d61-413495b29dea"/>
              <gml:surfaceMember xlink:href="#d50b1c44-c24a-464a-a21e-31096a3f0e8f"/>
              <gml:surfaceMember xlink:href="#abb159f1-d26d-4460-8dee-1f516b7dd444"/>
              <gml:surfaceMember xlink:href="#7f6e9c0e-0ed1-4925-8406-47ebab562304"/>
              <gml:surfaceMember xlink:href="#f926b498-63ac-4df3-b948-37e4985bd913"/>
              <gml:surfaceMember xlink:href="#791e1273-fdd3-4573-809f-2275b1d10e0a"/>
              <gml:surfaceMember xlink:href="#c4acbb91-3128-4999-80ec-73283e61cc0e"/>
              <gml:surfaceMember xlink:href="#1e1ef689-531b-4fb9-8aab-cb7f96aacdce"/>
              <gml:surfaceMember xlink:href="#54d5dbf0-6dc4-4699-b970-19c469b954c7"/>
              <gml:surfaceMember xlink:href="#da2b56d0-92ec-4c51-bb10-0c5f00655488"/>
              <gml:surfaceMember xlink:href="#5b203c91-1e2f-4a3d-9293-c6d1808a3051"/>
              <gml:surfaceMember xlink:href="#44c8d838-9a99-4480-a550-4a9742306137"/>
              <gml:surfaceMember xlink:href="#a59d2b73-7245-4d89-b99b-97dcdea8840c"/>
              <gml:surfaceMember xlink:href="#c4d6d0f4-ac19-45e1-8036-74c1eeed3877"/>
              <gml:surfaceMember xlink:href="#1fc80305-a512-4938-a62a-63ca94bf4a13"/>
              <gml:surfaceMember xlink:href="#682a1955-2f6c-4935-ada8-4a30f60b13ff"/>
              <gml:surfaceMember xlink:href="#6f815d89-17be-49a3-b80d-b8526f07485c"/>
              <gml:surfaceMember xlink:href="#3be8c4c3-d62b-47a8-aa96-33d5af33a77f"/>
              <gml:surfaceMember xlink:href="#c7c4fd8b-842e-4a8c-af0c-9c3d10125f87"/>
              <gml:surfaceMember xlink:href="#34391ac4-5472-4fda-8000-18e9bb77551a"/>
              <gml:surfaceMember xlink:href="#032553d0-6023-4461-ae9f-06de4c2741fd"/>
              <gml:surfaceMember xlink:href="#40114cbc-dd84-49ed-85f8-0621e9e993cf"/>
              <gml:surfaceMember xlink:href="#ce5f1001-c2f9-4252-9d1a-64ff3cd26090"/>
              <gml:surfaceMember xlink:href="#a270d94b-7205-4bc5-b2a9-9d40b59a2ab8"/>
              <gml:surfaceMember xlink:href="#60b99d29-9792-4edd-b9de-7d7f10cc1a4d"/>
              <gml:surfaceMember xlink:href="#8561cda0-9121-4ed5-b32c-d5b644b208d1"/>
              <gml:surfaceMember xlink:href="#b004d859-b5f6-4da4-91f2-0501494a6a6c"/>
              <gml:surfaceMember xlink:href="#4d035875-e5bd-4550-991e-8d32fc66b416"/>
              <gml:surfaceMember xlink:href="#c819ee56-e6fa-48bc-a55f-f0950b27b3c3"/>
              <gml:surfaceMember xlink:href="#e97d8808-e1f8-436a-93e7-2853ecb20646"/>
              <gml:surfaceMember xlink:href="#9f6c9f39-662b-4462-8153-317252e16d18"/>
              <gml:surfaceMember xlink:href="#ef6b93c5-fbf7-4aaa-b6fe-cd38da204f06"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="7b465bba-36b8-4ebd-9f10-6e9d2b01db80">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001dq8N_C:29300</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>12.75</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="bb508928-8fb2-4432-a4b4-bcd0c978ba9c" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="d0bb2d9a-d8ed-44b3-9d25-4790ad92f0d3">
                  <gml:exterior>
                    <gml:LinearRing gml:id="bbedb9cc-bf9d-4982-b7b3-a8d93cc760be">
                      <gml:posList>552174.568060114980000000 5805812.558153901200000000 73.621147155761719000 552172.186204605740000000 5805811.460996056000000000 73.621147155761719000 552174.221048588860000000 5805807.043487332800000000 73.621147155761719000 552176.602904098110000000 5805808.140645178000000000 73.621147155761719000 552174.568060114980000000 5805812.558153901200000000 73.621147155761719000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="cc88132b-b248-4541-8ddd-3cd782acabc6">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001dq8N_C:29301</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>48.82</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="7936ec53-516c-493f-bf44-13d30aaabfc4" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="6b41f37d-0686-4dda-96fc-951c8f362e1f">
                  <gml:exterior>
                    <gml:LinearRing gml:id="c257c8bb-4f68-4a8f-8e02-6751ce86a7f8">
                      <gml:posList>552176.602904098110000000 5805808.140645178000000000 70.571426391601562000 552174.221048588860000000 5805807.043487332800000000 70.571426391601562000 552172.186204605740000000 5805811.460996056000000000 70.571426391601562000 552174.568060114980000000 5805812.558153901200000000 70.571426391601562000 552172.079072041900000000 5805817.961578741700000000 70.571426391601562000 552168.227161245770000000 5805816.188609021700000000 70.571426391601562000 552170.575000000190000000 5805811.084000000700000000 70.571426391601562000 552168.303999999540000000 5805810.036000000300000000 70.571426391601562000 552170.638000000270000000 5805804.960000000900000000 70.571426391601562000 552172.913999999870000000 5805806.000000000000000000 70.571426391601562000 552176.770619990540000000 5805807.776545314100000000 70.571426391601562000 552176.602904098110000000 5805808.140645178000000000 70.571426391601562000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="3f326f55-e8d3-4dd9-a2d3-bda9cf2066fc">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001dq8N_C:29304</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>145.10</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="3d06034b-cc43-4e23-a326-87a0951881dc" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="ff494e5f-62e6-4353-ab3f-3eef91867ee8">
                  <gml:exterior>
                    <gml:LinearRing gml:id="1b5327a3-ac65-4e84-a5f6-b1d101f932fe">
                      <gml:posList>552184.886029962450000000 5805831.300786932000000000 79.452941894531250000 552184.275999999610000000 5805831.019999999600000000 79.452941894531250000 552165.401999999770000000 5805822.331000000200000000 79.452941894531250000 552168.227161245770000000 5805816.188609021700000000 79.452941894531250000 552172.079072041900000000 5805817.961578741700000000 79.452941894531250000 552187.730353421070000000 5805825.165600181600000000 79.452941894531250000 552184.886029962450000000 5805831.300786932000000000 79.452941894531250000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="5ca452fc-b31c-4e39-8c30-f34cfe31118f">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001dq8N_C:29305</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>193.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="429a5841-0369-4342-8d05-dffbf368832c" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="576bd863-5185-46dc-9085-72e7bb6bf306">
                  <gml:exterior>
                    <gml:LinearRing gml:id="425cacd7-b516-4855-8887-7e868b97cb02">
                      <gml:posList>552190.656000000420000000 5805818.855000000400000000 76.087593078613281000 552189.605000000450000000 5805821.121999999500000000 76.087593078613281000 552187.730353421070000000 5805825.165600181600000000 76.087593078613281000 552172.079072041900000000 5805817.961578741700000000 76.087593078613281000 552174.568060114980000000 5805812.558153901200000000 76.087593078613281000 552176.602904098110000000 5805808.140645178000000000 76.087593078613281000 552176.770619990540000000 5805807.776545314100000000 76.087593078613281000 552192.442999999970000000 5805814.995999999300000000 76.087593078613281000 552190.656000000420000000 5805818.855000000400000000 76.087593078613281000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="2e82f620-9c7d-4e63-91c7-50fc3c8d659c">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001dq8N_C:29302</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>110.33</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="95af8fb9-7a44-4cf9-b515-ad3a4e08c3e1" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="f4fedaf5-f29c-45e8-bea8-7540b427665f">
                  <gml:exterior>
                    <gml:LinearRing gml:id="532b273f-3019-408d-a1db-e1a1e8b39d68">
                      <gml:posList>552225.563000000080000000 5805837.649000000200000000 76.928344726562500000 552227.394999999550000000 5805838.507999999400000000 76.928344726562500000 552222.697999999860000000 5805848.719000000500000000 76.928344726562500000 552220.770999999720000000 5805847.820000000300000000 76.928344726562500000 552223.609475215310000000 5805841.654125603800000000 76.928344726562500000 552205.880849948620000000 5805833.436324728700000000 76.928344726562500000 552207.715074743030000000 5805829.445637458000000000 76.928344726562500000 552208.311999999920000000 5805829.720000000700000000 76.928344726562500000 552212.580000000070000000 5805831.680999999900000000 76.928344726562500000 552221.025000000370000000 5805835.562999999200000000 76.928344726562500000 552225.292999999600000000 5805837.525000000400000000 76.928344726562500000 552225.475999999790000000 5805837.607999999100000000 76.928344726562500000 552225.563000000080000000 5805837.649000000200000000 76.928344726562500000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="0d24bb39-a735-4fcc-85cf-fe6fa9785ce5">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001dq8N_C:29303</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>133.13</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="cc754405-2d06-409e-a153-c9430bf8136c" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="7ac26882-5d0d-4a14-88d8-f377eacf101f">
                  <gml:exterior>
                    <gml:LinearRing gml:id="4c9d3391-f798-4cf5-ae3f-dd819345785d">
                      <gml:posList>552219.862999999900000000 5805847.402000000700000000 79.873999999999995000 552215.141999999990000000 5805845.229000000300000000 79.873999999999995000 552203.024520526760000000 5805839.650785185400000000 79.873999999999995000 552205.880849948620000000 5805833.436324728700000000 79.874000000000009000 552223.609475215310000000 5805841.654125603800000000 79.873999999999995000 552220.770999999720000000 5805847.820000000300000000 79.873999999999995000 552219.862999999900000000 5805847.402000000700000000 79.873999999999995000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="a5f788b4-9521-402f-bb31-606cf605fe60">
          <gen:stringAttribute name="Index">
            <gen:value>MR_P:DENIAL430001dq8N_C:29306</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="Type">
            <gen:value>MainRoof</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>223.94</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>0.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="22d8781c-9277-47d8-b24c-10771a83a262" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="7bff9a41-b2b0-49c9-9d61-413495b29dea">
                  <gml:exterior>
                    <gml:LinearRing gml:id="36ef02e6-6921-4da4-9399-8940ab39ff4c">
                      <gml:posList>552205.880849948620000000 5805833.436324728700000000 69.996627807617188000 552203.024520526760000000 5805839.650785185400000000 69.996627807617188000 552195.169999999930000000 5805836.035000000100000000 69.996627807617188000 552190.449000000020000000 5805833.860999999600000000 69.996627807617188000 552188.997000000440000000 5805833.193000000000000000 69.996627807617188000 552184.886029962450000000 5805831.300786932000000000 69.996627807617188000 552187.730353421070000000 5805825.165600181600000000 69.996627807617188000 552189.605000000450000000 5805821.121999999500000000 69.996627807617188000 552193.236999999730000000 5805822.790999999300000000 69.996627807617188000 552195.598000000230000000 5805823.877000000300000000 69.996627807617188000 552202.317999999970000000 5805826.964999999900000000 69.996627807617188000 552207.715074743030000000 5805829.445637458000000000 69.996627807617188000 552205.880849948620000000 5805833.436324728700000000 69.996627807617188000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="7f0a3555-3540-4d3e-8d81-3b48623ffd0b">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>179.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>245.30</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="023f273f-8737-4636-94f0-9f9bce3826cb" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="d50b1c44-c24a-464a-a21e-31096a3f0e8f">
                  <gml:exterior>
                    <gml:LinearRing gml:id="026380ce-32a9-407a-b539-50ffe4163fbf">
                      <gml:posList>552168.227161245770000000 5805816.188609021700000000 70.571426391601562000 552168.227161245770000000 5805816.188609021700000000 79.452941894531250000 552165.401999999770000000 5805822.331000000200000000 79.452941894531250000 552165.401999999770000000 5805822.331000000200000000 52.850000000000001000 552168.227161245770000000 5805816.188609021700000000 52.850000000000001000 552168.227161245770000000 5805816.188609021700000000 70.571426391601562000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="b3afb4f3-0bd5-450d-98d3-6f13d3f3b0f2">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>570.62</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>335.28</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="80a7e33e-ae68-4f9a-b753-ffb24be9c6f1" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="abb159f1-d26d-4460-8dee-1f516b7dd444">
                  <gml:exterior>
                    <gml:LinearRing gml:id="663377aa-526b-4361-a8ca-1835b7a29a2b">
                      <gml:posList>552184.886029962450000000 5805831.300786932000000000 69.996627807617188000 552184.886029962450000000 5805831.300786932000000000 52.850000000000001000 552184.275999999610000000 5805831.019999999600000000 52.850000000000001000 552165.401999999770000000 5805822.331000000200000000 52.850000000000001000 552165.401999999770000000 5805822.331000000200000000 79.452941894531250000 552184.275999999610000000 5805831.019999999600000000 79.452941894531250000 552184.886029962450000000 5805831.300786932000000000 79.452941894531250000 552184.886029962450000000 5805831.300786932000000000 69.996627807617188000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bc445f8e-21a7-4ab5-86c1-cc84da33318c">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>95.64</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>155.28</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="fbb6f9ce-bcef-4f7f-b47b-baaf42f1617d" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="7f6e9c0e-0ed1-4925-8406-47ebab562304">
                  <gml:exterior>
                    <gml:LinearRing gml:id="fe85a56d-cf68-4629-97af-01249d2136ec">
                      <gml:posList>552187.730353421070000000 5805825.165600181600000000 76.087593078613281000 552187.730353421070000000 5805825.165600181600000000 79.452941894531250000 552172.079072041900000000 5805817.961578741700000000 79.452941894531250000 552168.227161245770000000 5805816.188609021700000000 79.452941894531250000 552168.227161245770000000 5805816.188609021700000000 70.571426391601562000 552172.079072041900000000 5805817.961578741700000000 70.571426391601562000 552172.079072041900000000 5805817.961578741700000000 76.087593078613281000 552187.730353421070000000 5805825.165600181600000000 76.087593078613281000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="a97bffe6-19fe-4e8d-8609-fa529508f4d8">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>63.95</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>65.13</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="ffaf7cf8-6863-4b8c-a3c1-5ef99ffd1a65" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="f926b498-63ac-4df3-b948-37e4985bd913">
                  <gml:exterior>
                    <gml:LinearRing gml:id="45aee786-58cd-4c37-9c62-8558c72a8e00">
                      <gml:posList>552187.730353421070000000 5805825.165600181600000000 76.087593078613281000 552187.730353421070000000 5805825.165600181600000000 69.996627807617188000 552184.886029962450000000 5805831.300786932000000000 69.996627807617188000 552184.886029962450000000 5805831.300786932000000000 79.452941894531250000 552187.730353421070000000 5805825.165600181600000000 79.452941894531250000 552187.730353421070000000 5805825.165600181600000000 76.087593078613281000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="35a0487f-86fa-4105-826b-f83e513e5fa1">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>98.82</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>65.15</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="8cd92138-edab-43c3-be0e-96b5964597c4" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="791e1273-fdd3-4573-809f-2275b1d10e0a">
                  <gml:exterior>
                    <gml:LinearRing gml:id="7453e1dd-5954-44c6-9af3-df6447acf9bf">
                      <gml:posList>552192.442999999970000000 5805814.995999999300000000 52.850000000000001000 552190.656000000420000000 5805818.855000000400000000 52.850000000000001000 552190.656000000420000000 5805818.855000000400000000 76.087593078613281000 552192.442999999970000000 5805814.995999999300000000 76.087593078613281000 552192.442999999970000000 5805814.995999999300000000 52.850000000000001000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="eb0ea501-d6fe-4497-863a-54810c12e358">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>400.97</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>155.27</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="e567a21f-c3fb-4bbf-845e-fc171f48201d" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="c4acbb91-3128-4999-80ec-73283e61cc0e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="af7e8984-3708-40c5-ae16-bf7b99a88caf">
                      <gml:posList>552192.442999999970000000 5805814.995999999300000000 76.087593078613281000 552176.770619990540000000 5805807.776545314100000000 76.087593078613281000 552176.770619990540000000 5805807.776545314100000000 70.571426391601562000 552176.770619990540000000 5805807.776545314100000000 52.849999999999994000 552192.442999999970000000 5805814.995999999300000000 52.850000000000001000 552192.442999999970000000 5805814.995999999300000000 76.087593078613281000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="7bb65310-e675-422c-92c7-c71157a157c4">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>47.02</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>245.27</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="2a4a4bb5-5e04-424f-887a-45c0992f0073" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="1e1ef689-531b-4fb9-8aab-cb7f96aacdce">
                  <gml:exterior>
                    <gml:LinearRing gml:id="b6628aaf-82fb-4f9d-8bdd-52c04d07c115">
                      <gml:posList>552176.770619990540000000 5805807.776545314100000000 70.571426391601562000 552176.770619990540000000 5805807.776545314100000000 76.087593078613281000 552176.602904098110000000 5805808.140645178000000000 76.087593078613281000 552174.568060114980000000 5805812.558153901200000000 76.087593078613281000 552172.079072041900000000 5805817.961578741700000000 76.087593078613281000 552172.079072041900000000 5805817.961578741700000000 70.571426391601562000 552174.568060114980000000 5805812.558153901200000000 70.571426391601562000 552174.568060114980000000 5805812.558153901200000000 73.621147155761719000 552176.602904098110000000 5805808.140645178000000000 73.621147155761719000 552176.602904098110000000 5805808.140645178000000000 70.571426391601562000 552176.770619990540000000 5805807.776545314100000000 70.571426391601562000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="f75da612-26b7-4e9c-ac51-72b60f3adf29">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>85.21</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>65.13</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="5c5b399c-b4a6-4733-b200-d8b0eb13a275" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="54d5dbf0-6dc4-4699-b970-19c469b954c7">
                  <gml:exterior>
                    <gml:LinearRing gml:id="8afe5433-88c6-47ab-87f3-519de6ba222b">
                      <gml:posList>552190.656000000420000000 5805818.855000000400000000 52.850000000000001000 552189.605000000450000000 5805821.121999999500000000 52.850000000000001000 552189.605000000450000000 5805821.121999999500000000 69.996627807617188000 552187.730353421070000000 5805825.165600181600000000 69.996627807617188000 552187.730353421070000000 5805825.165600181600000000 76.087593078613281000 552189.605000000450000000 5805821.121999999500000000 76.087593078613281000 552190.656000000420000000 5805818.855000000400000000 76.087593078613281000 552190.656000000420000000 5805818.855000000400000000 52.850000000000001000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="7678033d-07d3-48d6-adf2-2a879473ab67">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>8.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>335.27</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="84a5fa4a-c19a-44cc-89b4-869a0db8c363" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="da2b56d0-92ec-4c51-bb10-0c5f00655488">
                  <gml:exterior>
                    <gml:LinearRing gml:id="c81ad165-12f5-4f60-afdf-8ce2edb8ae21">
                      <gml:posList>552174.568060114980000000 5805812.558153901200000000 70.571426391601562000 552172.186204605740000000 5805811.460996056000000000 70.571426391601562000 552172.186204605740000000 5805811.460996056000000000 73.621147155761719000 552174.568060114980000000 5805812.558153901200000000 73.621147155761719000 552174.568060114980000000 5805812.558153901200000000 70.571426391601562000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="174083e5-9ffe-4765-bb12-969de3998d53">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>14.83</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>245.27</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="96ca67ca-5fda-4b02-99d7-3ff98ff785b8" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="5b203c91-1e2f-4a3d-9293-c6d1808a3051">
                  <gml:exterior>
                    <gml:LinearRing gml:id="6901fe70-95d4-48a5-8203-940d6e69fae8">
                      <gml:posList>552174.221048588860000000 5805807.043487332800000000 73.621147155761719000 552172.186204605740000000 5805811.460996056000000000 73.621147155761719000 552172.186204605740000000 5805811.460996056000000000 70.571426391601562000 552174.221048588860000000 5805807.043487332800000000 70.571426391601562000 552174.221048588860000000 5805807.043487332800000000 73.621147155761719000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="c9eb5208-ec90-4764-bbc9-a07ae81f17d7">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>8.00</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>155.27</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="e42a5cbc-822b-4307-b43e-8cfbabbb6307" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="44c8d838-9a99-4480-a550-4a9742306137">
                  <gml:exterior>
                    <gml:LinearRing gml:id="21f3177a-9719-4e3d-b1b9-f58e145cf509">
                      <gml:posList>552176.602904098110000000 5805808.140645178000000000 73.621147155761719000 552174.221048588860000000 5805807.043487332800000000 73.621147155761719000 552174.221048588860000000 5805807.043487332800000000 70.571426391601562000 552176.602904098110000000 5805808.140645178000000000 70.571426391601562000 552176.602904098110000000 5805808.140645178000000000 73.621147155761719000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="7655c89a-33b0-4650-9249-5d8840b093b2">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>44.35</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>155.44</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="723ca0bc-8bef-45d6-96b7-4cef4479448e" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="a59d2b73-7245-4d89-b99b-97dcdea8840c">
                  <gml:exterior>
                    <gml:LinearRing gml:id="17f8ec5e-47f4-4cfe-a224-e7bfe8fe2fe9">
                      <gml:posList>552172.913999999870000000 5805806.000000000000000000 70.571426391601562000 552170.638000000270000000 5805804.960000000900000000 70.571426391601562000 552170.638000000270000000 5805804.960000000900000000 52.850000000000001000 552172.913999999870000000 5805806.000000000000000000 52.850000000000001000 552172.913999999870000000 5805806.000000000000000000 70.571426391601562000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="c2aa5e52-9849-4b99-8a27-afea4612f5d0">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>75.25</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>155.27</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="0af4c741-c806-47ec-baec-e9e1715b9ae5" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="c4d6d0f4-ac19-45e1-8036-74c1eeed3877">
                  <gml:exterior>
                    <gml:LinearRing gml:id="4540142b-c81d-4086-8774-d034ccb58c0c">
                      <gml:posList>552176.770619990540000000 5805807.776545314100000000 70.571426391601562000 552172.913999999870000000 5805806.000000000000000000 70.571426391601562000 552172.913999999870000000 5805806.000000000000000000 52.850000000000001000 552176.770619990540000000 5805807.776545314100000000 52.849999999999994000 552176.770619990540000000 5805807.776545314100000000 70.571426391601562000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="6bb09112-283f-4ed5-9ffb-30a7e1bf399d">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>99.57</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>245.30</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="39b2cb8d-5f69-4898-90c2-588e83617e9b" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="1fc80305-a512-4938-a62a-63ca94bf4a13">
                  <gml:exterior>
                    <gml:LinearRing gml:id="d0b9bbfd-f573-4a8a-8b6e-94a362770862">
                      <gml:posList>552170.575000000190000000 5805811.084000000700000000 70.571426391601562000 552168.227161245770000000 5805816.188609021700000000 70.571426391601562000 552168.227161245770000000 5805816.188609021700000000 52.850000000000001000 552170.575000000190000000 5805811.084000000700000000 52.850000000000001000 552170.575000000190000000 5805811.084000000700000000 70.571426391601562000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="76037089-aa41-40e5-bf19-8a32332feb4b">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>44.32</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>335.23</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="1e2e2d7a-b48b-429d-8897-002bfa14e19b" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="682a1955-2f6c-4935-ada8-4a30f60b13ff">
                  <gml:exterior>
                    <gml:LinearRing gml:id="b1d37ced-e475-464f-b96f-d4428992169e">
                      <gml:posList>552170.575000000190000000 5805811.084000000700000000 52.850000000000001000 552168.303999999540000000 5805810.036000000300000000 52.850000000000001000 552168.303999999540000000 5805810.036000000300000000 70.571426391601562000 552170.575000000190000000 5805811.084000000700000000 70.571426391601562000 552170.575000000190000000 5805811.084000000700000000 52.850000000000001000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="a10a1c64-dcb6-4a13-8090-030e85051a51">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>99.01</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>245.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="a833ba6d-18ba-4eaf-9eb9-7a0aecd708a4" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="6f815d89-17be-49a3-b80d-b8526f07485c">
                  <gml:exterior>
                    <gml:LinearRing gml:id="8af6e2f3-b923-499e-acdc-8c960270eb2b">
                      <gml:posList>552170.638000000270000000 5805804.960000000900000000 70.571426391601562000 552168.303999999540000000 5805810.036000000300000000 70.571426391601562000 552168.303999999540000000 5805810.036000000300000000 52.850000000000001000 552170.638000000270000000 5805804.960000000900000000 52.850000000000001000 552170.638000000270000000 5805804.960000000900000000 70.571426391601562000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="01582663-9c0c-4a91-87dd-abcd808f8f24">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>270.63</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>65.30</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="8d2907e7-8fce-4fc5-8138-80ff4c7c5cd6" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="3be8c4c3-d62b-47a8-aa96-33d5af33a77f">
                  <gml:exterior>
                    <gml:LinearRing gml:id="33710da5-66e7-4670-96ef-2dd00cd87e70">
                      <gml:posList>552227.394999999550000000 5805838.507999999400000000 52.850000000000001000 552222.697999999860000000 5805848.719000000500000000 52.850000000000001000 552222.697999999860000000 5805848.719000000500000000 76.928344726562500000 552227.394999999550000000 5805838.507999999400000000 76.928344726562500000 552227.394999999550000000 5805838.507999999400000000 52.850000000000001000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="e2468f0f-c27c-406f-a482-cb453b01fda0">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>51.20</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>334.99</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="be787ca0-947c-4530-a21d-e0217b25e256" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="c7c4fd8b-842e-4a8c-af0c-9c3d10125f87">
                  <gml:exterior>
                    <gml:LinearRing gml:id="bdbbc4bf-17f7-4c3b-accb-684ca2aa58c7">
                      <gml:posList>552222.697999999860000000 5805848.719000000500000000 52.850000000000001000 552220.770999999720000000 5805847.820000000300000000 52.849999999999994000 552220.770999999720000000 5805847.820000000300000000 76.928344726562500000 552222.697999999860000000 5805848.719000000500000000 76.928344726562500000 552222.697999999860000000 5805848.719000000500000000 52.850000000000001000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="e8888693-be94-499f-98b7-6a52f9995bbe">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>465.81</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>155.32</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="52225386-0a6b-4daa-aa4f-38cc5a4b87a5" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="34391ac4-5472-4fda-8000-18e9bb77551a">
                  <gml:exterior>
                    <gml:LinearRing gml:id="763d6f76-6c4e-4f35-a104-af894091a0b0">
                      <gml:posList>552225.292999999600000000 5805837.525000000400000000 76.928344726562500000 552221.025000000370000000 5805835.562999999200000000 76.928344726562500000 552212.580000000070000000 5805831.680999999900000000 76.928344726562500000 552208.311999999920000000 5805829.720000000700000000 76.928344726562500000 552207.715074743030000000 5805829.445637458000000000 76.928344726562500000 552207.715074743030000000 5805829.445637458000000000 69.996627807617188000 552207.715074743030000000 5805829.445637458000000000 52.850000000000001000 552208.311999999920000000 5805829.720000000700000000 52.850000000000001000 552212.580000000070000000 5805831.680999999900000000 52.850000000000001000 552221.025000000370000000 5805835.562999999200000000 52.850000000000001000 552225.292999999600000000 5805837.525000000400000000 52.850000000000001000 552225.292999999600000000 5805837.525000000400000000 76.928344726562500000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="344d6ec5-bdec-4d66-ab6d-09f3b97337f5">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>4.84</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>155.60</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="f93ae775-23bc-4acc-b9d7-b9e42483549d" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="032553d0-6023-4461-ae9f-06de4c2741fd">
                  <gml:exterior>
                    <gml:LinearRing gml:id="e4ab6d29-11e5-46f6-9f20-044d6c63743f">
                      <gml:posList>552225.475999999790000000 5805837.607999999100000000 76.928344726562500000 552225.292999999600000000 5805837.525000000400000000 76.928344726562500000 552225.292999999600000000 5805837.525000000400000000 52.850000000000001000 552225.475999999790000000 5805837.607999999100000000 52.850000000000001000 552225.475999999790000000 5805837.607999999100000000 76.928344726562500000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="89d50190-289c-4f3e-9931-8e0affd78a4f">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>2.32</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>154.77</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="e8197704-b892-4efc-86db-ee51fffe2fa4" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="40114cbc-dd84-49ed-85f8-0621e9e993cf">
                  <gml:exterior>
                    <gml:LinearRing gml:id="58fc9f30-f3ff-47ae-b2f6-e3e386bba46a">
                      <gml:posList>552225.563000000080000000 5805837.649000000200000000 76.928344726562500000 552225.475999999790000000 5805837.607999999100000000 76.928344726562500000 552225.475999999790000000 5805837.607999999100000000 52.850000000000001000 552225.563000000080000000 5805837.649000000200000000 52.850000000000001000 552225.563000000080000000 5805837.649000000200000000 76.928344726562500000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="5b6470ad-d090-44c8-8e8c-12a6d2e8d071">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>48.72</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>154.88</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="f0fe3afe-87a2-4831-87a5-2cf57a1fdbee" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="ce5f1001-c2f9-4252-9d1a-64ff3cd26090">
                  <gml:exterior>
                    <gml:LinearRing gml:id="906bf172-971c-4606-824c-1db17aaf3566">
                      <gml:posList>552227.394999999550000000 5805838.507999999400000000 76.928344726562500000 552225.563000000080000000 5805837.649000000200000000 76.928344726562500000 552225.563000000080000000 5805837.649000000200000000 52.850000000000001000 552227.394999999550000000 5805838.507999999400000000 52.850000000000001000 552227.394999999550000000 5805838.507999999400000000 76.928344726562500000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="296c6005-7741-4951-9055-2b6bf7892fac">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>30.44</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>245.32</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="e7afed6a-d5f7-42ce-895c-8a5f55850adf" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="a270d94b-7205-4bc5-b2a9-9d40b59a2ab8">
                  <gml:exterior>
                    <gml:LinearRing gml:id="b2921c07-f14e-4ffe-af9a-43acb409de47">
                      <gml:posList>552207.715074743030000000 5805829.445637458000000000 76.928344726562500000 552205.880849948620000000 5805833.436324728700000000 76.928344726562500000 552205.880849948620000000 5805833.436324728700000000 69.996627807617188000 552207.715074743030000000 5805829.445637458000000000 69.996627807617188000 552207.715074743030000000 5805829.445637458000000000 76.928344726562500000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="76f3e46e-465c-4f08-b281-f182014e048a">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>57.56</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>155.13</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="61083e67-cc9a-4c67-8f81-105661adce48" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="60b99d29-9792-4edd-b9de-7d7f10cc1a4d">
                  <gml:exterior>
                    <gml:LinearRing gml:id="a79940c0-a9b8-4e6a-ae35-25a3c0760452">
                      <gml:posList>552223.609475215310000000 5805841.654125603800000000 79.873999999999995000 552205.880849948620000000 5805833.436324728700000000 79.874000000000009000 552205.880849948620000000 5805833.436324728700000000 76.928344726562500000 552223.609475215310000000 5805841.654125603800000000 76.928344726562500000 552223.609475215310000000 5805841.654125603800000000 79.873999999999995000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="fee9e940-85eb-479e-b651-2e52990a19a0">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>19.99</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>65.28</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="ffad4971-f85e-4eb4-aaa7-f8e6b7465ddf" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="8561cda0-9121-4ed5-b32c-d5b644b208d1">
                  <gml:exterior>
                    <gml:LinearRing gml:id="af29489b-fc01-49a5-8007-89557ab5074f">
                      <gml:posList>552223.609475215310000000 5805841.654125603800000000 76.928344726562500000 552220.770999999720000000 5805847.820000000300000000 76.928344726562500000 552220.770999999720000000 5805847.820000000300000000 79.873999999999995000 552223.609475215310000000 5805841.654125603800000000 79.873999999999995000 552223.609475215310000000 5805841.654125603800000000 76.928344726562500000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="f09a0cbd-f0da-4aef-83a5-e335fcebdd62">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>527.95</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>335.28</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="c3ec81ea-bfc6-4fe4-aef1-07edb630b253" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="b004d859-b5f6-4da4-91f2-0501494a6a6c">
                  <gml:exterior>
                    <gml:LinearRing gml:id="629e3f77-d19b-4450-948d-6132ff5e217a">
                      <gml:posList>552220.770999999720000000 5805847.820000000300000000 76.928344726562500000 552220.770999999720000000 5805847.820000000300000000 52.849999999999994000 552219.862999999900000000 5805847.402000000700000000 52.849999999999994000 552215.141999999990000000 5805845.229000000300000000 52.849999999999994000 552203.024520526760000000 5805839.650785185400000000 52.850000000000001000 552203.024520526760000000 5805839.650785185400000000 69.996627807617188000 552203.024520526760000000 5805839.650785185400000000 79.873999999999995000 552215.141999999990000000 5805845.229000000300000000 79.873999999999995000 552219.862999999900000000 5805847.402000000700000000 79.873999999999995000 552220.770999999720000000 5805847.820000000300000000 79.873999999999995000 552220.770999999720000000 5805847.820000000300000000 76.928344726562500000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="6444c7dc-ee32-43b3-b6a0-5a1bfb13ecd3">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>67.56</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>245.32</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="27aeb8a3-8a0b-4616-81ef-3f6811e5fc95" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="4d035875-e5bd-4550-991e-8d32fc66b416">
                  <gml:exterior>
                    <gml:LinearRing gml:id="77edb326-38f4-4618-bc53-7c570491e582">
                      <gml:posList>552205.880849948620000000 5805833.436324728700000000 76.928344726562500000 552205.880849948620000000 5805833.436324728700000000 79.874000000000009000 552203.024520526760000000 5805839.650785185400000000 79.873999999999995000 552203.024520526760000000 5805839.650785185400000000 69.996627807617188000 552205.880849948620000000 5805833.436324728700000000 69.996627807617188000 552205.880849948620000000 5805833.436324728700000000 76.928344726562500000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="f43c1d21-91b2-4b3b-86c9-a69914979234">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>342.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>335.28</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="6f3a2479-110b-4a48-9bfc-932eb8f07f54" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="c819ee56-e6fa-48bc-a55f-f0950b27b3c3">
                  <gml:exterior>
                    <gml:LinearRing gml:id="b8ffa448-0de2-4f32-b3c3-d4bd198ef2c0">
                      <gml:posList>552203.024520526760000000 5805839.650785185400000000 52.850000000000001000 552195.169999999930000000 5805836.035000000100000000 52.850000000000001000 552190.449000000020000000 5805833.860999999600000000 52.850000000000001000 552188.997000000440000000 5805833.193000000000000000 52.850000000000001000 552184.886029962450000000 5805831.300786932000000000 52.850000000000001000 552184.886029962450000000 5805831.300786932000000000 69.996627807617188000 552188.997000000440000000 5805833.193000000000000000 69.996627807617188000 552190.449000000020000000 5805833.860999999600000000 69.996627807617188000 552195.169999999930000000 5805836.035000000100000000 69.996627807617188000 552203.024520526760000000 5805839.650785185400000000 69.996627807617188000 552203.024520526760000000 5805839.650785185400000000 52.850000000000001000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="0b2ebef8-b4cb-4868-9293-bd5224bd848d">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>68.54</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>155.32</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="72eddf0e-e113-444a-a89c-d40c34094002" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="e97d8808-e1f8-436a-93e7-2853ecb20646">
                  <gml:exterior>
                    <gml:LinearRing gml:id="99eee48e-563f-4224-96f8-a87bc49dc3e9">
                      <gml:posList>552193.236999999730000000 5805822.790999999300000000 69.996627807617188000 552189.605000000450000000 5805821.121999999500000000 69.996627807617188000 552189.605000000450000000 5805821.121999999500000000 52.850000000000001000 552193.236999999730000000 5805822.790999999300000000 52.850000000000001000 552193.236999999730000000 5805822.790999999300000000 69.996627807617188000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="5dc8bda7-10c6-432a-baa6-d5452951ee8e">
          <gen:stringAttribute name="Type">
            <gen:value>MainWall</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>273.22</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Azimuth">
            <gen:value>155.31</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Tilt">
            <gen:value>90.00</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="2ff5518b-73f3-4495-b7ae-e414c46c21f8" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="9f6c9f39-662b-4462-8153-317252e16d18">
                  <gml:exterior>
                    <gml:LinearRing gml:id="c6735b03-2019-4c38-b9a6-08d269fd73a7">
                      <gml:posList>552207.715074743030000000 5805829.445637458000000000 69.996627807617188000 552202.317999999970000000 5805826.964999999900000000 69.996627807617188000 552195.598000000230000000 5805823.877000000300000000 69.996627807617188000 552193.236999999730000000 5805822.790999999300000000 69.996627807617188000 552193.236999999730000000 5805822.790999999300000000 52.850000000000001000 552195.598000000230000000 5805823.877000000300000000 52.850000000000001000 552202.317999999970000000 5805826.964999999900000000 52.850000000000001000 552207.715074743030000000 5805829.445637458000000000 52.850000000000001000 552207.715074743030000000 5805829.445637458000000000 69.996627807617188000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="3776ad35-c56b-4679-a069-26f25e4bd962">
          <gen:stringAttribute name="Type">
            <gen:value>Ground</gen:value>
          </gen:stringAttribute>
          <gen:doubleAttribute name="Area">
            <gen:value>867.40</gen:value>
          </gen:doubleAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="e5e2bf88-9a2d-44c0-959c-d5c0e0ee291d" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="ef6b93c5-fbf7-4aaa-b6fe-cd38da204f06">
                  <gml:exterior>
                    <gml:LinearRing gml:id="12a17301-f0af-47c1-8065-0f9da10786af">
                      <gml:posList>552221.025000000370000000 5805835.562999999200000000 52.850000000000001000 552212.580000000070000000 5805831.680999999900000000 52.850000000000001000 552208.311999999920000000 5805829.720000000700000000 52.850000000000001000 552207.715074743030000000 5805829.445637458000000000 52.850000000000001000 552202.317999999970000000 5805826.964999999900000000 52.850000000000001000 552195.598000000230000000 5805823.877000000300000000 52.850000000000001000 552193.236999999730000000 5805822.790999999300000000 52.850000000000001000 552189.605000000450000000 5805821.121999999500000000 52.850000000000001000 552190.656000000420000000 5805818.855000000400000000 52.850000000000001000 552192.442999999970000000 5805814.995999999300000000 52.850000000000001000 552176.770619990540000000 5805807.776545314100000000 52.849999999999994000 552172.913999999870000000 5805806.000000000000000000 52.850000000000001000 552170.638000000270000000 5805804.960000000900000000 52.850000000000001000 552168.303999999540000000 5805810.036000000300000000 52.850000000000001000 552170.575000000190000000 5805811.084000000700000000 52.850000000000001000 552168.227161245770000000 5805816.188609021700000000 52.850000000000001000 552165.401999999770000000 5805822.331000000200000000 52.850000000000001000 552184.275999999610000000 5805831.019999999600000000 52.850000000000001000 552184.886029962450000000 5805831.300786932000000000 52.850000000000001000 552188.997000000440000000 5805833.193000000000000000 52.850000000000001000 552190.449000000020000000 5805833.860999999600000000 52.850000000000001000 552195.169999999930000000 5805836.035000000100000000 52.850000000000001000 552203.024520526760000000 5805839.650785185400000000 52.850000000000001000 552215.141999999990000000 5805845.229000000300000000 52.849999999999994000 552219.862999999900000000 5805847.402000000700000000 52.849999999999994000 552220.770999999720000000 5805847.820000000300000000 52.849999999999994000 552222.697999999860000000 5805848.719000000500000000 52.850000000000001000 552227.394999999550000000 5805838.507999999400000000 52.850000000000001000 552225.563000000080000000 5805837.649000000200000000 52.850000000000001000 552225.475999999790000000 5805837.607999999100000000 52.850000000000001000 552225.292999999600000000 5805837.525000000400000000 52.850000000000001000 552221.025000000370000000 5805835.562999999200000000 52.850000000000001000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </cityObjectMember>

</CityModel>
