<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<CityModel xmlns="http://www.opengis.net/citygml/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:core="http://www.opengis.net/citygml/base/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:tex="http://www.opengis.net/citygml/textures/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation=" http://www.opengis.net/citygml/2.0 http://schemas.opengis.net/citygml/2.0/cityGMLBase.xsd http://www.opengis.net/citygml/appearance/2.0  http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd">

  <!-- File Written  With RhinoCity Software  CopyRight Rhinoterraain 2012 -->

  <gml:description>Exported by Rhinocity</gml:description>

  <gml:name>Essai Rhino</gml:name>

  <gml:boundedBy>
    <gml:Envelope srsDimension="3" srsName="urn:adv:crs:ETRS89_UTM32*DE_DHHN92_NH">
      <gml:lowerCorner>550123.875000 5804566.500000 52.819996
</gml:lowerCorner>
      <gml:upperCorner>550223.937500 5804623.500000 76.384308
</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>

  <cityObjectMember>
    <bldg:Building gml:id="DENIAL43000052IS">
      <gml:boundedBy>
        <gml:Envelope srsDimension="3" srsName="urn:adv:crs:ETRS89_UTM32*DE_DHHN92_NH">
          <gml:lowerCorner>550199.187500 5804566.000000 53.929996
</gml:lowerCorner>
          <gml:upperCorner>550219.187500 5804591.000000 76.384308
</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <creationDate>2017-11-21</creationDate>
      <gen:doubleAttribute name="Volume">
        <gen:value>4765.607</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Profile">
        <gen:value>Sig3d</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCK">
        <gen:value>10</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOECKE">
        <gen:value>111010</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STBEZNR">
        <gen:value>111</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="MBZNR">
        <gen:value>1102</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="WBZNR">
        <gen:value>1102</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNAM">
        <gen:value>Vahrenwald</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTNR">
        <gen:value>11</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNAM">
        <gen:value>Vahrenwald-List</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBEZ">
        <gen:value>02</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEMEINDE">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SCALE">
        <gen:value>5000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SHAPE_Area">
        <gen:value>7165.30049704715</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ALKISID">
        <gen:value>DENIAL43000052IS</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ZMIN">
        <gen:value>53.93</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="THICKNESS">
        <gen:value>0.1</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="TEXTURE">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="BUILDINGID">
        <gen:value>DENIAL43000052IS</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ARTBEZEICH">
        <gen:value>Gebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Join_Cou_1">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="FUNKTION_T">
        <gen:value>Wohngebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="OBJFLAECHE">
        <gen:value>258.78</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADT">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEB_Funkti">
        <gen:value>1000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STRSCHL">
        <gen:value>00712, 01466</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STR_komple">
        <gen:value>Dörnbergstraße 1, Isernhagener Straße 21</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="PLZ_NUMMER">
        <gen:value>30161</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STR">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="HAUSNR">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="OVERHANG">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <app:appearance>
        <app:Appearance>
          <app:theme>RhinoCity ObliqueTexturing</app:theme>
          <app:surfaceDataMember>
            <app:ParameterizedTexture>
              <app:imageURI>Hannover_teil1_Appearance/DENIAL43000052IS.jpg</app:imageURI>
              <app:textureType>specific</app:textureType>
              <app:wrapMode>border</app:wrapMode>
              <app:borderColor>0 0 0 1</app:borderColor>
              <app:target uri="#UUID_ac61ecfe-3473-4081-a70c-50d47eb8d1a8">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_217a5d03-5ec1-49c4-aacb-5de5481e9edc">0.506103 0.217479 0.385991 0.253622 0.240705 0.078851 0.325013 0.001296 0.326331 0.000978 0.506103 0.217479 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_5f3bde48-bc28-4eea-b580-fd817702201f">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_adcce6c8-af15-4572-8c33-279f62ce033f">0.602873 0.240365 0.532862 0.085990 0.562945 0.079501 0.558618 0.066633 0.510763 0.069833 0.626136 0.000978 0.728653 0.226535 0.602873 0.240365 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_1266c884-e989-4ad9-954d-6806308cbfda">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_a4e77d3f-e0fd-44e0-a1ef-82e4536e7761">0.703920 0.528562 0.572971 0.533677 0.462447 0.470628 0.461840 0.466330 0.791592 0.451613 0.703920 0.528562 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_a3a004b6-d999-43ad-9f10-3846cd508dfe">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_49555da3-877a-44dc-aa84-5e2db54525ea">0.268536 0.564027 0.152976 0.632300 0.001957 0.640605 0.002693 0.634360 0.122410 0.570668 0.268536 0.564027 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_07181485-e7ab-448d-b8d3-5567ad2d2477">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_34b705e6-3b56-463a-a8fd-45cf1bc235b2">0.716233 0.364462 0.597295 0.428409 0.575342 0.315738 0.716233 0.364462 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_d74bd103-b1a7-4222-91c9-65aeb7a289d0">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_583e4cf0-620a-4789-bafb-b631c16d781a">0.279717 0.448643 0.278571 0.442700 0.257053 0.331159 0.256360 0.327569 0.330078 0.315738 0.331910 0.319188 0.353731 0.432001 0.353004 0.438279 0.279717 0.448643 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_a1ac9fcf-cfcf-4364-9f97-82ba155dedd0">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_eb75cb18-64cc-4ce2-a438-15250947020d">0.358121 0.323087 0.392795 0.321507 0.519485 0.315738 0.569947 0.414970 0.442660 0.421352 0.412056 0.431619 0.358121 0.323087 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_89a44e4d-47d1-4d39-adbf-c7bb6cfc243b">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_d6f37816-4eab-43dd-a805-a2071cefdfbc">0.278390 0.587342 0.273973 0.575115 0.356260 0.564027 0.360530 0.576431 0.278390 0.587342 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_24d9bcd1-437e-4e0c-a77e-3360d456d9b6">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_46e9cc21-f37b-4c34-ae7b-74c25b06c794">0.456442 0.555097 0.430358 0.554082 0.383562 0.456143 0.406793 0.451613 0.456442 0.555097 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_e650d007-5b4e-4b90-8762-b78ba7db2d8e">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_971748f4-3eb1-4660-8269-8b26df921243">0.775135 0.000978 0.858795 0.108544 0.818313 0.211245 0.733855 0.103404 0.775135 0.000978 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_43b65072-94a5-4985-9bee-c5de14968683">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_8fa12703-192a-4ff2-a953-2b106dd277af">0.001957 0.333461 0.107838 0.323990 0.200097 0.315738 0.251323 0.417988 0.169841 0.449456 0.050117 0.432477 0.001957 0.333461 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_c4adca3c-1a49-4738-be2c-7c1825f690a8">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_006d2484-c84f-49f1-9e45-259421dfaf08">0.181554 0.312903 0.001957 0.096242 0.058529 0.000978 0.236281 0.216985 0.181554 0.312903 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_12bb95b1-8c4b-4404-904b-b7035b8f9d75">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_81d268f9-ed4e-4c78-aae4-97c51e2d5327">0.378843 0.546172 0.377781 0.546487 0.047787 0.561249 0.001957 0.465785 0.329777 0.451664 0.330964 0.451613 0.378843 0.546172 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
            </app:ParameterizedTexture>
          </app:surfaceDataMember>
        </app:Appearance>
      </app:appearance>
      <bldg:measuredHeight uom="#m">22.454</bldg:measuredHeight>
      <bldg:lod1Solid>
        <gml:Solid gml:id="UUID_35eb99a1-ff06-4de4-884a-f0358ca697b9">
          <gml:exterior>
            <gml:CompositeSurface gml:id="UUID_8b051510-325f-49fb-8ad1-0c4deaa27d85">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_9204e3a9-cd6e-4cfc-b862-851120432867">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_c9ba6db9-a2d8-4a7b-9abb-1311debced66">
                      <gml:posList>550210.046000000090000000 5804590.165000000000000000 53.930000000000000000 550207.328000001610000000 5804578.267000000000000000 53.930000000000000000 550207.328000001610000000 5804578.267000000000000000 76.384299507541499000 550210.046000000090000000 5804590.165000000000000000 76.384299507541499000 550210.046000000090000000 5804590.165000000000000000 53.930000000000000000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_c44e948a-a225-4627-9d25-3674fed0bed3">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_115810c0-5f4d-4d2a-b791-0db4ac7f74ea">
                      <gml:posList>550218.975999999790000000 5804588.207000000400000000 53.930000000000000000 550214.202357516620000000 5804589.253673235000000000 53.930000000000000000 550210.046000000090000000 5804590.165000000000000000 53.930000000000000000 550210.046000000090000000 5804590.165000000000000000 76.384299507541499000 550214.202357516620000000 5804589.253673235000000000 76.384299507541499000 550218.975999999790000000 5804588.207000000400000000 76.384299507541499000 550218.975999999790000000 5804588.207000000400000000 53.930000000000000000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_c8b39d75-053b-4daa-a259-306412636fd0">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_f2d31f74-9cf9-4692-b28b-f77196f37ea4">
                      <gml:posList>550218.975999999790000000 5804588.207000000400000000 76.384299507541499000 550214.026999998840000000 5804566.743999999900000000 76.384299507541499000 550214.026999998840000000 5804566.743999999900000000 53.930000000000000000 550218.975999999790000000 5804588.207000000400000000 53.930000000000000000 550218.975999999790000000 5804588.207000000400000000 76.384299507541499000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_67c9e1da-720c-4e91-9dd5-ca2ac959fb57">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_3e79f569-4597-42d7-908a-325c0558004a">
                      <gml:posList>550214.026999998840000000 5804566.743999999900000000 76.384299507541499000 550213.975700659330000000 5804566.749163104200000000 76.384299507541499000 550199.798999998720000000 5804568.176000000000000000 76.384299507541499000 550199.798999998720000000 5804568.176000000000000000 53.930000000000000000 550213.975700659330000000 5804566.749163104200000000 53.930000000000000000 550214.026999998840000000 5804566.743999999900000000 53.930000000000000000 550214.026999998840000000 5804566.743999999900000000 76.384299507541499000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_d3cda295-fdda-4394-a752-dbece8ed57fb">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_adabd30a-eff0-4939-a96f-13a4d6f8f3aa">
                      <gml:posList>550200.730999998750000000 5804577.646999999900000000 53.930000000000000000 550200.685249853180000000 5804577.182086235800000000 53.930000000000000000 550199.826636983660000000 5804568.456847515900000000 53.930000000000000000 550199.798999998720000000 5804568.176000000000000000 53.930000000000000000 550199.798999998720000000 5804568.176000000000000000 76.384299507541499000 550199.826636983660000000 5804568.456847515900000000 76.384299507541499000 550200.685249853180000000 5804577.182086235800000000 76.384299507541499000 550200.730999998750000000 5804577.646999999900000000 76.384299507541499000 550200.730999998750000000 5804577.646999999900000000 53.930000000000000000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_9cc8f637-03c3-4332-a4bd-3e1f82091311">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_7ecb17fe-c8c4-4403-aae0-1403ea66b59e">
                      <gml:posList>550208.142000000920000000 5804576.824000000000000000 53.930000000000000000 550206.549136348300000000 5804577.000889324600000000 53.930000000000000000 550200.730999998750000000 5804577.646999999900000000 53.930000000000000000 550200.730999998750000000 5804577.646999999900000000 76.384299507541499000 550206.549136348300000000 5804577.000889324600000000 76.384299507541499000 550208.142000000920000000 5804576.824000000000000000 76.384299507541499000 550208.142000000920000000 5804576.824000000000000000 53.930000000000000000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_ad045fee-a1fb-43e0-ab6f-f71f8468eccb">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_b7bfe387-3667-4a00-8859-d0cc7acbc262">
                      <gml:posList>550208.317999999970000000 5804577.813000000100000000 53.930000000000000000 550208.142000000920000000 5804576.824000000000000000 53.930000000000000000 550208.142000000920000000 5804576.824000000000000000 76.384299507541499000 550208.317999999970000000 5804577.813000000100000000 76.384299507541499000 550208.317999999970000000 5804577.813000000100000000 53.930000000000000000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_441b4ca2-7580-4b3f-b8c1-77037aae56d9">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_d62a62f2-7792-47cf-9824-24e65bc7d280">
                      <gml:posList>550208.317999999970000000 5804577.813000000100000000 76.384299507541499000 550207.328000001610000000 5804578.267000000000000000 76.384299507541499000 550207.328000001610000000 5804578.267000000000000000 53.930000000000000000 550208.317999999970000000 5804577.813000000100000000 53.930000000000000000 550208.317999999970000000 5804577.813000000100000000 76.384299507541499000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_e4f54e35-d2e1-477f-a221-f0b594f816a2">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_79602196-25c1-43ff-a9dc-a085dffe5a29">
                      <gml:posList>550207.328000001610000000 5804578.267000000000000000 53.930000000000000000 550210.046000000090000000 5804590.165000000000000000 53.930000000000000000 550214.202357516620000000 5804589.253673235000000000 53.930000000000000000 550218.975999999790000000 5804588.207000000400000000 53.930000000000000000 550214.026999998840000000 5804566.743999999900000000 53.930000000000000000 550213.975700659330000000 5804566.749163104200000000 53.930000000000000000 550199.798999998720000000 5804568.176000000000000000 53.930000000000000000 550199.826636983660000000 5804568.456847515900000000 53.930000000000000000 550200.685249853180000000 5804577.182086235800000000 53.930000000000000000 550200.730999998750000000 5804577.646999999900000000 53.930000000000000000 550206.549136348300000000 5804577.000889324600000000 53.930000000000000000 550208.142000000920000000 5804576.824000000000000000 53.930000000000000000 550208.317999999970000000 5804577.813000000100000000 53.930000000000000000 550207.328000001610000000 5804578.267000000000000000 53.930000000000000000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_08d33eef-b704-45c8-ac2c-96fb43ca4e63">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_b22bb9c5-ac08-4bdc-92cf-d950e030a9d3">
                      <gml:posList>550207.328000001610000000 5804578.267000000000000000 76.384299507541499000 550208.317999999970000000 5804577.813000000100000000 76.384299507541499000 550208.142000000920000000 5804576.824000000000000000 76.384299507541499000 550206.549136348300000000 5804577.000889324600000000 76.384299507541499000 550200.730999998750000000 5804577.646999999900000000 76.384299507541499000 550200.685249853180000000 5804577.182086235800000000 76.384299507541499000 550199.826636983660000000 5804568.456847515900000000 76.384299507541499000 550199.798999998720000000 5804568.176000000000000000 76.384299507541499000 550213.975700659330000000 5804566.749163104200000000 76.384299507541499000 550214.026999998840000000 5804566.743999999900000000 76.384299507541499000 550218.975999999790000000 5804588.207000000400000000 76.384299507541499000 550214.202357516620000000 5804589.253673235000000000 76.384299507541499000 550210.046000000090000000 5804590.165000000000000000 76.384299507541499000 550207.328000001610000000 5804578.267000000000000000 76.384299507541499000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod1Solid>
      <bldg:lod2Solid>
        <gml:Solid gml:id="UUID_de1cf66a-83a3-49ee-aaef-5a75da80d8ed">
          <gml:exterior>
            <gml:CompositeSurface gml:id="UUID_45311b96-62ee-47ca-b5d2-99d6de4fdf36">
              <gml:surfaceMember xlink:href="#UUID_ac61ecfe-3473-4081-a70c-50d47eb8d1a8"/>
              <gml:surfaceMember xlink:href="#UUID_5f3bde48-bc28-4eea-b580-fd817702201f"/>
              <gml:surfaceMember xlink:href="#UUID_1266c884-e989-4ad9-954d-6806308cbfda"/>
              <gml:surfaceMember xlink:href="#UUID_a3a004b6-d999-43ad-9f10-3846cd508dfe"/>
              <gml:surfaceMember xlink:href="#UUID_07181485-e7ab-448d-b8d3-5567ad2d2477"/>
              <gml:surfaceMember xlink:href="#UUID_d74bd103-b1a7-4222-91c9-65aeb7a289d0"/>
              <gml:surfaceMember xlink:href="#UUID_a1ac9fcf-cfcf-4364-9f97-82ba155dedd0"/>
              <gml:surfaceMember xlink:href="#UUID_89a44e4d-47d1-4d39-adbf-c7bb6cfc243b"/>
              <gml:surfaceMember xlink:href="#UUID_24d9bcd1-437e-4e0c-a77e-3360d456d9b6"/>
              <gml:surfaceMember xlink:href="#UUID_e650d007-5b4e-4b90-8762-b78ba7db2d8e"/>
              <gml:surfaceMember xlink:href="#UUID_43b65072-94a5-4985-9bee-c5de14968683"/>
              <gml:surfaceMember xlink:href="#UUID_c4adca3c-1a49-4738-be2c-7c1825f690a8"/>
              <gml:surfaceMember xlink:href="#UUID_12bb95b1-8c4b-4404-904b-b7035b8f9d75"/>
              <gml:surfaceMember xlink:href="#UUID_afa462c4-d3fd-41d0-a73e-09ac2279a3a6"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_81c7ee1b-7841-4c58-97da-fb59f1ff67b3">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_9736f0a7-7200-48a1-adcd-879949fcdbea" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_ac61ecfe-3473-4081-a70c-50d47eb8d1a8">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_217a5d03-5ec1-49c4-aacb-5de5481e9edc">
                      <gml:posList>550218.975999999790000000 5804588.207000000400000000 70.204665091736715000 550214.202357516620000000 5804589.253673235000000000 74.519002325104651000 550210.233967635780000000 5804571.914300164200000000 74.506564462134747000 550213.975700659330000000 5804566.749163104200000000 70.266140052894883000 550214.026999998840000000 5804566.743999999900000000 70.220979211785519000 550218.975999999790000000 5804588.207000000400000000 70.204665091736715000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_85bac329-ff4f-4b08-9558-2300865a6538">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_a7ac9e91-88a8-4030-aa02-0d6b89d2de01" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_5f3bde48-bc28-4eea-b580-fd817702201f">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_adcce6c8-af15-4572-8c33-279f62ce033f">
                      <gml:posList>550210.046000000090000000 5804590.165000000000000000 70.740421233752542000 550207.328000001610000000 5804578.267000000000000000 70.736255107671838000 550208.317999999970000000 5804577.813000000100000000 71.683052317588647000 550208.142000000920000000 5804576.824000000000000000 71.725935360492485000 550206.549136348300000000 5804577.000889324600000000 70.311880921680910000 550210.233967635780000000 5804571.914300164200000000 74.506564462134747000 550214.202357516620000000 5804589.253673235000000000 74.519002325104651000 550210.046000000090000000 5804590.165000000000000000 70.740421233752542000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_b95da440-f8a8-48b7-81b6-5d27433e2fbb">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_a06f7636-53fe-4f6d-b3a1-2de1b1d9c358" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_1266c884-e989-4ad9-954d-6806308cbfda">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_a4e77d3f-e0fd-44e0-a1ef-82e4536e7761">
                      <gml:posList>550210.233967635780000000 5804571.914300164200000000 74.506564462134747000 550204.603937155800000000 5804572.434076911800000000 74.505526154822832000 550199.826636983660000000 5804568.456847515900000000 70.616755213905947000 550199.798999998720000000 5804568.176000000000000000 70.367371593174084000 550213.975700659330000000 5804566.749163104200000000 70.266140052894883000 550210.233967635780000000 5804571.914300164200000000 74.506564462134747000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_a433af75-2531-4ea8-89f1-381603eb60ec">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_4e532564-d2e3-4378-b5c1-ef75a20636c6" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_a3a004b6-d999-43ad-9f10-3846cd508dfe">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_49555da3-877a-44dc-aa84-5e2db54525ea">
                      <gml:posList>550210.233967635780000000 5804571.914300164200000000 74.506564462134747000 550206.549136348300000000 5804577.000889324600000000 70.311880921680910000 550200.730999998750000000 5804577.646999999900000000 70.214525765128371000 550200.685249853180000000 5804577.182086235800000000 70.629055777377360000 550204.603937155800000000 5804572.434076911800000000 74.505526154822832000 550210.233967635780000000 5804571.914300164200000000 74.506564462134747000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_172ea54c-fafb-4c8a-a5ae-26bcaa41504c">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_512316a1-6b56-401c-a5b0-e6785773e279" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_07181485-e7ab-448d-b8d3-5567ad2d2477">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_34b705e6-3b56-463a-a8fd-45cf1bc235b2">
                      <gml:posList>550204.603937155800000000 5804572.434076911800000000 74.505526154822832000 550200.685249853180000000 5804577.182086235800000000 70.629055777377360000 550199.826636983660000000 5804568.456847515900000000 70.616755213905947000 550204.603937155800000000 5804572.434076911800000000 74.505526154822832000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_5482c6ad-82c8-4b55-8379-daf6b55c77eb">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_24115538-baea-405c-81b2-011fd276ff0c" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_d74bd103-b1a7-4222-91c9-65aeb7a289d0">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_583e4cf0-620a-4789-bafb-b631c16d781a">
                      <gml:posList>550200.730999998750000000 5804577.646999999900000000 53.930000000000000000 550200.685249853180000000 5804577.182086235800000000 53.930000000000000000 550199.826636983660000000 5804568.456847515900000000 53.930000000000000000 550199.798999998720000000 5804568.176000000000000000 53.930000000000000000 550199.798999998720000000 5804568.176000000000000000 70.367371593174084000 550199.826636983660000000 5804568.456847515900000000 70.616755213905947000 550200.685249853180000000 5804577.182086235800000000 70.629055777377360000 550200.730999998750000000 5804577.646999999900000000 70.214525765128371000 550200.730999998750000000 5804577.646999999900000000 53.930000000000000000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_f3e8fc45-a195-44b8-b420-1ee36e9e58f0">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_cd935f13-8d0c-48fb-b896-a46759144ac9" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_a1ac9fcf-cfcf-4364-9f97-82ba155dedd0">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_eb75cb18-64cc-4ce2-a438-15250947020d">
                      <gml:posList>550208.142000000920000000 5804576.824000000000000000 53.930000000000000000 550206.549136348300000000 5804577.000889324600000000 53.930000000000000000 550200.730999998750000000 5804577.646999999900000000 53.930000000000000000 550200.730999998750000000 5804577.646999999900000000 70.214525765128371000 550206.549136348300000000 5804577.000889324600000000 70.311880921680910000 550208.142000000920000000 5804576.824000000000000000 71.725935360492485000 550208.142000000920000000 5804576.824000000000000000 53.930000000000000000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_2bda7365-e304-459c-b499-a9125293eadb">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_14a40a7d-33df-4016-b6b4-fc0e582120fd" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_89a44e4d-47d1-4d39-adbf-c7bb6cfc243b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_d6f37816-4eab-43dd-a805-a2071cefdfbc">
                      <gml:posList>550208.317999999970000000 5804577.813000000100000000 53.930000000000000000 550208.142000000920000000 5804576.824000000000000000 53.930000000000000000 550208.142000000920000000 5804576.824000000000000000 71.725935360492485000 550208.317999999970000000 5804577.813000000100000000 71.683052317588647000 550208.317999999970000000 5804577.813000000100000000 53.930000000000000000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_ad47637a-9144-412f-a008-fbc7348ed565">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_e4fae497-43eb-4c15-802a-8bcda1212206" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_24d9bcd1-437e-4e0c-a77e-3360d456d9b6">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_46e9cc21-f37b-4c34-ae7b-74c25b06c794">
                      <gml:posList>550208.317999999970000000 5804577.813000000100000000 71.683052317588647000 550207.328000001610000000 5804578.267000000000000000 70.736255107671838000 550207.328000001610000000 5804578.267000000000000000 53.930000000000000000 550208.317999999970000000 5804577.813000000100000000 53.930000000000000000 550208.317999999970000000 5804577.813000000100000000 71.683052317588647000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_9cb554da-0680-41ff-a76c-d00c81c4bd42">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_6a03c28c-a281-4717-98a2-1dd2b803c14c" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_e650d007-5b4e-4b90-8762-b78ba7db2d8e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_971748f4-3eb1-4660-8269-8b26df921243">
                      <gml:posList>550210.046000000090000000 5804590.165000000000000000 53.930000000000000000 550207.328000001610000000 5804578.267000000000000000 53.930000000000000000 550207.328000001610000000 5804578.267000000000000000 70.736255107671838000 550210.046000000090000000 5804590.165000000000000000 70.740421233752542000 550210.046000000090000000 5804590.165000000000000000 53.930000000000000000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_5ca50fb1-cd1f-4009-bc9b-7f5655ba004c">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_126eb04d-333a-4069-a215-61a1ba9a73d0" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_43b65072-94a5-4985-9bee-c5de14968683">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_8fa12703-192a-4ff2-a953-2b106dd277af">
                      <gml:posList>550218.975999999790000000 5804588.207000000400000000 53.930000000000000000 550214.202357516620000000 5804589.253673235000000000 53.930000000000000000 550210.046000000090000000 5804590.165000000000000000 53.930000000000000000 550210.046000000090000000 5804590.165000000000000000 70.740421233752542000 550214.202357516620000000 5804589.253673235000000000 74.519002325104651000 550218.975999999790000000 5804588.207000000400000000 70.204665091736715000 550218.975999999790000000 5804588.207000000400000000 53.930000000000000000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_206790c9-c5db-4362-8500-73743eea9fd4">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_3bbafe61-9ae4-4f05-b452-75c5a02c427b" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_c4adca3c-1a49-4738-be2c-7c1825f690a8">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_006d2484-c84f-49f1-9e45-259421dfaf08">
                      <gml:posList>550218.975999999790000000 5804588.207000000400000000 70.204665091736715000 550214.026999998840000000 5804566.743999999900000000 70.220979211785519000 550214.026999998840000000 5804566.743999999900000000 53.930000000000000000 550218.975999999790000000 5804588.207000000400000000 53.930000000000000000 550218.975999999790000000 5804588.207000000400000000 70.204665091736715000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_0f516782-d8a7-4d54-8afc-6405d0e54e46">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_8c72dc93-5cfd-4477-b304-dbd049f9ce2c" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_12bb95b1-8c4b-4404-904b-b7035b8f9d75">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_81d268f9-ed4e-4c78-aae4-97c51e2d5327">
                      <gml:posList>550214.026999998840000000 5804566.743999999900000000 70.220979211785519000 550213.975700659330000000 5804566.749163104200000000 70.266140052894883000 550199.798999998720000000 5804568.176000000000000000 70.367371593174084000 550199.798999998720000000 5804568.176000000000000000 53.930000000000000000 550213.975700659330000000 5804566.749163104200000000 53.930000000000000000 550214.026999998840000000 5804566.743999999900000000 53.930000000000000000 550214.026999998840000000 5804566.743999999900000000 70.220979211785519000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="UUID_639e50b8-06c5-4769-8c7e-f11cea811ac4">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_db42ae26-bcee-49f9-87b4-8f4891811350" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_afa462c4-d3fd-41d0-a73e-09ac2279a3a6">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_327fbc72-5e47-41d7-9e22-fd385d538b30">
                      <gml:posList>550207.328000001610000000 5804578.267000000000000000 53.930000000000000000 550210.046000000090000000 5804590.165000000000000000 53.930000000000000000 550214.202357516620000000 5804589.253673235000000000 53.930000000000000000 550218.975999999790000000 5804588.207000000400000000 53.930000000000000000 550214.026999998840000000 5804566.743999999900000000 53.930000000000000000 550213.975700659330000000 5804566.749163104200000000 53.930000000000000000 550199.798999998720000000 5804568.176000000000000000 53.930000000000000000 550199.826636983660000000 5804568.456847515900000000 53.930000000000000000 550200.685249853180000000 5804577.182086235800000000 53.930000000000000000 550200.730999998750000000 5804577.646999999900000000 53.930000000000000000 550206.549136348300000000 5804577.000889324600000000 53.930000000000000000 550208.142000000920000000 5804576.824000000000000000 53.930000000000000000 550208.317999999970000000 5804577.813000000100000000 53.930000000000000000 550207.328000001610000000 5804578.267000000000000000 53.930000000000000000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </cityObjectMember>

  <cityObjectMember>
    <bldg:Building gml:id="DENIAL43000053KG">
      <gml:boundedBy>
        <gml:Envelope srsDimension="3" srsName="urn:adv:crs:ETRS89_UTM32*DE_DHHN92_NH">
          <gml:lowerCorner>550184.875000 5804568.000000 53.629997
</gml:lowerCorner>
          <gml:upperCorner>550201.562500 5804584.500000 70.920990
</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <creationDate>2017-11-21</creationDate>
      <gen:doubleAttribute name="Volume">
        <gen:value>2547.155</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Profile">
        <gen:value>Sig3d</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCK">
        <gen:value>10</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOECKE">
        <gen:value>111010</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STBEZNR">
        <gen:value>111</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="MBZNR">
        <gen:value>1102</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="WBZNR">
        <gen:value>1102</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNAM">
        <gen:value>Vahrenwald</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTNR">
        <gen:value>11</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNAM">
        <gen:value>Vahrenwald-List</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBEZ">
        <gen:value>02</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEMEINDE">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SCALE">
        <gen:value>5000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SHAPE_Area">
        <gen:value>7165.30049704715</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ALKISID">
        <gen:value>DENIAL43000053KG</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ZMIN">
        <gen:value>53.63</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="THICKNESS">
        <gen:value>0.1</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="TEXTURE">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="BUILDINGID">
        <gen:value>DENIAL43000053KG</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ARTBEZEICH">
        <gen:value>Gebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Join_Cou_1">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="FUNKTION_T">
        <gen:value>Wohngebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="OBJFLAECHE">
        <gen:value>169.0</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADT">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEB_Funkti">
        <gen:value>1000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STRSCHL">
        <gen:value>00712</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STR_komple">
        <gen:value>Dörnbergstraße 2</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="PLZ_NUMMER">
        <gen:value>30161</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STR">
        <gen:value>Dörnbergstraße</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="HAUSNR">
        <gen:value>2</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="OVERHANG">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <app:appearance>
        <app:Appearance>
          <app:theme>RhinoCity ObliqueTexturing</app:theme>
          <app:surfaceDataMember>
            <app:ParameterizedTexture>
              <app:imageURI>Hannover_teil1_Appearance/DENIAL43000053KG.jpg</app:imageURI>
              <app:textureType>specific</app:textureType>
              <app:wrapMode>border</app:wrapMode>
              <app:borderColor>0 0 0 1</app:borderColor>
              <app:target uri="#UUID_5e56cd39-0151-4709-9e08-334d079313e6">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_44d494fb-1279-4c1a-bbbe-497d3f9bac38">0.515060 0.672615 0.403785 0.683504 0.399217 0.617497 0.508571 0.606654 0.515060 0.672615 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_755f714e-02f6-416e-9960-1b9a44d853f2">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_a118d3b6-0f58-4866-8a80-00f66b0abeb4">0.143512 0.440861 0.240895 0.432875 0.240174 0.415524 0.341338 0.407045 0.351989 0.573221 0.012609 0.602703 0.001957 0.452470 0.035011 0.449760 0.040887 0.546616 0.070968 0.544108 0.149706 0.537224 0.143512 0.440861 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_d819aac4-af53-4590-964d-c8529b57bcad">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_510ee22d-249c-4f33-a706-e955f919fbc1">0.133634 0.708642 0.011489 0.720870 0.001957 0.618979 0.123791 0.606654 0.133634 0.708642 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_3e0ad88b-df29-4c16-b112-06f6b777eff6">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_a7c9a343-7bae-4f9b-996b-98e17df82e42">0.513876 0.585002 0.516022 0.479115 0.393023 0.491524 0.390808 0.597286 0.356164 0.600744 0.358674 0.445320 0.738351 0.407045 0.736123 0.479198 0.685583 0.483590 0.683325 0.568088 0.513876 0.585002 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_ad95420e-c936-4fb8-a649-b12bdd6dbee0">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_41b9dcf7-2e4d-4850-9690-a89c4d50bf08">0.352505 0.689793 0.265346 0.698607 0.232025 0.701827 0.223092 0.618270 0.343179 0.606654 0.352505 0.689793 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_57e9ec39-6164-4d02-8585-e650ab4cba14">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_f42bd9de-2714-4ac0-8cb9-408da21f1f99">0.373623 0.701867 0.358121 0.633661 0.365861 0.606654 0.373623 0.701867 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_b09721da-037e-4acc-ab79-635318241fd1">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_480c27cc-1f4c-4923-87e2-fbe0856c3828">0.395091 0.689978 0.377691 0.609822 0.386931 0.606654 0.395091 0.689978 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_8a9a69df-00d6-4dad-ab9a-ca70eb966cca">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_8bf5f1d3-b769-4191-b1eb-d5cac63e5bcb">0.542805 0.682818 0.520548 0.606654 0.552251 0.651457 0.542805 0.682818 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_3cdadd4b-9920-403e-84c6-fa6c545f3ee6">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_6fcd66bf-f18f-4876-b2dd-84e2c1a65491">0.149142 0.708388 0.138943 0.711541 0.140852 0.606654 0.149142 0.708388 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_babc6d0a-7048-4b6a-8ee3-866e901765d6">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_71949192-79ff-471a-b7ad-8fed30c23c4e">0.664675 0.635840 0.564763 0.643995 0.557730 0.614904 0.657499 0.606654 0.664675 0.635840 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_313071bc-d914-49ae-a0c7-364f284a0011">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_fea0784b-a80c-4fe8-9e1a-05f59b08f5ce">0.130856 0.299601 0.115329 0.150395 0.101761 0.019831 0.157276 0.001957 0.188246 0.128517 0.185778 0.284491 0.130856 0.299601 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_4c587783-0b60-4e65-85ef-6d29805711c0">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_950bcaac-7137-41c8-b8af-eeeb603863ed">0.219234 0.216719 0.193738 0.180346 0.247533 0.001957 0.264694 0.065271 0.219234 0.216719 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_aa25ef67-9253-42f5-873b-00bd282c5ac1">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_23d74373-1ba3-4649-b8c6-95d9325241e0">0.743640 0.410131 0.785575 0.407045 0.832587 0.592762 0.790315 0.595619 0.743640 0.410131 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_e7a6500a-42fd-452f-81de-f3e0786c49df">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_5078cb19-011a-412a-912f-6dae608702fb">0.310605 0.182377 0.270059 0.024898 0.413285 0.013019 0.517353 0.004387 0.546655 0.001957 0.588789 0.159507 0.559319 0.161930 0.568129 0.195006 0.463351 0.203657 0.454653 0.170535 0.310605 0.182377 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_03c170b1-2e16-4316-8508-b31303ddd930">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_64f7a7a2-7422-42d7-9d9c-c5c904696282">0.768793 0.163247 0.775089 0.190489 0.666678 0.199563 0.660459 0.172146 0.627457 0.174857 0.592955 0.021874 0.625749 0.019119 0.733395 0.010074 0.830009 0.001957 0.866026 0.155260 0.768793 0.163247 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_6f8ba16b-f424-44cb-9c1a-f1aae3584856">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_9b3bbb4e-c452-42dc-8ab4-92290295f6dd">0.043215 0.401583 0.021565 0.370335 0.006837 0.203601 0.016133 0.173052 0.001957 0.120205 0.038079 0.001957 0.052153 0.054688 0.082507 0.168420 0.096998 0.222685 0.043215 0.401583 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_e2d8dbb2-138f-4ce3-904d-a602f908aa15">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_d1155ea9-cb75-4d80-9b11-1b0d41331d3a">0.963466 0.523864 0.865030 0.532380 0.837573 0.415542 0.935555 0.407045 0.963466 0.523864 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_ad5ed1d4-dc6e-4d89-85ec-45bcdd66a8b4">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_0264e278-d722-4f2b-8145-35c8ed1d75c4">0.217636 0.683765 0.160086 0.701668 0.159075 0.686787 0.154599 0.620940 0.198681 0.606654 0.203188 0.672958 0.214569 0.669390 0.217636 0.683765 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
            </app:ParameterizedTexture>
          </app:surfaceDataMember>
        </app:Appearance>
      </app:appearance>
      <bldg:measuredHeight uom="#m">17.291</bldg:measuredHeight>
      <bldg:lod1Solid>
        <gml:Solid gml:id="UUID_080cae0e-837b-4615-aa08-d0df47356010">
          <gml:exterior>
            <gml:CompositeSurface gml:id="UUID_a34e2b07-f96d-444b-845a-27ea6e0bc24e">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_30793d54-8094-4ccd-a930-92d5da40081f">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_3ef9e6a9-3e69-4f09-8586-fc0f9e989604">
                      <gml:posList>550200.875999998300000000 5804579.116999999600000000 70.920988794924497000 550200.610080263000000000 5804576.418211569100000000 70.920988794924497000 550200.055345483940000000 5804570.780987209600000000 70.920988794924497000 550199.799000000000000000 5804568.176000000000000000 70.920988794924497000 550199.799000000000000000 5804568.176000000000000000 53.630000000000010000 550200.055345483940000000 5804570.780987209600000000 53.630000000000003000 550200.610080263000000000 5804576.418211569100000000 53.630000000000003000 550200.875999998300000000 5804579.116999999600000000 53.630000000000010000 550200.875999998300000000 5804579.116999999600000000 70.920988794924497000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_b7c1ba8c-f240-485e-98fd-19f616bed76f">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_c5a3495c-2b1b-4133-9901-dc3f75247105">
                      <gml:posList>550199.799000000000000000 5804568.176000000000000000 70.920988794924497000 550195.515000000010000000 5804568.607999999100000000 70.920988794924497000 550195.515000000010000000 5804568.607999999100000000 53.630000000000010000 550199.799000000000000000 5804568.176000000000000000 53.630000000000010000 550199.799000000000000000 5804568.176000000000000000 70.920988794924497000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_462e3e87-5342-4bed-be92-6bdd4e43a299">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_64d6e0d3-c789-45b7-a626-c8d4f7b0a442">
                      <gml:posList>550195.736999999730000000 5804571.803999999500000000 53.630000000000010000 550195.696076384160000000 5804571.214847403600000000 53.630000000000010000 550195.515000000010000000 5804568.607999999100000000 53.630000000000010000 550195.515000000010000000 5804568.607999999100000000 70.920988794924497000 550195.696076384160000000 5804571.214847403600000000 70.920988794924497000 550195.736999999730000000 5804571.803999999500000000 70.920988794924497000 550195.736999999730000000 5804571.803999999500000000 53.630000000000010000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_11e77750-5283-4fb6-a84b-63c38d483ce7">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_294b8840-9129-40ad-9d94-5b879ba564d0">
                      <gml:posList>550195.736999999730000000 5804571.803999999500000000 70.920988794924497000 550191.538930849410000000 5804572.215942920200000000 70.920988794924497000 550186.859814335480000000 5804572.675089460800000000 70.920988794924497000 550185.434000000360000000 5804572.815000000400000000 70.920988794924497000 550185.434000000360000000 5804572.815000000400000000 53.630000000000010000 550186.859814335480000000 5804572.675089460800000000 53.630000000000003000 550191.538930849410000000 5804572.215942920200000000 53.630000000000003000 550195.736999999730000000 5804571.803999999500000000 53.630000000000010000 550195.736999999730000000 5804571.803999999500000000 70.920988794924497000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_fd01f33f-7d16-4b4a-9601-2b188e0f383e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_988e55ed-6515-4b21-8bc7-fd97b287b3be">
                      <gml:posList>550186.596999999140000000 5804583.740000000200000000 53.630000000000010000 550185.976335074640000000 5804577.913534953300000000 53.630000000000003000 550185.434000000360000000 5804572.815000000400000000 53.630000000000010000 550185.434000000360000000 5804572.815000000400000000 70.920988794924497000 550185.976335074640000000 5804577.913534953300000000 70.920988794924497000 550186.596999999140000000 5804583.740000000200000000 70.920988794924497000 550186.596999999140000000 5804583.740000000200000000 53.630000000000010000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_6109067a-37dd-42d6-9e3c-c309b78392ad">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_77460160-ed21-4bb3-b5d4-89e66d76c9fb">
                      <gml:posList>550199.245000001040000000 5804582.460000000000000000 53.630000000000010000 550192.694097525210000000 5804583.122962932100000000 53.630000000000003000 550187.936297518780000000 5804583.604460719000000000 53.630000000000003000 550186.596999999140000000 5804583.740000000200000000 53.630000000000010000 550186.596999999140000000 5804583.740000000200000000 70.920988794924497000 550187.936297518780000000 5804583.604460719000000000 70.920988794924497000 550192.694097525210000000 5804583.122962932100000000 70.920988794924497000 550199.245000001040000000 5804582.460000000000000000 70.920988794924497000 550199.245000001040000000 5804582.460000000000000000 53.630000000000010000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_88edc9a1-a2e1-4954-816d-78c683dd231f">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_5b0144e1-db0d-4c54-946d-98a00e381401">
                      <gml:posList>550199.245000001040000000 5804582.460000000000000000 70.920988794924497000 550198.921999998390000000 5804579.290000000000000000 70.920988794924497000 550198.921999998390000000 5804579.290000000000000000 53.630000000000010000 550199.245000001040000000 5804582.460000000000000000 53.630000000000010000 550199.245000001040000000 5804582.460000000000000000 70.920988794924497000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_f819efbd-cc70-4132-ad74-b777e65207b9">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_563e81e9-d045-479e-b6a3-8ab9880ef659">
                      <gml:posList>550200.875999998300000000 5804579.116999999600000000 53.630000000000010000 550198.921999998390000000 5804579.290000000000000000 53.630000000000010000 550198.921999998390000000 5804579.290000000000000000 70.920988794924497000 550200.875999998300000000 5804579.116999999600000000 70.920988794924497000 550200.875999998300000000 5804579.116999999600000000 53.630000000000010000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_9cf55e95-0d46-4bf0-a4ef-f6b6f0e3235d">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_f9b467c1-acc3-4f28-b735-90469625c145">
                      <gml:posList>550200.055345483940000000 5804570.780987209600000000 53.630000000000003000 550199.799000000000000000 5804568.176000000000000000 53.630000000000010000 550195.515000000010000000 5804568.607999999100000000 53.630000000000010000 550195.696076384160000000 5804571.214847403600000000 53.630000000000010000 550195.736999999730000000 5804571.803999999500000000 53.630000000000010000 550191.538930849410000000 5804572.215942920200000000 53.630000000000003000 550186.859814335480000000 5804572.675089460800000000 53.630000000000003000 550185.434000000360000000 5804572.815000000400000000 53.630000000000010000 550185.976335074640000000 5804577.913534953300000000 53.630000000000003000 550186.596999999140000000 5804583.740000000200000000 53.630000000000010000 550187.936297518780000000 5804583.604460719000000000 53.630000000000003000 550192.694097525210000000 5804583.122962932100000000 53.630000000000003000 550199.245000001040000000 5804582.460000000000000000 53.630000000000010000 550198.921999998390000000 5804579.290000000000000000 53.630000000000010000 550200.875999998300000000 5804579.116999999600000000 53.630000000000010000 550200.610080263000000000 5804576.418211569100000000 53.630000000000003000 550200.055345483940000000 5804570.780987209600000000 53.630000000000003000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_d1956faf-0297-4543-8647-7d8dd1caa144">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_514a860c-01f1-4fc4-8797-e542bbada107">
                      <gml:posList>550200.610080263000000000 5804576.418211569100000000 70.920988794924497000 550200.875999998300000000 5804579.116999999600000000 70.920988794924497000 550198.921999998390000000 5804579.290000000000000000 70.920988794924497000 550199.245000001040000000 5804582.460000000000000000 70.920988794924497000 550192.694097525210000000 5804583.122962932100000000 70.920988794924497000 550187.936297518780000000 5804583.604460719000000000 70.920988794924497000 550186.596999999140000000 5804583.740000000200000000 70.920988794924497000 550185.976335074640000000 5804577.913534953300000000 70.920988794924497000 550185.434000000360000000 5804572.815000000400000000 70.920988794924497000 550186.859814335480000000 5804572.675089460800000000 70.920988794924497000 550191.538930849410000000 5804572.215942920200000000 70.920988794924497000 550195.736999999730000000 5804571.803999999500000000 70.920988794924497000 550195.696076384160000000 5804571.214847403600000000 70.920988794924497000 550195.515000000010000000 5804568.607999999100000000 70.920988794924497000 550199.799000000000000000 5804568.176000000000000000 70.920988794924497000 550200.055345483940000000 5804570.780987209600000000 70.920988794924497000 550200.610080263000000000 5804576.418211569100000000 70.920988794924497000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod1Solid>
      <bldg:lod2Solid>
        <gml:Solid gml:id="UUID_331d7be3-b4d3-4077-b7ff-e9a124989492">
          <gml:exterior>
            <gml:CompositeSurface gml:id="UUID_bc38ce97-24fd-4161-9678-8bccf648a674">
              <gml:surfaceMember xlink:href="#UUID_5e56cd39-0151-4709-9e08-334d079313e6"/>
              <gml:surfaceMember xlink:href="#UUID_755f714e-02f6-416e-9960-1b9a44d853f2"/>
              <gml:surfaceMember xlink:href="#UUID_d819aac4-af53-4590-964d-c8529b57bcad"/>
              <gml:surfaceMember xlink:href="#UUID_3e0ad88b-df29-4c16-b112-06f6b777eff6"/>
              <gml:surfaceMember xlink:href="#UUID_ad95420e-c936-4fb8-a649-b12bdd6dbee0"/>
              <gml:surfaceMember xlink:href="#UUID_57e9ec39-6164-4d02-8585-e650ab4cba14"/>
              <gml:surfaceMember xlink:href="#UUID_b09721da-037e-4acc-ab79-635318241fd1"/>
              <gml:surfaceMember xlink:href="#UUID_8a9a69df-00d6-4dad-ab9a-ca70eb966cca"/>
              <gml:surfaceMember xlink:href="#UUID_3cdadd4b-9920-403e-84c6-fa6c545f3ee6"/>
              <gml:surfaceMember xlink:href="#UUID_babc6d0a-7048-4b6a-8ee3-866e901765d6"/>
              <gml:surfaceMember xlink:href="#UUID_313071bc-d914-49ae-a0c7-364f284a0011"/>
              <gml:surfaceMember xlink:href="#UUID_4c587783-0b60-4e65-85ef-6d29805711c0"/>
              <gml:surfaceMember xlink:href="#UUID_aa25ef67-9253-42f5-873b-00bd282c5ac1"/>
              <gml:surfaceMember xlink:href="#UUID_e7a6500a-42fd-452f-81de-f3e0786c49df"/>
              <gml:surfaceMember xlink:href="#UUID_03c170b1-2e16-4316-8508-b31303ddd930"/>
              <gml:surfaceMember xlink:href="#UUID_6f8ba16b-f424-44cb-9c1a-f1aae3584856"/>
              <gml:surfaceMember xlink:href="#UUID_e2d8dbb2-138f-4ce3-904d-a602f908aa15"/>
              <gml:surfaceMember xlink:href="#UUID_ad5ed1d4-dc6e-4d89-85ec-45bcdd66a8b4"/>
              <gml:surfaceMember xlink:href="#UUID_a6c5d639-00f4-4eae-bd5a-1176c2d53fb8"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_596a883b-ae96-408d-ae49-938d332ab72f">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_f7016349-5abd-4f99-9ce5-b48193943c68" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_5e56cd39-0151-4709-9e08-334d079313e6">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_44d494fb-1279-4c1a-bbbe-497d3f9bac38">
                      <gml:posList>550200.055345483940000000 5804570.780987209600000000 63.774017333984375000 550195.696076384160000000 5804571.214847403600000000 63.774017333984375000 550195.515000000010000000 5804568.607999999100000000 63.774017333984375000 550199.799000000000000000 5804568.176000000000000000 63.774017333984375000 550200.055345483940000000 5804570.780987209600000000 63.774017333984375000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_6af99aea-09e7-45d3-9209-98276c046b1c">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_e3cfef06-1ebb-4b0a-8d8e-e5578ac6ef53" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_755f714e-02f6-416e-9960-1b9a44d853f2">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_a118d3b6-0f58-4866-8a80-00f66b0abeb4">
                      <gml:posList>550191.538930849410000000 5804572.215942920200000000 66.820706929932541000 550195.736999999730000000 5804571.803999999500000000 66.833798093819638000 550195.696076384050000000 5804571.214847403600000000 66.368362835175645000 550200.055345482780000000 5804570.780987209600000000 66.377172162187236000 550200.610080263000000000 5804576.418211569100000000 70.843693363717534000 550185.976335074640000000 5804577.913534953300000000 70.844624918833830000 550185.434000000360000000 5804572.815000000400000000 66.801669451653012000 550186.859814335480000000 5804572.675089460800000000 66.806115678826700000 550187.166320784250000000 5804575.961462065600000000 69.412762540242610000 550188.464419854460000000 5804575.834182648000000000 69.412767417862597000 550191.860070780730000000 5804575.485954317300000000 69.411570757359272000 550191.538930849410000000 5804572.215942920200000000 66.820706929932541000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_f21540dd-32c8-4381-8429-9a5f77ce486b">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_20c431a7-fa1c-4e3a-a789-dd4af6850836" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_d819aac4-af53-4590-964d-c8529b57bcad">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_510ee22d-249c-4f33-a706-e955f919fbc1">
                      <gml:posList>550192.694097525210000000 5804583.122962932100000000 69.305159055318001000 550187.936297518780000000 5804583.604460719000000000 69.305436990424624000 550187.530731470210000000 5804579.636190383700000000 69.489089161906193000 550192.275775943530000000 5804579.150973356300000000 69.489041437912306000 550192.694097525210000000 5804583.122962932100000000 69.305159055318001000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_1257bbde-8e0d-4390-b90b-aaccd07c8629">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_a1ba1b5a-4ead-4cba-953d-2ff8fbb5eb99" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_3e0ad88b-df29-4c16-b112-06f6b777eff6">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_a7c9a343-7bae-4f9b-996b-98e17df82e42">
                      <gml:posList>550192.694097525210000000 5804583.122962932100000000 66.596773314037392000 550192.275775943530000000 5804579.150973356300000000 69.489041437912306000 550187.530731470210000000 5804579.636190383700000000 69.489089161906193000 550187.936297518780000000 5804583.604460719000000000 66.600440194246119000 550186.596999999140000000 5804583.740000000200000000 66.601472403158198000 550185.976335074640000000 5804577.913534953300000000 70.844624918833830000 550200.610080263000000000 5804576.418211569100000000 70.843693363717534000 550200.875999998300000000 5804579.116999999600000000 68.879876241531576000 550198.921999998390000000 5804579.290000000000000000 68.899211016550908000 550199.245000001040000000 5804582.460000000000000000 66.591724473269494000 550192.694097525210000000 5804583.122962932100000000 66.596773314037392000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_b559c90a-b622-4d39-9acd-acded907505d">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_88522e41-46d4-4226-995e-173d1ec96d01" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_ad95420e-c936-4fb8-a649-b12bdd6dbee0">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_41b9dcf7-2e4d-4850-9690-a89c4d50bf08">
                      <gml:posList>550191.860070780730000000 5804575.485954317300000000 69.411570757359272000 550188.464419854460000000 5804575.834182648000000000 69.412767417862597000 550187.166320784250000000 5804575.961462065600000000 69.412762540242610000 550186.859814335480000000 5804572.675089460800000000 69.150310239315587000 550191.538930849410000000 5804572.215942920200000000 69.150299627105127000 550191.860070780730000000 5804575.485954317300000000 69.411570757359272000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_63a9515d-8f2e-4a35-8872-70acc3be1e91">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_8466d8c5-1966-4c9e-a7cd-2e2c04c36d19" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_57e9ec39-6164-4d02-8585-e650ab4cba14">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_f42bd9de-2714-4ac0-8cb9-408da21f1f99">
                      <gml:posList>550191.860070780730000000 5804575.485954317300000000 69.411570757359272000 550191.538930849410000000 5804572.215942920200000000 69.150299627105127000 550191.538930849410000000 5804572.215942920200000000 66.820706929932541000 550191.860070780730000000 5804575.485954317300000000 69.411570757359272000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_dafe078e-2ec6-40c2-b02c-666b8d7e153c">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_69556614-9e1b-4dd8-9b81-5979e2a53ab0" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_b09721da-037e-4acc-ab79-635318241fd1">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_480c27cc-1f4c-4923-87e2-fbe0856c3828">
                      <gml:posList>550187.166320784250000000 5804575.961462065600000000 69.412762540242610000 550186.859814335480000000 5804572.675089460800000000 66.806115678826700000 550186.859814335480000000 5804572.675089460800000000 69.150310239315587000 550187.166320784250000000 5804575.961462065600000000 69.412762540242610000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_d5ee4aa7-5f60-43af-a519-8ba385209fea">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_da267e25-0d00-4e86-8bc5-e0a0968cfb41" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_8a9a69df-00d6-4dad-ab9a-ca70eb966cca">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_8bf5f1d3-b769-4191-b1eb-d5cac63e5bcb">
                      <gml:posList>550192.694097525210000000 5804583.122962932100000000 69.305159055318001000 550192.275775943530000000 5804579.150973356300000000 69.489041437912306000 550192.694097525210000000 5804583.122962932100000000 66.596773314037392000 550192.694097525210000000 5804583.122962932100000000 69.305159055318001000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_65d085e8-b6eb-419c-b67c-9f0642bb77c8">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_b494416f-39fe-4dc9-a770-743240b551dc" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_3cdadd4b-9920-403e-84c6-fa6c545f3ee6">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_6fcd66bf-f18f-4876-b2dd-84e2c1a65491">
                      <gml:posList>550187.936297518780000000 5804583.604460719000000000 69.305436990424624000 550187.936297518780000000 5804583.604460719000000000 66.600440194246119000 550187.530731470210000000 5804579.636190383700000000 69.489089161906193000 550187.936297518780000000 5804583.604460719000000000 69.305436990424624000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_258ebc6e-de91-40f1-b7b1-25d0982ca03c">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_f31142b4-a9bf-4f65-9f97-3ef4e8fd2cd5" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_babc6d0a-7048-4b6a-8ee3-866e901765d6">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_71949192-79ff-471a-b7ad-8fed30c23c4e">
                      <gml:posList>550200.055345482780000000 5804570.780987209600000000 66.377172162187236000 550195.696076384050000000 5804571.214847403600000000 66.368362835175645000 550195.696076384160000000 5804571.214847403600000000 63.774017333984375000 550200.055345483940000000 5804570.780987209600000000 63.774017333984375000 550200.055345482780000000 5804570.780987209600000000 66.377172162187236000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_1626a1d2-4650-4a64-be40-4757acd2edf5">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_4334c42d-7b8d-42af-8d0e-0596b6438bee" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_313071bc-d914-49ae-a0c7-364f284a0011">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_fea0784b-a80c-4fe8-9e1a-05f59b08f5ce">
                      <gml:posList>550186.596999999140000000 5804583.740000000200000000 53.630000000000010000 550185.976335074640000000 5804577.913534953300000000 53.630000000000003000 550185.434000000360000000 5804572.815000000400000000 53.630000000000010000 550185.434000000360000000 5804572.815000000400000000 66.801669451653012000 550185.976335074640000000 5804577.913534953300000000 70.844624918833830000 550186.596999999140000000 5804583.740000000200000000 66.601472403158198000 550186.596999999140000000 5804583.740000000200000000 53.630000000000010000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_23f641b0-801f-4abd-8e7e-e29e4e8d22f1">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_f2ff21d9-fe90-4723-b421-b2a4b5b06f6e" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_4c587783-0b60-4e65-85ef-6d29805711c0">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_950bcaac-7137-41c8-b8af-eeeb603863ed">
                      <gml:posList>550199.245000001040000000 5804582.460000000000000000 66.591724473269494000 550198.921999998390000000 5804579.290000000000000000 68.899211016550908000 550198.921999998390000000 5804579.290000000000000000 53.630000000000010000 550199.245000001040000000 5804582.460000000000000000 53.630000000000010000 550199.245000001040000000 5804582.460000000000000000 66.591724473269494000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_ef8ccbb3-2940-4196-99f9-271286702dc7">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_f7d3634e-e1b9-4b4c-9e0f-88bf24995663" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_aa25ef67-9253-42f5-873b-00bd282c5ac1">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_23d74373-1ba3-4649-b8c6-95d9325241e0">
                      <gml:posList>550200.875999998300000000 5804579.116999999600000000 53.630000000000010000 550198.921999998390000000 5804579.290000000000000000 53.630000000000010000 550198.921999998390000000 5804579.290000000000000000 68.899211016550908000 550200.875999998300000000 5804579.116999999600000000 68.879876241531576000 550200.875999998300000000 5804579.116999999600000000 53.630000000000010000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_112f6426-a87f-46b7-9b34-6651a49007a7">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_573c7cc4-7279-46f1-a442-f5e3f926f4a9" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_e7a6500a-42fd-452f-81de-f3e0786c49df">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_5078cb19-011a-412a-912f-6dae608702fb">
                      <gml:posList>550199.245000001040000000 5804582.460000000000000000 66.591724473269494000 550199.245000001040000000 5804582.460000000000000000 53.630000000000010000 550192.694097525210000000 5804583.122962932100000000 53.630000000000003000 550187.936297518780000000 5804583.604460719000000000 53.630000000000003000 550186.596999999140000000 5804583.740000000200000000 53.630000000000010000 550186.596999999140000000 5804583.740000000200000000 66.601472403158198000 550187.936297518780000000 5804583.604460719000000000 66.600440194246119000 550187.936297518780000000 5804583.604460719000000000 69.305436990424624000 550192.694097525210000000 5804583.122962932100000000 69.305159055318001000 550192.694097525210000000 5804583.122962932100000000 66.596773314037392000 550199.245000001040000000 5804582.460000000000000000 66.591724473269494000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_b85ce6d1-71ed-46b1-9664-45b8753c3439">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_d147fdd0-7b1a-4dca-a3e4-384142b04952" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_03c170b1-2e16-4316-8508-b31303ddd930">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_64f7a7a2-7422-42d7-9d9c-c5c904696282">
                      <gml:posList>550191.538930849410000000 5804572.215942920200000000 66.820706929932541000 550191.538930849410000000 5804572.215942920200000000 69.150299627105127000 550186.859814335480000000 5804572.675089460800000000 69.150310239315587000 550186.859814335480000000 5804572.675089460800000000 66.806115678826700000 550185.434000000360000000 5804572.815000000400000000 66.801669451653012000 550185.434000000360000000 5804572.815000000400000000 53.630000000000010000 550186.859814335480000000 5804572.675089460800000000 53.630000000000003000 550191.538930849410000000 5804572.215942920200000000 53.630000000000003000 550195.736999999730000000 5804571.803999999500000000 53.630000000000010000 550195.736999999730000000 5804571.803999999500000000 66.833798093819638000 550191.538930849410000000 5804572.215942920200000000 66.820706929932541000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_2c40865e-0e2e-4188-9d08-14641c31839a">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_d011b58f-ecf2-46f8-8396-30cbdd866e30" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_6f8ba16b-f424-44cb-9c1a-f1aae3584856">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_9b3bbb4e-c452-42dc-8ab4-92290295f6dd">
                      <gml:posList>550200.875999998300000000 5804579.116999999600000000 68.879876241531576000 550200.610080263000000000 5804576.418211569100000000 70.843693363717534000 550200.055345482780000000 5804570.780987209600000000 66.377172162187236000 550200.055345483940000000 5804570.780987209600000000 63.774017333984375000 550199.799000000000000000 5804568.176000000000000000 63.774017333984375000 550199.799000000000000000 5804568.176000000000000000 53.630000000000010000 550200.055345483940000000 5804570.780987209600000000 53.630000000000003000 550200.610080263000000000 5804576.418211569100000000 53.630000000000003000 550200.875999998300000000 5804579.116999999600000000 53.630000000000010000 550200.875999998300000000 5804579.116999999600000000 68.879876241531576000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_396c7f68-c9f3-453e-8f9b-080b345cd5b0">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_5ad21278-cf61-4101-b1cd-f7cd5c04334c" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_e2d8dbb2-138f-4ce3-904d-a602f908aa15">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_d1155ea9-cb75-4d80-9b11-1b0d41331d3a">
                      <gml:posList>550199.799000000000000000 5804568.176000000000000000 63.774017333984375000 550195.515000000010000000 5804568.607999999100000000 63.774017333984375000 550195.515000000010000000 5804568.607999999100000000 53.630000000000010000 550199.799000000000000000 5804568.176000000000000000 53.630000000000010000 550199.799000000000000000 5804568.176000000000000000 63.774017333984375000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_6ad91ec6-d475-42b5-9fee-c700a5c44c9e">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_d7c39b1d-34d5-46e3-9667-d023c86cff9a" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_ad5ed1d4-dc6e-4d89-85ec-45bcdd66a8b4">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_0264e278-d722-4f2b-8145-35c8ed1d75c4">
                      <gml:posList>550195.736999999730000000 5804571.803999999500000000 66.833798093819638000 550195.736999999730000000 5804571.803999999500000000 53.630000000000010000 550195.696076384160000000 5804571.214847403600000000 53.630000000000010000 550195.515000000010000000 5804568.607999999100000000 53.630000000000010000 550195.515000000010000000 5804568.607999999100000000 63.774017333984375000 550195.696076384160000000 5804571.214847403600000000 63.774017333984375000 550195.696076384050000000 5804571.214847403600000000 66.368362835175645000 550195.736999999730000000 5804571.803999999500000000 66.833798093819638000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="UUID_ca52def8-6ed0-4447-8589-21adc9ad1673">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_f8f43ffe-f036-44ff-9306-128d86514782" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_a6c5d639-00f4-4eae-bd5a-1176c2d53fb8">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_0900325c-b0ce-48e5-b453-d1052f09126c">
                      <gml:posList>550200.055345483940000000 5804570.780987209600000000 53.630000000000003000 550199.799000000000000000 5804568.176000000000000000 53.630000000000010000 550195.515000000010000000 5804568.607999999100000000 53.630000000000010000 550195.696076384160000000 5804571.214847403600000000 53.630000000000010000 550195.736999999730000000 5804571.803999999500000000 53.630000000000010000 550191.538930849410000000 5804572.215942920200000000 53.630000000000003000 550186.859814335480000000 5804572.675089460800000000 53.630000000000003000 550185.434000000360000000 5804572.815000000400000000 53.630000000000010000 550185.976335074640000000 5804577.913534953300000000 53.630000000000003000 550186.596999999140000000 5804583.740000000200000000 53.630000000000010000 550187.936297518780000000 5804583.604460719000000000 53.630000000000003000 550192.694097525210000000 5804583.122962932100000000 53.630000000000003000 550199.245000001040000000 5804582.460000000000000000 53.630000000000010000 550198.921999998390000000 5804579.290000000000000000 53.630000000000010000 550200.875999998300000000 5804579.116999999600000000 53.630000000000010000 550200.610080263000000000 5804576.418211569100000000 53.630000000000003000 550200.055345483940000000 5804570.780987209600000000 53.630000000000003000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </cityObjectMember>

  <cityObjectMember>
    <bldg:Building gml:id="DENIAL43000053mu">
      <gml:boundedBy>
        <gml:Envelope srsDimension="3" srsName="urn:adv:crs:ETRS89_UTM32*DE_DHHN92_NH">
          <gml:lowerCorner>550170.000000 5804572.000000 53.039997
</gml:lowerCorner>
          <gml:upperCorner>550186.750000 5804585.000000 70.922157
</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <creationDate>2017-11-21</creationDate>
      <gen:doubleAttribute name="Volume">
        <gen:value>2499.348</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Profile">
        <gen:value>Sig3d</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCK">
        <gen:value>10</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOECKE">
        <gen:value>111010</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STBEZNR">
        <gen:value>111</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="MBZNR">
        <gen:value>1102</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="WBZNR">
        <gen:value>1102</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNAM">
        <gen:value>Vahrenwald</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTNR">
        <gen:value>11</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNAM">
        <gen:value>Vahrenwald-List</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBEZ">
        <gen:value>02</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEMEINDE">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SCALE">
        <gen:value>5000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SHAPE_Area">
        <gen:value>7165.30049704715</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ALKISID">
        <gen:value>DENIAL43000053mu</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ZMIN">
        <gen:value>53.04</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="THICKNESS">
        <gen:value>0.1</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="TEXTURE">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="BUILDINGID">
        <gen:value>DENIAL43000053mu</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ARTBEZEICH">
        <gen:value>Gebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Join_Cou_1">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="FUNKTION_T">
        <gen:value>Wohngebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="OBJFLAECHE">
        <gen:value>156.51</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADT">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEB_Funkti">
        <gen:value>1000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STRSCHL">
        <gen:value>00712</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STR_komple">
        <gen:value>Dörnbergstraße 3</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="PLZ_NUMMER">
        <gen:value>30161</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STR">
        <gen:value>Dörnbergstraße</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="HAUSNR">
        <gen:value>3</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="OVERHANG">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <app:appearance>
        <app:Appearance>
          <app:theme>RhinoCity ObliqueTexturing</app:theme>
          <app:surfaceDataMember>
            <app:ParameterizedTexture>
              <app:imageURI>Hannover_teil1_Appearance/DENIAL43000053mu.jpg</app:imageURI>
              <app:textureType>specific</app:textureType>
              <app:wrapMode>border</app:wrapMode>
              <app:borderColor>0 0 0 1</app:borderColor>
              <app:target uri="#UUID_84f8ea35-4b92-41db-80e3-2d979a970ac2">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_b2337318-af82-4fa3-8c42-25a956b9087d">0.221621 0.620824 0.012639 0.642004 0.001957 0.578391 0.210862 0.557730 0.221621 0.620824 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_be2c46d0-bef6-4da8-b8cc-817e3beb4720">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_514fde17-4a6c-40fb-bba3-81f602dfe64e">0.235224 0.630875 0.227006 0.567830 0.229799 0.557730 0.235224 0.630875 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_9e77f5ea-3e2a-4964-a943-794560667853">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_4175f302-74c0-45ed-afcf-cd68c2e59c13">0.517662 0.567339 0.330979 0.582488 0.328767 0.572800 0.515393 0.557730 0.517662 0.567339 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_61856807-9ac5-46d9-8787-33be39c89875">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_32061a1f-b4a1-41ce-bc1a-580c54780ef1">0.323790 0.620875 0.311155 0.558876 0.314360 0.557730 0.323790 0.620875 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_f94e49d1-2d8e-43c8-8ef5-97c540775c81">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_114dfc13-2951-4a63-9c04-d18f96bbce36">0.356904 0.523555 0.012794 0.553402 0.001957 0.402021 0.346324 0.373777 0.356904 0.523555 </app:textureCoordinates>
                  <app:textureCoordinates ring="#UUID_09176eda-af2a-49ac-b43c-a13a52403fce">0.269179 0.417694 0.081660 0.433748 0.086166 0.508705 0.273627 0.492086 0.269179 0.417694 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_f768b483-a2cf-4e12-8649-4ea90210a2af">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_742da198-ca71-4c49-9bf7-b96c1129f9fb">0.852667 0.468432 0.762576 0.476829 0.755382 0.382981 0.845434 0.373777 0.852667 0.468432 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_23223793-c4ff-4122-96c9-ca7dbfff972e">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_16bba434-9473-4825-b57f-94920b0dfc2d">0.954359 0.466966 0.864267 0.475360 0.857143 0.382978 0.947195 0.373777 0.954359 0.466966 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_3a0e4867-0e81-4baf-a1c1-e6c99053267d">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_67cda536-a21d-43ae-865c-68b57c7cae49">0.576176 0.532915 0.511713 0.538870 0.515491 0.441836 0.424097 0.451113 0.420538 0.547292 0.362035 0.552696 0.365453 0.412604 0.750899 0.373777 0.746163 0.517213 0.667346 0.524494 0.671509 0.425943 0.580114 0.435219 0.576176 0.532915 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_99dec5ea-d6c7-45d4-a73f-003ce397932e">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_4ed9e4fc-ad7c-49a6-ba06-7c34d2ee1fb0">0.261403 0.624613 0.240705 0.557730 0.270947 0.594160 0.261403 0.624613 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_507d9efa-c73e-40a2-ac68-5afe437d8f85">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_8a59ef4d-9cd1-45e9-8280-0e5ae6d75117">0.981639 0.466053 0.972603 0.469056 0.975583 0.373777 0.981639 0.466053 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_b15fd946-b8eb-48a6-86d8-3f846d3af391">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_12b1a8b7-339e-4009-9455-55e74d57b173">0.296737 0.625675 0.275930 0.557730 0.306276 0.594762 0.296737 0.625675 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_b242ffc4-1f2f-4e25-b855-6f42da16a624">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_d05b26e7-90a0-42e4-ba05-0468d52286e1">0.968318 0.467521 0.958904 0.470598 0.962202 0.373777 0.968318 0.467521 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_3f48ad20-26b3-4382-b2c8-4c47f1aa4fb0">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_58068050-b5ba-471b-b7fc-4024bfdb674a">0.961566 0.162010 0.617083 0.190270 0.583170 0.030676 0.925384 0.001957 0.961566 0.162010 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_b48e0d10-4ec4-41c2-be3a-da2f9b0ad319">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_89bddbd4-78ed-4dd4-bc98-91cce5734719">0.145162 0.286151 0.131185 0.152200 0.117417 0.020244 0.171757 0.001957 0.201947 0.130060 0.198588 0.270712 0.145162 0.286151 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_7748ef88-61f0-49c7-a85c-cf6929381c2d">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_7a443846-dee7-4ead-a81e-3f97c850b037">0.063648 0.368545 0.016400 0.313422 0.001957 0.163083 0.053072 0.001957 0.082148 0.104613 0.112559 0.211987 0.063648 0.368545 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_e1471528-038f-48df-9374-7fbe05921c62">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_d9d3dd52-e3b6-4194-8d0d-98d985b58ddc">0.317977 0.184306 0.250813 0.189205 0.207436 0.027008 0.274147 0.021870 0.351327 0.015925 0.405910 0.011721 0.483125 0.005774 0.532679 0.001957 0.578279 0.165318 0.528384 0.168958 0.537252 0.200930 0.459496 0.206882 0.450640 0.174629 0.395684 0.178638 0.404536 0.211108 0.326814 0.217057 0.317977 0.184306 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
            </app:ParameterizedTexture>
          </app:surfaceDataMember>
        </app:Appearance>
      </app:appearance>
      <bldg:measuredHeight uom="#m">17.882</bldg:measuredHeight>
      <bldg:lod1Solid>
        <gml:Solid gml:id="UUID_c5d6c32f-59fc-412f-9849-cc99d5b38220">
          <gml:exterior>
            <gml:CompositeSurface gml:id="UUID_9207a4cc-6a4c-40a2-b3aa-1fd4d178f628">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_a0ba8639-a934-470d-9841-460212552716">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_12048a41-0ae2-46aa-9cde-648b79763598">
                      <gml:posList>550186.546000000090000000 5804583.269000000300000000 53.039999999999999000 550183.495743287960000000 5804583.555660520700000000 53.039999999999999000 550179.967576326800000000 5804583.887234637500000000 53.039999999999999000 550177.472971509330000000 5804584.121675467100000000 53.039999999999999000 550173.944804548170000000 5804584.453249584000000000 53.039999999999999000 550171.681000001730000000 5804584.666000000200000000 53.039999999999999000 550171.681000001730000000 5804584.666000000200000000 70.922151693322647000 550173.944804548170000000 5804584.453249584000000000 70.922151693322647000 550177.472971509330000000 5804584.121675467100000000 70.922151693322647000 550179.967576326800000000 5804583.887234637500000000 70.922151693322647000 550183.495743287960000000 5804583.555660520700000000 70.922151693322647000 550186.546000000090000000 5804583.269000000300000000 70.922151693322647000 550186.546000000090000000 5804583.269000000300000000 53.039999999999999000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_23ed6e50-86a7-4826-96aa-4a87fc400989">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_e0869fd5-47d1-4c33-979c-3f086f542487">
                      <gml:posList>550186.546000000090000000 5804583.269000000300000000 70.922151693322647000 550185.976340737430000000 5804577.913588189500000000 70.922151693322647000 550185.434000000360000000 5804572.815000000400000000 70.922151693322647000 550185.434000000360000000 5804572.815000000400000000 53.039999999999999000 550185.976340737430000000 5804577.913588189500000000 53.039999999999999000 550186.546000000090000000 5804583.269000000300000000 53.039999999999999000 550186.546000000090000000 5804583.269000000300000000 70.922151693322647000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_2cfef54a-09a7-47f1-ac07-c819740f6abd">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_888a7071-8838-4b1f-9ff6-ce444e9a5a95">
                      <gml:posList>550185.434000000360000000 5804572.815000000400000000 70.922151693322647000 550170.570999998600000000 5804574.275000000400000000 70.922151693322647000 550170.570999998600000000 5804574.275000000400000000 53.039999999999999000 550185.434000000360000000 5804572.815000000400000000 53.039999999999999000 550185.434000000360000000 5804572.815000000400000000 70.922151693322647000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_69e28aea-e01f-4a65-bd0f-0d3244327d40">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_0e23b13c-0494-4f84-be97-88c7b55778a1">
                      <gml:posList>550171.681000001730000000 5804584.666000000200000000 53.039999999999999000 550171.121836362870000000 5804579.431523104200000000 53.039999999999999000 550170.570999998600000000 5804574.275000000400000000 53.039999999999999000 550170.570999998600000000 5804574.275000000400000000 70.922151693322647000 550171.121836362870000000 5804579.431523104200000000 70.922151693322647000 550171.681000001730000000 5804584.666000000200000000 70.922151693322647000 550171.681000001730000000 5804584.666000000200000000 53.039999999999999000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_baaadbed-526a-4b23-ad11-5a136a04c671">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_874fab45-5a3f-4623-83c4-3c1b910cd890">
                      <gml:posList>550183.495743287960000000 5804583.555660520700000000 53.039999999999999000 550186.546000000090000000 5804583.269000000300000000 53.039999999999999000 550185.976340737430000000 5804577.913588189500000000 53.039999999999999000 550185.434000000360000000 5804572.815000000400000000 53.039999999999999000 550170.570999998600000000 5804574.275000000400000000 53.039999999999999000 550171.121836362870000000 5804579.431523104200000000 53.039999999999999000 550171.681000001730000000 5804584.666000000200000000 53.039999999999999000 550173.944804548170000000 5804584.453249584000000000 53.039999999999999000 550177.472971509330000000 5804584.121675467100000000 53.039999999999999000 550179.967576326800000000 5804583.887234637500000000 53.039999999999999000 550183.495743287960000000 5804583.555660520700000000 53.039999999999999000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_24e66cf5-d6d2-4846-8e92-3908581f0891">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_bfecabdb-5c84-480b-8f16-90612d458845">
                      <gml:posList>550179.967576326800000000 5804583.887234637500000000 70.922151693322647000 550177.472971509330000000 5804584.121675467100000000 70.922151693322647000 550173.944804548170000000 5804584.453249584000000000 70.922151693322647000 550171.681000001730000000 5804584.666000000200000000 70.922151693322647000 550171.121836362870000000 5804579.431523104200000000 70.922151693322647000 550170.570999998600000000 5804574.275000000400000000 70.922151693322647000 550185.434000000360000000 5804572.815000000400000000 70.922151693322647000 550185.976340737430000000 5804577.913588189500000000 70.922151693322647000 550186.546000000090000000 5804583.269000000300000000 70.922151693322647000 550183.495743287960000000 5804583.555660520700000000 70.922151693322647000 550179.967576326800000000 5804583.887234637500000000 70.922151693322647000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod1Solid>
      <bldg:lod2Solid>
        <gml:Solid gml:id="UUID_d04d9539-c405-4891-b4a3-abfd03b35ced">
          <gml:exterior>
            <gml:CompositeSurface gml:id="UUID_9671d35c-1564-441b-b1fe-7d247b21a2b6">
              <gml:surfaceMember xlink:href="#UUID_84f8ea35-4b92-41db-80e3-2d979a970ac2"/>
              <gml:surfaceMember xlink:href="#UUID_be2c46d0-bef6-4da8-b8cc-817e3beb4720"/>
              <gml:surfaceMember xlink:href="#UUID_9e77f5ea-3e2a-4964-a943-794560667853"/>
              <gml:surfaceMember xlink:href="#UUID_61856807-9ac5-46d9-8787-33be39c89875"/>
              <gml:surfaceMember xlink:href="#UUID_f94e49d1-2d8e-43c8-8ef5-97c540775c81"/>
              <gml:surfaceMember xlink:href="#UUID_f768b483-a2cf-4e12-8649-4ea90210a2af"/>
              <gml:surfaceMember xlink:href="#UUID_23223793-c4ff-4122-96c9-ca7dbfff972e"/>
              <gml:surfaceMember xlink:href="#UUID_3a0e4867-0e81-4baf-a1c1-e6c99053267d"/>
              <gml:surfaceMember xlink:href="#UUID_99dec5ea-d6c7-45d4-a73f-003ce397932e"/>
              <gml:surfaceMember xlink:href="#UUID_507d9efa-c73e-40a2-ac68-5afe437d8f85"/>
              <gml:surfaceMember xlink:href="#UUID_b15fd946-b8eb-48a6-86d8-3f846d3af391"/>
              <gml:surfaceMember xlink:href="#UUID_b242ffc4-1f2f-4e25-b855-6f42da16a624"/>
              <gml:surfaceMember xlink:href="#UUID_3f48ad20-26b3-4382-b2c8-4c47f1aa4fb0"/>
              <gml:surfaceMember xlink:href="#UUID_b48e0d10-4ec4-41c2-be3a-da2f9b0ad319"/>
              <gml:surfaceMember xlink:href="#UUID_7748ef88-61f0-49c7-a85c-cf6929381c2d"/>
              <gml:surfaceMember xlink:href="#UUID_e1471528-038f-48df-9374-7fbe05921c62"/>
              <gml:surfaceMember xlink:href="#UUID_50159d03-85a9-4671-b04b-b31e049922f3"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:outerBuildingInstallation>
        <bldg:BuildingInstallation>
          <bldg:boundedBy>
            <bldg:RoofSurface gml:id="UUID_f942fd25-e9fd-4791-87d8-a14259bc05a2">
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_92e198f8-d4cd-4753-a68d-a81b3b8adbee" srsDimension="3">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="UUID_84f8ea35-4b92-41db-80e3-2d979a970ac2">
                      <gml:exterior>
                        <gml:LinearRing gml:id="UUID_b2337318-af82-4fa3-8c42-25a956b9087d">
                          <gml:posList>550182.361309726960000000 5804576.959232983200000000 69.813096864046983000 550174.269629755060000000 5804577.797944862400000000 69.821988727708998000 550174.033656846150000000 5804575.243810867900000000 68.695821489780840000 550182.127200348880000000 5804574.425269555300000000 68.695823220289569000 550182.361309726960000000 5804576.959232983200000000 69.813096864046983000 </gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:RoofSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_9442cd65-a597-4557-a4d6-f829b8e283dd">
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_a23df25a-434f-405c-b2a9-eb9ed62a407d" srsDimension="3">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="UUID_be2c46d0-bef6-4da8-b8cc-817e3beb4720">
                      <gml:exterior>
                        <gml:LinearRing gml:id="UUID_514fde17-4a6c-40fb-bba3-81f602dfe64e">
                          <gml:posList>550182.361309726960000000 5804576.959232983200000000 69.813096864046983000 550182.127200348880000000 5804574.425269555300000000 68.695823220289569000 550182.127200348880000000 5804574.425269555300000000 67.820474941843003000 550182.361309726960000000 5804576.959232983200000000 69.813096864046983000 </gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_a8f31b4a-4348-4e5c-8c8f-b892504557a3">
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_ba681dda-d737-4033-8e1e-2cc07ee59da1" srsDimension="3">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="UUID_9e77f5ea-3e2a-4964-a943-794560667853">
                      <gml:exterior>
                        <gml:LinearRing gml:id="UUID_4175f302-74c0-45ed-afcf-cd68c2e59c13">
                          <gml:posList>550182.127200348880000000 5804574.425269555300000000 68.695823220289569000 550174.033656846150000000 5804575.243810867900000000 68.695821489780840000 550174.033656846150000000 5804575.243810867900000000 67.813505363851192000 550182.127200348880000000 5804574.425269555300000000 67.820474941843003000 550182.127200348880000000 5804574.425269555300000000 68.695823220289569000 </gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_26317b40-a9d5-4aa9-a84a-aac018880e1f">
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_4211b46b-605b-400d-bf24-76e63796d038" srsDimension="3">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="UUID_61856807-9ac5-46d9-8787-33be39c89875">
                      <gml:exterior>
                        <gml:LinearRing gml:id="UUID_32061a1f-b4a1-41ce-bc1a-580c54780ef1">
                          <gml:posList>550174.269629755060000000 5804577.797944862400000000 69.821988727708998000 550174.033656846150000000 5804575.243810867900000000 67.813505363851192000 550174.033656846150000000 5804575.243810867900000000 68.695821489780840000 550174.269629755060000000 5804577.797944862400000000 69.821988727708998000 </gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
        </bldg:BuildingInstallation>
      </bldg:outerBuildingInstallation>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_7cca7f89-22b4-4c74-9608-ed596a5f80c9">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_124a82ec-4f80-455b-aa57-dac98ec5d7b7" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_f94e49d1-2d8e-43c8-8ef5-97c540775c81">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_114dfc13-2951-4a63-9c04-d18f96bbce36">
                      <gml:posList>550185.976340737430000000 5804577.913588189500000000 70.844470380565923000 550171.121836362870000000 5804579.431523104200000000 70.843848643017594000 550170.570999998600000000 5804574.275000000400000000 66.783007479323317000 550185.434000000360000000 5804572.815000000400000000 66.829436917447637000 550185.976340737430000000 5804577.913588189500000000 70.844470380565923000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                  <gml:interior>
                    <gml:LinearRing gml:id="UUID_09176eda-af2a-49ac-b43c-a13a52403fce">
                      <gml:posList>550182.127200348880000000 5804574.425269555300000000 67.820474941843003000 550174.033656846150000000 5804575.243810867900000000 67.813505363851192000 550174.269629755060000000 5804577.797944862400000000 69.821988727708998000 550182.361309726960000000 5804576.959232983200000000 69.813096864046983000 550182.127200348880000000 5804574.425269555300000000 67.820474941843003000 </gml:posList>
                    </gml:LinearRing>
                  </gml:interior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_765f7294-1764-4bf8-a730-d55124d2e03e">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_0475d875-d0ae-4c78-a351-beef9df6e960" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_f768b483-a2cf-4e12-8649-4ea90210a2af">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_742da198-ca71-4c49-9bf7-b96c1129f9fb">
                      <gml:posList>550183.495743287960000000 5804583.555660520700000000 69.088012009125336000 550179.967576326800000000 5804583.887234637500000000 69.088930761995755000 550179.620751749610000000 5804580.234758632300000000 69.468709627319129000 550183.145933457650000000 5804579.871869994300000000 69.471047027272874000 550183.495743287960000000 5804583.555660520700000000 69.088012009125336000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_d44ef154-fcd1-4853-a409-7286434ead51">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_10a8b2f5-a036-4567-afd3-e75618bcc564" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_23223793-c4ff-4122-96c9-ca7dbfff972e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_16bba434-9473-4825-b57f-94920b0dfc2d">
                      <gml:posList>550177.472971509330000000 5804584.121675467100000000 69.088012009125336000 550173.944804548170000000 5804584.453249584000000000 69.088930761995755000 550173.603260997680000000 5804580.856389082000000000 69.462926812384325000 550177.128442881510000000 5804580.493500426400000000 69.465264212454571000 550177.472971509330000000 5804584.121675467100000000 69.088012009125336000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_db488b68-bfd3-44db-9991-6321d62d2b6d">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_621a5e40-8f6c-40f3-9ddd-ba444abc8b76" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_3a0e4867-0e81-4baf-a1c1-e6c99053267d">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_67cda536-a21d-43ae-865c-68b57c7cae49">
                      <gml:posList>550179.967576326800000000 5804583.887234637500000000 66.434285383666278000 550177.472971509330000000 5804584.121675467100000000 66.451027885980437000 550177.128442881510000000 5804580.493500426400000000 69.465264212454571000 550173.603260997680000000 5804580.856389082000000000 69.462926812384325000 550173.944804548170000000 5804584.453249584000000000 66.474707124911589000 550171.681000001730000000 5804584.666000000200000000 66.489900614714230000 550171.121836362870000000 5804579.431523104200000000 70.843848643017594000 550185.976340737430000000 5804577.913588189500000000 70.844470380565923000 550186.546000000090000000 5804583.269000000300000000 66.390134393175742000 550183.495743287960000000 5804583.555660520700000000 66.410606144735127000 550183.145933457650000000 5804579.871869994300000000 69.471047027272874000 550179.620751749610000000 5804580.234758632300000000 69.468709627319129000 550179.967576326800000000 5804583.887234637500000000 66.434285383666278000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_6ab5f924-c9bf-43b0-9a8a-c548031994cd">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_2b63a5a8-b3fa-43ad-b44c-501b9977b195" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_99dec5ea-d6c7-45d4-a73f-003ce397932e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_4ed9e4fc-ad7c-49a6-ba06-7c34d2ee1fb0">
                      <gml:posList>550177.472971509330000000 5804584.121675467100000000 69.088012009125336000 550177.128442881510000000 5804580.493500426400000000 69.465264212454571000 550177.472971509330000000 5804584.121675467100000000 66.451027885980437000 550177.472971509330000000 5804584.121675467100000000 69.088012009125336000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_ada14a1d-1e03-45ce-8eb8-3d38d752caf3">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_fb878e11-3706-41f2-b59e-7950bc013cef" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_507d9efa-c73e-40a2-ac68-5afe437d8f85">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_8a59ef4d-9cd1-45e9-8280-0e5ae6d75117">
                      <gml:posList>550173.944804548170000000 5804584.453249584000000000 69.088930761995755000 550173.944804548170000000 5804584.453249584000000000 66.474707124911589000 550173.603260997680000000 5804580.856389082000000000 69.462926812384325000 550173.944804548170000000 5804584.453249584000000000 69.088930761995755000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_502de9af-67da-443f-8d03-8c19088b5db4">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_6475a002-4c63-407c-a7c4-cf78f4f74120" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_b15fd946-b8eb-48a6-86d8-3f846d3af391">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_12b1a8b7-339e-4009-9455-55e74d57b173">
                      <gml:posList>550183.495743287960000000 5804583.555660520700000000 69.088012009125336000 550183.145933457650000000 5804579.871869994300000000 69.471047027272874000 550183.495743287960000000 5804583.555660520700000000 66.410606144735127000 550183.495743287960000000 5804583.555660520700000000 69.088012009125336000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_a6bf15d4-9814-4b65-a688-c16a7576a6d1">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_11fdb685-673a-46ed-b246-4a15e19dc572" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_b242ffc4-1f2f-4e25-b855-6f42da16a624">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_d05b26e7-90a0-42e4-ba05-0468d52286e1">
                      <gml:posList>550179.967576326800000000 5804583.887234637500000000 69.088930761995755000 550179.967576326800000000 5804583.887234637500000000 66.434285383666278000 550179.620751749610000000 5804580.234758632300000000 69.468709627319129000 550179.967576326800000000 5804583.887234637500000000 69.088930761995755000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_96ce6ef5-0cb3-4122-a464-fc812c8e9d06">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_13a4813a-c7e7-4ef1-b25d-18801d9d1063" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_3f48ad20-26b3-4382-b2c8-4c47f1aa4fb0">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_58068050-b5ba-471b-b7fc-4024bfdb674a">
                      <gml:posList>550185.434000000360000000 5804572.815000000400000000 66.829436917447637000 550170.570999998600000000 5804574.275000000400000000 66.783007479323317000 550170.570999998600000000 5804574.275000000400000000 53.039999999999999000 550185.434000000360000000 5804572.815000000400000000 53.039999999999999000 550185.434000000360000000 5804572.815000000400000000 66.829436917447637000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_a688bfdc-c7d4-484d-b1ba-11e2bc746b4a">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_ab75ee66-5d44-437c-b9b3-c5f55b1a9ce7" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_b48e0d10-4ec4-41c2-be3a-da2f9b0ad319">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_89bddbd4-78ed-4dd4-bc98-91cce5734719">
                      <gml:posList>550171.681000001730000000 5804584.666000000200000000 53.039999999999999000 550171.121836362870000000 5804579.431523104200000000 53.039999999999999000 550170.570999998600000000 5804574.275000000400000000 53.039999999999999000 550170.570999998600000000 5804574.275000000400000000 66.783007479323317000 550171.121836362870000000 5804579.431523104200000000 70.843848643017594000 550171.681000001730000000 5804584.666000000200000000 66.489900614714230000 550171.681000001730000000 5804584.666000000200000000 53.039999999999999000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_8a7fb2b6-4068-423a-8859-56aad58b0ba8">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_e60c8c64-14ca-4633-975e-6b13748cd667" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_7748ef88-61f0-49c7-a85c-cf6929381c2d">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_7a443846-dee7-4ead-a81e-3f97c850b037">
                      <gml:posList>550186.546000000090000000 5804583.269000000300000000 66.390134393175742000 550185.976340737430000000 5804577.913588189500000000 70.844470380565923000 550185.434000000360000000 5804572.815000000400000000 66.829436917447637000 550185.434000000360000000 5804572.815000000400000000 53.039999999999999000 550185.976340737430000000 5804577.913588189500000000 53.039999999999999000 550186.546000000090000000 5804583.269000000300000000 53.039999999999999000 550186.546000000090000000 5804583.269000000300000000 66.390134393175742000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_154c55dc-be00-4bb4-a147-e74814117c06">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_e65beef0-b042-480c-88f6-fc31a278d573" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_e1471528-038f-48df-9374-7fbe05921c62">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_d9d3dd52-e3b6-4194-8d0d-98d985b58ddc">
                      <gml:posList>550183.495743287960000000 5804583.555660520700000000 66.410606144735127000 550186.546000000090000000 5804583.269000000300000000 66.390134393175742000 550186.546000000090000000 5804583.269000000300000000 53.039999999999999000 550183.495743287960000000 5804583.555660520700000000 53.039999999999999000 550179.967576326800000000 5804583.887234637500000000 53.039999999999999000 550177.472971509330000000 5804584.121675467100000000 53.039999999999999000 550173.944804548170000000 5804584.453249584000000000 53.039999999999999000 550171.681000001730000000 5804584.666000000200000000 53.039999999999999000 550171.681000001730000000 5804584.666000000200000000 66.489900614714230000 550173.944804548170000000 5804584.453249584000000000 66.474707124911589000 550173.944804548170000000 5804584.453249584000000000 69.088930761995755000 550177.472971509330000000 5804584.121675467100000000 69.088012009125336000 550177.472971509330000000 5804584.121675467100000000 66.451027885980437000 550179.967576326800000000 5804583.887234637500000000 66.434285383666278000 550179.967576326800000000 5804583.887234637500000000 69.088930761995755000 550183.495743287960000000 5804583.555660520700000000 69.088012009125336000 550183.495743287960000000 5804583.555660520700000000 66.410606144735127000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="UUID_dabb244f-66db-4503-b7f9-0163e2200d5d">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_6a2858b8-5411-40eb-9748-2750c15adb80" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_50159d03-85a9-4671-b04b-b31e049922f3">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_3e5a9284-b700-4b16-9e10-5e3dd3145754">
                      <gml:posList>550183.495743287960000000 5804583.555660520700000000 53.039999999999999000 550186.546000000090000000 5804583.269000000300000000 53.039999999999999000 550185.976340737430000000 5804577.913588189500000000 53.039999999999999000 550185.434000000360000000 5804572.815000000400000000 53.039999999999999000 550170.570999998600000000 5804574.275000000400000000 53.039999999999999000 550171.121836362870000000 5804579.431523104200000000 53.039999999999999000 550171.681000001730000000 5804584.666000000200000000 53.039999999999999000 550173.944804548170000000 5804584.453249584000000000 53.039999999999999000 550177.472971509330000000 5804584.121675467100000000 53.039999999999999000 550179.967576326800000000 5804583.887234637500000000 53.039999999999999000 550183.495743287960000000 5804583.555660520700000000 53.039999999999999000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </cityObjectMember>

  <cityObjectMember>
    <bldg:Building gml:id="DENIAL43000053ub">
      <gml:boundedBy>
        <gml:Envelope srsDimension="3" srsName="urn:adv:crs:ETRS89_UTM32*DE_DHHN92_NH">
          <gml:lowerCorner>550155.437500 5804573.500000 53.039997
</gml:lowerCorner>
          <gml:upperCorner>550172.062500 5804587.000000 70.941177
</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <creationDate>2017-11-21</creationDate>
      <gen:doubleAttribute name="Volume">
        <gen:value>2421.178</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Profile">
        <gen:value>Sig3d</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCK">
        <gen:value>10</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOECKE">
        <gen:value>111010</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STBEZNR">
        <gen:value>111</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="MBZNR">
        <gen:value>1102</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="WBZNR">
        <gen:value>1102</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNAM">
        <gen:value>Vahrenwald</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTNR">
        <gen:value>11</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNAM">
        <gen:value>Vahrenwald-List</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBEZ">
        <gen:value>02</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEMEINDE">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SCALE">
        <gen:value>5000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SHAPE_Area">
        <gen:value>7165.30049704715</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ALKISID">
        <gen:value>DENIAL43000053ub</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ZMIN">
        <gen:value>53.04</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="THICKNESS">
        <gen:value>0.1</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="TEXTURE">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="BUILDINGID">
        <gen:value>DENIAL43000053ub</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ARTBEZEICH">
        <gen:value>Gebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Join_Cou_1">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="FUNKTION_T">
        <gen:value>Wohngebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="OBJFLAECHE">
        <gen:value>152.36</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADT">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEB_Funkti">
        <gen:value>1000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STRSCHL">
        <gen:value>00712</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STR_komple">
        <gen:value>Dörnbergstraße 4</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="PLZ_NUMMER">
        <gen:value>30161</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STR">
        <gen:value>Dörnbergstraße</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="HAUSNR">
        <gen:value>4</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="OVERHANG">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <app:appearance>
        <app:Appearance>
          <app:theme>RhinoCity ObliqueTexturing</app:theme>
          <app:surfaceDataMember>
            <app:ParameterizedTexture>
              <app:imageURI>Hannover_teil1_Appearance/DENIAL43000053ub.jpg</app:imageURI>
              <app:textureType>specific</app:textureType>
              <app:wrapMode>border</app:wrapMode>
              <app:borderColor>0 0 0 1</app:borderColor>
              <app:target uri="#UUID_fa7b4b1c-b1d2-41c0-9b3d-e07d200e69d5">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_a95a7bfb-2f2e-433d-84fe-91c205b91b8b">0.348486 0.529407 0.011924 0.556554 0.001957 0.406188 0.337593 0.377691 0.348486 0.529407 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_43070a72-a579-4b0c-a75f-ac6a201ccde2">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_58815da5-e69a-40f8-991c-4468fb7d721e">0.856366 0.461299 0.743622 0.471755 0.737769 0.388277 0.850570 0.377691 0.856366 0.461299 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_f8153523-9df2-4808-b0f6-b314fa4e1d66">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_0d9b1c01-8b13-4c4c-8c5a-6b25f4305a63">0.505130 0.451732 0.391068 0.462415 0.389345 0.548911 0.354207 0.552162 0.356074 0.413073 0.733783 0.377691 0.731922 0.517211 0.503183 0.538377 0.505130 0.451732 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_6ad19a7b-6a38-4ed8-b97b-aa4cb16edc0c">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_cc83d9f5-fe63-4b5e-90de-7c8bd6b72d1a">0.890050 0.435686 0.870842 0.377691 0.897454 0.412831 0.890050 0.435686 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_0b8fbdcc-99e7-4df6-b84b-a5f81e04e7e8">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_7af003a2-483b-46a0-938d-aca86878be11">0.866818 0.460980 0.861057 0.463204 0.862356 0.377691 0.866818 0.460980 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_b8d13ccc-cec1-43d4-8c4b-63327fed2e4f">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_7b4a85d9-a107-45d6-8972-c291982c2a2d">0.944882 0.161275 0.609146 0.189785 0.577299 0.029972 0.911030 0.001957 0.944882 0.161275 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_f96b13ea-a003-49a5-9a34-910ee143c5a6">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_fb1a1aed-dcfb-4676-90e5-4ed6b13f0777">0.144912 0.284433 0.132020 0.150901 0.119374 0.019919 0.170234 0.001957 0.198182 0.129193 0.196351 0.268865 0.144912 0.284433 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_285bd482-0a62-4a04-a5a3-c4da97e0b04b">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_6e377077-8505-4162-b779-df2e2e7f6bb1">0.062710 0.372845 0.016635 0.314659 0.001957 0.162369 0.054832 0.001957 0.084986 0.105731 0.115425 0.210486 0.062710 0.372845 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_ce5c6044-b83a-48f7-af65-3ed0bd687d90">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_76438b6c-988c-4523-b705-177ecf7ff002">0.250433 0.194544 0.203523 0.026573 0.397137 0.011669 0.493540 0.004248 0.523302 0.001957 0.572176 0.170019 0.542231 0.172302 0.549256 0.196549 0.452188 0.203981 0.445236 0.179695 0.250433 0.194544 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
            </app:ParameterizedTexture>
          </app:surfaceDataMember>
        </app:Appearance>
      </app:appearance>
      <bldg:measuredHeight uom="#m">17.901</bldg:measuredHeight>
      <bldg:lod1Solid>
        <gml:Solid gml:id="UUID_26a60261-2cec-4551-9acd-b05d38200211">
          <gml:exterior>
            <gml:CompositeSurface gml:id="UUID_15062fab-79e3-48d2-ab08-8bbb89b44128">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_785bf187-dd8e-4686-8d87-305a1f121458">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_97bffa25-f5a2-462d-b148-d99d766805ed">
                      <gml:posList>550171.681000001730000000 5804584.666000000200000000 70.941176288935438000 550171.122228856660000000 5804579.435197340300000000 70.941176288935438000 550170.570999998600000000 5804574.275000000400000000 70.941176288935438000 550170.570999998600000000 5804574.275000000400000000 53.039999999999999000 550171.122228856660000000 5804579.435197340300000000 53.039999999999999000 550171.681000001730000000 5804584.666000000200000000 53.039999999999999000 550171.681000001730000000 5804584.666000000200000000 70.941176288935438000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_27a4e892-65a6-4523-b5b0-ef88fd553f7a">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_a3fd9b1a-072c-4e23-92db-f9ffc563bb5b">
                      <gml:posList>550170.570999998600000000 5804574.275000000400000000 70.941176288935438000 550156.057000000030000000 5804575.701000000400000000 70.941176288935438000 550156.057000000030000000 5804575.701000000400000000 53.039999999999999000 550170.570999998600000000 5804574.275000000400000000 53.039999999999999000 550170.570999998600000000 5804574.275000000400000000 70.941176288935438000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_b01bf238-9537-44d9-b29e-57e9c20767e0">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_586f1658-73a1-44a7-8064-2d48aca50b54">
                      <gml:posList>550157.081000000240000000 5804586.037999999700000000 53.039999999999999000 550156.564064117380000000 5804580.819673612700000000 53.039999999999999000 550156.057000000030000000 5804575.701000000400000000 53.039999999999999000 550156.057000000030000000 5804575.701000000400000000 70.941176288935438000 550156.564064117380000000 5804580.819673612700000000 70.941176288935438000 550157.081000000240000000 5804586.037999999700000000 70.941176288935438000 550157.081000000240000000 5804586.037999999700000000 53.039999999999999000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_f6f0a9da-680c-400e-80d5-b26842a2984c">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_ace03a28-b5ac-4b72-9ddd-613bb0d4974b">
                      <gml:posList>550171.681000001730000000 5804584.666000000200000000 53.039999999999999000 550162.839375075420000000 5804585.496870506600000000 53.039999999999999000 550158.439175697740000000 5804585.910368694900000000 53.039999999999999000 550157.081000000240000000 5804586.037999999700000000 53.039999999999999000 550157.081000000240000000 5804586.037999999700000000 70.941176288935438000 550158.439175697740000000 5804585.910368694900000000 70.941176288935438000 550162.839375075420000000 5804585.496870506600000000 70.941176288935438000 550171.681000001730000000 5804584.666000000200000000 70.941176288935438000 550171.681000001730000000 5804584.666000000200000000 53.039999999999999000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_425a7a69-0648-46ec-ba8d-88e0ab30dc4e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_45ba12bc-42f6-4614-9c2c-d1c4f3dff14c">
                      <gml:posList>550171.681000001730000000 5804584.666000000200000000 53.039999999999999000 550171.122228856660000000 5804579.435197340300000000 53.039999999999999000 550170.570999998600000000 5804574.275000000400000000 53.039999999999999000 550156.057000000030000000 5804575.701000000400000000 53.039999999999999000 550156.564064117380000000 5804580.819673612700000000 53.039999999999999000 550157.081000000240000000 5804586.037999999700000000 53.039999999999999000 550158.439175697740000000 5804585.910368694900000000 53.039999999999999000 550162.839375075420000000 5804585.496870506600000000 53.039999999999999000 550171.681000001730000000 5804584.666000000200000000 53.039999999999999000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_447cb6d3-c1cd-4fde-bdb6-74fa61b50ede">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_7f3cadcb-ed78-4d04-853b-d5f2fab45d91">
                      <gml:posList>550162.839375075420000000 5804585.496870506600000000 70.941176288935438000 550158.439175697740000000 5804585.910368694900000000 70.941176288935438000 550157.081000000240000000 5804586.037999999700000000 70.941176288935438000 550156.564064117380000000 5804580.819673612700000000 70.941176288935438000 550156.057000000030000000 5804575.701000000400000000 70.941176288935438000 550170.570999998600000000 5804574.275000000400000000 70.941176288935438000 550171.122228856660000000 5804579.435197340300000000 70.941176288935438000 550171.681000001730000000 5804584.666000000200000000 70.941176288935438000 550162.839375075420000000 5804585.496870506600000000 70.941176288935438000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod1Solid>
      <bldg:lod2Solid>
        <gml:Solid gml:id="UUID_d392ad94-97ce-44b6-a7fa-2174e098e593">
          <gml:exterior>
            <gml:CompositeSurface gml:id="UUID_94697430-88f9-4852-8607-fad4582b0e7e">
              <gml:surfaceMember xlink:href="#UUID_fa7b4b1c-b1d2-41c0-9b3d-e07d200e69d5"/>
              <gml:surfaceMember xlink:href="#UUID_43070a72-a579-4b0c-a75f-ac6a201ccde2"/>
              <gml:surfaceMember xlink:href="#UUID_f8153523-9df2-4808-b0f6-b314fa4e1d66"/>
              <gml:surfaceMember xlink:href="#UUID_6ad19a7b-6a38-4ed8-b97b-aa4cb16edc0c"/>
              <gml:surfaceMember xlink:href="#UUID_0b8fbdcc-99e7-4df6-b84b-a5f81e04e7e8"/>
              <gml:surfaceMember xlink:href="#UUID_b8d13ccc-cec1-43d4-8c4b-63327fed2e4f"/>
              <gml:surfaceMember xlink:href="#UUID_f96b13ea-a003-49a5-9a34-910ee143c5a6"/>
              <gml:surfaceMember xlink:href="#UUID_285bd482-0a62-4a04-a5a3-c4da97e0b04b"/>
              <gml:surfaceMember xlink:href="#UUID_ce5c6044-b83a-48f7-af65-3ed0bd687d90"/>
              <gml:surfaceMember xlink:href="#UUID_c30d595f-2e67-40a8-960c-07b83ce418e2"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_d8ce4d29-d2c8-4096-89e1-df3b48e27921">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_bf456019-3fe8-43d4-a3c3-27d99e77e62c" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_fa7b4b1c-b1d2-41c0-9b3d-e07d200e69d5">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_a95a7bfb-2f2e-433d-84fe-91c205b91b8b">
                      <gml:posList>550171.122228856660000000 5804579.435197340300000000 70.844167271092402000 550156.564064117380000000 5804580.819673612700000000 70.843838409789186000 550156.057000000030000000 5804575.701000000400000000 66.796397381106388000 550170.570999998600000000 5804574.275000000400000000 66.760909146816701000 550171.122228856660000000 5804579.435197340300000000 70.844167271092402000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_e43dca31-b781-466b-a9f3-9769589622a3">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_3ea7757b-bee2-4bd7-8413-5317d4cf6fc5" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_43070a72-a579-4b0c-a75f-ac6a201ccde2">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_58815da5-e69a-40f8-991c-4468fb7d721e">
                      <gml:posList>550162.839375075420000000 5804585.496870506600000000 68.868732631243347000 550158.439175697740000000 5804585.910368694900000000 68.869396668954280000 550158.136991127860000000 5804582.662022695900000000 69.347320875018539000 550162.537858192340000000 5804582.243568194100000000 69.347370412301984000 550162.839375075420000000 5804585.496870506600000000 68.868732631243347000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_9b559795-b979-4638-a84c-8ddd8e404d77">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_e9ef7845-0cd9-4173-90ab-a897b0efbfd3" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_f8153523-9df2-4808-b0f6-b314fa4e1d66">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_0d9b1c01-8b13-4c4c-8c5a-6b25f4305a63">
                      <gml:posList>550162.537858192340000000 5804582.243568194100000000 69.347370412301984000 550158.136991127860000000 5804582.662022695900000000 69.347320875018539000 550158.439175697740000000 5804585.910368694900000000 66.885233138591531000 550157.081000000240000000 5804586.037999999700000000 66.886352497429016000 550156.564064117380000000 5804580.819673612700000000 70.843838409789186000 550171.122228856660000000 5804579.435197340300000000 70.844167271092402000 550171.681000001730000000 5804584.666000000200000000 66.874319710316428000 550162.839375075420000000 5804585.496870506600000000 66.881606654868250000 550162.537858192340000000 5804582.243568194100000000 69.347370412301984000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_bef45d93-c2b0-4ae7-9572-8e04d428b331">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_ba134e8b-eb2d-47a3-974d-e9cadeee82c3" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_6ad19a7b-6a38-4ed8-b97b-aa4cb16edc0c">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_cc83d9f5-fe63-4b5e-90de-7c8bd6b72d1a">
                      <gml:posList>550162.839375075420000000 5804585.496870506600000000 68.868732631243347000 550162.537858192340000000 5804582.243568194100000000 69.347370412301984000 550162.839375075420000000 5804585.496870506600000000 66.881606654868250000 550162.839375075420000000 5804585.496870506600000000 68.868732631243347000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_25550cf5-ad9e-426c-925b-07f49bdec3ba">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_b34bbd90-178c-4922-bb59-1b5ea544ee4b" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_0b8fbdcc-99e7-4df6-b84b-a5f81e04e7e8">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_7af003a2-483b-46a0-938d-aca86878be11">
                      <gml:posList>550158.439175697740000000 5804585.910368694900000000 68.869396668954280000 550158.439175697740000000 5804585.910368694900000000 66.885233138591531000 550158.136991127860000000 5804582.662022695900000000 69.347320875018539000 550158.439175697740000000 5804585.910368694900000000 68.869396668954280000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_13cb35dd-ec7f-4e42-a08f-3dd45b28520b">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_9377cb8d-439c-4824-8832-047de9fa2a34" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_b8d13ccc-cec1-43d4-8c4b-63327fed2e4f">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_7b4a85d9-a107-45d6-8972-c291982c2a2d">
                      <gml:posList>550170.570999998600000000 5804574.275000000400000000 66.760909146816701000 550156.057000000030000000 5804575.701000000400000000 66.796397381106388000 550156.057000000030000000 5804575.701000000400000000 53.039999999999999000 550170.570999998600000000 5804574.275000000400000000 53.039999999999999000 550170.570999998600000000 5804574.275000000400000000 66.760909146816701000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_68e62b06-8939-41d7-804e-a3e8e2dbadb9">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_357bad9a-a9db-4b3a-9573-284cf8a6bd50" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_f96b13ea-a003-49a5-9a34-910ee143c5a6">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_fb1a1aed-dcfb-4676-90e5-4ed6b13f0777">
                      <gml:posList>550157.081000000240000000 5804586.037999999700000000 53.039999999999999000 550156.564064117380000000 5804580.819673612700000000 53.039999999999999000 550156.057000000030000000 5804575.701000000400000000 53.039999999999999000 550156.057000000030000000 5804575.701000000400000000 66.796397381106388000 550156.564064117380000000 5804580.819673612700000000 70.843838409789186000 550157.081000000240000000 5804586.037999999700000000 66.886352497429016000 550157.081000000240000000 5804586.037999999700000000 53.039999999999999000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_ec8b689a-c50d-49a5-a8b8-2ece3a748443">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_b4e18b18-7f07-447d-8ee1-f4385ab7ecda" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_285bd482-0a62-4a04-a5a3-c4da97e0b04b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_6e377077-8505-4162-b779-df2e2e7f6bb1">
                      <gml:posList>550171.681000001730000000 5804584.666000000200000000 66.874319710316428000 550171.122228856660000000 5804579.435197340300000000 70.844167271092402000 550170.570999998600000000 5804574.275000000400000000 66.760909146816701000 550170.570999998600000000 5804574.275000000400000000 53.039999999999999000 550171.122228856660000000 5804579.435197340300000000 53.039999999999999000 550171.681000001730000000 5804584.666000000200000000 53.039999999999999000 550171.681000001730000000 5804584.666000000200000000 66.874319710316428000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_5948edf2-b746-40f7-9585-afbef00cb78a">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_a84ce0ac-8938-4a24-a759-88509be781a4" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_ce5c6044-b83a-48f7-af65-3ed0bd687d90">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_76438b6c-988c-4523-b705-177ecf7ff002">
                      <gml:posList>550171.681000001730000000 5804584.666000000200000000 66.874319710316428000 550171.681000001730000000 5804584.666000000200000000 53.039999999999999000 550162.839375075420000000 5804585.496870506600000000 53.039999999999999000 550158.439175697740000000 5804585.910368694900000000 53.039999999999999000 550157.081000000240000000 5804586.037999999700000000 53.039999999999999000 550157.081000000240000000 5804586.037999999700000000 66.886352497429016000 550158.439175697740000000 5804585.910368694900000000 66.885233138591531000 550158.439175697740000000 5804585.910368694900000000 68.869396668954280000 550162.839375075420000000 5804585.496870506600000000 68.868732631243347000 550162.839375075420000000 5804585.496870506600000000 66.881606654868250000 550171.681000001730000000 5804584.666000000200000000 66.874319710316428000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="UUID_c5816880-99e3-4a20-8e72-0927957c12da">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_10811261-ae3f-48e8-af82-5f2d3ad60594" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_c30d595f-2e67-40a8-960c-07b83ce418e2">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_0a790dba-3d15-43e3-b755-531f80cf5c22">
                      <gml:posList>550171.681000001730000000 5804584.666000000200000000 53.039999999999999000 550171.122228856660000000 5804579.435197340300000000 53.039999999999999000 550170.570999998600000000 5804574.275000000400000000 53.039999999999999000 550156.057000000030000000 5804575.701000000400000000 53.039999999999999000 550156.564064117380000000 5804580.819673612700000000 53.039999999999999000 550157.081000000240000000 5804586.037999999700000000 53.039999999999999000 550158.439175697740000000 5804585.910368694900000000 53.039999999999999000 550162.839375075420000000 5804585.496870506600000000 53.039999999999999000 550171.681000001730000000 5804584.666000000200000000 53.039999999999999000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </cityObjectMember>

  <cityObjectMember>
    <bldg:Building gml:id="DENIAL43000053Bz">
      <gml:boundedBy>
        <gml:Envelope srsDimension="3" srsName="urn:adv:crs:ETRS89_UTM32*DE_DHHN92_NH">
          <gml:lowerCorner>550204.687500 5804587.500000 53.829998
</gml:lowerCorner>
          <gml:upperCorner>550224.250000 5804617.500000 75.861427
</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <creationDate>2017-11-21</creationDate>
      <gen:doubleAttribute name="Volume">
        <gen:value>6057.249</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Profile">
        <gen:value>Sig3d</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCK">
        <gen:value>10</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOECKE">
        <gen:value>111010</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STBEZNR">
        <gen:value>111</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="MBZNR">
        <gen:value>1102</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="WBZNR">
        <gen:value>1102</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNAM">
        <gen:value>Vahrenwald</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTNR">
        <gen:value>11</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNAM">
        <gen:value>Vahrenwald-List</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBEZ">
        <gen:value>02</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEMEINDE">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SCALE">
        <gen:value>5000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SHAPE_Area">
        <gen:value>7165.30049704715</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ALKISID">
        <gen:value>DENIAL43000053Bz</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ZMIN">
        <gen:value>53.83</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="THICKNESS">
        <gen:value>0.1</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="TEXTURE">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="BUILDINGID">
        <gen:value>DENIAL43000053Bz</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ARTBEZEICH">
        <gen:value>Gebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Join_Cou_1">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="FUNKTION_T">
        <gen:value>Wohngebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="OBJFLAECHE">
        <gen:value>320.86</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADT">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEB_Funkti">
        <gen:value>1000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STRSCHL">
        <gen:value>02557</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STR_komple">
        <gen:value>Seydlitzstraße 34</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="PLZ_NUMMER">
        <gen:value>30161</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STR">
        <gen:value>Seydlitzstraße</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="HAUSNR">
        <gen:value>34</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="OVERHANG">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <app:appearance>
        <app:Appearance>
          <app:theme>RhinoCity ObliqueTexturing</app:theme>
          <app:surfaceDataMember>
            <app:ParameterizedTexture>
              <app:imageURI>Hannover_teil1_Appearance/DENIAL43000053Bz.jpg</app:imageURI>
              <app:textureType>specific</app:textureType>
              <app:wrapMode>border</app:wrapMode>
              <app:borderColor>0 0 0 1</app:borderColor>
              <app:target uri="#UUID_67b593df-02d9-43a5-8b56-fb2c1b17bd05">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_818c2e11-0a46-4fbb-b531-5a9b1df2b479">0.295169 0.635369 0.389676 0.808896 0.267536 0.710978 0.271548 0.701704 0.263930 0.690123 0.287090 0.626223 0.295032 0.635840 0.295169 0.635369 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_5884f502-db6a-4560-a85b-b73f334371c6">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_175ad6b1-9250-4eb9-b5dc-179e942bbc62">0.245877 0.557687 0.151317 0.382943 0.157583 0.365257 0.117302 0.029663 0.180221 0.001957 0.245877 0.557687 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_1d0dd9f8-b040-4ee4-84b4-f53e462fd4e3">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_ba73a7b5-9c19-4ba7-8d9a-5dde0681433b">0.395021 0.429850 0.338604 0.496573 0.248289 0.073362 0.308181 0.001957 0.395747 0.426979 0.395021 0.429850 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_cd2cd842-45b2-4e9a-9ed8-3f3fe4f8d978">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_c273bbd3-af5f-4c19-a2db-417400153444">0.959054 0.626223 0.934564 0.732080 0.903226 0.692014 0.959054 0.626223 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_aa69de92-e084-4fc3-9612-bbe3f81e3a02">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_cb0daae7-ea5d-4ae3-8d95-cae5b5934f8b">0.872446 0.742966 0.870939 0.743302 0.869013 0.626223 0.901141 0.679306 0.872446 0.742966 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_5cf1f109-b30d-46b3-a98f-5b3ff9ea6c6d">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_6a8acd82-9da1-4af1-8524-e0ee7d5b10f3">0.572644 0.743784 0.391984 0.782351 0.393310 0.663519 0.570670 0.626223 0.572644 0.743784 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_37d5935b-37cc-4553-9eb9-d6c8d8b6f6e4">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_90dffc03-78e7-4097-a4f2-5e779a1a8e7a">0.646130 0.626223 0.763795 0.725321 0.586391 0.762551 0.574780 0.649526 0.642559 0.635102 0.646130 0.626223 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_3ddd68be-095a-477f-b7d6-c0f4fece78ce">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_b5cccf73-8070-4dee-b14a-ba75d21f4aa2">0.261187 0.810243 0.184751 0.626401 0.184776 0.626223 0.261187 0.810243 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_7f4c5ebf-c5a7-4656-9cf5-34748dc79b0f">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_a7169201-9f22-4df8-8973-06d88620d962">0.086140 0.847161 0.026581 0.858123 0.000978 0.637080 0.060044 0.626223 0.086140 0.847161 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_28970f9b-ea72-4424-8e52-0ffeaaf3d063">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_87bef7e5-f544-440c-9df7-e5d0fbdff099">0.688219 0.245127 0.681294 0.129547 0.674487 0.015951 0.717424 0.001957 0.728889 0.115687 0.727580 0.235049 0.688219 0.245127 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_9dc9edfd-6f46-4124-807d-927ac3a63988">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_c58d26d7-c370-4629-b465-e479b1ce4771">0.811339 0.029953 0.812470 0.029750 0.966862 0.001957 0.993822 0.208622 0.838285 0.236643 0.837281 0.237928 0.811339 0.029953 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_e1a08f8c-3e01-4ee8-b00f-f541646de453">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_643aa742-8704-4f7f-9b8c-914337981129">0.469208 0.103716 0.470059 0.101876 0.495741 0.046322 0.516250 0.001957 0.541987 0.210306 0.526505 0.297259 0.494578 0.303784 0.493607 0.304702 0.469208 0.103716 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_33f0edfd-d1fb-4c5e-adfe-ffca998a7cee">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_a072e1bb-3340-439f-8e30-982bc9d7126d">0.088377 0.621458 0.000978 0.196098 0.028610 0.001957 0.115342 0.424404 0.088377 0.621458 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_31077d36-5070-43ef-b102-fbf8c3db9ac7">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_375e8224-dfe3-4145-8350-47b229ae2c2c">0.671979 0.194969 0.622425 0.265853 0.568442 0.239198 0.544477 0.040588 0.592605 0.022609 0.647889 0.001957 0.671979 0.194969 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_4e931799-c2a9-41ed-a664-4ef7239d16da">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_2e13574c-4676-463e-8a33-109d35809778">0.419114 0.001957 0.466817 0.237955 0.446036 0.444344 0.397849 0.207975 0.419114 0.001957 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_a26fd5f0-fee2-4a80-bc93-fab8abcfe255">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_37126efb-9244-4429-8678-847be9b23eca">0.866335 0.750642 0.766373 0.649839 0.769785 0.626223 0.866335 0.750642 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_623b743e-913f-4782-a35e-c9cadfe99b50">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_02f53e06-5aea-4a2e-bbcd-2e615e6dad58">0.117683 0.825637 0.111805 0.833088 0.111826 0.833265 0.111684 0.833643 0.088954 0.639658 0.089096 0.639298 0.094264 0.626223 0.117683 0.825637 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_03c6812b-ef5f-4bf3-93fb-33464361cdb8">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_a8953ea3-56fc-4ccc-b446-153d612febe6">0.154832 0.827950 0.148583 0.816865 0.175433 0.626223 0.182133 0.633707 0.154832 0.827950 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_d0cfc7c2-1d38-47cc-9598-201b5d215f2a">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_95c82bd1-5632-4fea-9815-0e8a92ebb6c0">0.776883 0.192591 0.753651 0.242539 0.731183 0.050709 0.754346 0.001957 0.776883 0.192591 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_5dca0f0a-fcc4-4ee5-98a7-17aeb33a2100">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_cc2f5108-c826-407e-bbd2-eb3e90948136">0.140463 0.626223 0.146554 0.634614 0.126756 0.831210 0.120235 0.826613 0.140463 0.626223 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_13c4dfdb-d9f3-4439-b4e0-60673fd7e333">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_21557908-bc9e-4d09-adcb-8d383aa96e21">0.809342 0.197519 0.805467 0.204549 0.808219 0.228303 0.804649 0.236822 0.779081 0.015854 0.782808 0.009008 0.786646 0.001957 0.809342 0.197519 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
            </app:ParameterizedTexture>
          </app:surfaceDataMember>
        </app:Appearance>
      </app:appearance>
      <bldg:measuredHeight uom="#m">22.031</bldg:measuredHeight>
      <bldg:lod1Solid>
        <gml:Solid gml:id="UUID_87b34814-b7be-464a-8bd1-740118572e22">
          <gml:exterior>
            <gml:CompositeSurface gml:id="UUID_1be3f3b5-b826-4bd4-b11e-d931d0698b7f">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_e8aa5998-f70a-4da5-a5e0-be3a57747c66">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_9319ce84-14d4-4fc5-b9ad-1fcce4176db4">
                      <gml:posList>550213.168000001460000000 5804603.077999999700000000 53.829999999999998000 550210.046000000090000000 5804590.165000000000000000 53.829999999999998000 550210.046000000090000000 5804590.165000000000000000 75.861426562158684000 550213.168000001460000000 5804603.077999999700000000 75.861426562158684000 550213.168000001460000000 5804603.077999999700000000 53.829999999999998000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_622e457c-e640-4975-aba1-426cad464754">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_cdefd91d-ff6c-40bb-9374-8fec15c37dc9">
                      <gml:posList>550213.168000001460000000 5804603.077999999700000000 75.861426562158684000 550212.772866203330000000 5804603.743693389000000000 75.861426562158684000 550212.761999998240000000 5804603.762000000100000000 75.861426562158684000 550212.761999998240000000 5804603.762000000100000000 53.829999999999998000 550212.772866203330000000 5804603.743693389000000000 53.829999999999998000 550213.168000001460000000 5804603.077999999700000000 53.829999999999998000 550213.168000001460000000 5804603.077999999700000000 75.861426562158684000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_9abb0474-7032-4fc1-9c4b-d5696a5086c5">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_dcdb3a53-f305-4b58-b220-029f249e98f6">
                      <gml:posList>550212.761999998240000000 5804603.762000000100000000 75.861426562158684000 550212.203000001610000000 5804603.379999999900000000 75.861426562158684000 550212.203000001610000000 5804603.379999999900000000 53.829999999999998000 550212.761999998240000000 5804603.762000000100000000 53.829999999999998000 550212.761999998240000000 5804603.762000000100000000 75.861426562158684000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_7d0d10b3-4f8a-4968-8889-3bf9cd780e25">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_ad4c895d-5e09-4c55-a474-26cfa334dbcf">
                      <gml:posList>550212.203000001610000000 5804603.379999999900000000 75.861426562158684000 550210.396000001580000000 5804605.861999999700000000 75.861426562158684000 550210.396000001580000000 5804605.861999999700000000 53.829999999999998000 550212.203000001610000000 5804603.379999999900000000 53.829999999999998000 550212.203000001610000000 5804603.379999999900000000 75.861426562158684000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_90da749c-538f-4087-81b0-d5d060052e06">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_70d056ac-44c1-40eb-8c88-9092c4af3ff5">
                      <gml:posList>550210.927999999370000000 5804606.320000000300000000 53.829999999999998000 550210.396000001580000000 5804605.861999999700000000 53.829999999999998000 550210.396000001580000000 5804605.861999999700000000 75.861426562158684000 550210.927999999370000000 5804606.320000000300000000 75.861426562158684000 550210.927999999370000000 5804606.320000000300000000 53.829999999999998000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_12ec2904-768d-4f4c-b785-7e94231d3fd0">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_858919d6-2116-418e-9dd5-2f02a89876ee">
                      <gml:posList>550210.927999999370000000 5804606.320000000300000000 75.861426562158684000 550210.618545829670000000 5804606.679677059900000000 75.861426562158684000 550210.317999999970000000 5804607.029000000100000000 75.861426562158684000 550210.317999999970000000 5804607.029000000100000000 53.829999999999998000 550210.618545829670000000 5804606.679677059900000000 53.829999999999998000 550210.927999999370000000 5804606.320000000300000000 53.829999999999998000 550210.927999999370000000 5804606.320000000300000000 75.861426562158684000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_44d1eb88-abf7-478c-bc7b-b8c2e7903eea">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_7e0cfa29-4f99-4863-9e4a-fb76c60ae4e5">
                      <gml:posList>550210.317999999970000000 5804607.029000000100000000 75.861426562158684000 550205.092999998480000000 5804607.593999999600000000 75.861426562158684000 550205.092999998480000000 5804607.593999999600000000 53.829999999999998000 550210.317999999970000000 5804607.029000000100000000 53.829999999999998000 550210.317999999970000000 5804607.029000000100000000 75.861426562158684000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_df32fef6-8863-41f9-984b-32e7d762f06b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_68d51a0d-9fb6-49c0-9cc2-e4feaa921d89">
                      <gml:posList>550206.186000000690000000 5804616.554999999700000000 53.829999999999998000 550205.634770259260000000 5804612.035723052900000000 53.829999999999998000 550205.092999998480000000 5804607.593999999600000000 53.829999999999998000 550205.092999998480000000 5804607.593999999600000000 75.861426562158684000 550205.634770259260000000 5804612.035723052900000000 75.861426562158684000 550206.186000000690000000 5804616.554999999700000000 75.861426562158684000 550206.186000000690000000 5804616.554999999700000000 53.829999999999998000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_7df4f3bf-16a7-4966-a397-a2b91efe5a6c">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_e79f75a8-fa29-4216-9059-b5830d3a286d">
                      <gml:posList>550220.228999998420000000 5804615.035000000100000000 53.829999999999998000 550220.126810311570000000 5804615.046060907700000000 53.829999999999998000 550206.186000000690000000 5804616.554999999700000000 53.829999999999998000 550206.186000000690000000 5804616.554999999700000000 75.861426562158684000 550220.126810311570000000 5804615.046060907700000000 75.861426562158684000 550220.228999998420000000 5804615.035000000100000000 75.861426562158684000 550220.228999998420000000 5804615.035000000100000000 53.829999999999998000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_2294a1a4-43c8-47ba-9a3f-a9f49f71352a">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_685f782f-18f7-45a9-8ee3-e34b35752941">
                      <gml:posList>550223.896000001580000000 5804609.538999999900000000 53.829999999999998000 550223.829549887920000000 5804609.638593625300000000 53.829999999999998000 550221.825828812200000000 5804612.641716348000000000 53.829999999999998000 550220.228999998420000000 5804615.035000000100000000 53.829999999999998000 550220.228999998420000000 5804615.035000000100000000 75.861426562158684000 550221.825828812200000000 5804612.641716348000000000 75.861426562158684000 550223.829549887920000000 5804609.638593625300000000 75.861426562158684000 550223.896000001580000000 5804609.538999999900000000 75.861426562158684000 550223.896000001580000000 5804609.538999999900000000 53.829999999999998000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_cd50c68e-55b5-4cb2-8f03-17e125d83a10">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_7456c4ed-cafa-4580-aebc-6a4c62c64131">
                      <gml:posList>550223.896000001580000000 5804609.538999999900000000 75.861426562158684000 550218.975999999790000000 5804588.207000000400000000 75.861426562158684000 550218.975999999790000000 5804588.207000000400000000 53.829999999999998000 550223.896000001580000000 5804609.538999999900000000 53.829999999999998000 550223.896000001580000000 5804609.538999999900000000 75.861426562158684000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_68d49656-6564-4caa-95b8-d82df7c4fbea">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_7a48ff8d-5294-4cd3-a82c-62b9f90a8688">
                      <gml:posList>550218.975999999790000000 5804588.207000000400000000 75.861426562158684000 550214.203797197200000000 5804589.253357568800000000 75.861426562158684000 550210.046000000090000000 5804590.165000000000000000 75.861426562158684000 550210.046000000090000000 5804590.165000000000000000 53.829999999999998000 550214.203797197200000000 5804589.253357568800000000 53.829999999999998000 550218.975999999790000000 5804588.207000000400000000 53.829999999999998000 550218.975999999790000000 5804588.207000000400000000 75.861426562158684000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_3b247dc3-bf2b-48fb-a26c-597d30aff9ae">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_8c9ff3e3-c4e2-41bf-a14a-c34476f20fb7">
                      <gml:posList>550210.046000000090000000 5804590.165000000000000000 53.829999999999998000 550213.168000001460000000 5804603.077999999700000000 53.829999999999998000 550212.772866203330000000 5804603.743693389000000000 53.829999999999998000 550212.761999998240000000 5804603.762000000100000000 53.829999999999998000 550212.203000001610000000 5804603.379999999900000000 53.829999999999998000 550210.396000001580000000 5804605.861999999700000000 53.829999999999998000 550210.927999999370000000 5804606.320000000300000000 53.829999999999998000 550210.618545829670000000 5804606.679677059900000000 53.829999999999998000 550210.317999999970000000 5804607.029000000100000000 53.829999999999998000 550205.092999998480000000 5804607.593999999600000000 53.829999999999998000 550205.634770259260000000 5804612.035723052900000000 53.829999999999998000 550206.186000000690000000 5804616.554999999700000000 53.829999999999998000 550220.126810311570000000 5804615.046060907700000000 53.829999999999998000 550220.228999998420000000 5804615.035000000100000000 53.829999999999998000 550221.825828812200000000 5804612.641716348000000000 53.829999999999998000 550223.829549887920000000 5804609.638593625300000000 53.829999999999998000 550223.896000001580000000 5804609.538999999900000000 53.829999999999998000 550218.975999999790000000 5804588.207000000400000000 53.829999999999998000 550214.203797197200000000 5804589.253357568800000000 53.829999999999998000 550210.046000000090000000 5804590.165000000000000000 53.829999999999998000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_24a817c6-699f-4261-b310-fa62552573b0">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_429bf32b-f842-4976-8e84-3176bac71203">
                      <gml:posList>550210.046000000090000000 5804590.165000000000000000 75.861426562158684000 550214.203797197200000000 5804589.253357568800000000 75.861426562158684000 550218.975999999790000000 5804588.207000000400000000 75.861426562158684000 550223.896000001580000000 5804609.538999999900000000 75.861426562158684000 550223.829549887920000000 5804609.638593625300000000 75.861426562158684000 550221.825828812200000000 5804612.641716348000000000 75.861426562158684000 550220.228999998420000000 5804615.035000000100000000 75.861426562158684000 550220.126810311570000000 5804615.046060907700000000 75.861426562158684000 550206.186000000690000000 5804616.554999999700000000 75.861426562158684000 550205.634770259260000000 5804612.035723052900000000 75.861426562158684000 550205.092999998480000000 5804607.593999999600000000 75.861426562158684000 550210.317999999970000000 5804607.029000000100000000 75.861426562158684000 550210.618545829670000000 5804606.679677059900000000 75.861426562158684000 550210.927999999370000000 5804606.320000000300000000 75.861426562158684000 550210.396000001580000000 5804605.861999999700000000 75.861426562158684000 550212.203000001610000000 5804603.379999999900000000 75.861426562158684000 550212.761999998240000000 5804603.762000000100000000 75.861426562158684000 550212.772866203330000000 5804603.743693389000000000 75.861426562158684000 550213.168000001460000000 5804603.077999999700000000 75.861426562158684000 550210.046000000090000000 5804590.165000000000000000 75.861426562158684000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod1Solid>
      <bldg:lod2Solid>
        <gml:Solid gml:id="UUID_0dfd0922-9ebc-4d98-881e-3bf4527de7c2">
          <gml:exterior>
            <gml:CompositeSurface gml:id="UUID_20d035cf-b249-4edf-9a7b-befe6bdde0ca">
              <gml:surfaceMember xlink:href="#UUID_67b593df-02d9-43a5-8b56-fb2c1b17bd05"/>
              <gml:surfaceMember xlink:href="#UUID_5884f502-db6a-4560-a85b-b73f334371c6"/>
              <gml:surfaceMember xlink:href="#UUID_1d0dd9f8-b040-4ee4-84b4-f53e462fd4e3"/>
              <gml:surfaceMember xlink:href="#UUID_cd2cd842-45b2-4e9a-9ed8-3f3fe4f8d978"/>
              <gml:surfaceMember xlink:href="#UUID_aa69de92-e084-4fc3-9612-bbe3f81e3a02"/>
              <gml:surfaceMember xlink:href="#UUID_5cf1f109-b30d-46b3-a98f-5b3ff9ea6c6d"/>
              <gml:surfaceMember xlink:href="#UUID_37d5935b-37cc-4553-9eb9-d6c8d8b6f6e4"/>
              <gml:surfaceMember xlink:href="#UUID_3ddd68be-095a-477f-b7d6-c0f4fece78ce"/>
              <gml:surfaceMember xlink:href="#UUID_7f4c5ebf-c5a7-4656-9cf5-34748dc79b0f"/>
              <gml:surfaceMember xlink:href="#UUID_28970f9b-ea72-4424-8e52-0ffeaaf3d063"/>
              <gml:surfaceMember xlink:href="#UUID_9dc9edfd-6f46-4124-807d-927ac3a63988"/>
              <gml:surfaceMember xlink:href="#UUID_e1a08f8c-3e01-4ee8-b00f-f541646de453"/>
              <gml:surfaceMember xlink:href="#UUID_33f0edfd-d1fb-4c5e-adfe-ffca998a7cee"/>
              <gml:surfaceMember xlink:href="#UUID_31077d36-5070-43ef-b102-fbf8c3db9ac7"/>
              <gml:surfaceMember xlink:href="#UUID_4e931799-c2a9-41ed-a664-4ef7239d16da"/>
              <gml:surfaceMember xlink:href="#UUID_a26fd5f0-fee2-4a80-bc93-fab8abcfe255"/>
              <gml:surfaceMember xlink:href="#UUID_623b743e-913f-4782-a35e-c9cadfe99b50"/>
              <gml:surfaceMember xlink:href="#UUID_03c6812b-ef5f-4bf3-93fb-33464361cdb8"/>
              <gml:surfaceMember xlink:href="#UUID_d0cfc7c2-1d38-47cc-9598-201b5d215f2a"/>
              <gml:surfaceMember xlink:href="#UUID_5dca0f0a-fcc4-4ee5-98a7-17aeb33a2100"/>
              <gml:surfaceMember xlink:href="#UUID_13c4dfdb-d9f3-4439-b4e0-60673fd7e333"/>
              <gml:surfaceMember xlink:href="#UUID_ef48c202-67ff-4b3f-a206-b4cec4214bcd"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_e1454e0a-e72e-4b5f-af39-aa51b4197a22">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_af6601c0-ba51-488d-b7b3-98815c245ce0" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_67b593df-02d9-43a5-8b56-fb2c1b17bd05">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_818c2e11-0a46-4fbb-b531-5a9b1df2b479">
                      <gml:posList>550212.772866203330000000 5804603.743693389000000000 70.309083798931312000 550219.290609533670000000 5804610.579481243200000000 74.502328285501108000 550210.618545829670000000 5804606.679677059900000000 70.406257534324126000 550210.927999999370000000 5804606.320000000300000000 70.410015265325370000 550210.396000001580000000 5804605.861999999700000000 70.096284844879150000 550212.203000001610000000 5804603.379999999900000000 70.009251591369150000 550212.761999998240000000 5804603.762000000100000000 70.310572465141405000 550212.772866203330000000 5804603.743693389000000000 70.309083798931312000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_0ffe1077-3608-49ef-8249-2567d7e08cf8">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_14c94a6c-8241-4474-820a-c356d6ec8886" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_5884f502-db6a-4560-a85b-b73f334371c6">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_175ad6b1-9250-4eb9-b5dc-179e942bbc62">
                      <gml:posList>550219.290609533670000000 5804610.579481243200000000 74.502328285501108000 550212.772866203220000000 5804603.743693389000000000 70.294119918814985000 550213.168000001460000000 5804603.077999999700000000 70.772277755906998000 550210.046000000090000000 5804590.165000000000000000 70.746889118203569000 550214.203797197200000000 5804589.253357568800000000 74.520102542376634000 550219.290609533670000000 5804610.579481243200000000 74.502328285501108000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_15418b80-572a-433f-996f-4a29e0fae37c">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_4f413877-55ec-48ce-9f7f-cc3a61cc8be9" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_1d0dd9f8-b040-4ee4-84b4-f53e462fd4e3">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_ba73a7b5-9c19-4ba7-8d9a-5dde0681433b">
                      <gml:posList>550223.829549887920000000 5804609.638593625300000000 70.484923336915429000 550219.290609533670000000 5804610.579481243200000000 74.502328285501108000 550214.203797197200000000 5804589.253357568800000000 74.520102542376634000 550218.975999999790000000 5804588.207000000400000000 70.284795260208156000 550223.896000001580000000 5804609.538999999900000000 70.408912208772733000 550223.829549887920000000 5804609.638593625300000000 70.484923336915429000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_845dd78d-da8e-449e-a148-e338e47b4c6d">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_93f61c4a-90fa-43ad-8d60-ade1d4695021" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_cd2cd842-45b2-4e9a-9ed8-3f3fe4f8d978">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_c273bbd3-af5f-4c19-a2db-417400153444">
                      <gml:posList>550223.829549887920000000 5804609.638593625300000000 70.484923336915429000 550221.825828812200000000 5804612.641716348000000000 74.509890153627978000 550219.290609533670000000 5804610.579481243200000000 74.502328285501108000 550223.829549887920000000 5804609.638593625300000000 70.484923336915429000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_464cce8e-83c0-49ad-b5bf-fdd5d0b5d412">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_20c129e5-a6ae-4550-9e65-8e9cc0e91103" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_aa69de92-e084-4fc3-9612-bbe3f81e3a02">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_cb0daae7-ea5d-4ae3-8d95-cae5b5934f8b">
                      <gml:posList>550220.228999998420000000 5804615.035000000100000000 71.037332733124217000 550220.126810311570000000 5804615.046060907700000000 70.948566813783856000 550219.290609533670000000 5804610.579481243200000000 74.502328285501108000 550221.825828812200000000 5804612.641716348000000000 74.509890153627978000 550220.228999998420000000 5804615.035000000100000000 71.037332733124217000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_4b25721e-ca02-4df2-bafd-7b3a3b184558">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_59e62cd5-5a2d-46b3-86ba-489d92cd2bb2" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_5cf1f109-b30d-46b3-a98f-5b3ff9ea6c6d">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_6a8acd82-9da1-4af1-8524-e0ee7d5b10f3">
                      <gml:posList>550220.126810311570000000 5804615.046060907700000000 70.948566813783856000 550206.186000000690000000 5804616.554999999700000000 70.936697943887296000 550205.634770259260000000 5804612.035723052900000000 74.507746764124562000 550219.290609533670000000 5804610.579481243200000000 74.502328285501108000 550220.126810311570000000 5804615.046060907700000000 70.948566813783856000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_c6ed73a4-1960-4aba-a7a5-cb17ce3dad2d">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_82261ecf-c0e6-4da5-9503-ff8eba795fee" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_37d5935b-37cc-4553-9eb9-d6c8d8b6f6e4">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_90dffc03-78e7-4097-a4f2-5e779a1a8e7a">
                      <gml:posList>550210.618545829320000000 5804606.679677060800000000 72.403278077454416000 550219.290609533670000000 5804610.579481243200000000 74.502328285501108000 550205.634770259260000000 5804612.035723052900000000 74.507746764124562000 550205.092999998480000000 5804607.593999999600000000 72.547138675778442000 550210.317999999970000000 5804607.029000000100000000 72.541660885469923000 550210.618545829320000000 5804606.679677060800000000 72.403278077454416000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_2b335ed6-30b6-4baa-91d4-398b8f060f7c">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_e06ee6dc-a313-41ff-8c92-62736b3f7c15" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_3ddd68be-095a-477f-b7d6-c0f4fece78ce">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_b5cccf73-8070-4dee-b14a-ba75d21f4aa2">
                      <gml:posList>550219.290609533670000000 5804610.579481243200000000 74.502328285501108000 550212.772866203330000000 5804603.743693389000000000 70.309083798931312000 550212.772866203220000000 5804603.743693389000000000 70.294119918814985000 550219.290609533670000000 5804610.579481243200000000 74.502328285501108000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_88c98f3a-46d2-4002-acc7-9d2420248b03">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_6787334b-7337-4841-96b2-791992b5910b" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_7f4c5ebf-c5a7-4656-9cf5-34748dc79b0f">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_a7169201-9f22-4df8-8973-06d88620d962">
                      <gml:posList>550210.317999999970000000 5804607.029000000100000000 72.541660885469923000 550205.092999998480000000 5804607.593999999600000000 72.547138675778442000 550205.092999998480000000 5804607.593999999600000000 53.829999999999998000 550210.317999999970000000 5804607.029000000100000000 53.829999999999998000 550210.317999999970000000 5804607.029000000100000000 72.541660885469923000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_4a4fd5a0-002c-41c4-be8f-0c57166e4ee4">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_d568b57f-983c-4ab9-9889-5d9ec0473f36" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_28970f9b-ea72-4424-8e52-0ffeaaf3d063">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_87bef7e5-f544-440c-9df7-e5d0fbdff099">
                      <gml:posList>550206.186000000690000000 5804616.554999999700000000 53.829999999999998000 550205.634770259260000000 5804612.035723052900000000 53.829999999999998000 550205.092999998480000000 5804607.593999999600000000 53.829999999999998000 550205.092999998480000000 5804607.593999999600000000 72.547138675778442000 550205.634770259260000000 5804612.035723052900000000 74.507746764124562000 550206.186000000690000000 5804616.554999999700000000 70.936697943887296000 550206.186000000690000000 5804616.554999999700000000 53.829999999999998000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_19756713-ad10-462f-85d1-202cd003cb93">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_8dc18ab4-695f-432e-a6d5-689f5965b64a" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_9dc9edfd-6f46-4124-807d-927ac3a63988">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_c58d26d7-c370-4629-b465-e479b1ce4771">
                      <gml:posList>550220.228999998420000000 5804615.035000000100000000 53.829999999999998000 550220.126810311570000000 5804615.046060907700000000 53.829999999999998000 550206.186000000690000000 5804616.554999999700000000 53.829999999999998000 550206.186000000690000000 5804616.554999999700000000 70.936697943887296000 550220.126810311570000000 5804615.046060907700000000 70.948566813783856000 550220.228999998420000000 5804615.035000000100000000 71.037332733124217000 550220.228999998420000000 5804615.035000000100000000 53.829999999999998000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_20ff77c8-ba36-4215-82ef-4cad453a8ffe">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_3336711f-372c-4bb1-bc31-339869a1dd20" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_e1a08f8c-3e01-4ee8-b00f-f541646de453">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_643aa742-8704-4f7f-9b8c-914337981129">
                      <gml:posList>550223.896000001580000000 5804609.538999999900000000 53.829999999999998000 550223.829549887920000000 5804609.638593625300000000 53.829999999999998000 550221.825828812200000000 5804612.641716348000000000 53.829999999999998000 550220.228999998420000000 5804615.035000000100000000 53.829999999999998000 550220.228999998420000000 5804615.035000000100000000 71.037332733124217000 550221.825828812200000000 5804612.641716348000000000 74.509890153627978000 550223.829549887920000000 5804609.638593625300000000 70.484923336915429000 550223.896000001580000000 5804609.538999999900000000 70.408912208772733000 550223.896000001580000000 5804609.538999999900000000 53.829999999999998000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_95b53ca8-d301-4207-ac2c-e2c9f1a263fe">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_9c71b5f8-7775-424a-86a3-f9fc3d99a465" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_33f0edfd-d1fb-4c5e-adfe-ffca998a7cee">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_a072e1bb-3340-439f-8e30-982bc9d7126d">
                      <gml:posList>550223.896000001580000000 5804609.538999999900000000 70.408912208772733000 550218.975999999790000000 5804588.207000000400000000 70.284795260208156000 550218.975999999790000000 5804588.207000000400000000 53.829999999999998000 550223.896000001580000000 5804609.538999999900000000 53.829999999999998000 550223.896000001580000000 5804609.538999999900000000 70.408912208772733000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_415f8206-34f2-42dc-9b72-9db8abe4df13">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_c50f44c7-a81b-4b28-b6f3-1bb6b947b1db" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_31077d36-5070-43ef-b102-fbf8c3db9ac7">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_375e8224-dfe3-4145-8350-47b229ae2c2c">
                      <gml:posList>550218.975999999790000000 5804588.207000000400000000 70.284795260208156000 550214.203797197200000000 5804589.253357568800000000 74.520102542376634000 550210.046000000090000000 5804590.165000000000000000 70.746889118203569000 550210.046000000090000000 5804590.165000000000000000 53.829999999999998000 550214.203797197200000000 5804589.253357568800000000 53.829999999999998000 550218.975999999790000000 5804588.207000000400000000 53.829999999999998000 550218.975999999790000000 5804588.207000000400000000 70.284795260208156000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_e8623dba-6220-4ab5-aa56-e47fe51926fd">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_1364ecfc-b78b-4995-b115-c7b71b8e7420" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_4e931799-c2a9-41ed-a664-4ef7239d16da">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_2e13574c-4676-463e-8a33-109d35809778">
                      <gml:posList>550213.168000001460000000 5804603.077999999700000000 53.829999999999998000 550210.046000000090000000 5804590.165000000000000000 53.829999999999998000 550210.046000000090000000 5804590.165000000000000000 70.746889118203569000 550213.168000001460000000 5804603.077999999700000000 70.772277755906998000 550213.168000001460000000 5804603.077999999700000000 53.829999999999998000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_0e28135c-a310-4dd9-a29a-5ae204143855">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_a6c521e6-276b-4890-bfa5-070730dadfb4" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_a26fd5f0-fee2-4a80-bc93-fab8abcfe255">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_37126efb-9244-4429-8678-847be9b23eca">
                      <gml:posList>550219.290609533670000000 5804610.579481243200000000 74.502328285501108000 550210.618545829320000000 5804606.679677060800000000 72.403278077454416000 550210.618545829670000000 5804606.679677059900000000 70.406257534324126000 550219.290609533670000000 5804610.579481243200000000 74.502328285501108000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_5e074dd4-9545-40d7-9bd3-5b630f97ec29">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_3ad4b5b2-8e11-468a-957f-4e1541342896" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_623b743e-913f-4782-a35e-c9cadfe99b50">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_02f53e06-5aea-4a2e-bbcd-2e615e6dad58">
                      <gml:posList>550213.168000001460000000 5804603.077999999700000000 70.772277755906998000 550212.772866203220000000 5804603.743693389000000000 70.294119918814985000 550212.772866203330000000 5804603.743693389000000000 70.309083798931312000 550212.761999998240000000 5804603.762000000100000000 70.310572465141405000 550212.761999998240000000 5804603.762000000100000000 53.829999999999998000 550212.772866203330000000 5804603.743693389000000000 53.829999999999998000 550213.168000001460000000 5804603.077999999700000000 53.829999999999998000 550213.168000001460000000 5804603.077999999700000000 70.772277755906998000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_25398373-ea4d-45df-ad98-b70b742ee1a0">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_d4f0a51f-fea1-4ba8-8cc2-19a5a983cdb8" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_03c6812b-ef5f-4bf3-93fb-33464361cdb8">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_a8953ea3-56fc-4ccc-b446-153d612febe6">
                      <gml:posList>550212.761999998240000000 5804603.762000000100000000 70.310572465141405000 550212.203000001610000000 5804603.379999999900000000 70.009251591369150000 550212.203000001610000000 5804603.379999999900000000 53.829999999999998000 550212.761999998240000000 5804603.762000000100000000 53.829999999999998000 550212.761999998240000000 5804603.762000000100000000 70.310572465141405000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_ecd7b5a4-f935-4a20-b10d-7258f6eca716">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_69c2bf77-8dc8-4690-9f22-d50bf42bda37" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_d0cfc7c2-1d38-47cc-9598-201b5d215f2a">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_95c82bd1-5632-4fea-9815-0e8a92ebb6c0">
                      <gml:posList>550212.203000001610000000 5804603.379999999900000000 70.009251591369150000 550210.396000001580000000 5804605.861999999700000000 70.096284844879150000 550210.396000001580000000 5804605.861999999700000000 53.829999999999998000 550212.203000001610000000 5804603.379999999900000000 53.829999999999998000 550212.203000001610000000 5804603.379999999900000000 70.009251591369150000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_b9d81ca4-1b1d-405d-b6a4-f6d568749b40">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_5f6d6606-d14b-40e5-93d5-72d498b4f394" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_5dca0f0a-fcc4-4ee5-98a7-17aeb33a2100">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_cc2f5108-c826-407e-bbd2-eb3e90948136">
                      <gml:posList>550210.927999999370000000 5804606.320000000300000000 53.829999999999998000 550210.396000001580000000 5804605.861999999700000000 53.829999999999998000 550210.396000001580000000 5804605.861999999700000000 70.096284844879150000 550210.927999999370000000 5804606.320000000300000000 70.410015265325370000 550210.927999999370000000 5804606.320000000300000000 53.829999999999998000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_c8c79cd2-d146-4a72-9681-5042aca0fa13">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_0dface82-01d1-4719-a3b2-8d1771c91aea" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_13c4dfdb-d9f3-4439-b4e0-60673fd7e333">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_21557908-bc9e-4d09-adcb-8d383aa96e21">
                      <gml:posList>550210.927999999370000000 5804606.320000000300000000 70.410015265325370000 550210.618545829670000000 5804606.679677059900000000 70.406257534324126000 550210.618545829320000000 5804606.679677060800000000 72.403278077454416000 550210.317999999970000000 5804607.029000000100000000 72.541660885469923000 550210.317999999970000000 5804607.029000000100000000 53.829999999999998000 550210.618545829670000000 5804606.679677059900000000 53.829999999999998000 550210.927999999370000000 5804606.320000000300000000 53.829999999999998000 550210.927999999370000000 5804606.320000000300000000 70.410015265325370000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="UUID_78c53e26-7b3f-41c7-8a70-8a6d636c68b1">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_925f33cf-c111-47aa-841f-4f860f5a7700" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_ef48c202-67ff-4b3f-a206-b4cec4214bcd">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_3a184f1a-bf38-4810-a47e-012d60645a3e">
                      <gml:posList>550210.046000000090000000 5804590.165000000000000000 53.829999999999998000 550213.168000001460000000 5804603.077999999700000000 53.829999999999998000 550212.772866203330000000 5804603.743693389000000000 53.829999999999998000 550212.761999998240000000 5804603.762000000100000000 53.829999999999998000 550212.203000001610000000 5804603.379999999900000000 53.829999999999998000 550210.396000001580000000 5804605.861999999700000000 53.829999999999998000 550210.927999999370000000 5804606.320000000300000000 53.829999999999998000 550210.618545829670000000 5804606.679677059900000000 53.829999999999998000 550210.317999999970000000 5804607.029000000100000000 53.829999999999998000 550205.092999998480000000 5804607.593999999600000000 53.829999999999998000 550205.634770259260000000 5804612.035723052900000000 53.829999999999998000 550206.186000000690000000 5804616.554999999700000000 53.829999999999998000 550220.126810311570000000 5804615.046060907700000000 53.829999999999998000 550220.228999998420000000 5804615.035000000100000000 53.829999999999998000 550221.825828812200000000 5804612.641716348000000000 53.829999999999998000 550223.829549887920000000 5804609.638593625300000000 53.829999999999998000 550223.896000001580000000 5804609.538999999900000000 53.829999999999998000 550218.975999999790000000 5804588.207000000400000000 53.829999999999998000 550214.203797197200000000 5804589.253357568800000000 53.829999999999998000 550210.046000000090000000 5804590.165000000000000000 53.829999999999998000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </cityObjectMember>

  <cityObjectMember>
    <bldg:Building gml:id="DENIAL43000053m6">
      <gml:boundedBy>
        <gml:Envelope srsDimension="3" srsName="urn:adv:crs:ETRS89_UTM32*DE_DHHN92_NH">
          <gml:lowerCorner>550139.875000 5804572.500000 52.819996
</gml:lowerCorner>
          <gml:upperCorner>550157.437500 5804588.000000 76.233910
</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <creationDate>2017-11-21</creationDate>
      <gen:doubleAttribute name="Volume">
        <gen:value>2777.967</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Profile">
        <gen:value>Sig3d</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCK">
        <gen:value>10</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOECKE">
        <gen:value>111010</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STBEZNR">
        <gen:value>111</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="MBZNR">
        <gen:value>1102</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="WBZNR">
        <gen:value>1102</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNAM">
        <gen:value>Vahrenwald</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTNR">
        <gen:value>11</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNAM">
        <gen:value>Vahrenwald-List</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBEZ">
        <gen:value>02</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEMEINDE">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SCALE">
        <gen:value>5000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SHAPE_Area">
        <gen:value>7165.30049704715</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ALKISID">
        <gen:value>DENIAL43000053m6</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ZMIN">
        <gen:value>52.82</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="THICKNESS">
        <gen:value>0.1</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="TEXTURE">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="BUILDINGID">
        <gen:value>DENIAL43000053m6</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ARTBEZEICH">
        <gen:value>Gebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Join_Cou_1">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="FUNKTION_T">
        <gen:value>Wohngebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="OBJFLAECHE">
        <gen:value>160.93</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADT">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEB_Funkti">
        <gen:value>1000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STRSCHL">
        <gen:value>00712</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STR_komple">
        <gen:value>Dörnbergstraße 5</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="PLZ_NUMMER">
        <gen:value>30161</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STR">
        <gen:value>Dörnbergstraße</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="HAUSNR">
        <gen:value>5</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="OVERHANG">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <app:appearance>
        <app:Appearance>
          <app:theme>RhinoCity ObliqueTexturing</app:theme>
          <app:surfaceDataMember>
            <app:ParameterizedTexture>
              <app:imageURI>Hannover_teil1_Appearance/DENIAL43000053m6.jpg</app:imageURI>
              <app:textureType>specific</app:textureType>
              <app:wrapMode>border</app:wrapMode>
              <app:borderColor>0 0 0 1</app:borderColor>
              <app:target uri="#UUID_a19dd274-f352-4b20-8149-b6881db84a86">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_6a11e390-7f3a-4efa-88b2-ce024b21ee06">0.857747 0.123588 0.837736 0.196258 0.743640 0.175616 0.808197 0.009665 0.818700 0.001957 0.838813 0.064610 0.857747 0.123588 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_53bb6e52-e83b-4118-827b-d54019602e68">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_4df40da9-d973-4af9-ac52-4af3b9c2134a">0.136986 0.579346 0.194447 0.575247 0.249517 0.573386 0.262866 0.736754 0.207539 0.741531 0.142314 0.631632 0.136986 0.579346 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_95ff949b-ae7d-4f0b-9f76-4f226d186648">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_3fa2a9f7-d524-4c78-afdd-2492d06724bf">0.071483 0.739184 0.013437 0.744411 0.001957 0.585115 0.131972 0.573386 0.071483 0.739184 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_063c7f52-d9dc-46e9-b57b-815ee5917b3a">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_f66183bc-23e8-4438-a344-2d8b3d863e2c">0.750318 0.558709 0.749319 0.455078 0.735812 0.456334 0.735868 0.411849 0.993251 0.383562 0.989678 0.533282 0.750318 0.558709 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_714127cc-17f0-4594-baec-3f8f09d84153">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_5ecd6aab-dcf6-41a5-83d2-2b77876fbe90">0.719618 0.383562 0.730305 0.538826 0.500978 0.560561 0.522988 0.487896 0.515485 0.399548 0.719618 0.383562 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_fd2fa3fd-ec45-4ca7-a0b1-bad23e6bcdb7">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_0d6c0506-2b12-43f8-951e-0aaf4985515a">0.334638 0.616490 0.334638 0.616988 0.334638 0.573386 0.334638 0.616490 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_2e6deec6-4ea0-40f0-9e70-8f00c8457200">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_b72fb289-5695-482e-85e2-716470594eeb">0.001957 0.401491 0.205310 0.383562 0.255078 0.550650 0.050403 0.568275 0.001957 0.401491 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_fe666a78-e605-4298-8d7f-a83529248f30">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_09a0a3d0-bfd7-47bd-bfab-0556c8e1f664">0.066134 0.379473 0.014777 0.322234 0.001957 0.166342 0.058152 0.001957 0.088174 0.105941 0.120518 0.217932 0.066134 0.379473 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_912e298d-28d0-4d88-97a4-c77493e5d9e1">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_f564af48-ed6c-406c-992e-362593cc2f49">0.495376 0.546739 0.291201 0.562727 0.260274 0.400609 0.462940 0.383562 0.495376 0.546739 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_3450543e-4264-41ed-b1a4-3c5b313da30e">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_c767dcd4-5e0e-4783-9f2b-8cb79bf05d1c">0.155474 0.227881 0.164090 0.316491 0.145254 0.257290 0.125245 0.194401 0.192428 0.001957 0.208863 0.054217 0.212170 0.064730 0.155474 0.227881 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_4b052596-05a6-414e-befd-8bcec923fc4d">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_7767726b-37df-4648-ac3f-f4579a14e073">0.618453 0.193485 0.611421 0.201168 0.480916 0.212923 0.444227 0.014458 0.573717 0.002709 0.582003 0.001957 0.618453 0.193485 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_cfbd32b3-2e62-4eb7-a009-8e04518e19ac">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_ee5f0f86-3445-432c-bfd2-7fea672e26a5">0.239048 0.280068 0.227689 0.147206 0.217221 0.024754 0.276050 0.001957 0.305435 0.119034 0.296993 0.260647 0.239048 0.280068 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_daec3a49-7da9-46e5-8340-6ce8cc584a46">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_12eb34f5-ac17-4d9f-b066-d7a98ad34644">0.624266 0.005031 0.678497 0.001957 0.739194 0.205324 0.684209 0.207189 0.624266 0.005031 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_44e67faf-29ae-42a4-9d30-0b6e60d70700">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_28628712-cb0b-4f2b-a1fb-64d0c241e1c6">0.370583 0.209114 0.311155 0.007299 0.322503 0.006412 0.379460 0.001957 0.439509 0.204186 0.382035 0.208295 0.383611 0.213639 0.372152 0.214441 0.370583 0.209114 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_4939a45c-3e27-4d33-8011-1e6d029cf5e0">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_df70d07d-8a25-4ce6-96c2-723c8b38a5fc">0.280511 0.691487 0.268552 0.596683 0.268102 0.593115 0.326659 0.573895 0.328212 0.573386 0.329187 0.676478 0.280511 0.691487 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
            </app:ParameterizedTexture>
          </app:surfaceDataMember>
        </app:Appearance>
      </app:appearance>
      <bldg:measuredHeight uom="#m">23.414</bldg:measuredHeight>
      <bldg:lod1Solid>
        <gml:Solid gml:id="UUID_fd69ef80-b146-449e-96b1-d3dae1089a24">
          <gml:exterior>
            <gml:CompositeSurface gml:id="UUID_2090d34b-7f9e-473f-96e1-2a0415936ca5">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_96e02631-da99-43c3-90e5-97b45233704b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_2c95468f-b54e-42cc-beb0-7e870ff596f4">
                      <gml:posList>550157.125000000000000000 5804586.477000000000000000 76.233908065937314000 550156.569800627420000000 5804580.877582113300000000 76.233908065937314000 550156.057000000030000000 5804575.701000000400000000 76.233908065937314000 550156.057000000030000000 5804575.701000000400000000 52.820000000000007000 550156.569800627420000000 5804580.877582113300000000 52.820000000000000000 550157.125000000000000000 5804586.477000000000000000 52.820000000000007000 550157.125000000000000000 5804586.477000000000000000 76.233908065937314000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_bd7b77cc-6be2-44ac-baa8-6fde34d59756">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_c0c3dd11-72e6-49e0-af4d-486e59fca6e1">
                      <gml:posList>550156.057000000030000000 5804575.701000000400000000 76.233908065937314000 550147.207999999870000000 5804576.570000000300000000 76.233908065937314000 550147.207999999870000000 5804576.570000000300000000 52.820000000000007000 550156.057000000030000000 5804575.701000000400000000 52.820000000000007000 550156.057000000030000000 5804575.701000000400000000 76.233908065937314000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_b078190c-7a00-4dea-b306-b1f302cdd88e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_0d5b42d1-afbf-41fc-b075-bcb3c4860416">
                      <gml:posList>550147.207999999870000000 5804576.570000000300000000 76.233908065937314000 550147.145295983300000000 5804576.046907668000000000 76.233908065937314000 550146.833999999330000000 5804573.450000000200000000 76.233908065937314000 550146.833999999330000000 5804573.450000000200000000 52.820000000000007000 550147.145295983300000000 5804576.046907668000000000 52.820000000000000000 550147.207999999870000000 5804576.570000000300000000 52.820000000000007000 550147.207999999870000000 5804576.570000000300000000 76.233908065937314000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_9cdc639b-8a30-4853-95fe-e7b7e3e8b344">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_5ad27329-6434-48f0-9dad-5ff6614349a7">
                      <gml:posList>550146.833999999330000000 5804573.450000000200000000 76.233908065937314000 550146.471739258850000000 5804573.488120051100000000 76.233908065937314000 550140.809000000360000000 5804574.083999999800000000 76.233908065937314000 550140.809000000360000000 5804574.083999999800000000 52.820000000000007000 550146.471739258850000000 5804573.488120051100000000 52.820000000000000000 550146.833999999330000000 5804573.450000000200000000 52.820000000000007000 550146.833999999330000000 5804573.450000000200000000 76.233908065937314000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_db52041a-3387-4f27-bfc7-9064b964103e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_344127ef-f0ec-4094-ad7d-29b54a097cb9">
                      <gml:posList>550141.682999998330000000 5804584.064000000200000000 52.820000000000007000 550141.228179318600000000 5804578.870509847100000000 52.820000000000007000 550140.809000000360000000 5804574.083999999800000000 52.820000000000007000 550140.809000000360000000 5804574.083999999800000000 76.233908065937314000 550141.228179318600000000 5804578.870509847100000000 76.233908065937314000 550141.682999998330000000 5804584.064000000200000000 76.233908065937314000 550141.682999998330000000 5804584.064000000200000000 52.820000000000007000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_5ec5a19b-0bef-4ec1-bac8-12697effc6e4">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_4ce73ba2-6cd1-40eb-ab09-d572b49397ba">
                      <gml:posList>550144.197000000630000000 5804583.892000000000000000 52.820000000000007000 550141.682999998330000000 5804584.064000000200000000 52.820000000000007000 550141.682999998330000000 5804584.064000000200000000 76.233908065937314000 550144.197000000630000000 5804583.892000000000000000 76.233908065937314000 550144.197000000630000000 5804583.892000000000000000 52.820000000000007000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_33a3ad65-6c69-41dc-b9a9-1baebf3d1e0c">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_ea3e74ef-ca9b-4c28-8d84-a03afeb56d1c">
                      <gml:posList>550147.346000000830000000 5804583.593999999600000000 52.820000000000007000 550146.822766851870000000 5804583.643515235700000000 52.820000000000000000 550144.197000000630000000 5804583.892000000000000000 52.820000000000007000 550144.197000000630000000 5804583.892000000000000000 76.233908065937314000 550146.822766851870000000 5804583.643515235700000000 76.233908065937314000 550147.346000000830000000 5804583.593999999600000000 76.233908065937314000 550147.346000000830000000 5804583.593999999600000000 52.820000000000007000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_22fa0999-da40-4931-b899-390325ac7226">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_f5cd0a79-8d1e-4b48-a2ae-e6bd7b2fb342">
                      <gml:posList>550147.846000000830000000 5804587.474999999600000000 52.820000000000007000 550147.364133913650000000 5804583.734755431300000000 52.820000000000000000 550147.346000000830000000 5804583.593999999600000000 52.820000000000007000 550147.346000000830000000 5804583.593999999600000000 76.233908065937314000 550147.364133913650000000 5804583.734755431300000000 76.233908065937314000 550147.846000000830000000 5804587.474999999600000000 76.233908065937314000 550147.846000000830000000 5804587.474999999600000000 52.820000000000007000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_b3809033-8d9b-42f7-9dc8-d0bb79e84247">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_6b992825-4af6-4e84-97c8-4dacc74e9fa9">
                      <gml:posList>550157.125000000000000000 5804586.477000000000000000 52.820000000000007000 550147.846000000830000000 5804587.474999999600000000 52.820000000000007000 550147.846000000830000000 5804587.474999999600000000 76.233908065937314000 550157.125000000000000000 5804586.477000000000000000 76.233908065937314000 550157.125000000000000000 5804586.477000000000000000 52.820000000000007000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_7763ef9c-47f4-4b3f-bbc0-a8d3ff01d136">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_e62f1ef2-eed4-4fd8-bca2-a3fa19e45f6d">
                      <gml:posList>550157.125000000000000000 5804586.477000000000000000 52.820000000000007000 550156.569800627420000000 5804580.877582113300000000 52.820000000000000000 550156.057000000030000000 5804575.701000000400000000 52.820000000000007000 550147.207999999870000000 5804576.570000000300000000 52.820000000000007000 550147.145295983300000000 5804576.046907668000000000 52.820000000000000000 550146.833999999330000000 5804573.450000000200000000 52.820000000000007000 550146.471739258850000000 5804573.488120051100000000 52.820000000000000000 550140.809000000360000000 5804574.083999999800000000 52.820000000000007000 550141.228179318600000000 5804578.870509847100000000 52.820000000000007000 550141.682999998330000000 5804584.064000000200000000 52.820000000000007000 550144.197000000630000000 5804583.892000000000000000 52.820000000000007000 550146.822766851870000000 5804583.643515235700000000 52.820000000000000000 550147.346000000830000000 5804583.593999999600000000 52.820000000000007000 550147.364133913650000000 5804583.734755431300000000 52.820000000000000000 550147.846000000830000000 5804587.474999999600000000 52.820000000000007000 550157.125000000000000000 5804586.477000000000000000 52.820000000000007000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_1e1a93b4-939c-491d-acff-beeb39bd6b7c">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_daa8a691-04c5-44af-ae3d-4d1b4a7e3756">
                      <gml:posList>550157.125000000000000000 5804586.477000000000000000 76.233908065937314000 550147.846000000830000000 5804587.474999999600000000 76.233908065937314000 550147.364133913650000000 5804583.734755431300000000 76.233908065937314000 550147.346000000830000000 5804583.593999999600000000 76.233908065937314000 550146.822766851870000000 5804583.643515235700000000 76.233908065937314000 550144.197000000630000000 5804583.892000000000000000 76.233908065937314000 550141.682999998330000000 5804584.064000000200000000 76.233908065937314000 550141.228179318600000000 5804578.870509847100000000 76.233908065937314000 550140.809000000360000000 5804574.083999999800000000 76.233908065937314000 550146.471739258850000000 5804573.488120051100000000 76.233908065937314000 550146.833999999330000000 5804573.450000000200000000 76.233908065937314000 550147.145295983300000000 5804576.046907668000000000 76.233908065937314000 550147.207999999870000000 5804576.570000000300000000 76.233908065937314000 550156.057000000030000000 5804575.701000000400000000 76.233908065937314000 550156.569800627420000000 5804580.877582113300000000 76.233908065937314000 550157.125000000000000000 5804586.477000000000000000 76.233908065937314000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod1Solid>
      <bldg:lod2Solid>
        <gml:Solid gml:id="UUID_8e280b25-ed38-40da-a405-03e836369c73">
          <gml:exterior>
            <gml:CompositeSurface gml:id="UUID_dac5f976-74f0-47fe-bb2c-7875569d4738">
              <gml:surfaceMember xlink:href="#UUID_a19dd274-f352-4b20-8149-b6881db84a86"/>
              <gml:surfaceMember xlink:href="#UUID_53bb6e52-e83b-4118-827b-d54019602e68"/>
              <gml:surfaceMember xlink:href="#UUID_95ff949b-ae7d-4f0b-9f76-4f226d186648"/>
              <gml:surfaceMember xlink:href="#UUID_063c7f52-d9dc-46e9-b57b-815ee5917b3a"/>
              <gml:surfaceMember xlink:href="#UUID_714127cc-17f0-4594-baec-3f8f09d84153"/>
              <gml:surfaceMember xlink:href="#UUID_fd2fa3fd-ec45-4ca7-a0b1-bad23e6bcdb7"/>
              <gml:surfaceMember xlink:href="#UUID_2e6deec6-4ea0-40f0-9e70-8f00c8457200"/>
              <gml:surfaceMember xlink:href="#UUID_fe666a78-e605-4298-8d7f-a83529248f30"/>
              <gml:surfaceMember xlink:href="#UUID_912e298d-28d0-4d88-97a4-c77493e5d9e1"/>
              <gml:surfaceMember xlink:href="#UUID_3450543e-4264-41ed-b1a4-3c5b313da30e"/>
              <gml:surfaceMember xlink:href="#UUID_4b052596-05a6-414e-befd-8bcec923fc4d"/>
              <gml:surfaceMember xlink:href="#UUID_cfbd32b3-2e62-4eb7-a009-8e04518e19ac"/>
              <gml:surfaceMember xlink:href="#UUID_daec3a49-7da9-46e5-8340-6ce8cc584a46"/>
              <gml:surfaceMember xlink:href="#UUID_44e67faf-29ae-42a4-9d30-0b6e60d70700"/>
              <gml:surfaceMember xlink:href="#UUID_4939a45c-3e27-4d33-8011-1e6d029cf5e0"/>
              <gml:surfaceMember xlink:href="#UUID_0a4d043e-ade8-45fb-a528-289056f0c66f"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_ba82fb6e-c83f-4a4c-83e5-4deb82474fbd">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_9189bf72-7b8d-4bff-a2eb-182cea751a53" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_a19dd274-f352-4b20-8149-b6881db84a86">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_6a11e390-7f3a-4efa-88b2-ce024b21ee06">
                      <gml:posList>550147.560936467610000000 5804579.514282840300000000 69.267417771194715000 550146.628250505660000000 5804581.982716375000000000 71.240468004145185000 550143.755486909300000000 5804578.604323529600000000 75.227422143895524000 550146.471739258850000000 5804573.488120051100000000 69.875846653014946000 550146.833999999330000000 5804573.450000000200000000 69.284738774441081000 550147.207999999870000000 5804576.570000000300000000 69.275827328264384000 550147.560936467610000000 5804579.514282840300000000 69.267417771194715000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_ffa06088-b039-41f0-8f03-b421e120599e">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_ee2e157e-cfb6-4d0b-8d92-995cfa92b432" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_53bb6e52-e83b-4118-827b-d54019602e68">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_4df40da9-d973-4af9-ac52-4af3b9c2134a">
                      <gml:posList>550146.822766851870000000 5804583.643515235700000000 69.418894128261570000 550144.197000000630000000 5804583.892000000000000000 69.448099675943041000 550141.682999998330000000 5804584.064000000200000000 69.547472598058519000 550141.228179318600000000 5804578.870509847100000000 75.226287974938373000 550143.755486909300000000 5804578.604323529600000000 75.227422143895524000 550146.628250505660000000 5804581.982716375000000000 71.240468004145185000 550146.822766851870000000 5804583.643515235700000000 69.418894128261570000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_0fe10ee2-09d9-4344-b1b7-bf993b74cd4b">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_533153a4-e13f-4432-8f7a-ee4830791b7f" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_95ff949b-ae7d-4f0b-9f76-4f226d186648">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_3fa2a9f7-d524-4c78-afdd-2492d06724bf">
                      <gml:posList>550143.755486909300000000 5804578.604323529600000000 75.227422143895524000 550141.228179318600000000 5804578.870509847100000000 75.226287974938373000 550140.809000000360000000 5804574.083999999800000000 69.872703885848750000 550146.471739258850000000 5804573.488120051100000000 69.875846653014946000 550143.755486909300000000 5804578.604323529600000000 75.227422143895524000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_035a36c9-8175-4aee-919a-4e047e48e727">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_321bdae8-7643-4059-984a-5d4ee4e55073" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_063c7f52-d9dc-46e9-b57b-815ee5917b3a">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_f66183bc-23e8-4438-a344-2d8b3d863e2c">
                      <gml:posList>550147.846000000830000000 5804587.474999999600000000 66.606408736375698000 550147.346000000830000000 5804583.593999999600000000 69.847934394642408000 550146.822766851870000000 5804583.643515235700000000 69.855101763069555000 550146.628250505660000000 5804581.982716375000000000 71.240468004145185000 550156.569800627420000000 5804580.877582113300000000 71.239602681256258000 550157.125000000000000000 5804586.477000000000000000 66.578030250151187000 550147.846000000830000000 5804587.474999999600000000 66.606408736375698000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_e255b7cf-4ab4-4f17-b11c-5f3517fb06e8">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_bac8b566-52f4-4a0d-ac16-5a1a2eb31079" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_714127cc-17f0-4594-baec-3f8f09d84153">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_5ecd6aab-dcf6-41a5-83d2-2b77876fbe90">
                      <gml:posList>550156.057000000030000000 5804575.701000000400000000 66.873144356486648000 550156.569800627420000000 5804580.877582113300000000 71.239602681256258000 550146.628250505660000000 5804581.982716375000000000 71.240468004145185000 550147.560936467610000000 5804579.514282840300000000 69.267417771194715000 550147.208000000220000000 5804576.570000000300000000 66.778230580588328000 550156.057000000030000000 5804575.701000000400000000 66.873144356486648000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_f5fca4a7-ac07-483a-b135-d0338ee7a7e0">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_64d78e97-0870-42a9-8023-3259443f302d" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_fd2fa3fd-ec45-4ca7-a0b1-bad23e6bcdb7">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_0d6c0506-2b12-43f8-951e-0aaf4985515a">
                      <gml:posList>550146.822766851870000000 5804583.643515235700000000 69.855101763069555000 550146.822766851870000000 5804583.643515235700000000 69.418894128261570000 550146.628250505660000000 5804581.982716375000000000 71.240468004145185000 550146.822766851870000000 5804583.643515235700000000 69.855101763069555000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_5184653e-fac1-425f-bcb7-99628ca85961">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_ca7d24f6-9f1c-4e54-8726-14d2598c9227" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_2e6deec6-4ea0-40f0-9e70-8f00c8457200">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_b72fb289-5695-482e-85e2-716470594eeb">
                      <gml:posList>550157.125000000000000000 5804586.477000000000000000 52.820000000000007000 550147.846000000830000000 5804587.474999999600000000 52.820000000000007000 550147.846000000830000000 5804587.474999999600000000 66.606408736375698000 550157.125000000000000000 5804586.477000000000000000 66.578030250151187000 550157.125000000000000000 5804586.477000000000000000 52.820000000000007000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_a0a00cdc-e8a3-402f-953c-36547f482cea">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_9aae046e-ae73-46a7-8a85-b21e1d55dde5" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_fe666a78-e605-4298-8d7f-a83529248f30">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_09a0a3d0-bfd7-47bd-bfab-0556c8e1f664">
                      <gml:posList>550157.125000000000000000 5804586.477000000000000000 66.578030250151187000 550156.569800627420000000 5804580.877582113300000000 71.239602681256258000 550156.057000000030000000 5804575.701000000400000000 66.873144356486648000 550156.057000000030000000 5804575.701000000400000000 52.820000000000007000 550156.569800627420000000 5804580.877582113300000000 52.820000000000000000 550157.125000000000000000 5804586.477000000000000000 52.820000000000007000 550157.125000000000000000 5804586.477000000000000000 66.578030250151187000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_66f85276-3e98-4652-8bb8-fbf0cb20bf72">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_1bd75461-5ee1-4fc9-bf5a-22d651a14740" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_912e298d-28d0-4d88-97a4-c77493e5d9e1">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_f564af48-ed6c-406c-992e-362593cc2f49">
                      <gml:posList>550156.057000000030000000 5804575.701000000400000000 66.873144356486648000 550147.208000000220000000 5804576.570000000300000000 66.778230580588328000 550147.207999999870000000 5804576.570000000300000000 52.820000000000007000 550156.057000000030000000 5804575.701000000400000000 52.820000000000007000 550156.057000000030000000 5804575.701000000400000000 66.873144356486648000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_eabd17f9-55cf-448a-b7f9-d7576f941666">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_bf1b722d-45bc-4c58-8b8b-feaad0c966e6" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_3450543e-4264-41ed-b1a4-3c5b313da30e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_c767dcd4-5e0e-4783-9f2b-8cb79bf05d1c">
                      <gml:posList>550147.208000000220000000 5804576.570000000300000000 66.778230580588328000 550147.560936467610000000 5804579.514282840300000000 69.267417771194715000 550147.207999999870000000 5804576.570000000300000000 69.275827328264384000 550146.833999999330000000 5804573.450000000200000000 69.284738774441081000 550146.833999999330000000 5804573.450000000200000000 52.820000000000007000 550147.145295983300000000 5804576.046907668000000000 52.820000000000000000 550147.207999999870000000 5804576.570000000300000000 52.820000000000007000 550147.208000000220000000 5804576.570000000300000000 66.778230580588328000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_37f6d5e6-bfb7-4c13-a8af-4f13ac9dfea5">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_5930c5f8-5642-4808-8f7b-bb8f0337b426" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_4b052596-05a6-414e-befd-8bcec923fc4d">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_7767726b-37df-4648-ac3f-f4579a14e073">
                      <gml:posList>550146.833999999330000000 5804573.450000000200000000 69.284738774441081000 550146.471739258850000000 5804573.488120051100000000 69.875846653014946000 550140.809000000360000000 5804574.083999999800000000 69.872703885848750000 550140.809000000360000000 5804574.083999999800000000 52.820000000000007000 550146.471739258850000000 5804573.488120051100000000 52.820000000000000000 550146.833999999330000000 5804573.450000000200000000 52.820000000000007000 550146.833999999330000000 5804573.450000000200000000 69.284738774441081000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_1eb98f6d-2b45-4ef7-a783-7ff443026629">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_11dfff18-ae21-42a4-94e8-752191a22e83" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_cfbd32b3-2e62-4eb7-a009-8e04518e19ac">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_ee5f0f86-3445-432c-bfd2-7fea672e26a5">
                      <gml:posList>550141.682999998330000000 5804584.064000000200000000 52.820000000000007000 550141.228179318600000000 5804578.870509847100000000 52.820000000000007000 550140.809000000360000000 5804574.083999999800000000 52.820000000000007000 550140.809000000360000000 5804574.083999999800000000 69.872703885848750000 550141.228179318600000000 5804578.870509847100000000 75.226287974938373000 550141.682999998330000000 5804584.064000000200000000 69.547472598058519000 550141.682999998330000000 5804584.064000000200000000 52.820000000000007000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_0797c5be-4150-43b4-b481-878bb5d9ee14">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_a7e2b6f8-5241-4500-a953-70632ebc7515" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_daec3a49-7da9-46e5-8340-6ce8cc584a46">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_12eb34f5-ac17-4d9f-b066-d7a98ad34644">
                      <gml:posList>550144.197000000630000000 5804583.892000000000000000 52.820000000000007000 550141.682999998330000000 5804584.064000000200000000 52.820000000000007000 550141.682999998330000000 5804584.064000000200000000 69.547472598058519000 550144.197000000630000000 5804583.892000000000000000 69.448099675943041000 550144.197000000630000000 5804583.892000000000000000 52.820000000000007000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_a2e5e40c-d19b-4dc8-a253-f191aa0e999d">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_54184d3c-5938-4f64-bc27-64917eb8febe" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_44e67faf-29ae-42a4-9d30-0b6e60d70700">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_28628712-cb0b-4f2b-a1fb-64d0c241e1c6">
                      <gml:posList>550147.346000000830000000 5804583.593999999600000000 69.413074376720587000 550147.346000000830000000 5804583.593999999600000000 52.820000000000007000 550146.822766851870000000 5804583.643515235700000000 52.820000000000000000 550144.197000000630000000 5804583.892000000000000000 52.820000000000007000 550144.197000000630000000 5804583.892000000000000000 69.448099675943041000 550146.822766851870000000 5804583.643515235700000000 69.418894128261570000 550146.822766851870000000 5804583.643515235700000000 69.855101763069555000 550147.346000000830000000 5804583.593999999600000000 69.847934394642408000 550147.346000000830000000 5804583.593999999600000000 69.413074376720587000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_281aae3e-1775-4137-a376-9b41038cd750">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_f4a60b92-cb2e-4433-a411-febee09ba233" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_4939a45c-3e27-4d33-8011-1e6d029cf5e0">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_df70d07d-8a25-4ce6-96c2-723c8b38a5fc">
                      <gml:posList>550147.846000000830000000 5804587.474999999600000000 52.820000000000007000 550147.364133913650000000 5804583.734755431300000000 52.820000000000000000 550147.346000000830000000 5804583.593999999600000000 52.820000000000007000 550147.346000000830000000 5804583.593999999600000000 69.413074376720587000 550147.346000000830000000 5804583.593999999600000000 69.847934394642408000 550147.846000000830000000 5804587.474999999600000000 66.606408736375698000 550147.846000000830000000 5804587.474999999600000000 52.820000000000007000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="UUID_59fae097-c554-45c8-80d0-89a1aafe4bd8">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_460c36d2-1e29-4c8c-9100-618beccb5685" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_0a4d043e-ade8-45fb-a528-289056f0c66f">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_5f9d0a84-1345-4c1c-bac4-c4bd650c818a">
                      <gml:posList>550157.125000000000000000 5804586.477000000000000000 52.820000000000007000 550156.569800627420000000 5804580.877582113300000000 52.820000000000000000 550156.057000000030000000 5804575.701000000400000000 52.820000000000007000 550147.207999999870000000 5804576.570000000300000000 52.820000000000007000 550147.145295983300000000 5804576.046907668000000000 52.820000000000000000 550146.833999999330000000 5804573.450000000200000000 52.820000000000007000 550146.471739258850000000 5804573.488120051100000000 52.820000000000000000 550140.809000000360000000 5804574.083999999800000000 52.820000000000007000 550141.228179318600000000 5804578.870509847100000000 52.820000000000007000 550141.682999998330000000 5804584.064000000200000000 52.820000000000007000 550144.197000000630000000 5804583.892000000000000000 52.820000000000007000 550146.822766851870000000 5804583.643515235700000000 52.820000000000000000 550147.346000000830000000 5804583.593999999600000000 52.820000000000007000 550147.364133913650000000 5804583.734755431300000000 52.820000000000000000 550147.846000000830000000 5804587.474999999600000000 52.820000000000007000 550157.125000000000000000 5804586.477000000000000000 52.820000000000007000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </cityObjectMember>

  <cityObjectMember>
    <bldg:Building gml:id="DENIAL43000052PW">
      <gml:boundedBy>
        <gml:Envelope srsDimension="3" srsName="urn:adv:crs:ETRS89_UTM32*DE_DHHN92_NH">
          <gml:lowerCorner>550187.687500 5804597.500000 53.649998
</gml:lowerCorner>
          <gml:upperCorner>550206.562500 5804619.000000 72.351295
</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <creationDate>2017-11-21</creationDate>
      <gen:doubleAttribute name="Volume">
        <gen:value>3515.887</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Profile">
        <gen:value>Sig3d</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCK">
        <gen:value>10</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOECKE">
        <gen:value>111010</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STBEZNR">
        <gen:value>111</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="MBZNR">
        <gen:value>1102</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="WBZNR">
        <gen:value>1102</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNAM">
        <gen:value>Vahrenwald</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTNR">
        <gen:value>11</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNAM">
        <gen:value>Vahrenwald-List</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBEZ">
        <gen:value>02</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEMEINDE">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SCALE">
        <gen:value>5000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SHAPE_Area">
        <gen:value>7165.30049704715</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ALKISID">
        <gen:value>DENIAL43000052PW</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ZMIN">
        <gen:value>53.65</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="THICKNESS">
        <gen:value>0.1</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="TEXTURE">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="BUILDINGID">
        <gen:value>DENIAL43000052PW</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ARTBEZEICH">
        <gen:value>Gebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Join_Cou_1">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="FUNKTION_T">
        <gen:value>Wohngebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="OBJFLAECHE">
        <gen:value>224.24</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADT">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEB_Funkti">
        <gen:value>1000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STRSCHL">
        <gen:value>02557</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STR_komple">
        <gen:value>Seydlitzstraße 33</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="PLZ_NUMMER">
        <gen:value>30161</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STR">
        <gen:value>Seydlitzstraße</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="HAUSNR">
        <gen:value>33</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="OVERHANG">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <app:appearance>
        <app:Appearance>
          <app:theme>RhinoCity ObliqueTexturing</app:theme>
          <app:surfaceDataMember>
            <app:ParameterizedTexture>
              <app:imageURI>Hannover_teil1_Appearance/DENIAL43000052PW.jpg</app:imageURI>
              <app:textureType>specific</app:textureType>
              <app:wrapMode>border</app:wrapMode>
              <app:borderColor>0 0 0 1</app:borderColor>
              <app:target uri="#UUID_7f9d00f3-4930-4ed6-899a-0db0bb1f8435">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_587d1bcd-253f-4e27-aa9d-511e4c4b12d4">0.908313 0.813086 0.855163 0.761575 0.853229 0.742607 0.955994 0.737769 0.908313 0.813086 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_a8950299-ad1d-4fbc-bc18-bbe1938632ed">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_43fe2b1e-33bd-4fe3-a025-82a548c45d17">0.583266 0.540010 0.648397 0.533981 0.652410 0.580795 0.702716 0.620017 0.745628 0.572929 0.741546 0.525359 0.793516 0.520548 0.807303 0.685163 0.426361 0.717146 0.410959 0.555960 0.490171 0.548627 0.494065 0.594156 0.544337 0.633368 0.587231 0.586295 0.583266 0.540010 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_e4efab5b-62dd-4ff8-a908-0358e27e0868">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_fd7fba81-e09d-4e26-91dc-e8ad72c421d6">0.336255 0.042236 0.352866 0.233992 0.260753 0.179963 0.256360 0.175703 0.288976 0.144599 0.268637 0.001957 0.336255 0.042236 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_abaa4dc6-fb78-4755-b70c-8961530e36f6">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_6b091537-6ccb-4cfd-911c-46cfa28f98fd">0.448526 0.138606 0.444797 0.148249 0.395940 0.225806 0.358121 0.079841 0.404010 0.003263 0.406656 0.001957 0.448526 0.138606 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_d70fdd24-ce27-4c61-8b56-70f418960a1f">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_2c2d5bb9-a4c9-46b1-aa72-a6f1528e0639">0.385591 0.531705 0.389882 0.520548 0.395925 0.586442 0.406323 0.699787 0.015632 0.733196 0.001957 0.560408 0.257339 0.534626 0.258118 0.533553 0.261639 0.538919 0.334948 0.609221 0.385591 0.531705 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_0102801b-820f-49ae-8794-04187fc2b455">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_e1cb33d3-cfd7-41cc-9a01-638cfa49aba4">0.789966 0.807324 0.743724 0.812211 0.735812 0.737769 0.785925 0.769295 0.789966 0.807324 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_1ad05f06-223d-4b20-b63c-a7371de644cb">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_137b3a8e-4548-465e-8489-383087f72e9f">0.730387 0.812344 0.671449 0.818832 0.667319 0.780109 0.722433 0.737769 0.730387 0.812344 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_65165d48-fcf7-4e63-8dcd-2f31953e0579">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_6de41ded-73ff-4cdc-8c31-a9efefdb0659">0.848958 0.808369 0.802545 0.813275 0.794521 0.737769 0.844803 0.769277 0.848958 0.808369 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_72497172-5b11-472b-ab01-d678e60b373e">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_16394f5f-6f6b-435f-83ea-dc02d0b40438">0.661837 0.813410 0.603069 0.819880 0.598826 0.780092 0.653770 0.737769 0.661837 0.813410 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_049bb1e2-934a-41ca-a097-7c5049d0a8e9">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_36989674-fb13-4a73-b24c-5c696bb02cd7">0.960861 0.777484 0.962687 0.737769 0.965966 0.776653 0.960861 0.777484 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_fa79d01f-fc89-4c3f-949f-d14715740f9e">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_c135d526-7a30-40f0-bdbb-1dbfd2f794be">0.014275 0.933145 0.009590 0.950073 0.001957 0.921722 0.014275 0.933145 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_50f768f2-45df-462a-a891-411ff1a02a3f">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_4f074939-3d85-4dbe-a46c-73dc602f32c1">0.970646 0.776352 0.972565 0.737769 0.975756 0.775527 0.970646 0.776352 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_f8e3b030-5c55-4d18-a359-b58621d97ea8">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_9bcccccd-8974-451b-931c-653fd05b2b6a">0.992004 0.748902 0.987626 0.765366 0.980431 0.737769 0.992004 0.748902 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_cae6f483-41db-4950-b7b0-22899fe7377a">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_d0a14baf-30cb-43c9-aa8b-f8721dcffb30">0.019873 0.924649 0.019569 0.923605 0.024256 0.921722 0.019873 0.924649 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_16e9195f-9b73-40c6-bb53-de462e97231b">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_f813805d-976f-4daa-be29-7cf85983b2db">0.029354 0.921722 0.032125 0.921722 0.032019 0.921722 0.029354 0.921722 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_827f4406-663a-4632-a069-8e28686ea0cc">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_7fd910af-e679-4bb2-82d2-79120153c4bb">0.290597 0.890451 0.035704 0.916190 0.001957 0.762032 0.255671 0.737769 0.290597 0.890451 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_e30edb51-0a74-43fb-bba6-73f2d4f4f203">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_b4c20e5a-4972-4a47-9be2-4a8dd9b336b7">0.187520 0.293459 0.173246 0.154848 0.158513 0.011786 0.214442 0.001957 0.252290 0.142805 0.243837 0.286149 0.187520 0.293459 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_b6389501-cebf-4881-8f9a-277b18937a09">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_6801b45b-f0da-41ca-806a-a4f0e2c25cd8">0.108093 0.516287 0.059443 0.474343 0.050067 0.360572 0.044618 0.294430 0.044177 0.295951 0.001957 0.158636 0.047905 0.001957 0.088907 0.141134 0.089005 0.141467 0.101338 0.183326 0.122503 0.255154 0.153046 0.358804 0.108093 0.516287 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_06648ffe-ea77-4b1a-88b4-90672d9dc5b9">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_ef679bd0-fef6-408a-a6a6-e369ec2a7862">0.594836 0.892930 0.592719 0.894225 0.489449 0.899127 0.454012 0.744791 0.556211 0.737928 0.558579 0.737769 0.594836 0.892930 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_439c74aa-0629-4f79-b578-18051196a281">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_d8af25a6-bb03-4c29-90c6-1eabd5b3031f">0.387255 0.906676 0.368125 0.765633 0.365949 0.749594 0.424655 0.737769 0.429444 0.753437 0.449718 0.895643 0.387255 0.906676 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_ffd49219-a0d5-42f1-8786-683b9b9055c5">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_29aad996-a2cd-4ae0-8516-29df5071a769">0.360544 0.901937 0.330589 0.913880 0.330417 0.913110 0.329656 0.914181 0.295499 0.761518 0.296335 0.760811 0.323605 0.737769 0.360544 0.901937 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_8b66ec78-f775-4bfe-9582-4cf7bb74c646">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_728bbf35-24ae-4a6a-9c2f-ee7486c74027">0.575006 0.188442 0.495769 0.195780 0.454012 0.036124 0.532855 0.029050 0.579183 0.024893 0.625519 0.020735 0.690348 0.014918 0.736704 0.010759 0.783068 0.006599 0.834799 0.001957 0.878447 0.160337 0.826460 0.165152 0.831527 0.183664 0.789435 0.204649 0.738207 0.192004 0.733282 0.173782 0.668131 0.179816 0.672958 0.197836 0.630798 0.218823 0.579694 0.206172 0.575006 0.188442 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
            </app:ParameterizedTexture>
          </app:surfaceDataMember>
        </app:Appearance>
      </app:appearance>
      <bldg:measuredHeight uom="#m">18.701</bldg:measuredHeight>
      <bldg:lod1Solid>
        <gml:Solid gml:id="UUID_02d862ab-50a4-4d40-bcd9-70898ed6801f">
          <gml:exterior>
            <gml:CompositeSurface gml:id="UUID_3892994f-4042-4bbb-b34e-f5ccf047c003">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_82c251ce-f52a-4ccd-92ce-85540be0749b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_71e906d7-5082-4c59-90cb-9ca03420620a">
                      <gml:posList>550206.186000000690000000 5804616.554999999700000000 72.351290019230092000 550205.539451585850000000 5804611.254249465700000000 72.351290019230092000 550205.092999998480000000 5804607.593999999600000000 72.351290019230092000 550204.833542767910000000 5804605.465802643400000000 72.351290019230092000 550204.831481905190000000 5804605.448898421600000000 72.351290019230092000 550203.971999999140000000 5804598.399000000200000000 72.351290019230092000 550203.971999999140000000 5804598.399000000200000000 53.650000000000006000 550204.831481905190000000 5804605.448898421600000000 53.649999999999999000 550204.833542767910000000 5804605.465802643400000000 53.649999999999999000 550205.092999998480000000 5804607.593999999600000000 53.649999999999999000 550205.539451585850000000 5804611.254249465700000000 53.649999999999999000 550206.186000000690000000 5804616.554999999700000000 53.650000000000006000 550206.186000000690000000 5804616.554999999700000000 72.351290019230092000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_635a5cb5-91e7-4954-943d-9474b372354e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_f1650e6b-bf8c-4e11-8904-bea96a47cf3c">
                      <gml:posList>550203.971999999140000000 5804598.399000000200000000 72.351290019230092000 550203.867105879700000000 5804598.407310911500000000 72.351290019230092000 550199.339999999850000000 5804598.765999999800000000 72.351290019230092000 550199.339999999850000000 5804598.765999999800000000 53.650000000000006000 550203.867105879700000000 5804598.407310911500000000 53.649999999999999000 550203.971999999140000000 5804598.399000000200000000 53.650000000000006000 550203.971999999140000000 5804598.399000000200000000 72.351290019230092000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_84a428ca-cf09-4bdf-8d4f-5f8dca706e95">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_5b78c703-03e0-4c9c-b6a9-2922f834ceae">
                      <gml:posList>550200.190999999640000000 5804604.929999999700000000 53.650000000000006000 550199.426887915820000000 5804599.395350311000000000 53.649999999999999000 550199.339999999850000000 5804598.765999999800000000 53.650000000000006000 550199.339999999850000000 5804598.765999999800000000 72.351290019230092000 550199.426887915820000000 5804599.395350311000000000 72.351290019230092000 550200.190999999640000000 5804604.929999999700000000 72.351290019230092000 550200.190999999640000000 5804604.929999999700000000 53.650000000000006000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_4b103e68-2007-4f83-b6a9-3df06a559b63">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_cd9072de-15f6-4c34-a95c-080163284cf7">
                      <gml:posList>550200.190999999640000000 5804604.929999999700000000 72.351290019230092000 550199.080086976870000000 5804606.107858873000000000 72.351290019230092000 550199.046000000090000000 5804606.144000000300000000 72.351290019230092000 550199.046000000090000000 5804606.144000000300000000 53.650000000000006000 550199.080086976870000000 5804606.107858873000000000 53.649999999999999000 550200.190999999640000000 5804604.929999999700000000 53.650000000000006000 550200.190999999640000000 5804604.929999999700000000 72.351290019230092000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_273a4203-6a37-4447-a337-de26e23ddec7">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_7f8c05cf-e938-4c2b-b272-8622e8530492">
                      <gml:posList>550199.046000000090000000 5804606.144000000300000000 72.351290019230092000 550187.886999998240000000 5804607.407999999800000000 72.351290019230092000 550187.886999998240000000 5804607.407999999800000000 53.650000000000006000 550199.046000000090000000 5804606.144000000300000000 53.650000000000006000 550199.046000000090000000 5804606.144000000300000000 72.351290019230092000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_50263abd-5e6d-43cc-a449-f1ff0c86eb15">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_41c05952-915c-406b-a374-3ee7430d01ad">
                      <gml:posList>550189.045000001790000000 5804618.409000000000000000 53.650000000000006000 550188.475150630230000000 5804612.995430986400000000 53.649999999999999000 550187.886999998240000000 5804607.407999999800000000 53.650000000000006000 550187.886999998240000000 5804607.407999999800000000 72.351290019230092000 550188.475150630230000000 5804612.995430986400000000 72.351290019230092000 550189.045000001790000000 5804618.409000000000000000 72.351290019230092000 550189.045000001790000000 5804618.409000000000000000 53.650000000000006000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_3ddda0ae-61bd-4b99-b0d2-3ffcc3e0ce03">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_f7960ef1-287d-4fb5-bd29-5fca1a55f5bd">
                      <gml:posList>550206.186000000690000000 5804616.554999999700000000 53.650000000000006000 550202.634870991110000000 5804616.939096210500000000 53.649999999999999000 550200.548733029980000000 5804617.164736475800000000 53.649999999999999000 550198.462595068850000000 5804617.390376741100000000 53.649999999999999000 550195.544480212150000000 5804617.706005055500000000 53.649999999999999000 550193.458342251020000000 5804617.931645320700000000 53.650000000000006000 550191.372204289890000000 5804618.157285586000000000 53.649999999999999000 550189.045000001790000000 5804618.409000000000000000 53.650000000000006000 550189.045000001790000000 5804618.409000000000000000 72.351290019230092000 550191.372204289890000000 5804618.157285586000000000 72.351290019230092000 550193.458342251020000000 5804617.931645320700000000 72.351290019230092000 550195.544480212150000000 5804617.706005055500000000 72.351290019230092000 550198.462595068850000000 5804617.390376741100000000 72.351290019230092000 550200.548733029980000000 5804617.164736475800000000 72.351290019230092000 550202.634870991110000000 5804616.939096210500000000 72.351290019230092000 550206.186000000690000000 5804616.554999999700000000 72.351290019230092000 550206.186000000690000000 5804616.554999999700000000 53.650000000000006000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_fe44456e-ebb9-43e7-8efb-3c61a6ffa3c8">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_f1efe525-89e6-4e9e-8448-4c45e12b64cc">
                      <gml:posList>550205.092999998480000000 5804607.593999999600000000 53.649999999999999000 550204.833542767910000000 5804605.465802643400000000 53.649999999999999000 550204.831481905190000000 5804605.448898421600000000 53.649999999999999000 550203.971999999140000000 5804598.399000000200000000 53.650000000000006000 550203.867105879700000000 5804598.407310911500000000 53.649999999999999000 550199.339999999850000000 5804598.765999999800000000 53.650000000000006000 550199.426887915820000000 5804599.395350311000000000 53.649999999999999000 550200.190999999640000000 5804604.929999999700000000 53.650000000000006000 550199.080086976870000000 5804606.107858873000000000 53.649999999999999000 550199.046000000090000000 5804606.144000000300000000 53.650000000000006000 550187.886999998240000000 5804607.407999999800000000 53.650000000000006000 550188.475150630230000000 5804612.995430986400000000 53.649999999999999000 550189.045000001790000000 5804618.409000000000000000 53.650000000000006000 550191.372204289890000000 5804618.157285586000000000 53.649999999999999000 550193.458342251020000000 5804617.931645320700000000 53.650000000000006000 550195.544480212150000000 5804617.706005055500000000 53.649999999999999000 550198.462595068850000000 5804617.390376741100000000 53.649999999999999000 550200.548733029980000000 5804617.164736475800000000 53.649999999999999000 550202.634870991110000000 5804616.939096210500000000 53.649999999999999000 550206.186000000690000000 5804616.554999999700000000 53.650000000000006000 550205.539451585850000000 5804611.254249465700000000 53.649999999999999000 550205.092999998480000000 5804607.593999999600000000 53.649999999999999000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_91eb68ad-339c-4679-8204-8622c6a7ece7">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_c33595e4-6942-45bd-9096-47581fa4f7a7">
                      <gml:posList>550205.092999998480000000 5804607.593999999600000000 72.351290019230092000 550205.539451585850000000 5804611.254249465700000000 72.351290019230092000 550206.186000000690000000 5804616.554999999700000000 72.351290019230092000 550202.634870991110000000 5804616.939096210500000000 72.351290019230092000 550200.548733029980000000 5804617.164736475800000000 72.351290019230092000 550198.462595068850000000 5804617.390376741100000000 72.351290019230092000 550195.544480212150000000 5804617.706005055500000000 72.351290019230092000 550193.458342251020000000 5804617.931645320700000000 72.351290019230092000 550191.372204289890000000 5804618.157285586000000000 72.351290019230092000 550189.045000001790000000 5804618.409000000000000000 72.351290019230092000 550188.475150630230000000 5804612.995430986400000000 72.351290019230092000 550187.886999998240000000 5804607.407999999800000000 72.351290019230092000 550199.046000000090000000 5804606.144000000300000000 72.351290019230092000 550199.080086976870000000 5804606.107858873000000000 72.351290019230092000 550200.190999999640000000 5804604.929999999700000000 72.351290019230092000 550199.426887915820000000 5804599.395350311000000000 72.351290019230092000 550199.339999999850000000 5804598.765999999800000000 72.351290019230092000 550203.867105879700000000 5804598.407310911500000000 72.351290019230092000 550203.971999999140000000 5804598.399000000200000000 72.351290019230092000 550204.831481905190000000 5804605.448898421600000000 72.351290019230092000 550204.833542767910000000 5804605.465802643400000000 72.351290019230092000 550205.092999998480000000 5804607.593999999600000000 72.351290019230092000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod1Solid>
      <bldg:lod2Solid>
        <gml:Solid gml:id="UUID_b7eae7ef-7ef0-4e17-9a37-f6b85201a1b8">
          <gml:exterior>
            <gml:CompositeSurface gml:id="UUID_0aebcb4a-9c4e-4fcf-b357-8f435c76c729">
              <gml:surfaceMember xlink:href="#UUID_7f9d00f3-4930-4ed6-899a-0db0bb1f8435"/>
              <gml:surfaceMember xlink:href="#UUID_a8950299-ad1d-4fbc-bc18-bbe1938632ed"/>
              <gml:surfaceMember xlink:href="#UUID_e4efab5b-62dd-4ff8-a908-0358e27e0868"/>
              <gml:surfaceMember xlink:href="#UUID_abaa4dc6-fb78-4755-b70c-8961530e36f6"/>
              <gml:surfaceMember xlink:href="#UUID_d70fdd24-ce27-4c61-8b56-70f418960a1f"/>
              <gml:surfaceMember xlink:href="#UUID_0102801b-820f-49ae-8794-04187fc2b455"/>
              <gml:surfaceMember xlink:href="#UUID_1ad05f06-223d-4b20-b63c-a7371de644cb"/>
              <gml:surfaceMember xlink:href="#UUID_65165d48-fcf7-4e63-8dcd-2f31953e0579"/>
              <gml:surfaceMember xlink:href="#UUID_72497172-5b11-472b-ab01-d678e60b373e"/>
              <gml:surfaceMember xlink:href="#UUID_049bb1e2-934a-41ca-a097-7c5049d0a8e9"/>
              <gml:surfaceMember xlink:href="#UUID_fa79d01f-fc89-4c3f-949f-d14715740f9e"/>
              <gml:surfaceMember xlink:href="#UUID_50f768f2-45df-462a-a891-411ff1a02a3f"/>
              <gml:surfaceMember xlink:href="#UUID_f8e3b030-5c55-4d18-a359-b58621d97ea8"/>
              <gml:surfaceMember xlink:href="#UUID_cae6f483-41db-4950-b7b0-22899fe7377a"/>
              <gml:surfaceMember xlink:href="#UUID_16e9195f-9b73-40c6-bb53-de462e97231b"/>
              <gml:surfaceMember xlink:href="#UUID_827f4406-663a-4632-a069-8e28686ea0cc"/>
              <gml:surfaceMember xlink:href="#UUID_e30edb51-0a74-43fb-bba6-73f2d4f4f203"/>
              <gml:surfaceMember xlink:href="#UUID_b6389501-cebf-4881-8f9a-277b18937a09"/>
              <gml:surfaceMember xlink:href="#UUID_06648ffe-ea77-4b1a-88b4-90672d9dc5b9"/>
              <gml:surfaceMember xlink:href="#UUID_439c74aa-0629-4f79-b578-18051196a281"/>
              <gml:surfaceMember xlink:href="#UUID_ffd49219-a0d5-42f1-8786-683b9b9055c5"/>
              <gml:surfaceMember xlink:href="#UUID_8b66ec78-f775-4bfe-9582-4cf7bb74c646"/>
              <gml:surfaceMember xlink:href="#UUID_3d7d8088-a06e-48bb-a041-9e3c4d9e0b45"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_6fd7e247-28d2-4726-b880-e3631971a426">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_a8cea70f-d0d7-4e12-a446-d345cbf7f74f" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_7f9d00f3-4930-4ed6-899a-0db0bb1f8435">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_587d1bcd-253f-4e27-aa9d-511e4c4b12d4">
                      <gml:posList>550201.773295502760000000 5804601.012219036900000000 69.131373452265407000 550199.426887915820000000 5804599.395350311000000000 67.417939853818382000 550199.339999999850000000 5804598.765999999800000000 66.840752917509860000 550203.867105879700000000 5804598.407310911500000000 67.008816305746421000 550201.773295502760000000 5804601.012219036900000000 69.131373452265407000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_c84c0aa2-ef09-47b4-9d4a-0cf3b80b4dc6">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_542473f5-77b7-4094-bce3-a0d3b9be43de" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_a8950299-ad1d-4fbc-bc18-bbe1938632ed">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_43fe2b1e-33bd-4fe3-a025-82a548c45d17">
                      <gml:posList>550198.462595068970000000 5804617.390376742000000000 66.855000702947052000 550195.544480212270000000 5804617.706005056400000000 66.838182488077649000 550195.377918174020000000 5804616.166069837300000000 68.341316223187391000 550193.136934604380000000 5804614.960098145500000000 69.726692199707031000 550191.202977670590000000 5804616.592715208400000000 68.341316223184592000 550191.372204289890000000 5804618.157285586900000000 66.814136062078830000 550189.045000001790000000 5804618.409000000000000000 66.800723491399822000 550188.475150630230000000 5804612.995430986400000000 72.083370518546502000 550205.539451585850000000 5804611.254249465700000000 72.080809313540129000 550206.186000000690000000 5804616.554999999700000000 66.899513647106446000 550202.634870991230000000 5804616.939096211500000000 66.879047128945842000 550202.472837158130000000 5804615.441026129800000000 68.341316223190631000 550200.231853588480000000 5804614.235054437100000000 69.726692199707031000 550198.297896654690000000 5804615.867671500900000000 68.341316223181337000 550198.462595068970000000 5804617.390376742000000000 66.855000702947052000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_72a297d3-d6cd-414a-b7ef-14ae3542e6b9">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_f43b2fba-4c6e-4ee3-814a-a0db64cc75e7" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_e4efab5b-62dd-4ff8-a908-0358e27e0868">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_fd7fba81-e09d-4e26-91dc-e8ad72c421d6">
                      <gml:posList>550201.773295502760000000 5804601.012219036900000000 69.131373452265407000 550202.429891469660000000 5804608.435237312700000000 69.131330488232237000 550199.233531750270000000 5804606.276143135500000000 66.794240619943110000 550199.080086976870000000 5804606.107858873000000000 66.686492330734836000 550200.190999999640000000 5804604.929999999700000000 67.631408783630249000 550199.426887915820000000 5804599.395350311000000000 67.417939853818382000 550201.773295502760000000 5804601.012219036900000000 69.131373452265407000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_dd236b4a-4df0-4a73-93e3-0dd41da1ddde">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_a0dffddf-800c-4453-9c8f-8b085d27c589" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_abaa4dc6-fb78-4755-b70c-8961530e36f6">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_6b091537-6ccb-4cfd-911c-46cfa28f98fd">
                      <gml:posList>550204.833542767910000000 5804605.465802643400000000 66.696370447717740000 550204.645492064420000000 5804605.835027759000000000 66.897929362331212000 550202.429891469660000000 5804608.435237312700000000 69.131330488232237000 550201.773295502760000000 5804601.012219036900000000 69.131373452265407000 550203.867105879700000000 5804598.407310911500000000 67.008816305746421000 550203.971999999140000000 5804598.399000000200000000 66.912351632988191000 550204.833542767910000000 5804605.465802643400000000 66.696370447717740000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_aa6fd84d-7efa-45f2-8443-fdb31f17da1a">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_d6ad216a-6718-4f58-9019-4fb0ca961d32" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_d70fdd24-ce27-4c61-8b56-70f418960a1f">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_2c2d5bb9-a4c9-46b1-aa72-a6f1528e0639">
                      <gml:posList>550204.645492064420000000 5804605.835027759000000000 66.897929362331212000 550204.833542767910000000 5804605.465802643400000000 66.568662893884010000 550205.092999998480000000 5804607.593999999600000000 68.595267223280359000 550205.539451585850000000 5804611.254249465700000000 72.080809313540129000 550188.475150630230000000 5804612.995430986400000000 72.083370518546502000 550187.886999998240000000 5804607.407999999800000000 66.771578255887022000 550199.046000000090000000 5804606.144000000300000000 66.651979317085647000 550199.080086977100000000 5804606.107858873000000000 66.621251993175804000 550199.233531750270000000 5804606.276143135500000000 66.794240619943110000 550202.429891469660000000 5804608.435237312700000000 69.131330488232237000 550204.645492064420000000 5804605.835027759000000000 66.897929362331212000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_c947ed18-8ef1-45e4-8320-63ad8345e80e">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_54062c9f-3640-46d6-8fdb-02ae1a10c918" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_0102801b-820f-49ae-8794-04187fc2b455">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_e1cb33d3-cfd7-41cc-9a01-638cfa49aba4">
                      <gml:posList>550202.634870991110000000 5804616.939096210500000000 68.341316223306521000 550200.548733029980000000 5804617.164736475800000000 69.726692199707031000 550200.231853588480000000 5804614.235054437100000000 69.726692199707031000 550202.472837158130000000 5804615.441026129800000000 68.341316223190631000 550202.634870991110000000 5804616.939096210500000000 68.341316223306521000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_82b3d696-5c09-4dcc-9dcf-0da05a3d7090">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_266bd36c-040d-46ca-8efe-4652dea312ca" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_1ad05f06-223d-4b20-b63c-a7371de644cb">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_137b3a8e-4548-465e-8489-383087f72e9f">
                      <gml:posList>550200.548733029980000000 5804617.164736475800000000 69.726692199707031000 550198.462595068850000000 5804617.390376741100000000 68.341316223063544000 550198.297896654690000000 5804615.867671500900000000 68.341316223181337000 550200.231853588480000000 5804614.235054437100000000 69.726692199707031000 550200.548733029980000000 5804617.164736475800000000 69.726692199707031000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_8547c222-b86c-4e00-9b2e-771646c94bfd">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_1b418ad6-be5c-49dd-a123-f3a115ad6aad" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_65165d48-fcf7-4e63-8dcd-2f31953e0579">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_6de41ded-73ff-4cdc-8c31-a9efefdb0659">
                      <gml:posList>550195.544480212150000000 5804617.706005055500000000 68.341316223306521000 550193.458342251020000000 5804617.931645320700000000 69.726692199707045000 550193.136934604380000000 5804614.960098145500000000 69.726692199707031000 550195.377918174020000000 5804616.166069837300000000 68.341316223187391000 550195.544480212150000000 5804617.706005055500000000 68.341316223306521000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_197ef5ae-e23c-4fb6-a99b-88f82b03b80e">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_9434cea7-9d33-4fc5-a1a7-bd0943afc575" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_72497172-5b11-472b-ab01-d678e60b373e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_16394f5f-6f6b-435f-83ea-dc02d0b40438">
                      <gml:posList>550193.458342251020000000 5804617.931645320700000000 69.726692199707045000 550191.372204289890000000 5804618.157285586000000000 68.341316223063572000 550191.202977670590000000 5804616.592715208400000000 68.341316223184592000 550193.136934604380000000 5804614.960098145500000000 69.726692199707031000 550193.458342251020000000 5804617.931645320700000000 69.726692199707045000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_26c70d33-6b57-45b2-be85-d07c09b9c888">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_4b2f662d-5a0b-4d85-b9ce-1faa8d1c4aa0" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_049bb1e2-934a-41ca-a097-7c5049d0a8e9">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_36989674-fb13-4a73-b24c-5c696bb02cd7">
                      <gml:posList>550191.372204289890000000 5804618.157285586900000000 66.814136062078830000 550191.202977670590000000 5804616.592715208400000000 68.341316223184592000 550191.372204289890000000 5804618.157285586000000000 68.341316223063572000 550191.372204289890000000 5804618.157285586900000000 66.814136062078830000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_b1552409-47df-4427-af14-867e7c596925">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_7b8cc3a1-b91b-41e0-9f41-cd3ca26a6cad" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_fa79d01f-fc89-4c3f-949f-d14715740f9e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_c135d526-7a30-40f0-bdbb-1dbfd2f794be">
                      <gml:posList>550195.544480212270000000 5804617.706005056400000000 66.838182488077649000 550195.544480212150000000 5804617.706005055500000000 68.341316223306521000 550195.377918174020000000 5804616.166069837300000000 68.341316223187391000 550195.544480212270000000 5804617.706005056400000000 66.838182488077649000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_ddb1e267-340c-4361-a0fb-2664b46df44d">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_60cf0f3a-65d7-4b72-8827-5d0064567ce4" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_50f768f2-45df-462a-a891-411ff1a02a3f">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_4f074939-3d85-4dbe-a46c-73dc602f32c1">
                      <gml:posList>550198.462595068970000000 5804617.390376742000000000 66.855000702947052000 550198.297896654690000000 5804615.867671500900000000 68.341316223181337000 550198.462595068850000000 5804617.390376741100000000 68.341316223063544000 550198.462595068970000000 5804617.390376742000000000 66.855000702947052000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_014ad25a-a622-4860-a2a7-0a9947034c82">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_8310228d-629c-441a-98aa-14ea300b5fcb" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_f8e3b030-5c55-4d18-a359-b58621d97ea8">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_9bcccccd-8974-451b-931c-653fd05b2b6a">
                      <gml:posList>550202.634870991230000000 5804616.939096211500000000 66.879047128945842000 550202.634870991110000000 5804616.939096210500000000 68.341316223306521000 550202.472837158130000000 5804615.441026129800000000 68.341316223190631000 550202.634870991230000000 5804616.939096211500000000 66.879047128945842000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_b4e3229c-9f21-4483-ba75-816c23fa3924">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_981c0f1a-338f-4485-94af-73b8722e4061" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_cae6f483-41db-4950-b7b0-22899fe7377a">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_d0a14baf-30cb-43c9-aa8b-f8721dcffb30">
                      <gml:posList>550204.833542767910000000 5804605.465802643400000000 66.696370447717740000 550204.833542767910000000 5804605.465802643400000000 66.568662893884010000 550204.645492064420000000 5804605.835027759000000000 66.897929362331212000 550204.833542767910000000 5804605.465802643400000000 66.696370447717740000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_5200c9cd-ac2b-49a5-ba8e-af36d75bbceb">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_60a45377-4511-41c5-80e2-e35ff216eeca" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_16e9195f-9b73-40c6-bb53-de462e97231b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_f813805d-976f-4daa-be29-7cf85983b2db">
                      <gml:posList>550199.233531750270000000 5804606.276143135500000000 66.794240619943110000 550199.080086977100000000 5804606.107858873000000000 66.621251993175804000 550199.080086976870000000 5804606.107858873000000000 66.686492330734836000 550199.233531750270000000 5804606.276143135500000000 66.794240619943110000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_deca54da-2138-47d2-ac20-d16fccdaa275">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_e7928c02-23eb-43ef-8bf2-d1f799b15942" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_827f4406-663a-4632-a069-8e28686ea0cc">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_7fd910af-e679-4bb2-82d2-79120153c4bb">
                      <gml:posList>550199.046000000090000000 5804606.144000000300000000 66.651979317085647000 550187.886999998240000000 5804607.407999999800000000 66.771578255887022000 550187.886999998240000000 5804607.407999999800000000 53.650000000000006000 550199.046000000090000000 5804606.144000000300000000 53.650000000000006000 550199.046000000090000000 5804606.144000000300000000 66.651979317085647000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_99bca26f-79b7-4c84-bf06-72f8d5df5b34">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_3788a770-ff15-4660-826c-bbfd47984a97" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_e30edb51-0a74-43fb-bba6-73f2d4f4f203">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_b4c20e5a-4972-4a47-9be2-4a8dd9b336b7">
                      <gml:posList>550189.045000001790000000 5804618.409000000000000000 53.650000000000006000 550188.475150630230000000 5804612.995430986400000000 53.649999999999999000 550187.886999998240000000 5804607.407999999800000000 53.650000000000006000 550187.886999998240000000 5804607.407999999800000000 66.771578255887022000 550188.475150630230000000 5804612.995430986400000000 72.083370518546502000 550189.045000001790000000 5804618.409000000000000000 66.800723491399822000 550189.045000001790000000 5804618.409000000000000000 53.650000000000006000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_ec77930e-f6fd-486b-881c-d019ec0e88fb">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_a6f25bf9-e295-45ba-bfbf-1e70c26d1e6d" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_b6389501-cebf-4881-8f9a-277b18937a09">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_6801b45b-f0da-41ca-806a-a4f0e2c25cd8">
                      <gml:posList>550206.186000000690000000 5804616.554999999700000000 66.899513647106446000 550205.539451585850000000 5804611.254249465700000000 72.080809313540129000 550205.092999998480000000 5804607.593999999600000000 68.595267223280359000 550204.833542767910000000 5804605.465802643400000000 66.568662893884010000 550204.833542767910000000 5804605.465802643400000000 66.696370447717740000 550203.971999999140000000 5804598.399000000200000000 66.912351632988191000 550203.971999999140000000 5804598.399000000200000000 53.650000000000006000 550204.831481905190000000 5804605.448898421600000000 53.649999999999999000 550204.833542767910000000 5804605.465802643400000000 53.649999999999999000 550205.092999998480000000 5804607.593999999600000000 53.649999999999999000 550205.539451585850000000 5804611.254249465700000000 53.649999999999999000 550206.186000000690000000 5804616.554999999700000000 53.650000000000006000 550206.186000000690000000 5804616.554999999700000000 66.899513647106446000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_71426d42-349a-42fc-bb57-a80ccb65c88f">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_62d8f333-889d-4f7f-a4a0-a67c169ef90e" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_06648ffe-ea77-4b1a-88b4-90672d9dc5b9">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_ef679bd0-fef6-408a-a6a6-e369ec2a7862">
                      <gml:posList>550203.971999999140000000 5804598.399000000200000000 66.912351632988191000 550203.867105879700000000 5804598.407310911500000000 67.008816305746421000 550199.339999999850000000 5804598.765999999800000000 66.840752917509860000 550199.339999999850000000 5804598.765999999800000000 53.650000000000006000 550203.867105879700000000 5804598.407310911500000000 53.649999999999999000 550203.971999999140000000 5804598.399000000200000000 53.650000000000006000 550203.971999999140000000 5804598.399000000200000000 66.912351632988191000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_76e196be-7b7f-4b25-b813-4e42c8906dbd">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_be1007d7-1c55-4ba9-99de-23985bed4941" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_439c74aa-0629-4f79-b578-18051196a281">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_d8af25a6-bb03-4c29-90c6-1eabd5b3031f">
                      <gml:posList>550200.190999999640000000 5804604.929999999700000000 53.650000000000006000 550199.426887915820000000 5804599.395350311000000000 53.649999999999999000 550199.339999999850000000 5804598.765999999800000000 53.650000000000006000 550199.339999999850000000 5804598.765999999800000000 66.840752917509860000 550199.426887915820000000 5804599.395350311000000000 67.417939853818382000 550200.190999999640000000 5804604.929999999700000000 67.631408783630249000 550200.190999999640000000 5804604.929999999700000000 53.650000000000006000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_f66820e7-85ac-4d85-b039-2b517dd53e93">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_4f3d84c9-fc27-415e-848c-5e51abef3dcc" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_ffd49219-a0d5-42f1-8786-683b9b9055c5">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_29aad996-a2cd-4ae0-8516-29df5071a769">
                      <gml:posList>550200.190999999640000000 5804604.929999999700000000 67.631408783630249000 550199.080086976870000000 5804606.107858873000000000 66.686492330734836000 550199.080086977100000000 5804606.107858873000000000 66.621251993175804000 550199.046000000090000000 5804606.144000000300000000 66.651979317085647000 550199.046000000090000000 5804606.144000000300000000 53.650000000000006000 550199.080086976870000000 5804606.107858873000000000 53.649999999999999000 550200.190999999640000000 5804604.929999999700000000 53.650000000000006000 550200.190999999640000000 5804604.929999999700000000 67.631408783630249000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_2a837697-ebfb-4792-bfd4-f9f806c2616b">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_72ff7724-a341-46d6-97fa-1cf19a72241d" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_8b66ec78-f775-4bfe-9582-4cf7bb74c646">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_728bbf35-24ae-4a6a-9c2f-ee7486c74027">
                      <gml:posList>550202.634870991230000000 5804616.939096211500000000 66.879047128945842000 550206.186000000690000000 5804616.554999999700000000 66.899513647106446000 550206.186000000690000000 5804616.554999999700000000 53.650000000000006000 550202.634870991110000000 5804616.939096210500000000 53.649999999999999000 550200.548733029980000000 5804617.164736475800000000 53.649999999999999000 550198.462595068850000000 5804617.390376741100000000 53.649999999999999000 550195.544480212150000000 5804617.706005055500000000 53.649999999999999000 550193.458342251020000000 5804617.931645320700000000 53.650000000000006000 550191.372204289890000000 5804618.157285586000000000 53.649999999999999000 550189.045000001790000000 5804618.409000000000000000 53.650000000000006000 550189.045000001790000000 5804618.409000000000000000 66.800723491399822000 550191.372204289890000000 5804618.157285586900000000 66.814136062078830000 550191.372204289890000000 5804618.157285586000000000 68.341316223063572000 550193.458342251020000000 5804617.931645320700000000 69.726692199707045000 550195.544480212150000000 5804617.706005055500000000 68.341316223306521000 550195.544480212270000000 5804617.706005056400000000 66.838182488077649000 550198.462595068970000000 5804617.390376742000000000 66.855000702947052000 550198.462595068850000000 5804617.390376741100000000 68.341316223063544000 550200.548733029980000000 5804617.164736475800000000 69.726692199707031000 550202.634870991110000000 5804616.939096210500000000 68.341316223306521000 550202.634870991230000000 5804616.939096211500000000 66.879047128945842000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="UUID_69603a8f-11bb-4f66-a106-6235952e20db">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_3baee0fc-76d2-4fce-b3e6-68320492dd4c" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_3d7d8088-a06e-48bb-a041-9e3c4d9e0b45">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_fb1e0466-c2a8-4f1c-8f18-0efce464fd05">
                      <gml:posList>550205.092999998480000000 5804607.593999999600000000 53.649999999999999000 550204.833542767910000000 5804605.465802643400000000 53.649999999999999000 550204.831481905190000000 5804605.448898421600000000 53.649999999999999000 550203.971999999140000000 5804598.399000000200000000 53.650000000000006000 550203.867105879700000000 5804598.407310911500000000 53.649999999999999000 550199.339999999850000000 5804598.765999999800000000 53.650000000000006000 550199.426887915820000000 5804599.395350311000000000 53.649999999999999000 550200.190999999640000000 5804604.929999999700000000 53.650000000000006000 550199.080086976870000000 5804606.107858873000000000 53.649999999999999000 550199.046000000090000000 5804606.144000000300000000 53.650000000000006000 550187.886999998240000000 5804607.407999999800000000 53.650000000000006000 550188.475150630230000000 5804612.995430986400000000 53.649999999999999000 550189.045000001790000000 5804618.409000000000000000 53.650000000000006000 550191.372204289890000000 5804618.157285586000000000 53.649999999999999000 550193.458342251020000000 5804617.931645320700000000 53.650000000000006000 550195.544480212150000000 5804617.706005055500000000 53.649999999999999000 550198.462595068850000000 5804617.390376741100000000 53.649999999999999000 550200.548733029980000000 5804617.164736475800000000 53.649999999999999000 550202.634870991110000000 5804616.939096210500000000 53.649999999999999000 550206.186000000690000000 5804616.554999999700000000 53.650000000000006000 550205.539451585850000000 5804611.254249465700000000 53.649999999999999000 550205.092999998480000000 5804607.593999999600000000 53.649999999999999000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </cityObjectMember>

  <cityObjectMember>
    <bldg:Building gml:id="DENIAL43000053lJ">
      <gml:boundedBy>
        <gml:Envelope srsDimension="3" srsName="urn:adv:crs:ETRS89_UTM32*DE_DHHN92_NH">
          <gml:lowerCorner>550123.250000 5804573.000000 53.009998
</gml:lowerCorner>
          <gml:upperCorner>550141.750000 5804592.500000 75.353889
</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <creationDate>2017-11-21</creationDate>
      <gen:doubleAttribute name="Volume">
        <gen:value>4639.496</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Profile">
        <gen:value>Sig3d</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCK">
        <gen:value>10</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOECKE">
        <gen:value>111010</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STBEZNR">
        <gen:value>111</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="MBZNR">
        <gen:value>1102</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="WBZNR">
        <gen:value>1102</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNAM">
        <gen:value>Vahrenwald</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTNR">
        <gen:value>11</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNAM">
        <gen:value>Vahrenwald-List</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBEZ">
        <gen:value>02</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEMEINDE">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SCALE">
        <gen:value>5000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SHAPE_Area">
        <gen:value>7165.30049704715</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ALKISID">
        <gen:value>DENIAL43000053lJ</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ZMIN">
        <gen:value>53.01</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="THICKNESS">
        <gen:value>0.1</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="TEXTURE">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="BUILDINGID">
        <gen:value>DENIAL43000053lJ</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ARTBEZEICH">
        <gen:value>Gebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Join_Cou_1">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="FUNKTION_T">
        <gen:value>Wohngebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="OBJFLAECHE">
        <gen:value>231.21</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADT">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEB_Funkti">
        <gen:value>1000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STRSCHL">
        <gen:value>00712</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STR_komple">
        <gen:value>Dörnbergstraße 6</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="PLZ_NUMMER">
        <gen:value>30161</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STR">
        <gen:value>Dörnbergstraße</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="HAUSNR">
        <gen:value>6</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="OVERHANG">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <app:appearance>
        <app:Appearance>
          <app:theme>RhinoCity ObliqueTexturing</app:theme>
          <app:surfaceDataMember>
            <app:ParameterizedTexture>
              <app:imageURI>Hannover_teil1_Appearance/DENIAL43000053lJ.jpg</app:imageURI>
              <app:textureType>specific</app:textureType>
              <app:wrapMode>border</app:wrapMode>
              <app:borderColor>0 0 0 1</app:borderColor>
              <app:target uri="#UUID_4674ba9d-ca2d-4cc2-bb86-0113f76d6bf3">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_aefb1cc3-4b09-40f6-a641-75a9654854ed">0.797149 0.818672 0.730293 0.820888 0.727984 0.686888 0.793011 0.688854 0.797149 0.818672 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_0bec61c8-2f69-45c3-a155-f662a9f9e40e">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_5511fbdf-3998-4613-b4d5-c4aaaa5d0fa5">0.824300 0.786607 0.802348 0.686888 0.817618 0.716815 0.829668 0.770979 0.824300 0.786607 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_e35a390f-b0da-48cb-912c-2589fb58a20c">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_9387d1f0-8494-4c0b-aad9-94e301cf879e">0.838595 0.701580 0.833659 0.686888 0.895921 0.701899 0.838595 0.701580 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_f49c3b45-f9e6-462f-ae19-637a499cabaa">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_8f055521-f146-4dcb-a055-b4ca02ef7cde">0.273703 0.091619 0.280422 0.385816 0.145183 0.403275 0.133072 0.021140 0.151845 0.001957 0.273703 0.091619 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_cffac1bb-f4c1-4dc9-a34e-d620b33a7eb5">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_34801c63-600f-400f-aa88-540d06f6ea8e">0.522913 0.842569 0.502136 0.822224 0.504713 0.761661 0.438275 0.759692 0.395303 0.717614 0.722386 0.686888 0.723366 0.824399 0.522913 0.842569 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_fa4deea4-8a99-424c-8255-3850ef198b29">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_21ae57fb-3f34-49fc-9e5a-32ff625ff867">0.390725 0.847420 0.099501 0.869995 0.001957 0.741761 0.015683 0.720372 0.378952 0.686888 0.390725 0.847420 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_82122353-7219-4ade-8f02-947fb79806a5">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_af7596ef-aa5f-42f9-86c3-427c56242745">0.579514 0.107178 0.600117 0.127642 0.598283 0.127773 0.607921 0.284851 0.480291 0.295637 0.473581 0.001957 0.516191 0.044281 0.518530 0.179328 0.581732 0.178606 0.579514 0.107178 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_b7055eea-bb71-4d78-9d2f-7987005dbc95">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_92884a22-648a-4065-8abe-1d23c00a9ee8">0.388939 0.347962 0.356164 0.228851 0.437177 0.001957 0.468799 0.122503 0.388939 0.347962 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_de6c5dcf-a7aa-412a-b3e6-ec11c9b38ca5">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_5dc5c2aa-5f0d-4f99-ae15-3d0ce0dc9f43">0.001957 0.447317 0.170628 0.434570 0.172312 0.434442 0.244459 0.669653 0.242596 0.669244 0.072637 0.682450 0.001957 0.447317 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_a6ad1c67-c8d7-4b91-b63c-c7ceb148d2ec">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_e34401e0-09d8-4df7-bf01-1af165e86bcf">0.049209 0.429397 0.006821 0.360529 0.001957 0.199251 0.072161 0.001957 0.099456 0.098459 0.128862 0.202429 0.049209 0.429397 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_88e7f637-6976-4f83-8c56-d982d526d693">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_2bcc8b3f-44b5-483a-a756-f50457044384">0.645684 0.630628 0.282379 0.664184 0.248532 0.466972 0.609221 0.434442 0.645684 0.630628 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_57840134-3d2f-40b4-8644-244d9e04cee5">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_bc79c830-e5ee-46ee-9a2b-9fd930f68c2c">0.712704 0.631611 0.699314 0.653041 0.682348 0.658448 0.649706 0.461920 0.665088 0.447824 0.679691 0.434442 0.712704 0.631611 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_be23182d-b18d-411e-a0d3-4e41e97707e5">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_70167d8c-5323-4b2b-ab8c-ee837ac41977">0.297223 0.401488 0.285714 0.023650 0.338532 0.001957 0.350451 0.384083 0.297223 0.401488 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_8ac7b588-915b-4f72-92ee-34a57286897d">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_d10059e0-fd89-47d1-bbda-4c260f4380ac">0.612524 0.020203 0.730106 0.010375 0.830822 0.001957 0.896043 0.208129 0.814642 0.280727 0.684410 0.253274 0.612524 0.020203 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
            </app:ParameterizedTexture>
          </app:surfaceDataMember>
        </app:Appearance>
      </app:appearance>
      <bldg:measuredHeight uom="#m">22.344</bldg:measuredHeight>
      <bldg:lod1Solid>
        <gml:Solid gml:id="UUID_65a4faca-c090-4a64-927a-3d4e501b6056">
          <gml:exterior>
            <gml:CompositeSurface gml:id="UUID_f1af1f94-0983-481a-b07d-6c542c428936">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_fa68c01a-04c8-4409-b4b9-81c43fef36ba">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_ba1f42b1-b621-466a-84bf-bed270e00368">
                      <gml:posList>550141.682999998330000000 5804584.064000000200000000 75.353887906042132000 550141.228843489890000000 5804578.878093862000000000 75.353887906042132000 550140.809000000360000000 5804574.083999999800000000 75.353887906042132000 550140.809000000360000000 5804574.083999999800000000 53.010000000000005000 550141.228843489890000000 5804578.878093862000000000 53.009999999999998000 550141.682999998330000000 5804584.064000000200000000 53.010000000000005000 550141.682999998330000000 5804584.064000000200000000 75.353887906042132000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_bcb0764d-5dc7-47e0-b57d-b6c70c83c0e9">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_b66decfd-ec87-4d6e-9aee-4677204beeb8">
                      <gml:posList>550140.809000000360000000 5804574.083999999800000000 75.353887906042132000 550125.118999999020000000 5804575.734000000200000000 75.353887906042132000 550125.118999999020000000 5804575.734000000200000000 53.010000000000005000 550140.809000000360000000 5804574.083999999800000000 53.010000000000005000 550140.809000000360000000 5804574.083999999800000000 75.353887906042132000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_7da6a7ed-48c1-47de-a875-4406d6b390df">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_fb87f780-578f-4b77-8c62-a9d4c6f09784">
                      <gml:posList>550125.118999999020000000 5804575.734000000200000000 75.353887906042132000 550124.515767494680000000 5804576.400525664900000000 75.353887906042132000 550123.879999998960000000 5804577.103000000100000000 75.353887906042132000 550123.879999998960000000 5804577.103000000100000000 53.010000000000005000 550124.515767494680000000 5804576.400525664900000000 53.009999999999998000 550125.118999999020000000 5804575.734000000200000000 53.010000000000005000 550125.118999999020000000 5804575.734000000200000000 75.353887906042132000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_c3f997f9-d08a-4fad-95e0-a14cab71b79d">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_ff07908b-a1af-44bd-b7b1-06d92d8bf270">
                      <gml:posList>550124.351999998090000000 5804591.839999999900000000 53.010000000000005000 550123.879999998960000000 5804577.103000000100000000 53.010000000000005000 550123.879999998960000000 5804577.103000000100000000 75.353887906042132000 550124.351999998090000000 5804591.839999999900000000 75.353887906042132000 550124.351999998090000000 5804591.839999999900000000 53.010000000000005000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_366a1bed-dcf5-4ff9-8d5c-0e1ec76ab944">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_8e089f1a-36f5-41e7-b985-796e0ee6767c">
                      <gml:posList>550134.287000000480000000 5804590.831000000200000000 53.010000000000005000 550128.934755480730000000 5804591.374574707800000000 53.009999999999998000 550124.351999998090000000 5804591.839999999900000000 53.010000000000005000 550124.351999998090000000 5804591.839999999900000000 75.353887906042132000 550128.934755480730000000 5804591.374574707800000000 75.353887906042132000 550134.287000000480000000 5804590.831000000200000000 75.353887906042132000 550134.287000000480000000 5804590.831000000200000000 53.010000000000005000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_59c2926c-548b-465c-937a-49e9ec0f1530">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_762c5234-bbde-4e93-9955-fca8a0e5149c">
                      <gml:posList>550134.287000000480000000 5804590.831000000200000000 75.353887906042132000 550133.885999999940000000 5804584.781000000400000000 75.353887906042132000 550133.885999999940000000 5804584.781000000400000000 53.010000000000005000 550134.287000000480000000 5804590.831000000200000000 53.010000000000005000 550134.287000000480000000 5804590.831000000200000000 75.353887906042132000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_a9e15f5a-c344-49aa-8144-4e193a338436">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_4d6b3045-1d38-429d-be6b-1da798d56ee7">
                      <gml:posList>550141.682999998330000000 5804584.064000000200000000 53.010000000000005000 550133.963047059140000000 5804584.773914872700000000 53.009999999999998000 550133.885999999940000000 5804584.781000000400000000 53.010000000000005000 550133.885999999940000000 5804584.781000000400000000 75.353887906042132000 550133.963047059140000000 5804584.773914872700000000 75.353887906042132000 550141.682999998330000000 5804584.064000000200000000 75.353887906042132000 550141.682999998330000000 5804584.064000000200000000 53.010000000000005000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_c1f2cb41-dc9e-4201-978c-7c6079f7b421">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_d2c65123-0bf7-4cd3-b7f1-5ba1a6054ead">
                      <gml:posList>550141.682999998330000000 5804584.064000000200000000 53.010000000000005000 550141.228843489890000000 5804578.878093862000000000 53.009999999999998000 550140.809000000360000000 5804574.083999999800000000 53.010000000000005000 550125.118999999020000000 5804575.734000000200000000 53.010000000000005000 550124.515767494680000000 5804576.400525664900000000 53.009999999999998000 550123.879999998960000000 5804577.103000000100000000 53.010000000000005000 550124.351999998090000000 5804591.839999999900000000 53.010000000000005000 550128.934755480730000000 5804591.374574707800000000 53.009999999999998000 550134.287000000480000000 5804590.831000000200000000 53.010000000000005000 550133.885999999940000000 5804584.781000000400000000 53.010000000000005000 550133.963047059140000000 5804584.773914872700000000 53.009999999999998000 550141.682999998330000000 5804584.064000000200000000 53.010000000000005000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_b807d3c9-98a3-482a-b45d-b02ccd1cf47e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_a327f065-bfa8-4a94-a257-b4340afbd557">
                      <gml:posList>550141.682999998330000000 5804584.064000000200000000 75.353887906042132000 550133.963047059140000000 5804584.773914872700000000 75.353887906042132000 550133.885999999940000000 5804584.781000000400000000 75.353887906042132000 550134.287000000480000000 5804590.831000000200000000 75.353887906042132000 550128.934755480730000000 5804591.374574707800000000 75.353887906042132000 550124.351999998090000000 5804591.839999999900000000 75.353887906042132000 550123.879999998960000000 5804577.103000000100000000 75.353887906042132000 550124.515767494680000000 5804576.400525664900000000 75.353887906042132000 550125.118999999020000000 5804575.734000000200000000 75.353887906042132000 550140.809000000360000000 5804574.083999999800000000 75.353887906042132000 550141.228843489890000000 5804578.878093862000000000 75.353887906042132000 550141.682999998330000000 5804584.064000000200000000 75.353887906042132000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod1Solid>
      <bldg:lod2Solid>
        <gml:Solid gml:id="UUID_608d86d2-cede-4c80-af5b-e1d05c8b2711">
          <gml:exterior>
            <gml:CompositeSurface gml:id="UUID_09d67f41-b7e8-4cb0-b63e-edece07929ea">
              <gml:surfaceMember xlink:href="#UUID_4674ba9d-ca2d-4cc2-bb86-0113f76d6bf3"/>
              <gml:surfaceMember xlink:href="#UUID_0bec61c8-2f69-45c3-a155-f662a9f9e40e"/>
              <gml:surfaceMember xlink:href="#UUID_e35a390f-b0da-48cb-912c-2589fb58a20c"/>
              <gml:surfaceMember xlink:href="#UUID_f49c3b45-f9e6-462f-ae19-637a499cabaa"/>
              <gml:surfaceMember xlink:href="#UUID_cffac1bb-f4c1-4dc9-a34e-d620b33a7eb5"/>
              <gml:surfaceMember xlink:href="#UUID_fa4deea4-8a99-424c-8255-3850ef198b29"/>
              <gml:surfaceMember xlink:href="#UUID_82122353-7219-4ade-8f02-947fb79806a5"/>
              <gml:surfaceMember xlink:href="#UUID_b7055eea-bb71-4d78-9d2f-7987005dbc95"/>
              <gml:surfaceMember xlink:href="#UUID_de6c5dcf-a7aa-412a-b3e6-ec11c9b38ca5"/>
              <gml:surfaceMember xlink:href="#UUID_a6ad1c67-c8d7-4b91-b63c-c7ceb148d2ec"/>
              <gml:surfaceMember xlink:href="#UUID_88e7f637-6976-4f83-8c56-d982d526d693"/>
              <gml:surfaceMember xlink:href="#UUID_57840134-3d2f-40b4-8644-244d9e04cee5"/>
              <gml:surfaceMember xlink:href="#UUID_be23182d-b18d-411e-a0d3-4e41e97707e5"/>
              <gml:surfaceMember xlink:href="#UUID_8ac7b588-915b-4f72-92ee-34a57286897d"/>
              <gml:surfaceMember xlink:href="#UUID_6f9f0d8b-c48e-4a68-b3c6-eaa671117a02"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:outerBuildingInstallation>
        <bldg:BuildingInstallation>
          <bldg:boundedBy>
            <bldg:RoofSurface gml:id="UUID_94380d8b-a788-4a51-af07-82b08c59490e">
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_8eb14e4c-04a4-43fa-a2ab-7e2dc2c3e8a2" srsDimension="3">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="UUID_4674ba9d-ca2d-4cc2-bb86-0113f76d6bf3">
                      <gml:exterior>
                        <gml:LinearRing gml:id="UUID_aefb1cc3-4b09-40f6-a641-75a9654854ed">
                          <gml:posList>550133.189607204640000000 5804586.759851750900000000 74.052977121673450000 550130.538718848840000000 5804586.855461909400000000 74.220378364647729000 550130.443978222090000000 5804581.654865328200000000 74.217891989070296000 550133.022439207530000000 5804581.721225776700000000 74.055323814522893000 550133.189607204640000000 5804586.759851750900000000 74.052977121673450000 </gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:RoofSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_ee9005bf-7183-4a4b-a239-44e29555df82">
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_ee49d9a4-fbcb-4d06-bb22-2112c5b52fc2" srsDimension="3">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="UUID_0bec61c8-2f69-45c3-a155-f662a9f9e40e">
                      <gml:exterior>
                        <gml:LinearRing gml:id="UUID_5511fbdf-3998-4613-b4d5-c4aaaa5d0fa5">
                          <gml:posList>550133.189607204640000000 5804586.759851750900000000 74.052977121673450000 550133.022439207530000000 5804581.721225776700000000 74.055323814522893000 550133.098290168100000000 5804584.007456553200000000 72.741399875812306000 550133.189607204640000000 5804586.759851750900000000 72.719417552208000000 550133.189607204640000000 5804586.759851750900000000 74.052977121673450000 </gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_cf784335-772d-491a-a41b-32fab6c75837">
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_21d2526d-d8ab-4c93-8832-b1d504105540" srsDimension="3">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="UUID_e35a390f-b0da-48cb-912c-2589fb58a20c">
                      <gml:exterior>
                        <gml:LinearRing gml:id="UUID_9387d1f0-8494-4c0b-aad9-94e301cf879e">
                          <gml:posList>550133.189607204640000000 5804586.759851750900000000 74.052977121673450000 550133.189607204640000000 5804586.759851750900000000 72.719417552208000000 550130.538718848840000000 5804586.855461909400000000 74.220378364647729000 550133.189607204640000000 5804586.759851750900000000 74.052977121673450000 </gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
        </bldg:BuildingInstallation>
      </bldg:outerBuildingInstallation>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_d9a32af8-04cc-4c8c-9318-136eb1c0f973">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_510c227e-29ef-4dfe-b61f-a5ec6f6d040b" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_f49c3b45-f9e6-462f-ae19-637a499cabaa">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_8f055521-f146-4dcb-a055-b4ca02ef7cde">
                      <gml:posList>550128.660925818720000000 5804580.074495905100000000 75.209735775553384000 550128.934755480730000000 5804591.374574707800000000 75.176677596800403000 550124.351999998090000000 5804591.839999999900000000 69.954455553155867000 550123.879999998960000000 5804577.103000000100000000 69.867008070252027000 550124.515767494680000000 5804576.400525664900000000 70.610922432797878000 550128.660925818720000000 5804580.074495905100000000 75.209735775553384000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_e7c3a5ce-6097-455f-b3b3-bd32a911cd21">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_f00580b9-9e64-4584-a5b9-a761d79a272f" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_cffac1bb-f4c1-4dc9-a34e-d620b33a7eb5">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_34801c63-600f-400f-aa88-540d06f6ea8e">
                      <gml:posList>550133.963047059140000000 5804584.773914872700000000 72.260368725206575000 550133.098290168100000000 5804584.007456553200000000 72.741399875812306000 550133.022439207530000000 5804581.721225776700000000 74.055323814522893000 550130.443978222090000000 5804581.654865328200000000 74.217891989070296000 550128.660925818720000000 5804580.074495905100000000 75.209735775553384000 550141.228843489890000000 5804578.878093862000000000 75.288390753398886000 550141.682999998330000000 5804584.064000000200000000 72.294363772488083000 550133.963047059140000000 5804584.773914872700000000 72.260368725206575000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_68e969c4-bd50-4b7e-9dfc-8d64a4082c1a">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_ff628c5a-8f96-4824-bd67-5373488f3923" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_fa4deea4-8a99-424c-8255-3850ef198b29">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_21ae57fb-3f34-49fc-9e5a-32ff625ff867">
                      <gml:posList>550141.228843489890000000 5804578.878093862000000000 75.288390753398886000 550128.660925818720000000 5804580.074495905100000000 75.209735775553384000 550124.515767494680000000 5804576.400525664900000000 70.610922432797878000 550125.118999999020000000 5804575.734000000200000000 69.930098046606261000 550140.809000000360000000 5804574.083999999800000000 69.852515947408151000 550141.228843489890000000 5804578.878093862000000000 75.288390753398886000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_c1357989-4e70-4716-beb5-270cea3184f7">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_c02399de-bf4d-4452-9ea1-682b1708b04b" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_82122353-7219-4ade-8f02-947fb79806a5">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_af7596ef-aa5f-42f9-86c3-427c56242745">
                      <gml:posList>550133.098290168100000000 5804584.007456553200000000 72.741399875812306000 550133.963047059140000000 5804584.773914872700000000 72.260368725206575000 550133.885999999940000000 5804584.781000000400000000 72.304040025283484000 550134.287000000480000000 5804590.831000000200000000 72.142399773654716000 550128.934755480730000000 5804591.374574707800000000 75.176677596800403000 550128.660925818720000000 5804580.074495905100000000 75.209735775553384000 550130.443978222090000000 5804581.654865328200000000 74.217891989070296000 550130.538718848840000000 5804586.855461909400000000 74.220378364647729000 550133.189607204640000000 5804586.759851750900000000 72.719417552208000000 550133.098290168100000000 5804584.007456553200000000 72.741399875812306000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_154ae172-b711-4685-aa12-052cac9c7a9c">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_83502de3-b4fa-4954-bcb4-c0131c33749b" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_b7055eea-bb71-4d78-9d2f-7987005dbc95">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_92884a22-648a-4065-8abe-1d23c00a9ee8">
                      <gml:posList>550134.287000000480000000 5804590.831000000200000000 72.142399773654716000 550133.885999999940000000 5804584.781000000400000000 72.304040025283484000 550133.885999999940000000 5804584.781000000400000000 53.010000000000005000 550134.287000000480000000 5804590.831000000200000000 53.010000000000005000 550134.287000000480000000 5804590.831000000200000000 72.142399773654716000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_0a60eb4d-27d8-46d9-9b80-d9eca88f8028">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_2d6b5969-8e87-4bdb-ae6f-37b7ff37aa71" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_de6c5dcf-a7aa-412a-b3e6-ec11c9b38ca5">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_5dc5c2aa-5f0d-4f99-ae15-3d0ce0dc9f43">
                      <gml:posList>550141.682999998330000000 5804584.064000000200000000 53.010000000000005000 550133.963047059140000000 5804584.773914872700000000 53.009999999999998000 550133.885999999940000000 5804584.781000000400000000 53.010000000000005000 550133.885999999940000000 5804584.781000000400000000 72.304040025283484000 550133.963047059140000000 5804584.773914872700000000 72.260368725206575000 550141.682999998330000000 5804584.064000000200000000 72.294363772488083000 550141.682999998330000000 5804584.064000000200000000 53.010000000000005000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_73b90367-3847-4dd3-aecb-e23db119dac9">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_c0808a82-2bcd-45f9-8cb6-872114b021bd" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_a6ad1c67-c8d7-4b91-b63c-c7ceb148d2ec">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_e34401e0-09d8-4df7-bf01-1af165e86bcf">
                      <gml:posList>550141.682999998330000000 5804584.064000000200000000 72.294363772488083000 550141.228843489890000000 5804578.878093862000000000 75.288390753398886000 550140.809000000360000000 5804574.083999999800000000 69.852515947408151000 550140.809000000360000000 5804574.083999999800000000 53.010000000000005000 550141.228843489890000000 5804578.878093862000000000 53.009999999999998000 550141.682999998330000000 5804584.064000000200000000 53.010000000000005000 550141.682999998330000000 5804584.064000000200000000 72.294363772488083000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_32895744-1444-442c-ada5-12c7f733b7c0">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_348c0107-e87b-4c20-b35e-5481bdc21c11" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_88e7f637-6976-4f83-8c56-d982d526d693">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_2bcc8b3f-44b5-483a-a756-f50457044384">
                      <gml:posList>550140.809000000360000000 5804574.083999999800000000 69.852515947408151000 550125.118999999020000000 5804575.734000000200000000 69.930098046606261000 550125.118999999020000000 5804575.734000000200000000 53.010000000000005000 550140.809000000360000000 5804574.083999999800000000 53.010000000000005000 550140.809000000360000000 5804574.083999999800000000 69.852515947408151000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_f15fce83-c135-4f7a-b3c7-8fd1665f8344">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_b29e3b4a-ef89-4631-a284-8c355e8f0902" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_57840134-3d2f-40b4-8644-244d9e04cee5">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_bc79c830-e5ee-46ee-9a2b-9fd930f68c2c">
                      <gml:posList>550125.118999999020000000 5804575.734000000200000000 69.930098046606261000 550124.515767494680000000 5804576.400525664900000000 70.610922432797878000 550123.879999998960000000 5804577.103000000100000000 69.867008070252027000 550123.879999998960000000 5804577.103000000100000000 53.010000000000005000 550124.515767494680000000 5804576.400525664900000000 53.009999999999998000 550125.118999999020000000 5804575.734000000200000000 53.010000000000005000 550125.118999999020000000 5804575.734000000200000000 69.930098046606261000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_a2a7dafc-8cf9-4ebf-9a1d-de9137165de7">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_683f09b8-3d8b-4b80-9ed7-5f61f7080bbc" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_be23182d-b18d-411e-a0d3-4e41e97707e5">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_70167d8c-5323-4b2b-ab8c-ee837ac41977">
                      <gml:posList>550124.351999998090000000 5804591.839999999900000000 53.010000000000005000 550123.879999998960000000 5804577.103000000100000000 53.010000000000005000 550123.879999998960000000 5804577.103000000100000000 69.867008070252027000 550124.351999998090000000 5804591.839999999900000000 69.954455553155867000 550124.351999998090000000 5804591.839999999900000000 53.010000000000005000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_38a75caf-679d-4a4a-8c30-9b2973efea62">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_f937def8-8335-4c08-92f7-674544189700" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_8ac7b588-915b-4f72-92ee-34a57286897d">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_d10059e0-fd89-47d1-bbda-4c260f4380ac">
                      <gml:posList>550134.287000000480000000 5804590.831000000200000000 53.010000000000005000 550128.934755480730000000 5804591.374574707800000000 53.009999999999998000 550124.351999998090000000 5804591.839999999900000000 53.010000000000005000 550124.351999998090000000 5804591.839999999900000000 69.954455553155867000 550128.934755480730000000 5804591.374574707800000000 75.176677596800403000 550134.287000000480000000 5804590.831000000200000000 72.142399773654716000 550134.287000000480000000 5804590.831000000200000000 53.010000000000005000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="UUID_b66a1bb4-a14e-4b62-9421-acaa5c47f159">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_4f228f4a-a938-4123-a376-a60a4f8d106c" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_6f9f0d8b-c48e-4a68-b3c6-eaa671117a02">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_e6edbb11-d026-43ca-8118-07d2b10b2098">
                      <gml:posList>550141.682999998330000000 5804584.064000000200000000 53.010000000000005000 550141.228843489890000000 5804578.878093862000000000 53.009999999999998000 550140.809000000360000000 5804574.083999999800000000 53.010000000000005000 550125.118999999020000000 5804575.734000000200000000 53.010000000000005000 550124.515767494680000000 5804576.400525664900000000 53.009999999999998000 550123.879999998960000000 5804577.103000000100000000 53.010000000000005000 550124.351999998090000000 5804591.839999999900000000 53.010000000000005000 550128.934755480730000000 5804591.374574707800000000 53.009999999999998000 550134.287000000480000000 5804590.831000000200000000 53.010000000000005000 550133.885999999940000000 5804584.781000000400000000 53.010000000000005000 550133.963047059140000000 5804584.773914872700000000 53.009999999999998000 550141.682999998330000000 5804584.064000000200000000 53.010000000000005000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </cityObjectMember>

  <cityObjectMember>
    <bldg:Building gml:id="DENIAL43000053GH">
      <gml:boundedBy>
        <gml:Envelope srsDimension="3" srsName="urn:adv:crs:ETRS89_UTM32*DE_DHHN92_NH">
          <gml:lowerCorner>550170.687500 5804607.000000 53.519997
</gml:lowerCorner>
          <gml:upperCorner>550189.187500 5804620.500000 71.671394
</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <creationDate>2017-11-21</creationDate>
      <gen:doubleAttribute name="Volume">
        <gen:value>3081.502</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Profile">
        <gen:value>Sig3d</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCK">
        <gen:value>10</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOECKE">
        <gen:value>111010</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STBEZNR">
        <gen:value>111</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="MBZNR">
        <gen:value>1102</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="WBZNR">
        <gen:value>1102</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNAM">
        <gen:value>Vahrenwald</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTNR">
        <gen:value>11</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNAM">
        <gen:value>Vahrenwald-List</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBEZ">
        <gen:value>02</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEMEINDE">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SCALE">
        <gen:value>5000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SHAPE_Area">
        <gen:value>7165.30049704715</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ALKISID">
        <gen:value>DENIAL43000053GH</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ZMIN">
        <gen:value>53.52</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="THICKNESS">
        <gen:value>0.1</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="TEXTURE">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="BUILDINGID">
        <gen:value>DENIAL43000053GH</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ARTBEZEICH">
        <gen:value>Gebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Join_Cou_1">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="FUNKTION_T">
        <gen:value>Wohngebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="OBJFLAECHE">
        <gen:value>187.89</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADT">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEB_Funkti">
        <gen:value>1000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STRSCHL">
        <gen:value>02557</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STR_komple">
        <gen:value>Seydlitzstraße 32</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="PLZ_NUMMER">
        <gen:value>30161</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STR">
        <gen:value>Seydlitzstraße</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="HAUSNR">
        <gen:value>32</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="OVERHANG">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <app:appearance>
        <app:Appearance>
          <app:theme>RhinoCity ObliqueTexturing</app:theme>
          <app:surfaceDataMember>
            <app:ParameterizedTexture>
              <app:imageURI>Hannover_teil1_Appearance/DENIAL43000053GH.jpg</app:imageURI>
              <app:textureType>specific</app:textureType>
              <app:wrapMode>border</app:wrapMode>
              <app:borderColor>0 0 0 1</app:borderColor>
              <app:target uri="#UUID_38bef11e-44ad-4f6a-9967-c9dc8f665e6a">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_d46b211f-11c2-42d0-a452-d2bdeb2d0927">0.440211 0.610568 0.470292 0.751737 0.029688 0.797583 0.001957 0.659499 0.368148 0.618612 0.440211 0.610568 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_72535d23-750e-4c5d-8567-10225d8d90eb">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_d0977a7e-2dcd-4c33-8128-02f8cb0914d0">0.870620 0.537625 0.430528 0.584597 0.430651 0.441154 0.871141 0.395303 0.870620 0.537625 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_ebc3b98e-48fb-4bf1-9b0b-979029ebf0c1">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_b1cd4da4-aa11-4b86-a2f6-f3fd3b52d092">0.424830 0.568405 0.361099 0.574628 0.037509 0.606236 0.001957 0.432092 0.323572 0.401355 0.386915 0.395303 0.424830 0.568405 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_2db0df9b-c118-4d75-98c7-ea0a4526c9b9">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_df4446d7-181f-4f2b-a00a-da8026f6369b">0.146840 0.292172 0.133054 0.151818 0.119374 0.012546 0.177992 0.001957 0.205220 0.140550 0.205100 0.284505 0.146840 0.292172 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_45d915cd-c94c-423d-ad36-0ec360a1dba7">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_5a87841c-7736-47c9-9c04-fcc518826235">0.209393 0.035851 0.587090 0.001957 0.638086 0.178024 0.258049 0.212349 0.209393 0.035851 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_9d2b0d1d-cbbf-42fd-9079-fffcdf7648e5">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_7c6e1284-087e-4871-87a8-462d80f23bce">0.063529 0.391083 0.020835 0.326180 0.001957 0.176050 0.054661 0.001957 0.085471 0.110978 0.115424 0.216967 0.063529 0.391083 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
            </app:ParameterizedTexture>
          </app:surfaceDataMember>
        </app:Appearance>
      </app:appearance>
      <bldg:measuredHeight uom="#m">18.151</bldg:measuredHeight>
      <bldg:lod1Solid>
        <gml:Solid gml:id="UUID_d5511935-e969-4eef-85ee-5e7b88b37bcf">
          <gml:exterior>
            <gml:CompositeSurface gml:id="UUID_6e08362d-972e-4f2c-8e00-63a9d953222d">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_1ff7328e-4685-436c-b0d3-f5b6a325763b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_85371d09-b065-480f-a80e-380ac1ae6cbb">
                      <gml:posList>550189.045000001790000000 5804618.409000000000000000 71.671389425948504000 550188.472898623440000000 5804612.974036922700000000 71.671389425948504000 550187.886999998240000000 5804607.407999999800000000 71.671389425948504000 550187.886999998240000000 5804607.407999999800000000 53.520000000000010000 550188.472898623440000000 5804612.974036922700000000 53.520000000000003000 550189.045000001790000000 5804618.409000000000000000 53.520000000000010000 550189.045000001790000000 5804618.409000000000000000 71.671389425948504000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_a3f9ed09-27ed-4ab3-971a-9207e1fbcd0f">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_b282a81e-89ff-4215-80e8-cb771b14692e">
                      <gml:posList>550187.886999998240000000 5804607.407999999800000000 71.671389425948504000 550185.105000000450000000 5804607.723000000200000000 71.671389425948504000 550170.969000000510000000 5804609.324000000000000000 71.671389425948504000 550170.969000000510000000 5804609.324000000000000000 53.520000000000010000 550185.105000000450000000 5804607.723000000200000000 53.520000000000003000 550187.886999998240000000 5804607.407999999800000000 53.520000000000010000 550187.886999998240000000 5804607.407999999800000000 71.671389425948504000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_d135a784-ead4-491e-b6a9-0211c325221b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_5d3f35ec-8c30-4126-b375-1ddaefe8af1f">
                      <gml:posList>550172.067999999970000000 5804620.246000000300000000 53.520000000000010000 550171.516376610960000000 5804614.763897491600000000 53.520000000000010000 550170.969000000510000000 5804609.324000000000000000 53.520000000000010000 550170.969000000510000000 5804609.324000000000000000 71.671389425948504000 550171.516376610960000000 5804614.763897491600000000 71.671389425948504000 550172.067999999970000000 5804620.246000000300000000 71.671389425948504000 550172.067999999970000000 5804620.246000000300000000 53.520000000000010000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_03f416aa-b05c-42bf-a9ad-c396ab7dba32">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_2d709343-eefb-41b7-bb5c-35e48039a2ee">
                      <gml:posList>550189.045000001790000000 5804618.409000000000000000 53.520000000000010000 550172.067999999970000000 5804620.246000000300000000 53.520000000000010000 550172.067999999970000000 5804620.246000000300000000 71.671389425948504000 550189.045000001790000000 5804618.409000000000000000 71.671389425948504000 550189.045000001790000000 5804618.409000000000000000 53.520000000000010000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_60706858-3334-4440-844e-69db04e5795b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_c6fc2894-d7d0-4b4a-bb96-4e3063f6be9c">
                      <gml:posList>550188.472898623440000000 5804612.974036922700000000 53.520000000000003000 550187.886999998240000000 5804607.407999999800000000 53.520000000000010000 550185.105000000450000000 5804607.723000000200000000 53.520000000000003000 550170.969000000510000000 5804609.324000000000000000 53.520000000000010000 550171.516376610960000000 5804614.763897491600000000 53.520000000000010000 550172.067999999970000000 5804620.246000000300000000 53.520000000000010000 550189.045000001790000000 5804618.409000000000000000 53.520000000000010000 550188.472898623440000000 5804612.974036922700000000 53.520000000000003000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_eeaf936b-0945-47e5-97ef-6c266ff8956c">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_1fd6eb8c-036a-4a82-bf3c-f02099a164d0">
                      <gml:posList>550188.472898623440000000 5804612.974036922700000000 71.671389425948504000 550189.045000001790000000 5804618.409000000000000000 71.671389425948504000 550172.067999999970000000 5804620.246000000300000000 71.671389425948504000 550171.516376610960000000 5804614.763897491600000000 71.671389425948504000 550170.969000000510000000 5804609.324000000000000000 71.671389425948504000 550185.105000000450000000 5804607.723000000200000000 71.671389425948504000 550187.886999998240000000 5804607.407999999800000000 71.671389425948504000 550188.472898623440000000 5804612.974036922700000000 71.671389425948504000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod1Solid>
      <bldg:lod2Solid>
        <gml:Solid gml:id="UUID_0f954cbd-3190-4bd0-b7c1-25ba5e921b6e">
          <gml:exterior>
            <gml:CompositeSurface gml:id="UUID_b792caec-2d72-464c-a81c-11f5865fd5c8">
              <gml:surfaceMember xlink:href="#UUID_38bef11e-44ad-4f6a-9967-c9dc8f665e6a"/>
              <gml:surfaceMember xlink:href="#UUID_72535d23-750e-4c5d-8567-10225d8d90eb"/>
              <gml:surfaceMember xlink:href="#UUID_ebc3b98e-48fb-4bf1-9b0b-979029ebf0c1"/>
              <gml:surfaceMember xlink:href="#UUID_2db0df9b-c118-4d75-98c7-ea0a4526c9b9"/>
              <gml:surfaceMember xlink:href="#UUID_45d915cd-c94c-423d-ad36-0ec360a1dba7"/>
              <gml:surfaceMember xlink:href="#UUID_9d2b0d1d-cbbf-42fd-9079-fffcdf7648e5"/>
              <gml:surfaceMember xlink:href="#UUID_c6d50b62-5812-432e-971e-7499badaceb2"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_351dac18-a89b-4254-990a-6c571e8b8318">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_390f2d9b-1fec-4f25-8526-0cf46c1f24cb" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_38bef11e-44ad-4f6a-9967-c9dc8f665e6a">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_d46b211f-11c2-42d0-a452-d2bdeb2d0927">
                      <gml:posList>550187.886999998240000000 5804607.407999999800000000 68.221900720807213000 550188.472898623440000000 5804612.974036922700000000 71.632396868368659000 550171.516376610960000000 5804614.763897491600000000 71.632355222178546000 550170.969000000510000000 5804609.324000000000000000 68.300763756113724000 550185.105000000450000000 5804607.723000000200000000 68.234828071464847000 550187.886999998240000000 5804607.407999999800000000 68.221900720807213000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_56227ab0-c69d-4bab-bc48-24b99a69daba">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_666cfa20-7ff3-4f2a-abef-e21c89d9232a" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_72535d23-750e-4c5d-8567-10225d8d90eb">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_d0977a7e-2dcd-4c33-8128-02f8cb0914d0">
                      <gml:posList>550189.045000001790000000 5804618.409000000000000000 68.170844628489135000 550172.067999999970000000 5804620.246000000300000000 68.142471120066929000 550171.516376610960000000 5804614.763897491600000000 71.632355222178546000 550188.472898623440000000 5804612.974036922700000000 71.632396868368659000 550189.045000001790000000 5804618.409000000000000000 68.170844628489135000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_95f4b511-c0f6-49d1-b6d6-87a48e89b030">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_d1affe5c-0726-40a5-8b2d-b5c4c7ec0354" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_ebc3b98e-48fb-4bf1-9b0b-979029ebf0c1">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_b1cd4da4-aa11-4b86-a2f6-f3fd3b52d092">
                      <gml:posList>550187.886999998240000000 5804607.407999999800000000 68.221900720807213000 550185.105000000450000000 5804607.723000000200000000 68.234828071464847000 550170.969000000510000000 5804609.324000000000000000 68.300763756113724000 550170.969000000510000000 5804609.324000000000000000 53.520000000000010000 550185.105000000450000000 5804607.723000000200000000 53.520000000000003000 550187.886999998240000000 5804607.407999999800000000 53.520000000000010000 550187.886999998240000000 5804607.407999999800000000 68.221900720807213000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_2bd758cb-f54e-4e2b-b00a-77d31ec68751">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_72e264bb-f30a-43af-a640-2bdfbbbd1c44" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_2db0df9b-c118-4d75-98c7-ea0a4526c9b9">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_df4446d7-181f-4f2b-a00a-da8026f6369b">
                      <gml:posList>550172.067999999970000000 5804620.246000000300000000 53.520000000000010000 550171.516376610960000000 5804614.763897491600000000 53.520000000000010000 550170.969000000510000000 5804609.324000000000000000 53.520000000000010000 550170.969000000510000000 5804609.324000000000000000 68.300763756113724000 550171.516376610960000000 5804614.763897491600000000 71.632355222178546000 550172.067999999970000000 5804620.246000000300000000 68.142471120066929000 550172.067999999970000000 5804620.246000000300000000 53.520000000000010000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_b355d940-d122-483b-b4a0-2aaaa98d8bff">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_766f7659-cd67-4114-ad1e-c6ea10849213" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_45d915cd-c94c-423d-ad36-0ec360a1dba7">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_5a87841c-7736-47c9-9c04-fcc518826235">
                      <gml:posList>550189.045000001790000000 5804618.409000000000000000 53.520000000000010000 550172.067999999970000000 5804620.246000000300000000 53.520000000000010000 550172.067999999970000000 5804620.246000000300000000 68.142471120066929000 550189.045000001790000000 5804618.409000000000000000 68.170844628489135000 550189.045000001790000000 5804618.409000000000000000 53.520000000000010000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_ff1973bb-33f7-4c9b-b3b2-4496492f1622">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_e29e6513-1623-42df-99e0-15dc0cfdd599" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_9d2b0d1d-cbbf-42fd-9079-fffcdf7648e5">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_7c6e1284-087e-4871-87a8-462d80f23bce">
                      <gml:posList>550189.045000001790000000 5804618.409000000000000000 68.170844628489135000 550188.472898623440000000 5804612.974036922700000000 71.632396868368659000 550187.886999998240000000 5804607.407999999800000000 68.221900720807213000 550187.886999998240000000 5804607.407999999800000000 53.520000000000010000 550188.472898623440000000 5804612.974036922700000000 53.520000000000003000 550189.045000001790000000 5804618.409000000000000000 53.520000000000010000 550189.045000001790000000 5804618.409000000000000000 68.170844628489135000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="UUID_ca2676f4-1079-4d17-a2b6-2d2853fbb3e5">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_908bcd8f-58f6-42b7-bc44-09547d94d3a5" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_c6d50b62-5812-432e-971e-7499badaceb2">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_1e98d39f-6f91-4c2b-b045-bd32b864acee">
                      <gml:posList>550188.472898623440000000 5804612.974036922700000000 53.520000000000003000 550187.886999998240000000 5804607.407999999800000000 53.520000000000010000 550185.105000000450000000 5804607.723000000200000000 53.520000000000003000 550170.969000000510000000 5804609.324000000000000000 53.520000000000010000 550171.516376610960000000 5804614.763897491600000000 53.520000000000010000 550172.067999999970000000 5804620.246000000300000000 53.520000000000010000 550189.045000001790000000 5804618.409000000000000000 53.520000000000010000 550188.472898623440000000 5804612.974036922700000000 53.520000000000003000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </cityObjectMember>

  <cityObjectMember>
    <bldg:Building gml:id="DENIAL43000053lQ">
      <gml:boundedBy>
        <gml:Envelope srsDimension="3" srsName="urn:adv:crs:ETRS89_UTM32*DE_DHHN92_NH">
          <gml:lowerCorner>550123.687500 5804590.000000 53.189999
</gml:lowerCorner>
          <gml:upperCorner>550136.187500 5804607.500000 75.518051
</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <creationDate>2017-11-21</creationDate>
      <gen:doubleAttribute name="Volume">
        <gen:value>3387.456</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Profile">
        <gen:value>Sig3d</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCK">
        <gen:value>10</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOECKE">
        <gen:value>111010</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STBEZNR">
        <gen:value>111</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="MBZNR">
        <gen:value>1102</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="WBZNR">
        <gen:value>1102</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNAM">
        <gen:value>Vahrenwald</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTNR">
        <gen:value>11</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNAM">
        <gen:value>Vahrenwald-List</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBEZ">
        <gen:value>02</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEMEINDE">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SCALE">
        <gen:value>5000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SHAPE_Area">
        <gen:value>7165.30049704715</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ALKISID">
        <gen:value>DENIAL43000053lQ</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ZMIN">
        <gen:value>53.19</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="THICKNESS">
        <gen:value>0.1</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="TEXTURE">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="BUILDINGID">
        <gen:value>DENIAL43000053lQ</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ARTBEZEICH">
        <gen:value>Gebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Join_Cou_1">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="FUNKTION_T">
        <gen:value>Wohngebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="OBJFLAECHE">
        <gen:value>166.61</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADT">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEB_Funkti">
        <gen:value>1000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STRSCHL">
        <gen:value>00689</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STR_komple">
        <gen:value>Dessauerstraße 12</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="PLZ_NUMMER">
        <gen:value>30161</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STR">
        <gen:value>Dessauerstraße</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="HAUSNR">
        <gen:value>12</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="OVERHANG">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <app:appearance>
        <app:Appearance>
          <app:theme>RhinoCity ObliqueTexturing</app:theme>
          <app:surfaceDataMember>
            <app:ParameterizedTexture>
              <app:imageURI>Hannover_teil1_Appearance/DENIAL43000053lQ.jpg</app:imageURI>
              <app:textureType>specific</app:textureType>
              <app:wrapMode>border</app:wrapMode>
              <app:borderColor>0 0 0 1</app:borderColor>
              <app:target uri="#UUID_956225e0-e9f2-4462-8aad-bc712ffc9402">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_175853c8-53e4-4eb9-b764-79016a9edf07">0.400507 0.604588 0.401511 0.643486 0.305672 0.646537 0.303327 0.527477 0.398440 0.524462 0.400507 0.604588 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_4fc9ceb2-be9f-4d99-8bee-ffe6e46f0982">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_90c3efcc-96cb-4187-b7b2-224192de2f00">0.505229 0.643486 0.409390 0.646536 0.407045 0.527477 0.502158 0.524462 0.505229 0.643486 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_ab4366d3-33e1-475c-b22e-3cc2ac69db88">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_67909d00-b53f-4720-b01a-1aee18a3eb75">0.472776 0.233098 0.383367 0.234452 0.385732 0.354809 0.475822 0.353276 0.476718 0.388471 0.469669 0.389021 0.317515 0.400916 0.307241 0.015735 0.435047 0.004698 0.466920 0.001957 0.467640 0.030344 0.378215 0.031438 0.380579 0.151798 0.470685 0.150526 0.472776 0.233098 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_547aa66d-8a51-41db-8ba1-659ac7f86f5a">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_403053aa-09dc-4ad5-a700-279514d7dc6b">0.592514 0.592854 0.518417 0.595878 0.516634 0.525918 0.551811 0.524462 0.592514 0.592854 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_b3cbd7c2-37bf-4c1f-b182-564151c09c0d">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_5413986e-69df-40b2-acf9-46e0ec5ef51d">0.787766 0.526310 0.821081 0.524462 0.821641 0.586075 0.751468 0.588763 0.787766 0.526310 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_fe8c26b6-41b1-416a-b25c-1d5ab1ab8865">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_1d549af5-6aff-48b9-b931-e04ed4b84486">0.207411 0.281928 0.160875 0.285527 0.156128 0.132569 0.202912 0.128285 0.253237 0.073475 0.199506 0.018897 0.152724 0.022874 0.152642 0.020242 0.291462 0.001957 0.301721 0.387147 0.164495 0.402159 0.164280 0.395223 0.210814 0.391320 0.261154 0.336904 0.207411 0.281928 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_4031e5ed-3e61-4124-9730-eaac192359b1">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_73b622c1-36cc-4b81-a119-7d34110230d5">0.713551 0.526313 0.746810 0.524462 0.747368 0.586283 0.677104 0.588988 0.713551 0.526313 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_e6314f68-3a6b-4cfc-9759-cdd2bb093a88">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_4bbc4e97-55e0-46bb-8237-a090aead5b07">0.672245 0.592595 0.598643 0.595587 0.596869 0.525899 0.631709 0.524462 0.672245 0.592595 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_8c474b96-f30b-465d-94d8-ea05dbef16c4">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_90ae7a69-4925-4892-a673-75834faace04">0.258891 0.822808 0.224859 0.824170 0.221135 0.802348 0.258891 0.822808 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_fecef690-b907-451c-8ded-e5d8a465474c">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_4d1b0aa6-a079-4814-8bda-6a34b1e6aec1">0.863014 0.548565 0.888474 0.524462 0.895761 0.546800 0.863014 0.548565 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_c90c0e6f-baea-4590-a134-5a9b3c0cd04d">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_87d12c7b-da45-4590-b516-3f0b046a7929">0.825832 0.548747 0.851356 0.524462 0.858632 0.546985 0.825832 0.548747 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_aebb4cc7-bea5-41a6-9dd9-bb21c04c5489">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_0cfcbef8-685d-4ae8-a871-a597fe32f4b0">0.040087 0.822833 0.005727 0.824214 0.001957 0.802348 0.040087 0.822833 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_f600b628-f908-4ffb-b725-06ea2a45c325">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_67c26b2c-c3f5-4b58-8e0a-706b2223f0f9">0.900196 0.524462 0.988526 0.543916 0.988604 0.544148 0.907374 0.546192 0.900196 0.524462 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_c6eff50e-f540-4824-895d-15516b3fffce">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_76d93293-2a35-497f-810b-b97ac06fd3c3">0.128272 0.823297 0.045052 0.825364 0.045010 0.825134 0.124383 0.802348 0.128272 0.823297 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_694dd7c1-8413-415f-850d-0552a7a3ccea">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_0c8cef80-6b0c-4741-8cdf-64ffc777309d">0.511959 0.643477 0.510763 0.524480 0.510799 0.524462 0.511994 0.643461 0.511959 0.643477 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_411a7a2d-6f3b-4a71-887e-9217a4be47b6">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_d3520e31-7eea-46ff-84eb-a5a442889477">0.212694 0.802348 0.216557 0.822980 0.133072 0.825061 0.212694 0.802348 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_b193e889-f27c-4ece-bb2a-c218409c00d9">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_5f413d33-955a-40cb-a3ca-e24b821b6206">0.264188 0.802348 0.352177 0.821687 0.271233 0.823704 0.264188 0.802348 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_acbf0531-544a-49d8-a69d-f47b6bd66c3b">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_c4d9d4a1-930b-4bfc-a3e4-27dd971cd7c1">0.561644 0.020101 0.568108 0.019626 0.707330 0.009394 0.808517 0.001957 0.871886 0.199319 0.792518 0.277800 0.639742 0.248999 0.632659 0.247664 0.561644 0.020101 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_d2a52f5a-ac28-4d92-8a9e-82d631ab3386">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_3ccf9e65-db50-48c9-8255-cd4fe7e8f6da">0.297684 0.745298 0.268637 0.755850 0.152281 0.798156 0.033892 0.737700 0.001957 0.546709 0.107635 0.537543 0.228370 0.527071 0.258557 0.524462 0.297684 0.745298 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_38cbf4e7-262a-4288-8a77-eec31047c1e4">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_bc86002a-621b-40ab-8979-ec026cc75053">0.056495 0.515215 0.049891 0.485280 0.036262 0.423504 0.044191 0.400860 0.030227 0.337788 0.022317 0.360293 0.001957 0.268002 0.009838 0.245700 0.005011 0.223898 0.083544 0.001957 0.088282 0.023713 0.108296 0.115611 0.122003 0.178555 0.135401 0.240071 0.141892 0.269880 0.147712 0.296562 0.070375 0.519102 0.064451 0.492368 0.056495 0.515215 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_76adcc80-3017-44e0-9943-6bc03795ffe1">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_02c2af6a-4bad-4550-b386-65518ba6e43c">0.544193 0.376911 0.544406 0.383847 0.492936 0.396343 0.492727 0.389485 0.491190 0.339141 0.489416 0.281007 0.484799 0.129746 0.483263 0.079403 0.481488 0.021268 0.481409 0.018665 0.532699 0.001957 0.532779 0.004588 0.539123 0.002526 0.547963 0.059191 0.542424 0.112383 0.536142 0.114275 0.540830 0.267223 0.547150 0.265528 0.556002 0.322592 0.550451 0.375383 0.544193 0.376911 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
            </app:ParameterizedTexture>
          </app:surfaceDataMember>
        </app:Appearance>
      </app:appearance>
      <bldg:measuredHeight uom="#m">22.328</bldg:measuredHeight>
      <bldg:lod1Solid>
        <gml:Solid gml:id="UUID_0ae39b3a-da69-42df-9cf2-7c12faac18a3">
          <gml:exterior>
            <gml:CompositeSurface gml:id="UUID_9d17ba76-5e47-4a0e-9231-e182c06aa8bb">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_faee394c-9202-4cf6-9587-d658df75e2ca">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_95e47b91-add1-4b46-885c-96c6bd1973b2">
                      <gml:posList>550136.008999999610000000 5804605.576999999600000000 75.518049235205012000 550135.972403789870000000 5804604.222058336300000000 75.518049235205012000 550135.932000000030000000 5804602.710000000000000000 75.518049235205012000 550135.848773652690000000 5804599.595364139400000000 75.518049235205012000 550135.763831196700000000 5804596.416505187700000000 75.518049235205012000 550135.640201059520000000 5804591.789810990900000000 75.518049235205012000 550135.611000001430000000 5804590.696999999700000000 75.518049235205012000 550135.611000001430000000 5804590.696999999700000000 53.189999999999998000 550135.640201059520000000 5804591.789810990900000000 53.189999999999998000 550135.763831196700000000 5804596.416505187700000000 53.189999999999998000 550135.848773652690000000 5804599.595364139400000000 53.189999999999998000 550135.932000000030000000 5804602.710000000000000000 53.189999999999998000 550135.972403789870000000 5804604.222058336300000000 53.189999999999998000 550136.008999999610000000 5804605.576999999600000000 53.189999999999998000 550136.008999999610000000 5804605.576999999600000000 75.518049235205012000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_6e535790-e389-4624-8d17-35090daca389">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_0dad092b-8475-4bf3-a606-9b8fa2c78c5e">
                      <gml:posList>550135.611000001430000000 5804590.696999999700000000 75.518049235205012000 550134.287000000480000000 5804590.831000000200000000 75.518049235205012000 550128.990161502730000000 5804591.368947664300000000 75.518049235205012000 550124.351999998090000000 5804591.839999999900000000 75.518049235205012000 550124.351999998090000000 5804591.839999999900000000 53.189999999999998000 550128.990161502730000000 5804591.368947664300000000 53.189999999999998000 550134.287000000480000000 5804590.831000000200000000 53.189999999999998000 550135.611000001430000000 5804590.696999999700000000 53.189999999999998000 550135.611000001430000000 5804590.696999999700000000 75.518049235205012000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_28a84440-77ad-4198-9db0-24585f31d819">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_85befecd-947b-4fe2-81dc-b970f805cef4">
                      <gml:posList>550124.822999998930000000 5804606.570000000300000000 53.189999999999998000 550124.814446637520000000 5804606.302503120200000000 53.189999999999998000 550124.751663764940000000 5804604.339038761300000000 53.189999999999998000 550124.679164133270000000 5804602.071693593600000000 53.189999999999998000 550124.490528097260000000 5804596.172311828500000000 53.189999999999998000 550124.427745224680000000 5804594.208847469700000000 53.189999999999998000 550124.355245593000000000 5804591.941502302000000000 53.189999999999998000 550124.351999998090000000 5804591.839999999900000000 53.189999999999998000 550124.351999998090000000 5804591.839999999900000000 75.518049235205012000 550124.355245593000000000 5804591.941502302000000000 75.518049235205012000 550124.427745224680000000 5804594.208847469700000000 75.518049235205012000 550124.490528097260000000 5804596.172311828500000000 75.518049235205012000 550124.679164133270000000 5804602.071693593600000000 75.518049235205012000 550124.751663764940000000 5804604.339038761300000000 75.518049235205012000 550124.814446637520000000 5804606.302503120200000000 75.518049235205012000 550124.822999998930000000 5804606.570000000300000000 75.518049235205012000 550124.822999998930000000 5804606.570000000300000000 53.189999999999998000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_f4c32db3-ce95-424a-8a8c-68729dd9af65">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_acd6590b-b563-4bff-852f-fe4d47d4bda5">
                      <gml:posList>550136.008999999610000000 5804605.576999999600000000 53.189999999999998000 550135.715999998150000000 5804605.603000000100000000 53.189999999999998000 550129.406767500450000000 5804606.163087012200000000 53.189999999999998000 550124.822999998930000000 5804606.570000000300000000 53.189999999999998000 550124.822999998930000000 5804606.570000000300000000 75.518049235205012000 550129.406767500450000000 5804606.163087012200000000 75.518049235205012000 550135.715999998150000000 5804605.603000000100000000 75.518049235205012000 550136.008999999610000000 5804605.576999999600000000 75.518049235205012000 550136.008999999610000000 5804605.576999999600000000 53.189999999999998000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_66a23c1e-6782-4b2c-8bc5-b5ace5d3bbeb">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_aa8bd997-4a33-4581-9119-4779967edda4">
                      <gml:posList>550136.008999999610000000 5804605.576999999600000000 53.189999999999998000 550135.972403789870000000 5804604.222058336300000000 53.189999999999998000 550135.932000000030000000 5804602.710000000000000000 53.189999999999998000 550135.848773652690000000 5804599.595364139400000000 53.189999999999998000 550135.763831196700000000 5804596.416505187700000000 53.189999999999998000 550135.640201059520000000 5804591.789810990900000000 53.189999999999998000 550135.611000001430000000 5804590.696999999700000000 53.189999999999998000 550134.287000000480000000 5804590.831000000200000000 53.189999999999998000 550128.990161502730000000 5804591.368947664300000000 53.189999999999998000 550124.351999998090000000 5804591.839999999900000000 53.189999999999998000 550124.355245593000000000 5804591.941502302000000000 53.189999999999998000 550124.427745224680000000 5804594.208847469700000000 53.189999999999998000 550124.490528097260000000 5804596.172311828500000000 53.189999999999998000 550124.679164133270000000 5804602.071693593600000000 53.189999999999998000 550124.751663764940000000 5804604.339038761300000000 53.189999999999998000 550124.814446637520000000 5804606.302503120200000000 53.189999999999998000 550124.822999998930000000 5804606.570000000300000000 53.189999999999998000 550129.406767500450000000 5804606.163087012200000000 53.189999999999998000 550135.715999998150000000 5804605.603000000100000000 53.189999999999998000 550136.008999999610000000 5804605.576999999600000000 53.189999999999998000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_a7d3ab03-3a35-4745-89e4-abdad6669c89">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_55c232b0-7020-4ae8-98c4-83f0eb2bf8d7">
                      <gml:posList>550136.008999999610000000 5804605.576999999600000000 75.518049235205012000 550135.715999998150000000 5804605.603000000100000000 75.518049235205012000 550129.406767500450000000 5804606.163087012200000000 75.518049235205012000 550124.822999998930000000 5804606.570000000300000000 75.518049235205012000 550124.814446637520000000 5804606.302503120200000000 75.518049235205012000 550124.751663764940000000 5804604.339038761300000000 75.518049235205012000 550124.679164133270000000 5804602.071693593600000000 75.518049235205012000 550124.490528097260000000 5804596.172311828500000000 75.518049235205012000 550124.427745224680000000 5804594.208847469700000000 75.518049235205012000 550124.355245593000000000 5804591.941502302000000000 75.518049235205012000 550124.351999998090000000 5804591.839999999900000000 75.518049235205012000 550128.990161502730000000 5804591.368947664300000000 75.518049235205012000 550134.287000000480000000 5804590.831000000200000000 75.518049235205012000 550135.611000001430000000 5804590.696999999700000000 75.518049235205012000 550135.640201059520000000 5804591.789810990900000000 75.518049235205012000 550135.763831196700000000 5804596.416505187700000000 75.518049235205012000 550135.848773652690000000 5804599.595364139400000000 75.518049235205012000 550135.932000000030000000 5804602.710000000000000000 75.518049235205012000 550135.972403789870000000 5804604.222058336300000000 75.518049235205012000 550136.008999999610000000 5804605.576999999600000000 75.518049235205012000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod1Solid>
      <bldg:lod2Solid>
        <gml:Solid gml:id="UUID_919eeaf5-217b-4d4a-9bf9-66e3622ad371">
          <gml:exterior>
            <gml:CompositeSurface gml:id="UUID_15f2dbcc-7cf0-49a1-b5a8-e34d007799de">
              <gml:surfaceMember xlink:href="#UUID_956225e0-e9f2-4462-8aad-bc712ffc9402"/>
              <gml:surfaceMember xlink:href="#UUID_4fc9ceb2-be9f-4d99-8bee-ffe6e46f0982"/>
              <gml:surfaceMember xlink:href="#UUID_ab4366d3-33e1-475c-b22e-3cc2ac69db88"/>
              <gml:surfaceMember xlink:href="#UUID_547aa66d-8a51-41db-8ba1-659ac7f86f5a"/>
              <gml:surfaceMember xlink:href="#UUID_b3cbd7c2-37bf-4c1f-b182-564151c09c0d"/>
              <gml:surfaceMember xlink:href="#UUID_fe8c26b6-41b1-416a-b25c-1d5ab1ab8865"/>
              <gml:surfaceMember xlink:href="#UUID_4031e5ed-3e61-4124-9730-eaac192359b1"/>
              <gml:surfaceMember xlink:href="#UUID_e6314f68-3a6b-4cfc-9759-cdd2bb093a88"/>
              <gml:surfaceMember xlink:href="#UUID_8c474b96-f30b-465d-94d8-ea05dbef16c4"/>
              <gml:surfaceMember xlink:href="#UUID_fecef690-b907-451c-8ded-e5d8a465474c"/>
              <gml:surfaceMember xlink:href="#UUID_c90c0e6f-baea-4590-a134-5a9b3c0cd04d"/>
              <gml:surfaceMember xlink:href="#UUID_aebb4cc7-bea5-41a6-9dd9-bb21c04c5489"/>
              <gml:surfaceMember xlink:href="#UUID_f600b628-f908-4ffb-b725-06ea2a45c325"/>
              <gml:surfaceMember xlink:href="#UUID_c6eff50e-f540-4824-895d-15516b3fffce"/>
              <gml:surfaceMember xlink:href="#UUID_694dd7c1-8413-415f-850d-0552a7a3ccea"/>
              <gml:surfaceMember xlink:href="#UUID_411a7a2d-6f3b-4a71-887e-9217a4be47b6"/>
              <gml:surfaceMember xlink:href="#UUID_b193e889-f27c-4ece-bb2a-c218409c00d9"/>
              <gml:surfaceMember xlink:href="#UUID_acbf0531-544a-49d8-a69d-f47b6bd66c3b"/>
              <gml:surfaceMember xlink:href="#UUID_d2a52f5a-ac28-4d92-8a9e-82d631ab3386"/>
              <gml:surfaceMember xlink:href="#UUID_38cbf4e7-262a-4288-8a77-eec31047c1e4"/>
              <gml:surfaceMember xlink:href="#UUID_76adcc80-3017-44e0-9943-6bc03795ffe1"/>
              <gml:surfaceMember xlink:href="#UUID_be9b7dde-7fd5-4f00-990c-0afee872a63f"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_8f0c66a5-7176-4072-94a5-d1ed63f803bf">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_6711ba32-4c92-4f65-a5e2-015dd2e4d4e4" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_956225e0-e9f2-4462-8aad-bc712ffc9402">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_175853c8-53e4-4eb9-b764-79016a9edf07">
                      <gml:posList>550135.932000000030000000 5804602.710000000000000000 73.857109069824219000 550135.972403789870000000 5804604.222058336300000000 73.857109069824219000 550132.232975746390000000 5804604.343870340800000000 73.857109069824219000 550132.137679831940000000 5804599.715782330400000000 73.857109069824219000 550135.848773652690000000 5804599.595364139400000000 73.857109069824219000 550135.932000000030000000 5804602.710000000000000000 73.857109069824219000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_d301b998-dea6-4e84-9d5a-8d2da3b302a2">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_fd958363-28be-4dc3-9d49-e053600f6a5b" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_4fc9ceb2-be9f-4d99-8bee-ffe6e46f0982">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_90c3efcc-96cb-4187-b7b2-224192de2f00">
                      <gml:posList>550135.763831196700000000 5804596.416505187700000000 73.857112889898232000 550132.024403153220000000 5804596.538317192300000000 73.857111617701932000 550131.929107238770000000 5804591.910229181900000000 73.857109924375095000 550135.640201059520000000 5804591.789810990900000000 73.857111187100628000 550135.763831196700000000 5804596.416505187700000000 73.857112889898232000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_f7dc7224-3a5c-40a9-83a9-30b31189fff7">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_4a7a1058-4237-48d0-bba5-1953bf49b268" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_ab4366d3-33e1-475c-b22e-3cc2ac69db88">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_67909d00-b53f-4720-b01a-1aee18a3eb75">
                      <gml:posList>550135.848865902400000000 5804599.595361671400000000 71.962449266961087000 550132.137679831940000000 5804599.715782330400000000 73.836322430532420000 550132.232975746390000000 5804604.343870340800000000 73.836696929125992000 550135.972575694320000000 5804604.222053736400000000 71.948451373493413000 550136.008999999610000000 5804605.576999999600000000 71.944352015617540000 550135.715999998150000000 5804605.603000000100000000 72.092462296117787000 550129.406767500450000000 5804606.163087012200000000 75.281755412791711000 550128.990161502730000000 5804591.368947664300000000 75.337062790827204000 550134.287000000480000000 5804590.831000000200000000 72.658820313192763000 550135.611000001430000000 5804590.696999999700000000 71.989370941050339000 550135.640215398280000000 5804591.789810607200000000 71.986064681507699000 550131.929107238770000000 5804591.910229181900000000 73.857109924375095000 550132.024403153220000000 5804596.538317192300000000 73.857111617701932000 550135.763906239420000000 5804596.416503180700000000 71.972066788036074000 550135.848865902400000000 5804599.595361671400000000 71.962449266961087000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_35a917cb-b140-4775-91f1-b38bfb6d1aef">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_95332b88-8dc9-4e81-b8b3-55a99f7e9077" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_547aa66d-8a51-41db-8ba1-659ac7f86f5a">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_403053aa-09dc-4ad5-a700-279514d7dc6b">
                      <gml:posList>550127.729616888680000000 5804594.051647235600000000 73.639671576619634000 550124.427745224680000000 5804594.208847469700000000 73.647894320135791000 550124.355245593000000000 5804591.941502302000000000 71.448363197096100000 550125.922921413440000000 5804591.866255682900000000 71.443867816068206000 550127.729616888680000000 5804594.051647235600000000 73.639671576619634000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_c2ec82af-3775-42b8-b1f9-dba83f16137c">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_5dfeeae4-47ff-42bc-83d9-b92122aadc52" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_b3cbd7c2-37bf-4c1f-b182-564151c09c0d">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_5413986e-69df-40b2-acf9-46e0ec5ef51d">
                      <gml:posList>550126.057988338290000000 5804596.079490095400000000 71.448855517558485000 550124.490528097260000000 5804596.172311828500000000 71.432258899839397000 550124.427745224680000000 5804594.208847469700000000 73.647894320135791000 550127.729616888680000000 5804594.051647235600000000 73.639671576619634000 550126.057988338290000000 5804596.079490095400000000 71.448855517558485000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_0d4b77d9-4349-40c1-aebf-8decca0a8609">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_a59dcf2f-f12a-44de-bffd-ad6adbffc493" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_fe8c26b6-41b1-416a-b25c-1d5ab1ab8865">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_1d549af5-6aff-48b9-b931-e04ed4b84486">
                      <gml:posList>550126.238209540020000000 5804601.996861224100000000 71.443892564168351000 550124.679164132800000000 5804602.071693593600000000 69.472016706339531000 550124.490528096330000000 5804596.172311828500000000 69.465645453744912000 550126.057988338290000000 5804596.079490095400000000 71.448855517558485000 550127.729616888680000000 5804594.051647235600000000 73.639671576619634000 550125.922921413440000000 5804591.866255682900000000 71.443867816068206000 550124.355245592070000000 5804591.941502302000000000 69.461076236622489000 550124.351999998090000000 5804591.839999999900000000 69.460966615507630000 550128.990161502730000000 5804591.368947664300000000 75.337062790827204000 550129.406767500450000000 5804606.163087012200000000 75.281755412791711000 550124.822999998930000000 5804606.570000000300000000 69.476874816478087000 550124.814446636940000000 5804606.302503120200000000 69.476585923461954000 550126.373186939160000000 5804606.210197763500000000 71.448763188899861000 550128.044902342020000000 5804604.182249544200000000 73.639693075837840000 550126.238209540020000000 5804601.996861224100000000 71.443892564168351000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_0d4083cd-7143-4722-9cc9-7d150e15d70c">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_92683448-3374-447e-9e50-c849b7e3d1ee" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_4031e5ed-3e61-4124-9730-eaac192359b1">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_73b622c1-36cc-4b81-a119-7d34110230d5">
                      <gml:posList>550126.373186939160000000 5804606.210197763500000000 71.448763188899861000 550124.814446637520000000 5804606.302503120200000000 71.432258899839425000 550124.751663764940000000 5804604.339038761300000000 73.647894320135791000 550128.044902342020000000 5804604.182249544200000000 73.639693075837840000 550126.373186939160000000 5804606.210197763500000000 71.448763188899861000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_7c0ce6c2-ee04-432d-8e36-9bef0f0f88f2">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_04c08982-9148-4385-8e2d-348d32a563dd" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_e6314f68-3a6b-4cfc-9759-cdd2bb093a88">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_4bbc4e97-55e0-46bb-8237-a090aead5b07">
                      <gml:posList>550128.044902342020000000 5804604.182249544200000000 73.639693075837840000 550124.751663764940000000 5804604.339038761300000000 73.647894320135791000 550124.679164133270000000 5804602.071693593600000000 71.448363197096100000 550126.238209540020000000 5804601.996861224100000000 71.443892564168351000 550128.044902342020000000 5804604.182249544200000000 73.639693075837840000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_39f41a0b-7b32-4fbc-8596-51fb08865186">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_c4eaa55e-692e-4581-a810-989c3587dc51" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_8c474b96-f30b-465d-94d8-ea05dbef16c4">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_90ae7a69-4925-4892-a673-75834faace04">
                      <gml:posList>550126.238209540020000000 5804601.996861224100000000 71.443892564168351000 550124.679164133270000000 5804602.071693593600000000 71.448363197096100000 550124.679164132800000000 5804602.071693593600000000 69.472016706339531000 550126.238209540020000000 5804601.996861224100000000 71.443892564168351000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_1235bba6-3ba2-4045-8f7d-b0bab24cc1e1">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_cf3cb8f6-b2a8-4388-839d-24479a197db0" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_fecef690-b907-451c-8ded-e5d8a465474c">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_4d1b0aa6-a079-4814-8bda-6a34b1e6aec1">
                      <gml:posList>550126.373186939160000000 5804606.210197763500000000 71.448763188899861000 550124.814446636940000000 5804606.302503120200000000 69.476585923461954000 550124.814446637520000000 5804606.302503120200000000 71.432258899839425000 550126.373186939160000000 5804606.210197763500000000 71.448763188899861000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_7695c113-3ac9-4e1c-8efa-5e6473254808">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_f4406901-1fc1-48ef-beb3-29b1eaef4904" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_c90c0e6f-baea-4590-a134-5a9b3c0cd04d">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_87d12c7b-da45-4590-b516-3f0b046a7929">
                      <gml:posList>550126.057988338290000000 5804596.079490095400000000 71.448855517558485000 550124.490528096330000000 5804596.172311828500000000 69.465645453744912000 550124.490528097260000000 5804596.172311828500000000 71.432258899839397000 550126.057988338290000000 5804596.079490095400000000 71.448855517558485000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_13f904a8-ce21-4172-9eee-2a28e2c49007">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_1fb555e5-bc24-4ca4-a670-95e228effa55" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_aebb4cc7-bea5-41a6-9dd9-bb21c04c5489">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_0cfcbef8-685d-4ae8-a871-a597fe32f4b0">
                      <gml:posList>550125.922921413440000000 5804591.866255682900000000 71.443867816068206000 550124.355245593000000000 5804591.941502302000000000 71.448363197096100000 550124.355245592070000000 5804591.941502302000000000 69.461076236622489000 550125.922921413440000000 5804591.866255682900000000 71.443867816068206000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_7c7906c7-bcde-4435-bbc7-15c994924989">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_ce682d66-1b35-4de2-8340-5cda304ad98f" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_f600b628-f908-4ffb-b725-06ea2a45c325">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_67c26b2c-c3f5-4b58-8e0a-706b2223f0f9">
                      <gml:posList>550135.972575694320000000 5804604.222053736400000000 71.948451373493413000 550132.232975746390000000 5804604.343870340800000000 73.836696929125992000 550132.232975746390000000 5804604.343870340800000000 73.857109069824219000 550135.972403789870000000 5804604.222058336300000000 73.857109069824219000 550135.972575694320000000 5804604.222053736400000000 71.948451373493413000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_d4ab50ab-f956-4897-be0f-10b6645199d2">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_81728db7-4a34-4ac9-9e61-46045719e52d" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_c6eff50e-f540-4824-895d-15516b3fffce">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_76d93293-2a35-497f-810b-b97ac06fd3c3">
                      <gml:posList>550135.848773652690000000 5804599.595364139400000000 73.857109069824219000 550132.137679831940000000 5804599.715782330400000000 73.857109069824219000 550132.137679831940000000 5804599.715782330400000000 73.836322430532420000 550135.848865902400000000 5804599.595361671400000000 71.962449266961087000 550135.848773652690000000 5804599.595364139400000000 73.857109069824219000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_b8c8bfc0-b15a-4d9e-b5ea-a6920bc8fc1e">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_c2d0f4d6-1e8b-4a33-8ceb-f7486c49f7b1" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_694dd7c1-8413-415f-850d-0552a7a3ccea">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_0c8cef80-6b0c-4741-8cdf-64ffc777309d">
                      <gml:posList>550132.232975746390000000 5804604.343870340800000000 73.836696929125992000 550132.137679831940000000 5804599.715782330400000000 73.836322430532420000 550132.137679831940000000 5804599.715782330400000000 73.857109069824219000 550132.232975746390000000 5804604.343870340800000000 73.857109069824219000 550132.232975746390000000 5804604.343870340800000000 73.836696929125992000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_5b745deb-d80f-4374-b226-06c3e70a7cd0">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_840b22b3-6b8e-4cf4-9a6d-6ea13d68c140" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_411a7a2d-6f3b-4a71-887e-9217a4be47b6">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_d3520e31-7eea-46ff-84eb-a5a442889477">
                      <gml:posList>550135.640215398280000000 5804591.789810607200000000 71.986064681507699000 550135.640201059520000000 5804591.789810990900000000 73.857111187100628000 550131.929107238770000000 5804591.910229181900000000 73.857109924375095000 550135.640215398280000000 5804591.789810607200000000 71.986064681507699000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_63046089-2885-4940-af04-259a1fc1f7d1">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_b9be5322-82b2-4d8a-8898-47bededc593f" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_b193e889-f27c-4ece-bb2a-c218409c00d9">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_5f413d33-955a-40cb-a3ca-e24b821b6206">
                      <gml:posList>550135.763906239420000000 5804596.416503180700000000 71.972066788036074000 550132.024403153220000000 5804596.538317192300000000 73.857111617701932000 550135.763831196700000000 5804596.416505187700000000 73.857112889898232000 550135.763906239420000000 5804596.416503180700000000 71.972066788036074000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_42671930-9399-46a9-ab87-823873588c5c">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_3a760c87-b071-4868-baef-1050634c834d" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_acbf0531-544a-49d8-a69d-f47b6bd66c3b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_c4d9d4a1-930b-4bfc-a3e4-27dd971cd7c1">
                      <gml:posList>550136.008999999610000000 5804605.576999999600000000 53.189999999999998000 550135.715999998150000000 5804605.603000000100000000 53.189999999999998000 550129.406767500450000000 5804606.163087012200000000 53.189999999999998000 550124.822999998930000000 5804606.570000000300000000 53.189999999999998000 550124.822999998930000000 5804606.570000000300000000 69.476874816478087000 550129.406767500450000000 5804606.163087012200000000 75.281755412791711000 550135.715999998150000000 5804605.603000000100000000 72.092462296117787000 550136.008999999610000000 5804605.576999999600000000 71.944352015617540000 550136.008999999610000000 5804605.576999999600000000 53.189999999999998000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_83069ea4-9454-4756-a7cd-ae197faeae38">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_514ee94d-2eb7-41e2-8e6b-08788a25f8b8" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_d2a52f5a-ac28-4d92-8a9e-82d631ab3386">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_3ccf9e65-db50-48c9-8255-cd4fe7e8f6da">
                      <gml:posList>550135.611000001430000000 5804590.696999999700000000 71.989370941050339000 550134.287000000480000000 5804590.831000000200000000 72.658820313192763000 550128.990161502730000000 5804591.368947664300000000 75.337062790827204000 550124.351999998090000000 5804591.839999999900000000 69.460966615507630000 550124.351999998090000000 5804591.839999999900000000 53.189999999999998000 550128.990161502730000000 5804591.368947664300000000 53.189999999999998000 550134.287000000480000000 5804590.831000000200000000 53.189999999999998000 550135.611000001430000000 5804590.696999999700000000 53.189999999999998000 550135.611000001430000000 5804590.696999999700000000 71.989370941050339000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_e256a5ff-bddf-4139-9fd3-5efd5ec7a81c">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_753e25c6-f0d6-43f8-b8e8-26838ad412f3" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_38cbf4e7-262a-4288-8a77-eec31047c1e4">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_bc86002a-621b-40ab-8979-ec026cc75053">
                      <gml:posList>550135.972403789870000000 5804604.222058336300000000 73.857109069824219000 550135.932000000030000000 5804602.710000000000000000 73.857109069824219000 550135.848773652690000000 5804599.595364139400000000 73.857109069824219000 550135.848865902400000000 5804599.595361671400000000 71.962449266961087000 550135.763906239420000000 5804596.416503180700000000 71.972066788036074000 550135.763831196700000000 5804596.416505187700000000 73.857112889898232000 550135.640201059520000000 5804591.789810990900000000 73.857111187100628000 550135.640215398280000000 5804591.789810607200000000 71.986064681507699000 550135.611000001430000000 5804590.696999999700000000 71.989370941050339000 550135.611000001430000000 5804590.696999999700000000 53.189999999999998000 550135.640201059520000000 5804591.789810990900000000 53.189999999999998000 550135.763831196700000000 5804596.416505187700000000 53.189999999999998000 550135.848773652690000000 5804599.595364139400000000 53.189999999999998000 550135.932000000030000000 5804602.710000000000000000 53.189999999999998000 550135.972403789870000000 5804604.222058336300000000 53.189999999999998000 550136.008999999610000000 5804605.576999999600000000 53.189999999999998000 550136.008999999610000000 5804605.576999999600000000 71.944352015617540000 550135.972575694320000000 5804604.222053736400000000 71.948451373493413000 550135.972403789870000000 5804604.222058336300000000 73.857109069824219000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_42cb5c4c-5bc9-431d-9a7a-5e573c3295eb">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_cdb77f8c-400d-42e1-a3f7-7afb88d12f3f" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_76adcc80-3017-44e0-9943-6bc03795ffe1">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_02c2af6a-4bad-4550-b386-65518ba6e43c">
                      <gml:posList>550124.814446636940000000 5804606.302503120200000000 69.476585923461954000 550124.822999998930000000 5804606.570000000300000000 69.476874816478087000 550124.822999998930000000 5804606.570000000300000000 53.189999999999998000 550124.814446637520000000 5804606.302503120200000000 53.189999999999998000 550124.751663764940000000 5804604.339038761300000000 53.189999999999998000 550124.679164133270000000 5804602.071693593600000000 53.189999999999998000 550124.490528097260000000 5804596.172311828500000000 53.189999999999998000 550124.427745224680000000 5804594.208847469700000000 53.189999999999998000 550124.355245593000000000 5804591.941502302000000000 53.189999999999998000 550124.351999998090000000 5804591.839999999900000000 53.189999999999998000 550124.351999998090000000 5804591.839999999900000000 69.460966615507630000 550124.355245592070000000 5804591.941502302000000000 69.461076236622489000 550124.355245593000000000 5804591.941502302000000000 71.448363197096100000 550124.427745224680000000 5804594.208847469700000000 73.647894320135791000 550124.490528097260000000 5804596.172311828500000000 71.432258899839397000 550124.490528096330000000 5804596.172311828500000000 69.465645453744912000 550124.679164132800000000 5804602.071693593600000000 69.472016706339531000 550124.679164133270000000 5804602.071693593600000000 71.448363197096100000 550124.751663764940000000 5804604.339038761300000000 73.647894320135791000 550124.814446637520000000 5804606.302503120200000000 71.432258899839425000 550124.814446636940000000 5804606.302503120200000000 69.476585923461954000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="UUID_2ac4d02d-a230-450c-aed5-ad2d7766087c">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_47791e07-08be-44a3-97d6-ccb5520c7b1d" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_be9b7dde-7fd5-4f00-990c-0afee872a63f">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_bde49c8f-dd77-454e-9d9e-8070d94c0ad1">
                      <gml:posList>550136.008999999610000000 5804605.576999999600000000 53.189999999999998000 550135.972403789870000000 5804604.222058336300000000 53.189999999999998000 550135.932000000030000000 5804602.710000000000000000 53.189999999999998000 550135.848773652690000000 5804599.595364139400000000 53.189999999999998000 550135.763831196700000000 5804596.416505187700000000 53.189999999999998000 550135.640201059520000000 5804591.789810990900000000 53.189999999999998000 550135.611000001430000000 5804590.696999999700000000 53.189999999999998000 550134.287000000480000000 5804590.831000000200000000 53.189999999999998000 550128.990161502730000000 5804591.368947664300000000 53.189999999999998000 550124.351999998090000000 5804591.839999999900000000 53.189999999999998000 550124.355245593000000000 5804591.941502302000000000 53.189999999999998000 550124.427745224680000000 5804594.208847469700000000 53.189999999999998000 550124.490528097260000000 5804596.172311828500000000 53.189999999999998000 550124.679164133270000000 5804602.071693593600000000 53.189999999999998000 550124.751663764940000000 5804604.339038761300000000 53.189999999999998000 550124.814446637520000000 5804606.302503120200000000 53.189999999999998000 550124.822999998930000000 5804606.570000000300000000 53.189999999999998000 550129.406767500450000000 5804606.163087012200000000 53.189999999999998000 550135.715999998150000000 5804605.603000000100000000 53.189999999999998000 550136.008999999610000000 5804605.576999999600000000 53.189999999999998000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </cityObjectMember>

  <cityObjectMember>
    <bldg:Building gml:id="DENIAL43000053IW">
      <gml:boundedBy>
        <gml:Envelope srsDimension="3" srsName="urn:adv:crs:ETRS89_UTM32*DE_DHHN92_NH">
          <gml:lowerCorner>550157.312500 5804594.000000 53.959995
</gml:lowerCorner>
          <gml:upperCorner>550172.375000 5804622.000000 71.605492
</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <creationDate>2017-11-21</creationDate>
      <gen:doubleAttribute name="Volume">
        <gen:value>2678.106</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Profile">
        <gen:value>Sig3d</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCK">
        <gen:value>10</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOECKE">
        <gen:value>111010</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STBEZNR">
        <gen:value>111</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="MBZNR">
        <gen:value>1102</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="WBZNR">
        <gen:value>1102</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNAM">
        <gen:value>Vahrenwald</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTNR">
        <gen:value>11</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNAM">
        <gen:value>Vahrenwald-List</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBEZ">
        <gen:value>02</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEMEINDE">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SCALE">
        <gen:value>5000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SHAPE_Area">
        <gen:value>7165.30049704715</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ALKISID">
        <gen:value>DENIAL43000053IW</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ZMIN">
        <gen:value>53.96</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="THICKNESS">
        <gen:value>0.1</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="TEXTURE">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="BUILDINGID">
        <gen:value>DENIAL43000053IW</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ARTBEZEICH">
        <gen:value>Gebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Join_Cou_1">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="FUNKTION_T">
        <gen:value>Wohngebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="OBJFLAECHE">
        <gen:value>197.26</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADT">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEB_Funkti">
        <gen:value>1000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STRSCHL">
        <gen:value>02557</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STR_komple">
        <gen:value>Seydlitzstraße 31</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="PLZ_NUMMER">
        <gen:value>30161</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STR">
        <gen:value>Seydlitzstraße</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="HAUSNR">
        <gen:value>31</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="OVERHANG">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <app:appearance>
        <app:Appearance>
          <app:theme>RhinoCity ObliqueTexturing</app:theme>
          <app:surfaceDataMember>
            <app:ParameterizedTexture>
              <app:imageURI>Hannover_teil1_Appearance/DENIAL43000053IW.jpg</app:imageURI>
              <app:textureType>specific</app:textureType>
              <app:wrapMode>border</app:wrapMode>
              <app:borderColor>0 0 0 1</app:borderColor>
              <app:target uri="#UUID_988bfd39-38da-47b5-bbe0-468830d7621b">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_72939569-be2d-41c1-a8ca-a51bc7c4ae64">0.258400 0.475978 0.221954 0.477529 0.215264 0.434574 0.251736 0.432063 0.258400 0.475978 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_1e853746-923b-4265-82cf-1d6304a018a9">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_bd88da82-08ff-4948-8ad1-3b44ab5d51af">0.671427 0.456090 0.592134 0.460987 0.587084 0.436202 0.666014 0.432063 0.671427 0.456090 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_5506487a-28ee-4817-a36d-5969906fd3d0">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_3320ae0b-1e95-4af5-837c-8394daef6d2a">0.344015 0.475933 0.292236 0.476411 0.287671 0.449778 0.335524 0.432063 0.344015 0.475933 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_fedbb998-1dca-4f4c-bfc4-a0156d6f3173">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_0dc05369-05df-438c-98cc-eb6bb62d3c92">0.428920 0.470859 0.378330 0.475799 0.369863 0.432063 0.424345 0.444239 0.428920 0.470859 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_ad653e9d-4a04-4bb6-b6df-2ed135c1ce42">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_baecc7f0-cc5d-4840-8218-7bd00555b777">0.207539 0.482893 0.158936 0.500041 0.103718 0.488249 0.207539 0.482893 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_c9cb2322-822b-422c-bb22-32e6d6aae037">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_ef388301-2f80-481d-8d3e-96bd25987030">0.106228 0.466878 0.106521 0.472985 0.026486 0.477181 0.025440 0.441466 0.210819 0.432063 0.210974 0.461107 0.106228 0.466878 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_e5c47d0d-b57f-43ef-a212-d8c2a1000ca8">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_9686f5de-eb21-4193-b370-1fc654bcc398">0.636723 0.392822 0.682906 0.393324 0.676624 0.344232 0.701272 0.343109 0.710091 0.410399 0.499220 0.419736 0.487280 0.354142 0.562613 0.350598 0.562481 0.349437 0.584336 0.348440 0.590621 0.397394 0.636723 0.392822 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_e04aaead-2ad5-4041-8601-c2f7189680eb">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_aefae927-b6a9-46ae-996f-b1e2dce3c59b">0.952216 0.401756 0.716416 0.414334 0.714286 0.355641 0.954256 0.343109 0.952216 0.401756 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_35cc7291-57b1-401c-9401-36fa64d2727a">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_912510f6-c789-4a5a-af07-49e549efad31">0.215554 0.225279 0.214823 0.221129 0.245044 0.219641 0.241040 0.199510 0.234119 0.154524 0.212712 0.036341 0.207436 0.007277 0.329658 0.000978 0.368042 0.194661 0.372437 0.216830 0.344582 0.218330 0.240264 0.223948 0.215554 0.225279 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_860f07a0-27f6-4d3f-9e9e-d496f7bca66b">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_6af94984-d63a-42a4-9fd7-b34be0ff65d4">0.734859 0.451503 0.680988 0.456129 0.677104 0.436873 0.689422 0.435773 0.730938 0.432063 0.734859 0.451503 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_72d78079-78e3-48e7-aaec-05436d15dea8">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_15bb1af1-61b6-4810-bf92-2c9525ce0498">0.482646 0.425459 0.456571 0.426574 0.424658 0.344213 0.450573 0.343109 0.482646 0.425459 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_249b439a-ad4e-43f7-9d5d-9eb1e897206f">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_903a949e-0f01-4f01-b310-1d8bc95db523">0.214195 0.491960 0.214015 0.491007 0.213307 0.487254 0.266778 0.482893 0.267492 0.486682 0.267913 0.487626 0.214195 0.491960 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_26633b9c-06ce-4f5c-9312-1c6ed1406a78">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_8c5cc461-9d72-4d7d-915d-a158dfc82305">0.419828 0.425880 0.345190 0.429428 0.313112 0.346591 0.387301 0.343109 0.419828 0.425880 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_d4f81c56-de61-4386-9269-ab6ed5fb5d5c">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_0dc8de9a-abe4-4be4-a75c-3f103a935a19">0.493011 0.000978 0.513877 0.042705 0.535117 0.085179 0.506608 0.170352 0.477971 0.149172 0.463796 0.086963 0.493011 0.000978 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_d2e72dbf-9bca-49a1-976a-f05ce7c5bbe4">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_25df6948-6d87-4ecf-a0d1-05c706bc161e">0.540117 0.010086 0.742846 0.000978 0.793701 0.086646 0.589759 0.095885 0.540117 0.010086 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_62ae8058-e370-4660-8c99-98a68c4775a2">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_e8568819-36db-4bbc-a8d3-e58e33cb10c1">0.148408 0.340356 0.110433 0.316427 0.097292 0.248604 0.087558 0.231768 0.001957 0.083684 0.055696 0.000978 0.140434 0.148659 0.150070 0.165450 0.176543 0.211582 0.201667 0.255363 0.148408 0.340356 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_43445577-485a-4638-8011-018feed0c65a">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_cb41cf79-757f-48f3-9c61-63fe387f5746">0.309142 0.425000 0.200069 0.429789 0.183506 0.388881 0.018077 0.396208 0.001957 0.355086 0.166894 0.347853 0.169185 0.347752 0.275293 0.343109 0.309142 0.425000 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_00b41e15-84b4-4433-b5b2-df715829b8da">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_c8bfccad-d442-4018-a650-00e0cf8129d3">0.913894 0.434976 0.981620 0.432063 0.991282 0.448156 0.923475 0.451072 0.913894 0.434976 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_df01929a-7955-48ff-9d4d-77b1179415fa">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_bf3acfd3-9c2a-4adf-9bfd-3879c1b02285">0.434442 0.436158 0.523563 0.432063 0.543439 0.466042 0.454139 0.470216 0.434442 0.436158 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_aa7bc210-accf-41fa-9aae-45b3ff1511c4">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_142fb3b5-4c3d-4db1-bd79-005dc5d462fa">0.411070 0.152302 0.398652 0.153539 0.378828 0.042903 0.377691 0.036557 0.399688 0.033899 0.399536 0.004384 0.426556 0.000978 0.431770 0.030023 0.452925 0.148132 0.459764 0.193090 0.417861 0.196930 0.411070 0.152302 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_2ab2abf8-da04-4228-8099-c9e208440752">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_a001b733-a6ff-4964-9c51-c76cc7e79774">0.099455 0.497849 0.008071 0.501899 0.001957 0.486952 0.093229 0.482893 0.099455 0.497849 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_fda0fd06-18f4-4a13-98c3-035516167b6a">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_633c4aa3-899d-46ad-ad1f-4f45d3127439">0.365670 0.475799 0.348337 0.432945 0.357804 0.432063 0.365670 0.475799 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_b990e9c0-61d0-41e5-86b3-cfcdf9e84f82">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_a01521a6-f4f1-4587-84c5-879b3b200bcc">0.019783 0.480712 0.001957 0.447481 0.011118 0.432063 0.019783 0.480712 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_79542440-bc1d-4a7f-ac4e-82b87c8e3488">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_c54c5754-cfb6-41be-92b3-a9652514df1e">0.979876 0.405499 0.970407 0.406647 0.965596 0.381469 0.958904 0.346440 0.983927 0.343109 0.984914 0.379031 0.975056 0.380275 0.979876 0.405499 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_4de46118-a4cb-4139-a93a-5d2441b9707e">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_4552420f-840d-4445-92b6-6848affacf3f">0.831703 0.435513 0.899898 0.432063 0.909193 0.447522 0.840918 0.450974 0.831703 0.435513 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_e8cda663-8d07-4699-bdd0-d3975760bf9f">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_d73cab93-3fc7-43f2-ab3e-ea724539772d">0.573153 0.470652 0.562055 0.452014 0.551709 0.468011 0.547945 0.465018 0.569274 0.432063 0.571997 0.436642 0.583075 0.455269 0.573153 0.470652 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_9c16018b-e91e-4b73-93a7-a0f0cc179d08">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_61be1794-477d-4d3e-a9aa-23fd9b1d0ff6">0.739726 0.433153 0.770993 0.432063 0.781903 0.450752 0.750592 0.451844 0.739726 0.433153 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_693ba83e-4946-4f4b-a563-c77ce7023f34">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_52c1e805-d606-406e-a57e-d8c11a8b37d0">0.826189 0.450274 0.793920 0.452133 0.786693 0.433920 0.818915 0.432063 0.826189 0.450274 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_5e1e1ac3-2e00-46de-9589-5fd214f9a4f3">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_3461473e-11cc-47d4-ada9-26bd3857b04e">0.270520 0.476115 0.264188 0.433274 0.275670 0.432063 0.282016 0.474998 0.270520 0.476115 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
            </app:ParameterizedTexture>
          </app:surfaceDataMember>
        </app:Appearance>
      </app:appearance>
      <bldg:measuredHeight uom="#m">17.645</bldg:measuredHeight>
      <bldg:lod1Solid>
        <gml:Solid gml:id="UUID_8ecbe699-b0c1-40cd-ac97-b491ea41848e">
          <gml:exterior>
            <gml:CompositeSurface gml:id="UUID_ab9156ad-76ab-483e-851b-e295cff72bff">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_d9c2ac13-2d0a-4983-b27e-4372559d0e76">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_7af01c56-62c6-4b24-b91f-cdb734d9d811">
                      <gml:posList>550165.048999998720000000 5804611.775999999600000000 53.960000000000008000 550165.041704687290000000 5804611.694535684800000000 53.960000000000001000 550165.012999999920000000 5804611.373999999800000000 53.960000000000008000 550165.012999999920000000 5804611.373999999800000000 71.605487647769834000 550165.041704687290000000 5804611.694535684800000000 71.605487647769834000 550165.048999998720000000 5804611.775999999600000000 71.605487647769834000 550165.048999998720000000 5804611.775999999600000000 53.960000000000008000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_63e4dee4-9e2f-4ac2-9891-3c307e870fd9">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_a0cb8001-dddd-4a35-96a8-70f4a69c833c">
                      <gml:posList>550165.048999998720000000 5804611.775999999600000000 71.605487647769834000 550161.736999999730000000 5804612.140999999800000000 71.605487647769834000 550161.736999999730000000 5804612.140999999800000000 53.960000000000008000 550165.048999998720000000 5804611.775999999600000000 53.960000000000008000 550165.048999998720000000 5804611.775999999600000000 71.605487647769834000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_ed3fb561-3559-41fb-b023-12dc8ae5a590">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_7de2e739-e7f3-49f4-867b-637696c894f3">
                      <gml:posList>550162.938999999310000000 5804621.234000000200000000 53.960000000000008000 550162.344391629220000000 5804616.735851987300000000 53.960000000000001000 550161.736999999730000000 5804612.140999999800000000 53.960000000000008000 550161.736999999730000000 5804612.140999999800000000 71.605487647769834000 550162.344391629220000000 5804616.735851987300000000 71.605487647769834000 550162.938999999310000000 5804621.234000000200000000 71.605487647769834000 550162.938999999310000000 5804621.234000000200000000 53.960000000000008000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_92478fbe-93a7-4b2d-aac0-18691ec159be">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_37ac568e-9915-4206-9c81-26bc214fc108">
                      <gml:posList>550172.067999999970000000 5804620.246000000300000000 53.960000000000008000 550162.938999999310000000 5804621.234000000200000000 53.960000000000008000 550162.938999999310000000 5804621.234000000200000000 71.605487647769834000 550172.067999999970000000 5804620.246000000300000000 71.605487647769834000 550172.067999999970000000 5804620.246000000300000000 53.960000000000008000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_78bb8def-b729-4c85-acb0-8123f9c3a23e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_c0687b13-6be5-4901-887b-535a552e7a6b">
                      <gml:posList>550172.067999999970000000 5804620.246000000300000000 71.605487647769834000 550171.615920838320000000 5804615.753180522500000000 71.605487647769834000 550171.141307272130000000 5804611.036411302200000000 71.605487647769834000 550170.969000000160000000 5804609.324000000000000000 71.605487647769834000 550169.464000000150000000 5804594.362999999000000000 71.605487647769834000 550169.464000000150000000 5804594.362999999000000000 53.960000000000008000 550170.969000000160000000 5804609.324000000000000000 53.960000000000001000 550171.141307272130000000 5804611.036411302200000000 53.960000000000001000 550171.615920838320000000 5804615.753180522500000000 53.960000000000001000 550172.067999999970000000 5804620.246000000300000000 53.960000000000008000 550172.067999999970000000 5804620.246000000300000000 71.605487647769834000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_367e19b2-4e3b-4d2b-b1ce-1fa2e7a673c5">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_9cf11c45-b6a0-408b-80c7-b8268a3daf14">
                      <gml:posList>550169.464000000150000000 5804594.362999999000000000 71.605487647769834000 550164.812478820090000000 5804594.843618034400000000 71.605487647769834000 550164.712001085980000000 5804594.854010487900000000 71.605487647769834000 550157.478000000120000000 5804595.603000000100000000 71.605487647769834000 550157.478000000120000000 5804595.603000000100000000 53.960000000000008000 550164.712001085980000000 5804594.854010487900000000 53.960000000000001000 550164.812478820090000000 5804594.843618034400000000 53.960000000000001000 550169.464000000150000000 5804594.362999999000000000 53.960000000000008000 550169.464000000150000000 5804594.362999999000000000 71.605487647769834000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_febb402e-62b7-4be7-86b7-ae82d7de1e4f">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_acbe6873-4ab4-4839-9fcd-97fc706234cb">
                      <gml:posList>550157.959999999960000000 5804600.349999998700000000 53.960000000000008000 550157.758397841940000000 5804598.364869072100000000 53.960000000000001000 550157.478000000120000000 5804595.603000000100000000 53.960000000000008000 550157.478000000120000000 5804595.603000000100000000 71.605487647769834000 550157.758397841940000000 5804598.364869072100000000 71.605487647769834000 550157.959999999960000000 5804600.349999998700000000 71.605487647769834000 550157.959999999960000000 5804600.349999998700000000 53.960000000000008000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_12a911df-96cd-4302-8703-01d045cec44c">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_3d020673-08a1-447d-865c-c85fb80e838c">
                      <gml:posList>550161.099999999980000000 5804599.954999999100000000 53.960000000000008000 550157.959999999960000000 5804600.349999998700000000 53.960000000000008000 550157.959999999960000000 5804600.349999998700000000 71.605487647769834000 550161.099999999980000000 5804599.954999999100000000 71.605487647769834000 550161.099999999980000000 5804599.954999999100000000 53.960000000000008000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_f42cb5ab-f8d2-4a8c-a399-3f599efe4fe6">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_fd04de04-7a50-4010-95cb-3ec9567052be">
                      <gml:posList>550161.099999999980000000 5804599.954999999100000000 71.605487647769834000 550160.884037918530000000 5804598.030609258500000000 71.605487647769834000 550160.831000000240000000 5804597.558000000200000000 71.605487647769834000 550160.831000000240000000 5804597.558000000200000000 53.960000000000008000 550160.884037918530000000 5804598.030609258500000000 53.960000000000001000 550161.099999999980000000 5804599.954999999100000000 53.960000000000008000 550161.099999999980000000 5804599.954999999100000000 71.605487647769834000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_6601193e-d15d-4e9d-bbfb-52f91fe4ee4d">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_7924e54b-86a8-4739-8043-79934b0ed96d">
                      <gml:posList>550164.918999999760000000 5804597.099000000400000000 53.960000000000008000 550160.831000000240000000 5804597.558000000200000000 53.960000000000008000 550160.831000000240000000 5804597.558000000200000000 71.605487647769834000 550164.918999999760000000 5804597.099000000400000000 71.605487647769834000 550164.918999999760000000 5804597.099000000400000000 53.960000000000008000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_31d75ad3-66d7-46c1-a490-45fb93753eb9">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_686f8b16-1dec-46f7-8f38-9051b8327580">
                      <gml:posList>550165.758999999960000000 5804606.227999999200000000 53.960000000000008000 550164.964568529390000000 5804597.594232270500000000 53.960000000000001000 550164.918999999760000000 5804597.099000000400000000 53.960000000000008000 550164.918999999760000000 5804597.099000000400000000 71.605487647769834000 550164.964568529390000000 5804597.594232270500000000 71.605487647769834000 550165.758999999960000000 5804606.227999999200000000 71.605487647769834000 550165.758999999960000000 5804606.227999999200000000 53.960000000000008000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_27cf0c55-68e9-48a4-8d53-e82ec16b5918">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_c2ae1a91-2203-4713-a3fb-c1dc01a64340">
                      <gml:posList>550165.758999999960000000 5804606.227999999200000000 71.605487647769834000 550164.285999999960000000 5804606.427999999400000000 71.605487647769834000 550164.285999999960000000 5804606.427999999400000000 53.960000000000008000 550165.758999999960000000 5804606.227999999200000000 53.960000000000008000 550165.758999999960000000 5804606.227999999200000000 71.605487647769834000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_75fb6f53-e752-4d45-bd87-cd9986aeef8b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_54e19fd3-5c97-48a6-bd95-810ba4d97852">
                      <gml:posList>550164.559000000010000000 5804609.826999999600000000 53.960000000000008000 550164.285999999960000000 5804606.427999999400000000 53.960000000000008000 550164.285999999960000000 5804606.427999999400000000 71.605487647769834000 550164.559000000010000000 5804609.826999999600000000 71.605487647769834000 550164.559000000010000000 5804609.826999999600000000 53.960000000000008000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_ba34de36-a771-4c4f-8454-7db5f03f8a9c">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_f7e275fa-0a19-465a-8616-0a5fdfedce4f">
                      <gml:posList>550166.030999999960000000 5804609.702999998800000000 53.960000000000008000 550164.559000000010000000 5804609.826999999600000000 53.960000000000008000 550164.559000000010000000 5804609.826999999600000000 71.605487647769834000 550166.030999999960000000 5804609.702999998800000000 71.605487647769834000 550166.030999999960000000 5804609.702999998800000000 53.960000000000008000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_b5eaa322-0b12-426b-bd84-44c1a59bcabc">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_f871df48-170d-414c-b0c0-188d26d0a89e">
                      <gml:posList>550166.187999999970000000 5804611.258000000400000000 53.960000000000008000 550166.030999999960000000 5804609.702999998800000000 53.960000000000008000 550166.030999999960000000 5804609.702999998800000000 71.605487647769834000 550166.187999999970000000 5804611.258000000400000000 71.605487647769834000 550166.187999999970000000 5804611.258000000400000000 53.960000000000008000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_a0cafb82-6916-4213-aa97-95f39224705e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_c2a6a460-5165-428f-89ee-37e95687a062">
                      <gml:posList>550166.187999999970000000 5804611.258000000400000000 71.605487647769834000 550165.012999999920000000 5804611.373999999800000000 71.605487647769834000 550165.012999999920000000 5804611.373999999800000000 53.960000000000008000 550166.187999999970000000 5804611.258000000400000000 53.960000000000008000 550166.187999999970000000 5804611.258000000400000000 71.605487647769834000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_e4f9e0b2-279d-42b6-970b-7b2aa217bc2a">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_2b48ecb7-c452-43c0-9589-f6f4c362249f">
                      <gml:posList>550165.041704687290000000 5804611.694535684800000000 53.960000000000001000 550165.048999998720000000 5804611.775999999600000000 53.960000000000008000 550161.736999999730000000 5804612.140999999800000000 53.960000000000008000 550162.344391629220000000 5804616.735851987300000000 53.960000000000001000 550162.938999999310000000 5804621.234000000200000000 53.960000000000008000 550172.067999999970000000 5804620.246000000300000000 53.960000000000008000 550171.615920838320000000 5804615.753180522500000000 53.960000000000001000 550171.141307272130000000 5804611.036411302200000000 53.960000000000001000 550170.969000000160000000 5804609.324000000000000000 53.960000000000001000 550169.464000000150000000 5804594.362999999000000000 53.960000000000008000 550164.812478820090000000 5804594.843618034400000000 53.960000000000001000 550164.712001085980000000 5804594.854010487900000000 53.960000000000001000 550157.478000000120000000 5804595.603000000100000000 53.960000000000008000 550157.758397841940000000 5804598.364869072100000000 53.960000000000001000 550157.959999999960000000 5804600.349999998700000000 53.960000000000008000 550161.099999999980000000 5804599.954999999100000000 53.960000000000008000 550160.884037918530000000 5804598.030609258500000000 53.960000000000001000 550160.831000000240000000 5804597.558000000200000000 53.960000000000008000 550164.918999999760000000 5804597.099000000400000000 53.960000000000008000 550164.964568529390000000 5804597.594232270500000000 53.960000000000001000 550165.758999999960000000 5804606.227999999200000000 53.960000000000008000 550164.285999999960000000 5804606.427999999400000000 53.960000000000008000 550164.559000000010000000 5804609.826999999600000000 53.960000000000008000 550166.030999999960000000 5804609.702999998800000000 53.960000000000008000 550166.187999999970000000 5804611.258000000400000000 53.960000000000008000 550165.012999999920000000 5804611.373999999800000000 53.960000000000008000 550165.041704687290000000 5804611.694535684800000000 53.960000000000001000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_28409288-2092-4849-9a45-1b7bbce52610">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_c6611f55-e364-4ccd-99f2-5b47e3b7d9ba">
                      <gml:posList>550165.012999999920000000 5804611.373999999800000000 71.605487647769834000 550166.187999999970000000 5804611.258000000400000000 71.605487647769834000 550166.030999999960000000 5804609.702999998800000000 71.605487647769834000 550164.559000000010000000 5804609.826999999600000000 71.605487647769834000 550164.285999999960000000 5804606.427999999400000000 71.605487647769834000 550165.758999999960000000 5804606.227999999200000000 71.605487647769834000 550164.964568529390000000 5804597.594232270500000000 71.605487647769834000 550164.918999999760000000 5804597.099000000400000000 71.605487647769834000 550160.831000000240000000 5804597.558000000200000000 71.605487647769834000 550160.884037918530000000 5804598.030609258500000000 71.605487647769834000 550161.099999999980000000 5804599.954999999100000000 71.605487647769834000 550157.959999999960000000 5804600.349999998700000000 71.605487647769834000 550157.758397841940000000 5804598.364869072100000000 71.605487647769834000 550157.478000000120000000 5804595.603000000100000000 71.605487647769834000 550164.712001085980000000 5804594.854010487900000000 71.605487647769834000 550164.812478820090000000 5804594.843618034400000000 71.605487647769834000 550169.464000000150000000 5804594.362999999000000000 71.605487647769834000 550170.969000000160000000 5804609.324000000000000000 71.605487647769834000 550171.141307272130000000 5804611.036411302200000000 71.605487647769834000 550171.615920838320000000 5804615.753180522500000000 71.605487647769834000 550172.067999999970000000 5804620.246000000300000000 71.605487647769834000 550162.938999999310000000 5804621.234000000200000000 71.605487647769834000 550162.344391629220000000 5804616.735851987300000000 71.605487647769834000 550161.736999999730000000 5804612.140999999800000000 71.605487647769834000 550165.048999998720000000 5804611.775999999600000000 71.605487647769834000 550165.041704687290000000 5804611.694535684800000000 71.605487647769834000 550165.012999999920000000 5804611.373999999800000000 71.605487647769834000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod1Solid>
      <bldg:lod2Solid>
        <gml:Solid gml:id="UUID_33dd74e3-7af8-49cd-abed-171c596a34e6">
          <gml:exterior>
            <gml:CompositeSurface gml:id="UUID_13399fff-e496-4a04-84f3-89657fb3e18e">
              <gml:surfaceMember xlink:href="#UUID_988bfd39-38da-47b5-bbe0-468830d7621b"/>
              <gml:surfaceMember xlink:href="#UUID_1e853746-923b-4265-82cf-1d6304a018a9"/>
              <gml:surfaceMember xlink:href="#UUID_5506487a-28ee-4817-a36d-5969906fd3d0"/>
              <gml:surfaceMember xlink:href="#UUID_fedbb998-1dca-4f4c-bfc4-a0156d6f3173"/>
              <gml:surfaceMember xlink:href="#UUID_ad653e9d-4a04-4bb6-b6df-2ed135c1ce42"/>
              <gml:surfaceMember xlink:href="#UUID_c9cb2322-822b-422c-bb22-32e6d6aae037"/>
              <gml:surfaceMember xlink:href="#UUID_e5c47d0d-b57f-43ef-a212-d8c2a1000ca8"/>
              <gml:surfaceMember xlink:href="#UUID_e04aaead-2ad5-4041-8601-c2f7189680eb"/>
              <gml:surfaceMember xlink:href="#UUID_35cc7291-57b1-401c-9401-36fa64d2727a"/>
              <gml:surfaceMember xlink:href="#UUID_860f07a0-27f6-4d3f-9e9e-d496f7bca66b"/>
              <gml:surfaceMember xlink:href="#UUID_72d78079-78e3-48e7-aaec-05436d15dea8"/>
              <gml:surfaceMember xlink:href="#UUID_249b439a-ad4e-43f7-9d5d-9eb1e897206f"/>
              <gml:surfaceMember xlink:href="#UUID_26633b9c-06ce-4f5c-9312-1c6ed1406a78"/>
              <gml:surfaceMember xlink:href="#UUID_d4f81c56-de61-4386-9269-ab6ed5fb5d5c"/>
              <gml:surfaceMember xlink:href="#UUID_d2e72dbf-9bca-49a1-976a-f05ce7c5bbe4"/>
              <gml:surfaceMember xlink:href="#UUID_62ae8058-e370-4660-8c99-98a68c4775a2"/>
              <gml:surfaceMember xlink:href="#UUID_43445577-485a-4638-8011-018feed0c65a"/>
              <gml:surfaceMember xlink:href="#UUID_00b41e15-84b4-4433-b5b2-df715829b8da"/>
              <gml:surfaceMember xlink:href="#UUID_df01929a-7955-48ff-9d4d-77b1179415fa"/>
              <gml:surfaceMember xlink:href="#UUID_aa7bc210-accf-41fa-9aae-45b3ff1511c4"/>
              <gml:surfaceMember xlink:href="#UUID_2ab2abf8-da04-4228-8099-c9e208440752"/>
              <gml:surfaceMember xlink:href="#UUID_fda0fd06-18f4-4a13-98c3-035516167b6a"/>
              <gml:surfaceMember xlink:href="#UUID_b990e9c0-61d0-41e5-86b3-cfcdf9e84f82"/>
              <gml:surfaceMember xlink:href="#UUID_79542440-bc1d-4a7f-ac4e-82b87c8e3488"/>
              <gml:surfaceMember xlink:href="#UUID_4de46118-a4cb-4139-a93a-5d2441b9707e"/>
              <gml:surfaceMember xlink:href="#UUID_e8cda663-8d07-4699-bdd0-d3975760bf9f"/>
              <gml:surfaceMember xlink:href="#UUID_9c16018b-e91e-4b73-93a7-a0f0cc179d08"/>
              <gml:surfaceMember xlink:href="#UUID_693ba83e-4946-4f4b-a563-c77ce7023f34"/>
              <gml:surfaceMember xlink:href="#UUID_5e1e1ac3-2e00-46de-9589-5fd214f9a4f3"/>
              <gml:surfaceMember xlink:href="#UUID_33ead521-dab8-4c20-abf6-941f81f5707b"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_46c64775-1bbe-4115-a222-d06ce8dd98df">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_d0ab8cb8-9a2e-443b-8fd2-a50589acfff8" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_988bfd39-38da-47b5-bbe0-468830d7621b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_72939569-be2d-41c1-a8ca-a51bc7c4ae64">
                      <gml:posList>550166.030999999960000000 5804609.702999998800000000 57.190914154052734000 550164.559000000010000000 5804609.826999999600000000 57.190914154052734000 550164.285999999960000000 5804606.427999999400000000 57.190914154052734000 550165.758999999960000000 5804606.227999999200000000 57.190914154052734000 550166.030999999960000000 5804609.702999998800000000 57.190914154052734000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_6f4efe01-84da-4c9e-954c-8d9aa726c38c">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_3393b2af-269c-4958-9073-cf4c3a2c1c3b" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_1e853746-923b-4265-82cf-1d6304a018a9">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_bd88da82-08ff-4948-8ad1-3b44ab5d51af">
                      <gml:posList>550161.099999999980000000 5804599.954999999100000000 56.632343292236328000 550157.959999999960000000 5804600.349999998700000000 56.632343292236328000 550157.758397841940000000 5804598.364869072100000000 56.632343292236328000 550160.884037918530000000 5804598.030609258500000000 56.632343292236328000 550161.099999999980000000 5804599.954999999100000000 56.632343292236328000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_31b3e52f-855a-464c-afa4-72a2db72f49d">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_6fccfae9-17ab-423e-83c0-5ae2103f66df" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_5506487a-28ee-4817-a36d-5969906fd3d0">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_3320ae0b-1e95-4af5-837c-8394daef6d2a">
                      <gml:posList>550170.397642341560000000 5804614.594531444800000000 70.596805865892037000 550168.364406546000000000 5804614.630464203700000000 70.460637512349336000 550168.181543282700000000 5804612.541155874700000000 70.460659668828356000 550170.058321611490000000 5804611.153261418500000000 70.594292746564776000 550170.397642341560000000 5804614.594531444800000000 70.596805865892037000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_3bfd63fd-74cc-43af-ae13-6065063b41bd">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_f1b0b423-79b3-4c9b-89ce-ba3ac931ddcd" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_fedbb998-1dca-4f4c-bfc4-a0156d6f3173">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_0dc05369-05df-438c-98cc-eb6bb62d3c92">
                      <gml:posList>550168.364406546000000000 5804614.630464203700000000 70.460637512349336000 550166.340856196820000000 5804615.023130321900000000 70.594558870143629000 550166.002424601810000000 5804611.590877587900000000 70.597070147873310000 550168.181543282700000000 5804612.541155874700000000 70.460659668828356000 550168.364406546000000000 5804614.630464203700000000 70.460637512349336000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_64fc47d2-62e6-4eac-8a48-4087f14b47cf">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_db43a208-96fc-47d1-874f-8b1410c89dc9" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_ad653e9d-4a04-4bb6-b6df-2ed135c1ce42">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_baecc7f0-cc5d-4840-8218-7bd00555b777">
                      <gml:posList>550170.058321611490000000 5804611.153261418500000000 70.594292746564776000 550168.181543282700000000 5804612.541155874700000000 70.460659668828356000 550166.002424601810000000 5804611.590877587900000000 70.597070147873310000 550170.058321611490000000 5804611.153261418500000000 70.594292746564776000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_36ecc26d-a84a-4272-b228-eb66bb08540b">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_08862e16-0fb6-4793-82d2-d61a2b3452d3" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_c9cb2322-822b-422c-bb22-32e6d6aae037">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_ef388301-2f80-481d-8d3e-96bd25987030">
                      <gml:posList>550160.831000000240000000 5804597.558000000200000000 59.682206513876260000 550160.884037918880000000 5804598.030609258500000000 59.406429290771484000 550157.758397842990000000 5804598.364869071200000000 59.406429290771484000 550157.478000000120000000 5804595.603000000100000000 61.016214634423527000 550164.712001085980000000 5804594.854010487900000000 61.002010592160829000 550164.918999999760000000 5804597.099000000400000000 59.694790376177821000 550160.831000000240000000 5804597.558000000200000000 59.682206513876260000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_ed490de1-9608-40a2-868c-5261e5659fb3">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_4d45ec1c-61cf-485f-af28-a23ae0e75b7a" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_e5c47d0d-b57f-43ef-a212-d8c2a1000ca8">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_9686f5de-eb21-4193-b370-1fc654bcc398">
                      <gml:posList>550168.364406546000000000 5804614.630464203700000000 70.460637512349336000 550170.397642341560000000 5804614.594531444800000000 70.596805865892037000 550170.058321611490000000 5804611.153261418500000000 67.971606907284098000 550171.141307272130000000 5804611.036411302200000000 67.970370922798566000 550171.615920838320000000 5804615.753180522500000000 71.569358648234484000 550162.344391629220000000 5804616.735851987300000000 71.566590562361938000 550161.736999999730000000 5804612.140999999800000000 68.048977175494869000 550165.048999998720000000 5804611.775999999600000000 68.039421375683659000 550165.041704687290000000 5804611.694535684800000000 67.977332246883321000 550166.002424601810000000 5804611.590877587900000000 67.976235801240122000 550166.340856196820000000 5804615.023130321900000000 70.594558870143629000 550168.364406546000000000 5804614.630464203700000000 70.460637512349336000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_e9402bf0-8a16-4811-bfec-7ef116bd6051">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_fb0707e9-4a30-4be9-b57a-8beb683beb18" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_e04aaead-2ad5-4041-8601-c2f7189680eb">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_aefae927-b6a9-46ae-996f-b1e2dce3c59b">
                      <gml:posList>550172.067999999970000000 5804620.246000000300000000 68.235336050959575000 550162.938999999310000000 5804621.234000000200000000 68.217606971880798000 550162.344391629220000000 5804616.735851987300000000 71.566590562361938000 550171.615920838320000000 5804615.753180522500000000 71.569358648234484000 550172.067999999970000000 5804620.246000000300000000 68.235336050959575000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_3e9d8cab-8b61-4557-84d9-a43a6e511a8e">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_48f9cca3-6e30-4400-b7de-30159144bc28" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_35cc7291-57b1-401c-9401-36fa64d2727a">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_912510f6-c789-4a5a-af07-49e549efad31">
                      <gml:posList>550165.041704687290000000 5804611.694535684800000000 67.977332246883321000 550165.012999999920000000 5804611.373999999800000000 67.977399372310160000 550166.187999999970000000 5804611.258000000400000000 67.976057229691719000 550166.030999999960000000 5804609.702999998800000000 67.976403328881517000 550165.758999999960000000 5804606.227999999200000000 67.977085870638376000 550164.919000000110000000 5804597.099000000400000000 67.979023543580084000 550164.712000000060000000 5804594.853999999400000000 67.979500548166570000 550169.464000000150000000 5804594.362999999000000000 67.974074902661258000 550170.969000000160000000 5804609.324000000000000000 67.970751382292093000 550171.141307272130000000 5804611.036411302200000000 67.970370922798566000 550170.058321611490000000 5804611.153261418500000000 67.971606907284098000 550166.002424601810000000 5804611.590877587900000000 67.976235801240122000 550165.041704687290000000 5804611.694535684800000000 67.977332246883321000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_c293f8e5-1ce1-4d7d-8d36-258c034e70f2">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_967a345a-462c-4dd8-ba41-b4a73496a461" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_860f07a0-27f6-4d3f-9e9e-d496f7bca66b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_6af94984-d63a-42a4-9fd7-b34be0ff65d4">
                      <gml:posList>550166.187999999970000000 5804611.258000000400000000 67.976057229691719000 550166.187999999970000000 5804611.258000000400000000 53.960000000000008000 550166.030999999960000000 5804609.702999998800000000 53.960000000000008000 550166.030999999960000000 5804609.702999998800000000 57.190914154052734000 550166.030999999960000000 5804609.702999998800000000 67.976403328881517000 550166.187999999970000000 5804611.258000000400000000 67.976057229691719000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_c24624e0-878c-486d-9078-ed0a3f982450">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_e52eb331-ee7e-4579-88f1-ff7e93f8e003" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_72d78079-78e3-48e7-aaec-05436d15dea8">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_15bb1af1-61b6-4810-bf92-2c9525ce0498">
                      <gml:posList>550166.187999999970000000 5804611.258000000400000000 67.976057229691719000 550165.012999999920000000 5804611.373999999800000000 67.977399372310160000 550165.012999999920000000 5804611.373999999800000000 53.960000000000008000 550166.187999999970000000 5804611.258000000400000000 53.960000000000008000 550166.187999999970000000 5804611.258000000400000000 67.976057229691719000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_c0de6611-5ca3-49da-9680-65669c5e6df9">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_893c493c-2703-4d86-b377-5c57bcc24100" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_249b439a-ad4e-43f7-9d5d-9eb1e897206f">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_903a949e-0f01-4f01-b310-1d8bc95db523">
                      <gml:posList>550165.048999998720000000 5804611.775999999600000000 53.960000000000008000 550165.041704687290000000 5804611.694535684800000000 53.960000000000001000 550165.012999999920000000 5804611.373999999800000000 53.960000000000008000 550165.012999999920000000 5804611.373999999800000000 67.977399372310160000 550165.041704687290000000 5804611.694535684800000000 67.977332246883321000 550165.048999998720000000 5804611.775999999600000000 68.039421375683659000 550165.048999998720000000 5804611.775999999600000000 53.960000000000008000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_3fdc89e8-2d99-4564-9904-77d4eb569006">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_af9f1210-75b5-47b8-bbcb-15637600119f" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_26633b9c-06ce-4f5c-9312-1c6ed1406a78">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_8c5cc461-9d72-4d7d-915d-a158dfc82305">
                      <gml:posList>550165.048999998720000000 5804611.775999999600000000 68.039421375683659000 550161.736999999730000000 5804612.140999999800000000 68.048977175494869000 550161.736999999730000000 5804612.140999999800000000 53.960000000000008000 550165.048999998720000000 5804611.775999999600000000 53.960000000000008000 550165.048999998720000000 5804611.775999999600000000 68.039421375683659000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_2a849ed7-674c-4281-b809-7a23b4df8aa6">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_b91e0a37-e774-4df8-915f-227fa27486f7" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_d4f81c56-de61-4386-9269-ab6ed5fb5d5c">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_0dc8de9a-abe4-4be4-a75c-3f103a935a19">
                      <gml:posList>550162.938999999310000000 5804621.234000000200000000 53.960000000000008000 550162.344391629220000000 5804616.735851987300000000 53.960000000000001000 550161.736999999730000000 5804612.140999999800000000 53.960000000000008000 550161.736999999730000000 5804612.140999999800000000 68.048977175494869000 550162.344391629220000000 5804616.735851987300000000 71.566590562361938000 550162.938999999310000000 5804621.234000000200000000 68.217606971880798000 550162.938999999310000000 5804621.234000000200000000 53.960000000000008000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_a3fa2e23-7a34-404b-80b2-6fe8615bb944">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_0a0e81de-18b3-4bb5-b428-be32062731f7" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_d2e72dbf-9bca-49a1-976a-f05ce7c5bbe4">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_25df6948-6d87-4ecf-a0d1-05c706bc161e">
                      <gml:posList>550172.067999999970000000 5804620.246000000300000000 53.960000000000008000 550162.938999999310000000 5804621.234000000200000000 53.960000000000008000 550162.938999999310000000 5804621.234000000200000000 68.217606971880798000 550172.067999999970000000 5804620.246000000300000000 68.235336050959575000 550172.067999999970000000 5804620.246000000300000000 53.960000000000008000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_053e6e84-ac4e-4c73-b268-8f258f84123d">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_e5a2ba18-2565-41e0-81a9-3518bdfb7379" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_62ae8058-e370-4660-8c99-98a68c4775a2">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_e8568819-36db-4bbc-a8d3-e58e33cb10c1">
                      <gml:posList>550172.067999999970000000 5804620.246000000300000000 68.235336050959575000 550171.615920838320000000 5804615.753180522500000000 71.569358648234484000 550171.141307272130000000 5804611.036411302200000000 67.970370922798566000 550170.969000000160000000 5804609.324000000000000000 67.970751382292093000 550169.464000000150000000 5804594.362999999000000000 67.974074902661258000 550169.464000000150000000 5804594.362999999000000000 53.960000000000008000 550170.969000000160000000 5804609.324000000000000000 53.960000000000001000 550171.141307272130000000 5804611.036411302200000000 53.960000000000001000 550171.615920838320000000 5804615.753180522500000000 53.960000000000001000 550172.067999999970000000 5804620.246000000300000000 53.960000000000008000 550172.067999999970000000 5804620.246000000300000000 68.235336050959575000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_edd22521-b3e5-4aa2-b6f0-1ff5c6f688d3">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_243b1bd9-9300-4baa-a749-34c8015dfd0c" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_43445577-485a-4638-8011-018feed0c65a">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_cb41cf79-757f-48f3-9c61-63fe387f5746">
                      <gml:posList>550169.464000000150000000 5804594.362999999000000000 67.974074902661258000 550164.712000000060000000 5804594.853999999400000000 67.979500548166570000 550164.712001085980000000 5804594.854010487900000000 61.002010592160829000 550157.478000000120000000 5804595.603000000100000000 61.016214634423527000 550157.478000000120000000 5804595.603000000100000000 53.960000000000008000 550164.712001085980000000 5804594.854010487900000000 53.960000000000001000 550164.812478820090000000 5804594.843618034400000000 53.960000000000001000 550169.464000000150000000 5804594.362999999000000000 53.960000000000008000 550169.464000000150000000 5804594.362999999000000000 67.974074902661258000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_1f5dc861-6057-4f34-8c10-d088d2512c72">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_bb87996c-44f0-447c-8ab3-e652ec10ffd7" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_00b41e15-84b4-4433-b5b2-df715829b8da">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_c8bfccad-d442-4018-a650-00e0cf8129d3">
                      <gml:posList>550160.884037918530000000 5804598.030609258500000000 56.632343292236328000 550157.758397841940000000 5804598.364869072100000000 56.632343292236328000 550157.758397842990000000 5804598.364869071200000000 59.406429290771484000 550160.884037918880000000 5804598.030609258500000000 59.406429290771484000 550160.884037918530000000 5804598.030609258500000000 56.632343292236328000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_0865ca49-6fd1-483e-a5f1-78b380cd5e2b">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_28bee87b-7c6a-4706-9f6a-a788256e66d5" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_df01929a-7955-48ff-9d4d-77b1179415fa">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_bf3acfd3-9c2a-4adf-9bfd-3879c1b02285">
                      <gml:posList>550164.918999999760000000 5804597.099000000400000000 53.960000000000008000 550160.831000000240000000 5804597.558000000200000000 53.960000000000008000 550160.831000000240000000 5804597.558000000200000000 59.682206513876260000 550164.918999999760000000 5804597.099000000400000000 59.694790376177821000 550164.918999999760000000 5804597.099000000400000000 53.960000000000008000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_943e66ab-0275-4174-8bb4-56a2c7cb84f1">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_8fcdf8db-ac86-448a-92c1-3a262c8a0009" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_aa7bc210-accf-41fa-9aae-45b3ff1511c4">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_142fb3b5-4c3d-4db1-bd79-005dc5d462fa">
                      <gml:posList>550165.758999999960000000 5804606.227999999200000000 57.190914154052734000 550165.758999999960000000 5804606.227999999200000000 53.960000000000008000 550164.964568529390000000 5804597.594232270500000000 53.960000000000001000 550164.918999999760000000 5804597.099000000400000000 53.960000000000008000 550164.918999999760000000 5804597.099000000400000000 59.694790376177821000 550164.712001085980000000 5804594.854010487900000000 61.002010592160829000 550164.712000000060000000 5804594.853999999400000000 67.979500548166570000 550164.919000000110000000 5804597.099000000400000000 67.979023543580084000 550165.758999999960000000 5804606.227999999200000000 67.977085870638376000 550166.030999999960000000 5804609.702999998800000000 67.976403328881517000 550166.030999999960000000 5804609.702999998800000000 57.190914154052734000 550165.758999999960000000 5804606.227999999200000000 57.190914154052734000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_e88a9819-3032-4ae7-9fc6-90f46d8272a8">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_65755496-167b-469c-b712-a583cdac74a1" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_2ab2abf8-da04-4228-8099-c9e208440752">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_a001b733-a6ff-4964-9c51-c76cc7e79774">
                      <gml:posList>550170.058321611490000000 5804611.153261418500000000 70.594292746564776000 550166.002424601810000000 5804611.590877587900000000 70.597070147873310000 550166.002424601810000000 5804611.590877587900000000 67.976235801240122000 550170.058321611490000000 5804611.153261418500000000 67.971606907284098000 550170.058321611490000000 5804611.153261418500000000 70.594292746564776000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_b4d8781c-3ed5-4883-b6e8-953abdd21aee">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_bf406cc3-b6b7-42ce-9883-53baf706cd25" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_fda0fd06-18f4-4a13-98c3-035516167b6a">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_633c4aa3-899d-46ad-ad1f-4f45d3127439">
                      <gml:posList>550166.340856196820000000 5804615.023130321900000000 70.594558870143629000 550166.002424601810000000 5804611.590877587900000000 67.976235801240122000 550166.002424601810000000 5804611.590877587900000000 70.597070147873310000 550166.340856196820000000 5804615.023130321900000000 70.594558870143629000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_76a742d5-2630-4b2b-957c-84ce77f45808">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_40ac7a5c-f818-42cf-b596-a181dbe444d6" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_b990e9c0-61d0-41e5-86b3-cfcdf9e84f82">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_a01521a6-f4f1-4587-84c5-879b3b200bcc">
                      <gml:posList>550170.397642341560000000 5804614.594531444800000000 70.596805865892037000 550170.058321611490000000 5804611.153261418500000000 70.594292746564776000 550170.058321611490000000 5804611.153261418500000000 67.971606907284098000 550170.397642341560000000 5804614.594531444800000000 70.596805865892037000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_1a32b08c-cd1c-48a6-990b-01edac42303a">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_c1bfd190-71eb-4ff3-8b7c-63cddf0753ad" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_79542440-bc1d-4a7f-ac4e-82b87c8e3488">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_c54c5754-cfb6-41be-92b3-a9652514df1e">
                      <gml:posList>550157.959999999960000000 5804600.349999998700000000 56.632343292236328000 550157.959999999960000000 5804600.349999998700000000 53.960000000000008000 550157.758397841940000000 5804598.364869072100000000 53.960000000000001000 550157.478000000120000000 5804595.603000000100000000 53.960000000000008000 550157.478000000120000000 5804595.603000000100000000 61.016214634423527000 550157.758397842990000000 5804598.364869071200000000 59.406429290771484000 550157.758397841940000000 5804598.364869072100000000 56.632343292236328000 550157.959999999960000000 5804600.349999998700000000 56.632343292236328000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_d27001d2-722d-4f24-9f98-8b1c276094cb">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_a2002b3b-0d8f-42f1-b75c-4d2ecf87eca5" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_4de46118-a4cb-4139-a93a-5d2441b9707e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_4552420f-840d-4445-92b6-6848affacf3f">
                      <gml:posList>550161.099999999980000000 5804599.954999999100000000 53.960000000000008000 550157.959999999960000000 5804600.349999998700000000 53.960000000000008000 550157.959999999960000000 5804600.349999998700000000 56.632343292236328000 550161.099999999980000000 5804599.954999999100000000 56.632343292236328000 550161.099999999980000000 5804599.954999999100000000 53.960000000000008000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_59852205-ea2a-418c-bdf3-c3d29ca849e0">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_9d02f345-c524-437d-8667-09881e2255d2" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_e8cda663-8d07-4699-bdd0-d3975760bf9f">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_d73cab93-3fc7-43f2-ab3e-ea724539772d">
                      <gml:posList>550161.099999999980000000 5804599.954999999100000000 56.632343292236328000 550160.884037918530000000 5804598.030609258500000000 56.632343292236328000 550160.884037918880000000 5804598.030609258500000000 59.406429290771484000 550160.831000000240000000 5804597.558000000200000000 59.682206513876260000 550160.831000000240000000 5804597.558000000200000000 53.960000000000008000 550160.884037918530000000 5804598.030609258500000000 53.960000000000001000 550161.099999999980000000 5804599.954999999100000000 53.960000000000008000 550161.099999999980000000 5804599.954999999100000000 56.632343292236328000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_10da8a38-8f6d-4f2b-b2aa-68f5eaeccdad">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_037afc87-cac5-4f18-b60d-2a7c3f051fd3" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_9c16018b-e91e-4b73-93a7-a0f0cc179d08">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_61be1794-477d-4d3e-a9aa-23fd9b1d0ff6">
                      <gml:posList>550166.030999999960000000 5804609.702999998800000000 53.960000000000008000 550164.559000000010000000 5804609.826999999600000000 53.960000000000008000 550164.559000000010000000 5804609.826999999600000000 57.190914154052734000 550166.030999999960000000 5804609.702999998800000000 57.190914154052734000 550166.030999999960000000 5804609.702999998800000000 53.960000000000008000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_9f919b67-7cc7-4f14-a91a-cd9b24420f54">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_09fa9615-b59f-4b9b-b9ef-7132f0529ae1" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_693ba83e-4946-4f4b-a563-c77ce7023f34">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_52c1e805-d606-406e-a57e-d8c11a8b37d0">
                      <gml:posList>550165.758999999960000000 5804606.227999999200000000 57.190914154052734000 550164.285999999960000000 5804606.427999999400000000 57.190914154052734000 550164.285999999960000000 5804606.427999999400000000 53.960000000000008000 550165.758999999960000000 5804606.227999999200000000 53.960000000000008000 550165.758999999960000000 5804606.227999999200000000 57.190914154052734000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_5d7b2836-03da-4eec-a1fe-ff64c8ca54e9">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_5eea069b-8d18-4ca4-9d46-c6968323e7d9" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_5e1e1ac3-2e00-46de-9589-5fd214f9a4f3">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_3461473e-11cc-47d4-ada9-26bd3857b04e">
                      <gml:posList>550164.559000000010000000 5804609.826999999600000000 53.960000000000008000 550164.285999999960000000 5804606.427999999400000000 53.960000000000008000 550164.285999999960000000 5804606.427999999400000000 57.190914154052734000 550164.559000000010000000 5804609.826999999600000000 57.190914154052734000 550164.559000000010000000 5804609.826999999600000000 53.960000000000008000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="UUID_427efc58-ffb5-4653-b0f8-f4b553ec0379">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_c0ac7327-a855-43e8-8e57-101c09572fe3" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_33ead521-dab8-4c20-abf6-941f81f5707b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_d25e945c-d92b-4240-a4d8-96652756bedb">
                      <gml:posList>550165.041704687290000000 5804611.694535684800000000 53.960000000000001000 550165.048999998720000000 5804611.775999999600000000 53.960000000000008000 550161.736999999730000000 5804612.140999999800000000 53.960000000000008000 550162.344391629220000000 5804616.735851987300000000 53.960000000000001000 550162.938999999310000000 5804621.234000000200000000 53.960000000000008000 550172.067999999970000000 5804620.246000000300000000 53.960000000000008000 550171.615920838320000000 5804615.753180522500000000 53.960000000000001000 550171.141307272130000000 5804611.036411302200000000 53.960000000000001000 550170.969000000160000000 5804609.324000000000000000 53.960000000000001000 550169.464000000150000000 5804594.362999999000000000 53.960000000000008000 550164.812478820090000000 5804594.843618034400000000 53.960000000000001000 550164.712001085980000000 5804594.854010487900000000 53.960000000000001000 550157.478000000120000000 5804595.603000000100000000 53.960000000000008000 550157.758397841940000000 5804598.364869072100000000 53.960000000000001000 550157.959999999960000000 5804600.349999998700000000 53.960000000000008000 550161.099999999980000000 5804599.954999999100000000 53.960000000000008000 550160.884037918530000000 5804598.030609258500000000 53.960000000000001000 550160.831000000240000000 5804597.558000000200000000 53.960000000000008000 550164.918999999760000000 5804597.099000000400000000 53.960000000000008000 550164.964568529390000000 5804597.594232270500000000 53.960000000000001000 550165.758999999960000000 5804606.227999999200000000 53.960000000000008000 550164.285999999960000000 5804606.427999999400000000 53.960000000000008000 550164.559000000010000000 5804609.826999999600000000 53.960000000000008000 550166.030999999960000000 5804609.702999998800000000 53.960000000000008000 550166.187999999970000000 5804611.258000000400000000 53.960000000000008000 550165.012999999920000000 5804611.373999999800000000 53.960000000000008000 550165.041704687290000000 5804611.694535684800000000 53.960000000000001000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </cityObjectMember>

  <cityObjectMember>
    <bldg:Building gml:id="DENIAL43000052P6">
      <gml:boundedBy>
        <gml:Envelope srsDimension="3" srsName="urn:adv:crs:ETRS89_UTM32*DE_DHHN92_NH">
          <gml:lowerCorner>550124.250000 5804605.000000 53.989998
</gml:lowerCorner>
          <gml:upperCorner>550136.562500 5804624.000000 75.062996
</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <creationDate>2017-11-21</creationDate>
      <gen:doubleAttribute name="Volume">
        <gen:value>3348.017</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Profile">
        <gen:value>Sig3d</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCK">
        <gen:value>10</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOECKE">
        <gen:value>111010</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STBEZNR">
        <gen:value>111</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="MBZNR">
        <gen:value>1102</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="WBZNR">
        <gen:value>1102</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNAM">
        <gen:value>Vahrenwald</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTNR">
        <gen:value>11</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNAM">
        <gen:value>Vahrenwald-List</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBEZ">
        <gen:value>02</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEMEINDE">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SCALE">
        <gen:value>5000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SHAPE_Area">
        <gen:value>7165.30049704715</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ALKISID">
        <gen:value>DENIAL43000052P6</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ZMIN">
        <gen:value>53.99</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="THICKNESS">
        <gen:value>0.1</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="TEXTURE">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="BUILDINGID">
        <gen:value>DENIAL43000052P6</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ARTBEZEICH">
        <gen:value>Gebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Join_Cou_1">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="FUNKTION_T">
        <gen:value>Wohngebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="OBJFLAECHE">
        <gen:value>185.43</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADT">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEB_Funkti">
        <gen:value>1000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STRSCHL">
        <gen:value>00689</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STR_komple">
        <gen:value>Dessauerstraße 14</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="PLZ_NUMMER">
        <gen:value>30161</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STR">
        <gen:value>Dessauerstraße</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="HAUSNR">
        <gen:value>14</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="OVERHANG">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <app:appearance>
        <app:Appearance>
          <app:theme>RhinoCity ObliqueTexturing</app:theme>
          <app:surfaceDataMember>
            <app:ParameterizedTexture>
              <app:imageURI>Hannover_teil1_Appearance/DENIAL43000052P6.jpg</app:imageURI>
              <app:textureType>specific</app:textureType>
              <app:wrapMode>border</app:wrapMode>
              <app:borderColor>0 0 0 1</app:borderColor>
              <app:target uri="#UUID_b0e5f273-3662-4e3b-b043-88afc87352c5">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_8a9e8af9-2af6-46f0-a603-69a326e1a94a">0.305313 0.536053 0.411726 0.537732 0.544366 0.534247 0.425290 0.679363 0.305284 0.540407 0.305313 0.536053 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_92b384d2-f71b-42a8-8b9c-539d684d1fb9">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_79afd0ab-714d-4605-920a-33d7c5ef1b42">0.302842 0.333472 0.148299 0.449279 0.142288 0.098145 0.140900 0.017837 0.291844 0.001957 0.302842 0.333472 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_5e73b5b5-815e-4974-a76b-2f3bde8f6848">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_8a3377fa-90cc-47b8-a329-382e0f5d553d">0.531936 0.063608 0.590435 0.344519 0.428001 0.327502 0.369863 0.077346 0.519364 0.001957 0.531936 0.063608 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_4e00b5b9-b8d8-4c9f-9fc1-158ab1464114">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_9a0e0ad8-bcd8-4c36-b231-199d805812b4">0.873127 0.183953 0.754378 0.258827 0.625623 0.206889 0.594912 0.020487 0.712538 0.011645 0.841421 0.001957 0.873127 0.183953 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_0102819e-bf4e-48ce-bab1-5c390d5443ef">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_bfb44174-7a3b-4447-b8dd-fdfc0da0e21e">0.315025 0.440774 0.308699 0.093506 0.307241 0.014094 0.356829 0.001957 0.358189 0.082258 0.364076 0.433365 0.315025 0.440774 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_1bee5e95-7244-494b-a4e9-42beb64562fb">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_10a8be5f-8eac-431d-a5ff-088201b4e0e7">0.029920 0.536945 0.161025 0.534247 0.134064 0.721809 0.001957 0.725305 0.029920 0.536945 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_665c4128-5d2e-44b0-ba22-c5210efa83cb">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_2e93e7f8-865e-4e75-8332-615c77c2d886">0.194744 0.534247 0.299986 0.534545 0.272087 0.722886 0.166341 0.721202 0.194744 0.534247 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_7348e8dd-4912-42cb-b4d7-5a351916b47b">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_2970905d-e61d-4d0b-b7c2-0213a7daefcf">0.073828 0.528755 0.072632 0.528175 0.014459 0.246710 0.001957 0.184938 0.065512 0.001957 0.078088 0.062847 0.136422 0.340746 0.136950 0.343261 0.073828 0.528755 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
            </app:ParameterizedTexture>
          </app:surfaceDataMember>
        </app:Appearance>
      </app:appearance>
      <bldg:measuredHeight uom="#m">21.073</bldg:measuredHeight>
      <bldg:lod1Solid>
        <gml:Solid gml:id="UUID_fac0bc31-2138-4c4d-ae52-b3fbd707862d">
          <gml:exterior>
            <gml:CompositeSurface gml:id="UUID_78eb7fa5-75b4-4c4d-a1d2-c5a0fa805fbb">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_815012c0-64bb-49d3-88ac-a27b378a47cf">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_f9fb6e65-2de4-4101-894e-2d7d880005ae">
                      <gml:posList>550136.052000001070000000 5804623.061999999900000000 75.062991866652595000 550136.049438340240000000 5804622.932455729700000000 75.062991866652595000 550135.767999999230000000 5804608.700000000200000000 75.062991866652595000 550135.767999999230000000 5804608.700000000200000000 53.990000000000002000 550136.049438340240000000 5804622.932455729700000000 53.990000000000002000 550136.052000001070000000 5804623.061999999900000000 53.990000000000002000 550136.052000001070000000 5804623.061999999900000000 75.062991866652595000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_d7b52d0b-1877-461a-abe1-8277450dde76">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_5e23471f-a6c1-45f6-95a1-4783d9991b51">
                      <gml:posList>550135.767999999230000000 5804608.700000000200000000 75.062991866652595000 550135.715999998150000000 5804605.603000000100000000 75.062991866652595000 550135.715999998150000000 5804605.603000000100000000 53.990000000000002000 550135.767999999230000000 5804608.700000000200000000 53.990000000000002000 550135.767999999230000000 5804608.700000000200000000 75.062991866652595000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_6fee01d0-31e8-4cb2-8bb6-d149ec43b9e3">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_2e94315c-8901-455f-a17c-0b0ba9ff000e">
                      <gml:posList>550135.715999998150000000 5804605.603000000100000000 75.062991866652595000 550130.021849055660000000 5804606.108484620200000000 75.062991866652595000 550124.822999998930000000 5804606.570000000300000000 75.062991866652595000 550124.822999998930000000 5804606.570000000300000000 53.990000000000002000 550130.021849055660000000 5804606.108484620200000000 53.990000000000002000 550135.715999998150000000 5804605.603000000100000000 53.990000000000002000 550135.715999998150000000 5804605.603000000100000000 75.062991866652595000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_9112cc1c-71d4-4abd-9c0a-47499b76324b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_4cbb1fc3-1ab6-47c4-a337-131cb9850237">
                      <gml:posList>550125.148710010810000000 5804623.193865006800000000 53.990000000000002000 550124.883999999610000000 5804609.663999999900000000 53.990000000000002000 550124.822999998930000000 5804606.570000000300000000 53.990000000000002000 550124.822999998930000000 5804606.570000000300000000 75.062991866652595000 550124.883999999610000000 5804609.663999999900000000 75.062991866652595000 550125.148710010810000000 5804623.193865006800000000 75.062991866652595000 550125.148710010810000000 5804623.193865006800000000 53.990000000000002000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_7eddc3f6-afcc-491f-93f9-73e5207480e5">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_6c7bb808-8d4a-4495-b03e-3d697098f685">
                      <gml:posList>550131.195999998600000000 5804623.047000000300000000 53.990000000000002000 550125.148710010810000000 5804623.193865006800000000 53.990000000000002000 550125.148710010810000000 5804623.193865006800000000 75.062991866652595000 550131.195999998600000000 5804623.047000000300000000 75.062991866652595000 550131.195999998600000000 5804623.047000000300000000 53.990000000000002000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_2fcb4ec4-0d88-4b15-a5d9-a0a0a9804e7f">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_56298b78-5922-4d7c-867d-c62ca5fb4978">
                      <gml:posList>550136.052000001070000000 5804623.061999999900000000 53.990000000000002000 550131.195999998600000000 5804623.047000000300000000 53.990000000000002000 550131.195999998600000000 5804623.047000000300000000 75.062991866652595000 550136.052000001070000000 5804623.061999999900000000 75.062991866652595000 550136.052000001070000000 5804623.061999999900000000 53.990000000000002000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_aea9652c-ed34-4784-99a5-1b521c8a3637">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_21dd45bf-6940-462d-b4a3-83b3d8cf44f4">
                      <gml:posList>550136.049438340240000000 5804622.932455729700000000 53.990000000000002000 550135.767999999230000000 5804608.700000000200000000 53.990000000000002000 550135.715999998150000000 5804605.603000000100000000 53.990000000000002000 550130.021849055660000000 5804606.108484620200000000 53.990000000000002000 550124.822999998930000000 5804606.570000000300000000 53.990000000000002000 550124.883999999610000000 5804609.663999999900000000 53.990000000000002000 550125.148710010810000000 5804623.193865006800000000 53.990000000000002000 550131.195999998600000000 5804623.047000000300000000 53.990000000000002000 550136.052000001070000000 5804623.061999999900000000 53.990000000000002000 550136.049438340240000000 5804622.932455729700000000 53.990000000000002000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_8d7d7c42-bee8-427f-afe0-1f829f2c7582">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_a7feb1af-8b10-4a19-ad94-fbf02b181643">
                      <gml:posList>550136.049438340240000000 5804622.932455729700000000 75.062991866652595000 550136.052000001070000000 5804623.061999999900000000 75.062991866652595000 550131.195999998600000000 5804623.047000000300000000 75.062991866652595000 550125.148710010810000000 5804623.193865006800000000 75.062991866652595000 550124.883999999610000000 5804609.663999999900000000 75.062991866652595000 550124.822999998930000000 5804606.570000000300000000 75.062991866652595000 550130.021849055660000000 5804606.108484620200000000 75.062991866652595000 550135.715999998150000000 5804605.603000000100000000 75.062991866652595000 550135.767999999230000000 5804608.700000000200000000 75.062991866652595000 550136.049438340240000000 5804622.932455729700000000 75.062991866652595000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod1Solid>
      <bldg:lod2Solid>
        <gml:Solid gml:id="UUID_94cfeb69-abf9-4d05-9ed4-0f9d88ccb70f">
          <gml:exterior>
            <gml:CompositeSurface gml:id="UUID_e98ca216-ab4f-4b8e-87d3-df78c03870ae">
              <gml:surfaceMember xlink:href="#UUID_b0e5f273-3662-4e3b-b043-88afc87352c5"/>
              <gml:surfaceMember xlink:href="#UUID_92b384d2-f71b-42a8-8b9c-539d684d1fb9"/>
              <gml:surfaceMember xlink:href="#UUID_5e73b5b5-815e-4974-a76b-2f3bde8f6848"/>
              <gml:surfaceMember xlink:href="#UUID_4e00b5b9-b8d8-4c9f-9fc1-158ab1464114"/>
              <gml:surfaceMember xlink:href="#UUID_0102819e-bf4e-48ce-bab1-5c390d5443ef"/>
              <gml:surfaceMember xlink:href="#UUID_1bee5e95-7244-494b-a4e9-42beb64562fb"/>
              <gml:surfaceMember xlink:href="#UUID_665c4128-5d2e-44b0-ba22-c5210efa83cb"/>
              <gml:surfaceMember xlink:href="#UUID_7348e8dd-4912-42cb-b4d7-5a351916b47b"/>
              <gml:surfaceMember xlink:href="#UUID_ecc2c155-f905-4ae9-bbc2-419c8ca5d780"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_f61bbbba-8c6e-4c1b-a4f8-6720b98b7b6f">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_49d54d96-6182-423b-8f4b-24a0e0403676" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_b0e5f273-3662-4e3b-b043-88afc87352c5">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_8a9e8af9-2af6-46f0-a603-69a326e1a94a">
                      <gml:posList>550136.052000001070000000 5804623.061999999900000000 69.542908687429588000 550131.195999998600000000 5804623.047000000300000000 69.657151582191617000 550125.148710010810000000 5804623.193865006800000000 69.591686793970979000 550130.454197921320000000 5804618.840497087700000000 74.922486384537706000 550136.049438340240000000 5804622.932455729700000000 69.704661973198455000 550136.052000001070000000 5804623.061999999900000000 69.542908687429588000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_658ff357-9e40-44a0-9ed5-aca93419a1a2">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_8414bfea-7aaa-4475-a81e-0c5a39cfb55f" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_92b384d2-f71b-42a8-8b9c-539d684d1fb9">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_79afd0ab-714d-4605-920a-33d7c5ef1b42">
                      <gml:posList>550130.454197921320000000 5804618.840497087700000000 74.922486384537706000 550125.148710010810000000 5804623.193865006800000000 69.591686793970979000 550124.883999999610000000 5804609.663999999900000000 69.751186245051969000 550124.822999998930000000 5804606.570000000300000000 69.787356640528543000 550130.021849055660000000 5804606.108484620200000000 74.893724716655868000 550130.454197921320000000 5804618.840497087700000000 74.922486384537706000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_1af24907-4eb7-4706-b663-bf7bf4d36e47">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_33bb166a-e482-4274-89fb-abd06b161ccf" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_5e73b5b5-815e-4974-a76b-2f3bde8f6848">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_8a3377fa-90cc-47b8-a329-382e0f5d553d">
                      <gml:posList>550135.767999999230000000 5804608.700000000200000000 69.479157144489108000 550136.049438340240000000 5804622.932455729700000000 69.704661973198455000 550130.454197921320000000 5804618.840497087700000000 74.922486384537706000 550130.021849055660000000 5804606.108484620200000000 74.893724716655868000 550135.715999998150000000 5804605.603000000100000000 69.421234416999141000 550135.767999999230000000 5804608.700000000200000000 69.479157144489108000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_3b07ccdc-93c3-4f61-a5ce-10386190d928">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_a8b31557-bf34-486c-89a4-f6dbc5352e90" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_4e00b5b9-b8d8-4c9f-9fc1-158ab1464114">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_9a0e0ad8-bcd8-4c36-b231-199d805812b4">
                      <gml:posList>550135.715999998150000000 5804605.603000000100000000 69.421234416999141000 550130.021849055660000000 5804606.108484620200000000 74.893724716655868000 550124.822999998930000000 5804606.570000000300000000 69.787356640528543000 550124.822999998930000000 5804606.570000000300000000 53.990000000000002000 550130.021849055660000000 5804606.108484620200000000 53.990000000000002000 550135.715999998150000000 5804605.603000000100000000 53.990000000000002000 550135.715999998150000000 5804605.603000000100000000 69.421234416999141000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_7907f2a6-d050-406b-bc88-bd2283da9b1a">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_e89fc2ec-f6a8-4317-9499-c91949270c0e" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_0102819e-bf4e-48ce-bab1-5c390d5443ef">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_bfb44174-7a3b-4447-b8dd-fdfc0da0e21e">
                      <gml:posList>550125.148710010810000000 5804623.193865006800000000 53.990000000000002000 550124.883999999610000000 5804609.663999999900000000 53.990000000000002000 550124.822999998930000000 5804606.570000000300000000 53.990000000000002000 550124.822999998930000000 5804606.570000000300000000 69.787356640528543000 550124.883999999610000000 5804609.663999999900000000 69.751186245051969000 550125.148710010810000000 5804623.193865006800000000 69.591686793970979000 550125.148710010810000000 5804623.193865006800000000 53.990000000000002000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_286c2eb2-e05e-47f7-84b8-9f901dbdfae7">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_65ab3c1d-d070-4ffb-be16-c18f25ecacf8" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_1bee5e95-7244-494b-a4e9-42beb64562fb">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_10a8be5f-8eac-431d-a5ff-088201b4e0e7">
                      <gml:posList>550131.195999998600000000 5804623.047000000300000000 53.990000000000002000 550125.148710010810000000 5804623.193865006800000000 53.990000000000002000 550125.148710010810000000 5804623.193865006800000000 69.591686793970979000 550131.195999998600000000 5804623.047000000300000000 69.657151582191617000 550131.195999998600000000 5804623.047000000300000000 53.990000000000002000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_e8f286bb-5502-482e-ae2b-9a509c385739">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_1fdeeb9d-7496-4f64-82a8-195ca271b98c" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_665c4128-5d2e-44b0-ba22-c5210efa83cb">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_2e93e7f8-865e-4e75-8332-615c77c2d886">
                      <gml:posList>550136.052000001070000000 5804623.061999999900000000 53.990000000000002000 550131.195999998600000000 5804623.047000000300000000 53.990000000000002000 550131.195999998600000000 5804623.047000000300000000 69.657151582191617000 550136.052000001070000000 5804623.061999999900000000 69.542908687429588000 550136.052000001070000000 5804623.061999999900000000 53.990000000000002000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_0d4ae5de-a1ba-423d-8b75-97293f4657f0">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_c55a5acd-88c0-4704-bac0-1f474814b303" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_7348e8dd-4912-42cb-b4d7-5a351916b47b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_2970905d-e61d-4d0b-b7c2-0213a7daefcf">
                      <gml:posList>550136.052000001070000000 5804623.061999999900000000 69.542908687429588000 550136.049438340240000000 5804622.932455729700000000 69.704661973198455000 550135.767999999230000000 5804608.700000000200000000 69.479157144489108000 550135.715999998150000000 5804605.603000000100000000 69.421234416999141000 550135.715999998150000000 5804605.603000000100000000 53.990000000000002000 550135.767999999230000000 5804608.700000000200000000 53.990000000000002000 550136.049438340240000000 5804622.932455729700000000 53.990000000000002000 550136.052000001070000000 5804623.061999999900000000 53.990000000000002000 550136.052000001070000000 5804623.061999999900000000 69.542908687429588000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="UUID_31cbff32-83fa-498f-a437-405f8eb5d1e5">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_80c11bcc-7a7c-4a5c-9723-0dab5fba3714" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_ecc2c155-f905-4ae9-bbc2-419c8ca5d780">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_9d212ef9-5f9e-447c-a3b9-4e0c46d015dc">
                      <gml:posList>550136.049438340240000000 5804622.932455729700000000 53.990000000000002000 550135.767999999230000000 5804608.700000000200000000 53.990000000000002000 550135.715999998150000000 5804605.603000000100000000 53.990000000000002000 550130.021849055660000000 5804606.108484620200000000 53.990000000000002000 550124.822999998930000000 5804606.570000000300000000 53.990000000000002000 550124.883999999610000000 5804609.663999999900000000 53.990000000000002000 550125.148710010810000000 5804623.193865006800000000 53.990000000000002000 550131.195999998600000000 5804623.047000000300000000 53.990000000000002000 550136.052000001070000000 5804623.061999999900000000 53.990000000000002000 550136.049438340240000000 5804622.932455729700000000 53.990000000000002000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </cityObjectMember>

  <cityObjectMember>
    <bldg:Building gml:id="DENIAL43000052ZK">
      <gml:boundedBy>
        <gml:Envelope srsDimension="3" srsName="urn:adv:crs:ETRS89_UTM32*DE_DHHN92_NH">
          <gml:lowerCorner>550150.250000 5804595.500000 54.129997
</gml:lowerCorner>
          <gml:upperCorner>550158.000000 5804601.500000 56.191929
</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <creationDate>2017-11-21</creationDate>
      <gen:doubleAttribute name="Volume">
        <gen:value>75.300</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Profile">
        <gen:value>Sig3d</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCK">
        <gen:value>10</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOECKE">
        <gen:value>111010</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STBEZNR">
        <gen:value>111</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="MBZNR">
        <gen:value>1102</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="WBZNR">
        <gen:value>1102</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNAM">
        <gen:value>Vahrenwald</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTNR">
        <gen:value>11</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNAM">
        <gen:value>Vahrenwald-List</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBEZ">
        <gen:value>02</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEMEINDE">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SCALE">
        <gen:value>5000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SHAPE_Area">
        <gen:value>7165.30049704715</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ALKISID">
        <gen:value>DENIAL43000052ZK</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ZMIN">
        <gen:value>54.13</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="THICKNESS">
        <gen:value>0.1</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="TEXTURE">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="BUILDINGID">
        <gen:value>DENIAL43000052ZK</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ARTBEZEICH">
        <gen:value>Gebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Join_Cou_1">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="FUNKTION_T">
        <gen:value>Gebäude für Wirtschaft oder Gewerbe</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="OBJFLAECHE">
        <gen:value>36.52</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADT">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEB_Funkti">
        <gen:value>2000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STRSCHL">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STR_komple">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="PLZ_NUMMER">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STR">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="HAUSNR">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="OVERHANG">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <app:appearance>
        <app:Appearance>
          <app:theme>RhinoCity ObliqueTexturing</app:theme>
          <app:surfaceDataMember>
            <app:ParameterizedTexture>
              <app:imageURI>Hannover_teil1_Appearance/DENIAL43000052ZK.jpg</app:imageURI>
              <app:textureType>specific</app:textureType>
              <app:wrapMode>border</app:wrapMode>
              <app:borderColor>0 0 0 1</app:borderColor>
              <app:target uri="#UUID_9ba7a2df-13ee-40ed-a316-f942bc4e438c">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_3d2179f0-e46d-4bdc-8fba-192454922bfe">0.796657 0.246314 0.798085 0.253258 0.020622 0.294640 0.007874 0.041591 0.747480 0.003922 0.771354 0.121620 0.796657 0.246314 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_d9d9198a-19f5-4182-8e04-4c39505947cf">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_f357b0e7-cabe-458d-98d7-d52fa3ca75b4">0.117275 0.545345 0.114218 0.540021 0.059558 0.444330 0.007874 0.353835 0.039066 0.305882 0.090675 0.396339 0.145254 0.491989 0.148307 0.497311 0.117275 0.545345 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_a5100cb3-7183-49aa-bb02-26ece693ea1e">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_432ee869-7e4c-46d7-969c-c8546cb8cf11">0.682518 0.603078 0.026081 0.630765 0.007874 0.584538 0.663702 0.556863 0.682518 0.603078 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_99426fe2-a9ab-4c34-8caa-c0bfbe179a84">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_f98ab83c-ad40-454d-a1f2-0aeed4d135ed">0.829611 0.260004 0.818898 0.007763 0.844210 0.003922 0.854938 0.256517 0.829611 0.260004 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_81444d65-b543-46c5-9087-37c7bc21c5b0">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_ecde48ab-59d1-4f7a-aba2-1ac6955a35f9">0.165354 0.334505 0.833490 0.305882 0.863340 0.353718 0.194604 0.382350 0.165354 0.334505 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
            </app:ParameterizedTexture>
          </app:surfaceDataMember>
        </app:Appearance>
      </app:appearance>
      <bldg:measuredHeight uom="#m">2.062</bldg:measuredHeight>
      <bldg:lod1Solid>
        <gml:Solid gml:id="UUID_8561b72c-d03c-4c5d-8243-7ad2dc48e405">
          <gml:exterior>
            <gml:CompositeSurface gml:id="UUID_7fdbcd24-5a99-4e65-bb71-3f73cce1621f">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_19ef21ae-37be-4a28-a2cb-015881dc9bb2">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_b5292068-e756-46ba-aae1-240ae15741ae">
                      <gml:posList>550157.959999999960000000 5804600.349999998700000000 56.191925048828125000 550157.712000000060000000 5804597.907999999800000000 56.191925048828125000 550157.478000000000000000 5804595.602999999200000000 56.191925048828125000 550157.478000000000000000 5804595.602999999200000000 54.130000000000010000 550157.712000000060000000 5804597.907999999800000000 54.130000000000003000 550157.959999999960000000 5804600.349999998700000000 54.130000000000003000 550157.959999999960000000 5804600.349999998700000000 56.191925048828125000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_8731a2cb-09a9-4dfb-8bea-67abb6d2c21b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_30749a0a-ab88-4951-a445-1baae3a56487">
                      <gml:posList>550157.478000000000000000 5804595.602999999200000000 56.191925048828125000 550150.287999999940000000 5804596.347000000100000000 56.191925048828125000 550150.287999999940000000 5804596.347000000100000000 54.130000000000010000 550157.478000000000000000 5804595.602999999200000000 54.130000000000010000 550157.478000000000000000 5804595.602999999200000000 56.191925048828125000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_d8a9fa11-a4f6-4018-a765-e0c7afba5ff8">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_bf0b4809-b64b-4a4c-acb9-9d1c8624c672">
                      <gml:posList>550150.415999999850000000 5804601.302999999400000000 54.130000000000010000 550150.287999999940000000 5804596.347000000100000000 54.130000000000010000 550150.287999999940000000 5804596.347000000100000000 56.191925048828125000 550150.415999999850000000 5804601.302999999400000000 56.191925048828125000 550150.415999999850000000 5804601.302999999400000000 54.130000000000010000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_3693c270-5286-4a7b-a4c7-a9a4cc252e5e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_c7a52637-8845-48a1-9bc1-ee4ab733f857">
                      <gml:posList>550157.974000000050000000 5804600.485999999600000000 54.130000000000010000 550150.415999999850000000 5804601.302999999400000000 54.130000000000010000 550150.415999999850000000 5804601.302999999400000000 56.191925048828125000 550157.974000000050000000 5804600.485999999600000000 56.191925048828125000 550157.974000000050000000 5804600.485999999600000000 54.130000000000010000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_f12d0c5e-ed1e-45a3-af20-d8f8a87972b7">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_ae3c3b41-04cc-482b-ba7c-1804fde6d859">
                      <gml:posList>550157.974000000050000000 5804600.485999999600000000 56.191925048828125000 550157.959999999960000000 5804600.349999998700000000 56.191925048828125000 550157.959999999960000000 5804600.349999998700000000 54.130000000000003000 550157.974000000050000000 5804600.485999999600000000 54.130000000000010000 550157.974000000050000000 5804600.485999999600000000 56.191925048828125000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_a7236d74-ec82-4df6-b09e-0cece6874953">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_8ef76bc1-bf6b-4911-a27a-596e39891703">
                      <gml:posList>550157.712000000060000000 5804597.907999999800000000 54.130000000000003000 550157.478000000000000000 5804595.602999999200000000 54.130000000000010000 550150.287999999940000000 5804596.347000000100000000 54.130000000000010000 550150.415999999850000000 5804601.302999999400000000 54.130000000000010000 550157.974000000050000000 5804600.485999999600000000 54.130000000000010000 550157.959999999960000000 5804600.349999998700000000 54.130000000000003000 550157.712000000060000000 5804597.907999999800000000 54.130000000000003000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_38063e2c-b9b9-4fb8-8e59-7e7ba8a088c5">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_a6c749ee-e4fe-4b6d-9994-13d31f7e2521">
                      <gml:posList>550157.959999999960000000 5804600.349999998700000000 56.191925048828125000 550157.974000000050000000 5804600.485999999600000000 56.191925048828125000 550150.415999999850000000 5804601.302999999400000000 56.191925048828125000 550150.287999999940000000 5804596.347000000100000000 56.191925048828125000 550157.478000000000000000 5804595.602999999200000000 56.191925048828125000 550157.712000000060000000 5804597.907999999800000000 56.191925048828125000 550157.959999999960000000 5804600.349999998700000000 56.191925048828125000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod1Solid>
      <bldg:lod2Solid>
        <gml:Solid gml:id="UUID_fa78f802-a6e0-42a8-90da-35792737fc8a">
          <gml:exterior>
            <gml:CompositeSurface gml:id="UUID_58fad902-213b-4b6f-8898-efb85f5f1ef9">
              <gml:surfaceMember xlink:href="#UUID_9ba7a2df-13ee-40ed-a316-f942bc4e438c"/>
              <gml:surfaceMember xlink:href="#UUID_d9d9198a-19f5-4182-8e04-4c39505947cf"/>
              <gml:surfaceMember xlink:href="#UUID_a5100cb3-7183-49aa-bb02-26ece693ea1e"/>
              <gml:surfaceMember xlink:href="#UUID_99426fe2-a9ab-4c34-8caa-c0bfbe179a84"/>
              <gml:surfaceMember xlink:href="#UUID_81444d65-b543-46c5-9087-37c7bc21c5b0"/>
              <gml:surfaceMember xlink:href="#UUID_1fc390cd-565a-4bb6-a493-aa80360e0796"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_1a17eb72-95dc-415f-bb6f-7c8bdb0f98bf">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_d3fc1556-3d69-4cbe-b351-e4e80e3e326a" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_9ba7a2df-13ee-40ed-a316-f942bc4e438c">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_3d2179f0-e46d-4bdc-8fba-192454922bfe">
                      <gml:posList>550157.959999999960000000 5804600.349999998700000000 56.191925048828125000 550157.974000000050000000 5804600.485999999600000000 56.191925048828125000 550150.415999999850000000 5804601.302999999400000000 56.191925048828125000 550150.287999999940000000 5804596.347000000100000000 56.191925048828125000 550157.478000000000000000 5804595.602999999200000000 56.191925048828125000 550157.712000000060000000 5804597.907999999800000000 56.191925048828125000 550157.959999999960000000 5804600.349999998700000000 56.191925048828125000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_aba3be64-93a6-43b2-89f2-5de09ac06d11">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_913a4da7-05ba-4f80-b0f9-bcb9708ff3ab" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_d9d9198a-19f5-4182-8e04-4c39505947cf">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_f357b0e7-cabe-458d-98d7-d52fa3ca75b4">
                      <gml:posList>550157.974000000050000000 5804600.485999999600000000 56.191925048828125000 550157.959999999960000000 5804600.349999998700000000 56.191925048828125000 550157.712000000060000000 5804597.907999999800000000 56.191925048828125000 550157.478000000000000000 5804595.602999999200000000 56.191925048828125000 550157.478000000000000000 5804595.602999999200000000 54.130000000000010000 550157.712000000060000000 5804597.907999999800000000 54.130000000000003000 550157.959999999960000000 5804600.349999998700000000 54.130000000000003000 550157.974000000050000000 5804600.485999999600000000 54.130000000000010000 550157.974000000050000000 5804600.485999999600000000 56.191925048828125000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_d49f8419-9c06-4a70-95ac-d86c67f5595a">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_68ed59b0-0ee0-48db-aee3-793648115050" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_a5100cb3-7183-49aa-bb02-26ece693ea1e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_432ee869-7e4c-46d7-969c-c8546cb8cf11">
                      <gml:posList>550157.478000000000000000 5804595.602999999200000000 56.191925048828125000 550150.287999999940000000 5804596.347000000100000000 56.191925048828125000 550150.287999999940000000 5804596.347000000100000000 54.130000000000010000 550157.478000000000000000 5804595.602999999200000000 54.130000000000010000 550157.478000000000000000 5804595.602999999200000000 56.191925048828125000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_da6fd234-8b0d-462b-be79-56f986306b0f">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_47417078-bb30-4228-bb59-f66a7c8d7463" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_99426fe2-a9ab-4c34-8caa-c0bfbe179a84">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_f98ab83c-ad40-454d-a1f2-0aeed4d135ed">
                      <gml:posList>550150.415999999850000000 5804601.302999999400000000 54.130000000000010000 550150.287999999940000000 5804596.347000000100000000 54.130000000000010000 550150.287999999940000000 5804596.347000000100000000 56.191925048828125000 550150.415999999850000000 5804601.302999999400000000 56.191925048828125000 550150.415999999850000000 5804601.302999999400000000 54.130000000000010000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_39ed516e-34d5-4ba7-8cc5-236c33c969aa">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_d61e7eeb-056c-4d94-8dc6-ad2dd2d82444" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_81444d65-b543-46c5-9087-37c7bc21c5b0">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_ecde48ab-59d1-4f7a-aba2-1ac6955a35f9">
                      <gml:posList>550157.974000000050000000 5804600.485999999600000000 54.130000000000010000 550150.415999999850000000 5804601.302999999400000000 54.130000000000010000 550150.415999999850000000 5804601.302999999400000000 56.191925048828125000 550157.974000000050000000 5804600.485999999600000000 56.191925048828125000 550157.974000000050000000 5804600.485999999600000000 54.130000000000010000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="UUID_4400ce8a-29bd-4295-9ab4-68ae38f138e6">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_d19f4ad8-0277-46fe-8789-56d0e2cf4cb7" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_1fc390cd-565a-4bb6-a493-aa80360e0796">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_784fb825-66d7-443c-904c-d08033a13910">
                      <gml:posList>550157.712000000060000000 5804597.907999999800000000 54.130000000000003000 550157.478000000000000000 5804595.602999999200000000 54.130000000000010000 550150.287999999940000000 5804596.347000000100000000 54.130000000000010000 550150.415999999850000000 5804601.302999999400000000 54.130000000000010000 550157.974000000050000000 5804600.485999999600000000 54.130000000000010000 550157.959999999960000000 5804600.349999998700000000 54.130000000000003000 550157.712000000060000000 5804597.907999999800000000 54.130000000000003000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </cityObjectMember>

  <cityObjectMember>
    <bldg:Building gml:id="DENIAL43000053pW">
      <gml:boundedBy>
        <gml:Envelope srsDimension="3" srsName="urn:adv:crs:ETRS89_UTM32*DE_DHHN92_NH">
          <gml:lowerCorner>550139.500000 5804596.000000 54.109997
</gml:lowerCorner>
          <gml:upperCorner>550150.500000 5804605.000000 57.472145
</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <creationDate>2017-11-21</creationDate>
      <gen:doubleAttribute name="Volume">
        <gen:value>209.691</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Profile">
        <gen:value>Sig3d</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCK">
        <gen:value>10</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOECKE">
        <gen:value>111010</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STBEZNR">
        <gen:value>111</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="MBZNR">
        <gen:value>1102</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="WBZNR">
        <gen:value>1102</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNAM">
        <gen:value>Vahrenwald</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTNR">
        <gen:value>11</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNAM">
        <gen:value>Vahrenwald-List</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBEZ">
        <gen:value>02</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEMEINDE">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SCALE">
        <gen:value>5000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SHAPE_Area">
        <gen:value>7165.30049704715</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ALKISID">
        <gen:value>DENIAL43000053pW</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ZMIN">
        <gen:value>54.11</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="THICKNESS">
        <gen:value>0.1</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="TEXTURE">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="BUILDINGID">
        <gen:value>DENIAL43000053pW</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ARTBEZEICH">
        <gen:value>Gebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Join_Cou_1">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="FUNKTION_T">
        <gen:value>Gebäude für Wirtschaft oder Gewerbe</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="OBJFLAECHE">
        <gen:value>62.37</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADT">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEB_Funkti">
        <gen:value>2000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STRSCHL">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STR_komple">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="PLZ_NUMMER">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STR">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="HAUSNR">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="OVERHANG">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <app:appearance>
        <app:Appearance>
          <app:theme>RhinoCity ObliqueTexturing</app:theme>
          <app:surfaceDataMember>
            <app:ParameterizedTexture>
              <app:imageURI>Hannover_teil1_Appearance/DENIAL43000053pW.jpg</app:imageURI>
              <app:textureType>specific</app:textureType>
              <app:wrapMode>border</app:wrapMode>
              <app:borderColor>0 0 0 1</app:borderColor>
              <app:target uri="#UUID_4310abd0-b571-49fd-b22b-208150dd6f11">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_1e6ed206-37f7-4fac-ab88-b8099f93a2bf">0.562621 0.258256 0.566403 0.411647 0.302484 0.434769 0.290720 0.232620 0.011221 0.252547 0.003922 0.062512 0.131925 0.047278 0.308577 0.029213 0.556248 0.003922 0.562621 0.258256 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_e0c1be77-8a6f-4ec3-92c1-ddea8a599747">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_50e20543-3d28-466b-9baa-cdf10d3d2bd4">0.640995 0.395660 0.616801 0.278136 0.576471 0.082651 0.602606 0.003922 0.642825 0.199272 0.666952 0.316715 0.640995 0.395660 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_91079bd4-b731-4f65-9ba6-f316ec7f7fe8">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_d9beea59-b9ea-45ba-8fe9-0f73989c94a8">0.508330 0.520044 0.288492 0.538899 0.131746 0.552362 0.018032 0.563734 0.003922 0.486797 0.117463 0.475433 0.273973 0.461979 0.493479 0.443137 0.508330 0.520044 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_740c9042-fd47-4ac5-9869-1d924f1f82e1">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_9a7bfdd3-1680-4eaf-b83d-fe5317152d0a">0.727986 0.197601 0.721569 0.010028 0.742058 0.003922 0.748490 0.191924 0.727986 0.197601 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_63952a5d-14fb-4997-8f48-6bf1d66cf9e3">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_f6e12f41-b097-4154-bd75-03b9fe4afe1e">0.003922 0.586382 0.241174 0.572549 0.266007 0.651218 0.028408 0.665059 0.003922 0.586382 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_0547506d-b0d2-4bdf-a19d-c4ad391291a8">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_1c2b9496-09b2-4d7b-b176-20cbb29fec6a">0.689068 0.209402 0.678431 0.009650 0.700101 0.003922 0.710762 0.204131 0.689068 0.209402 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_18a4e9a3-1293-4a07-92ff-3f9dc0a197ea">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_13852143-5111-465b-beb0-6d4716bbddab">0.517647 0.459253 0.742341 0.443137 0.766871 0.521857 0.541848 0.537982 0.517647 0.459253 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
            </app:ParameterizedTexture>
          </app:surfaceDataMember>
        </app:Appearance>
      </app:appearance>
      <bldg:measuredHeight uom="#m">3.362</bldg:measuredHeight>
      <bldg:lod1Solid>
        <gml:Solid gml:id="UUID_69a2e535-235a-428e-b985-ff09b3a60319">
          <gml:exterior>
            <gml:CompositeSurface gml:id="UUID_a821d430-5f53-47d1-bba1-344c6540eb73">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_89bb234d-2834-4128-b659-998282cef697">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_d59e7243-c32a-4800-9fe7-2beca38d230b">
                      <gml:posList>550150.492000000200000000 5804604.292000000400000000 57.472141265869141000 550150.415999999850000000 5804601.302999999400000000 57.472141265869141000 550150.287999999940000000 5804596.347000000100000000 57.472141265869141000 550150.287999999940000000 5804596.347000000100000000 54.109999999999992000 550150.415999999850000000 5804601.302999999400000000 54.109999999999999000 550150.492000000200000000 5804604.292000000400000000 54.109999999999992000 550150.492000000200000000 5804604.292000000400000000 57.472141265869141000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_d086835c-3f63-42ea-b664-4400b86c1f6d">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_c53e2535-fd8a-45a1-bb60-91a222168edd">
                      <gml:posList>550150.287999999940000000 5804596.347000000100000000 57.472141265869141000 550145.472000000070000000 5804596.843999999600000000 57.472141265869141000 550142.037000000010000000 5804597.198999999100000000 57.472141265869141000 550142.037000000010000000 5804597.198999999100000000 54.109999999999999000 550145.472000000070000000 5804596.843999999600000000 54.109999999999999000 550150.287999999940000000 5804596.347000000100000000 54.109999999999992000 550150.287999999940000000 5804596.347000000100000000 57.472141265869141000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_b3b164f2-fefe-422a-80d7-0cdaab53d2b1">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_474ffcbc-71e1-4553-9b0a-98c9ad9893c7">
                      <gml:posList>550142.037000000010000000 5804597.198999999100000000 57.472141265869141000 550139.547999999950000000 5804597.497999998700000000 57.472141265869141000 550139.547999999950000000 5804597.497999998700000000 54.109999999999992000 550142.037000000010000000 5804597.198999999100000000 54.109999999999999000 550142.037000000010000000 5804597.198999999100000000 57.472141265869141000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_9e3f81ef-3fb5-4edc-ad9d-7a03aa292cf6">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_a9d46029-6df7-472a-b5a3-c1e9ea67ab51">
                      <gml:posList>550139.692999999970000000 5804601.201000000400000000 54.109999999999992000 550139.547999999950000000 5804597.497999998700000000 54.109999999999992000 550139.547999999950000000 5804597.497999998700000000 57.472141265869141000 550139.692999999970000000 5804601.201000000400000000 57.472141265869141000 550139.692999999970000000 5804601.201000000400000000 54.109999999999992000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_112780c2-6201-419a-a628-2d4524a6e098">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_1c8a292a-6234-485e-b9ba-c3218563c0f9">
                      <gml:posList>550145.128000000140000000 5804600.807999999300000000 54.109999999999992000 550139.692999999970000000 5804601.201000000400000000 54.109999999999992000 550139.692999999970000000 5804601.201000000400000000 57.472141265869141000 550145.128000000140000000 5804600.807999999300000000 57.472141265869141000 550145.128000000140000000 5804600.807999999300000000 54.109999999999992000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_61389f86-70ff-42e6-b438-0169a9c59bef">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_869e895a-b889-4290-bfca-4c1c9cad4381">
                      <gml:posList>550145.359999999990000000 5804604.747000000400000000 54.109999999999992000 550145.128000000140000000 5804600.807999999300000000 54.109999999999992000 550145.128000000140000000 5804600.807999999300000000 57.472141265869141000 550145.359999999990000000 5804604.747000000400000000 57.472141265869141000 550145.359999999990000000 5804604.747000000400000000 54.109999999999992000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_0705f1b8-9391-4e30-9090-9d69a826e461">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_ffb47fa2-fe95-421c-ad5a-81a8d6a759fc">
                      <gml:posList>550150.492000000200000000 5804604.292000000400000000 54.109999999999992000 550145.359999999990000000 5804604.747000000400000000 54.109999999999992000 550145.359999999990000000 5804604.747000000400000000 57.472141265869141000 550150.492000000200000000 5804604.292000000400000000 57.472141265869141000 550150.492000000200000000 5804604.292000000400000000 54.109999999999992000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_cd3119af-6e47-424a-8af3-88700e57431c">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_d7da9ce7-c15f-48eb-8dde-f04a6456509b">
                      <gml:posList>550150.415999999850000000 5804601.302999999400000000 54.109999999999999000 550150.287999999940000000 5804596.347000000100000000 54.109999999999992000 550145.472000000070000000 5804596.843999999600000000 54.109999999999999000 550142.037000000010000000 5804597.198999999100000000 54.109999999999999000 550139.547999999950000000 5804597.497999998700000000 54.109999999999992000 550139.692999999970000000 5804601.201000000400000000 54.109999999999992000 550145.128000000140000000 5804600.807999999300000000 54.109999999999992000 550145.359999999990000000 5804604.747000000400000000 54.109999999999992000 550150.492000000200000000 5804604.292000000400000000 54.109999999999992000 550150.415999999850000000 5804601.302999999400000000 54.109999999999999000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_5fc6a0ce-6e27-4ac8-a172-6f359f86424f">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_192834f4-fe53-47a6-b4ac-7fbad47d5dd7">
                      <gml:posList>550150.415999999850000000 5804601.302999999400000000 57.472141265869141000 550150.492000000200000000 5804604.292000000400000000 57.472141265869141000 550145.359999999990000000 5804604.747000000400000000 57.472141265869141000 550145.128000000140000000 5804600.807999999300000000 57.472141265869141000 550139.692999999970000000 5804601.201000000400000000 57.472141265869141000 550139.547999999950000000 5804597.497999998700000000 57.472141265869141000 550142.037000000010000000 5804597.198999999100000000 57.472141265869141000 550145.472000000070000000 5804596.843999999600000000 57.472141265869141000 550150.287999999940000000 5804596.347000000100000000 57.472141265869141000 550150.415999999850000000 5804601.302999999400000000 57.472141265869141000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod1Solid>
      <bldg:lod2Solid>
        <gml:Solid gml:id="UUID_0bba941d-8c2f-4045-b7a1-3ce393904456">
          <gml:exterior>
            <gml:CompositeSurface gml:id="UUID_e2904f80-d173-4498-a67f-3341d990dd46">
              <gml:surfaceMember xlink:href="#UUID_4310abd0-b571-49fd-b22b-208150dd6f11"/>
              <gml:surfaceMember xlink:href="#UUID_e0c1be77-8a6f-4ec3-92c1-ddea8a599747"/>
              <gml:surfaceMember xlink:href="#UUID_91079bd4-b731-4f65-9ba6-f316ec7f7fe8"/>
              <gml:surfaceMember xlink:href="#UUID_740c9042-fd47-4ac5-9869-1d924f1f82e1"/>
              <gml:surfaceMember xlink:href="#UUID_63952a5d-14fb-4997-8f48-6bf1d66cf9e3"/>
              <gml:surfaceMember xlink:href="#UUID_0547506d-b0d2-4bdf-a19d-c4ad391291a8"/>
              <gml:surfaceMember xlink:href="#UUID_18a4e9a3-1293-4a07-92ff-3f9dc0a197ea"/>
              <gml:surfaceMember xlink:href="#UUID_a1ae486c-5f11-46b6-8234-d6d4e842054d"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_d6c59794-cdb9-4381-a21e-23cbf99df53a">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_9fb15ee7-c8bc-4063-adce-9a698df1785e" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_4310abd0-b571-49fd-b22b-208150dd6f11">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_1e6ed206-37f7-4fac-ab88-b8099f93a2bf">
                      <gml:posList>550150.415999999850000000 5804601.302999999400000000 57.472141265869141000 550150.492000000200000000 5804604.292000000400000000 57.472141265869141000 550145.359999999990000000 5804604.747000000400000000 57.472141265869141000 550145.128000000140000000 5804600.807999999300000000 57.472141265869141000 550139.692999999970000000 5804601.201000000400000000 57.472141265869141000 550139.547999999950000000 5804597.497999998700000000 57.472141265869141000 550142.037000000010000000 5804597.198999999100000000 57.472141265869141000 550145.472000000070000000 5804596.843999999600000000 57.472141265869141000 550150.287999999940000000 5804596.347000000100000000 57.472141265869141000 550150.415999999850000000 5804601.302999999400000000 57.472141265869141000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_bd473b0f-928c-44a2-9913-383312839430">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_021bcf2e-b14c-4760-9137-14d2d0da66cf" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_e0c1be77-8a6f-4ec3-92c1-ddea8a599747">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_50e20543-3d28-466b-9baa-cdf10d3d2bd4">
                      <gml:posList>550150.492000000200000000 5804604.292000000400000000 57.472141265869141000 550150.415999999850000000 5804601.302999999400000000 57.472141265869141000 550150.287999999940000000 5804596.347000000100000000 57.472141265869141000 550150.287999999940000000 5804596.347000000100000000 54.109999999999992000 550150.415999999850000000 5804601.302999999400000000 54.109999999999999000 550150.492000000200000000 5804604.292000000400000000 54.109999999999992000 550150.492000000200000000 5804604.292000000400000000 57.472141265869141000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_6188cef5-415b-4f49-aa0b-7cea586f4803">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_a16f9407-446e-4bdd-a682-3b20fccf429e" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_91079bd4-b731-4f65-9ba6-f316ec7f7fe8">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_d9beea59-b9ea-45ba-8fe9-0f73989c94a8">
                      <gml:posList>550150.287999999940000000 5804596.347000000100000000 57.472141265869141000 550145.472000000070000000 5804596.843999999600000000 57.472141265869141000 550142.037000000010000000 5804597.198999999100000000 57.472141265869141000 550139.547999999950000000 5804597.497999998700000000 57.472141265869141000 550139.547999999950000000 5804597.497999998700000000 54.109999999999992000 550142.037000000010000000 5804597.198999999100000000 54.109999999999999000 550145.472000000070000000 5804596.843999999600000000 54.109999999999999000 550150.287999999940000000 5804596.347000000100000000 54.109999999999992000 550150.287999999940000000 5804596.347000000100000000 57.472141265869141000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_79ee2a9c-d500-46d7-8827-ee52263eb490">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_be91b9c1-08e0-49f3-a7cd-6fac161e1bdf" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_740c9042-fd47-4ac5-9869-1d924f1f82e1">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_9a7bfdd3-1680-4eaf-b83d-fe5317152d0a">
                      <gml:posList>550139.692999999970000000 5804601.201000000400000000 54.109999999999992000 550139.547999999950000000 5804597.497999998700000000 54.109999999999992000 550139.547999999950000000 5804597.497999998700000000 57.472141265869141000 550139.692999999970000000 5804601.201000000400000000 57.472141265869141000 550139.692999999970000000 5804601.201000000400000000 54.109999999999992000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_53bae0f2-4937-451a-8ce8-d16bc882403c">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_9892381f-e8ef-4a58-8842-8c07c33257f2" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_63952a5d-14fb-4997-8f48-6bf1d66cf9e3">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_f6e12f41-b097-4154-bd75-03b9fe4afe1e">
                      <gml:posList>550145.128000000140000000 5804600.807999999300000000 54.109999999999992000 550139.692999999970000000 5804601.201000000400000000 54.109999999999992000 550139.692999999970000000 5804601.201000000400000000 57.472141265869141000 550145.128000000140000000 5804600.807999999300000000 57.472141265869141000 550145.128000000140000000 5804600.807999999300000000 54.109999999999992000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_8c5372ac-45b6-433e-be97-def848bffcbc">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_654cf25c-3c59-4c62-91f8-39c1253dc598" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_0547506d-b0d2-4bdf-a19d-c4ad391291a8">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_1c2b9496-09b2-4d7b-b176-20cbb29fec6a">
                      <gml:posList>550145.359999999990000000 5804604.747000000400000000 54.109999999999992000 550145.128000000140000000 5804600.807999999300000000 54.109999999999992000 550145.128000000140000000 5804600.807999999300000000 57.472141265869141000 550145.359999999990000000 5804604.747000000400000000 57.472141265869141000 550145.359999999990000000 5804604.747000000400000000 54.109999999999992000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_fe6754f7-88c6-4ef0-9fa7-c0977893ca47">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_503685ab-c8bf-4aeb-8e54-131dde17087e" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_18a4e9a3-1293-4a07-92ff-3f9dc0a197ea">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_13852143-5111-465b-beb0-6d4716bbddab">
                      <gml:posList>550150.492000000200000000 5804604.292000000400000000 54.109999999999992000 550145.359999999990000000 5804604.747000000400000000 54.109999999999992000 550145.359999999990000000 5804604.747000000400000000 57.472141265869141000 550150.492000000200000000 5804604.292000000400000000 57.472141265869141000 550150.492000000200000000 5804604.292000000400000000 54.109999999999992000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="UUID_3461a4f8-33a2-424b-b874-d6718cd4bb55">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_336ef400-6e14-42a5-80e0-74c87ab61721" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_a1ae486c-5f11-46b6-8234-d6d4e842054d">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_9f6e745a-38e2-4536-93fb-cb7540c896f4">
                      <gml:posList>550150.415999999850000000 5804601.302999999400000000 54.109999999999999000 550150.287999999940000000 5804596.347000000100000000 54.109999999999992000 550145.472000000070000000 5804596.843999999600000000 54.109999999999999000 550142.037000000010000000 5804597.198999999100000000 54.109999999999999000 550139.547999999950000000 5804597.497999998700000000 54.109999999999992000 550139.692999999970000000 5804601.201000000400000000 54.109999999999992000 550145.128000000140000000 5804600.807999999300000000 54.109999999999992000 550145.359999999990000000 5804604.747000000400000000 54.109999999999992000 550150.492000000200000000 5804604.292000000400000000 54.109999999999992000 550150.415999999850000000 5804601.302999999400000000 54.109999999999999000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </cityObjectMember>

  <cityObjectMember>
    <bldg:Building gml:id="DENIAL43000052xG">
      <gml:boundedBy>
        <gml:Envelope srsDimension="3" srsName="urn:adv:crs:ETRS89_UTM32*DE_DHHN92_NH">
          <gml:lowerCorner>550144.875000 5804604.000000 54.089996
</gml:lowerCorner>
          <gml:upperCorner>550150.625000 5804610.500000 56.021534
</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <creationDate>2017-11-21</creationDate>
      <gen:doubleAttribute name="Volume">
        <gen:value>55.117</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Profile">
        <gen:value>Sig3d</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCK">
        <gen:value>10</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOECKE">
        <gen:value>111010</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STBEZNR">
        <gen:value>111</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="MBZNR">
        <gen:value>1102</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="WBZNR">
        <gen:value>1102</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNAM">
        <gen:value>Vahrenwald</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTNR">
        <gen:value>11</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNAM">
        <gen:value>Vahrenwald-List</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBEZ">
        <gen:value>02</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEMEINDE">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SCALE">
        <gen:value>5000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SHAPE_Area">
        <gen:value>7165.30049704715</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ALKISID">
        <gen:value>DENIAL43000052xG</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ZMIN">
        <gen:value>54.09</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="THICKNESS">
        <gen:value>0.1</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="TEXTURE">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="BUILDINGID">
        <gen:value>DENIAL43000052xG</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ARTBEZEICH">
        <gen:value>Gebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Join_Cou_1">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="FUNKTION_T">
        <gen:value>Gebäude für Wirtschaft oder Gewerbe</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="OBJFLAECHE">
        <gen:value>28.54</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADT">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEB_Funkti">
        <gen:value>2000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STRSCHL">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STR_komple">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="PLZ_NUMMER">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STR">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="HAUSNR">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="OVERHANG">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <app:appearance>
        <app:Appearance>
          <app:theme>RhinoCity ObliqueTexturing</app:theme>
          <app:surfaceDataMember>
            <app:ParameterizedTexture>
              <app:imageURI>Hannover_teil1_Appearance/DENIAL43000052xG.jpg</app:imageURI>
              <app:textureType>specific</app:textureType>
              <app:wrapMode>border</app:wrapMode>
              <app:borderColor>0 0 0 1</app:borderColor>
              <app:target uri="#UUID_187f8fd5-0fe0-4c29-b87b-47ead9e53c20">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_1fa3493e-92f4-4082-a5ee-07d07bc7d492">0.581696 0.007874 0.594400 0.514880 0.019326 0.592394 0.007874 0.058325 0.055641 0.054061 0.581696 0.007874 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_757313c8-5c87-42ee-b924-18c118d72a85">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_2e94a05f-be96-4832-85ae-93324e2d6dc5">0.739418 0.484978 0.661417 0.098306 0.690508 0.007874 0.768385 0.394395 0.739418 0.484978 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_a9a64111-b3d5-49e3-8eca-4459e2285657">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_1ed333a0-0e73-46a1-8730-91d7c3f2e3ac">0.531175 0.865899 0.066529 0.899222 0.024341 0.902298 0.007874 0.815912 0.050026 0.812837 0.514270 0.779528 0.531175 0.865899 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_f3f412b6-69e1-4a50-9a56-f5e9e2fddf9e">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_a78df016-6d43-4712-b5cf-d81611a5ebda">0.623444 0.546725 0.614173 0.013968 0.636344 0.007874 0.645627 0.541332 0.623444 0.546725 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_f7b512f8-ae79-4efa-8c68-a080cdc4a7a4">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_213fe286-defe-4d26-8016-d63730d7bee1">0.007874 0.668009 0.505870 0.614173 0.534308 0.703704 0.035890 0.757558 0.007874 0.668009 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
            </app:ParameterizedTexture>
          </app:surfaceDataMember>
        </app:Appearance>
      </app:appearance>
      <bldg:measuredHeight uom="#m">1.932</bldg:measuredHeight>
      <bldg:lod1Solid>
        <gml:Solid gml:id="UUID_2d11a979-c166-4e10-b217-932a93067a04">
          <gml:exterior>
            <gml:CompositeSurface gml:id="UUID_c1451ebf-16c0-41e9-86ad-d1ba47c5d42b">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_5bd18647-c6ee-4fc4-92cf-ac29afc0b9ed">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_a989c48d-9567-4fc0-bec8-e673b6f88fd5">
                      <gml:posList>550150.492000000200000000 5804604.292000000400000000 56.021530151367188000 550145.359999999990000000 5804604.747000000400000000 56.021530151367188000 550145.359999999990000000 5804604.747000000400000000 54.090000000000003000 550150.492000000200000000 5804604.292000000400000000 54.090000000000011000 550150.492000000200000000 5804604.292000000400000000 56.021530151367188000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_5a2cac23-4a75-48b8-b398-4459c97457fb">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_bcab19f4-873c-41c3-9e94-c5d058db3100">
                      <gml:posList>550145.359999999990000000 5804604.747000000400000000 56.021530151367188000 550144.893999999970000000 5804604.788999998900000000 56.021530151367188000 550144.893999999970000000 5804604.788999998900000000 54.090000000000011000 550145.359999999990000000 5804604.747000000400000000 54.090000000000003000 550145.359999999990000000 5804604.747000000400000000 56.021530151367188000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_ce971057-e7f4-4356-96a8-61d7e8da1577">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_14e9a68a-a4da-4918-8851-c72e2f7678ab">
                      <gml:posList>550145.010000000010000000 5804609.998999999800000000 54.090000000000011000 550144.893999999970000000 5804604.788999998900000000 54.090000000000011000 550144.893999999970000000 5804604.788999998900000000 56.021530151367188000 550145.010000000010000000 5804609.998999999800000000 56.021530151367188000 550145.010000000010000000 5804609.998999999800000000 54.090000000000011000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_245056be-cbf7-406d-a719-898d878159c4">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_bb55a2e0-a97a-4203-835f-ebacb801882b">
                      <gml:posList>550150.620000000000000000 5804609.237999999900000000 54.090000000000011000 550145.010000000010000000 5804609.998999999800000000 54.090000000000011000 550145.010000000010000000 5804609.998999999800000000 56.021530151367188000 550150.620000000000000000 5804609.237999999900000000 56.021530151367188000 550150.620000000000000000 5804609.237999999900000000 54.090000000000011000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_4416f1f0-0b2b-4ab1-b8a8-b6acfada725b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_310f56a3-b3e4-4ad7-a041-af0570e2723f">
                      <gml:posList>550150.620000000000000000 5804609.237999999900000000 56.021530151367188000 550150.492000000200000000 5804604.292000000400000000 56.021530151367188000 550150.492000000200000000 5804604.292000000400000000 54.090000000000011000 550150.620000000000000000 5804609.237999999900000000 54.090000000000011000 550150.620000000000000000 5804609.237999999900000000 56.021530151367188000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_745696b9-295e-4e84-ac22-c3df89c7ae83">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_6b764684-eea7-4850-8a36-3a67c9ac8fb9">
                      <gml:posList>550150.492000000200000000 5804604.292000000400000000 54.090000000000011000 550145.359999999990000000 5804604.747000000400000000 54.090000000000003000 550144.893999999970000000 5804604.788999998900000000 54.090000000000011000 550145.010000000010000000 5804609.998999999800000000 54.090000000000011000 550150.620000000000000000 5804609.237999999900000000 54.090000000000011000 550150.492000000200000000 5804604.292000000400000000 54.090000000000011000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_2a0d3392-8605-4adc-bb58-dcc79d24c9f8">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_f7f3e998-9bd6-4922-a101-b495d2c81eea">
                      <gml:posList>550150.492000000200000000 5804604.292000000400000000 56.021530151367188000 550150.620000000000000000 5804609.237999999900000000 56.021530151367188000 550145.010000000010000000 5804609.998999999800000000 56.021530151367188000 550144.893999999970000000 5804604.788999998900000000 56.021530151367188000 550145.359999999990000000 5804604.747000000400000000 56.021530151367188000 550150.492000000200000000 5804604.292000000400000000 56.021530151367188000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod1Solid>
      <bldg:lod2Solid>
        <gml:Solid gml:id="UUID_2c435598-a55a-413a-9021-ade8614b6805">
          <gml:exterior>
            <gml:CompositeSurface gml:id="UUID_c0388c5a-7ea6-4eed-9791-9e5cbdaabcee">
              <gml:surfaceMember xlink:href="#UUID_187f8fd5-0fe0-4c29-b87b-47ead9e53c20"/>
              <gml:surfaceMember xlink:href="#UUID_757313c8-5c87-42ee-b924-18c118d72a85"/>
              <gml:surfaceMember xlink:href="#UUID_a9a64111-b3d5-49e3-8eca-4459e2285657"/>
              <gml:surfaceMember xlink:href="#UUID_f3f412b6-69e1-4a50-9a56-f5e9e2fddf9e"/>
              <gml:surfaceMember xlink:href="#UUID_f7b512f8-ae79-4efa-8c68-a080cdc4a7a4"/>
              <gml:surfaceMember xlink:href="#UUID_a123c791-f6d1-4a79-98cb-0321563ae40f"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_9d92ba13-700e-4d46-9dbc-4b26a6e19195">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_0d7e7707-12b1-4a69-b9bb-b090a618523e" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_187f8fd5-0fe0-4c29-b87b-47ead9e53c20">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_1fa3493e-92f4-4082-a5ee-07d07bc7d492">
                      <gml:posList>550150.492000000200000000 5804604.292000000400000000 56.021530151367188000 550150.620000000000000000 5804609.237999999900000000 56.021530151367188000 550145.010000000010000000 5804609.998999999800000000 56.021530151367188000 550144.893999999970000000 5804604.788999998900000000 56.021530151367188000 550145.359999999990000000 5804604.747000000400000000 56.021530151367188000 550150.492000000200000000 5804604.292000000400000000 56.021530151367188000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_9e057596-801c-4ceb-9290-d8457f07c424">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_4038a25e-d2d5-474d-a268-f3dea7b7e03f" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_757313c8-5c87-42ee-b924-18c118d72a85">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_2e94a05f-be96-4832-85ae-93324e2d6dc5">
                      <gml:posList>550150.620000000000000000 5804609.237999999900000000 56.021530151367188000 550150.492000000200000000 5804604.292000000400000000 56.021530151367188000 550150.492000000200000000 5804604.292000000400000000 54.090000000000011000 550150.620000000000000000 5804609.237999999900000000 54.090000000000011000 550150.620000000000000000 5804609.237999999900000000 56.021530151367188000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_c9c34d89-75ad-4b52-8a5b-0a23546f1fc3">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_549df4b8-81d5-44ff-be3e-4cf0e35d2724" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_a9a64111-b3d5-49e3-8eca-4459e2285657">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_1ed333a0-0e73-46a1-8730-91d7c3f2e3ac">
                      <gml:posList>550150.492000000200000000 5804604.292000000400000000 56.021530151367188000 550145.359999999990000000 5804604.747000000400000000 56.021530151367188000 550144.893999999970000000 5804604.788999998900000000 56.021530151367188000 550144.893999999970000000 5804604.788999998900000000 54.090000000000011000 550145.359999999990000000 5804604.747000000400000000 54.090000000000003000 550150.492000000200000000 5804604.292000000400000000 54.090000000000011000 550150.492000000200000000 5804604.292000000400000000 56.021530151367188000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_c88e3f15-b20f-497d-9899-8d2539ee5195">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_de591750-e95e-41e9-beb6-e229939f3437" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_f3f412b6-69e1-4a50-9a56-f5e9e2fddf9e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_a78df016-6d43-4712-b5cf-d81611a5ebda">
                      <gml:posList>550145.010000000010000000 5804609.998999999800000000 54.090000000000011000 550144.893999999970000000 5804604.788999998900000000 54.090000000000011000 550144.893999999970000000 5804604.788999998900000000 56.021530151367188000 550145.010000000010000000 5804609.998999999800000000 56.021530151367188000 550145.010000000010000000 5804609.998999999800000000 54.090000000000011000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_fc979d5f-d9a1-462b-a8ac-ce2208824211">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_440f852f-db74-4a0c-8345-e69d1967f840" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_f7b512f8-ae79-4efa-8c68-a080cdc4a7a4">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_213fe286-defe-4d26-8016-d63730d7bee1">
                      <gml:posList>550150.620000000000000000 5804609.237999999900000000 54.090000000000011000 550145.010000000010000000 5804609.998999999800000000 54.090000000000011000 550145.010000000010000000 5804609.998999999800000000 56.021530151367188000 550150.620000000000000000 5804609.237999999900000000 56.021530151367188000 550150.620000000000000000 5804609.237999999900000000 54.090000000000011000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="UUID_6142df2f-43c3-4e50-8751-206eb56eab7f">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_814a6208-7142-4ef3-879f-d335a660f958" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_a123c791-f6d1-4a79-98cb-0321563ae40f">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_703c0cb6-daa7-460c-91cc-7e72ddaf347c">
                      <gml:posList>550150.492000000200000000 5804604.292000000400000000 54.090000000000011000 550145.359999999990000000 5804604.747000000400000000 54.090000000000003000 550144.893999999970000000 5804604.788999998900000000 54.090000000000011000 550145.010000000010000000 5804609.998999999800000000 54.090000000000011000 550150.620000000000000000 5804609.237999999900000000 54.090000000000011000 550150.492000000200000000 5804604.292000000400000000 54.090000000000011000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </cityObjectMember>

  <cityObjectMember>
    <bldg:Building gml:id="DENIAL43000053at">
      <gml:boundedBy>
        <gml:Envelope srsDimension="3" srsName="urn:adv:crs:ETRS89_UTM32*DE_DHHN92_NH">
          <gml:lowerCorner>550195.250000 5804590.500000 53.969997
</gml:lowerCorner>
          <gml:upperCorner>550203.937500 5804597.500000 60.816063
</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <creationDate>2017-11-21</creationDate>
      <gen:doubleAttribute name="Volume">
        <gen:value>292.732</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Profile">
        <gen:value>Sig3d</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCK">
        <gen:value>10</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOECKE">
        <gen:value>111010</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STBEZNR">
        <gen:value>111</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="MBZNR">
        <gen:value>1102</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="WBZNR">
        <gen:value>1102</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNAM">
        <gen:value>Vahrenwald</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTNR">
        <gen:value>11</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNAM">
        <gen:value>Vahrenwald-List</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBEZ">
        <gen:value>02</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEMEINDE">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SCALE">
        <gen:value>5000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SHAPE_Area">
        <gen:value>7165.30049704715</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ALKISID">
        <gen:value>DENIAL43000053at</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ZMIN">
        <gen:value>53.97</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="THICKNESS">
        <gen:value>0.1</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="TEXTURE">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="BUILDINGID">
        <gen:value>DENIAL43000053at</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ARTBEZEICH">
        <gen:value>Gebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Join_Cou_1">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="FUNKTION_T">
        <gen:value>Wohngebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="OBJFLAECHE">
        <gen:value>42.76</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADT">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEB_Funkti">
        <gen:value>1000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STRSCHL">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STR_komple">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="PLZ_NUMMER">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STR">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="HAUSNR">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="OVERHANG">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <app:appearance>
        <app:Appearance>
          <app:theme>RhinoCity ObliqueTexturing</app:theme>
          <app:surfaceDataMember>
            <app:ParameterizedTexture>
              <app:imageURI>Hannover_teil1_Appearance/DENIAL43000053at.jpg</app:imageURI>
              <app:textureType>specific</app:textureType>
              <app:wrapMode>border</app:wrapMode>
              <app:borderColor>0 0 0 1</app:borderColor>
              <app:target uri="#UUID_437279b1-c817-4d35-bc74-8f555cd7122c">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_b4af8ae7-d97f-4cf3-8043-9cb9879132f6">0.507239 0.003922 0.531221 0.291180 0.139460 0.326484 0.113725 0.044198 0.444456 0.010380 0.507239 0.003922 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_baf25a79-110f-407b-8116-e7bac7806e1b">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_03ed22f0-ee09-4500-993c-c76490aa9baf">0.391098 0.554431 0.335147 0.559332 0.040521 0.584980 0.003922 0.426585 0.297638 0.400972 0.353416 0.396078 0.391098 0.554431 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_85c9e2dd-de56-42e4-8e39-0fe65fe09a98">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_18a9199c-dc78-4569-803e-bdc3931bb05d">0.565906 0.298512 0.541176 0.017849 0.599843 0.003922 0.624689 0.285898 0.565906 0.298512 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_e092d752-1398-45d7-bfea-64d917b0eeb0">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_bfcfb3dd-91be-4143-98a2-d3e7f5e3048e">0.400000 0.421021 0.733816 0.396078 0.777341 0.559994 0.442532 0.584965 0.400000 0.421021 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_959a3488-d130-45bb-8ccf-0aa5c09a4955">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_b72eeb1c-b3b5-4dac-a6e6-ca4f2a6d9314">0.059015 0.386102 0.003922 0.164132 0.050534 0.003922 0.105355 0.225577 0.059015 0.386102 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
            </app:ParameterizedTexture>
          </app:surfaceDataMember>
        </app:Appearance>
      </app:appearance>
      <bldg:measuredHeight uom="#m">6.846</bldg:measuredHeight>
      <bldg:lod1Solid>
        <gml:Solid gml:id="UUID_fa8fe849-cb29-4a30-b182-c4c2412a50f2">
          <gml:exterior>
            <gml:CompositeSurface gml:id="UUID_d188a85b-a23a-4db2-821c-68045b02c298">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_1db50f97-7bc1-412c-8233-1011161f4638">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_6d56b6da-5a5f-4656-b900-c4130b032bea">
                      <gml:posList>550203.265999998900000000 5804590.866000000400000000 60.816059112548828000 550202.045000001790000000 5804590.992999999800000000 60.816059112548828000 550195.613000001760000000 5804591.657999999800000000 60.816059112548828000 550195.613000001760000000 5804591.657999999800000000 53.970000000000006000 550202.045000001790000000 5804590.992999999800000000 53.969999999999999000 550203.265999998900000000 5804590.866000000400000000 53.970000000000006000 550203.265999998900000000 5804590.866000000400000000 60.816059112548828000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_36a5f6f9-e776-4544-bd10-be1c369dbab5">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_4a3070da-9223-43a1-b640-9f7c202e288f">
                      <gml:posList>550196.118000000720000000 5804597.161999999500000000 53.970000000000006000 550195.613000001760000000 5804591.657999999800000000 53.970000000000006000 550195.613000001760000000 5804591.657999999800000000 60.816059112548828000 550196.118000000720000000 5804597.161999999500000000 60.816059112548828000 550196.118000000720000000 5804597.161999999500000000 53.970000000000006000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_cd3b59f2-60cf-4d49-99ca-90eaf598fd2d">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_e9e6d057-e739-4ddf-b7b4-51ef895215ae">
                      <gml:posList>550203.736999999730000000 5804596.467000000200000000 53.970000000000006000 550196.118000000720000000 5804597.161999999500000000 53.970000000000006000 550196.118000000720000000 5804597.161999999500000000 60.816059112548828000 550203.736999999730000000 5804596.467000000200000000 60.816059112548828000 550203.736999999730000000 5804596.467000000200000000 53.970000000000006000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_651c5674-9d9f-415b-91dc-31cc50693620">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_57af7129-fe9e-4bc1-bf21-64727a63e122">
                      <gml:posList>550203.736999999730000000 5804596.467000000200000000 60.816059112548828000 550203.265999998900000000 5804590.866000000400000000 60.816059112548828000 550203.265999998900000000 5804590.866000000400000000 53.970000000000006000 550203.736999999730000000 5804596.467000000200000000 53.970000000000006000 550203.736999999730000000 5804596.467000000200000000 60.816059112548828000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_e52dfb2d-3497-4bb4-acbc-9dbeaae2358c">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_ff2b5087-9ebd-4d29-add2-8a13cee6e129">
                      <gml:posList>550203.265999998900000000 5804590.866000000400000000 53.970000000000006000 550202.045000001790000000 5804590.992999999800000000 53.969999999999999000 550195.613000001760000000 5804591.657999999800000000 53.970000000000006000 550196.118000000720000000 5804597.161999999500000000 53.970000000000006000 550203.736999999730000000 5804596.467000000200000000 53.970000000000006000 550203.265999998900000000 5804590.866000000400000000 53.970000000000006000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_89146c37-a86d-4a5c-ab63-6f83246372fd">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_9b6cedf5-eb73-498b-883e-35f07bbee848">
                      <gml:posList>550203.265999998900000000 5804590.866000000400000000 60.816059112548828000 550203.736999999730000000 5804596.467000000200000000 60.816059112548828000 550196.118000000720000000 5804597.161999999500000000 60.816059112548828000 550195.613000001760000000 5804591.657999999800000000 60.816059112548828000 550202.045000001790000000 5804590.992999999800000000 60.816059112548828000 550203.265999998900000000 5804590.866000000400000000 60.816059112548828000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod1Solid>
      <bldg:lod2Solid>
        <gml:Solid gml:id="UUID_66292ff8-4237-472d-b886-a88da827e437">
          <gml:exterior>
            <gml:CompositeSurface gml:id="UUID_421b1587-385b-4332-922f-e8590d0eccc4">
              <gml:surfaceMember xlink:href="#UUID_437279b1-c817-4d35-bc74-8f555cd7122c"/>
              <gml:surfaceMember xlink:href="#UUID_baf25a79-110f-407b-8116-e7bac7806e1b"/>
              <gml:surfaceMember xlink:href="#UUID_85c9e2dd-de56-42e4-8e39-0fe65fe09a98"/>
              <gml:surfaceMember xlink:href="#UUID_e092d752-1398-45d7-bfea-64d917b0eeb0"/>
              <gml:surfaceMember xlink:href="#UUID_959a3488-d130-45bb-8ccf-0aa5c09a4955"/>
              <gml:surfaceMember xlink:href="#UUID_9c25b66a-47d7-4807-b91c-beadb9e26d10"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_f49b2891-fa61-40ed-9e22-584b0a2e110e">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_f0f7088c-5247-4cc1-be60-7f640281c77a" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_437279b1-c817-4d35-bc74-8f555cd7122c">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_b4af8ae7-d97f-4cf3-8043-9cb9879132f6">
                      <gml:posList>550203.265999998900000000 5804590.866000000400000000 60.816059112548828000 550203.736999999730000000 5804596.467000000200000000 60.816059112548828000 550196.118000000720000000 5804597.161999999500000000 60.816059112548828000 550195.613000001760000000 5804591.657999999800000000 60.816059112548828000 550202.045000001790000000 5804590.992999999800000000 60.816059112548828000 550203.265999998900000000 5804590.866000000400000000 60.816059112548828000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_24553d18-8bc2-4d43-9956-0fd6f59fd0ec">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_215a9043-bd9a-4c1e-a720-0bec7148fab6" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_baf25a79-110f-407b-8116-e7bac7806e1b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_03ed22f0-ee09-4500-993c-c76490aa9baf">
                      <gml:posList>550203.265999998900000000 5804590.866000000400000000 60.816059112548828000 550202.045000001790000000 5804590.992999999800000000 60.816059112548828000 550195.613000001760000000 5804591.657999999800000000 60.816059112548828000 550195.613000001760000000 5804591.657999999800000000 53.970000000000006000 550202.045000001790000000 5804590.992999999800000000 53.969999999999999000 550203.265999998900000000 5804590.866000000400000000 53.970000000000006000 550203.265999998900000000 5804590.866000000400000000 60.816059112548828000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_94b713ba-eb60-45af-869c-c32eca267e51">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_21026e1a-8de0-49bd-accd-41046c44d39c" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_85c9e2dd-de56-42e4-8e39-0fe65fe09a98">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_18a9199c-dc78-4569-803e-bdc3931bb05d">
                      <gml:posList>550196.118000000720000000 5804597.161999999500000000 53.970000000000006000 550195.613000001760000000 5804591.657999999800000000 53.970000000000006000 550195.613000001760000000 5804591.657999999800000000 60.816059112548828000 550196.118000000720000000 5804597.161999999500000000 60.816059112548828000 550196.118000000720000000 5804597.161999999500000000 53.970000000000006000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_9a8582d8-fb4f-435a-a924-bff811fb856e">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_e4c8fe51-7b1f-47aa-a149-0f7c9f3acb60" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_e092d752-1398-45d7-bfea-64d917b0eeb0">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_bfcfb3dd-91be-4143-98a2-d3e7f5e3048e">
                      <gml:posList>550203.736999999730000000 5804596.467000000200000000 53.970000000000006000 550196.118000000720000000 5804597.161999999500000000 53.970000000000006000 550196.118000000720000000 5804597.161999999500000000 60.816059112548828000 550203.736999999730000000 5804596.467000000200000000 60.816059112548828000 550203.736999999730000000 5804596.467000000200000000 53.970000000000006000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_a5a8e6d7-b5fd-42d2-a1eb-a04d58bc0665">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_4e623a24-eee4-47ce-8f38-b1ba1caa305f" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_959a3488-d130-45bb-8ccf-0aa5c09a4955">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_b72eeb1c-b3b5-4dac-a6e6-ca4f2a6d9314">
                      <gml:posList>550203.736999999730000000 5804596.467000000200000000 60.816059112548828000 550203.265999998900000000 5804590.866000000400000000 60.816059112548828000 550203.265999998900000000 5804590.866000000400000000 53.970000000000006000 550203.736999999730000000 5804596.467000000200000000 53.970000000000006000 550203.736999999730000000 5804596.467000000200000000 60.816059112548828000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="UUID_6d52ecc1-f607-4bce-99df-0231a7535bda">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_2a97d9e2-59c2-4291-9996-41255ca7e48f" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_9c25b66a-47d7-4807-b91c-beadb9e26d10">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_47913f43-c4d2-497a-9613-eba9d2cbe143">
                      <gml:posList>550203.265999998900000000 5804590.866000000400000000 53.970000000000006000 550202.045000001790000000 5804590.992999999800000000 53.969999999999999000 550195.613000001760000000 5804591.657999999800000000 53.970000000000006000 550196.118000000720000000 5804597.161999999500000000 53.970000000000006000 550203.736999999730000000 5804596.467000000200000000 53.970000000000006000 550203.265999998900000000 5804590.866000000400000000 53.970000000000006000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </cityObjectMember>

  <cityObjectMember>
    <bldg:Building gml:id="DENIAL43000052zA">
      <gml:boundedBy>
        <gml:Envelope srsDimension="3" srsName="urn:adv:crs:ETRS89_UTM32*DE_DHHN92_NH">
          <gml:lowerCorner>550141.937500 5804609.000000 54.029995
</gml:lowerCorner>
          <gml:upperCorner>550160.250000 5804624.000000 73.825859
</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <creationDate>2017-11-21</creationDate>
      <gen:doubleAttribute name="Volume">
        <gen:value>2890.163</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Profile">
        <gen:value>Sig3d</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCK">
        <gen:value>10</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOECKE">
        <gen:value>111010</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STBEZNR">
        <gen:value>111</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="MBZNR">
        <gen:value>1102</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="WBZNR">
        <gen:value>1102</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNAM">
        <gen:value>Vahrenwald</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTNR">
        <gen:value>11</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNAM">
        <gen:value>Vahrenwald-List</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBEZ">
        <gen:value>02</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEMEINDE">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SCALE">
        <gen:value>5000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SHAPE_Area">
        <gen:value>7165.30049704715</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ALKISID">
        <gen:value>DENIAL43000052zA</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ZMIN">
        <gen:value>54.03</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="THICKNESS">
        <gen:value>0.1</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="TEXTURE">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="BUILDINGID">
        <gen:value>DENIAL43000052zA</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ARTBEZEICH">
        <gen:value>Gebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Join_Cou_1">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="FUNKTION_T">
        <gen:value>Wohngebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="OBJFLAECHE">
        <gen:value>167.29</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADT">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEB_Funkti">
        <gen:value>1000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STRSCHL">
        <gen:value>02557</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STR_komple">
        <gen:value>Seydlitzstraße 30</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="PLZ_NUMMER">
        <gen:value>30161</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STR">
        <gen:value>Seydlitzstraße</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="HAUSNR">
        <gen:value>30</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="OVERHANG">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <app:appearance>
        <app:Appearance>
          <app:theme>RhinoCity ObliqueTexturing</app:theme>
          <app:surfaceDataMember>
            <app:ParameterizedTexture>
              <app:imageURI>Hannover_teil1_Appearance/DENIAL43000052zA.jpg</app:imageURI>
              <app:textureType>specific</app:textureType>
              <app:wrapMode>border</app:wrapMode>
              <app:borderColor>0 0 0 1</app:borderColor>
              <app:target uri="#UUID_d655bc30-3e35-4a53-ba06-4e6be5bff03b">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_53423f34-80bc-4e5c-9716-e382ddeff332">0.731059 0.001957 0.729674 0.197124 0.499022 0.221863 0.533782 0.064499 0.578176 0.017837 0.731059 0.001957 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_04797cdc-b8ba-445c-8b63-77b4229267d4">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_a00eb0a6-ae03-499f-8818-ce67405bd5ee">0.320965 0.754280 0.167974 0.770068 0.069370 0.673388 0.068493 0.669302 0.299964 0.637965 0.320965 0.754280 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_bdecb0cc-dcb9-4738-86d1-5a89f0990667">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_b0de222a-30e1-4bab-a6d8-dfd206ddb791">0.876176 0.412916 0.841398 0.569965 0.628180 0.592775 0.628371 0.458444 0.684283 0.450902 0.683836 0.432812 0.876176 0.412916 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_84468304-19e0-44b9-84e9-31c84542fceb">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_0511d2f1-7d5a-4962-b1a6-cfcf81e2aab5">0.529337 0.685255 0.337329 0.704808 0.326810 0.654813 0.475954 0.637965 0.529337 0.685255 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_dea49c12-15a3-4cda-8854-c4b637d81588">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_d13ec45d-d04c-4627-ade9-bd8e53cc11bc">0.934150 0.556157 0.881310 0.508165 0.893778 0.506659 0.880626 0.412916 0.978040 0.509709 0.934150 0.556157 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_b7cbecaf-284f-4055-a20d-25e346ba4150">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_bee5f49c-f20b-4383-bd60-36c3d60ee509">0.493563 0.207506 0.288932 0.233277 0.250489 0.025667 0.453888 0.001957 0.493563 0.207506 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_05c102aa-3381-4dda-985b-cb38c73d4b58">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_dcd4ecce-2cd4-4137-b093-b2371787fb6c">0.176714 0.001957 0.193492 0.069209 0.194233 0.072179 0.160896 0.284977 0.159990 0.282998 0.142857 0.216673 0.176714 0.001957 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_79cafa9e-c759-44c6-bfb1-2749c6c71f2a">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_85b22f09-e498-4e6c-a5f6-4c59af790575">0.180863 0.622787 0.170111 0.620201 0.039002 0.633690 0.001957 0.426719 0.132157 0.413902 0.142171 0.412916 0.180863 0.622787 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_e1826bd9-346f-469f-8569-e5b894f08111">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_72b91ce5-1914-4148-a409-5fdaf459f994">0.233134 0.001957 0.236107 0.014721 0.244707 0.051647 0.212446 0.263191 0.201683 0.239694 0.199609 0.220806 0.233134 0.001957 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_a953caa9-07dc-4bbe-89e8-3b7a7294f0c5">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_ddd7850e-4899-46a7-b91b-6dc9a160f6d5">0.821659 0.216424 0.773219 0.221543 0.735812 0.007561 0.783773 0.001957 0.821659 0.216424 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_40b09597-93e6-459c-822d-af38602e0f42">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_c64f46ce-b700-4352-9795-858d1562dbcd">0.014575 0.778591 0.001957 0.648034 0.064347 0.637965 0.064160 0.771896 0.014575 0.778591 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_718d33f0-30f4-4a6c-b447-e13a0931e3d7">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_def74b07-e521-4797-816d-a5abffeea314">0.185910 0.447297 0.385140 0.429434 0.569361 0.412916 0.623582 0.585694 0.438181 0.602220 0.237692 0.620149 0.185910 0.447297 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_10866ff3-8c42-4212-b30f-973550f9b5b6">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_edae2742-16e2-4afd-ab45-02613dba6a8d">0.083166 0.407155 0.019487 0.326342 0.001957 0.208552 0.069504 0.001957 0.095752 0.091592 0.138118 0.236260 0.083166 0.407155 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
            </app:ParameterizedTexture>
          </app:surfaceDataMember>
        </app:Appearance>
      </app:appearance>
      <bldg:measuredHeight uom="#m">19.796</bldg:measuredHeight>
      <bldg:lod1Solid>
        <gml:Solid gml:id="UUID_5e4c50fd-a07c-4769-9c93-379bde3fc7f6">
          <gml:exterior>
            <gml:CompositeSurface gml:id="UUID_e5fcc93a-323f-4f60-a382-aceeede66acb">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_84bcae4d-eb38-4b68-8a0c-c93deae68ccc">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_513ab95e-5e6a-474c-aa6e-a4e0653d163e">
                      <gml:posList>550150.458000000570000000 5804614.563000000100000000 54.030000000000001000 550149.954276161500000000 5804610.920099844200000000 54.030000000000001000 550149.932000000030000000 5804610.758999999600000000 54.030000000000001000 550149.932000000030000000 5804610.758999999600000000 73.825853309865707000 550149.954276161500000000 5804610.920099844200000000 73.825853309865707000 550150.458000000570000000 5804614.563000000100000000 73.825853309865707000 550150.458000000570000000 5804614.563000000100000000 54.030000000000001000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_aeea1e45-076e-438c-b993-8210c140ab0b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_6959f328-a32e-4f3d-8902-2a2389e2a41c">
                      <gml:posList>550150.458000000570000000 5804614.563000000100000000 73.825853309865707000 550150.013685075680000000 5804614.614549956300000000 73.825853309865707000 550144.234999999400000000 5804615.285000000100000000 73.825853309865707000 550144.234999999400000000 5804615.285000000100000000 54.030000000000001000 550150.013685075680000000 5804614.614549956300000000 54.030000000000001000 550150.458000000570000000 5804614.563000000100000000 54.030000000000001000 550150.458000000570000000 5804614.563000000100000000 73.825853309865707000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_f64e5da7-c6e6-4bb7-9780-4bb59d447c36">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_8b3f0bd6-e5d4-4d00-b16e-78db12e9ab6a">
                      <gml:posList>550144.576000001280000000 5804617.969999999700000000 54.030000000000001000 550144.488471090560000000 5804617.280806087900000000 54.030000000000001000 550144.234999999400000000 5804615.285000000100000000 54.030000000000001000 550144.234999999400000000 5804615.285000000100000000 73.825853309865707000 550144.488471090560000000 5804617.280806087900000000 73.825853309865707000 550144.576000001280000000 5804617.969999999700000000 73.825853309865707000 550144.576000001280000000 5804617.969999999700000000 54.030000000000001000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_e1221add-6e07-4643-bda4-f403de244688">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_1489cf57-45de-482c-9391-9084fc7a9fd2">
                      <gml:posList>550144.576000001280000000 5804617.969999999700000000 73.825853309865707000 550142.423000000420000000 5804618.263000000300000000 73.825853309865707000 550142.423000000420000000 5804618.263000000300000000 54.030000000000001000 550144.576000001280000000 5804617.969999999700000000 54.030000000000001000 550144.576000001280000000 5804617.969999999700000000 73.825853309865707000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_01db0d4e-31b3-4b48-83fb-08ecc186019f">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_79c857fa-58c8-4382-bf80-b74e0f53c4af">
                      <gml:posList>550142.932000000030000000 5804623.396999999900000000 54.030000000000001000 550142.423000000420000000 5804618.263000000300000000 54.030000000000001000 550142.423000000420000000 5804618.263000000300000000 73.825853309865707000 550142.932000000030000000 5804623.396999999900000000 73.825853309865707000 550142.932000000030000000 5804623.396999999900000000 54.030000000000001000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_b0ccf3f0-2698-49ae-8390-4839fc55c2c5">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_c4c426eb-2631-42e9-ab00-2d333139b171">
                      <gml:posList>550160.120000001040000000 5804621.537999999700000000 54.030000000000001000 550151.186466275370000000 5804622.504222898700000000 54.030000000000001000 550142.932000000030000000 5804623.396999999900000000 54.030000000000001000 550142.932000000030000000 5804623.396999999900000000 73.825853309865707000 550151.186466275370000000 5804622.504222898700000000 73.825853309865707000 550160.120000001040000000 5804621.537999999700000000 73.825853309865707000 550160.120000001040000000 5804621.537999999700000000 54.030000000000001000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_a8ebf9fd-db6a-4a50-b42a-2f4e7d05d9b4">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_8037a029-7939-4fdd-a627-8b0a21d9d390">
                      <gml:posList>550160.120000001040000000 5804621.537999999700000000 73.825853309865707000 550159.362824037090000000 5804614.109026771000000000 73.825853309865707000 550158.896000001580000000 5804609.527999999900000000 73.825853309865707000 550158.896000001580000000 5804609.527999999900000000 54.030000000000001000 550159.362824037090000000 5804614.109026771000000000 54.030000000000001000 550160.120000001040000000 5804621.537999999700000000 54.030000000000001000 550160.120000001040000000 5804621.537999999700000000 73.825853309865707000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_cc50794f-264e-47c0-a1fe-2536f90045e1">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_6eaf54c4-3d94-4966-8b68-4fd313e8666a">
                      <gml:posList>550158.896000001580000000 5804609.527999999900000000 73.825853309865707000 550149.932000000030000000 5804610.758999999600000000 73.825853309865707000 550149.932000000030000000 5804610.758999999600000000 54.030000000000001000 550158.896000001580000000 5804609.527999999900000000 54.030000000000001000 550158.896000001580000000 5804609.527999999900000000 73.825853309865707000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_15f864f9-47d8-4762-85fd-abb59c715655">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_e2559f4a-e416-4116-98ed-03afacfb789c">
                      <gml:posList>550149.954276161500000000 5804610.920099844200000000 54.030000000000001000 550150.458000000570000000 5804614.563000000100000000 54.030000000000001000 550150.013685075680000000 5804614.614549956300000000 54.030000000000001000 550144.234999999400000000 5804615.285000000100000000 54.030000000000001000 550144.488471090560000000 5804617.280806087900000000 54.030000000000001000 550144.576000001280000000 5804617.969999999700000000 54.030000000000001000 550142.423000000420000000 5804618.263000000300000000 54.030000000000001000 550142.932000000030000000 5804623.396999999900000000 54.030000000000001000 550151.186466275370000000 5804622.504222898700000000 54.030000000000001000 550160.120000001040000000 5804621.537999999700000000 54.030000000000001000 550159.362824037090000000 5804614.109026771000000000 54.030000000000001000 550158.896000001580000000 5804609.527999999900000000 54.030000000000001000 550149.932000000030000000 5804610.758999999600000000 54.030000000000001000 550149.954276161500000000 5804610.920099844200000000 54.030000000000001000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_baf3380c-244b-4628-98fd-11570ba48f4e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_fbd118d8-ae8f-4af4-ace1-f3f267d44831">
                      <gml:posList>550149.954276161500000000 5804610.920099844200000000 73.825853309865707000 550149.932000000030000000 5804610.758999999600000000 73.825853309865707000 550158.896000001580000000 5804609.527999999900000000 73.825853309865707000 550159.362824037090000000 5804614.109026771000000000 73.825853309865707000 550160.120000001040000000 5804621.537999999700000000 73.825853309865707000 550151.186466275370000000 5804622.504222898700000000 73.825853309865707000 550142.932000000030000000 5804623.396999999900000000 73.825853309865707000 550142.423000000420000000 5804618.263000000300000000 73.825853309865707000 550144.576000001280000000 5804617.969999999700000000 73.825853309865707000 550144.488471090560000000 5804617.280806087900000000 73.825853309865707000 550144.234999999400000000 5804615.285000000100000000 73.825853309865707000 550150.013685075680000000 5804614.614549956300000000 73.825853309865707000 550150.458000000570000000 5804614.563000000100000000 73.825853309865707000 550149.954276161500000000 5804610.920099844200000000 73.825853309865707000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod1Solid>
      <bldg:lod2Solid>
        <gml:Solid gml:id="UUID_857f1ba0-a8c0-47d3-8b7d-d71d7b8eef04">
          <gml:exterior>
            <gml:CompositeSurface gml:id="UUID_c29ea3d1-d265-4e06-99fd-f5cea7d3260a">
              <gml:surfaceMember xlink:href="#UUID_d655bc30-3e35-4a53-ba06-4e6be5bff03b"/>
              <gml:surfaceMember xlink:href="#UUID_04797cdc-b8ba-445c-8b63-77b4229267d4"/>
              <gml:surfaceMember xlink:href="#UUID_bdecb0cc-dcb9-4738-86d1-5a89f0990667"/>
              <gml:surfaceMember xlink:href="#UUID_84468304-19e0-44b9-84e9-31c84542fceb"/>
              <gml:surfaceMember xlink:href="#UUID_dea49c12-15a3-4cda-8854-c4b637d81588"/>
              <gml:surfaceMember xlink:href="#UUID_b7cbecaf-284f-4055-a20d-25e346ba4150"/>
              <gml:surfaceMember xlink:href="#UUID_05c102aa-3381-4dda-985b-cb38c73d4b58"/>
              <gml:surfaceMember xlink:href="#UUID_79cafa9e-c759-44c6-bfb1-2749c6c71f2a"/>
              <gml:surfaceMember xlink:href="#UUID_e1826bd9-346f-469f-8569-e5b894f08111"/>
              <gml:surfaceMember xlink:href="#UUID_a953caa9-07dc-4bbe-89e8-3b7a7294f0c5"/>
              <gml:surfaceMember xlink:href="#UUID_40b09597-93e6-459c-822d-af38602e0f42"/>
              <gml:surfaceMember xlink:href="#UUID_718d33f0-30f4-4a6c-b447-e13a0931e3d7"/>
              <gml:surfaceMember xlink:href="#UUID_10866ff3-8c42-4212-b30f-973550f9b5b6"/>
              <gml:surfaceMember xlink:href="#UUID_9cb4025e-de2f-4d63-b988-3de89415c096"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_8e2767ce-3279-4ca1-8113-22743297350e">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_41120eff-743f-42c0-bf9c-6945b2512b39" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_d655bc30-3e35-4a53-ba06-4e6be5bff03b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_53423f34-80bc-4e5c-9716-e382ddeff332">
                      <gml:posList>550159.362824037090000000 5804614.109026771000000000 73.753865264201210000 550160.120000001040000000 5804621.537999999700000000 68.388206896882309000 550151.186466275370000000 5804622.504222898700000000 68.386627437028778000 550151.915704626710000000 5804616.503690140300000000 72.617213471829743000 550153.461132392170000000 5804614.727471353500000000 73.767012624824901000 550159.362824037090000000 5804614.109026771000000000 73.753865264201210000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_f16705da-3cfc-478f-978e-d7b41fe4b026">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_07fbf8dc-c41f-4cc8-8967-0edabb4436cc" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_04797cdc-b8ba-445c-8b63-77b4229267d4">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_a00eb0a6-ae03-499f-8818-ce67405bd5ee">
                      <gml:posList>550159.362824037090000000 5804614.109026771000000000 73.753865264201210000 550153.461132392170000000 5804614.727471353500000000 73.767012624824901000 550149.954276161500000000 5804610.920099844200000000 71.680685298420173000 550149.932000000030000000 5804610.758999999600000000 71.598755101252365000 550158.896000001580000000 5804609.527999999900000000 71.432491954975205000 550159.362824037090000000 5804614.109026771000000000 73.753865264201210000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_7ac4ee91-87b1-462a-af06-829b62af11ce">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_2b61f925-90ac-4a4a-8857-242529de62f2" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_bdecb0cc-dcb9-4738-86d1-5a89f0990667">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_b0de222a-30e1-4bab-a6d8-dfd206ddb791">
                      <gml:posList>550151.915704626710000000 5804616.503690140300000000 72.617213471829743000 550151.186466275370000000 5804622.504222898700000000 68.386627437028778000 550142.932000000030000000 5804623.396999999900000000 68.389364441017150000 550142.423000000420000000 5804618.263000000300000000 72.096936645952667000 550144.576000001280000000 5804617.969999999700000000 72.139189551004677000 550144.488471090560000000 5804617.280806087900000000 72.638388247434335000 550151.915704626710000000 5804616.503690140300000000 72.617213471829743000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_83a656fb-b85a-4bb9-8767-d3a53ea1df51">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_2b485c5d-968e-4151-814e-019191a58e91" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_84468304-19e0-44b9-84e9-31c84542fceb">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_0511d2f1-7d5a-4962-b1a6-cfcf81e2aab5">
                      <gml:posList>550151.915704626710000000 5804616.503690140300000000 72.617213471829743000 550144.488471090560000000 5804617.280806087900000000 72.638388247434335000 550144.234999999400000000 5804615.285000000100000000 71.526605010685472000 550150.013685075680000000 5804614.614549956300000000 71.473920931971833000 550151.915704626710000000 5804616.503690140300000000 72.617213471829743000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_0cc27ee0-0955-4cd2-98e3-f949bdc01870">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_71cbef54-a3f4-4973-86f4-1695d06ad99d" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_dea49c12-15a3-4cda-8854-c4b637d81588">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_d13ec45d-d04c-4627-ade9-bd8e53cc11bc">
                      <gml:posList>550151.915704626710000000 5804616.503690140300000000 72.617213471829743000 550150.013685075680000000 5804614.614549956300000000 71.473920931971833000 550150.458000000570000000 5804614.563000000100000000 71.773868872524815000 550149.954276161500000000 5804610.920099844200000000 71.680685298420173000 550153.461132392170000000 5804614.727471353500000000 73.767012624824901000 550151.915704626710000000 5804616.503690140300000000 72.617213471829743000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_556a9b1b-cfc1-4c21-80bd-bdea1f50c39a">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_e449eca8-f1ed-472e-83dc-29f00c66f2c0" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_b7cbecaf-284f-4055-a20d-25e346ba4150">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_bee5f49c-f20b-4383-bd60-36c3d60ee509">
                      <gml:posList>550158.896000001580000000 5804609.527999999900000000 71.432491954975205000 550149.932000000030000000 5804610.758999999600000000 71.598755101252365000 550149.932000000030000000 5804610.758999999600000000 54.030000000000001000 550158.896000001580000000 5804609.527999999900000000 54.030000000000001000 550158.896000001580000000 5804609.527999999900000000 71.432491954975205000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_453985eb-cca4-498f-9a3b-9e9ce383ac5f">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_7f053d5e-cb29-447b-9f0e-bb4a9ddfd89f" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_05c102aa-3381-4dda-985b-cb38c73d4b58">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_dcd4ecce-2cd4-4137-b093-b2371787fb6c">
                      <gml:posList>550150.458000000570000000 5804614.563000000100000000 54.030000000000001000 550149.954276161500000000 5804610.920099844200000000 54.030000000000001000 550149.932000000030000000 5804610.758999999600000000 54.030000000000001000 550149.932000000030000000 5804610.758999999600000000 71.598755101252365000 550149.954276161500000000 5804610.920099844200000000 71.680685298420173000 550150.458000000570000000 5804614.563000000100000000 71.773868872524815000 550150.458000000570000000 5804614.563000000100000000 54.030000000000001000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_a6a1f489-1071-4f30-98f1-5681b901feb3">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_19d13133-c08e-49f7-b374-55ee7c4837c1" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_79cafa9e-c759-44c6-bfb1-2749c6c71f2a">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_85b22f09-e498-4e6c-a5f6-4c59af790575">
                      <gml:posList>550150.458000000570000000 5804614.563000000100000000 71.773868872524815000 550150.013685075680000000 5804614.614549956300000000 71.473920931971833000 550144.234999999400000000 5804615.285000000100000000 71.526605010685472000 550144.234999999400000000 5804615.285000000100000000 54.030000000000001000 550150.013685075680000000 5804614.614549956300000000 54.030000000000001000 550150.458000000570000000 5804614.563000000100000000 54.030000000000001000 550150.458000000570000000 5804614.563000000100000000 71.773868872524815000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_2bbb0597-3cee-44d3-80bc-21c596055ed4">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_6c9ee078-603e-4792-ba85-9722c6659040" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_e1826bd9-346f-469f-8569-e5b894f08111">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_72b91ce5-1914-4148-a409-5fdaf459f994">
                      <gml:posList>550144.576000001280000000 5804617.969999999700000000 54.030000000000001000 550144.488471090560000000 5804617.280806087900000000 54.030000000000001000 550144.234999999400000000 5804615.285000000100000000 54.030000000000001000 550144.234999999400000000 5804615.285000000100000000 71.526605010685472000 550144.488471090560000000 5804617.280806087900000000 72.638388247434335000 550144.576000001280000000 5804617.969999999700000000 72.139189551004677000 550144.576000001280000000 5804617.969999999700000000 54.030000000000001000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_8d93f28a-4e6f-4f73-be55-efb6fb273458">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_70ffb6b5-281e-4637-9120-a82409907a55" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_a953caa9-07dc-4bbe-89e8-3b7a7294f0c5">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_ddd7850e-4899-46a7-b91b-6dc9a160f6d5">
                      <gml:posList>550144.576000001280000000 5804617.969999999700000000 72.139189551004677000 550142.423000000420000000 5804618.263000000300000000 72.096936645952667000 550142.423000000420000000 5804618.263000000300000000 54.030000000000001000 550144.576000001280000000 5804617.969999999700000000 54.030000000000001000 550144.576000001280000000 5804617.969999999700000000 72.139189551004677000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_92e24819-fd73-46c1-aad5-6747f78b6402">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_3bfac1be-e48d-4d66-bcbd-aeabf3b634ff" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_40b09597-93e6-459c-822d-af38602e0f42">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_c64f46ce-b700-4352-9795-858d1562dbcd">
                      <gml:posList>550142.932000000030000000 5804623.396999999900000000 54.030000000000001000 550142.423000000420000000 5804618.263000000300000000 54.030000000000001000 550142.423000000420000000 5804618.263000000300000000 72.096936645952667000 550142.932000000030000000 5804623.396999999900000000 68.389364441017150000 550142.932000000030000000 5804623.396999999900000000 54.030000000000001000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_1dd6db77-59b2-4362-ab4d-e1a6b711d05e">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_f9b4a2fa-9892-474f-a39e-bbb5c5634238" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_718d33f0-30f4-4a6c-b447-e13a0931e3d7">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_def74b07-e521-4797-816d-a5abffeea314">
                      <gml:posList>550160.120000001040000000 5804621.537999999700000000 54.030000000000001000 550151.186466275370000000 5804622.504222898700000000 54.030000000000001000 550142.932000000030000000 5804623.396999999900000000 54.030000000000001000 550142.932000000030000000 5804623.396999999900000000 68.389364441017150000 550151.186466275370000000 5804622.504222898700000000 68.386627437028778000 550160.120000001040000000 5804621.537999999700000000 68.388206896882309000 550160.120000001040000000 5804621.537999999700000000 54.030000000000001000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_ce9770eb-b07b-4af5-aef1-7cada85e0b1e">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_741a30e3-04b5-4b2a-b0b5-12952207de70" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_10866ff3-8c42-4212-b30f-973550f9b5b6">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_edae2742-16e2-4afd-ab45-02613dba6a8d">
                      <gml:posList>550160.120000001040000000 5804621.537999999700000000 68.388206896882309000 550159.362824037090000000 5804614.109026771000000000 73.753865264201210000 550158.896000001580000000 5804609.527999999900000000 71.432491954975205000 550158.896000001580000000 5804609.527999999900000000 54.030000000000001000 550159.362824037090000000 5804614.109026771000000000 54.030000000000001000 550160.120000001040000000 5804621.537999999700000000 54.030000000000001000 550160.120000001040000000 5804621.537999999700000000 68.388206896882309000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="UUID_874daae5-99f1-486e-b50c-2ae0878742ab">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_b00ccdae-80a6-4a5e-8f0b-9721fc2eff80" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_9cb4025e-de2f-4d63-b988-3de89415c096">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_f97bbf1b-a396-488f-aefe-358b447ff3d5">
                      <gml:posList>550149.954276161500000000 5804610.920099844200000000 54.030000000000001000 550150.458000000570000000 5804614.563000000100000000 54.030000000000001000 550150.013685075680000000 5804614.614549956300000000 54.030000000000001000 550144.234999999400000000 5804615.285000000100000000 54.030000000000001000 550144.488471090560000000 5804617.280806087900000000 54.030000000000001000 550144.576000001280000000 5804617.969999999700000000 54.030000000000001000 550142.423000000420000000 5804618.263000000300000000 54.030000000000001000 550142.932000000030000000 5804623.396999999900000000 54.030000000000001000 550151.186466275370000000 5804622.504222898700000000 54.030000000000001000 550160.120000001040000000 5804621.537999999700000000 54.030000000000001000 550159.362824037090000000 5804614.109026771000000000 54.030000000000001000 550158.896000001580000000 5804609.527999999900000000 54.030000000000001000 550149.932000000030000000 5804610.758999999600000000 54.030000000000001000 550149.954276161500000000 5804610.920099844200000000 54.030000000000001000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </cityObjectMember>

  <cityObjectMember>
    <bldg:Building gml:id="DENIAL43000053Cn">
      <gml:boundedBy>
        <gml:Envelope srsDimension="3" srsName="urn:adv:crs:ETRS89_UTM32*DE_DHHN92_NH">
          <gml:lowerCorner>550136.437500 5804616.000000 54.019997
</gml:lowerCorner>
          <gml:upperCorner>550142.875000 5804623.500000 56.952572
</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <creationDate>2017-11-21</creationDate>
      <gen:doubleAttribute name="Volume">
        <gen:value>109.716</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Profile">
        <gen:value>Sig3d</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOCK">
        <gen:value>10</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="BAUBLOECKE">
        <gen:value>111010</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STBEZNR">
        <gen:value>111</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="MBZNR">
        <gen:value>1102</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="WBZNR">
        <gen:value>1102</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTLNAM">
        <gen:value>Vahrenwald</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTTNR">
        <gen:value>11</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBZNAM">
        <gen:value>Vahrenwald-List</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADTBEZ">
        <gen:value>02</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEMEINDE">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SCALE">
        <gen:value>5000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="SHAPE_Area">
        <gen:value>7165.30049704715</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ALKISID">
        <gen:value>DENIAL43000053Cn</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ZMIN">
        <gen:value>54.02</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="THICKNESS">
        <gen:value>0.1</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="TEXTURE">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <gen:stringAttribute name="BUILDINGID">
        <gen:value>DENIAL43000053Cn</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="ARTBEZEICH">
        <gen:value>Gebäude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Join_Cou_1">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="FUNKTION_T">
        <gen:value>Gebäude zur Versorgung</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="OBJFLAECHE">
        <gen:value>37.41</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STADT">
        <gen:value>Hannover</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="GEB_Funkti">
        <gen:value>2500</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STRSCHL">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STR_komple">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="PLZ_NUMMER">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="STR">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="HAUSNR">
        <gen:value> </gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="OVERHANG">
        <gen:value>1</gen:value>
      </gen:intAttribute>
      <app:appearance>
        <app:Appearance>
          <app:theme>RhinoCity ObliqueTexturing</app:theme>
          <app:surfaceDataMember>
            <app:ParameterizedTexture>
              <app:imageURI>Hannover_teil1_Appearance/DENIAL43000053Cn.jpg</app:imageURI>
              <app:textureType>specific</app:textureType>
              <app:wrapMode>border</app:wrapMode>
              <app:borderColor>0 0 0 1</app:borderColor>
              <app:target uri="#UUID_844da1b4-370a-4af4-b264-a4368ed2cd62">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_31933356-0afb-4095-9540-5beb80d0c1b6">0.619908 0.104622 0.661358 0.314522 0.014534 0.351206 0.007874 0.030288 0.599946 0.003922 0.619908 0.104622 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_4e3323ff-7bf6-4294-83c6-e28fdb3ca29f">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_6ad47c3f-d15e-418f-b4f5-29216354d88d">0.870256 0.307572 0.777132 0.149070 0.732283 0.072844 0.778224 0.003922 0.822979 0.080103 0.915908 0.238513 0.870256 0.307572 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_c55a8ddb-3a08-4c5f-936b-7b131571f110">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_1eda4a22-47cf-4fd3-8180-fbefdf833aba">0.551566 0.533338 0.031680 0.552438 0.007874 0.485756 0.527082 0.466667 0.551566 0.533338 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_ab50c40e-d07b-43dc-b4fe-1288b8763d9b">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_b8ce0c8a-01b9-4324-8159-deabe4d70ec7">0.682771 0.327338 0.677165 0.007317 0.710791 0.003922 0.716408 0.324582 0.682771 0.327338 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
              <app:target uri="#UUID_415b3a5a-86a9-4b84-b913-f9293c0b2480">
                <app:TexCoordList>
                  <app:textureCoordinates ring="#UUID_e1b9e3a4-776a-411e-8196-3b8e9b2be71a">0.007874 0.386771 0.569345 0.360784 0.614007 0.429083 0.051811 0.455083 0.007874 0.386771 </app:textureCoordinates>
                </app:TexCoordList>
              </app:target>
            </app:ParameterizedTexture>
          </app:surfaceDataMember>
        </app:Appearance>
      </app:appearance>
      <bldg:measuredHeight uom="#m">2.933</bldg:measuredHeight>
      <bldg:lod1Solid>
        <gml:Solid gml:id="UUID_140224f3-60e6-433a-86f3-1aeba1fe9c2c">
          <gml:exterior>
            <gml:CompositeSurface gml:id="UUID_ec8351fc-4824-4a91-91fa-bd2574a8f00f">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_3e26eca2-6385-4678-a281-8b3f6d476e5d">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_64b4b93e-4d20-42a7-bd5b-d81e97fac4b6">
                      <gml:posList>550142.829999999960000000 5804622.362999999900000000 56.952568054199219000 550142.422999999950000000 5804618.263000000300000000 56.952568054199219000 550142.227000000070000000 5804616.296000000100000000 56.952568054199219000 550142.227000000070000000 5804616.296000000100000000 54.020000000000010000 550142.422999999950000000 5804618.263000000300000000 54.020000000000003000 550142.829999999960000000 5804622.362999999900000000 54.020000000000010000 550142.829999999960000000 5804622.362999999900000000 56.952568054199219000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_cef896ce-d019-4a5a-ada3-37b6b2a5b37b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_f75cb726-8a15-4c60-af61-4d96d4a8ae02">
                      <gml:posList>550142.227000000070000000 5804616.296000000100000000 56.952568054199219000 550136.462000000060000000 5804616.816000000600000000 56.952568054199219000 550136.462000000060000000 5804616.816000000600000000 54.020000000000010000 550142.227000000070000000 5804616.296000000100000000 54.020000000000010000 550142.227000000070000000 5804616.296000000100000000 56.952568054199219000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_5378d0fa-58fc-45a1-96c6-86213b4c3338">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_0397ec7f-1d96-4d4b-895a-432f048d65c4">
                      <gml:posList>550136.532000000010000000 5804623.085000000900000000 54.020000000000010000 550136.462000000060000000 5804616.816000000600000000 54.020000000000010000 550136.462000000060000000 5804616.816000000600000000 56.952568054199219000 550136.532000000010000000 5804623.085000000900000000 56.952568054199219000 550136.532000000010000000 5804623.085000000900000000 54.020000000000010000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_fd0ee4ab-338d-4c02-88ed-be44bdda125e">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_25ace3fb-f979-4cac-afdd-2dc7bcc221ac">
                      <gml:posList>550142.829999999960000000 5804622.362999999900000000 54.020000000000010000 550136.532000000010000000 5804623.085000000900000000 54.020000000000010000 550136.532000000010000000 5804623.085000000900000000 56.952568054199219000 550142.829999999960000000 5804622.362999999900000000 56.952568054199219000 550142.829999999960000000 5804622.362999999900000000 54.020000000000010000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_368a04e5-0a13-4810-b770-3252e3492f29">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_8f52d077-28d5-4755-b883-8469e715ce18">
                      <gml:posList>550142.422999999950000000 5804618.263000000300000000 54.020000000000003000 550142.227000000070000000 5804616.296000000100000000 54.020000000000010000 550136.462000000060000000 5804616.816000000600000000 54.020000000000010000 550136.532000000010000000 5804623.085000000900000000 54.020000000000010000 550142.829999999960000000 5804622.362999999900000000 54.020000000000010000 550142.422999999950000000 5804618.263000000300000000 54.020000000000003000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_160c859d-602a-44bc-a623-2fd68e341c4a">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_e1453446-713f-445f-8637-1d3ac9270606">
                      <gml:posList>550142.422999999950000000 5804618.263000000300000000 56.952568054199219000 550142.829999999960000000 5804622.362999999900000000 56.952568054199219000 550136.532000000010000000 5804623.085000000900000000 56.952568054199219000 550136.462000000060000000 5804616.816000000600000000 56.952568054199219000 550142.227000000070000000 5804616.296000000100000000 56.952568054199219000 550142.422999999950000000 5804618.263000000300000000 56.952568054199219000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod1Solid>
      <bldg:lod2Solid>
        <gml:Solid gml:id="UUID_31189dc9-8e41-4776-b357-c029e3b99101">
          <gml:exterior>
            <gml:CompositeSurface gml:id="UUID_da214b0e-b192-4661-bbed-0d50b5be3f7a">
              <gml:surfaceMember xlink:href="#UUID_844da1b4-370a-4af4-b264-a4368ed2cd62"/>
              <gml:surfaceMember xlink:href="#UUID_4e3323ff-7bf6-4294-83c6-e28fdb3ca29f"/>
              <gml:surfaceMember xlink:href="#UUID_c55a8ddb-3a08-4c5f-936b-7b131571f110"/>
              <gml:surfaceMember xlink:href="#UUID_ab50c40e-d07b-43dc-b4fe-1288b8763d9b"/>
              <gml:surfaceMember xlink:href="#UUID_415b3a5a-86a9-4b84-b913-f9293c0b2480"/>
              <gml:surfaceMember xlink:href="#UUID_b7f89126-2c88-40e8-b0e9-685c50264f82"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_6f162620-ce65-4860-8c42-799aa8993381">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_906c0d6f-ec88-49bb-946c-e907349402b5" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_844da1b4-370a-4af4-b264-a4368ed2cd62">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_31933356-0afb-4095-9540-5beb80d0c1b6">
                      <gml:posList>550142.422999999950000000 5804618.263000000300000000 56.952568054199219000 550142.829999999960000000 5804622.362999999900000000 56.952568054199219000 550136.532000000010000000 5804623.085000000900000000 56.952568054199219000 550136.462000000060000000 5804616.816000000600000000 56.952568054199219000 550142.227000000070000000 5804616.296000000100000000 56.952568054199219000 550142.422999999950000000 5804618.263000000300000000 56.952568054199219000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_2b2cbd47-5baa-41a2-b6c8-9d87b2d8daca">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_b74c7c27-0795-49c8-8b19-e9d973b96cd2" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_4e3323ff-7bf6-4294-83c6-e28fdb3ca29f">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_6ad47c3f-d15e-418f-b4f5-29216354d88d">
                      <gml:posList>550142.829999999960000000 5804622.362999999900000000 56.952568054199219000 550142.422999999950000000 5804618.263000000300000000 56.952568054199219000 550142.227000000070000000 5804616.296000000100000000 56.952568054199219000 550142.227000000070000000 5804616.296000000100000000 54.020000000000010000 550142.422999999950000000 5804618.263000000300000000 54.020000000000003000 550142.829999999960000000 5804622.362999999900000000 54.020000000000010000 550142.829999999960000000 5804622.362999999900000000 56.952568054199219000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_80b0a194-5e3f-4a71-bc87-24c497064633">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_96c5e952-7374-47a2-9d7c-6499633e490a" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_c55a8ddb-3a08-4c5f-936b-7b131571f110">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_1eda4a22-47cf-4fd3-8180-fbefdf833aba">
                      <gml:posList>550142.227000000070000000 5804616.296000000100000000 56.952568054199219000 550136.462000000060000000 5804616.816000000600000000 56.952568054199219000 550136.462000000060000000 5804616.816000000600000000 54.020000000000010000 550142.227000000070000000 5804616.296000000100000000 54.020000000000010000 550142.227000000070000000 5804616.296000000100000000 56.952568054199219000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_8f81cabe-e40c-42fa-b986-fca36213489a">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_af27106c-0ea2-48f7-a6ae-831c609afc08" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_ab50c40e-d07b-43dc-b4fe-1288b8763d9b">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_b8ce0c8a-01b9-4324-8159-deabe4d70ec7">
                      <gml:posList>550136.532000000010000000 5804623.085000000900000000 54.020000000000010000 550136.462000000060000000 5804616.816000000600000000 54.020000000000010000 550136.462000000060000000 5804616.816000000600000000 56.952568054199219000 550136.532000000010000000 5804623.085000000900000000 56.952568054199219000 550136.532000000010000000 5804623.085000000900000000 54.020000000000010000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_c7cf0eef-24d4-4b0d-bedc-d3d46396beca">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_4daa92c6-e4cb-454c-8c1b-ccbf1e240374" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_415b3a5a-86a9-4b84-b913-f9293c0b2480">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_e1b9e3a4-776a-411e-8196-3b8e9b2be71a">
                      <gml:posList>550142.829999999960000000 5804622.362999999900000000 54.020000000000010000 550136.532000000010000000 5804623.085000000900000000 54.020000000000010000 550136.532000000010000000 5804623.085000000900000000 56.952568054199219000 550142.829999999960000000 5804622.362999999900000000 56.952568054199219000 550142.829999999960000000 5804622.362999999900000000 54.020000000000010000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="UUID_f3b6481f-4e75-447e-8be3-fae91c298248">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_88e258cc-8856-4781-8e4c-1aafab4838d1" srsDimension="3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="UUID_b7f89126-2c88-40e8-b0e9-685c50264f82">
                  <gml:exterior>
                    <gml:LinearRing gml:id="UUID_6a4970c0-416a-4da0-9353-50bc72627732">
                      <gml:posList>550142.422999999950000000 5804618.263000000300000000 54.020000000000003000 550142.227000000070000000 5804616.296000000100000000 54.020000000000010000 550136.462000000060000000 5804616.816000000600000000 54.020000000000010000 550136.532000000010000000 5804623.085000000900000000 54.020000000000010000 550142.829999999960000000 5804622.362999999900000000 54.020000000000010000 550142.422999999950000000 5804618.263000000300000000 54.020000000000003000 </gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </cityObjectMember>

</CityModel>
