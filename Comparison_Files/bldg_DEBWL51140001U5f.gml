<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<core:CityModel xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:tex="http://www.opengis.net/citygml/texturedsurface/2.0" xmlns:core="http://www.opengis.net/citygml/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:frn="http://www.opengis.net/citygml/cityfurniture/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd http://www.opengis.net/citygml/appearance/2.0 http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/cityobjectgroup/2.0 http://schemas.opengis.net/citygml/cityobjectgroup/2.0/cityObjectGroup.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/cityfurniture/2.0 http://schemas.opengis.net/citygml/cityfurniture/2.0/cityFurniture.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd">
  <core:cityObjectMember>
    <bldg:Building gml:id="bldg_DEBWL51140001U5f">
      <gml:name>Amtsgericht</gml:name>
      <gml:boundedBy>
        <gml:Envelope srsName="urn:ogc:def:crs:EPSG::25832" srsDimension="3">
          <gml:lowerCorner>456142.03 5428855.25 115.1082</gml:lowerCorner>
          <gml:upperCorner>456218.36 5428931.96 131.88</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <core:creationDate>2019-06-28</core:creationDate>
      <core:externalReference>
        <core:informationSystem>http://www.karlsruhe.de/b3/bauen/geodaten/3dgis</core:informationSystem>
        <core:externalObject>
          <core:uri>DEBWL51140001U5f</core:uri>
        </core:externalObject>
      </core:externalReference>
      <gen:doubleAttribute name="HoeheGrund">
        <gen:value>115.108</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="HoeheDach">
        <gen:value>131.88</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="NiedrigsteTraufeDesGebaeudes">
        <gen:value>115.108</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Dachtypen">
        <gen:value>FLACHDACH;PULTDACH;WALMDACH;WALMDACH;PULTDACH;PULTDACH</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Dachkodierungsliste">
        <gen:value>1000;2100;3200;3200;2100;2100</gen:value>
      </gen:stringAttribute>
      <gen:doubleAttribute name="Volumen">
        <gen:value>30288.063</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Verfahren">
        <gen:value>BREC_2000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Objektart">
        <gen:value>Hauptgebaeude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Strassenschluessel">
        <gen:value>06770</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Strassenname">
        <gen:value>Schlossplatz</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Hausnummer">
        <gen:value>23</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="creationUser">
        <gen:value>Firma VCS</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Feinheit">
        <gen:value>Basis</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="AmtlicherGemeindeschluessel">
        <gen:value>08212000</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="DatenquelleDachhoehe">
        <gen:value>1000</gen:value>
      </gen:intAttribute>
      <gen:intAttribute name="DatenquelleLage">
        <gen:value>1000</gen:value>
      </gen:intAttribute>
      <gen:intAttribute name="DatenquelleBodenhoehe">
        <gen:value>1100</gen:value>
      </gen:intAttribute>
      <bldg:function>31001_3015</bldg:function>
      <bldg:roofType>3200</bldg:roofType>
      <bldg:measuredHeight uom="urn:adv:uom:m">16.772</bldg:measuredHeight>
      <bldg:lod2Solid>
        <gml:Solid>
          <gml:exterior>
            <gml:CompositeSurface>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_4c1d20bf-9db8-4a77-aa3c-a93a362f1585_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_be44531d-e323-4157-a7ac-6019dd6a5e4d_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_416d2fcc-0ed8-41cc-8941-d1928aad61b6_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_ba0b35c4-b752-41dc-a503-ab26ebb9dbdf_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_e8d6b556-05d6-4a38-8e79-1026e884dd4b_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_fa3d04aa-77f6-4653-88b5-00b981b7d8ac_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_7b14bb0e-277b-4090-abdd-e83def20f0d9_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_596c8211-efb5-4f9f-8d0a-dd7ae2957bc4_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_aa74b072-8b9e-4c8f-9ef6-502495e6833d_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_b9c688bb-b3c7-4c45-9e30-c36ec0bfd41b_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_1bc7e1a1-b6df-424b-a46d-06746e56978e_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_2aad6c49-8f31-4cb9-a6fc-536f57dc627c_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_c494fca1-8c86-4d1b-b671-3a9c8297c0dd_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_fa5ad761-6285-48e3-a8eb-160a7f388a03_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_556fac7c-b8bb-480c-b000-c3d99479ec5f_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_6f196875-9b2b-41c7-bdab-ffd1dca46254_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_5bbf7474-4b41-404a-a39e-f3df3ad4513e_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_5a720584-7ef4-481d-8f8e-4be71878364f_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_623f67f0-3c11-4f03-9183-1ab5a745ddc3_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_2edfb2a6-f021-402e-acaf-8a93817753de_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_a2985af8-3b6f-40ed-8664-f495e577ffc2_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_1e53d314-e0b2-40ac-9d9a-f829ec22dad8_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_2f09a571-5658-4f32-93ab-834a857e83d8_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_3e83961e-211d-4108-890e-1b840e7c8a6c_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_ecf26176-9031-4588-872e-1b1a65bcdf68_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_b54bb18a-8b48-41d9-8a8f-2b4bd9e21dc0_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_c9c2ca84-58e7-43a4-a71b-8077d3f62874_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_03e1a198-957e-41df-a759-f7c67e7eb1ff_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_1be575b5-b7a2-4603-a4ee-af858f49a70d_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_0f929224-5f4e-4edc-8274-bc11d638028f_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_ee8f5121-e9b0-4024-8efa-e42cc5be0f66_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_599ec471-7637-4bf7-925e-ad66dd8fae18_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_b1c86b5c-d1af-4cdc-9990-547fb4e2c22f_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_a8ecd20d-8650-4678-99ce-dc12a12922f3_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_3f45a92e-0821-40b3-8418-fbcbe121664a_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_08d1f254-fb63-48fb-8d3b-4b7cee3cf4c4_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_139046b6-06aa-45ee-b1ee-bf1916aa4f8a_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_f1add039-ef7e-403b-b061-c050b8156016_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_107b6f9b-a43f-480d-8ad6-62f765a0538a_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_06c77569-9b89-4faf-97a6-8e6718cdb162_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_cdad7ecb-13da-4501-8d69-0d838b9c7b54_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_b32641e5-0361-4e0d-b521-d2f483c8fa11_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_be946d9d-0a45-4a3e-8bf0-917a51a48ce8_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_2f9941be-a676-4546-ac06-610875961262_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_16e4b99b-19b4-4d99-8ed6-3ace2f84a02c_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_480e89a9-7cb7-4218-8ef5-c77ee4ef1310_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_7c3a437b-1c76-435f-a834-941e409e48a9_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_2c8c72db-0444-455b-9b5f-bc1659f402b2_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_c51cda19-4d22-4459-8dfb-cef61f579226_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_b03e09fd-51a0-4946-b88e-f16868ab2107_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_93c27ebd-c578-4f5b-bb3a-96f9bf759887_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_2b519102-d9df-4b88-87db-7785a23f127f_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_1efb926c-2c46-428a-8340-0326666fe0d8_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_03e101ed-e643-4cc1-8f0b-13602a245d50_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_6fe6ce30-0467-4bdf-a60a-643f2b8e504b_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_cdc3111e-e0f2-4a51-88f9-23774f57d2ae_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_ede274b1-52a6-4e73-8be3-64e71a7490fd_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_d413e065-9207-4144-984d-d24ea0bc261e_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_934d9513-045a-4822-aa78-e17dcdde0cd9_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_5fd6ef4a-5f0a-47fe-88a3-c06139c273d7_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_b378d50d-2a86-4366-9121-5e5954d06bfc_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_dc967643-41d7-4243-93e7-147275fcd10e_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_15ad1700-645b-488a-b79a-c866bfcf38cd_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_851992aa-6f56-4111-907c-6eb940b0a829_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_434c226d-4940-4ca1-b40b-a7d9463cb58f_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_d4fa3632-90d9-4fb6-8ada-3ff0efebb275_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_ea310928-174f-4ab1-883e-15c522212cda_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_bc84c5ff-96e2-41cc-ab85-2d7110e9cd2b_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_5004123b-60f5-43e2-81f8-05635584129e_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_32887106-5a2d-4a4c-a89f-8ff13e0e04d2_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_c674232e-678c-4184-be59-0c378e8de095_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_3f4ac2b4-02af-4b8f-9aeb-15431b4925fd_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_8a4f3d1d-897f-4651-a1b3-9b27cb19ab32_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_94649681-4d39-4873-b8c5-99aa07184330_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_dd9b7f09-1c74-48c4-993e-50156d4583ab_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_6d91a27d-06a9-4945-8c9e-a5fb6deb15ec_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_560189cf-bc7d-4ccb-8bbf-16ea3c37491a_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_ab3bc777-4c4e-4963-8aae-4bc5a72a9e46_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_abd8962e-bc9d-46ae-b4e5-f5067c4b1e81_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_939551cc-f04c-46d2-8b7e-bab4fdc423bd_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_c9c3a20f-d9fa-466f-aede-bbb662ee2bce_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_fc8f6af2-8851-44fb-8f1f-2f680e7898d2_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_4bc06476-9af6-49be-a355-cb76e8c02b76_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_72fb0d87-b581-4e5e-84bb-0797f2d171e9_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_61893f17-d433-4ea4-8bcc-e49b1852fa26_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_bf8d6a35-2f20-460f-ba14-61d1ed20afc4_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_d12a96ff-0a95-4ead-b388-fbc65c0df449_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_5624a4a2-d5fb-482c-b58f-d9aac3074570_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_65d53ac1-835f-4419-a095-ea47d2e58b2d_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_0e893f3b-2ed7-4a93-b92e-4638479f97a6_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_a661349e-a2b7-46cc-bfde-20c38d247453_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_4c7ce4c3-c629-4ff4-baaa-c59eaa6c7486_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_ee1558c8-b850-47e5-990f-8b26925fbe66_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_4885cc53-586e-4424-967f-5d65be6205b6_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_b5c2a393-9d28-434f-af64-ed7d9a616100_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_f964a94b-aad1-4adb-9767-721d08c08380_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_10157a67-b9b1-4926-9db7-94dffb63878e_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_855f338b-e125-4ff8-896f-389d54b0bf16_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_2f7ea477-e092-4cbf-b4c1-e7364c52a933_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_33a5ffaa-6c38-483e-8029-8f46852a87e1_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_6a97ee1f-fe3b-42e8-97d1-f2386c69f4d3_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_248e305e-44d6-4fdc-8f36-17449bb98c4c_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_68e3c756-0393-46d8-b18b-ae38dd2331db_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_4a9e7487-50ec-41b0-b0ac-7bcc7b9467af_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_13ad000c-b355-4239-92c1-81be83999ec8_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_4a1a69d7-7bf8-4e78-8f5c-5fb36a49e627_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_58be168e-1f24-4951-b9e9-d6f23536426d_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_7088c27d-e454-4892-8933-9be9c58bc314_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_8c013283-2855-4aef-980d-5ec6e5049ce9_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_0cfe39f5-ae62-47fa-b7b1-958b74ed6bf4_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_6a5cfa75-2b9d-4a8d-873f-634ba64f818e_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_602479c4-6d70-46d5-b331-0b5f19a02fdf_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_d304cb7f-7acd-472b-8550-49a5e3528dbb_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_8a6f4572-1243-4a64-adb8-d759764948e5_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_afb4a49d-4b72-41d8-b30d-4789714a04f0_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_3ac686eb-4a5c-4219-9bb1-f3ac6cf24ba8_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_6d197d17-a81c-423a-bbd5-9d70dada98df_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_5f20e513-8305-406b-b93c-d6541d398c22_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_909556c4-64ee-47b1-a080-eecc43b4fa7e_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_04d27d33-c9f2-44ed-a12c-3f085a10552d_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_737d958a-4d59-4ea1-aca5-bc31ca53733f_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_febe60b2-f282-486a-a247-bfba8ddefdeb_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_e3924178-cb6a-4d99-a680-09c3750c12f2_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_5d5f58c9-061d-47c9-a74d-469b1bc2ce5c_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_68c4118a-6a61-4e48-aa17-586964e32702_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_2f957aef-f59e-49d2-b872-38c577002cc5_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_da8b2e99-926f-45b1-a986-5646276cf8ba_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_950458b0-f779-428f-bae8-e29d7c7c41eb_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_6b79e846-3db3-492d-b584-8e6027232cde_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_f9ff1116-c656-40d3-b3d9-38b9a6a5343d_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_e6c5ee89-ae83-4053-85cc-08d84ec8f833_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_87aed47a-e341-4a13-998d-bd0e51aa8e01_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_dbc5e450-2a08-4bfa-96f9-c5d0c8ded7af_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_80695755-821a-4c8f-8515-aefd66f82264_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_e3221a9c-ca7d-43cd-b24a-9528d669187f_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_a6b9b5ab-a638-47ea-97ba-56c9e38731ae_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001U5f_c3ed0766-55ae-4c1d-9792-d343d1e1f084_2_poly"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_4c1d20bf-9db8-4a77-aa3c-a93a362f1585_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>124.755</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>90.0</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>123.44</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>123.44</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>-1.0</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>4c1d20bf-9db8-4a77-aa3c-a93a362f1585</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854883_100">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_4c1d20bf-9db8-4a77-aa3c-a93a362f1585_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32554">
                      <gml:posList srsDimension="3">456168.8154 5428870.7056 123.44 456169.4163 5428871.3231 123.44 456143.86819999997 5428896.164399999 123.44 456169.779 5428918.3189 123.44 456175.02569999994 5428911.924499999 123.44 456178.1653 5428914.6994 123.44 456177.8396 5428914.992 123.44 456171.82 5428921.579999999 123.44 456169.84 5428919.83 123.44 456169.52 5428920.22 123.44 456142.03 5428896.75 123.44 456168.8154 5428870.7056 123.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_be44531d-e323-4157-a7ac-6019dd6a5e4d_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.801</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>85.461</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>128.3</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.07</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>50.635</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>be44531d-e323-4157-a7ac-6019dd6a5e4d</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854883_101">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_be44531d-e323-4157-a7ac-6019dd6a5e4d_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32555">
                      <gml:posList srsDimension="3">456160.0474 5428893.2251 128.07 456159.6975 5428893.6516 128.07 456154.8295 5428895.0192 128.2999 456160.0474 5428893.2251 128.07</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_416d2fcc-0ed8-41cc-8941-d1928aad61b6_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>10.879</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>84.43</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>128.3</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.07</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>44.269</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>416d2fcc-0ed8-41cc-8941-d1928aad61b6</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854883_102">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_416d2fcc-0ed8-41cc-8941-d1928aad61b6_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32556">
                      <gml:posList srsDimension="3">456161.4116 5428888.6058 128.2997 456160.0474 5428893.2251 128.07 456154.8295 5428895.0192 128.2999 456161.4116 5428888.6058 128.2997</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_ba0b35c4-b752-41dc-a503-ab26ebb9dbdf_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>7.614</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>90.0</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>128.5</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.5</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>-1.0</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>ba0b35c4-b752-41dc-a503-ab26ebb9dbdf</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854883_103">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_ba0b35c4-b752-41dc-a503-ab26ebb9dbdf_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32557">
                      <gml:posList srsDimension="3">456161.4438 5428891.5233 128.5 456164.07459999993 5428893.7693 128.5 456162.67819999997 5428895.4712 128.5 456161.1476 5428894.1644 128.5 456160.0474 5428893.2251 128.5 456161.4438 5428891.5233 128.5</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_e8d6b556-05d6-4a38-8e79-1026e884dd4b_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.08</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>83.018</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>128.3</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.07</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>50.63</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e8d6b556-05d6-4a38-8e79-1026e884dd4b</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854883_104">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_e8d6b556-05d6-4a38-8e79-1026e884dd4b_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32558">
                      <gml:posList srsDimension="3">456161.4116 5428888.6058 128.2997 456161.4438 5428891.5233 128.07 456160.0474 5428893.2251 128.07 456161.4116 5428888.6058 128.2997</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_fa3d04aa-77f6-4653-88b5-00b981b7d8ac_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.016</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>79.46</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>128.301</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.07</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>295.664</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>fa3d04aa-77f6-4653-88b5-00b981b7d8ac</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854883_105">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_fa3d04aa-77f6-4653-88b5-00b981b7d8ac_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32559">
                      <gml:posList srsDimension="3">456161.4223 5428888.6169 128.3006 456161.4438 5428891.5233 128.07 456161.4116 5428888.6058 128.2997 456161.4223 5428888.6169 128.3006</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_7b14bb0e-277b-4090-abdd-e83def20f0d9_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>1.897</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>82.934</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>128.301</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.07</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>50.63</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>7b14bb0e-277b-4090-abdd-e83def20f0d9</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854883_106">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_7b14bb0e-277b-4090-abdd-e83def20f0d9_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32560">
                      <gml:posList srsDimension="3">456162.7277 5428889.9586 128.07 456161.4438 5428891.5233 128.07 456161.4223 5428888.6169 128.3006 456162.7277 5428889.9586 128.07</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_596c8211-efb5-4f9f-8d0a-dd7ae2957bc4_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>83.592</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>84.843</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>128.301</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>127.8</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>44.2</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>596c8211-efb5-4f9f-8d0a-dd7ae2957bc4</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854883_107">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_596c8211-efb5-4f9f-8d0a-dd7ae2957bc4_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32561">
                      <gml:posList srsDimension="3">456170.205 5428880.0772 128.3 456178.0419 5428880.1884 127.8 456165.2877 5428892.5897 127.8 456162.7277 5428889.9586 128.1311 456161.4223 5428888.6169 128.3006 456170.205 5428880.0772 128.3</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_aa74b072-8b9e-4c8f-9ef6-502495e6833d_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>6.625</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>85.694</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>128.07</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>127.8</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>48.501</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>aa74b072-8b9e-4c8f-9ef6-502495e6833d</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854884_108">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_aa74b072-8b9e-4c8f-9ef6-502495e6833d_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32562">
                      <gml:posList srsDimension="3">456162.7277 5428889.9586 128.07 456165.2877 5428892.5897 127.8 456164.07459999993 5428893.7693 127.8039 456161.4438 5428891.5233 128.07 456162.7277 5428889.9586 128.07</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_b9c688bb-b3c7-4c45-9e30-c36ec0bfd41b_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>5.279</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>86.139</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>127.951</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>127.8</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>138.73</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>b9c688bb-b3c7-4c45-9e30-c36ec0bfd41b</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854884_109">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_b9c688bb-b3c7-4c45-9e30-c36ec0bfd41b_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32563">
                      <gml:posList srsDimension="3">456164.07459999993 5428893.7693 127.8039 456165.89469999995 5428895.3233 127.8 456164.4982999999 5428897.0251 127.9502 456162.67819999997 5428895.4712 127.9507 456164.07459999993 5428893.7693 127.8039</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_1bc7e1a1-b6df-424b-a46d-06746e56978e_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>13.512</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>86.096</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>128.3</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>127.8</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>141.287</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>1bc7e1a1-b6df-424b-a46d-06746e56978e</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854884_110">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_1bc7e1a1-b6df-424b-a46d-06746e56978e_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32564">
                      <gml:posList srsDimension="3">456165.89469999995 5428895.3233 127.8 456170.8037 5428908.6489 128.3 456164.4982999999 5428897.0251 127.9502 456165.89469999995 5428895.3233 127.8</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_2aad6c49-8f31-4cb9-a6fc-536f57dc627c_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>56.522</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>85.883</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>128.3</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>127.8</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>139.511</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>2aad6c49-8f31-4cb9-a6fc-536f57dc627c</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854884_111">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_2aad6c49-8f31-4cb9-a6fc-536f57dc627c_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32565">
                      <gml:posList srsDimension="3">456178.2383 5428905.8618 127.8 456170.8037 5428908.6489 128.3 456165.89469999995 5428895.3233 127.8 456178.2383 5428905.8618 127.8</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_c494fca1-8c86-4d1b-b671-3a9c8297c0dd_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>1.534</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>90.0</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>118.444</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>118.444</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>-1.0</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>c494fca1-8c86-4d1b-b671-3a9c8297c0dd</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854884_112">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_c494fca1-8c86-4d1b-b671-3a9c8297c0dd_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32566">
                      <gml:posList srsDimension="3">456170.0918 5428869.4646 118.4443 456170.6927 5428870.082 118.4443 456169.4163 5428871.3231 118.4443 456168.8154 5428870.7056 118.4443 456170.0918 5428869.4646 118.4443</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_fa5ad761-6285-48e3-a8eb-160a7f388a03_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>27.869</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>82.839</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>128.3</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>127.8</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>50.631</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>fa5ad761-6285-48e3-a8eb-160a7f388a03</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854884_113">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_fa5ad761-6285-48e3-a8eb-160a7f388a03_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32567">
                      <gml:posList srsDimension="3">456170.8037 5428908.6489 128.3 456178.2383 5428905.8618 127.8 456169.4234 5428916.605 127.8 456170.8037 5428908.6489 128.3</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_556fac7c-b8bb-480c-b000-c3d99479ec5f_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>16.902</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>90.0</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>124.32</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>124.32</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>-1.0</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>556fac7c-b8bb-480c-b000-c3d99479ec5f</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854884_114">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_556fac7c-b8bb-480c-b000-c3d99479ec5f_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32568">
                      <gml:posList srsDimension="3">456172.03 5428867.58 124.32 456172.39 5428867.949999999 124.32 456176.4321 5428872.2444 124.32 456174.4857 5428873.9805 124.32 456170.6927 5428870.082 124.32 456170.0918 5428869.4646 124.32 456172.03 5428867.58 124.32</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_6f196875-9b2b-41c7-bdab-ffd1dca46254_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>30.83</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>84.841</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>128.3</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>127.8</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>134.232</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>6f196875-9b2b-41c7-bdab-ffd1dca46254</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854884_115">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_6f196875-9b2b-41c7-bdab-ffd1dca46254_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32569">
                      <gml:posList srsDimension="3">456170.311 5428872.2427 127.7997 456178.0419 5428880.1884 127.8 456170.205 5428880.0772 128.3 456170.311 5428872.2427 127.7997</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_5bbf7474-4b41-404a-a39e-f3df3ad4513e_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>37.211</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>47.5</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.88</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>316.831</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>5bbf7474-4b41-404a-a39e-f3df3ad4513e</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854884_116">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_5bbf7474-4b41-404a-a39e-f3df3ad4513e_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32570">
                      <gml:posList srsDimension="3">456178.2922 5428923.3876 131.88 456178.1415 5428930.7576 126.86 456170.8366 5428923.9053 126.86 456178.2922 5428923.3876 131.88</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_5a720584-7ef4-481d-8f8e-4be71878364f_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>11.472</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>45.129</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.88</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>226.405</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>5a720584-7ef4-481d-8f8e-4be71878364f</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854884_117">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_5a720584-7ef4-481d-8f8e-4be71878364f_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32571">
                      <gml:posList srsDimension="3">456173.0601 5428921.57 126.86 456178.2922 5428923.3876 131.88 456170.8366 5428923.9053 126.86 456173.0601 5428921.57 126.86</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_623f67f0-3c11-4f03-9183-1ab5a745ddc3_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>17.501</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>55.434</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>130.83</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.03</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>218.751</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>623f67f0-3c11-4f03-9183-1ab5a745ddc3</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854884_118">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_623f67f0-3c11-4f03-9183-1ab5a745ddc3_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32572">
                      <gml:posList srsDimension="3">456182.8 5428855.25 130.03 456182.3997 5428857.0602 130.82999999999998 456173.7811 5428863.9775 130.82999999999998 456172.06 5428863.87 130.03 456182.8 5428855.25 130.03</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_2edfb2a6-f021-402e-acaf-8a93817753de_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>5.757</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>77.586</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>123.38</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>122.99</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>133.341</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>2edfb2a6-f021-402e-acaf-8a93817753de</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854884_119">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_2edfb2a6-f021-402e-acaf-8a93817753de_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32573">
                      <gml:posList srsDimension="3">456172.4856 5428867.8573 123.349 456177.7313 5428871.050299999 122.99159999999999 456177.7548 5428871.0646 122.99 456176.4321 5428872.2444 123.38 456172.39 5428867.949999999 123.3783 456172.4856 5428867.8573 123.349</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_a2985af8-3b6f-40ed-8664-f495e577ffc2_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>6.473</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>80.524</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>123.349</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>122.992</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>128.264</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>a2985af8-3b6f-40ed-8664-f495e577ffc2</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854884_120">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_a2985af8-3b6f-40ed-8664-f495e577ffc2_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32574">
                      <gml:posList srsDimension="3">456174.0279 5428866.3617 122.9923 456177.7313 5428871.050299999 122.99159999999999 456172.4856 5428867.8573 123.349 456174.0279 5428866.3617 122.9923</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_1e53d314-e0b2-40ac-9d9a-f829ec22dad8_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>29.498</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>44.846</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.88</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>225.196</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>1e53d314-e0b2-40ac-9d9a-f829ec22dad8</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854884_121">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_1e53d314-e0b2-40ac-9d9a-f829ec22dad8_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32575">
                      <gml:posList srsDimension="3">456178.93169999996 5428915.6581 126.86 456178.2922 5428923.3876 131.88 456173.0601 5428921.57 126.86 456178.93169999996 5428915.6581 126.86</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_2f09a571-5658-4f32-93ab-834a857e83d8_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>40.036</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>71.374</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>130.83</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.16</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>310.076</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>2f09a571-5658-4f32-93ab-834a857e83d8</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854885_122">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_2f09a571-5658-4f32-93ab-834a857e83d8_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32576">
                      <gml:posList srsDimension="3">456195.097 5428889.3123 130.82999999999998 456194.3597 5428891.523 130.1601 456189.8877 5428886.0266 130.1995 456173.7811 5428863.9775 130.82999999999998 456195.097 5428889.3123 130.82999999999998</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_3e83961e-211d-4108-890e-1b840e7c8a6c_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>91.816</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>81.707</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.63</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.83</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>310.076</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>3e83961e-211d-4108-890e-1b840e7c8a6c</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854885_123">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_3e83961e-211d-4108-890e-1b840e7c8a6c_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32577">
                      <gml:posList srsDimension="3">456179.95239999995 5428862.7876 131.63 456195.097 5428889.3123 130.82999999999998 456173.7811 5428863.9775 130.82999999999998 456179.95239999995 5428862.7876 131.63</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_ecf26176-9031-4588-872e-1b1a65bcdf68_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>16.808</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>74.752</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.63</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.83</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>218.751</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>ecf26176-9031-4588-872e-1b1a65bcdf68</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854885_124">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_ecf26176-9031-4588-872e-1b1a65bcdf68_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32578">
                      <gml:posList srsDimension="3">456182.3997 5428857.0602 130.82999999999998 456179.95239999995 5428862.7876 131.63 456173.7811 5428863.9775 130.82999999999998 456182.3997 5428857.0602 130.82999999999998</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_b54bb18a-8b48-41d9-8a8f-2b4bd9e21dc0_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>23.631</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>90.0</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>123.27</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>123.27</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>-1.0</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>b54bb18a-8b48-41d9-8a8f-2b4bd9e21dc0</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854885_125">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_b54bb18a-8b48-41d9-8a8f-2b4bd9e21dc0_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32579">
                      <gml:posList srsDimension="3">456178.9872 5428872.6288 123.27 456182.7487 5428877.4026 123.27 456180.69 5428879.21 123.27 456180.9335 5428879.5244 123.27 456180.32639999996 5428879.983499999 123.27 456175.87309999997 5428875.4064 123.27 456178.9872 5428872.6288 123.27</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_c9c2ca84-58e7-43a4-a71b-8077d3f62874_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>58.448</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>45.937</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.88</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>44.018</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>c9c2ca84-58e7-43a4-a71b-8077d3f62874</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854885_126">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_c9c2ca84-58e7-43a4-a71b-8077d3f62874_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32580">
                      <gml:posList srsDimension="3">456178.2922 5428923.3876 131.88 456182.5486 5428919.2944 131.8742 456185.5391 5428923.597799999 126.86 456178.1415 5428930.7576 126.86 456178.2922 5428923.3876 131.88</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_03e1a198-957e-41df-a759-f7c67e7eb1ff_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>70.905</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>81.131</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.63</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.83</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>309.388</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>03e1a198-957e-41df-a759-f7c67e7eb1ff</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854885_127">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_03e1a198-957e-41df-a759-f7c67e7eb1ff_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32581">
                      <gml:posList srsDimension="3">456197.295 5428883.9102 131.63 456195.097 5428889.3123 130.82999999999998 456179.95239999995 5428862.7876 131.63 456197.295 5428883.9102 131.63</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_1be575b5-b7a2-4603-a4ee-af858f49a70d_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>76.298</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>81.762</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.63</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.83</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>129.388</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>1be575b5-b7a2-4603-a4ee-af858f49a70d</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854885_128">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_1be575b5-b7a2-4603-a4ee-af858f49a70d_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32582">
                      <gml:posList srsDimension="3">456182.3997 5428857.0602 130.82999999999998 456197.295 5428883.9102 131.63 456179.95239999995 5428862.7876 131.63 456182.3997 5428857.0602 130.82999999999998</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_0f929224-5f4e-4edc-8274-bc11d638028f_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>86.458</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>81.191</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.63</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.83</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>128.699</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>0f929224-5f4e-4edc-8274-bc11d638028f</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854885_129">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_0f929224-5f4e-4edc-8274-bc11d638028f_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32583">
                      <gml:posList srsDimension="3">456203.09489999997 5428882.8931 130.82999999999998 456197.295 5428883.9102 131.63 456182.3997 5428857.0602 130.82999999999998 456203.09489999997 5428882.8931 130.82999999999998</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_ee8f5121-e9b0-4024-8efa-e42cc5be0f66_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>57.097</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>60.98</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>130.83</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.028</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>128.695</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>ee8f5121-e9b0-4024-8efa-e42cc5be0f66</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854885_130">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_ee8f5121-e9b0-4024-8efa-e42cc5be0f66_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32584">
                      <gml:posList srsDimension="3">456182.8 5428855.25 130.03 456205.23 5428883.25 130.0282 456205.0907 5428883.3566 130.1255 456203.09489999997 5428882.8931 130.82999999999998 456182.3997 5428857.0602 130.82999999999998 456182.8 5428855.25 130.03</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_599ec471-7637-4bf7-925e-ad66dd8fae18_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>43.328</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>45.741</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.88</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>222.13</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>599ec471-7637-4bf7-925e-ad66dd8fae18</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854885_131">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_599ec471-7637-4bf7-925e-ad66dd8fae18_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32585">
                      <gml:posList srsDimension="3">456197.1842 5428905.7306 131.88 456188.2496 5428913.812 131.88 456185.3058 5428909.5284 126.86 456197.1842 5428905.7306 131.88</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_b1c86b5c-d1af-4cdc-9990-547fb4e2c22f_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>48.551</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>45.298</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.88</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>221.731</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>b1c86b5c-d1af-4cdc-9990-547fb4e2c22f</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854885_132">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_b1c86b5c-d1af-4cdc-9990-547fb4e2c22f_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32586">
                      <gml:posList srsDimension="3">456195.4598 5428900.471499999 126.86 456197.1842 5428905.7306 131.88 456185.3058 5428909.5284 126.86 456195.4598 5428900.471499999 126.86</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_a8ecd20d-8650-4678-99ce-dc12a12922f3_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>86.917</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>45.996</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.88</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>42.129</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>a8ecd20d-8650-4678-99ce-dc12a12922f3</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854885_133">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_a8ecd20d-8650-4678-99ce-dc12a12922f3_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32587">
                      <gml:posList srsDimension="3">456197.1842 5428905.7306 131.88 456200.1467 5428910.0594 126.86 456191.2401 5428918.1154 126.86 456188.2496 5428913.812 131.88 456197.1842 5428905.7306 131.88</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_3f45a92e-0821-40b3-8418-fbcbe121664a_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>9.602</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>64.867</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>130.83</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.03</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>38.751</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>3f45a92e-0821-40b3-8418-fbcbe121664a</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854885_134">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_3f45a92e-0821-40b3-8418-fbcbe121664a_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32588">
                      <gml:posList srsDimension="3">456203.09489999997 5428882.8931 130.82999999999998 456201.3314 5428886.2356 130.1249 456200.95999999996 5428886.52 130.1299 456201.0942 5428886.6852 130.0301 456195.097 5428889.3123 130.82999999999998 456203.09489999997 5428882.8931 130.82999999999998</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_08d1f254-fb63-48fb-8d3b-4b7cee3cf4c4_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>15.115</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>74.253</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.63</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.83</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>38.751</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>08d1f254-fb63-48fb-8d3b-4b7cee3cf4c4</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854886_135">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_08d1f254-fb63-48fb-8d3b-4b7cee3cf4c4_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32589">
                      <gml:posList srsDimension="3">456197.295 5428883.9102 131.63 456203.09489999997 5428882.8931 130.82999999999998 456195.097 5428889.3123 130.82999999999998 456197.295 5428883.9102 131.63</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_139046b6-06aa-45ee-b1ee-bf1916aa4f8a_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>37.746</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>45.691</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.88</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>219.85</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>139046b6-06aa-45ee-b1ee-bf1916aa4f8a</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854886_136">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_139046b6-06aa-45ee-b1ee-bf1916aa4f8a_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32590">
                      <gml:posList srsDimension="3">456205.2489 5428898.9993 131.88 456197.1842 5428905.7306 131.88 456195.4598 5428900.471499999 126.86 456205.2489 5428898.9993 131.88</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_f1add039-ef7e-403b-b061-c050b8156016_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>33.106</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>44.061</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.88</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>217.943</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>f1add039-ef7e-403b-b061-c050b8156016</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854886_137">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_f1add039-ef7e-403b-b061-c050b8156016_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32591">
                      <gml:posList srsDimension="3">456202.9344999999 5428894.6436 126.86 456205.2489 5428898.9993 131.88 456195.4598 5428900.471499999 126.86 456202.9344999999 5428894.6436 126.86</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_107b6f9b-a43f-480d-8ad6-62f765a0538a_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>18.276</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>90.0</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>126.103</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.103</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>-1.0</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>107b6f9b-a43f-480d-8ad6-62f765a0538a</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854886_138">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_107b6f9b-a43f-480d-8ad6-62f765a0538a_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32592">
                      <gml:posList srsDimension="3">456201.0942 5428886.6852 126.1034 456205.06 5428891.57 126.1034 456202.5869 5428893.6841 126.1034 456202.5605 5428893.651099999 126.1034 456202.87 5428893.39 126.1034 456198.8014 5428888.4412 126.1034 456201.0942 5428886.6852 126.1034</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_06c77569-9b89-4faf-97a6-8e6718cdb162_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.099</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>65.983</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>130.83</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.125</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>37.43</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>06c77569-9b89-4faf-97a6-8e6718cdb162</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854886_139">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_06c77569-9b89-4faf-97a6-8e6718cdb162_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32593">
                      <gml:posList srsDimension="3">456203.09489999997 5428882.8931 130.82999999999998 456205.0907 5428883.3566 130.1255 456201.3314 5428886.2356 130.1249 456203.09489999997 5428882.8931 130.82999999999998</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_cdad7ecb-13da-4501-8d69-0d838b9c7b54_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.023</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>90.0</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>115.108</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>115.108</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>-1.0</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>cdad7ecb-13da-4501-8d69-0d838b9c7b54</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854886_140">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_cdad7ecb-13da-4501-8d69-0d838b9c7b54_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32594">
                      <gml:posList srsDimension="3">456202.5605 5428893.651099999 115.1082 456202.5869 5428893.6841 115.1082 456201.72 5428894.36 115.1082 456202.5605 5428893.651099999 115.1082</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_b32641e5-0361-4e0d-b521-d2f483c8fa11_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>14.315</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>43.877</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.88</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>219.851</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>b32641e5-0361-4e0d-b521-d2f483c8fa11</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854886_141">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_b32641e5-0361-4e0d-b521-d2f483c8fa11_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32595">
                      <gml:posList srsDimension="3">456208.4051 5428896.3649 131.88 456205.2489 5428898.9993 131.88 456202.9344999999 5428894.6436 126.86 456208.4051 5428896.3649 131.88</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_be946d9d-0a45-4a3e-8bf0-917a51a48ce8_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>12.175</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>44.09</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.88</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>220.525</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>be946d9d-0a45-4a3e-8bf0-917a51a48ce8</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854886_142">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_be946d9d-0a45-4a3e-8bf0-917a51a48ce8_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32596">
                      <gml:posList srsDimension="3">456205.5826 5428892.3799 126.86 456208.4051 5428896.3649 131.88 456202.9344999999 5428894.6436 126.86 456205.5826 5428892.3799 126.86</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_2f9941be-a676-4546-ac06-610875961262_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>29.483</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>44.179</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.88</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>217.943</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>2f9941be-a676-4546-ac06-610875961262</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854886_143">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_2f9941be-a676-4546-ac06-610875961262_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32597">
                      <gml:posList srsDimension="3">456210.7259 5428888.3697 126.86 456209.9052 5428895.1953 131.88 456208.4051 5428896.3649 131.88 456205.5826 5428892.3799 126.86 456210.7259 5428888.3697 126.86</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_16e4b99b-19b4-4d99-8ed6-3ace2f84a02c_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>6.848</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>45.793</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.88</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>37.943</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>16e4b99b-19b4-4d99-8ed6-3ace2f84a02c</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854886_144">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_16e4b99b-19b4-4d99-8ed6-3ace2f84a02c_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32598">
                      <gml:posList srsDimension="3">456209.9052 5428895.1953 131.88 456213.0839 5428899.2612 126.86 456208.4051 5428896.3649 131.88 456209.9052 5428895.1953 131.88</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_480e89a9-7cb7-4218-8ef5-c77ee4ef1310_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>17.483</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>45.778</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.88</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>39.881</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>480e89a9-7cb7-4218-8ef5-c77ee4ef1310</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854886_145">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_480e89a9-7cb7-4218-8ef5-c77ee4ef1310_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32599">
                      <gml:posList srsDimension="3">456216.8117 5428896.1464 126.86 456213.0839 5428899.2612 126.86 456209.9052 5428895.1953 131.88 456216.8117 5428896.1464 126.86</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_7c3a437b-1c76-435f-a834-941e409e48a9_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>34.474</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>44.03</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.88</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>128.046</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>7c3a437b-1c76-435f-a834-941e409e48a9</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854886_146">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_7c3a437b-1c76-435f-a834-941e409e48a9_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32600">
                      <gml:posList srsDimension="3">456210.7259 5428888.3697 126.86 456216.8117 5428896.1464 126.86 456209.9052 5428895.1953 131.88 456210.7259 5428888.3697 126.86</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_2c8c72db-0444-455b-9b5f-bc1659f402b2_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>301.162</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>2c8c72db-0444-455b-9b5f-bc1659f402b2</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854886_147">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_2c8c72db-0444-455b-9b5f-bc1659f402b2_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32601">
                      <gml:posList srsDimension="3">456142.03 5428896.75 115.1082 456142.03 5428896.75 123.44 456169.52 5428920.22 123.44 456169.52 5428920.22 115.1082 456142.03 5428896.75 115.1082</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_c51cda19-4d22-4459-8dfb-cef61f579226_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.203</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>c51cda19-4d22-4459-8dfb-cef61f579226</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854886_148">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_c51cda19-4d22-4459-8dfb-cef61f579226_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32602">
                      <gml:posList srsDimension="3">456169.52 5428920.22 115.1082 456169.52 5428920.22 123.44 456169.84 5428919.83 123.44 456169.84 5428919.83 115.1082 456169.52 5428920.22 115.1082</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_b03e09fd-51a0-4946-b88e-f16868ab2107_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>22.017</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>b03e09fd-51a0-4946-b88e-f16868ab2107</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854886_149">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_b03e09fd-51a0-4946-b88e-f16868ab2107_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32603">
                      <gml:posList srsDimension="3">456169.84 5428919.83 115.1082 456169.84 5428919.83 123.44 456171.82 5428921.579999999 123.44 456171.82 5428921.579999999 115.1082 456169.84 5428919.83 115.1082</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_93c27ebd-c578-4f5b-bb3a-96f9bf759887_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.304</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>93c27ebd-c578-4f5b-bb3a-96f9bf759887</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854887_150">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_93c27ebd-c578-4f5b-bb3a-96f9bf759887_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32604">
                      <gml:posList srsDimension="3">456169.4163 5428871.3231 118.4443 456169.4163 5428871.3231 123.44 456168.8154 5428870.7056 123.44 456168.8154 5428870.7056 118.4443 456169.4163 5428871.3231 118.4443</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_2b519102-d9df-4b88-87db-7785a23f127f_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>111.477</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>2b519102-d9df-4b88-87db-7785a23f127f</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854887_151">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_2b519102-d9df-4b88-87db-7785a23f127f_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32605">
                      <gml:posList srsDimension="3">456143.86819999997 5428896.164399999 123.44 456143.86819999997 5428896.164399999 126.71 456169.779 5428918.3189 126.71 456169.779 5428918.3189 123.44 456143.86819999997 5428896.164399999 123.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_1efb926c-2c46-428a-8340-0326666fe0d8_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>27.048</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>1efb926c-2c46-428a-8340-0326666fe0d8</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854887_152">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_1efb926c-2c46-428a-8340-0326666fe0d8_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32606">
                      <gml:posList srsDimension="3">456169.779 5428918.3189 123.44 456169.779 5428918.3189 126.71 456175.02569999994 5428911.924499999 126.71 456175.02569999994 5428911.924499999 123.44 456169.779 5428918.3189 123.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_03e101ed-e643-4cc1-8f0b-13602a245d50_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>15.524</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>03e101ed-e643-4cc1-8f0b-13602a245d50</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854887_153">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_03e101ed-e643-4cc1-8f0b-13602a245d50_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32607">
                      <gml:posList srsDimension="3">456176.1807 5428910.5169 124.34 456176.1807 5428910.5169 126.71 456180.3355 5428905.453199999 126.71 456180.3355 5428905.453199999 124.34 456176.1807 5428910.5169 124.34</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_6fe6ce30-0467-4bdf-a60a-643f2b8e504b_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>43.417</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>6fe6ce30-0467-4bdf-a60a-643f2b8e504b</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854887_154">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_6fe6ce30-0467-4bdf-a60a-643f2b8e504b_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32608">
                      <gml:posList srsDimension="3">456180.3355 5428905.453199999 124.34 456180.3355 5428905.453199999 126.71 456166.6091 5428893.3213 126.71 456166.6091 5428893.3213 124.34 456180.3355 5428905.453199999 124.34</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_cdc3111e-e0f2-4a51-88f9-23774f57d2ae_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>1.371</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>cdc3111e-e0f2-4a51-88f9-23774f57d2ae</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854887_155">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_cdc3111e-e0f2-4a51-88f9-23774f57d2ae_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32609">
                      <gml:posList srsDimension="3">456162.67819999997 5428895.4712 127.9507 456162.67819999997 5428895.4712 128.5 456164.07459999993 5428893.7693 128.5 456164.07459999993 5428893.7693 127.8039 456162.67819999997 5428895.4712 127.9507</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_ede274b1-52a6-4e73-8be3-64e71a7490fd_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>1.948</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>ede274b1-52a6-4e73-8be3-64e71a7490fd</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854887_156">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_ede274b1-52a6-4e73-8be3-64e71a7490fd_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32610">
                      <gml:posList srsDimension="3">456164.07459999993 5428893.7693 127.8039 456164.07459999993 5428893.7693 128.5 456161.4438 5428891.5233 128.5 456161.4438 5428891.5233 128.07 456164.07459999993 5428893.7693 127.8039</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_d413e065-9207-4144-984d-d24ea0bc261e_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.947</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>d413e065-9207-4144-984d-d24ea0bc261e</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854887_157">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_d413e065-9207-4144-984d-d24ea0bc261e_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32611">
                      <gml:posList srsDimension="3">456161.4438 5428891.5233 128.07 456161.4438 5428891.5233 128.5 456160.0474 5428893.2251 128.5 456160.0474 5428893.2251 128.07 456161.4438 5428891.5233 128.07</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_934d9513-045a-4822-aa78-e17dcdde0cd9_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>15.172</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>934d9513-045a-4822-aa78-e17dcdde0cd9</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854887_158">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_934d9513-045a-4822-aa78-e17dcdde0cd9_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32612">
                      <gml:posList srsDimension="3">456182.48 5428907.28 115.1082 456182.48 5428907.28 124.34 456183.71 5428906.19 124.34 456183.71 5428906.19 115.1082 456182.48 5428907.28 115.1082</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_5fd6ef4a-5f0a-47fe-88a3-c06139c273d7_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>180.051</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>5fd6ef4a-5f0a-47fe-88a3-c06139c273d7</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854887_159">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_5fd6ef4a-5f0a-47fe-88a3-c06139c273d7_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32613">
                      <gml:posList srsDimension="3">456183.71 5428906.19 115.1082 456183.71 5428906.19 124.34 456169.0999999999 5428893.27 124.34 456169.0999999999 5428893.27 115.1082 456183.71 5428906.19 115.1082</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_b378d50d-2a86-4366-9121-5e5954d06bfc_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>21.968</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>b378d50d-2a86-4366-9121-5e5954d06bfc</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854887_160">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_b378d50d-2a86-4366-9121-5e5954d06bfc_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32614">
                      <gml:posList srsDimension="3">456180.32639999996 5428879.983499999 123.27 456180.32639999996 5428879.983499999 124.34 456180.32639999996 5428879.983499999 126.71 456175.87309999997 5428875.4064 126.71 456175.87309999997 5428875.4064 123.27 456180.32639999996 5428879.983499999 123.27</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_dc967643-41d7-4243-93e7-147275fcd10e_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>13.0</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>dc967643-41d7-4243-93e7-147275fcd10e</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854887_161">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_dc967643-41d7-4243-93e7-147275fcd10e_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32615">
                      <gml:posList srsDimension="3">456174.4857 5428873.9805 124.32 456174.4857 5428873.9805 126.71 456170.6927 5428870.082 126.71 456170.6927 5428870.082 124.32 456174.4857 5428873.9805 124.32</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_15ad1700-645b-488a-b79a-c866bfcf38cd_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>15.147</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>15ad1700-645b-488a-b79a-c866bfcf38cd</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854887_162">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_15ad1700-645b-488a-b79a-c866bfcf38cd_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32616">
                      <gml:posList srsDimension="3">456169.4234 5428916.605 126.71 456169.4234 5428916.605 127.8 456178.2383 5428905.8618 127.8 456178.2383 5428905.8618 126.71 456169.4234 5428916.605 126.71</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_851992aa-6f56-4111-907c-6eb940b0a829_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>142.775</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>851992aa-6f56-4111-907c-6eb940b0a829</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854887_163">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_851992aa-6f56-4111-907c-6eb940b0a829_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32617">
                      <gml:posList srsDimension="3">456169.45 5428924.06 115.1082 456169.45 5428924.06 126.86 456178.68 5428931.96 126.86 456178.68 5428931.96 115.1082 456169.45 5428924.06 115.1082</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_434c226d-4940-4ca1-b40b-a7d9463cb58f_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>40.313</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>434c226d-4940-4ca1-b40b-a7d9463cb58f</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854887_164">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_434c226d-4940-4ca1-b40b-a7d9463cb58f_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32618">
                      <gml:posList srsDimension="3">456171.82 5428921.579999999 115.1082 456171.82 5428921.579999999 123.44 456171.82 5428921.579999999 126.86 456169.45 5428924.06 126.86 456169.45 5428924.06 115.1082 456171.82 5428921.579999999 115.1082</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_d4fa3632-90d9-4fb6-8ada-3ff0efebb275_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>5.062</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>d4fa3632-90d9-4fb6-8ada-3ff0efebb275</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854887_165">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_d4fa3632-90d9-4fb6-8ada-3ff0efebb275_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32619">
                      <gml:posList srsDimension="3">456170.0918 5428869.4646 118.4443 456170.0918 5428869.4646 124.32 456170.6927 5428870.082 124.32 456170.6927 5428870.082 118.4443 456170.0918 5428869.4646 118.4443</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_ea310928-174f-4ab1-883e-15c522212cda_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>5.549</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>ea310928-174f-4ab1-883e-15c522212cda</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854887_166">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_ea310928-174f-4ab1-883e-15c522212cda_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32620">
                      <gml:posList srsDimension="3">456176.4321 5428872.2444 123.38 456176.4321 5428872.2444 124.32 456172.39 5428867.949999999 124.32 456172.39 5428867.949999999 123.3783 456176.4321 5428872.2444 123.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_bc84c5ff-96e2-41cc-ab85-2d7110e9cd2b_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.755</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>bc84c5ff-96e2-41cc-ab85-2d7110e9cd2b</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854887_167">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_bc84c5ff-96e2-41cc-ab85-2d7110e9cd2b_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32621">
                      <gml:posList srsDimension="3">456172.39 5428867.949999999 115.1082 456172.39 5428867.949999999 123.3783 456172.39 5428867.949999999 124.32 456172.03 5428867.58 124.32 456172.03 5428867.58 115.1082 456172.39 5428867.949999999 115.1082</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_5004123b-60f5-43e2-81f8-05635584129e_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>12.082</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>5004123b-60f5-43e2-81f8-05635584129e</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854888_168">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_5004123b-60f5-43e2-81f8-05635584129e_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32622">
                      <gml:posList srsDimension="3">456178.0419 5428880.1884 126.71 456178.0419 5428880.1884 127.8 456170.311 5428872.2427 127.7997 456170.311 5428872.2427 126.71 456178.0419 5428880.1884 126.71</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_32887106-5a2d-4a4c-a89f-8ff13e0e04d2_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>30.52</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>32887106-5a2d-4a4c-a89f-8ff13e0e04d2</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854888_169">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_32887106-5a2d-4a4c-a89f-8ff13e0e04d2_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32623">
                      <gml:posList srsDimension="3">456177.8396 5428914.992 123.44 456177.8396 5428914.992 126.86 456171.82 5428921.579999999 126.86 456171.82 5428921.579999999 123.44 456177.8396 5428914.992 123.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_c674232e-678c-4184-be59-0c378e8de095_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>205.494</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>c674232e-678c-4184-be59-0c378e8de095</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854888_170">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_c674232e-678c-4184-be59-0c378e8de095_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32624">
                      <gml:posList srsDimension="3">456182.8 5428855.25 115.1082 456182.8 5428855.25 130.03 456172.06 5428863.87 130.03 456172.06 5428863.87 115.1082 456182.8 5428855.25 115.1082</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_3f4ac2b4-02af-4b8f-9aeb-15431b4925fd_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>42.063</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>3f4ac2b4-02af-4b8f-9aeb-15431b4925fd</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854888_171">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_3f4ac2b4-02af-4b8f-9aeb-15431b4925fd_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32625">
                      <gml:posList srsDimension="3">456174.0279 5428866.3617 122.9923 456174.0279 5428866.3617 130.031 456177.7313 5428871.050299999 130.03299999999996 456177.7313 5428871.050299999 122.99159999999999 456174.0279 5428866.3617 122.9923</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_8a4f3d1d-897f-4651-a1b3-9b27cb19ab32_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.194</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>8a4f3d1d-897f-4651-a1b3-9b27cb19ab32</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854888_172">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_8a4f3d1d-897f-4651-a1b3-9b27cb19ab32_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32626">
                      <gml:posList srsDimension="3">456177.7313 5428871.050299999 122.99159999999999 456177.7313 5428871.050299999 130.03299999999996 456177.7548 5428871.0646 130.039 456177.7548 5428871.0646 126.71 456177.7548 5428871.0646 122.99 456177.7313 5428871.050299999 122.99159999999999</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_94649681-4d39-4873-b8c5-99aa07184330_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>14.355</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>94649681-4d39-4873-b8c5-99aa07184330</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854888_173">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_94649681-4d39-4873-b8c5-99aa07184330_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32627">
                      <gml:posList srsDimension="3">456175.87309999997 5428875.4064 123.27 456175.87309999997 5428875.4064 126.71 456178.9872 5428872.6288 126.71 456178.9872 5428872.6288 123.27 456175.87309999997 5428875.4064 123.27</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_dd9b7f09-1c74-48c4-993e-50156d4583ab_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>13.702</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>dd9b7f09-1c74-48c4-993e-50156d4583ab</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854888_174">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_dd9b7f09-1c74-48c4-993e-50156d4583ab_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32628">
                      <gml:posList srsDimension="3">456175.02569999994 5428911.924499999 123.44 456175.02569999994 5428911.924499999 126.71 456178.1653 5428914.6994 126.71 456178.1653 5428914.6994 123.44 456175.02569999994 5428911.924499999 123.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_6d91a27d-06a9-4945-8c9e-a5fb6deb15ec_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>10.609</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>6d91a27d-06a9-4945-8c9e-a5fb6deb15ec</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854888_175">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_6d91a27d-06a9-4945-8c9e-a5fb6deb15ec_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32629">
                      <gml:posList srsDimension="3">456179.5852 5428913.4236 124.34 456179.5852 5428913.4236 126.71 456176.1807 5428910.5169 126.71 456176.1807 5428910.5169 124.34 456179.5852 5428913.4236 124.34</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_560189cf-bc7d-4ccb-8bbf-16ea3c37491a_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>216.844</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>560189cf-bc7d-4ccb-8bbf-16ea3c37491a</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854888_176">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_560189cf-bc7d-4ccb-8bbf-16ea3c37491a_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32630">
                      <gml:posList srsDimension="3">456178.68 5428931.96 115.1082 456178.68 5428931.96 126.86 456191.98 5428919.17 126.86 456191.98 5428919.17 115.1082 456178.68 5428931.96 115.1082</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_ab3bc777-4c4e-4963-8aae-4bc5a72a9e46_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.814</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>ab3bc777-4c4e-4963-8aae-4bc5a72a9e46</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854888_177">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_ab3bc777-4c4e-4963-8aae-4bc5a72a9e46_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32631">
                      <gml:posList srsDimension="3">456180.9335 5428879.5244 123.27 456180.9335 5428879.5244 124.34 456180.32639999996 5428879.983499999 124.34 456180.32639999996 5428879.983499999 123.27 456180.9335 5428879.5244 123.27</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_abd8962e-bc9d-46ae-b4e5-f5067c4b1e81_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>535.306</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>abd8962e-bc9d-46ae-b4e5-f5067c4b1e81</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854888_178">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_abd8962e-bc9d-46ae-b4e5-f5067c4b1e81_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32632">
                      <gml:posList srsDimension="3">456205.23 5428883.25 115.1082 456205.23 5428883.25 130.0282 456182.8 5428855.25 130.03 456182.8 5428855.25 115.1082 456205.23 5428883.25 115.1082</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_939551cc-f04c-46d2-8b7e-bab4fdc423bd_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>17.821</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>939551cc-f04c-46d2-8b7e-bab4fdc423bd</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854888_179">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_939551cc-f04c-46d2-8b7e-bab4fdc423bd_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32633">
                      <gml:posList srsDimension="3">456194.81999999995 5428900.239999999 115.1082 456194.81999999995 5428900.239999999 126.86 456193.68 5428901.24 126.86 456193.68 5428901.24 115.1082 456194.81999999995 5428900.239999999 115.1082</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_c9c3a20f-d9fa-466f-aede-bbb662ee2bce_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>17.744</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>c9c3a20f-d9fa-466f-aede-bbb662ee2bce</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854888_180">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_c9c3a20f-d9fa-466f-aede-bbb662ee2bce_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32634">
                      <gml:posList srsDimension="3">456193.68 5428901.24 115.1082 456193.68 5428901.24 126.86 456192.54 5428902.23 126.86 456192.54 5428902.23 115.1082 456193.68 5428901.24 115.1082</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_fc8f6af2-8851-44fb-8f1f-2f680e7898d2_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>17.733</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>fc8f6af2-8851-44fb-8f1f-2f680e7898d2</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854889_181">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_fc8f6af2-8851-44fb-8f1f-2f680e7898d2_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32635">
                      <gml:posList srsDimension="3">456192.54 5428902.23 115.1082 456192.54 5428902.23 126.86 456191.41 5428903.23 126.86 456191.41 5428903.23 115.1082 456192.54 5428902.23 115.1082</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_4bc06476-9af6-49be-a355-cb76e8c02b76_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>17.655</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>4bc06476-9af6-49be-a355-cb76e8c02b76</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854889_182">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_4bc06476-9af6-49be-a355-cb76e8c02b76_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32636">
                      <gml:posList srsDimension="3">456191.41 5428903.23 115.1082 456191.41 5428903.23 126.86 456190.28 5428904.22 126.86 456190.28 5428904.22 115.1082 456191.41 5428903.23 115.1082</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_72fb0d87-b581-4e5e-84bb-0797f2d171e9_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>17.899</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>72fb0d87-b581-4e5e-84bb-0797f2d171e9</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854889_183">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_72fb0d87-b581-4e5e-84bb-0797f2d171e9_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32637">
                      <gml:posList srsDimension="3">456190.28 5428904.22 115.1082 456190.28 5428904.22 126.86 456189.14 5428905.23 126.86 456189.14 5428905.23 115.1082 456190.28 5428904.22 115.1082</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_61893f17-d433-4ea4-8bcc-e49b1852fa26_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>17.645</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>61893f17-d433-4ea4-8bcc-e49b1852fa26</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854889_184">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_61893f17-d433-4ea4-8bcc-e49b1852fa26_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32638">
                      <gml:posList srsDimension="3">456189.14 5428905.23 115.1082 456189.14 5428905.23 126.86 456188.01999999996 5428906.23 126.86 456188.01999999996 5428906.23 115.1082 456189.14 5428905.23 115.1082</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_bf8d6a35-2f20-460f-ba14-61d1ed20afc4_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>17.811</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>bf8d6a35-2f20-460f-ba14-61d1ed20afc4</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854889_185">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_bf8d6a35-2f20-460f-ba14-61d1ed20afc4_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32639">
                      <gml:posList srsDimension="3">456188.01999999996 5428906.23 115.1082 456188.01999999996 5428906.23 126.86 456186.89 5428907.24 126.86 456186.89 5428907.24 115.1082 456188.01999999996 5428906.23 115.1082</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_d12a96ff-0a95-4ead-b388-fbc65c0df449_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>141.028</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>d12a96ff-0a95-4ead-b388-fbc65c0df449</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854889_186">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_d12a96ff-0a95-4ead-b388-fbc65c0df449_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32640">
                      <gml:posList srsDimension="3">456191.98 5428919.17 115.1082 456191.98 5428919.17 126.86 456200.88 5428911.119999999 126.86 456200.88 5428911.119999999 115.1082 456191.98 5428919.17 115.1082</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_5624a4a2-d5fb-482c-b58f-d9aac3074570_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>11.34</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>5624a4a2-d5fb-482c-b58f-d9aac3074570</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854889_187">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_5624a4a2-d5fb-482c-b58f-d9aac3074570_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32641">
                      <gml:posList srsDimension="3">456198.8014 5428888.4412 126.1034 456198.8014 5428888.4412 130.03 456201.0942 5428886.6852 130.0301 456201.0942 5428886.6852 126.1034 456198.8014 5428888.4412 126.1034</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_65d53ac1-835f-4419-a095-ea47d2e58b2d_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>12.918</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>65d53ac1-835f-4419-a095-ea47d2e58b2d</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854889_188">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_65d53ac1-835f-4419-a095-ea47d2e58b2d_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32642">
                      <gml:posList srsDimension="3">456202.5869 5428893.6841 115.1082 456202.5869 5428893.6841 126.1034 456202.5869 5428893.6841 126.86 456201.72 5428894.36 126.86 456201.72 5428894.36 115.1082 456202.5869 5428893.6841 115.1082</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_0e893f3b-2ed7-4a93-b92e-4638479f97a6_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>17.77</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>0e893f3b-2ed7-4a93-b92e-4638479f97a6</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854889_189">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_0e893f3b-2ed7-4a93-b92e-4638479f97a6_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32643">
                      <gml:posList srsDimension="3">456201.72 5428894.36 115.1082 456201.72 5428894.36 126.86 456200.56 5428895.33 126.86 456200.56 5428895.33 115.1082 456201.72 5428894.36 115.1082</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_a661349e-a2b7-46cc-bfde-20c38d247453_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>17.833</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>a661349e-a2b7-46cc-bfde-20c38d247453</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854889_190">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_a661349e-a2b7-46cc-bfde-20c38d247453_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32644">
                      <gml:posList srsDimension="3">456197.1099999999 5428898.27 115.1082 456197.1099999999 5428898.27 126.86 456195.96 5428899.26 126.86 456195.96 5428899.26 115.1082 456197.1099999999 5428898.27 115.1082</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_4c7ce4c3-c629-4ff4-baaa-c59eaa6c7486_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.452</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>4c7ce4c3-c629-4ff4-baaa-c59eaa6c7486</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854889_191">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_4c7ce4c3-c629-4ff4-baaa-c59eaa6c7486_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32645">
                      <gml:posList srsDimension="3">456202.87 5428893.39 115.1082 456202.87 5428893.39 126.1034 456202.5605 5428893.651099999 126.1034 456202.5605 5428893.651099999 115.1082 456202.87 5428893.39 115.1082</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_ee1558c8-b850-47e5-990f-8b26925fbe66_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.465</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>ee1558c8-b850-47e5-990f-8b26925fbe66</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854889_192">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_ee1558c8-b850-47e5-990f-8b26925fbe66_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32646">
                      <gml:posList srsDimension="3">456202.5605 5428893.651099999 115.1082 456202.5605 5428893.651099999 126.1034 456202.5869 5428893.6841 126.1034 456202.5869 5428893.6841 115.1082 456202.5605 5428893.651099999 115.1082</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_4885cc53-586e-4424-967f-5d65be6205b6_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.462</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>4885cc53-586e-4424-967f-5d65be6205b6</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854889_193">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_4885cc53-586e-4424-967f-5d65be6205b6_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32647">
                      <gml:posList srsDimension="3">456205.06 5428891.57 126.1034 456205.06 5428891.57 126.86 456202.5869 5428893.6841 126.86 456202.5869 5428893.6841 126.1034 456205.06 5428891.57 126.1034</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_b5c2a393-9d28-434f-af64-ed7d9a616100_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>86.579</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>b5c2a393-9d28-434f-af64-ed7d9a616100</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854889_194">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_b5c2a393-9d28-434f-af64-ed7d9a616100_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32648">
                      <gml:posList srsDimension="3">456210.87 5428887.04 115.1082 456210.87 5428887.04 126.86 456205.06 5428891.57 126.86 456205.06 5428891.57 126.1034 456205.06 5428891.57 115.1082 456210.87 5428887.04 115.1082</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_f964a94b-aad1-4adb-9767-721d08c08380_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>41.79</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>f964a94b-aad1-4adb-9767-721d08c08380</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854889_195">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_f964a94b-aad1-4adb-9767-721d08c08380_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32649">
                      <gml:posList srsDimension="3">456218.36 5428896.53 115.1082 456218.36 5428896.53 126.86 456216.13 5428893.76 126.86 456216.13 5428893.76 115.1082 456218.36 5428896.53 115.1082</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_10157a67-b9b1-4926-9db7-94dffb63878e_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>100.288</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>10157a67-b9b1-4926-9db7-94dffb63878e</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854890_196">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_10157a67-b9b1-4926-9db7-94dffb63878e_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32650">
                      <gml:posList srsDimension="3">456216.13 5428893.76 115.1082 456216.13 5428893.76 126.86 456210.87 5428887.04 126.86 456210.87 5428887.04 115.1082 456216.13 5428893.76 115.1082</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_855f338b-e125-4ff8-896f-389d54b0bf16_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>134.349</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>90.0</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>126.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>-1.0</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>855f338b-e125-4ff8-896f-389d54b0bf16</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854890_197">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_855f338b-e125-4ff8-896f-389d54b0bf16_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32651">
                      <gml:posList srsDimension="3">456171.82 5428921.579999999 126.86 456177.8396 5428914.992 126.86 456178.1653 5428914.6994 126.86 456179.5852 5428913.4236 126.86 456179.93139999994 5428913.112599999 126.86 456184.4363 5428909.065 126.86 456184.65 5428909.26 126.86 456184.9675 5428908.9737 126.86 456185.77 5428908.25 126.86 456186.89 5428907.24 126.86 456188.01999999996 5428906.23 126.86 456189.14 5428905.23 126.86 456190.28 5428904.22 126.86 456191.41 5428903.23 126.86 456192.54 5428902.23 126.86 456193.68 5428901.24 126.86 456194.81999999995 5428900.239999999 126.86 456195.39879999997 5428899.7424 126.86 456195.96 5428899.26 126.86 456197.1099999999 5428898.27 126.86 456198.26 5428897.289999999 126.86 456199.41 5428896.31 126.86 456200.56 5428895.33 126.86 456201.72 5428894.36 126.86 456202.5869 5428893.6841 126.86 456205.06 5428891.57 126.86 456210.87 5428887.04 126.86 456216.13 5428893.76 126.86 456218.36 5428896.53 126.86 456214.2243 5428899.9819 126.86 456208.9447 5428904.3887 126.86 456200.88 5428911.119999999 126.86 456191.98 5428919.17 126.86 456178.68 5428931.96 126.86 456169.45 5428924.06 126.86 456171.82 5428921.579999999 126.86</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                  <gml:interior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32652">
                      <gml:posList srsDimension="3">456178.1415 5428930.7576 126.86 456185.5391 5428923.597799999 126.86 456188.3599 5428920.8851 126.86 456191.2401 5428918.1154 126.86 456200.1467 5428910.0594 126.86 456208.2114 5428903.3281 126.86 456213.0839 5428899.2612 126.86 456216.8117 5428896.1464 126.86 456210.7259 5428888.3697 126.86 456205.5826 5428892.3799 126.86 456202.9344999999 5428894.6436 126.86 456195.4598 5428900.471499999 126.86 456185.3058 5428909.5284 126.86 456180.7898 5428913.8712 126.86 456178.93169999996 5428915.6581 126.86 456173.0601 5428921.57 126.86 456170.8366 5428923.9053 126.86 456178.1415 5428930.7576 126.86</gml:posList>
                    </gml:LinearRing>
                  </gml:interior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001U5f_2f7ea477-e092-4cbf-b4c1-e7364c52a933_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2307.865</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>2f7ea477-e092-4cbf-b4c1-e7364c52a933</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854890_198">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_2f7ea477-e092-4cbf-b4c1-e7364c52a933_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32653">
                      <gml:posList srsDimension="3">456216.13 5428893.76 115.1082 456210.87 5428887.04 115.1082 456205.06 5428891.57 115.1082 456201.0942 5428886.6852 115.1082 456200.95999999996 5428886.52 115.1082 456201.3314 5428886.2356 115.1082 456205.0907 5428883.3566 115.1082 456205.23 5428883.25 115.1082 456182.8 5428855.25 115.1082 456172.06 5428863.87 115.1082 456173.7171 5428865.9455 115.1082 456174.0399999999 5428866.35 115.1082 456174.0279 5428866.3617 115.1082 456172.4856 5428867.8573 115.1082 456172.39 5428867.949999999 115.1082 456172.03 5428867.58 115.1082 456170.0918 5428869.4646 115.1082 456168.8154 5428870.7056 115.1082 456142.03 5428896.75 115.1082 456169.52 5428920.22 115.1082 456169.84 5428919.83 115.1082 456171.82 5428921.579999999 115.1082 456169.45 5428924.06 115.1082 456178.68 5428931.96 115.1082 456191.98 5428919.17 115.1082 456200.88 5428911.119999999 115.1082 456208.9447 5428904.3887 115.1082 456214.2243 5428899.9819 115.1082 456218.36 5428896.53 115.1082 456216.13 5428893.76 115.1082</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                  <gml:interior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982242_32654">
                      <gml:posList srsDimension="3">456202.5605 5428893.651099999 115.1082 456201.72 5428894.36 115.1082 456200.56 5428895.33 115.1082 456199.41 5428896.31 115.1082 456198.26 5428897.289999999 115.1082 456197.1099999999 5428898.27 115.1082 456195.96 5428899.26 115.1082 456195.39879999997 5428899.7424 115.1082 456194.81999999995 5428900.239999999 115.1082 456193.68 5428901.24 115.1082 456192.54 5428902.23 115.1082 456191.41 5428903.23 115.1082 456190.28 5428904.22 115.1082 456189.14 5428905.23 115.1082 456188.01999999996 5428906.23 115.1082 456186.89 5428907.24 115.1082 456185.77 5428908.25 115.1082 456184.9675 5428908.9737 115.1082 456184.65 5428909.26 115.1082 456184.4363 5428909.065 115.1082 456182.48 5428907.28 115.1082 456183.71 5428906.19 115.1082 456169.0999999999 5428893.27 115.1082 456180.1048 5428882.3086 115.1082 456181.7899999999 5428880.63 115.1082 456180.9335 5428879.5244 115.1082 456180.69 5428879.21 115.1082 456182.7487 5428877.4026 115.1082 456182.82 5428877.34 115.1082 456189.8877 5428886.0266 115.1082 456194.3597 5428891.523 115.1082 456194.52 5428891.72 115.1082 456198.27009999997 5428888.6859 115.1082 456198.71 5428888.33 115.1082 456198.8014 5428888.4412 115.1082 456202.87 5428893.39 115.1082 456202.5605 5428893.651099999 115.1082</gml:posList>
                    </gml:LinearRing>
                  </gml:interior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_33a5ffaa-6c38-483e-8029-8f46852a87e1_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>202.232</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>90.0</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>126.71</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.71</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>-1.0</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>33a5ffaa-6c38-483e-8029-8f46852a87e1</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854890_199">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_33a5ffaa-6c38-483e-8029-8f46852a87e1_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32655">
                      <gml:posList srsDimension="3">456169.4163 5428871.3231 126.71 456170.6927 5428870.082 126.71 456174.4857 5428873.9805 126.71 456176.4321 5428872.2444 126.71 456177.7548 5428871.0646 126.71 456178.9872 5428872.6288 126.71 456175.87309999997 5428875.4064 126.71 456180.32639999996 5428879.983499999 126.71 456179.0501 5428881.2245 126.71 456166.6091 5428893.3213 126.71 456180.3355 5428905.453199999 126.71 456176.1807 5428910.5169 126.71 456179.5852 5428913.4236 126.71 456178.1653 5428914.6994 126.71 456175.02569999994 5428911.924499999 126.71 456169.779 5428918.3189 126.71 456143.86819999997 5428896.164399999 126.71 456169.4163 5428871.3231 126.71</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                  <gml:interior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32656">
                      <gml:posList srsDimension="3">456157.0798 5428906.066399999 126.71 456169.4234 5428916.605 126.71 456178.2383 5428905.8618 126.71 456165.89469999995 5428895.3233 126.71 456164.07459999993 5428893.7693 126.71 456165.2877 5428892.5897 126.71 456178.0419 5428880.1884 126.71 456170.311 5428872.2427 126.71 456157.5568 5428884.6441 126.71 456145.6026 5428896.2676 126.71 456157.0798 5428906.066399999 126.71</gml:posList>
                    </gml:LinearRing>
                  </gml:interior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_6a97ee1f-fe3b-42e8-97d1-f2386c69f4d3_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>81.279</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>45.624</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.88</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>223.88</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>6a97ee1f-fe3b-42e8-97d1-f2386c69f4d3</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854890_200">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_6a97ee1f-fe3b-42e8-97d1-f2386c69f4d3_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32657">
                      <gml:posList srsDimension="3">456180.7898 5428913.8712 126.86 456185.3058 5428909.5284 126.86 456188.2496 5428913.812 131.88 456185.0036 5428916.9335 131.88 456182.5486 5428919.2944 131.8742 456178.2922 5428923.3876 131.88 456178.93169999996 5428915.6581 126.86 456180.7898 5428913.8712 126.86</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_248e305e-44d6-4fdc-8f36-17449bb98c4c_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>342.118</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>248e305e-44d6-4fdc-8f36-17449bb98c4c</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854890_201">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_248e305e-44d6-4fdc-8f36-17449bb98c4c_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32658">
                      <gml:posList srsDimension="3">456168.8154 5428870.7056 115.1082 456170.0918 5428869.4646 115.1082 456172.03 5428867.58 115.1082 456172.03 5428867.58 124.32 456170.0918 5428869.4646 124.32 456170.0918 5428869.4646 118.4443 456168.8154 5428870.7056 118.4443 456168.8154 5428870.7056 123.44 456142.03 5428896.75 123.44 456142.03 5428896.75 115.1082 456168.8154 5428870.7056 115.1082</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_68e3c756-0393-46d8-b18b-ae38dd2331db_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>279.017</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>68e3c756-0393-46d8-b18b-ae38dd2331db</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854890_202">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_68e3c756-0393-46d8-b18b-ae38dd2331db_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32659">
                      <gml:posList srsDimension="3">456182.82 5428877.34 130.0948 456189.8877 5428886.0266 130.1995 456194.3597 5428891.523 130.1601 456194.52 5428891.72 130.03 456194.52 5428891.72 115.1082 456194.3597 5428891.523 115.1082 456189.8877 5428886.0266 115.1082 456182.82 5428877.34 115.1082 456182.82 5428877.34 130.0948</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_4a9e7487-50ec-41b0-b0ac-7bcc7b9467af_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>18.672</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>4a9e7487-50ec-41b0-b0ac-7bcc7b9467af</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854890_203">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_4a9e7487-50ec-41b0-b0ac-7bcc7b9467af_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32660">
                      <gml:posList srsDimension="3">456172.4856 5428867.8573 115.1082 456174.0279 5428866.3617 115.1082 456174.0399999999 5428866.35 115.1082 456174.0399999999 5428866.35 130.0414 456174.0279 5428866.3617 130.031 456174.0279 5428866.3617 122.9923 456172.4856 5428867.8573 123.349 456172.39 5428867.949999999 123.3783 456172.39 5428867.949999999 115.1082 456172.4856 5428867.8573 115.1082</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_13ad000c-b355-4239-92c1-81be83999ec8_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>18.218</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>13ad000c-b355-4239-92c1-81be83999ec8</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854890_204">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_13ad000c-b355-4239-92c1-81be83999ec8_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32661">
                      <gml:posList srsDimension="3">456178.1653 5428914.6994 123.44 456178.1653 5428914.6994 126.71 456179.5852 5428913.4236 126.71 456179.5852 5428913.4236 124.34 456179.93139999994 5428913.112599999 124.34 456184.4363 5428909.065 124.34 456184.4363 5428909.065 126.86 456179.93139999994 5428913.112599999 126.86 456179.5852 5428913.4236 126.86 456178.1653 5428914.6994 126.86 456177.8396 5428914.992 126.86 456177.8396 5428914.992 123.44 456178.1653 5428914.6994 123.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_4a1a69d7-7bf8-4e78-8f5c-5fb36a49e627_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>35.447</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>4a1a69d7-7bf8-4e78-8f5c-5fb36a49e627</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854890_205">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_4a1a69d7-7bf8-4e78-8f5c-5fb36a49e627_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32662">
                      <gml:posList srsDimension="3">456184.9675 5428908.9737 115.1082 456185.77 5428908.25 115.1082 456186.89 5428907.24 115.1082 456186.89 5428907.24 126.86 456185.77 5428908.25 126.86 456184.9675 5428908.9737 126.86 456184.65 5428909.26 126.86 456184.65 5428909.26 115.1082 456184.9675 5428908.9737 115.1082</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_58be168e-1f24-4951-b9e9-d6f23536426d_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>80.758</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>58be168e-1f24-4951-b9e9-d6f23536426d</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854890_206">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_58be168e-1f24-4951-b9e9-d6f23536426d_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32663">
                      <gml:posList srsDimension="3">456201.3314 5428886.2356 130.1249 456205.0907 5428883.3566 130.1255 456205.23 5428883.25 130.0282 456205.23 5428883.25 115.1082 456205.0907 5428883.3566 115.1082 456201.3314 5428886.2356 115.1082 456200.95999999996 5428886.52 115.1082 456200.95999999996 5428886.52 130.1299 456201.3314 5428886.2356 130.1249</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_7088c27d-e454-4892-8933-9be9c58bc314_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>53.268</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>7088c27d-e454-4892-8933-9be9c58bc314</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854890_207">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_7088c27d-e454-4892-8933-9be9c58bc314_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32664">
                      <gml:posList srsDimension="3">456198.26 5428897.289999999 115.1082 456199.41 5428896.31 115.1082 456200.56 5428895.33 115.1082 456200.56 5428895.33 126.86 456199.41 5428896.31 126.86 456198.26 5428897.289999999 126.86 456197.1099999999 5428898.27 126.86 456197.1099999999 5428898.27 115.1082 456198.26 5428897.289999999 115.1082</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_8c013283-2855-4aef-980d-5ec6e5049ce9_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>267.574</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>8c013283-2855-4aef-980d-5ec6e5049ce9</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854890_208">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_8c013283-2855-4aef-980d-5ec6e5049ce9_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32665">
                      <gml:posList srsDimension="3">456208.9447 5428904.3887 126.86 456214.2243 5428899.9819 126.86 456218.36 5428896.53 126.86 456218.36 5428896.53 115.1082 456214.2243 5428899.9819 115.1082 456208.9447 5428904.3887 115.1082 456200.88 5428911.119999999 115.1082 456200.88 5428911.119999999 126.86 456208.9447 5428904.3887 126.86</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_0cfe39f5-ae62-47fa-b7b1-958b74ed6bf4_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>72.368</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>0cfe39f5-ae62-47fa-b7b1-958b74ed6bf4</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854890_209">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_0cfe39f5-ae62-47fa-b7b1-958b74ed6bf4_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32666">
                      <gml:posList srsDimension="3">456200.95999999996 5428886.52 115.1082 456201.0942 5428886.6852 115.1082 456205.06 5428891.57 115.1082 456205.06 5428891.57 126.1034 456201.0942 5428886.6852 126.1034 456201.0942 5428886.6852 130.0301 456200.95999999996 5428886.52 130.1299 456200.95999999996 5428886.52 115.1082</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_6a5cfa75-2b9d-4a8d-873f-634ba64f818e_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>72.595</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>6a5cfa75-2b9d-4a8d-873f-634ba64f818e</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854890_210">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_6a5cfa75-2b9d-4a8d-873f-634ba64f818e_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32667">
                      <gml:posList srsDimension="3">456198.71 5428888.33 130.1038 456198.8014 5428888.4412 130.03 456198.8014 5428888.4412 126.1034 456202.87 5428893.39 126.1034 456202.87 5428893.39 115.1082 456198.8014 5428888.4412 115.1082 456198.71 5428888.33 115.1082 456198.71 5428888.33 130.1038</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_602479c4-6d70-46d5-b331-0b5f19a02fdf_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>80.622</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>602479c4-6d70-46d5-b331-0b5f19a02fdf</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854890_211">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_602479c4-6d70-46d5-b331-0b5f19a02fdf_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32668">
                      <gml:posList srsDimension="3">456198.27009999997 5428888.6859 130.096 456198.71 5428888.33 130.1038 456198.71 5428888.33 115.1082 456198.27009999997 5428888.6859 115.1082 456194.52 5428891.72 115.1082 456194.52 5428891.72 130.03 456198.27009999997 5428888.6859 130.096</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_d304cb7f-7acd-472b-8550-49a5e3528dbb_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>17.667</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>d304cb7f-7acd-472b-8550-49a5e3528dbb</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854890_212">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_d304cb7f-7acd-472b-8550-49a5e3528dbb_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32669">
                      <gml:posList srsDimension="3">456195.39879999997 5428899.7424 115.1082 456195.96 5428899.26 115.1082 456195.96 5428899.26 126.86 456195.39879999997 5428899.7424 126.86 456194.81999999995 5428900.239999999 126.86 456194.81999999995 5428900.239999999 115.1082 456195.39879999997 5428899.7424 115.1082</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_8a6f4572-1243-4a64-adb8-d759764948e5_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>16.157</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>8a6f4572-1243-4a64-adb8-d759764948e5</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854890_213">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_8a6f4572-1243-4a64-adb8-d759764948e5_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32670">
                      <gml:posList srsDimension="3">456180.9335 5428879.5244 123.27 456180.69 5428879.21 123.27 456180.69 5428879.21 115.1082 456180.9335 5428879.5244 115.1082 456181.7899999999 5428880.63 115.1082 456181.7899999999 5428880.63 124.34 456180.9335 5428879.5244 124.34 456180.9335 5428879.5244 123.27</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_afb4a49d-4b72-41d8-b30d-4789714a04f0_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>12.481</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>afb4a49d-4b72-41d8-b30d-4789714a04f0</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854891_214">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_afb4a49d-4b72-41d8-b30d-4789714a04f0_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32671">
                      <gml:posList srsDimension="3">456176.4321 5428872.2444 124.32 456176.4321 5428872.2444 123.38 456177.7548 5428871.0646 122.99 456177.7548 5428871.0646 126.71 456176.4321 5428872.2444 126.71 456174.4857 5428873.9805 126.71 456174.4857 5428873.9805 124.32 456176.4321 5428872.2444 124.32</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_3ac686eb-4a5c-4219-9bb1-f3ac6cf24ba8_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>23.779</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>3ac686eb-4a5c-4219-9bb1-f3ac6cf24ba8</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854891_215">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_3ac686eb-4a5c-4219-9bb1-f3ac6cf24ba8_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32672">
                      <gml:posList srsDimension="3">456182.82 5428877.34 130.0948 456182.82 5428877.34 115.1082 456182.7487 5428877.4026 115.1082 456180.69 5428879.21 115.1082 456180.69 5428879.21 123.27 456182.7487 5428877.4026 123.27 456182.7487 5428877.4026 130.0357 456182.82 5428877.34 130.0948</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_6d197d17-a81c-423a-bbd5-9d70dada98df_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>47.756</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>6d197d17-a81c-423a-bbd5-9d70dada98df</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854891_216">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_6d197d17-a81c-423a-bbd5-9d70dada98df_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32673">
                      <gml:posList srsDimension="3">456178.9872 5428872.6288 126.71 456177.7548 5428871.0646 126.71 456177.7548 5428871.0646 130.039 456178.9872 5428872.6288 130.0382 456182.7487 5428877.4026 130.0357 456182.7487 5428877.4026 123.27 456178.9872 5428872.6288 123.27 456178.9872 5428872.6288 126.71</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_5f20e513-8305-406b-b93c-d6541d398c22_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>47.372</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>5f20e513-8305-406b-b93c-d6541d398c22</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854891_217">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_5f20e513-8305-406b-b93c-d6541d398c22_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32674">
                      <gml:posList srsDimension="3">456172.06 5428863.87 130.03 456173.7171 5428865.9455 130.0395 456174.0399999999 5428866.35 130.0414 456174.0399999999 5428866.35 115.1082 456173.7171 5428865.9455 115.1082 456172.06 5428863.87 115.1082 456172.06 5428863.87 130.03</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_909556c4-64ee-47b1-a080-eecc43b4fa7e_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>165.351</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>909556c4-64ee-47b1-a080-eecc43b4fa7e</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854891_218">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_909556c4-64ee-47b1-a080-eecc43b4fa7e_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32675">
                      <gml:posList srsDimension="3">456180.1048 5428882.3086 124.34 456181.7899999999 5428880.63 124.34 456181.7899999999 5428880.63 115.1082 456180.1048 5428882.3086 115.1082 456169.0999999999 5428893.27 115.1082 456169.0999999999 5428893.27 124.34 456180.1048 5428882.3086 124.34</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_04d27d33-c9f2-44ed-a12c-3f085a10552d_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>27.848</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>04d27d33-c9f2-44ed-a12c-3f085a10552d</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854891_219">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_04d27d33-c9f2-44ed-a12c-3f085a10552d_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32676">
                      <gml:posList srsDimension="3">456182.48 5428907.28 115.1082 456184.4363 5428909.065 115.1082 456184.65 5428909.26 115.1082 456184.65 5428909.26 126.86 456184.4363 5428909.065 126.86 456184.4363 5428909.065 124.34 456182.48 5428907.28 124.34 456182.48 5428907.28 115.1082</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_737d958a-4d59-4ea1-aca5-bc31ca53733f_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>45.345</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>737d958a-4d59-4ea1-aca5-bc31ca53733f</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854891_220">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_737d958a-4d59-4ea1-aca5-bc31ca53733f_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32677">
                      <gml:posList srsDimension="3">456179.0501 5428881.2245 126.71 456180.32639999996 5428879.983499999 126.71 456180.32639999996 5428879.983499999 124.34 456179.0501 5428881.2245 124.34 456166.6091 5428893.3213 124.34 456166.6091 5428893.3213 126.71 456179.0501 5428881.2245 126.71</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_febe60b2-f282-486a-a247-bfba8ddefdeb_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>20.304</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>febe60b2-f282-486a-a247-bfba8ddefdeb</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854891_221">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_febe60b2-f282-486a-a247-bfba8ddefdeb_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32678">
                      <gml:posList srsDimension="3">456164.07459999993 5428893.7693 126.71 456165.89469999995 5428895.3233 126.71 456178.2383 5428905.8618 126.71 456178.2383 5428905.8618 127.8 456165.89469999995 5428895.3233 127.8 456164.07459999993 5428893.7693 127.8039 456164.07459999993 5428893.7693 126.71</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_e3924178-cb6a-4d99-a680-09c3750c12f2_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>21.238</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e3924178-cb6a-4d99-a680-09c3750c12f2</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854891_222">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_e3924178-cb6a-4d99-a680-09c3750c12f2_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32679">
                      <gml:posList srsDimension="3">456165.2877 5428892.5897 127.8 456178.0419 5428880.1884 127.8 456178.0419 5428880.1884 126.71 456165.2877 5428892.5897 126.71 456164.07459999993 5428893.7693 126.71 456164.07459999993 5428893.7693 127.8039 456165.2877 5428892.5897 127.8</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_5d5f58c9-061d-47c9-a74d-469b1bc2ce5c_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.169</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>5d5f58c9-061d-47c9-a74d-469b1bc2ce5c</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854891_223">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_5d5f58c9-061d-47c9-a74d-469b1bc2ce5c_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32680">
                      <gml:posList srsDimension="3">456162.7277 5428889.9586 128.1311 456165.2877 5428892.5897 127.8 456162.7277 5428889.9586 128.07 456161.4223 5428888.6169 128.3006 456162.7277 5428889.9586 128.1311</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_68c4118a-6a61-4e48-aa17-586964e32702_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>1.813</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>68c4118a-6a61-4e48-aa17-586964e32702</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854891_224">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_68c4118a-6a61-4e48-aa17-586964e32702_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32681">
                      <gml:posList srsDimension="3">456160.0474 5428893.2251 128.5 456161.1476 5428894.1644 128.5 456162.67819999997 5428895.4712 128.5 456162.67819999997 5428895.4712 127.9507 456161.1476 5428894.1644 127.9512 456160.0474 5428893.2251 128.07 456160.0474 5428893.2251 128.5</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_2f957aef-f59e-49d2-b872-38c577002cc5_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>37.568</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>2f957aef-f59e-49d2-b872-38c577002cc5</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854891_225">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_2f957aef-f59e-49d2-b872-38c577002cc5_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32682">
                      <gml:posList srsDimension="3">456157.5568 5428884.6441 126.71 456170.311 5428872.2427 126.71 456170.311 5428872.2427 127.7997 456157.5568 5428884.6441 127.8003 456145.6026 5428896.2676 127.8001 456145.6026 5428896.2676 126.71 456157.5568 5428884.6441 126.71</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_da8b2e99-926f-45b1-a986-5646276cf8ba_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>34.14</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>da8b2e99-926f-45b1-a986-5646276cf8ba</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854891_226">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_da8b2e99-926f-45b1-a986-5646276cf8ba_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32683">
                      <gml:posList srsDimension="3">456145.6026 5428896.2676 127.8001 456157.0798 5428906.066399999 127.7999 456169.4234 5428916.605 127.8 456169.4234 5428916.605 126.71 456157.0798 5428906.066399999 126.71 456145.6026 5428896.2676 126.71 456145.6026 5428896.2676 127.8001</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001U5f_950458b0-f779-428f-bae8-e29d7c7c41eb_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>131.239</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>950458b0-f779-428f-bae8-e29d7c7c41eb</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854891_227">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_950458b0-f779-428f-bae8-e29d7c7c41eb_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32684">
                      <gml:posList srsDimension="3">456169.4163 5428871.3231 123.44 456169.4163 5428871.3231 118.4443 456170.6927 5428870.082 118.4443 456170.6927 5428870.082 124.32 456170.6927 5428870.082 126.71 456169.4163 5428871.3231 126.71 456143.86819999997 5428896.164399999 126.71 456143.86819999997 5428896.164399999 123.44 456169.4163 5428871.3231 123.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_6b79e846-3db3-492d-b584-8e6027232cde_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>113.964</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>46.128</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.88</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>39.851</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>6b79e846-3db3-492d-b584-8e6027232cde</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854891_228">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_6b79e846-3db3-492d-b584-8e6027232cde_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32685">
                      <gml:posList srsDimension="3">456205.2489 5428898.9993 131.88 456208.4051 5428896.3649 131.88 456213.0839 5428899.2612 126.86 456208.2114 5428903.3281 126.86 456200.1467 5428910.0594 126.86 456197.1842 5428905.7306 131.88 456205.2489 5428898.9993 131.88</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_f9ff1116-c656-40d3-b3d9-38b9a6a5343d_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>7.106</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>62.861</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>130.83</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.03</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>37.445</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>f9ff1116-c656-40d3-b3d9-38b9a6a5343d</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854891_229">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_f9ff1116-c656-40d3-b3d9-38b9a6a5343d_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32686">
                      <gml:posList srsDimension="3">456195.097 5428889.3123 130.82999999999998 456201.0942 5428886.6852 130.0301 456198.8014 5428888.4412 130.03 456198.71 5428888.33 130.1038 456198.27009999997 5428888.6859 130.096 456194.52 5428891.72 130.03 456194.3597 5428891.523 130.1601 456195.097 5428889.3123 130.82999999999998</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_e6c5ee89-ae83-4053-85cc-08d84ec8f833_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>57.015</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>45.878</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.88</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>43.859</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e6c5ee89-ae83-4053-85cc-08d84ec8f833</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854891_230">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_e6c5ee89-ae83-4053-85cc-08d84ec8f833_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32687">
                      <gml:posList srsDimension="3">456185.0036 5428916.9335 131.88 456188.2496 5428913.812 131.88 456191.2401 5428918.1154 126.86 456188.3599 5428920.8851 126.86 456185.5391 5428923.597799999 126.86 456182.5486 5428919.2944 131.8742 456185.0036 5428916.9335 131.88</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_87aed47a-e341-4a13-998d-bd0e51aa8e01_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>23.241</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>58.086</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>130.83</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.03</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>308.273</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>87aed47a-e341-4a13-998d-bd0e51aa8e01</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854892_231">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_87aed47a-e341-4a13-998d-bd0e51aa8e01_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32688">
                      <gml:posList srsDimension="3">456173.7811 5428863.9775 130.82999999999998 456189.8877 5428886.0266 130.1995 456182.82 5428877.34 130.0948 456182.7487 5428877.4026 130.0357 456178.9872 5428872.6288 130.0382 456177.7548 5428871.0646 130.039 456177.7313 5428871.050299999 130.03299999999996 456174.0279 5428866.3617 130.031 456174.0399999999 5428866.35 130.0414 456173.7171 5428865.9455 130.0395 456172.06 5428863.87 130.03 456173.7811 5428863.9775 130.82999999999998</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_dbc5e450-2a08-4bfa-96f9-c5d0c8ded7af_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>96.798</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>90.0</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>124.34</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>124.34</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>-1.0</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>dbc5e450-2a08-4bfa-96f9-c5d0c8ded7af</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854892_232">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_dbc5e450-2a08-4bfa-96f9-c5d0c8ded7af_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32689">
                      <gml:posList srsDimension="3">456179.0501 5428881.2245 124.34 456180.32639999996 5428879.983499999 124.34 456180.9335 5428879.5244 124.34 456181.7899999999 5428880.63 124.34 456180.1048 5428882.3086 124.34 456169.0999999999 5428893.27 124.34 456183.71 5428906.19 124.34 456182.48 5428907.28 124.34 456184.4363 5428909.065 124.34 456179.93139999994 5428913.112599999 124.34 456179.5852 5428913.4236 124.34 456176.1807 5428910.5169 124.34 456180.3355 5428905.453199999 124.34 456166.6091 5428893.3213 124.34 456179.0501 5428881.2245 124.34</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_80695755-821a-4c8f-8515-aefd66f82264_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.648</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>85.281</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>128.3</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>127.951</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>49.373</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>80695755-821a-4c8f-8515-aefd66f82264</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854892_233">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_80695755-821a-4c8f-8515-aefd66f82264_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32690">
                      <gml:posList srsDimension="3">456159.6975 5428893.6516 128.07 456160.0474 5428893.2251 128.07 456161.1476 5428894.1644 127.9512 456159.0431 5428894.449099999 128.0678 456154.8295 5428895.0192 128.2999 456159.6975 5428893.6516 128.07</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_e3221a9c-ca7d-43cd-b24a-9528d669187f_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>60.463</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>85.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>128.3</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>127.95</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>139.484</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e3221a9c-ca7d-43cd-b24a-9528d669187f</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854892_234">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_e3221a9c-ca7d-43cd-b24a-9528d669187f_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32691">
                      <gml:posList srsDimension="3">456159.0431 5428894.449099999 128.0678 456161.1476 5428894.1644 127.9512 456162.67819999997 5428895.4712 127.9507 456164.4982999999 5428897.0251 127.9502 456170.8037 5428908.6489 128.3 456161.4872 5428900.6948 128.30009999999996 456154.8295 5428895.0192 128.2999 456159.0431 5428894.449099999 128.0678</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_a6b9b5ab-a638-47ea-97ba-56c9e38731ae_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>155.446</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>84.843</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>128.301</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>127.8</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>224.187</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>a6b9b5ab-a638-47ea-97ba-56c9e38731ae</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854892_235">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_a6b9b5ab-a638-47ea-97ba-56c9e38731ae_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32692">
                      <gml:posList srsDimension="3">456157.5568 5428884.6441 127.8003 456170.311 5428872.2427 127.7997 456170.205 5428880.0772 128.3 456161.4223 5428888.6169 128.3006 456161.4116 5428888.6058 128.2997 456154.8295 5428895.0192 128.2999 456145.6026 5428896.2676 127.8001 456157.5568 5428884.6441 127.8003</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001U5f_c3ed0766-55ae-4c1d-9792-d343d1e1f084_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>182.157</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>85.882</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>128.3</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>127.8</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>319.518</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>c3ed0766-55ae-4c1d-9792-d343d1e1f084</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854892_236">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001U5f_c3ed0766-55ae-4c1d-9792-d343d1e1f084_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941982243_32693">
                      <gml:posList srsDimension="3">456154.8295 5428895.0192 128.2999 456161.4872 5428900.6948 128.30009999999996 456170.8037 5428908.6489 128.3 456169.4234 5428916.605 127.8 456157.0798 5428906.066399999 127.7999 456145.6026 5428896.2676 127.8001 456154.8295 5428895.0192 128.2999</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:address xlink:href="#UUID_f7ab8c32-1b2b-4e72-9171-36ff4014dcbc"/>
    </bldg:Building>
  </core:cityObjectMember>
</core:CityModel>