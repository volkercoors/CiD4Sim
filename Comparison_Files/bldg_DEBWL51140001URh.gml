<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<core:CityModel xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:tex="http://www.opengis.net/citygml/texturedsurface/2.0" xmlns:core="http://www.opengis.net/citygml/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:frn="http://www.opengis.net/citygml/cityfurniture/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd http://www.opengis.net/citygml/appearance/2.0 http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/cityobjectgroup/2.0 http://schemas.opengis.net/citygml/cityobjectgroup/2.0/cityObjectGroup.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/cityfurniture/2.0 http://schemas.opengis.net/citygml/cityfurniture/2.0/cityFurniture.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd">
  <core:cityObjectMember>
    <bldg:Building gml:id="bldg_DEBWL51140001URh">
      <gml:name>Landeskreditbank</gml:name>
      <gml:boundedBy>
        <gml:Envelope srsName="urn:ogc:def:crs:EPSG::25832" srsDimension="3">
          <gml:lowerCorner>456425.78 5428759.51 112.38</gml:lowerCorner>
          <gml:upperCorner>456494.41 5428816.34 133.51</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <core:creationDate>2019-06-28</core:creationDate>
      <core:externalReference>
        <core:informationSystem>http://www.karlsruhe.de/b3/bauen/geodaten/3dgis</core:informationSystem>
        <core:externalObject>
          <core:uri>DEBWL51140001URh</core:uri>
        </core:externalObject>
      </core:externalReference>
      <gen:doubleAttribute name="HoeheGrund">
        <gen:value>112.376</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="HoeheDach">
        <gen:value>133.51</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="NiedrigsteTraufeDesGebaeudes">
        <gen:value>112.38</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Dachtypen">
        <gen:value>FLACHDACH;PULTDACH;PULTDACH</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Dachkodierungsliste">
        <gen:value>1000;2100;2100</gen:value>
      </gen:stringAttribute>
      <gen:doubleAttribute name="Volumen">
        <gen:value>37378.319</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Verfahren">
        <gen:value>BREC_2000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Objektart">
        <gen:value>Hauptgebaeude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Strassenschluessel">
        <gen:value>06770</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Strassenname">
        <gen:value>Schlossplatz</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Hausnummer">
        <gen:value>12</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="creationUser">
        <gen:value>Firma VCS</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Feinheit">
        <gen:value>Basis</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="AmtlicherGemeindeschluessel">
        <gen:value>08212000</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="DatenquelleDachhoehe">
        <gen:value>1000</gen:value>
      </gen:intAttribute>
      <gen:intAttribute name="DatenquelleLage">
        <gen:value>1000</gen:value>
      </gen:intAttribute>
      <gen:intAttribute name="DatenquelleBodenhoehe">
        <gen:value>1100</gen:value>
      </gen:intAttribute>
      <bldg:function>31001_3010</bldg:function>
      <bldg:roofType>3200</bldg:roofType>
      <bldg:measuredHeight uom="urn:adv:uom:m">21.134</bldg:measuredHeight>
      <bldg:lod2Solid>
        <gml:Solid>
          <gml:exterior>
            <gml:CompositeSurface>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_5c0353dc-7f36-44f4-bad2-d75e016461b5_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_88ecf406-b39b-498a-9d86-5c73d972c90c_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_bf4bf9ca-c376-45c0-9d70-1abecea6f749_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_59b553b5-9e9f-4603-b6db-613a0c10c849_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_307b1104-9f0a-4d8a-9057-8cdb8404086c_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_25c82333-8a84-4056-93ee-eee4207a9464_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_7ecebf9e-53ed-41ae-914c-498a917ab8b3_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_fbb21abb-0315-4bf1-9b42-b180298abfd4_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_b7558aa5-286d-413f-8dad-854be50658dc_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_63d43bfe-3f89-4f99-bab9-06af850458a0_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_606d4172-8520-420a-8944-9d154b60a11d_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_ab55747c-b8a8-4a39-9f04-9e441707f61e_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_dc5b226b-643e-473e-999f-8e9e596d1892_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_e6b00c8e-6a42-4d6c-9220-cecc2f3394ba_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_c117e987-1281-4f93-8088-34ece3efa9c6_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_5763da67-3093-44a8-89f7-19619bb4839f_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_37e379f1-c7e4-468b-9e31-8b1496f9c24f_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_bd7ea529-5749-4df8-abbc-a3721661ae0d_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_d4050bd2-df98-49e1-aef3-8f74c46f36f9_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_d6cf16f5-e153-4115-8c1e-6ddde9537444_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_82e340b9-8077-46d2-be62-408f899c90a1_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_673f8dfe-22ed-4900-b6b4-ed481b17c435_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_bc69458e-607a-4ac9-9fde-8963db6c197f_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_4142cec3-d821-4126-9208-52999a4c2180_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_54cd728a-99f2-4231-884e-db1806669413_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_c2857e26-1312-4f7a-813a-53b1ee174a02_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_641a6add-62f3-4013-aea5-7225e979ea82_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_498e8eb4-ec60-4a07-9f00-1e89e5784ac2_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_f9a7d976-afcf-488b-89c4-667360912764_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_25152dad-084a-4f1c-ab6c-b785e1964ce2_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_38598e29-faa4-43d1-a55e-3ae8639858be_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_31977c4b-04fc-4c0e-8b03-715067dbd717_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_989a5336-bb1b-4126-890c-8ca33ac62842_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_606a639c-37b7-4233-823b-c7c2add5fd4f_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_f8ccb4ee-6d24-4fce-b31f-67840f796449_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_14cbc07b-6c05-4c84-88ca-69147bb2d68a_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_608c60d8-d8d9-4705-8d17-5e48f327e8aa_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_88e67852-a42e-420e-a7c7-01574d8e3872_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_5b1ac83c-d9f7-4930-9390-94a6a867200b_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_6b9878e8-ed3c-46ca-8743-21738dc3fb39_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_6325c600-d19b-402f-8bc0-29373a15f310_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_b2d510de-9659-4521-9801-29ae6df229ba_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_288d8e17-e6c9-470a-8840-1e3fdc126569_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_5e546bdf-955a-48dd-91a0-c6becb4dceb9_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_474dc211-6edd-453e-81b4-f78392ce47a2_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_e64bb4fb-28de-431e-a14d-f9dea0b4c3b2_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_84123b74-0850-41d8-b0db-a4f791ad2540_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_9fd80578-7e90-4941-b80d-abddd691ed38_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_6e1ffb3a-fcdb-4a7a-ba28-7e66e5cfff38_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_55feb7e5-ad51-4390-917d-88591a78d35b_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_406d61ed-9c5c-4041-a82c-5dccb3553243_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_3cc06106-a1a9-46e2-99a2-f746cd6b1e97_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_9454a0d0-6d7b-44ea-a232-0ba16b213a1f_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_6d4f0758-19f1-40e1-a4cc-18953e754486_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_2cfa78dc-08ba-4399-bd00-02cd14ce6fe2_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_162a587a-2b53-4893-8d1e-aa9422735a18_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_f4aec7c1-77b4-498a-a3bb-9a44a617dea6_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_efdea9ea-a13a-4f65-a24c-295c9355a26f_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_03115c15-a2d8-427d-b38c-ba8170b68db5_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_ac3a7668-58cf-421c-a99d-580ab777e5f3_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_9eaeee2e-8b0c-4727-9775-c14b0b6a0a86_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_52d032dc-c603-4cb9-aec7-3de320cab1ee_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_f5ccd817-73c3-4a0d-9205-9125b96ccf63_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_223659c3-7966-40b3-902c-aaeca662558d_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_aee2ed5a-902f-4214-86bb-3a962ec884dc_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_64b7c10f-319e-426b-bfd1-413da72c43e5_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_4997958c-11d2-4faf-b8f7-70d964d54b6d_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_eedca241-0fc8-4b95-966d-6a7ef979f8a4_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_f27db8fd-2aae-40c5-a4f1-291c31b878b0_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_e33a0f39-ed12-4f5d-8c31-79ee5ccc81c6_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_c8ed6fd8-e9c7-4f1f-b078-9c0f64e58361_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_8d2237a4-012b-407b-8a30-ec09ee38658a_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_85998fd2-417b-4046-89e3-e26b11d61bf0_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_a124322c-28c4-412b-8ef6-e805c43c5cf3_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_143aafea-b4de-434b-9604-b3095e0d23c4_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_b87501be-92a7-4749-b0b0-357daded7a09_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_538a7dcf-83b7-49e7-a38a-9bac04bef601_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_d71fe779-4242-4e29-a030-41aa3c0f4a17_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_d80a2282-6de1-4ae5-9a2f-d62c9756c805_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_58741842-041c-41bb-89d0-8e937cbdab77_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_20558b6c-c1be-468c-a81b-b79c8ecc34ce_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_880e0dbf-c986-4b83-9a04-c7b86cef8b3b_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_12c6738e-d227-4fa9-8f1f-befe70aa5847_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_725180e1-b555-4125-8617-a0f9429b5d83_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_1ed99e74-511f-4910-b14a-9290c245d208_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_ec4dec5f-4347-4384-b774-cb32e94ae6e4_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_bb53f52a-9e34-474b-99fd-7ca620921878_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_74365ec2-3eb8-4f9c-9931-a0ef71cd10a2_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_6113f106-1834-4c19-bc9e-05a5d0db3778_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_a21cfa2b-75dd-423e-9f7c-c7f13dbd6a91_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_11757287-b8b2-425b-8cc0-2fec491f3a65_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_23c5a7c5-a826-4ff0-bf0f-ae4f246749cd_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_2c4cc151-a129-4ad0-b545-96f20b97b6d7_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_c8808e4e-ac50-44b8-8906-bafc80a0195f_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_7bd39a73-0493-4735-9aa3-b8b1fad0b6a3_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_b78d8527-187c-4ae7-869a-a46a0093b60b_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_b4f3da06-a3b9-442e-945a-e759f8469d4e_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_87e0536b-0c45-448e-8ebd-a62cf83bbb8b_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_b82836b9-24b2-41c1-b7ca-db02289923f1_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_6d5c56ea-941c-40b1-a86e-6d33caea6cd7_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_9a28e6a8-7249-47e8-97aa-e6d9e8147421_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_bfa9edd2-e7d0-43f9-b5ab-460e70a20b29_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_084f8e1e-95de-41b1-b2ae-825e31def2bd_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_d1b21a80-a7a1-419e-b298-d6aeadafa89a_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_88b3c4e2-1997-4ff2-beea-38c8879212ec_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_c9cdf605-8420-4529-93a8-ebec0edbff83_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_eeef5dfe-8336-47cb-b75f-7d4e49a623b3_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_f9f43977-640f-456c-a01b-8253251ed69c_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_5cb978df-07e5-4547-be38-f4cc7f5c1481_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_f4a7cd70-35bc-4a89-98ff-b6b6f217411d_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_32cb6204-9631-4c0d-9597-f404ae2e68cf_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_86cb719e-e844-4a75-b682-331abaa6b252_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_d2acc30a-4903-4db9-b4c5-f09854c97a70_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_d5098de2-976a-49a2-bae8-9df4815d8387_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_abaae07c-bbb1-4872-b556-2e06311a4a89_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_958ed03b-5f31-4953-91d7-6deb4cd0c501_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_5ce31595-0248-4247-a87f-3491f61d51a6_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_cc83aba5-a235-4eb3-a7d3-842797290ac7_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_3dbdebae-fcba-4e2b-b4fc-74f81fc2b10e_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_16bcf045-314e-4ed5-b94b-efd7faad780c_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_35749e83-1d83-4b05-a8fa-d440689798fc_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_3d712632-4097-47c1-ab61-a5d40d372e97_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_b7f96a0b-63f4-4654-b574-26c1d97291ed_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_3013f24d-3c9b-488d-b054-45c456921334_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_5ec42c9c-bfee-459c-ad03-025d7bbb542c_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_f6fab39d-0fdc-473b-9501-a3dc2cfdd1b2_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_7c5ac6ac-547f-418e-84eb-183c7f492f5f_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_0193946a-7c82-493e-b9bd-0cb7206580ba_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_5f840533-612a-428d-8fd9-77ba9f0d889c_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_8aafb525-8925-4464-8e94-eadebfb04a19_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_0ca2df6d-65f6-4f59-9fd4-2ba8a11419a6_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_1ae7afb5-76ba-4473-a237-d3efd970c92c_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_5776a27f-2982-4903-94ee-68baeab94ce0_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_6b2379eb-8d99-4bc4-9fe7-0505de5db6a6_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_c366bccc-950f-44a1-aeee-7a81003b0169_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_63d419ad-c9f6-4466-acd4-4adce37247fb_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_24a41f5b-ba47-4691-b27f-bb28f50d09a7_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_fafdbc31-03b8-43c0-9a6d-02ca1317e0d1_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_1d5b6cca-9fbe-43a1-83e9-68739e6b40ce_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_3bfd45c2-f149-4c36-ad6d-0acc59b4a093_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_6b53c8b1-c2db-4cbb-8dc6-239ca9e6b265_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_8187fdab-a1fe-4ea9-b074-ade2c1c03051_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_5f9f2c63-8392-455b-af77-a597a7441444_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_0694e22f-13af-4092-833b-da4bc9103ab5_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_e585766a-72e5-4da6-b1f9-d779c2bf0461_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_0233270d-83ca-4bfb-b420-550b647791f1_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_4808344d-3229-4683-893d-3d93786a79f4_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_878d638f-1c4f-44c4-9905-adee483b6d28_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_133707c7-0d76-4603-9492-5316692722e1_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_672be91e-d1d7-4c51-a70d-689e274dc4f5_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_a401b647-f1eb-42ce-9cec-464120fa69b0_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_fc64aaa2-3677-47f0-9838-0de9f638dd53_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_c91cafb9-deea-42b8-9b97-a00234bba3cb_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_d7338baf-eadc-4447-925f-05c2e07dda50_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_512d21f8-1b69-4040-8dd5-692702007d49_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_06f077d8-ebf9-4fb8-ba32-04713d8a2dda_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_7e8870d6-6642-4b04-aebf-074d3c34a47b_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_aa219a50-e16b-4210-b620-91af9a13b521_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_e12fbb57-d37f-4ef7-b77e-2c2f2c71fd42_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001URh_42c61cfb-294a-4fcf-be55-73b781948277_2_poly"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_5c0353dc-7f36-44f4-bad2-d75e016461b5_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>15.294</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>46.948</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.4</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>272.417</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>5c0353dc-7f36-44f4-bad2-d75e016461b5</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855689_19227">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_5c0353dc-7f36-44f4-bad2-d75e016461b5_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32694">
                      <gml:posList srsDimension="3">456429.8532 5428763.5493 132.4 456426.6182999999 5428767.117 129.24 456426.33959999995 5428760.5149 129.24 456429.8532 5428763.5493 132.4</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_88ecf406-b39b-498a-9d86-5c73d972c90c_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>35.523</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>45.308</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.4</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>182.661</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>88ecf406-b39b-498a-9d86-5c73d972c90c</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855689_19228">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_88ecf406-b39b-498a-9d86-5c73d972c90c_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32695">
                      <gml:posList srsDimension="3">456435.9361 5428760.069 129.24 456436.0516 5428763.2612 132.4 456433.0057 5428763.4028 132.4 456429.8532 5428763.5493 132.4 456426.33959999995 5428760.5149 129.24 456435.9361 5428760.069 129.24</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_bf4bf9ca-c376-45c0-9d70-1abecea6f749_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>22.501</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>47.212</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.4</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>2.66</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>bf4bf9ca-c376-45c0-9d70-1abecea6f749</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855689_19229">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_bf4bf9ca-c376-45c0-9d70-1abecea6f749_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32696">
                      <gml:posList srsDimension="3">456429.8532 5428763.5493 132.4 456433.0057 5428763.4028 132.4 456433.0661 5428765.0725 130.8535 456433.0672 5428765.1009 130.8272 456433.1292 5428766.8147 129.24 456426.6182999999 5428767.117 129.24 456429.8532 5428763.5493 132.4</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_59b553b5-9e9f-4603-b6db-613a0c10c849_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>48.028</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>39.87</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.23</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.12</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>273.815</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>59b553b5-9e9f-4603-b6db-613a0c10c849</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855689_19230">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_59b553b5-9e9f-4603-b6db-613a0c10c849_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32697">
                      <gml:posList srsDimension="3">456431.6025 5428793.7341 132.23 456429.2407 5428797.3566 129.12 456427.6634 5428773.7042 129.12 456431.6025 5428793.7341 132.23</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_307b1104-9f0a-4d8a-9057-8cdb8404086c_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>37.453</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>42.569</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.23</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.12</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>273.081</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>307b1104-9f0a-4d8a-9057-8cdb8404086c</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855689_19231">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_307b1104-9f0a-4d8a-9057-8cdb8404086c_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32698">
                      <gml:posList srsDimension="3">456430.649 5428776.0217 132.23 456431.6025 5428793.7341 132.23 456427.6634 5428773.7042 129.12 456430.649 5428776.0217 132.23</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_25c82333-8a84-4056-93ee-eee4207a9464_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>12.653</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>39.132</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.23</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.12</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>184.208</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>25c82333-8a84-4056-93ee-eee4207a9464</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855689_19232">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_25c82333-8a84-4056-93ee-eee4207a9464_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32699">
                      <gml:posList srsDimension="3">456433.9583 5428773.241 129.12 456430.649 5428776.0217 132.23 456427.6634 5428773.7042 129.12 456433.9583 5428773.241 129.12</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_7ecebf9e-53ed-41ae-914c-498a917ab8b3_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>14.925</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>47.879</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.23</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.12</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>4.208</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>7ecebf9e-53ed-41ae-914c-498a917ab8b3</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855689_19233">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_7ecebf9e-53ed-41ae-914c-498a917ab8b3_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32700">
                      <gml:posList srsDimension="3">456431.6025 5428793.7341 132.23 456435.6607 5428796.8842 129.12 456429.2407 5428797.3566 129.12 456431.6025 5428793.7341 132.23</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_fbb21abb-0315-4bf1-9b42-b180298abfd4_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>38.685</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>41.647</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.51</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.51</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>270.041</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>fbb21abb-0315-4bf1-9b42-b180298abfd4</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855689_19234">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_fbb21abb-0315-4bf1-9b42-b180298abfd4_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32701">
                      <gml:posList srsDimension="3">456434.7056 5428807.7654 133.51 456430.2634 5428813.9085 128.51 456430.2552 5428802.3456 128.51 456434.7056 5428807.7654 133.51</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_b7558aa5-286d-413f-8dad-854be50658dc_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>64.841</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>47.4</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.51</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.51</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>180.228</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>b7558aa5-286d-413f-8dad-854be50658dc</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855689_19235">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_b7558aa5-286d-413f-8dad-854be50658dc_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32702">
                      <gml:posList srsDimension="3">456440.4732 5428802.3049 128.51 456442.0433 5428807.7361 133.51 456434.7056 5428807.7654 133.51 456430.2552 5428802.3456 128.51 456440.4732 5428802.3049 128.51</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_63d43bfe-3f89-4f99-bab9-06af850458a0_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>76.017</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>50.776</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.51</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.51</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>0.229</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>63d43bfe-3f89-4f99-bab9-06af850458a0</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855689_19236">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_63d43bfe-3f89-4f99-bab9-06af850458a0_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32703">
                      <gml:posList srsDimension="3">456434.7056 5428807.7654 133.51 456442.0433 5428807.7361 133.51 456442.1535 5428813.8611 128.51 456430.2634 5428813.9085 128.51 456434.7056 5428807.7654 133.51</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_606d4172-8520-420a-8944-9d154b60a11d_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>41.222</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>48.0</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.23</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.12</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>93.081</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>606d4172-8520-420a-8944-9d154b60a11d</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855689_19237">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_606d4172-8520-420a-8944-9d154b60a11d_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32704">
                      <gml:posList srsDimension="3">456433.9583 5428773.241 129.12 456431.6025 5428793.7341 132.23 456430.649 5428776.0217 132.23 456433.9583 5428773.241 129.12</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_ab55747c-b8a8-4a39-9f04-9e441707f61e_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>58.396</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>50.861</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.23</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.12</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>94.118</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>ab55747c-b8a8-4a39-9f04-9e441707f61e</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855689_19238">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_ab55747c-b8a8-4a39-9f04-9e441707f61e_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32705">
                      <gml:posList srsDimension="3">456433.9583 5428773.241 129.12 456435.6607 5428796.8842 129.12 456431.6025 5428793.7341 132.23 456433.9583 5428773.241 129.12</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_dc5b226b-643e-473e-999f-8e9e596d1892_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>5.917</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>46.781</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.4</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.83</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>2.661</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>dc5b226b-643e-473e-999f-8e9e596d1892</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855689_19239">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_dc5b226b-643e-473e-999f-8e9e596d1892_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32706">
                      <gml:posList srsDimension="3">456436.0516 5428763.2612 132.4 456435.1760999999 5428764.9745 130.82999999999998 456433.0661 5428765.0725 130.82999999999998 456433.0057 5428763.4028 132.4 456436.0516 5428763.2612 132.4</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_e6b00c8e-6a42-4d6c-9220-cecc2f3394ba_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.843</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>84.43</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>130.83</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.66</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>2.655</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e6b00c8e-6a42-4d6c-9220-cecc2f3394ba</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855689_19240">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_e6b00c8e-6a42-4d6c-9220-cecc2f3394ba_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32707">
                      <gml:posList srsDimension="3">456435.1760999999 5428764.9745 130.82999999999998 456434.2627 5428766.762 130.66 456433.1292 5428766.8147 130.66 456433.0672 5428765.1009 130.8272 456433.0661 5428765.0725 130.82999999999998 456435.1760999999 5428764.9745 130.82999999999998</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_c117e987-1281-4f93-8088-34ece3efa9c6_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.043</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>90.0</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>112.38</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>112.38</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>-1.0</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>c117e987-1281-4f93-8088-34ece3efa9c6</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855689_19241">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_c117e987-1281-4f93-8088-34ece3efa9c6_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32708">
                      <gml:posList srsDimension="3">456433.7344 5428771.1955 112.38 456433.79 5428772.53 112.38 456433.7255 5428772.5317 112.38 456433.7344 5428771.1955 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_5763da67-3093-44a8-89f7-19619bb4839f_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>68.948</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>45.296</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.4</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>180.263</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>5763da67-3093-44a8-89f7-19619bb4839f</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855689_19242">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_5763da67-3093-44a8-89f7-19619bb4839f_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32709">
                      <gml:posList srsDimension="3">456451.2284 5428759.9988 129.24 456451.456 5428763.1906 132.4 456443.1575 5428763.2287 132.4 456436.0516 5428763.2612 132.4 456435.9361 5428760.069 129.24 456451.2284 5428759.9988 129.24</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_37e379f1-c7e4-468b-9e31-8b1496f9c24f_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.011</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>90.0</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>112.38</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>112.38</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>-1.0</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>37e379f1-c7e4-468b-9e31-8b1496f9c24f</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855689_19243">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_37e379f1-c7e4-468b-9e31-8b1496f9c24f_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32710">
                      <gml:posList srsDimension="3">456439.5994 5428767.2884 112.38 456439.9425 5428767.2935 112.38 456438.5959 5428767.3351 112.38 456439.5994 5428767.2884 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_bd7ea529-5749-4df8-abbc-a3721661ae0d_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>47.91</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>47.321</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.51</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.51</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>179.683</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>bd7ea529-5749-4df8-abbc-a3721661ae0d</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855689_19244">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_bd7ea529-5749-4df8-abbc-a3721661ae0d_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32711">
                      <gml:posList srsDimension="3">456453.4641 5428802.3767 128.51 456442.0433 5428807.7361 133.51 456440.4732 5428802.3049 128.51 456453.4641 5428802.3767 128.51</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_d4050bd2-df98-49e1-aef3-8f74c46f36f9_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>120.665</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>50.751</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.51</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.51</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>358.602</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>d4050bd2-df98-49e1-aef3-8f74c46f36f9</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855689_19245">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_d4050bd2-df98-49e1-aef3-8f74c46f36f9_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32712">
                      <gml:posList srsDimension="3">456457.3675 5428808.1114 133.5095 456457.3608 5428814.2311 128.5105 456442.1535 5428813.8611 128.51 456442.0433 5428807.7361 133.51 456457.3675 5428808.1114 133.5095</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_d6cf16f5-e153-4115-8c1e-6ddde9537444_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>57.751</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>48.431</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.51</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.51</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>178.595</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>d6cf16f5-e153-4115-8c1e-6ddde9537444</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855690_19246">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_d6cf16f5-e153-4115-8c1e-6ddde9537444_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32713">
                      <gml:posList srsDimension="3">456453.4641 5428802.3767 128.51 456457.3675 5428808.1114 133.5095 456442.0433 5428807.7361 133.51 456453.4641 5428802.3767 128.51</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_82e340b9-8077-46d2-be62-408f899c90a1_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>5.468</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>84.593</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>130.83</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.66</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>359.185</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>82e340b9-8077-46d2-be62-408f899c90a1</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855690_19247">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_82e340b9-8077-46d2-be62-408f899c90a1_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32714">
                      <gml:posList srsDimension="3">456450.7722 5428764.9029 130.82999999999998 456453.4456 5428764.9414 130.82999999999998 456453.4456 5428764.9701 130.8273 456453.4467 5428766.7371 130.66 456450.0586 5428766.6896 130.66 456450.7722 5428764.9029 130.82999999999998</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_673f8dfe-22ed-4900-b6b4-ed481b17c435_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>5.432</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>47.643</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.4</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.83</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>359.175</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>673f8dfe-22ed-4900-b6b4-ed481b17c435</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855690_19248">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_673f8dfe-22ed-4900-b6b4-ed481b17c435_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32715">
                      <gml:posList srsDimension="3">456451.456 5428763.1906 132.4 456453.4446 5428763.2192 132.4 456453.4456 5428764.9414 130.82999999999998 456450.7722 5428764.9029 130.82999999999998 456451.456 5428763.1906 132.4</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_bc69458e-607a-4ac9-9fde-8963db6c197f_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>59.421</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>45.236</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.402</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.239</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>179.142</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>bc69458e-607a-4ac9-9fde-8963db6c197f</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855690_19249">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_bc69458e-607a-4ac9-9fde-8963db6c197f_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32716">
                      <gml:posList srsDimension="3">456464.9326 5428760.2048 129.2394 456464.2375 5428763.3827 132.40189999999998 456459.6498 5428763.3086 132.3968 456453.4446 5428763.2192 132.4 456451.456 5428763.1906 132.4 456451.2284 5428759.9988 129.24 456464.9326 5428760.2048 129.2394</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_4142cec3-d821-4126-9208-52999a4c2180_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>21.563</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>48.069</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.4</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>359.201</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>4142cec3-d821-4126-9208-52999a4c2180</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855690_19250">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_4142cec3-d821-4126-9208-52999a4c2180_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32717">
                      <gml:posList srsDimension="3">456459.6498 5428763.3086 132.3968 456456.3638 5428766.778 129.2403 456453.4467 5428766.7371 129.2397 456453.4456 5428764.9701 130.8273 456453.4456 5428764.9414 130.853 456453.4446 5428763.2192 132.4 456459.6498 5428763.3086 132.3968</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_54cd728a-99f2-4231-884e-db1806669413_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>28.865</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>48.539</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.51</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.51</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>178.9</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>54cd728a-99f2-4231-884e-db1806669413</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855690_19251">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_54cd728a-99f2-4231-884e-db1806669413_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32718">
                      <gml:posList srsDimension="3">456461.1082 5428802.523499999 128.51 456457.3675 5428808.1114 133.5095 456453.4641 5428802.3767 128.51 456461.1082 5428802.523499999 128.51</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_c2857e26-1312-4f7a-813a-53b1ee174a02_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>14.575</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>44.308</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.4</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>358.325</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>c2857e26-1312-4f7a-813a-53b1ee174a02</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855690_19252">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_c2857e26-1312-4f7a-813a-53b1ee174a02_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32719">
                      <gml:posList srsDimension="3">456458.5246 5428792.8121 132.4 456461.6143 5428795.9883 129.24 456455.0161 5428795.7954 129.24 456458.5246 5428792.8121 132.4</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_641a6add-62f3-4013-aea5-7225e979ea82_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>67.06</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>46.811</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.4</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>267.341</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>641a6add-62f3-4013-aea5-7225e979ea82</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855690_19253">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_641a6add-62f3-4013-aea5-7225e979ea82_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32720">
                      <gml:posList srsDimension="3">456456.3638 5428766.778 129.2403 456458.5246 5428792.8121 132.4 456455.0161 5428795.7954 129.24 456456.3638 5428766.778 129.2403</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_498e8eb4-ec60-4a07-9f00-1e89e5784ac2_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>65.85</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>44.95</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.4</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>267.81</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>498e8eb4-ec60-4a07-9f00-1e89e5784ac2</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855690_19254">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_498e8eb4-ec60-4a07-9f00-1e89e5784ac2_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32721">
                      <gml:posList srsDimension="3">456459.6498 5428763.3086 132.3968 456458.5246 5428792.8121 132.4 456456.3638 5428766.778 129.2403 456459.6498 5428763.3086 132.3968</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_f9a7d976-afcf-488b-89c4-667360912764_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>74.341</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>50.741</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.51</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.51</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>358.264</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>f9a7d976-afcf-488b-89c4-667360912764</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855690_19255">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_f9a7d976-afcf-488b-89c4-667360912764_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32722">
                      <gml:posList srsDimension="3">456457.3675 5428808.1114 133.5095 456466.9061 5428808.4004 133.51 456466.6334 5428814.5121 128.51 456457.3608 5428814.2311 128.5105 456457.3675 5428808.1114 133.5095</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_25152dad-084a-4f1c-ab6c-b785e1964ce2_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>36.173</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>48.738</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.51</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.51</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>178.268</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>25152dad-084a-4f1c-ab6c-b785e1964ce2</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855690_19256">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_25152dad-084a-4f1c-ab6c-b785e1964ce2_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32723">
                      <gml:posList srsDimension="3">456461.1082 5428802.523499999 128.51 456466.9061 5428808.4004 133.51 456457.3675 5428808.1114 133.5095 456461.1082 5428802.523499999 128.51</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_38598e29-faa4-43d1-a55e-3ae8639858be_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>65.898</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>45.723</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.4</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>87.208</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>38598e29-faa4-43d1-a55e-3ae8639858be</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855690_19257">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_38598e29-faa4-43d1-a55e-3ae8639858be_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32724">
                      <gml:posList srsDimension="3">456463.0326 5428766.9055 129.24 456461.6143 5428795.9883 129.24 456458.5246 5428792.8121 132.4 456463.0326 5428766.9055 129.24</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_31977c4b-04fc-4c0e-8b03-715067dbd717_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>69.775</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>48.089</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.4</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>87.823</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>31977c4b-04fc-4c0e-8b03-715067dbd717</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855690_19258">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_31977c4b-04fc-4c0e-8b03-715067dbd717_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32725">
                      <gml:posList srsDimension="3">456459.6498 5428763.3086 132.3968 456463.0326 5428766.9055 129.24 456458.5246 5428792.8121 132.4 456459.6498 5428763.3086 132.3968</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_989a5336-bb1b-4126-890c-8ca33ac62842_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>10.89</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>48.255</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.402</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>359.003</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>989a5336-bb1b-4126-890c-8ca33ac62842</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855690_19259">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_989a5336-bb1b-4126-890c-8ca33ac62842_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32726">
                      <gml:posList srsDimension="3">456464.2375 5428763.3827 132.40189999999998 456463.0326 5428766.9055 129.24 456459.6498 5428763.3086 132.3968 456464.2375 5428763.3827 132.40189999999998</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_606a639c-37b7-4233-823b-c7c2add5fd4f_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>44.064</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>49.062</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.51</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.51</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>178.899</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>606a639c-37b7-4233-823b-c7c2add5fd4f</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855690_19260">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_606a639c-37b7-4233-823b-c7c2add5fd4f_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32727">
                      <gml:posList srsDimension="3">456472.65499999997 5428802.7454 128.51 456466.9061 5428808.4004 133.51 456461.1082 5428802.523499999 128.51 456472.65499999997 5428802.7454 128.51</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_f8ccb4ee-6d24-4fce-b31f-67840f796449_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>5.165</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>48.954</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.402</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>353.865</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>f8ccb4ee-6d24-4fce-b31f-67840f796449</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855690_19261">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_f8ccb4ee-6d24-4fce-b31f-67840f796449_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32728">
                      <gml:posList srsDimension="3">456464.2375 5428763.3827 132.40189999999998 456465.1658 5428767.1348 129.24 456463.0326 5428766.9055 129.24 456464.2375 5428763.3827 132.40189999999998</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_14cbc07b-6c05-4c84-88ca-69147bb2d68a_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>7.029</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>49.036</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.402</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>354.296</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>14cbc07b-6c05-4c84-88ca-69147bb2d68a</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855690_19262">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_14cbc07b-6c05-4c84-88ca-69147bb2d68a_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32729">
                      <gml:posList srsDimension="3">456466.1852 5428763.5787 132.4 456466.1535 5428765.3408 130.8754 456466.1525 5428765.3993 130.8247 456466.1195 5428767.2308 129.24 456465.1658 5428767.1348 129.24 456464.2375 5428763.3827 132.40189999999998 456466.1852 5428763.5787 132.4</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_608c60d8-d8d9-4705-8d17-5e48f327e8aa_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>15.729</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>48.721</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.23</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.12</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>352.32</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>608c60d8-d8d9-4705-8d17-5e48f327e8aa</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855690_19263">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_608c60d8-d8d9-4705-8d17-5e48f327e8aa_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32730">
                      <gml:posList srsDimension="3">456486.0258 5428795.4922 132.23 456489.0729 5428799.4778 129.12 456482.4596 5428798.586 129.12 456486.0258 5428795.4922 132.23</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_88e67852-a42e-420e-a7c7-01574d8e3872_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>86.727</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>45.549</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.231</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.119</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>263.112</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>88e67852-a42e-420e-a7c7-01574d8e3872</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855690_19264">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_88e67852-a42e-420e-a7c7-01574d8e3872_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32731">
                      <gml:posList srsDimension="3">456485.2316 5428775.624999999 129.1192 456487.9365 5428779.6869 132.2308 456486.0258 5428795.4922 132.23 456482.4596 5428798.586 129.12 456485.2316 5428775.624999999 129.1192</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_5b1ac83c-d9f7-4930-9390-94a6a867200b_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>40.127</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>41.394</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.51</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.51</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>86.95</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>5b1ac83c-d9f7-4930-9390-94a6a867200b</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855690_19265">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_5b1ac83c-d9f7-4930-9390-94a6a867200b_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32732">
                      <gml:posList srsDimension="3">456488.0818 5428803.5407 128.51 456487.4412 5428815.5645 128.51 456483.3654 5428809.2277 133.51 456488.0818 5428803.5407 128.51</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_6b9878e8-ed3c-46ca-8743-21738dc3fb39_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>16.255</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>50.319</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.231</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.119</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>173.881</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>6b9878e8-ed3c-46ca-8743-21738dc3fb39</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855690_19266">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_6b9878e8-ed3c-46ca-8743-21738dc3fb39_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32733">
                      <gml:posList srsDimension="3">456491.8657 5428776.3372 129.12 456487.9365 5428779.6869 132.2308 456485.2316 5428775.624999999 129.1192 456491.8657 5428776.3372 129.12</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_6325c600-d19b-402f-8bc0-29373a15f310_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>91.858</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>48.382</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.231</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.12</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>83.112</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>6325c600-d19b-402f-8bc0-29373a15f310</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855690_19267">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_6325c600-d19b-402f-8bc0-29373a15f310_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32734">
                      <gml:posList srsDimension="3">456487.9365 5428779.6869 132.2308 456491.8657 5428776.3372 129.12 456489.0729 5428799.4778 129.12 456486.0258 5428795.4922 132.23 456487.9365 5428779.6869 132.2308</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_b2d510de-9659-4521-9801-29ae6df229ba_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>16.871</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>49.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.4</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>354.253</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>b2d510de-9659-4521-9801-29ae6df229ba</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855690_19268">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_b2d510de-9659-4521-9801-29ae6df229ba_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32735">
                      <gml:posList srsDimension="3">456487.5116 5428767.5491 130.8247 456487.5179999999 5428767.4911 130.8754 456487.7108 5428765.7453 132.4 456489.46699999995 5428765.922 132.4 456492.5197 5428769.8879 129.24 456487.3112 5428769.363699999 129.24 456487.5116 5428767.5491 130.8247</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_288d8e17-e6c9-470a-8840-1e3fdc126569_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>16.202</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>47.914</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.4</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>83.23</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>288d8e17-e6c9-470a-8840-1e3fdc126569</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855690_19269">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_288d8e17-e6c9-470a-8840-1e3fdc126569_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32736">
                      <gml:posList srsDimension="3">456493.32989999995 5428763.063 129.24 456492.5197 5428769.8879 129.24 456489.46699999995 5428765.922 132.4 456493.32989999995 5428763.063 129.24</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_5e546bdf-955a-48dd-91a0-c6becb4dceb9_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>130.032</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>5e546bdf-955a-48dd-91a0-c6becb4dceb9</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855690_19270">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_5e546bdf-955a-48dd-91a0-c6becb4dceb9_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32737">
                      <gml:posList srsDimension="3">456425.78 5428760.05 112.38 456425.78 5428760.05 128.8 456426.16 5428767.96 128.8 456426.16 5428767.96 112.38 456425.78 5428760.05 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_474dc211-6edd-453e-81b4-f78392ce47a2_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>165.857</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>474dc211-6edd-453e-81b4-f78392ce47a2</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855691_19271">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_474dc211-6edd-453e-81b4-f78392ce47a2_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32738">
                      <gml:posList srsDimension="3">456435.87 5428759.58 112.38 456435.87 5428759.58 128.8 456425.78 5428760.05 128.8 456425.78 5428760.05 112.38 456435.87 5428759.58 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_e64bb4fb-28de-431e-a14d-f9dea0b4c3b2_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>43.93</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e64bb4fb-28de-431e-a14d-f9dea0b4c3b2</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855691_19272">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_e64bb4fb-28de-431e-a14d-f9dea0b4c3b2_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32739">
                      <gml:posList srsDimension="3">456426.16 5428767.96 112.38 456426.16 5428767.96 128.8 456428.83 5428767.79 128.8 456428.83 5428767.79 112.38 456426.16 5428767.96 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_84123b74-0850-41d8-b0db-a4f791ad2540_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.908</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>84123b74-0850-41d8-b0db-a4f791ad2540</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855691_19273">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_84123b74-0850-41d8-b0db-a4f791ad2540_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32740">
                      <gml:posList srsDimension="3">456426.33959999995 5428760.5149 128.8 456426.33959999995 5428760.5149 129.24 456426.6182999999 5428767.117 129.24 456426.6182999999 5428767.117 128.8 456426.33959999995 5428760.5149 128.8</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_9fd80578-7e90-4941-b80d-abddd691ed38_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.227</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>9fd80578-7e90-4941-b80d-abddd691ed38</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855691_19274">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_9fd80578-7e90-4941-b80d-abddd691ed38_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32741">
                      <gml:posList srsDimension="3">456435.9361 5428760.069 128.8 456435.9361 5428760.069 129.24 456426.33959999995 5428760.5149 129.24 456426.33959999995 5428760.5149 128.8 456435.9361 5428760.069 128.8</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_6e1ffb3a-fcdb-4a7a-ba28-7e66e5cfff38_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>421.738</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>6e1ffb3a-fcdb-4a7a-ba28-7e66e5cfff38</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855691_19275">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_6e1ffb3a-fcdb-4a7a-ba28-7e66e5cfff38_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32742">
                      <gml:posList srsDimension="3">456426.7 5428773.05 112.38 456426.7 5428773.05 129.0 456428.51999999996 5428798.36 129.0 456428.51999999996 5428798.36 112.38 456426.7 5428773.05 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_55feb7e5-ad51-4390-917d-88591a78d35b_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>1.072</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>55feb7e5-ad51-4390-917d-88591a78d35b</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855691_19276">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_55feb7e5-ad51-4390-917d-88591a78d35b_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32743">
                      <gml:posList srsDimension="3">456433.79 5428772.53 112.38 456433.79 5428772.53 129.0 456433.7255 5428772.5317 129.0 456433.7255 5428772.5317 128.8 456433.7255 5428772.5317 112.38 456433.79 5428772.53 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_406d61ed-9c5c-4041-a82c-5dccb3553243_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.946</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>406d61ed-9c5c-4041-a82c-5dccb3553243</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855691_19277">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_406d61ed-9c5c-4041-a82c-5dccb3553243_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32744">
                      <gml:posList srsDimension="3">456433.7255 5428772.5317 128.8 456433.7255 5428772.5317 129.0 456429.01 5428772.88 129.0 456429.01 5428772.88 128.8 456433.7255 5428772.5317 128.8</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_3cc06106-a1a9-46e2-99a2-f746cd6b1e97_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>38.496</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>3cc06106-a1a9-46e2-99a2-f746cd6b1e97</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855691_19278">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_3cc06106-a1a9-46e2-99a2-f746cd6b1e97_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32745">
                      <gml:posList srsDimension="3">456429.01 5428772.88 112.38 456429.01 5428772.88 128.8 456429.01 5428772.88 129.0 456426.7 5428773.05 129.0 456426.7 5428773.05 112.38 456429.01 5428772.88 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_9454a0d0-6d7b-44ea-a232-0ba16b213a1f_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.845</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>9454a0d0-6d7b-44ea-a232-0ba16b213a1f</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855691_19279">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_9454a0d0-6d7b-44ea-a232-0ba16b213a1f_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32746">
                      <gml:posList srsDimension="3">456427.6634 5428773.7042 129.0 456427.6634 5428773.7042 129.12 456429.2407 5428797.3566 129.12 456429.2407 5428797.3566 129.0 456427.6634 5428773.7042 129.0</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_6d4f0758-19f1-40e1-a4cc-18953e754486_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.757</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>6d4f0758-19f1-40e1-a4cc-18953e754486</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855691_19280">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_6d4f0758-19f1-40e1-a4cc-18953e754486_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32747">
                      <gml:posList srsDimension="3">456433.9583 5428773.241 129.0 456433.9583 5428773.241 129.12 456427.6634 5428773.7042 129.12 456427.6634 5428773.7042 129.0 456433.9583 5428773.241 129.0</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_2cfa78dc-08ba-4399-bd00-02cd14ce6fe2_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>5.001</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>2cfa78dc-08ba-4399-bd00-02cd14ce6fe2</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855691_19281">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_2cfa78dc-08ba-4399-bd00-02cd14ce6fe2_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32748">
                      <gml:posList srsDimension="3">456431.03 5428798.17 128.1 456431.03 5428798.17 129.0 456436.566 5428797.695699999 129.0 456436.566 5428797.695699999 128.1 456431.03 5428798.17 128.1</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_162a587a-2b53-4893-8d1e-aa9422735a18_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>21.941</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>162a587a-2b53-4893-8d1e-aa9422735a18</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855691_19282">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_162a587a-2b53-4893-8d1e-aa9422735a18_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32749">
                      <gml:posList srsDimension="3">456433.7255 5428772.5317 112.38 456433.7255 5428772.5317 128.8 456433.7344 5428771.1955 128.8 456433.7344 5428771.1955 112.38 456433.7255 5428772.5317 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_f4aec7c1-77b4-498a-a3bb-9a44a617dea6_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.772</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>f4aec7c1-77b4-498a-a3bb-9a44a617dea6</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855691_19283">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_f4aec7c1-77b4-498a-a3bb-9a44a617dea6_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32750">
                      <gml:posList srsDimension="3">456429.2407 5428797.3566 129.0 456429.2407 5428797.3566 129.12 456435.6607 5428796.8842 129.12 456435.6607 5428796.8842 129.0 456429.2407 5428797.3566 129.0</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_efdea9ea-a13a-4f65-a24c-295c9355a26f_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>164.461</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>efdea9ea-a13a-4f65-a24c-295c9355a26f</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855691_19284">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_efdea9ea-a13a-4f65-a24c-295c9355a26f_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32751">
                      <gml:posList srsDimension="3">456429.48 5428801.81 112.38 456429.48 5428801.81 128.1 456429.68 5428812.27 128.1 456429.68 5428812.27 112.38 456429.48 5428801.81 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_03115c15-a2d8-427d-b38c-ba8170b68db5_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>37.576</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>03115c15-a2d8-427d-b38c-ba8170b68db5</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855691_19285">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_03115c15-a2d8-427d-b38c-ba8170b68db5_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32752">
                      <gml:posList srsDimension="3">456429.68 5428812.27 112.38 456429.68 5428812.27 128.1 456429.72 5428814.66 128.1 456429.72 5428814.66 112.38 456429.68 5428812.27 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_ac3a7668-58cf-421c-a99d-580ab777e5f3_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.741</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>ac3a7668-58cf-421c-a99d-580ab777e5f3</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855691_19286">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_ac3a7668-58cf-421c-a99d-580ab777e5f3_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32753">
                      <gml:posList srsDimension="3">456430.2552 5428802.3456 128.1 456430.2552 5428802.3456 128.51 456430.2634 5428813.9085 128.51 456430.2634 5428813.9085 128.1 456430.2552 5428802.3456 128.1</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_9eaeee2e-8b0c-4727-9775-c14b0b6a0a86_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.189</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>9eaeee2e-8b0c-4727-9775-c14b0b6a0a86</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855691_19287">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_9eaeee2e-8b0c-4727-9775-c14b0b6a0a86_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32754">
                      <gml:posList srsDimension="3">456440.4732 5428802.3049 128.1 456440.4732 5428802.3049 128.51 456430.2552 5428802.3456 128.51 456430.2552 5428802.3456 128.1 456440.4732 5428802.3049 128.1</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_52d032dc-c603-4cb9-aec7-3de320cab1ee_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.875</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>52d032dc-c603-4cb9-aec7-3de320cab1ee</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855691_19288">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_52d032dc-c603-4cb9-aec7-3de320cab1ee_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32755">
                      <gml:posList srsDimension="3">456430.2634 5428813.9085 128.1 456430.2634 5428813.9085 128.51 456442.1535 5428813.8611 128.51 456442.1535 5428813.8611 128.1 456430.2634 5428813.9085 128.1</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_f5ccd817-73c3-4a0d-9205-9125b96ccf63_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>56.728</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>f5ccd817-73c3-4a0d-9205-9125b96ccf63</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855691_19289">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_f5ccd817-73c3-4a0d-9205-9125b96ccf63_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32756">
                      <gml:posList srsDimension="3">456431.03 5428798.17 112.38 456431.03 5428798.17 128.1 456431.28 5428801.77 128.1 456431.28 5428801.77 112.38 456431.03 5428798.17 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_223659c3-7966-40b3-902c-aaeca662558d_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>68.399</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>223659c3-7966-40b3-902c-aaeca662558d</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855691_19290">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_223659c3-7966-40b3-902c-aaeca662558d_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32757">
                      <gml:posList srsDimension="3">456440.23 5428801.83 112.38 456440.23 5428801.83 128.1 456439.9199999999 5428797.49 128.1 456439.9199999999 5428797.49 112.38 456440.23 5428801.83 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_aee2ed5a-902f-4214-86bb-3a962ec884dc_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>52.82</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>aee2ed5a-902f-4214-86bb-3a962ec884dc</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855691_19291">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_aee2ed5a-902f-4214-86bb-3a962ec884dc_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32758">
                      <gml:posList srsDimension="3">456439.9199999999 5428797.49 112.38 456439.9199999999 5428797.49 128.1 456436.57 5428797.75 128.1 456436.57 5428797.75 112.38 456439.9199999999 5428797.49 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_64b7c10f-319e-426b-bfd1-413da72c43e5_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.845</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>64b7c10f-319e-426b-bfd1-413da72c43e5</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855691_19292">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_64b7c10f-319e-426b-bfd1-413da72c43e5_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32759">
                      <gml:posList srsDimension="3">456435.6607 5428796.8842 129.0 456435.6607 5428796.8842 129.12 456433.9583 5428773.241 129.12 456433.9583 5428773.241 129.0 456435.6607 5428796.8842 129.0</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_4997958c-11d2-4faf-b8f7-70d964d54b6d_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>1.218</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>4997958c-11d2-4faf-b8f7-70d964d54b6d</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855691_19293">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_4997958c-11d2-4faf-b8f7-70d964d54b6d_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32760">
                      <gml:posList srsDimension="3">456433.0672 5428765.1009 130.8272 456433.1292 5428766.8147 130.66 456433.1292 5428766.8147 129.24 456433.0672 5428765.1009 130.8272</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_eedca241-0fc8-4b95-966d-6a7ef979f8a4_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>16.495</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>eedca241-0fc8-4b95-966d-6a7ef979f8a4</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855691_19294">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_eedca241-0fc8-4b95-966d-6a7ef979f8a4_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32761">
                      <gml:posList srsDimension="3">456438.5959 5428767.3351 112.38 456438.5959 5428767.3351 128.8 456439.5994 5428767.2884 128.8 456439.5994 5428767.2884 112.38 456438.5959 5428767.3351 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_f27db8fd-2aae-40c5-a4f1-291c31b878b0_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>250.572</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>f27db8fd-2aae-40c5-a4f1-291c31b878b0</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855691_19295">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_f27db8fd-2aae-40c5-a4f1-291c31b878b0_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32762">
                      <gml:posList srsDimension="3">456451.13 5428759.51 112.38 456451.13 5428759.51 128.8 456435.87 5428759.58 128.8 456435.87 5428759.58 112.38 456451.13 5428759.51 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_e33a0f39-ed12-4f5d-8c31-79ee5ccc81c6_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>6.729</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e33a0f39-ed12-4f5d-8c31-79ee5ccc81c6</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855691_19296">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_e33a0f39-ed12-4f5d-8c31-79ee5ccc81c6_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32763">
                      <gml:posList srsDimension="3">456451.2284 5428759.9988 128.8 456451.2284 5428759.9988 129.24 456435.9361 5428760.069 129.24 456435.9361 5428760.069 128.8 456451.2284 5428759.9988 128.8</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_c8ed6fd8-e9c7-4f1f-b078-9c0f64e58361_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>5.634</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>c8ed6fd8-e9c7-4f1f-b078-9c0f64e58361</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855692_19297">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_c8ed6fd8-e9c7-4f1f-b078-9c0f64e58361_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32764">
                      <gml:posList srsDimension="3">456439.5994 5428767.2884 112.38 456439.5994 5428767.2884 128.8 456439.9425 5428767.2935 128.8 456439.9425 5428767.2935 112.38 456439.5994 5428767.2884 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_8d2237a4-012b-407b-8a30-ec09ee38658a_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>7.187</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>8d2237a4-012b-407b-8a30-ec09ee38658a</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855692_19298">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_8d2237a4-012b-407b-8a30-ec09ee38658a_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32765">
                      <gml:posList srsDimension="3">456439.9425 5428767.2935 112.38 456439.9425 5428767.2935 128.8 456440.38 5428767.28 128.8 456440.38 5428767.28 112.38 456439.9425 5428767.2935 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_85998fd2-417b-4046-89e3-e26b11d61bf0_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>199.018</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>85998fd2-417b-4046-89e3-e26b11d61bf0</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855692_19299">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_85998fd2-417b-4046-89e3-e26b11d61bf0_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32766">
                      <gml:posList srsDimension="3">456452.89 5428801.9 112.38 456452.89 5428801.9 128.1 456440.23 5428801.83 128.1 456440.23 5428801.83 112.38 456452.89 5428801.9 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_a124322c-28c4-412b-8ef6-e805c43c5cf3_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>5.326</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>a124322c-28c4-412b-8ef6-e805c43c5cf3</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855692_19300">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_a124322c-28c4-412b-8ef6-e805c43c5cf3_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32767">
                      <gml:posList srsDimension="3">456453.4641 5428802.3767 128.1 456453.4641 5428802.3767 128.51 456440.4732 5428802.3049 128.51 456440.4732 5428802.3049 128.1 456453.4641 5428802.3767 128.1</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_143aafea-b4de-434b-9604-b3095e0d23c4_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>6.241</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>143aafea-b4de-434b-9604-b3095e0d23c4</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855692_19301">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_143aafea-b4de-434b-9604-b3095e0d23c4_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32768">
                      <gml:posList srsDimension="3">456442.1535 5428813.8611 128.1 456442.1535 5428813.8611 128.51 456457.3608 5428814.2311 128.5105 456457.3608 5428814.2311 128.1 456442.1535 5428813.8611 128.1</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_b87501be-92a7-4749-b0b0-357daded7a09_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>237.6</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>b87501be-92a7-4749-b0b0-357daded7a09</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855692_19302">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_b87501be-92a7-4749-b0b0-357daded7a09_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32769">
                      <gml:posList srsDimension="3">456442.25 5428814.61 112.38 456442.25 5428814.61 128.1 456457.36 5428814.98 128.1 456457.36 5428814.98 112.38 456442.25 5428814.61 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_538a7dcf-83b7-49e7-a38a-9bac04bef601_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>1.255</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>538a7dcf-83b7-49e7-a38a-9bac04bef601</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855692_19303">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_538a7dcf-83b7-49e7-a38a-9bac04bef601_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32770">
                      <gml:posList srsDimension="3">456453.4467 5428766.7371 129.2397 456453.4467 5428766.7371 130.66 456453.4456 5428764.9701 130.8273 456453.4467 5428766.7371 129.2397</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_d71fe779-4242-4e29-a030-41aa3c0f4a17_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>229.413</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>d71fe779-4242-4e29-a030-41aa3c0f4a17</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855692_19304">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_d71fe779-4242-4e29-a030-41aa3c0f4a17_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32771">
                      <gml:posList srsDimension="3">456465.0999999999 5428759.72 112.38 456465.0999999999 5428759.72 128.8 456451.13 5428759.51 128.8 456451.13 5428759.51 112.38 456465.0999999999 5428759.72 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_d80a2282-6de1-4ae5-9a2f-d62c9756c805_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>6.026</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>d80a2282-6de1-4ae5-9a2f-d62c9756c805</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855692_19305">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_d80a2282-6de1-4ae5-9a2f-d62c9756c805_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32772">
                      <gml:posList srsDimension="3">456464.9326 5428760.2048 128.8 456464.9326 5428760.2048 129.2394 456451.2284 5428759.9988 129.24 456451.2284 5428759.9988 128.8 456464.9326 5428760.2048 128.8</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_58741842-041c-41bb-89d0-8e937cbdab77_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>22.336</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>58741842-041c-41bb-89d0-8e937cbdab77</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855692_19306">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_58741842-041c-41bb-89d0-8e937cbdab77_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32773">
                      <gml:posList srsDimension="3">456454.52 5428796.6 112.38 456454.52 5428796.6 128.1 456453.1 5428796.55 128.1 456453.1 5428796.55 112.38 456454.52 5428796.6 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_20558b6c-c1be-468c-a81b-b79c8ecc34ce_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>84.167</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>20558b6c-c1be-468c-a81b-b79c8ecc34ce</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855692_19307">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_20558b6c-c1be-468c-a81b-b79c8ecc34ce_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32774">
                      <gml:posList srsDimension="3">456453.1 5428796.55 112.38 456453.1 5428796.55 128.1 456452.89 5428801.9 128.1 456452.89 5428801.9 112.38 456453.1 5428796.55 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_880e0dbf-c986-4b83-9a04-c7b86cef8b3b_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.103</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>880e0dbf-c986-4b83-9a04-c7b86cef8b3b</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855692_19308">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_880e0dbf-c986-4b83-9a04-c7b86cef8b3b_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32775">
                      <gml:posList srsDimension="3">456454.5214 5428796.5703 128.1 456454.5214 5428796.5703 128.8 456460.3516 5428797.177 128.8 456460.3516 5428797.177 128.1 456454.5214 5428796.5703 128.1</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_12c6738e-d227-4fa9-8f1f-befe70aa5847_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>32.21</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>12c6738e-d227-4fa9-8f1f-befe70aa5847</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855692_19309">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_12c6738e-d227-4fa9-8f1f-befe70aa5847_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32776">
                      <gml:posList srsDimension="3">456460.36 5428797.01 112.38 456460.36 5428797.01 128.8 456462.32 5428797.09 128.8 456462.32 5428797.09 112.38 456460.36 5428797.01 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_725180e1-b555-4125-8617-a0f9429b5d83_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>81.859</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>725180e1-b555-4125-8617-a0f9429b5d83</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855692_19310">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_725180e1-b555-4125-8617-a0f9429b5d83_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32777">
                      <gml:posList srsDimension="3">456455.87 5428767.48 112.38 456455.87 5428767.48 128.8 456455.64 5428772.46 128.8 456455.64 5428772.46 112.38 456455.87 5428767.48 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_1ed99e74-511f-4910-b14a-9290c245d208_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.904</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>1ed99e74-511f-4910-b14a-9290c245d208</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855692_19311">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_1ed99e74-511f-4910-b14a-9290c245d208_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32778">
                      <gml:posList srsDimension="3">456455.0161 5428795.7954 128.8 456455.0161 5428795.7954 129.24 456461.6143 5428795.9883 129.24 456461.6143 5428795.9883 128.8 456455.0161 5428795.7954 128.8</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_ec4dec5f-4347-4384-b774-cb32e94ae6e4_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>12.786</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>ec4dec5f-4347-4384-b774-cb32e94ae6e4</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855692_19312">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_ec4dec5f-4347-4384-b774-cb32e94ae6e4_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32779">
                      <gml:posList srsDimension="3">456456.3638 5428766.778 128.8 456456.3638 5428766.778 129.2403 456455.0161 5428795.7954 129.24 456455.0161 5428795.7954 128.8 456456.3638 5428766.778 128.8</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_bb53f52a-9e34-474b-99fd-7ca620921878_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>145.319</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>bb53f52a-9e34-474b-99fd-7ca620921878</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855692_19313">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_bb53f52a-9e34-474b-99fd-7ca620921878_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32780">
                      <gml:posList srsDimension="3">456457.36 5428814.98 112.38 456457.36 5428814.98 128.1 456466.6 5428815.26 128.1 456466.6 5428815.26 112.38 456457.36 5428814.98 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_74365ec2-3eb8-4f9c-9931-a0ef71cd10a2_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>3.806</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>74365ec2-3eb8-4f9c-9931-a0ef71cd10a2</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855692_19314">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_74365ec2-3eb8-4f9c-9931-a0ef71cd10a2_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32781">
                      <gml:posList srsDimension="3">456457.3608 5428814.2311 128.1 456457.3608 5428814.2311 128.5105 456466.6334 5428814.5121 128.51 456466.6334 5428814.5121 128.1 456457.3608 5428814.2311 128.1</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_6113f106-1834-4c19-bc9e-05a5d0db3778_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>12.812</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>6113f106-1834-4c19-bc9e-05a5d0db3778</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855692_19315">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_6113f106-1834-4c19-bc9e-05a5d0db3778_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32782">
                      <gml:posList srsDimension="3">456461.6143 5428795.9883 128.8 456461.6143 5428795.9883 129.24 456463.0326 5428766.9055 129.24 456463.0326 5428766.9055 128.8 456461.6143 5428795.9883 128.8</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_a21cfa2b-75dd-423e-9f7c-c7f13dbd6a91_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>399.937</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>a21cfa2b-75dd-423e-9f7c-c7f13dbd6a91</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855692_19316">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_a21cfa2b-75dd-423e-9f7c-c7f13dbd6a91_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32783">
                      <gml:posList srsDimension="3">456462.32 5428797.09 112.38 456462.32 5428797.09 128.8 456463.46 5428772.76 128.8 456463.46 5428772.76 112.38 456462.32 5428797.09 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_11757287-b8b2-425b-8cc0-2fec491f3a65_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>81.531</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>11757287-b8b2-425b-8cc0-2fec491f3a65</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855692_19317">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_11757287-b8b2-425b-8cc0-2fec491f3a65_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32784">
                      <gml:posList srsDimension="3">456463.46 5428772.76 112.38 456463.46 5428772.76 128.8 456463.69 5428767.8 128.8 456463.69 5428767.8 112.38 456463.46 5428772.76 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_23c5a7c5-a826-4ff0-bf0f-ae4f246749cd_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.944</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>23c5a7c5-a826-4ff0-bf0f-ae4f246749cd</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855692_19318">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_23c5a7c5-a826-4ff0-bf0f-ae4f246749cd_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32785">
                      <gml:posList srsDimension="3">456463.0326 5428766.9055 128.8 456463.0326 5428766.9055 129.24 456465.1658 5428767.1348 129.24 456465.1658 5428767.1348 128.8 456463.0326 5428766.9055 128.8</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_2c4cc151-a129-4ad0-b545-96f20b97b6d7_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>483.702</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>2c4cc151-a129-4ad0-b545-96f20b97b6d7</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855692_19319">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_2c4cc151-a129-4ad0-b545-96f20b97b6d7_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32786">
                      <gml:posList srsDimension="3">456494.41 5428762.67 112.38 456494.41 5428762.67 128.8 456465.0999999999 5428759.72 128.8 456465.0999999999 5428759.72 112.38 456494.41 5428762.67 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_c8808e4e-ac50-44b8-8906-bafc80a0195f_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>1.301</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>c8808e4e-ac50-44b8-8906-bafc80a0195f</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855692_19320">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_c8808e4e-ac50-44b8-8906-bafc80a0195f_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32787">
                      <gml:posList srsDimension="3">456466.1525 5428765.3993 130.8247 456466.1195 5428767.2308 130.66 456466.1195 5428767.2308 129.24 456466.1525 5428765.3993 130.8247</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_7bd39a73-0493-4735-9aa3-b8b1fad0b6a3_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>143.541</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>7bd39a73-0493-4735-9aa3-b8b1fad0b6a3</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855692_19321">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_7bd39a73-0493-4735-9aa3-b8b1fad0b6a3_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32788">
                      <gml:posList srsDimension="3">456466.6 5428815.26 112.38 456466.6 5428815.26 128.1 456475.72 5428815.71 128.1 456475.72 5428815.71 112.38 456466.6 5428815.26 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_b78d8527-187c-4ae7-869a-a46a0093b60b_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>3.766</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>b78d8527-187c-4ae7-869a-a46a0093b60b</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855692_19322">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_b78d8527-187c-4ae7-869a-a46a0093b60b_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32789">
                      <gml:posList srsDimension="3">456466.6334 5428814.5121 128.1 456466.6334 5428814.5121 128.51 456475.807 5428814.9647 128.51 456475.807 5428814.9647 128.1 456466.6334 5428814.5121 128.1</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_b4f3da06-a3b9-442e-945a-e759f8469d4e_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>192.354</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>b4f3da06-a3b9-442e-945a-e759f8469d4e</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855693_19323">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_b4f3da06-a3b9-442e-945a-e759f8469d4e_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32790">
                      <gml:posList srsDimension="3">456475.72 5428815.71 112.38 456475.72 5428815.71 128.1 456487.94 5428816.34 128.1 456487.94 5428816.34 112.38 456475.72 5428815.71 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_87e0536b-0c45-448e-8ebd-a62cf83bbb8b_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.776</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>87e0536b-0c45-448e-8ebd-a62cf83bbb8b</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855693_19324">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_87e0536b-0c45-448e-8ebd-a62cf83bbb8b_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32791">
                      <gml:posList srsDimension="3">456475.807 5428814.9647 128.1 456475.807 5428814.9647 128.51 456487.4412 5428815.5645 128.51 456487.4412 5428815.5645 128.1 456475.807 5428814.9647 128.1</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_b82836b9-24b2-41c1-b7ca-db02289923f1_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>50.994</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>b82836b9-24b2-41c1-b7ca-db02289923f1</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855693_19325">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_b82836b9-24b2-41c1-b7ca-db02289923f1_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32792">
                      <gml:posList srsDimension="3">456480.5999999999 5428802.79 112.38 456480.5999999999 5428802.79 128.1 456477.37 5428802.49 128.1 456477.37 5428802.49 112.38 456480.5999999999 5428802.79 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_6d5c56ea-941c-40b1-a86e-6d33caea6cd7_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>16.58</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>6d5c56ea-941c-40b1-a86e-6d33caea6cd7</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855693_19326">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_6d5c56ea-941c-40b1-a86e-6d33caea6cd7_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32793">
                      <gml:posList srsDimension="3">456484.55 5428769.51 112.38 456484.55 5428769.51 128.8 456485.55 5428769.65 128.8 456485.55 5428769.65 112.38 456484.55 5428769.51 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_9a28e6a8-7249-47e8-97aa-e6d9e8147421_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>50.879</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>9a28e6a8-7249-47e8-97aa-e6d9e8147421</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855693_19327">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_9a28e6a8-7249-47e8-97aa-e6d9e8147421_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32794">
                      <gml:posList srsDimension="3">456490.37 5428770.31 112.38 456490.37 5428770.31 128.8 456493.44 5428770.73 128.8 456493.44 5428770.73 112.38 456490.37 5428770.31 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_bfa9edd2-e7d0-43f9-b5ab-460e70a20b29_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>1.296</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>bfa9edd2-e7d0-43f9-b5ab-460e70a20b29</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855693_19328">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_bfa9edd2-e7d0-43f9-b5ab-460e70a20b29_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32795">
                      <gml:posList srsDimension="3">456487.3112 5428769.363699999 129.24 456487.3112 5428769.363699999 130.66 456487.5116 5428767.5491 130.8247 456487.3112 5428769.363699999 129.24</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_084f8e1e-95de-41b1-b2ae-825e31def2bd_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>11.739</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>084f8e1e-95de-41b1-b2ae-825e31def2bd</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855693_19329">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_084f8e1e-95de-41b1-b2ae-825e31def2bd_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32796">
                      <gml:posList srsDimension="3">456481.81 5428799.36 112.38 456481.81 5428799.36 128.1 456481.07 5428799.26 128.1 456481.07 5428799.26 112.38 456481.81 5428799.36 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_d1b21a80-a7a1-419e-b298-d6aeadafa89a_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>55.981</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>d1b21a80-a7a1-419e-b298-d6aeadafa89a</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855693_19330">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_d1b21a80-a7a1-419e-b298-d6aeadafa89a_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32797">
                      <gml:posList srsDimension="3">456481.07 5428799.26 112.38 456481.07 5428799.26 128.1 456480.5999999999 5428802.79 128.1 456480.5999999999 5428802.79 112.38 456481.07 5428799.26 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_88b3c4e2-1997-4ff2-beea-38c8879212ec_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.801</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>88b3c4e2-1997-4ff2-beea-38c8879212ec</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855693_19331">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_88b3c4e2-1997-4ff2-beea-38c8879212ec_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32798">
                      <gml:posList srsDimension="3">456482.4596 5428798.586 129.0 456482.4596 5428798.586 129.12 456489.0729 5428799.4778 129.12 456489.0729 5428799.4778 129.0 456482.4596 5428798.586 129.0</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_c9cdf605-8420-4529-93a8-ebec0edbff83_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.766</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>c9cdf605-8420-4529-93a8-ebec0edbff83</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855693_19332">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_c9cdf605-8420-4529-93a8-ebec0edbff83_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32799">
                      <gml:posList srsDimension="3">456485.2316 5428775.624999999 129.0 456485.2316 5428775.624999999 129.1192 456482.4596 5428798.586 129.12 456482.4596 5428798.586 129.0 456485.2316 5428775.624999999 129.0</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_eeef5dfe-8336-47cb-b75f-7d4e49a623b3_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.937</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>eeef5dfe-8336-47cb-b75f-7d4e49a623b3</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855693_19333">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_eeef5dfe-8336-47cb-b75f-7d4e49a623b3_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32800">
                      <gml:posList srsDimension="3">456487.4412 5428815.5645 128.1 456487.4412 5428815.5645 128.51 456488.0818 5428803.5407 128.51 456488.0818 5428803.5407 128.1 456487.4412 5428815.5645 128.1</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_f9f43977-640f-456c-a01b-8253251ed69c_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>51.978</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>f9f43977-640f-456c-a01b-8253251ed69c</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855693_19334">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_f9f43977-640f-456c-a01b-8253251ed69c_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32801">
                      <gml:posList srsDimension="3">456492.83 5428775.77 112.38 456492.83 5428775.77 129.0 456489.72 5428775.44 129.0 456489.72 5428775.44 128.8 456489.72 5428775.44 112.38 456492.83 5428775.77 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_5cb978df-07e5-4547-be38-f4cc7f5c1481_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.974</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>5cb978df-07e5-4547-be38-f4cc7f5c1481</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855693_19335">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_5cb978df-07e5-4547-be38-f4cc7f5c1481_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32802">
                      <gml:posList srsDimension="3">456489.72 5428775.44 128.8 456489.72 5428775.44 129.0 456484.8778 5428774.9276 129.0 456484.8778 5428774.9276 128.8 456489.72 5428775.44 128.8</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_f4a7cd70-35bc-4a89-98ff-b6b6f217411d_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>84.908</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>f4a7cd70-35bc-4a89-98ff-b6b6f217411d</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855693_19336">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_f4a7cd70-35bc-4a89-98ff-b6b6f217411d_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32803">
                      <gml:posList srsDimension="3">456489.72 5428775.44 112.38 456489.72 5428775.44 128.8 456490.37 5428770.31 128.8 456490.37 5428770.31 112.38 456489.72 5428775.44 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_32cb6204-9631-4c0d-9597-f404ae2e68cf_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>87.067</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>32cb6204-9631-4c0d-9597-f404ae2e68cf</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855693_19337">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_32cb6204-9631-4c0d-9597-f404ae2e68cf_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32804">
                      <gml:posList srsDimension="3">456485.55 5428769.65 112.38 456485.55 5428769.65 128.8 456484.88 5428774.91 128.8 456484.88 5428774.91 112.38 456485.55 5428769.65 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_86cb719e-e844-4a75-b682-331abaa6b252_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.798</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>86cb719e-e844-4a75-b682-331abaa6b252</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855693_19338">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_86cb719e-e844-4a75-b682-331abaa6b252_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32805">
                      <gml:posList srsDimension="3">456491.8657 5428776.3372 129.0 456491.8657 5428776.3372 129.12 456485.2316 5428775.624999999 129.1192 456485.2316 5428775.624999999 129.0 456491.8657 5428776.3372 129.0</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_d2acc30a-4903-4db9-b4c5-f09854c97a70_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.797</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>d2acc30a-4903-4db9-b4c5-f09854c97a70</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855693_19339">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_d2acc30a-4903-4db9-b4c5-f09854c97a70_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32806">
                      <gml:posList srsDimension="3">456489.0729 5428799.4778 129.0 456489.0729 5428799.4778 129.12 456491.8657 5428776.3372 129.12 456491.8657 5428776.3372 129.0 456489.0729 5428799.4778 129.0</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_d5098de2-976a-49a2-bae8-9df4815d8387_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>39.205</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>d5098de2-976a-49a2-bae8-9df4815d8387</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855693_19340">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_d5098de2-976a-49a2-bae8-9df4815d8387_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32807">
                      <gml:posList srsDimension="3">456487.94 5428816.34 112.38 456487.94 5428816.34 128.1 456488.08 5428813.85 128.1 456488.08 5428813.85 112.38 456487.94 5428816.34 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_abaae07c-bbb1-4872-b556-2e06311a4a89_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>412.666</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>abaae07c-bbb1-4872-b556-2e06311a4a89</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855693_19341">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_abaae07c-bbb1-4872-b556-2e06311a4a89_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32808">
                      <gml:posList srsDimension="3">456489.85 5428800.42 112.38 456489.85 5428800.42 129.0 456492.83 5428775.77 129.0 456492.83 5428775.77 112.38 456489.85 5428800.42 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_958ed03b-5f31-4953-91d7-6deb4cd0c501_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>3.024</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>958ed03b-5f31-4953-91d7-6deb4cd0c501</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855693_19342">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_958ed03b-5f31-4953-91d7-6deb4cd0c501_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32809">
                      <gml:posList srsDimension="3">456492.5197 5428769.8879 128.8 456492.5197 5428769.8879 129.24 456493.32989999995 5428763.063 129.24 456493.32989999995 5428763.063 128.8 456492.5197 5428769.8879 128.8</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_5ce31595-0248-4247-a87f-3491f61d51a6_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>133.3</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>5ce31595-0248-4247-a87f-3491f61d51a6</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855693_19343">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_5ce31595-0248-4247-a87f-3491f61d51a6_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32810">
                      <gml:posList srsDimension="3">456493.44 5428770.73 112.38 456493.44 5428770.73 128.8 456494.41 5428762.67 128.8 456494.41 5428762.67 112.38 456493.44 5428770.73 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001URh_cc83aba5-a235-4eb3-a7d3-842797290ac7_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2083.87</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>cc83aba5-a235-4eb3-a7d3-842797290ac7</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855693_19344">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_cc83aba5-a235-4eb3-a7d3-842797290ac7_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32811">
                      <gml:posList srsDimension="3">456465.0999999999 5428759.72 112.38 456451.13 5428759.51 112.38 456435.87 5428759.58 112.38 456425.78 5428760.05 112.38 456426.16 5428767.96 112.38 456428.83 5428767.79 112.38 456428.831 5428767.8195 112.38 456429.01 5428772.88 112.38 456426.7 5428773.05 112.38 456428.51999999996 5428798.36 112.38 456428.5406 5428798.3584 112.38 456431.03 5428798.17 112.38 456431.28 5428801.77 112.38 456430.6893 5428801.7831 112.38 456429.5233 5428801.809 112.38 456429.48 5428801.81 112.38 456429.68 5428812.27 112.38 456429.72 5428814.66 112.38 456442.167 5428814.6103 112.38 456442.25 5428814.61 112.38 456457.36 5428814.98 112.38 456466.6 5428815.26 112.38 456475.72 5428815.71 112.38 456487.94 5428816.34 112.38 456488.08 5428813.85 112.38 456488.6605 5428803.2056 112.38 456488.82 5428800.28 112.38 456489.85 5428800.42 112.38 456492.83 5428775.77 112.38 456489.72 5428775.44 112.38 456490.37 5428770.31 112.38 456493.44 5428770.73 112.38 456494.41 5428762.67 112.38 456465.0999999999 5428759.72 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                  <gml:interior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32812">
                      <gml:posList srsDimension="3">456484.88 5428774.91 112.38 456484.8778 5428774.9276 112.38 456484.861 5428775.0614 112.38 456481.8141 5428799.3272 112.38 456481.81 5428799.36 112.38 456481.07 5428799.26 112.38 456480.5999999999 5428802.79 112.38 456477.37 5428802.49 112.38 456476.991 5428802.4718 112.38 456472.1311 5428802.2383 112.38 456471.75 5428802.22 112.38 456460.2173 5428801.9723 112.38 456460.11 5428801.97 112.38 456460.1115 5428801.9399 112.38 456460.3516 5428797.177 112.38 456460.36 5428797.01 112.38 456462.32 5428797.09 112.38 456463.46 5428772.76 112.38 456463.69 5428767.8 112.38 456463.7655999999 5428767.8029 112.38 456465.0 5428767.849999999 112.38 456465.4669 5428767.887299999 112.38 456476.76 5428768.79 112.38 456478.3124 5428768.9335 112.38 456484.55 5428769.51 112.38 456485.55 5428769.65 112.38 456484.88 5428774.91 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:interior>
                  <gml:interior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32813">
                      <gml:posList srsDimension="3">456455.64 5428772.46 112.38 456454.53549999994 5428796.2665 112.38 456454.5214 5428796.5703 112.38 456454.52 5428796.6 112.38 456453.1 5428796.55 112.38 456452.89 5428801.9 112.38 456440.23 5428801.83 112.38 456439.9199999999 5428797.49 112.38 456436.57 5428797.75 112.38 456436.566 5428797.695699999 112.38 456436.563 5428797.6549 112.38 456434.7054999999 5428772.5347 112.38 456434.7 5428772.46 112.38 456434.03 5428772.5115 112.38 456433.79 5428772.53 112.38 456433.7344 5428771.1955 112.38 456433.5833 5428767.5686 112.38 456433.58 5428767.49 112.38 456433.8076 5428767.483 112.38 456438.5959 5428767.3351 112.38 456439.9425 5428767.2935 112.38 456440.38 5428767.28 112.38 456449.7113999999 5428767.3788 112.38 456454.54 5428767.429999999 112.38 456455.7357 5428767.475 112.38 456455.87 5428767.48 112.38 456455.64 5428772.46 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:interior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_3dbdebae-fcba-4e2b-b4fc-74f81fc2b10e_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>52.88</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>90.0</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>129.0</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.0</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>-1.0</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>3dbdebae-fcba-4e2b-b4fc-74f81fc2b10e</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855693_19345">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_3dbdebae-fcba-4e2b-b4fc-74f81fc2b10e_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32814">
                      <gml:posList srsDimension="3">456429.01 5428772.88 129.0 456433.7255 5428772.5317 129.0 456433.79 5428772.53 129.0 456434.03 5428772.5115 129.0 456434.7 5428772.46 129.0 456434.7054999999 5428772.5347 129.0 456436.563 5428797.6549 129.0 456436.566 5428797.695699999 129.0 456431.03 5428798.17 129.0 456428.5406 5428798.3584 129.0 456428.51999999996 5428798.36 129.0 456426.7 5428773.05 129.0 456429.01 5428772.88 129.0</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                  <gml:interior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32815">
                      <gml:posList srsDimension="3">456429.2407 5428797.3566 129.0 456435.6607 5428796.8842 129.0 456433.9583 5428773.241 129.0 456427.6634 5428773.7042 129.0 456429.2407 5428797.3566 129.0</gml:posList>
                    </gml:LinearRing>
                  </gml:interior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_16bcf045-314e-4ed5-b94b-efd7faad780c_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>123.764</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>49.84</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.51</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.51</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>177.087</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>16bcf045-314e-4ed5-b94b-efd7faad780c</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855693_19346">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_16bcf045-314e-4ed5-b94b-efd7faad780c_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32816">
                      <gml:posList srsDimension="3">456472.65499999997 5428802.7454 128.51 456477.77669999993 5428803.0094 128.51 456480.9002 5428803.1705 128.51 456488.0818 5428803.5407 128.51 456483.3654 5428809.2277 133.51 456476.5177 5428808.8747 133.51 456466.9061 5428808.4004 133.51 456472.65499999997 5428802.7454 128.51</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_35749e83-1d83-4b05-a8fa-d440689798fc_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>44.066</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>90.0</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>129.0</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.0</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>-1.0</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>35749e83-1d83-4b05-a8fa-d440689798fc</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855693_19347">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_35749e83-1d83-4b05-a8fa-d440689798fc_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32817">
                      <gml:posList srsDimension="3">456484.861 5428775.0614 129.0 456484.8778 5428774.9276 129.0 456489.72 5428775.44 129.0 456492.83 5428775.77 129.0 456489.85 5428800.42 129.0 456488.82 5428800.28 129.0 456481.8141 5428799.3272 129.0 456484.861 5428775.0614 129.0</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                  <gml:interior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32818">
                      <gml:posList srsDimension="3">456489.0729 5428799.4778 129.0 456491.8657 5428776.3372 129.0 456485.2316 5428775.624999999 129.0 456482.4596 5428798.586 129.0 456489.0729 5428799.4778 129.0</gml:posList>
                    </gml:LinearRing>
                  </gml:interior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_3d712632-4097-47c1-ab61-a5d40d372e97_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>421.418</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>3d712632-4097-47c1-ab61-a5d40d372e97</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855693_19348">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_3d712632-4097-47c1-ab61-a5d40d372e97_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32819">
                      <gml:posList srsDimension="3">456434.7 5428772.46 112.38 456434.7054999999 5428772.5347 112.38 456436.563 5428797.6549 112.38 456436.566 5428797.695699999 112.38 456436.57 5428797.75 112.38 456436.57 5428797.75 128.1 456436.566 5428797.695699999 128.1 456436.566 5428797.695699999 129.0 456436.563 5428797.6549 129.0 456434.7054999999 5428772.5347 129.0 456434.7 5428772.46 129.0 456434.7 5428772.46 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_b7f96a0b-63f4-4654-b574-26c1d97291ed_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>42.341</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>b7f96a0b-63f4-4654-b574-26c1d97291ed</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855694_19349">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_b7f96a0b-63f4-4654-b574-26c1d97291ed_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32820">
                      <gml:posList srsDimension="3">456465.1658 5428767.1348 129.24 456466.1195 5428767.2308 129.24 456466.1195 5428767.2308 130.66 456478.3798 5428768.4648 130.66 456487.3112 5428769.363699999 130.66 456487.3112 5428769.363699999 129.24 456492.5197 5428769.8879 129.24 456492.5197 5428769.8879 128.8 456487.3112 5428769.363699999 128.8 456478.3798 5428768.4648 128.8 456466.1195 5428767.2308 128.8 456465.1658 5428767.1348 128.8 456465.1658 5428767.1348 129.24</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_3013f24d-3c9b-488d-b054-45c456921334_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>409.516</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>3013f24d-3c9b-488d-b054-45c456921334</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855694_19350">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_3013f24d-3c9b-488d-b054-45c456921334_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32821">
                      <gml:posList srsDimension="3">456481.8141 5428799.3272 112.38 456484.861 5428775.0614 112.38 456484.8778 5428774.9276 112.38 456484.88 5428774.91 112.38 456484.88 5428774.91 128.8 456484.8778 5428774.9276 128.8 456484.8778 5428774.9276 129.0 456484.861 5428775.0614 129.0 456481.8141 5428799.3272 129.0 456481.8141 5428799.3272 128.1 456481.81 5428799.36 128.1 456481.81 5428799.36 112.38 456481.8141 5428799.3272 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_5ec42c9c-bfee-459c-ad03-025d7bbb542c_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>184.945</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>90.0</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>128.1</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.1</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>-1.0</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>5ec42c9c-bfee-459c-ad03-025d7bbb542c</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855694_19351">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_5ec42c9c-bfee-459c-ad03-025d7bbb542c_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32822">
                      <gml:posList srsDimension="3">456429.5233 5428801.809 128.1 456430.6893 5428801.7831 128.1 456431.28 5428801.77 128.1 456431.03 5428798.17 128.1 456436.566 5428797.695699999 128.1 456436.57 5428797.75 128.1 456439.9199999999 5428797.49 128.1 456440.23 5428801.83 128.1 456452.89 5428801.9 128.1 456453.1 5428796.55 128.1 456454.52 5428796.6 128.1 456454.5214 5428796.5703 128.1 456460.3516 5428797.177 128.1 456460.1115 5428801.9399 128.1 456460.11 5428801.97 128.1 456460.2173 5428801.9723 128.1 456471.75 5428802.22 128.1 456472.1311 5428802.2383 128.1 456476.991 5428802.4718 128.1 456477.37 5428802.49 128.1 456480.5999999999 5428802.79 128.1 456481.07 5428799.26 128.1 456481.81 5428799.36 128.1 456481.8141 5428799.3272 128.1 456488.82 5428800.28 128.1 456488.6605 5428803.2056 128.1 456488.08 5428813.85 128.1 456487.94 5428816.34 128.1 456475.72 5428815.71 128.1 456466.6 5428815.26 128.1 456457.36 5428814.98 128.1 456442.25 5428814.61 128.1 456442.167 5428814.6103 128.1 456429.72 5428814.66 128.1 456429.68 5428812.27 128.1 456429.48 5428801.81 128.1 456429.5233 5428801.809 128.1</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                  <gml:interior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32823">
                      <gml:posList srsDimension="3">456430.2634 5428813.9085 128.1 456442.1535 5428813.8611 128.1 456457.3608 5428814.2311 128.1 456466.6334 5428814.5121 128.1 456475.807 5428814.9647 128.1 456487.4412 5428815.5645 128.1 456488.0818 5428803.5407 128.1 456480.9002 5428803.1705 128.1 456477.77669999993 5428803.0094 128.1 456472.65499999997 5428802.7454 128.1 456461.1082 5428802.523499999 128.1 456453.4641 5428802.3767 128.1 456440.4732 5428802.3049 128.1 456430.2552 5428802.3456 128.1 456430.2634 5428813.9085 128.1</gml:posList>
                    </gml:LinearRing>
                  </gml:interior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_f6fab39d-0fdc-473b-9501-a3dc2cfdd1b2_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>173.482</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>90.0</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>128.8</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.8</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>-1.0</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>f6fab39d-0fdc-473b-9501-a3dc2cfdd1b2</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855694_19352">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_f6fab39d-0fdc-473b-9501-a3dc2cfdd1b2_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32824">
                      <gml:posList srsDimension="3">456435.87 5428759.58 128.8 456451.13 5428759.51 128.8 456465.0999999999 5428759.72 128.8 456494.41 5428762.67 128.8 456493.44 5428770.73 128.8 456490.37 5428770.31 128.8 456489.72 5428775.44 128.8 456484.8778 5428774.9276 128.8 456484.88 5428774.91 128.8 456485.55 5428769.65 128.8 456484.55 5428769.51 128.8 456478.3124 5428768.9335 128.8 456476.76 5428768.79 128.8 456465.4669 5428767.887299999 128.8 456465.0 5428767.849999999 128.8 456463.7655999999 5428767.8029 128.8 456463.69 5428767.8 128.8 456463.46 5428772.76 128.8 456462.32 5428797.09 128.8 456460.36 5428797.01 128.8 456460.3516 5428797.177 128.8 456454.5214 5428796.5703 128.8 456454.53549999994 5428796.2665 128.8 456455.64 5428772.46 128.8 456455.87 5428767.48 128.8 456455.7357 5428767.475 128.8 456454.54 5428767.429999999 128.8 456449.7113999999 5428767.3788 128.8 456440.38 5428767.28 128.8 456439.9425 5428767.2935 128.8 456439.5994 5428767.2884 128.8 456438.5959 5428767.3351 128.8 456433.8076 5428767.483 128.8 456433.58 5428767.49 128.8 456433.5833 5428767.5686 128.8 456433.7344 5428771.1955 128.8 456433.7255 5428772.5317 128.8 456429.01 5428772.88 128.8 456428.831 5428767.8195 128.8 456428.83 5428767.79 128.8 456426.16 5428767.96 128.8 456425.78 5428760.05 128.8 456435.87 5428759.58 128.8</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                  <gml:interior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32825">
                      <gml:posList srsDimension="3">456426.6182999999 5428767.117 128.8 456433.1292 5428766.8147 128.8 456434.2627 5428766.762 128.8 456440.22079999995 5428766.734699999 128.8 456450.0586 5428766.6896 128.8 456453.4467 5428766.7371 128.8 456456.3638 5428766.778 128.8 456455.0161 5428795.7954 128.8 456461.6143 5428795.9883 128.8 456463.0326 5428766.9055 128.8 456465.1658 5428767.1348 128.8 456466.1195 5428767.2308 128.8 456478.3798 5428768.4648 128.8 456487.3112 5428769.363699999 128.8 456492.5197 5428769.8879 128.8 456493.32989999995 5428763.063 128.8 456479.2292 5428761.643799999 128.8 456464.9326 5428760.2048 128.8 456451.2284 5428759.9988 128.8 456435.9361 5428760.069 128.8 456426.33959999995 5428760.5149 128.8 456426.6182999999 5428767.117 128.8</gml:posList>
                    </gml:LinearRing>
                  </gml:interior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_7c5ac6ac-547f-418e-84eb-183c7f492f5f_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>28.303</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>7c5ac6ac-547f-418e-84eb-183c7f492f5f</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855694_19353">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_7c5ac6ac-547f-418e-84eb-183c7f492f5f_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32826">
                      <gml:posList srsDimension="3">456429.5233 5428801.809 112.38 456430.6893 5428801.7831 112.38 456431.28 5428801.77 112.38 456431.28 5428801.77 128.1 456430.6893 5428801.7831 128.1 456429.5233 5428801.809 128.1 456429.48 5428801.81 128.1 456429.48 5428801.81 112.38 456429.5233 5428801.809 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_0193946a-7c82-493e-b9bd-0cb7206580ba_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>78.187</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>0193946a-7c82-493e-b9bd-0cb7206580ba</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855694_19354">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_0193946a-7c82-493e-b9bd-0cb7206580ba_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32827">
                      <gml:posList srsDimension="3">456460.36 5428797.01 128.8 456460.36 5428797.01 112.38 456460.3516 5428797.177 112.38 456460.1115 5428801.9399 112.38 456460.11 5428801.97 112.38 456460.11 5428801.97 128.1 456460.1115 5428801.9399 128.1 456460.3516 5428797.177 128.1 456460.3516 5428797.177 128.8 456460.36 5428797.01 128.8</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_5f840533-612a-428d-8fd9-77ba9f0d889c_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>396.784</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>5f840533-612a-428d-8fd9-77ba9f0d889c</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855694_19355">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_5f840533-612a-428d-8fd9-77ba9f0d889c_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32828">
                      <gml:posList srsDimension="3">456454.5214 5428796.5703 112.38 456454.53549999994 5428796.2665 112.38 456455.64 5428772.46 112.38 456455.64 5428772.46 128.8 456454.53549999994 5428796.2665 128.8 456454.5214 5428796.5703 128.8 456454.5214 5428796.5703 128.1 456454.52 5428796.6 128.1 456454.52 5428796.6 112.38 456454.5214 5428796.5703 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_8aafb525-8925-4464-8e94-eadebfb04a19_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>88.448</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>8aafb525-8925-4464-8e94-eadebfb04a19</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855694_19356">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_8aafb525-8925-4464-8e94-eadebfb04a19_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32829">
                      <gml:posList srsDimension="3">456471.75 5428802.22 112.38 456472.1311 5428802.2383 112.38 456476.991 5428802.4718 112.38 456477.37 5428802.49 112.38 456477.37 5428802.49 128.1 456476.991 5428802.4718 128.1 456472.1311 5428802.2383 128.1 456471.75 5428802.22 128.1 456471.75 5428802.22 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_0ca2df6d-65f6-4f59-9fd4-2ba8a11419a6_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>6.333</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>0ca2df6d-65f6-4f59-9fd4-2ba8a11419a6</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855694_19357">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_0ca2df6d-65f6-4f59-9fd4-2ba8a11419a6_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32830">
                      <gml:posList srsDimension="3">456472.65499999997 5428802.7454 128.1 456477.77669999993 5428803.0094 128.1 456480.9002 5428803.1705 128.1 456488.0818 5428803.5407 128.1 456488.0818 5428803.5407 128.51 456480.9002 5428803.1705 128.51 456477.77669999993 5428803.0094 128.51 456472.65499999997 5428802.7454 128.51 456472.65499999997 5428802.7454 128.1</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_1ae7afb5-76ba-4473-a237-d3efd970c92c_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.041</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>1ae7afb5-76ba-4473-a237-d3efd970c92c</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855694_19358">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_1ae7afb5-76ba-4473-a237-d3efd970c92c_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32831">
                      <gml:posList srsDimension="3">456487.5179999999 5428767.4911 130.82999999999998 456487.7108 5428765.7453 132.4 456487.5179999999 5428767.4911 130.8754 456487.5116 5428767.5491 130.8247 456487.5179999999 5428767.4911 130.82999999999998</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_5776a27f-2982-4903-94ee-68baeab94ce0_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>23.639</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>5776a27f-2982-4903-94ee-68baeab94ce0</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855694_19359">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_5776a27f-2982-4903-94ee-68baeab94ce0_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32832">
                      <gml:posList srsDimension="3">456481.8141 5428799.3272 129.0 456488.82 5428800.28 129.0 456489.85 5428800.42 129.0 456489.85 5428800.42 112.38 456488.82 5428800.28 112.38 456488.82 5428800.28 128.1 456481.8141 5428799.3272 128.1 456481.8141 5428799.3272 129.0</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_6b2379eb-8d99-4bc4-9fe7-0505de5db6a6_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>213.637</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>6b2379eb-8d99-4bc4-9fe7-0505de5db6a6</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855694_19360">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_6b2379eb-8d99-4bc4-9fe7-0505de5db6a6_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32833">
                      <gml:posList srsDimension="3">456488.6605 5428803.2056 128.1 456488.82 5428800.28 128.1 456488.82 5428800.28 112.38 456488.6605 5428803.2056 112.38 456488.08 5428813.85 112.38 456488.08 5428813.85 128.1 456488.6605 5428803.2056 128.1</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_c366bccc-950f-44a1-aeee-7a81003b0169_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>128.457</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>c366bccc-950f-44a1-aeee-7a81003b0169</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855694_19361">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_c366bccc-950f-44a1-aeee-7a81003b0169_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32834">
                      <gml:posList srsDimension="3">456476.76 5428768.79 128.8 456478.3124 5428768.9335 128.8 456484.55 5428769.51 128.8 456484.55 5428769.51 112.38 456478.3124 5428768.9335 112.38 456476.76 5428768.79 112.38 456476.76 5428768.79 128.8</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_63d419ad-c9f6-4466-acd4-4adce37247fb_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>12.554</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>63d419ad-c9f6-4466-acd4-4adce37247fb</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855694_19362">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_63d419ad-c9f6-4466-acd4-4adce37247fb_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32835">
                      <gml:posList srsDimension="3">456464.9326 5428760.2048 128.8 456479.2292 5428761.643799999 128.8 456493.32989999995 5428763.063 128.8 456493.32989999995 5428763.063 129.24 456479.2292 5428761.643799999 129.24 456464.9326 5428760.2048 129.2394 456464.9326 5428760.2048 128.8</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_24a41f5b-ba47-4691-b27f-bb28f50d09a7_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.041</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>24a41f5b-ba47-4691-b27f-bb28f50d09a7</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855694_19363">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_24a41f5b-ba47-4691-b27f-bb28f50d09a7_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32836">
                      <gml:posList srsDimension="3">456466.1535 5428765.3408 130.82999999999998 456466.1525 5428765.3993 130.8247 456466.1535 5428765.3408 130.8754 456466.1852 5428763.5787 132.4 456466.1535 5428765.3408 130.82999999999998</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_fafdbc31-03b8-43c0-9a6d-02ca1317e0d1_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>193.715</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>fafdbc31-03b8-43c0-9a6d-02ca1317e0d1</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855694_19364">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_fafdbc31-03b8-43c0-9a6d-02ca1317e0d1_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32837">
                      <gml:posList srsDimension="3">456465.0 5428767.849999999 128.8 456465.4669 5428767.887299999 128.8 456476.76 5428768.79 128.8 456476.76 5428768.79 112.38 456465.4669 5428767.887299999 112.38 456465.0 5428767.849999999 112.38 456465.0 5428767.849999999 128.8</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_1d5b6cca-9fbe-43a1-83e9-68739e6b40ce_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>21.526</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>1d5b6cca-9fbe-43a1-83e9-68739e6b40ce</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855694_19365">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_1d5b6cca-9fbe-43a1-83e9-68739e6b40ce_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32838">
                      <gml:posList srsDimension="3">456463.69 5428767.8 128.8 456463.7655999999 5428767.8029 128.8 456465.0 5428767.849999999 128.8 456465.0 5428767.849999999 112.38 456463.7655999999 5428767.8029 112.38 456463.69 5428767.8 112.38 456463.69 5428767.8 128.8</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_3bfd45c2-f149-4c36-ad6d-0acc59b4a093_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>7.87</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>3bfd45c2-f149-4c36-ad6d-0acc59b4a093</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855694_19366">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_3bfd45c2-f149-4c36-ad6d-0acc59b4a093_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32839">
                      <gml:posList srsDimension="3">456453.4641 5428802.3767 128.1 456461.1082 5428802.523499999 128.1 456472.65499999997 5428802.7454 128.1 456472.65499999997 5428802.7454 128.51 456461.1082 5428802.523499999 128.51 456453.4641 5428802.3767 128.51 456453.4641 5428802.3767 128.1</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_6b53c8b1-c2db-4cbb-8dc6-239ca9e6b265_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.02</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>6b53c8b1-c2db-4cbb-8dc6-239ca9e6b265</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855694_19367">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_6b53c8b1-c2db-4cbb-8dc6-239ca9e6b265_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32840">
                      <gml:posList srsDimension="3">456453.4456 5428764.9414 130.82999999999998 456453.4446 5428763.2192 132.4 456453.4456 5428764.9414 130.853 456453.4456 5428764.9701 130.8273 456453.4456 5428764.9414 130.82999999999998</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_8187fdab-a1fe-4ea9-b074-ade2c1c03051_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>183.023</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>8187fdab-a1fe-4ea9-b074-ade2c1c03051</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855694_19368">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_8187fdab-a1fe-4ea9-b074-ade2c1c03051_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32841">
                      <gml:posList srsDimension="3">456460.11 5428801.97 112.38 456460.2173 5428801.9723 112.38 456471.75 5428802.22 112.38 456471.75 5428802.22 128.1 456460.2173 5428801.9723 128.1 456460.11 5428801.97 128.1 456460.11 5428801.97 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_5f9f2c63-8392-455b-af77-a597a7441444_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>7.586</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>5f9f2c63-8392-455b-af77-a597a7441444</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855694_19369">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_5f9f2c63-8392-455b-af77-a597a7441444_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32842">
                      <gml:posList srsDimension="3">456450.0586 5428766.6896 130.66 456453.4467 5428766.7371 130.66 456453.4467 5428766.7371 129.2397 456456.3638 5428766.778 129.2403 456456.3638 5428766.778 128.8 456453.4467 5428766.7371 128.8 456450.0586 5428766.6896 128.8 456450.0586 5428766.6896 130.66</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_0694e22f-13af-4092-833b-da4bc9103ab5_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>21.854</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>0694e22f-13af-4092-833b-da4bc9103ab5</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855694_19370">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_0694e22f-13af-4092-833b-da4bc9103ab5_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32843">
                      <gml:posList srsDimension="3">456454.54 5428767.429999999 128.8 456455.7357 5428767.475 128.8 456455.87 5428767.48 128.8 456455.87 5428767.48 112.38 456455.7357 5428767.475 112.38 456454.54 5428767.429999999 112.38 456454.54 5428767.429999999 128.8</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_e585766a-72e5-4da6-b1f9-d779c2bf0461_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>232.52</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e585766a-72e5-4da6-b1f9-d779c2bf0461</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855694_19371">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_e585766a-72e5-4da6-b1f9-d779c2bf0461_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32844">
                      <gml:posList srsDimension="3">456440.38 5428767.28 128.8 456449.7113999999 5428767.3788 128.8 456454.54 5428767.429999999 128.8 456454.54 5428767.429999999 112.38 456449.7113999999 5428767.3788 112.38 456440.38 5428767.28 112.38 456440.38 5428767.28 128.8</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_0233270d-83ca-4bfb-b420-550b647791f1_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>29.381</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>0233270d-83ca-4bfb-b420-550b647791f1</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855694_19372">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_0233270d-83ca-4bfb-b420-550b647791f1_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32845">
                      <gml:posList srsDimension="3">456440.22079999995 5428766.734699999 130.66 456450.0586 5428766.6896 130.66 456450.0586 5428766.6896 128.8 456440.22079999995 5428766.734699999 128.8 456434.2627 5428766.762 128.8 456434.2627 5428766.762 130.66 456440.22079999995 5428766.734699999 130.66</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_4808344d-3229-4683-893d-3d93786a79f4_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>196.973</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>4808344d-3229-4683-893d-3d93786a79f4</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855694_19373">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_4808344d-3229-4683-893d-3d93786a79f4_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32846">
                      <gml:posList srsDimension="3">456442.167 5428814.6103 128.1 456442.25 5428814.61 128.1 456442.25 5428814.61 112.38 456442.167 5428814.6103 112.38 456429.72 5428814.66 112.38 456429.72 5428814.66 128.1 456442.167 5428814.6103 128.1</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_878d638f-1c4f-44c4-9905-adee483b6d28_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>83.63</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>878d638f-1c4f-44c4-9905-adee483b6d28</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855695_19374">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_878d638f-1c4f-44c4-9905-adee483b6d28_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32847">
                      <gml:posList srsDimension="3">456428.83 5428767.79 128.8 456428.831 5428767.8195 128.8 456429.01 5428772.88 128.8 456429.01 5428772.88 112.38 456428.831 5428767.8195 112.38 456428.83 5428767.79 112.38 456428.83 5428767.79 128.8</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_133707c7-0d76-4603-9492-5316692722e1_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>15.169</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>133707c7-0d76-4603-9492-5316692722e1</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855695_19375">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_133707c7-0d76-4603-9492-5316692722e1_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32848">
                      <gml:posList srsDimension="3">456434.03 5428772.5115 112.38 456434.7 5428772.46 112.38 456434.7 5428772.46 129.0 456434.03 5428772.5115 129.0 456433.79 5428772.53 129.0 456433.79 5428772.53 112.38 456434.03 5428772.5115 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_672be91e-d1d7-4c51-a70d-689e274dc4f5_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>41.836</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>672be91e-d1d7-4c51-a70d-689e274dc4f5</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855695_19376">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_672be91e-d1d7-4c51-a70d-689e274dc4f5_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32849">
                      <gml:posList srsDimension="3">456428.5406 5428798.3584 129.0 456431.03 5428798.17 129.0 456431.03 5428798.17 128.1 456431.03 5428798.17 112.38 456428.5406 5428798.3584 112.38 456428.51999999996 5428798.36 112.38 456428.51999999996 5428798.36 129.0 456428.5406 5428798.3584 129.0</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_a401b647-f1eb-42ce-9cec-464120fa69b0_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.02</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>a401b647-f1eb-42ce-9cec-464120fa69b0</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855695_19377">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_a401b647-f1eb-42ce-9cec-464120fa69b0_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32850">
                      <gml:posList srsDimension="3">456433.0661 5428765.0725 130.82999999999998 456433.0672 5428765.1009 130.8272 456433.0661 5428765.0725 130.8535 456433.0057 5428763.4028 132.4 456433.0661 5428765.0725 130.82999999999998</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_fc64aaa2-3677-47f0-9838-0de9f638dd53_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.978</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>fc64aaa2-3677-47f0-9838-0de9f638dd53</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855695_19378">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_fc64aaa2-3677-47f0-9838-0de9f638dd53_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32851">
                      <gml:posList srsDimension="3">456433.1292 5428766.8147 129.24 456433.1292 5428766.8147 130.66 456434.2627 5428766.762 130.66 456434.2627 5428766.762 128.8 456433.1292 5428766.8147 128.8 456426.6182999999 5428767.117 128.8 456426.6182999999 5428767.117 129.24 456433.1292 5428766.8147 129.24</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_c91cafb9-deea-42b8-9b97-a00234bba3cb_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>82.4</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>c91cafb9-deea-42b8-9b97-a00234bba3cb</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855695_19379">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_c91cafb9-deea-42b8-9b97-a00234bba3cb_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32852">
                      <gml:posList srsDimension="3">456433.8076 5428767.483 128.8 456438.5959 5428767.3351 128.8 456438.5959 5428767.3351 112.38 456433.8076 5428767.483 112.38 456433.58 5428767.49 112.38 456433.58 5428767.49 128.8 456433.8076 5428767.483 128.8</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001URh_d7338baf-eadc-4447-925f-05c2e07dda50_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>60.897</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>d7338baf-eadc-4447-925f-05c2e07dda50</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855695_19380">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_d7338baf-eadc-4447-925f-05c2e07dda50_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32853">
                      <gml:posList srsDimension="3">456433.58 5428767.49 112.38 456433.5833 5428767.5686 112.38 456433.7344 5428771.1955 112.38 456433.7344 5428771.1955 128.8 456433.5833 5428767.5686 128.8 456433.58 5428767.49 128.8 456433.58 5428767.49 112.38</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_512d21f8-1b69-4040-8dd5-692702007d49_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>147.417</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>50.743</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.51</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.51</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>357.113</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>512d21f8-1b69-4040-8dd5-692702007d49</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855695_19381">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_512d21f8-1b69-4040-8dd5-692702007d49_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32854">
                      <gml:posList srsDimension="3">456466.9061 5428808.4004 133.51 456476.5177 5428808.8747 133.51 456483.3654 5428809.2277 133.51 456487.4412 5428815.5645 128.51 456475.807 5428814.9647 128.51 456466.6334 5428814.5121 128.51 456466.9061 5428808.4004 133.51</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_06f077d8-ebf9-4fb8-ba32-04713d8a2dda_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>50.775</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>48.207</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.4</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.83</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>354.253</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>06f077d8-ebf9-4fb8-ba32-04713d8a2dda</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855695_19382">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_06f077d8-ebf9-4fb8-ba32-04713d8a2dda_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32855">
                      <gml:posList srsDimension="3">456466.1852 5428763.5787 132.4 456478.8746 5428764.8559 132.4 456487.7108 5428765.7453 132.4 456487.5179999999 5428767.4911 130.82999999999998 456478.6358 5428766.5971 130.82999999999998 456466.1535 5428765.3408 130.82999999999998 456466.1852 5428763.5787 132.4</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_7e8870d6-6642-4b04-aebf-074d3c34a47b_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>40.453</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>84.844</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>130.83</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.66</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>354.253</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>7e8870d6-6642-4b04-aebf-074d3c34a47b</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855695_19383">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_7e8870d6-6642-4b04-aebf-074d3c34a47b_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32856">
                      <gml:posList srsDimension="3">456466.1525 5428765.3993 130.8247 456466.1535 5428765.3408 130.82999999999998 456478.6358 5428766.5971 130.82999999999998 456487.5179999999 5428767.4911 130.82999999999998 456487.5116 5428767.5491 130.8247 456487.3112 5428769.363699999 130.66 456478.3798 5428768.4648 130.66 456466.1195 5428767.2308 130.66 456466.1525 5428765.3993 130.8247</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_aa219a50-e16b-4210-b620-91af9a13b521_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>121.806</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>45.639</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.402</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.239</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>174.251</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>aa219a50-e16b-4210-b620-91af9a13b521</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855695_19384">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_aa219a50-e16b-4210-b620-91af9a13b521_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32857">
                      <gml:posList srsDimension="3">456464.9326 5428760.2048 129.2394 456479.2292 5428761.643799999 129.24 456493.32989999995 5428763.063 129.24 456489.46699999995 5428765.922 132.4 456487.7108 5428765.7453 132.4 456478.8746 5428764.8559 132.4 456466.1852 5428763.5787 132.4 456464.2375 5428763.3827 132.40189999999998 456464.9326 5428760.2048 129.2394</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_e12fbb57-d37f-4ef7-b77e-2c2f2c71fd42_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>35.974</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>47.431</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.4</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.83</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>0.263</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e12fbb57-d37f-4ef7-b77e-2c2f2c71fd42</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855695_19385">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_e12fbb57-d37f-4ef7-b77e-2c2f2c71fd42_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32858">
                      <gml:posList srsDimension="3">456436.0516 5428763.2612 132.4 456443.1575 5428763.2287 132.4 456451.456 5428763.1906 132.4 456450.7722 5428764.9029 130.82999999999998 456441.7203 5428764.9445 130.82999999999998 456435.1760999999 5428764.9745 130.82999999999998 456436.0516 5428763.2612 132.4</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001URh_42c61cfb-294a-4fcf-be55-73b781948277_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>28.118</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>84.555</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>130.83</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.66</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>0.263</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>42c61cfb-294a-4fcf-be55-73b781948277</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940855695_19386">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001URh_42c61cfb-294a-4fcf-be55-73b781948277_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572942213402_32859">
                      <gml:posList srsDimension="3">456435.1760999999 5428764.9745 130.82999999999998 456441.7203 5428764.9445 130.82999999999998 456450.7722 5428764.9029 130.82999999999998 456450.0586 5428766.6896 130.66 456440.22079999995 5428766.734699999 130.66 456434.2627 5428766.762 130.66 456435.1760999999 5428764.9745 130.82999999999998</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:address xlink:href="#UUID_23beaabe-7b86-4214-bacd-366e38991feb"/>
    </bldg:Building>
  </core:cityObjectMember>
</core:CityModel>