<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<core:CityModel xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:tex="http://www.opengis.net/citygml/texturedsurface/2.0" xmlns:core="http://www.opengis.net/citygml/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:frn="http://www.opengis.net/citygml/cityfurniture/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd http://www.opengis.net/citygml/appearance/2.0 http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/cityobjectgroup/2.0 http://schemas.opengis.net/citygml/cityobjectgroup/2.0/cityObjectGroup.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/cityfurniture/2.0 http://schemas.opengis.net/citygml/cityfurniture/2.0/cityFurniture.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd">
  <core:cityObjectMember>
    <bldg:Building gml:id="bldg_DEBWL51140001tiF">
      <gml:name/>
      <gml:boundedBy>
        <gml:Envelope srsName="urn:ogc:def:crs:EPSG::25832" srsDimension="3">
          <gml:lowerCorner>456178.69 5428623.58 114.86</gml:lowerCorner>
          <gml:upperCorner>456204.4 5428647.23 136.27</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <core:creationDate>2019-06-28</core:creationDate>
      <core:externalReference>
        <core:informationSystem>http://www.karlsruhe.de/b3/bauen/geodaten/3dgis</core:informationSystem>
        <core:externalObject>
          <core:uri>DEBWL51140001tiF</core:uri>
        </core:externalObject>
      </core:externalReference>
      <gen:doubleAttribute name="HoeheGrund">
        <gen:value>114.862</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="HoeheDach">
        <gen:value>136.27</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="NiedrigsteTraufeDesGebaeudes">
        <gen:value>127.45</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Dachtypen">
        <gen:value>PULTDACH;PULTDACH;PULTDACH;FLACHDACH</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Dachkodierungsliste">
        <gen:value>2100;2100;2100;1000</gen:value>
      </gen:stringAttribute>
      <gen:doubleAttribute name="Volumen">
        <gen:value>6004.694</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Verfahren">
        <gen:value>BREC_2000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Objektart">
        <gen:value>Hauptgebaeude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Strassenschluessel">
        <gen:value>06390</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Strassenname">
        <gen:value>Ritterstraße</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Hausnummer">
        <gen:value>7</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="creationUser">
        <gen:value>Firma VCS</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Feinheit">
        <gen:value>Basis</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="AmtlicherGemeindeschluessel">
        <gen:value>08212000</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="DatenquelleDachhoehe">
        <gen:value>1000</gen:value>
      </gen:intAttribute>
      <gen:intAttribute name="DatenquelleLage">
        <gen:value>1000</gen:value>
      </gen:intAttribute>
      <gen:intAttribute name="DatenquelleBodenhoehe">
        <gen:value>1100</gen:value>
      </gen:intAttribute>
      <bldg:function>31001_1123</bldg:function>
      <bldg:roofType>2100</bldg:roofType>
      <bldg:measuredHeight uom="urn:adv:uom:m">21.408</bldg:measuredHeight>
      <bldg:lod2Solid>
        <gml:Solid>
          <gml:exterior>
            <gml:CompositeSurface>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_37f0cd03-ae0e-4447-b8bf-5f4c38a6251c_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_d6ff269b-832e-4f89-b6a8-dd023fd2f13d_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_f928006d-8747-425b-b519-b053e5c3a29c_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_fccf8a6f-3570-4ca3-9a66-6977ad42abd2_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_57e49e0c-ef98-4195-893e-5e7acfbcfc02_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_60fbad93-8ce0-4665-86ac-79174c243c5c_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_9ca49b54-90bc-4835-a30b-631b139a0024_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_cc25c847-a158-4e8b-805e-331a0af09936_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_1bb0d716-3cc1-4d0b-81e4-343017355abd_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_68be9167-781a-4036-b5ea-cabb4ed4aa66_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_45f4e776-4a38-4775-bca7-e9d0791dd490_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_6d4ad7ab-76b8-45c3-acd3-b5c5cb48e6eb_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_b36072af-1b79-4230-a553-5c9fdd1f6505_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_bbe28498-6cee-4c07-8536-c600fa53f534_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_40662d1a-6fe5-48e7-b08b-e0d436e2a72b_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_1314220f-1a11-40ea-969c-234e3bfd30b9_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_c061807c-8b70-4576-95cb-aa3f9e289394_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_67253e46-7efa-46d0-9653-e3a0ce994a22_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_7b8e1bef-7038-403c-8c92-7dafbcae2f2e_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_1608b1e6-bd4f-4448-921e-014be14bc4c9_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_77ee7057-a7fe-40fa-af4a-72f2530942b5_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_a41e2a1e-16a9-48e4-bde4-5536dc3ee938_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_dd50906f-41d6-4e02-93fd-36da3122d525_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_6e8f2f42-bac2-42ce-a8af-6546ae510d29_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_cdd3543d-9003-48b6-8c34-c1a8eeb9a366_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_b3b6a673-c6ef-4f97-a941-7de11e322d99_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_0a1827af-ba6e-4732-901d-bf73420954fb_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_f5d6cf46-860c-43ab-bcba-b2b72fb65f05_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_25c09492-8439-4b1e-a522-c0c4b2ad54d8_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_3aabee52-e148-4120-8a6e-5c00297c02f9_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_af01e87a-856d-4f20-95ed-90f1844dca4b_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_30d1bd3b-ab79-4971-8803-94675ad351db_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_e00bb528-ed5f-4367-baed-be775b17bff1_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_d9080912-2dc0-4129-a5cd-2760006814e2_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_7253127a-1019-48f9-98c5-b99abbac56b8_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_30724633-425c-4af9-a7ea-0cb83f33a978_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_6eafef3d-42e1-4a0b-be86-a29fee2573db_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_37316563-689c-494e-822d-0ea58c9f596b_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_95a960a0-ac70-4347-9d6a-d5f336801d79_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_f579056b-5d19-4672-80a4-11f46dcf84bc_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_9361fbf4-84cf-4be9-9930-754bac54503f_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_70fd4d6d-5431-4e34-8a5c-72a683f3e2c2_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_1d8a9155-64ec-4fc6-9022-f7425a14ee2f_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001tiF_88bb7f8a-f861-4889-b185-cd375bc4ef02_2_poly"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001tiF_37f0cd03-ae0e-4447-b8bf-5f4c38a6251c_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>118.285</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>43.316</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>136.27</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.95</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>296.643</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>37f0cd03-ae0e-4447-b8bf-5f4c38a6251c</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856104_27815">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_37f0cd03-ae0e-4447-b8bf-5f4c38a6251c_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892134_32123">
                      <gml:posList srsDimension="3">456185.2508 5428630.8109 136.27 456189.5586 5428639.3972 136.27 456190.2822 5428640.8395 136.27 456188.6451 5428646.8341 131.8671 456187.51 5428646.5 130.95 456178.69 5428628.92 130.95 456185.2508 5428630.8109 136.27</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001tiF_d6ff269b-832e-4f89-b6a8-dd023fd2f13d_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>17.627</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>40.798</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>136.27</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.95</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>206.184</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>d6ff269b-832e-4f89-b6a8-dd023fd2f13d</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856104_27816">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_d6ff269b-832e-4f89-b6a8-dd023fd2f13d_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892134_32124">
                      <gml:posList srsDimension="3">456185.2508 5428630.8109 136.27 456178.69 5428628.92 130.95 456183.1916 5428626.7065 130.95 456185.2508 5428630.8109 136.27</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001tiF_f928006d-8747-425b-b519-b053e5c3a29c_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>3.669</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>39.373</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>134.87</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.95</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>189.341</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>f928006d-8747-425b-b519-b053e5c3a29c</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856104_27817">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_f928006d-8747-425b-b519-b053e5c3a29c_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892134_32125">
                      <gml:posList srsDimension="3">456185.7411 5428628.7072 133.86 456184.7025 5428629.718 134.87 456183.1916 5428626.7065 130.95 456185.7411 5428628.7072 133.86</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001tiF_fccf8a6f-3570-4ca3-9a66-6977ad42abd2_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.848</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>45.102</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.95</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>206.185</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>fccf8a6f-3570-4ca3-9a66-6977ad42abd2</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856104_27818">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_fccf8a6f-3570-4ca3-9a66-6977ad42abd2_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892134_32126">
                      <gml:posList srsDimension="3">456184.4315 5428626.0968 130.95 456185.7411 5428628.7072 133.86 456183.1916 5428626.7065 130.95 456184.4315 5428626.0968 130.95</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001tiF_57e49e0c-ef98-4195-893e-5e7acfbcfc02_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>23.716</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>90.0</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>133.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>-1.0</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>57e49e0c-ef98-4195-893e-5e7acfbcfc02</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856104_27819">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_57e49e0c-ef98-4195-893e-5e7acfbcfc02_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892134_32127">
                      <gml:posList srsDimension="3">456189.72 5428627.65 133.86 456188.9406 5428628.0878 133.86 456187.47689999995 5428628.9013 133.86 456186.1957 5428629.6133 133.86 456185.7411 5428628.7072 133.86 456184.4315 5428626.0968 133.86 456189.55 5428623.58 133.86 456190.54 5428623.93 133.86 456191.5 5428625.82 133.86 456191.15 5428626.86 133.86 456189.72 5428627.65 133.86</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001tiF_60fbad93-8ce0-4665-86ac-79174c243c5c_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>1.346</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>38.873</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>136.27</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>134.87</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>184.011</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>60fbad93-8ce0-4665-86ac-79174c243c5c</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856104_27820">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_60fbad93-8ce0-4665-86ac-79174c243c5c_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892134_32128">
                      <gml:posList srsDimension="3">456186.1957 5428629.6133 134.87 456185.2508 5428630.8109 136.27 456184.7025 5428629.718 134.87 456186.1957 5428629.6133 134.87</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001tiF_9ca49b54-90bc-4835-a30b-631b139a0024_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>1.03</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>42.812</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>134.87</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>133.86</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>184.011</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>9ca49b54-90bc-4835-a30b-631b139a0024</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856104_27821">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_9ca49b54-90bc-4835-a30b-631b139a0024_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892134_32129">
                      <gml:posList srsDimension="3">456185.7411 5428628.7072 133.86 456186.1957 5428629.6133 134.87 456184.7025 5428629.718 134.87 456185.7411 5428628.7072 133.86</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001tiF_cc25c847-a158-4e8b-805e-331a0af09936_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>9.568</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>45.347</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>136.27</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>134.87</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>116.643</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>cc25c847-a158-4e8b-805e-331a0af09936</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856104_27822">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_cc25c847-a158-4e8b-805e-331a0af09936_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892134_32130">
                      <gml:posList srsDimension="3">456188.7233 5428634.572299999 134.87 456189.5586 5428639.3972 136.27 456185.2508 5428630.8109 136.27 456188.7233 5428634.572299999 134.87</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001tiF_1bb0d716-3cc1-4d0b-81e4-343017355abd_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>5.482</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>44.706</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>136.27</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>134.87</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>117.008</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>1bb0d716-3cc1-4d0b-81e4-343017355abd</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856104_27823">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_1bb0d716-3cc1-4d0b-81e4-343017355abd_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892134_32131">
                      <gml:posList srsDimension="3">456186.1957 5428629.6133 134.87 456188.7233 5428634.572299999 134.87 456185.2508 5428630.8109 136.27 456186.1957 5428629.6133 134.87</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001tiF_68be9167-781a-4036-b5ea-cabb4ed4aa66_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>5.961</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>47.755</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>134.87</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>134.15</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>117.008</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>68be9167-781a-4036-b5ea-cabb4ed4aa66</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856104_27824">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_68be9167-781a-4036-b5ea-cabb4ed4aa66_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892134_32132">
                      <gml:posList srsDimension="3">456186.8891 5428629.2279 134.15 456189.4167 5428634.186899999 134.15 456188.7233 5428634.572299999 134.87 456186.1957 5428629.6133 134.87 456186.8891 5428629.2279 134.15</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001tiF_45f4e776-4a38-4775-bca7-e9d0791dd490_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>14.227</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>66.577</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>134.15</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>133.134</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>116.988</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>45f4e776-4a38-4775-bca7-e9d0791dd490</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856104_27825">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_45f4e776-4a38-4775-bca7-e9d0791dd490_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892134_32133">
                      <gml:posList srsDimension="3">456188.9406 5428628.0878 133.1342 456191.4682 5428633.0468 133.1336 456189.4167 5428634.186899999 134.15 456186.8891 5428629.2279 134.15 456187.47689999995 5428628.9013 133.86 456188.9406 5428628.0878 133.1342</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001tiF_6d4ad7ab-76b8-45c3-acd3-b5c5cb48e6eb_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>39.284</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>46.317</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>136.27</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.95</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>26.834</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>6d4ad7ab-76b8-45c3-acd3-b5c5cb48e6eb</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856104_27826">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_6d4ad7ab-76b8-45c3-acd3-b5c5cb48e6eb_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892134_32134">
                      <gml:posList srsDimension="3">456190.2822 5428640.8395 136.27 456197.6963 5428643.3314 130.95 456189.99 5428647.23 130.95 456188.6451 5428646.8341 131.8671 456190.2822 5428640.8395 136.27</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001tiF_b36072af-1b79-4230-a553-5c9fdd1f6505_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.73</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>45.941</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>136.27</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>134.87</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>117.007</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>b36072af-1b79-4230-a553-5c9fdd1f6505</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856104_27827">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_b36072af-1b79-4230-a553-5c9fdd1f6505_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892135_32135">
                      <gml:posList srsDimension="3">456190.857 5428638.7587 134.87 456189.5586 5428639.3972 136.27 456188.7233 5428634.572299999 134.87 456190.857 5428638.7587 134.87</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001tiF_bbe28498-6cee-4c07-8536-c600fa53f534_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>11.044</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>41.881</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>134.87</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.37</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>117.007</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>bbe28498-6cee-4c07-8536-c600fa53f534</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856104_27828">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_bbe28498-6cee-4c07-8536-c600fa53f534_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892135_32136">
                      <gml:posList srsDimension="3">456191.4682 5428633.0468 131.37 456190.857 5428638.7587 134.87 456188.7233 5428634.572299999 134.87 456191.4682 5428633.0468 131.37</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001tiF_40662d1a-6fe5-48e7-b08b-e0d436e2a72b_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>3.884</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>43.389</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.37</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.35</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>117.008</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>40662d1a-6fe5-48e7-b08b-e0d436e2a72b</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856104_27829">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_40662d1a-6fe5-48e7-b08b-e0d436e2a72b_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892135_32137">
                      <gml:posList srsDimension="3">456191.4203 5428631.014 130.4386 456191.57999999996 5428631.33 130.4399 456191.6563 5428631.2925 130.35 456191.4682 5428633.0468 131.37 456188.9406 5428628.0878 131.37 456191.4203 5428631.014 130.4386</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001tiF_1314220f-1a11-40ea-969c-234e3bfd30b9_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.416</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>44.152</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.37</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.439</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>116.643</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>1314220f-1a11-40ea-969c-234e3bfd30b9</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856104_27830">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_1314220f-1a11-40ea-969c-234e3bfd30b9_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892135_32138">
                      <gml:posList srsDimension="3">456189.72 5428627.65 130.4502 456191.4203 5428631.014 130.4386 456188.9406 5428628.0878 131.37 456189.72 5428627.65 130.4502</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001tiF_c061807c-8b70-4576-95cb-aa3f9e289394_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>18.199</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>43.763</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>130.949</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>127.45</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>26.835</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>c061807c-8b70-4576-95cb-aa3f9e289394</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856104_27831">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_c061807c-8b70-4576-95cb-aa3f9e289394_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892135_32139">
                      <gml:posList srsDimension="3">456204.4 5428639.94 127.45 456197.6963 5428643.3314 127.45 456196.1936 5428640.336199999 130.9489 456204.4 5428639.94 127.45</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001tiF_67253e46-7efa-46d0-9653-e3a0ce994a22_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>38.401</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>43.479</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>134.87</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>127.45</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>26.582</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>67253e46-7efa-46d0-9653-e3a0ce994a22</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856105_27832">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_67253e46-7efa-46d0-9653-e3a0ce994a22_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892135_32140">
                      <gml:posList srsDimension="3">456201.2407 5428633.6529 134.8701 456204.4 5428639.94 127.45 456196.1936 5428640.336199999 130.9489 456201.2407 5428633.6529 134.8701</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001tiF_7b8e1bef-7038-403c-8c92-7dafbcae2f2e_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>316.466</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>7b8e1bef-7038-403c-8c92-7dafbcae2f2e</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856105_27833">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_7b8e1bef-7038-403c-8c92-7dafbcae2f2e_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892135_32141">
                      <gml:posList srsDimension="3">456178.69 5428628.92 114.86 456178.69 5428628.92 130.95 456187.51 5428646.5 130.95 456187.51 5428646.5 114.86 456178.69 5428628.92 114.86</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001tiF_1608b1e6-bd4f-4448-921e-014be14bc4c9_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.249</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>1608b1e6-bd4f-4448-921e-014be14bc4c9</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856105_27834">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_1608b1e6-bd4f-4448-921e-014be14bc4c9_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892135_32142">
                      <gml:posList srsDimension="3">456184.4315 5428626.0968 130.95 456184.4315 5428626.0968 133.86 456185.7411 5428628.7072 133.86 456184.4315 5428626.0968 130.95</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001tiF_77ee7057-a7fe-40fa-af4a-72f2530942b5_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.637</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>77ee7057-a7fe-40fa-af4a-72f2530942b5</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856105_27835">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_77ee7057-a7fe-40fa-af4a-72f2530942b5_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892135_32143">
                      <gml:posList srsDimension="3">456188.9406 5428628.0878 131.37 456188.9406 5428628.0878 133.1342 456188.9406 5428628.0878 133.86 456189.72 5428627.65 133.86 456189.72 5428627.65 130.4502 456188.9406 5428628.0878 131.37</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001tiF_a41e2a1e-16a9-48e4-bde4-5536dc3ee938_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>20.849</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>a41e2a1e-16a9-48e4-bde4-5536dc3ee938</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856105_27836">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_a41e2a1e-16a9-48e4-bde4-5536dc3ee938_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892135_32144">
                      <gml:posList srsDimension="3">456191.15 5428626.86 114.86 456191.15 5428626.86 133.86 456191.5 5428625.82 133.86 456191.5 5428625.82 114.86 456191.15 5428626.86 114.86</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001tiF_dd50906f-41d6-4e02-93fd-36da3122d525_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>40.277</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>dd50906f-41d6-4e02-93fd-36da3122d525</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856105_27837">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_dd50906f-41d6-4e02-93fd-36da3122d525_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892135_32145">
                      <gml:posList srsDimension="3">456191.5 5428625.82 114.86 456191.5 5428625.82 133.86 456190.54 5428623.93 133.86 456190.54 5428623.93 114.86 456191.5 5428625.82 114.86</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001tiF_6e8f2f42-bac2-42ce-a8af-6546ae510d29_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>19.951</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>6e8f2f42-bac2-42ce-a8af-6546ae510d29</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856105_27838">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_6e8f2f42-bac2-42ce-a8af-6546ae510d29_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892135_32146">
                      <gml:posList srsDimension="3">456190.54 5428623.93 114.86 456190.54 5428623.93 133.86 456189.55 5428623.58 133.86 456189.55 5428623.58 114.86 456190.54 5428623.93 114.86</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001tiF_cdd3543d-9003-48b6-8c34-c1a8eeb9a366_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.512</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>cdd3543d-9003-48b6-8c34-c1a8eeb9a366</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856105_27839">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_cdd3543d-9003-48b6-8c34-c1a8eeb9a366_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892135_32147">
                      <gml:posList srsDimension="3">456186.1957 5428629.6133 133.86 456186.1957 5428629.6133 134.87 456185.7411 5428628.7072 133.86 456186.1957 5428629.6133 133.86</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001tiF_b3b6a673-c6ef-4f97-a941-7de11e322d99_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>9.818</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>b3b6a673-c6ef-4f97-a941-7de11e322d99</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856105_27840">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_b3b6a673-c6ef-4f97-a941-7de11e322d99_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892135_32148">
                      <gml:posList srsDimension="3">456191.4682 5428633.0468 133.1336 456188.9406 5428628.0878 133.1342 456188.9406 5428628.0878 131.37 456191.4682 5428633.0468 131.37 456191.4682 5428633.0468 133.1336</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001tiF_0a1827af-ba6e-4732-901d-bf73420954fb_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>5.864</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>0a1827af-ba6e-4732-901d-bf73420954fb</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856105_27841">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_0a1827af-ba6e-4732-901d-bf73420954fb_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892135_32149">
                      <gml:posList srsDimension="3">456197.6963 5428643.3314 127.45 456197.6963 5428643.3314 130.95 456196.1936 5428640.336199999 130.9489 456197.6963 5428643.3314 127.45</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001tiF_f5d6cf46-860c-43ab-bcba-b2b72fb65f05_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.037</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>f5d6cf46-860c-43ab-bcba-b2b72fb65f05</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856105_27842">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_f5d6cf46-860c-43ab-bcba-b2b72fb65f05_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892135_32150">
                      <gml:posList srsDimension="3">456191.6563 5428631.2925 130.35 456191.4682 5428633.0468 131.4123 456191.4682 5428633.0468 131.37 456191.6563 5428631.2925 130.35</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001tiF_25c09492-8439-4b1e-a522-c0c4b2ad54d8_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.121</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>25c09492-8439-4b1e-a522-c0c4b2ad54d8</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856105_27843">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_25c09492-8439-4b1e-a522-c0c4b2ad54d8_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892135_32151">
                      <gml:posList srsDimension="3">456191.4682 5428633.0468 131.37 456191.4682 5428633.0468 131.4123 456190.857 5428638.7587 134.87 456191.4682 5428633.0468 131.37</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001tiF_3aabee52-e148-4120-8a6e-5c00297c02f9_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>331.707</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>3aabee52-e148-4120-8a6e-5c00297c02f9</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856105_27844">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_3aabee52-e148-4120-8a6e-5c00297c02f9_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892135_32152">
                      <gml:posList srsDimension="3">456198.39 5428627.98 114.86 456191.57999999996 5428631.33 114.86 456189.72 5428627.65 114.86 456191.15 5428626.86 114.86 456191.5 5428625.82 114.86 456190.54 5428623.93 114.86 456189.55 5428623.58 114.86 456178.69 5428628.92 114.86 456187.51 5428646.5 114.86 456189.99 5428647.23 114.86 456204.4 5428639.94 114.86 456198.39 5428627.98 114.86</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001tiF_af01e87a-856d-4f20-95ed-90f1844dca4b_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>22.81</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>45.985</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>136.27</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.949</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>116.68</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>af01e87a-856d-4f20-95ed-90f1844dca4b</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856105_27845">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_af01e87a-856d-4f20-95ed-90f1844dca4b_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892135_32153">
                      <gml:posList srsDimension="3">456190.857 5428638.7587 134.87 456196.1936 5428640.336199999 130.9489 456197.6963 5428643.3314 130.95 456190.2822 5428640.8395 136.27 456189.5586 5428639.3972 136.27 456190.857 5428638.7587 134.87</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001tiF_30d1bd3b-ab79-4971-8803-94675ad351db_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>211.317</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>30d1bd3b-ab79-4971-8803-94675ad351db</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856105_27846">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_30d1bd3b-ab79-4971-8803-94675ad351db_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892135_32154">
                      <gml:posList srsDimension="3">456178.69 5428628.92 114.86 456189.55 5428623.58 114.86 456189.55 5428623.58 133.86 456184.4315 5428626.0968 133.86 456184.4315 5428626.0968 130.95 456183.1916 5428626.7065 130.95 456178.69 5428628.92 130.95 456178.69 5428628.92 114.86</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001tiF_e00bb528-ed5f-4367-baed-be775b17bff1_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>227.384</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e00bb528-ed5f-4367-baed-be775b17bff1</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856105_27847">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_e00bb528-ed5f-4367-baed-be775b17bff1_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892135_32155">
                      <gml:posList srsDimension="3">456198.39 5428627.98 114.86 456204.4 5428639.94 114.86 456204.4 5428639.94 127.45 456201.2407 5428633.6529 134.8701 456198.39 5428627.98 130.3499 456198.39 5428627.98 114.86</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001tiF_d9080912-2dc0-4129-a5cd-2760006814e2_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>64.258</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>d9080912-2dc0-4129-a5cd-2760006814e2</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856105_27848">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_d9080912-2dc0-4129-a5cd-2760006814e2_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892135_32156">
                      <gml:posList srsDimension="3">456189.72 5428627.65 114.86 456191.57999999996 5428631.33 114.86 456191.57999999996 5428631.33 130.4399 456191.4203 5428631.014 130.4386 456189.72 5428627.65 130.4502 456189.72 5428627.65 114.86</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001tiF_7253127a-1019-48f9-98c5-b99abbac56b8_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>117.563</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>7253127a-1019-48f9-98c5-b99abbac56b8</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856105_27849">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_7253127a-1019-48f9-98c5-b99abbac56b8_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892135_32157">
                      <gml:posList srsDimension="3">456191.57999999996 5428631.33 114.86 456198.39 5428627.98 114.86 456198.39 5428627.98 130.3499 456191.6563 5428631.2925 130.35 456191.57999999996 5428631.33 130.4399 456191.57999999996 5428631.33 114.86</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001tiF_30724633-425c-4af9-a7ea-0cb83f33a978_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>233.544</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>30724633-425c-4af9-a7ea-0cb83f33a978</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856106_27850">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_30724633-425c-4af9-a7ea-0cb83f33a978_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892135_32158">
                      <gml:posList srsDimension="3">456204.4 5428639.94 114.86 456189.99 5428647.23 114.86 456189.99 5428647.23 130.95 456197.6963 5428643.3314 130.95 456197.6963 5428643.3314 127.45 456204.4 5428639.94 127.45 456204.4 5428639.94 114.86</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001tiF_6eafef3d-42e1-4a0b-be86-a29fee2573db_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.613</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>6eafef3d-42e1-4a0b-be86-a29fee2573db</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856106_27851">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_6eafef3d-42e1-4a0b-be86-a29fee2573db_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892135_32159">
                      <gml:posList srsDimension="3">456186.1957 5428629.6133 133.86 456187.47689999995 5428628.9013 133.86 456186.8891 5428629.2279 134.15 456186.1957 5428629.6133 134.87 456186.1957 5428629.6133 133.86</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001tiF_37316563-689c-494e-822d-0ea58c9f596b_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.327</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>37316563-689c-494e-822d-0ea58c9f596b</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856106_27852">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_37316563-689c-494e-822d-0ea58c9f596b_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892135_32160">
                      <gml:posList srsDimension="3">456191.4682 5428633.0468 133.1336 456191.4682 5428633.0468 131.37 456188.7233 5428634.572299999 134.87 456189.4167 5428634.186899999 134.15 456191.4682 5428633.0468 133.1336</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001tiF_95a960a0-ac70-4347-9d6a-d5f336801d79_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>31.04</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>95a960a0-ac70-4347-9d6a-d5f336801d79</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856106_27853">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_95a960a0-ac70-4347-9d6a-d5f336801d79_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892135_32161">
                      <gml:posList srsDimension="3">456191.15 5428626.86 114.86 456189.72 5428627.65 114.86 456189.72 5428627.65 130.4502 456189.72 5428627.65 133.86 456191.15 5428626.86 133.86 456191.15 5428626.86 114.86</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001tiF_f579056b-5d19-4672-80a4-11f46dcf84bc_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.608</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>f579056b-5d19-4672-80a4-11f46dcf84bc</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856106_27854">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_f579056b-5d19-4672-80a4-11f46dcf84bc_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892135_32162">
                      <gml:posList srsDimension="3">456188.9406 5428628.0878 133.1342 456187.47689999995 5428628.9013 133.86 456188.9406 5428628.0878 133.86 456188.9406 5428628.0878 133.1342</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001tiF_9361fbf4-84cf-4be9-9930-754bac54503f_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.038</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>9361fbf4-84cf-4be9-9930-754bac54503f</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856106_27855">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_9361fbf4-84cf-4be9-9930-754bac54503f_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892135_32163">
                      <gml:posList srsDimension="3">456185.2508 5428630.8109 136.27 456183.1916 5428626.7065 130.95 456184.7025 5428629.718 134.87 456185.2508 5428630.8109 136.27</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001tiF_70fd4d6d-5431-4e34-8a5c-72a683f3e2c2_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>42.781</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>70fd4d6d-5431-4e34-8a5c-72a683f3e2c2</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856106_27856">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_70fd4d6d-5431-4e34-8a5c-72a683f3e2c2_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892135_32164">
                      <gml:posList srsDimension="3">456189.99 5428647.23 114.86 456187.51 5428646.5 114.86 456187.51 5428646.5 130.95 456188.6451 5428646.8341 131.8671 456189.99 5428647.23 130.95 456189.99 5428647.23 114.86</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001tiF_1d8a9155-64ec-4fc6-9022-f7425a14ee2f_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>74.326</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>54.547</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>134.87</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.35</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>206.186</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>1d8a9155-64ec-4fc6-9022-f7425a14ee2f</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856106_27857">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_1d8a9155-64ec-4fc6-9022-f7425a14ee2f_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892135_32165">
                      <gml:posList srsDimension="3">456190.857 5428638.7587 134.87 456191.4682 5428633.0468 131.4123 456191.6563 5428631.2925 130.35 456198.39 5428627.98 130.3499 456201.2407 5428633.6529 134.8701 456190.857 5428638.7587 134.87</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001tiF_88bb7f8a-f861-4889-b185-cd375bc4ef02_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>31.472</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>43.877</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>134.87</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.949</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>26.183</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>88bb7f8a-f861-4889-b185-cd375bc4ef02</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940856106_27858">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001tiF_88bb7f8a-f861-4889-b185-cd375bc4ef02_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572940892135_32166">
                      <gml:posList srsDimension="3">456190.857 5428638.7587 134.87 456201.2407 5428633.6529 134.8701 456196.1936 5428640.336199999 130.9489 456190.857 5428638.7587 134.87</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:address xlink:href="#UUID_765628ae-2006-44d3-b4a4-039a330ec59d"/>
    </bldg:Building>
  </core:cityObjectMember>
</core:CityModel>