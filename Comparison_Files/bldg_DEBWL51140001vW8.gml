<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<core:CityModel xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:tex="http://www.opengis.net/citygml/texturedsurface/2.0" xmlns:core="http://www.opengis.net/citygml/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:frn="http://www.opengis.net/citygml/cityfurniture/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd http://www.opengis.net/citygml/appearance/2.0 http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/cityobjectgroup/2.0 http://schemas.opengis.net/citygml/cityobjectgroup/2.0/cityObjectGroup.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/cityfurniture/2.0 http://schemas.opengis.net/citygml/cityfurniture/2.0/cityFurniture.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd">
  <core:cityObjectMember>
    <bldg:Building gml:id="bldg_DEBWL51140001vW8">
      <gml:name/>
      <gml:boundedBy>
        <gml:Envelope srsName="urn:ogc:def:crs:EPSG::25832" srsDimension="3">
          <gml:lowerCorner>456568.92 5428780.01 112.44</gml:lowerCorner>
          <gml:upperCorner>456651.12 5428857.83 133.69</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <core:creationDate>2019-06-28</core:creationDate>
      <core:externalReference>
        <core:informationSystem>http://www.karlsruhe.de/b3/bauen/geodaten/3dgis</core:informationSystem>
        <core:externalObject>
          <core:uri>DEBWL51140001vW8</core:uri>
        </core:externalObject>
      </core:externalReference>
      <gen:doubleAttribute name="HoeheGrund">
        <gen:value>112.443</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="HoeheDach">
        <gen:value>133.69</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="NiedrigsteTraufeDesGebaeudes">
        <gen:value>112.44</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Dachtypen">
        <gen:value>PULTDACH;FLACHDACH;PULTDACH;PULTDACH;WALMDACH;SATTELDACH</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Dachkodierungsliste">
        <gen:value>2100;1000;2100;2100;3200;3100</gen:value>
      </gen:stringAttribute>
      <gen:doubleAttribute name="Volumen">
        <gen:value>54407.298</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Verfahren">
        <gen:value>BREC_2000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Objektart">
        <gen:value>Hauptgebaeude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Strassenschluessel">
        <gen:value>00020</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Strassenname">
        <gen:value>Adlerstraße</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Hausnummer">
        <gen:value>1</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="creationUser">
        <gen:value>Firma VCS</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Feinheit">
        <gen:value>Basis</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="AmtlicherGemeindeschluessel">
        <gen:value>08212000</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="DatenquelleDachhoehe">
        <gen:value>1000</gen:value>
      </gen:intAttribute>
      <gen:intAttribute name="DatenquelleLage">
        <gen:value>1000</gen:value>
      </gen:intAttribute>
      <gen:intAttribute name="DatenquelleBodenhoehe">
        <gen:value>1100</gen:value>
      </gen:intAttribute>
      <bldg:function>31001_3010</bldg:function>
      <bldg:roofType>1000</bldg:roofType>
      <bldg:measuredHeight uom="urn:adv:uom:m">21.247</bldg:measuredHeight>
      <bldg:lod2Solid>
        <gml:Solid>
          <gml:exterior>
            <gml:CompositeSurface>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_425bcd6f-6583-4c7c-bae2-dfbb2e8673d6_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_753b12e9-4378-4a90-ab2a-f9c426c55b07_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_fc89377f-607c-4fb5-90f2-ff931fc3f036_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_dc09c106-ce84-4889-a8ef-31431f9beb32_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_e06b4134-9257-4e4c-b04f-84d69bcf1323_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_9fb9c0ab-7e4d-4c1e-9490-8d42b450c0cb_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_44640427-9a1c-4e16-9205-f16825a41e90_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_541e7de6-6553-42f5-9d4f-48cf50f23c6c_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_98a13dc4-1e5c-4b29-8f28-34cef9f5e450_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_8fdeb363-2d76-4e40-b552-fc26a02b9365_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_55df0c8e-8a05-4d4e-8fb2-25a4f68103a5_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_0ea315d9-526a-42e6-819d-265701b84083_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_dc3f7cfd-6d68-4ed0-ac95-df586d9b7066_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_732d8ffc-7a0e-455a-9dae-909fdf79dad7_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_6727a964-585e-4549-9deb-f9f3bd597db5_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_81e12c30-bfb2-41a2-adc7-0be9a7578a7b_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_ce7efb2d-36db-44b6-a0b7-df3a6418aaac_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_aad9fa21-a17c-4aaa-8baa-b60fda9f8003_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_d0f8da80-074e-4819-8a42-482a4060ba31_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_a0c50fd5-73e9-46cf-b625-6bbbc10378a3_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_a53daa25-8bb8-41c8-8cf4-805abccfe50f_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_745ae98f-1288-4049-b619-e8b8ddeb9921_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_460d0923-f762-49f5-b52c-c3ca7e796410_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_05a45ed8-a361-4746-8196-02a05024394f_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_9432a605-8bbd-4699-b961-8bd9605e15b8_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_58c77241-5e75-4d6f-8e83-3a6e56c6c9c2_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_0dd8bb5e-3f88-4653-826e-3c7c75d32c29_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_cb458a9b-754b-4303-9b2a-72b81fcbedac_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_32cffdb9-20b6-4fde-a5ce-3a4f5f9153c4_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_7534da49-612a-46f0-844f-defc889e5fb3_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_de373966-ee5e-4368-8fd3-ebd27337ad38_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_beae90d3-a764-429b-9240-f6df5343c316_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_052e543e-6e80-44b4-a447-76670abf9edd_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_622e1ddb-2085-4c37-a0ea-0d9651426319_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_8091cd59-bc43-4265-8423-38d6df9a0cb5_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_53cbd62c-f7f4-4f89-a3c2-adfbe1b0edac_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_9f1354dc-bd51-4b80-a7f6-06e2c67ce6db_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_4f8c43ca-2acb-42ab-81cc-0c73a3de21d0_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_18e220d1-75e0-47e2-95e9-31114f5b86e3_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_60c50302-be5f-48a0-adba-9b90f415ceda_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_35369121-773b-4fa3-85dc-f24112a4a324_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_1866e2ac-367b-4a71-b3b4-0673fd53404c_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_b594b7ce-29de-43f2-826d-e78b4f2ea787_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_4d5a5dc9-1a51-45a4-ba02-e339399008fc_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_dc4ec527-1c68-4b34-a96d-39ee650f6c7e_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_26839f82-449c-4bb4-bb79-5983b5b8c486_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_7e743c21-62dd-4c73-84ae-78f1832f0346_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_6d0bedb9-0bdb-4b57-97bb-b85231ceca2c_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_3afe0639-0865-44e7-929b-cec8dcf177a7_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_a2a707e9-8504-41e7-8dc4-38dfca2457ef_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_8b709cc0-3fbc-48b5-acfb-6a440f51e831_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_a52bb882-3c0d-4dd0-9820-10a514689fc0_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_0545cb65-09fc-4e24-892f-1194bee8e2d7_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_2c55036a-0f24-457e-b512-03fc3438f171_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_dc9a562c-239e-4f32-9995-ab65940c729b_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_f775990d-d8f0-4aaf-8b55-ab3dae07a119_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_06cd3aad-da73-4489-89a7-3c7c82ac0f68_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_94f9c636-8ea8-4b08-bf2d-968defa7352b_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_59c234f7-5390-4261-89d5-5c6502a9b15b_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_8a9a3a54-8e5f-4104-aa40-1235d958c20d_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_ab1c2f02-6dda-47b5-b024-dd4348fe9c28_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_d224ae47-8b5c-47eb-ac65-772042802f8d_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_ba89bae7-3fef-4d26-a825-7fb310ee810f_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_8af374b6-8297-4f87-83c1-530b99d79ae8_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_8e25cb9c-d516-42d5-9dcc-89bc6ecd832f_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_ef849a8f-b05a-415b-bf52-05d61b75906d_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_3d8dc3f6-d833-4301-bd67-9627b172ef85_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_5ae032f4-395e-4016-865e-a7a9de62a7df_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_a11d26b1-1413-45f7-963a-13c301698dc6_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_e5fc3898-56ca-4b29-a06e-ee6c71e30481_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_0f05d1dc-8b58-4507-b97c-f68905a78f32_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_0b309a76-acfd-4ef0-9b21-cb9fd695a100_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_09bf8276-996a-4e2e-930c-ea32701ce502_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_ba196c6d-1aff-4722-a25a-7417d97f8a3d_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_57344bb3-ffe4-431d-bb59-2a52b441e438_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_d1d1b54d-a519-4e20-a2ee-2cefa2107058_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_50dd73f5-a1dc-4c4f-813c-125f8f404e60_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_8b9de822-56c2-4785-b833-9ba5ebee2ed8_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_77a37421-7b99-4223-a670-b6ed40b6faa2_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_e5e5d20a-96e2-425d-80c6-a4ab3ea0d4e2_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_79a505b9-1f78-4294-ab13-e124aacaae14_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_4894b8a4-9172-4734-ae46-99325c079038_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_85096926-04af-444c-83e4-bf75e844003b_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_94f22c2b-294b-41e7-be9d-d11e5817dbff_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_f7032497-10ee-4580-86ab-a2d9268b418b_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_276fb58d-ebac-4eca-94ef-83b9b6f6bffe_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_75e0303d-09e6-43b2-ac7f-1dfc0cd21998_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_fb7f2928-11c8-45aa-a96c-7d7e1df3ad58_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_0c737a21-cd43-4af0-9ee8-9ebbb5f6321a_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_b62d9d07-11c0-411e-afa2-33fdb643b28e_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_6e8f9304-c711-4020-bfa3-8ac5159a6335_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_0cb726e7-40ae-458f-979c-465d9e51823f_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_723a1f3e-dd88-4e0c-87fa-550db361e7bb_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_bf833494-6f49-4083-9324-e26f766b5db7_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_b55d2150-56e7-4550-a31d-5fdd637c62ee_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_620c996c-e831-46d5-8e1b-4992747ae270_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_e11d88ea-3444-46e0-bf24-c34625ca9be7_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_2ea12ae5-54e8-409e-89cd-75faf9a08595_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_c6317f6d-9ca5-45ee-9edf-c133e99bde11_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_00d9379c-960c-4607-a39c-decb5e1cfbc2_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_d495570e-3c26-45d3-a1d5-bdb2459c487f_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_17aabb24-6033-4cba-a49d-db8eb8acdabd_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_abd79283-7228-4944-ad9b-a3895cfb43dd_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_2e97c556-04a3-48ed-a110-6535279f896e_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_543426d2-a23f-403e-9dbd-cdbbf3401948_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_9a96e5ff-ddf0-4ea3-8f08-25fa1e519c9d_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_b7ac7f3e-8a0b-4315-8c01-9a11e1ebcd67_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_05b696fb-881c-4904-915b-ca0163d4c949_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_b86c06af-c27f-4f5d-9f36-32e916ba36b4_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_94c9a397-fb32-48ad-ad87-efcdb46b9a16_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_3b52d4d0-c23a-4c92-b77b-95d6b6fb878d_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_fed7e289-94b7-4dd6-b7d1-aa87c32b7fb5_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_e406c7bc-119d-4c1d-ad11-52190d87bba4_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_185fa9f1-1de9-4d88-a4ad-8d71ea12f7e1_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_1b98351b-6a45-4943-81f3-3165aa850c86_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_91ca428c-0a3d-42d7-912d-a96b1ffd2ad8_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_21943d11-ec64-45d4-a5bf-56d6884064fa_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_f0a3d9ba-bc77-4e58-85fb-234ca13f9edb_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_7a59a332-bf13-4afe-a3a3-4a28d24351e1_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_9f6ea118-5dff-46cc-b0c7-a3ec72445fce_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_56674c2a-60ff-43d3-9947-dabf17971b52_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_30a56c9e-41dd-41cf-a3d7-2a4761990b50_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_47041e8c-6185-4870-b24e-2afa101f77d4_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_e45b6847-df8c-4f83-8dfc-c110fa20ea92_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_1d6764c6-426d-4c46-bce5-b7d4976f68e3_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_fd9990c3-c0b4-4830-8d05-d3f0a299e94a_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_c44d9cba-e213-4409-8ce5-22232bc73ca0_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_636e754c-b898-47f3-9991-49200199cdbe_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_7cb2e356-a6f8-43e3-8d69-4291c14e84ca_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_95c4aac4-3a7a-4d83-8dd4-ff9f6a59dabc_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_038731e8-df4f-4e27-8136-fbcf230cf6a9_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_77f2d858-2937-4847-bde8-894f685c4aac_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_e899c3a8-5bfe-4669-af2d-68edec872143_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_512d91bb-b49a-433b-bd71-a6b2a2370a91_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_a0157e80-46b7-46af-87cf-71c82bd56222_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_959920d8-930f-4e20-a3bd-24e5d92d4b1a_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_c9deb3ee-ed11-44cf-a6df-73f6ee48d61d_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_ab788e64-e0f4-4ccd-ac6f-c8c1dafe08b5_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_3087dc75-c36b-40ae-a01f-a2bcb65e19ac_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_1bae7a54-f1b4-48d7-8e37-b266ad92f851_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_a2f547e0-2e0c-445d-9087-217c2106fd49_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_031f68ed-28a9-4034-89f5-c03fa3e86346_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_896eda34-680a-4f6f-8088-a898e63e82d0_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_bde9ac7d-c072-475e-943a-d56e3648d61d_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_de24d6db-df80-4679-86bb-e6cb117caa64_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_b950faf6-d8cd-4c59-92be-c404f74b3819_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_71a11872-302a-425e-8bb1-17f68ff5e37b_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_5ac2864a-8c08-4f93-a627-9828d6a0c3f3_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_df6c088a-d65c-49aa-8e09-a2a3aae83002_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_1f84bc6e-dce1-4841-b3e0-5bb5f8b82454_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_1df80a5f-b7f1-4c4c-b9be-5da25269bb30_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_5af367b3-22bd-4e54-8b53-38d05c896ca3_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_1a030b9e-5141-44dd-9c5a-fdc8aec59393_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_dd6e08cb-b53d-46bd-9867-7b95667462ff_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_8ddeed19-b1dc-4adf-93a4-7dc8f71caf54_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_a081b24c-ca3a-46b4-afba-26523276babd_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_f8a12c93-5b95-4c97-8a94-9dbdc0fae1f1_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_79857e8a-26c9-4bbb-b2fc-432701c9cf67_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_18060a0e-ec7a-4303-b4b2-d05d0e2e747c_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_78e02257-416f-453b-87e6-aeab19157ede_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_b6749f02-227f-4bd0-b4cd-0a7b70008825_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_5e2f51a5-2c9d-4bca-b314-9204b2648582_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_dba50201-4f12-49bd-a542-c7f7035ddd3f_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_298ea433-d110-4c13-b05f-aa8846361fc1_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_343142a4-7ce9-471e-8737-03c5a76bc275_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_a60181c4-21ef-42b0-94a3-d786cb8bf75e_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_a996a81d-caed-4f81-8254-c1d114c7fb2f_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_f72cf0c5-9be4-4922-82b5-0051f7746e22_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_dbab981e-6410-4260-a9f4-e8cfc260cbff_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_e910b927-b837-442d-b25f-1b6cff3fb998_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_0247c8fa-7aa7-4bcd-a95d-e9e72cf486b6_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_e6575387-ece9-4800-90dc-d2ae8faaa077_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_6751622d-ebb0-4562-9c34-a0b67bda5ad6_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_6c565868-00fc-4b1c-bb8d-cad42220d9d0_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_1143c020-88cd-4295-b019-7bc8921a0df3_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_67385e04-dd77-4218-8a5d-27c940286423_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_5fb447b8-a1ae-4ddf-b58c-78b48a2e8dcf_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_c95e0b61-adac-4816-8327-eeea8831ccba_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_3ac651b0-e593-4440-91e4-da0c8b2a2fb9_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_16afb51e-5b89-463c-9e86-e6baf6e9e23d_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_ca58e949-1043-455c-a7f0-414d3ff94420_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_cc6cb384-a2e6-45dd-9a64-d788c3efb594_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_59024e7b-0ecb-411c-bc14-3579983f3261_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_203a6bb6-5807-4182-8c92-e777b914cd44_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_9ed90f66-de02-49e8-901b-57d0e9f94737_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_c0f3ec90-8b53-4b0b-a02e-a324ac8283f6_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_b4a3770d-7feb-4a42-a874-bdb1525c7a04_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_3b6b34ff-cba4-4521-a336-cba22a4cbe69_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_53cf4b31-519b-4bbe-a278-358babd532c4_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_0ef08a7c-eea4-4ad9-8a61-0bc0307d49d0_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_ef1a3656-bc02-406b-bd11-c0b3a6758c05_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_0ea49728-88e2-40f5-988f-c4c06b14740e_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_4ca180b1-52bd-4f0c-bdf9-124a4012fb95_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_728d6cc1-088a-4d2f-a608-6db88f10bdcc_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_8e7060a7-a891-4180-bb83-db3e0b7ff04b_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_46ad65d8-e5a1-4e07-b0b9-340fe37f2490_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_309dc7bf-ddd9-4912-8005-df9c6e8e4c3c_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_9ab3ff09-99ff-4985-8163-9c4e2eeeeb91_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_5239dec5-948f-4fc2-8683-b1c8d0041bf1_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_409e34da-e4b0-46e2-b67c-98c72aea7aab_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_868548bc-0fcf-41cf-8432-abbe1d4652bc_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_b667b818-cf83-41bb-858e-7912d8286ded_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_29730b94-cabe-4843-baee-853db1a1de5b_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_cdddac81-dc42-4714-9c58-85b2add1ab35_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_66bab9aa-b3c6-425e-88d0-47555023333e_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_6cedc2f4-2942-4616-90aa-57b08763a26e_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_90572a47-1003-419d-bb37-dc92ef413f3c_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_09c5c1b3-83c8-42d0-8923-051150f6bdb7_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_ee9e63b9-a799-4529-943f-92bef2233939_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_94a7216a-b8f5-48d3-aa58-2ca309b45f9c_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_19966186-46e0-4afb-9e22-30ef27bfb130_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_54754a1f-ee99-4402-afc3-6f21e3a5a667_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_05a051c8-194f-4a49-989d-0262055aa8cc_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_037b4e83-df88-40d2-90a8-65c3dea14e2a_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_8cb8ec12-bbd2-458c-bad4-301de74c676e_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_f2bf61da-036f-4961-ba29-ed55329b7039_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_a122a64c-6789-412a-98f6-d884604f1dd0_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_58199ec4-9f66-4875-a305-b13857417c1c_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_3ed1cea7-f95e-4e77-9d55-12c794469801_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_9f122a76-e78c-4e98-8bb4-8736406c811f_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_93c31d16-7324-4767-8577-83ca28515c97_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_efc49de3-46d5-4ce5-9bbf-f1fe4a797085_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_65b01ffc-c112-4769-b056-5d2c49dbc300_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_5a45c296-0190-41ac-9b15-b7b6bc2872d0_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_743fc0da-1a00-438c-8fde-287d8c6c10f1_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_389ba21e-73dd-4d67-a8d0-1284f29f2d23_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_33ef9f57-1628-44fd-a797-3834ec9183ba_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_e118ca77-9818-43a6-9442-9cd43b4c783d_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_3331561b-d589-4e6b-a0d7-a278d41a644c_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_5698f1f9-ea4d-4818-8912-22e28fce4dde_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_cc174c83-09c7-47d0-9610-ea8f0128da99_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_6768722b-a033-4984-8ec9-8aa0934a1726_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_f0090abb-6728-41e9-bada-39ac5ff36505_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_b83fdbfc-4b24-4a86-9349-bd43cbd83890_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_f533ad0b-a209-4563-a3e1-96af18d7c8c7_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_68afa420-50d0-463c-8140-1c2da58dc6e6_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_d385b3b6-b289-4050-9da7-3157cf351500_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_02778ef7-8c21-41d8-a467-7ae48bf37bff_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_193aec86-a730-431e-b1f5-46bae677fab2_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_e898013b-0483-4819-b729-2635defacfca_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_defba096-4856-4756-b8bf-348d25f5f5ef_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_b44dea41-b30f-4db7-b686-04179e27dfbb_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_d6209d8a-e9d9-41a9-a6b9-d551054a997d_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_a901b59c-b039-497b-8d0a-066c7bc962d1_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_a9092e9e-b648-4fe0-a95b-90ec32b7cf67_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_26e14769-1fac-4622-b93f-7f705f670fcc_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_343c71d7-1f35-4c9e-8a26-97878179456a_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_a146f547-1728-4a45-9d68-69d4b27c1eb3_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_236c927b-9b94-49c0-8016-21912cb678b0_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_3651a99a-5de1-4cd2-8a23-0b6199545952_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_a341da3e-3598-42a2-a16f-c57e74745ef8_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_0fef8ce4-7669-49a6-ae1e-1e656faa5c98_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_c32c66d6-2807-45f4-a4d2-d67528994761_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_4dedb1ed-56bb-4a53-a7dd-bdddffeeb39c_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_0745513b-2cb4-4d26-a20e-8ddfbb366e2c_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_2e992f79-e3ad-432e-9fd5-47cf712ffc1e_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_e0527bee-27b3-473f-b200-289ac97c7eb6_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_7e0df6ea-b6bc-4841-aabb-056f9216aaf9_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_85a43622-f8d0-4fd5-be27-e92c731c8fd9_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_1c79002a-2662-44d9-85df-f1086856b1ee_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_04015ac1-0ea3-4bc0-b975-2f043a7323e0_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_37589b65-5d36-4857-ac98-2eccc305fd55_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_1ef82076-2bf8-4175-a830-46ed23db1eb9_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_7a0bc6f9-f0fb-4f12-9e72-8b7be9a39c8f_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_de190afa-dd55-48be-8dae-2ab1877a851e_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_20c54bc4-8310-4d4a-b48b-afe783d7414a_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_d91055e9-7799-4830-8444-492ad36d4100_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_824bb9bc-6433-4cae-a224-fcd870e4cb43_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_af6459cd-a97d-4fc2-a036-07e6d162f836_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_5b8cff76-b6cd-4945-8492-043582195706_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_670badc8-253a-4a4d-9a23-4acf4c008ca4_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_4bbc9c82-477c-464d-86d2-07dfd4d960b1_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_dc06072d-a854-44a5-903d-8dd39efb1a99_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_e57a3726-ffac-4f8b-b0aa-a5e73651f9d3_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_c5c4ff19-58de-42f0-92e6-80fe05d70637_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_2a9d7e94-4a57-471d-8cf7-01032b2f68c9_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_ecedbb4e-8971-4150-b4b3-faa906e40c75_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_e497bd3a-d561-46c9-93f6-40f6785b9b47_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_1c2cc50c-3250-4adc-be29-61fec1ab9e0b_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_18338a57-8737-4902-a558-2e1991d94fe2_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_b78bae04-c0e5-45c4-b23d-553e953f848e_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_c6bfc394-6276-413b-9a46-d4b2f0d8d577_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_971b9919-2cf6-4b91-9976-24fbd4543871_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_52d19005-5883-49b4-a667-927c2a38342a_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_17d66976-9aeb-434c-83e7-5f0a863f34a2_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_88d6bb0b-395f-428c-95c5-3df4f990136d_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_99b77784-0b45-44ee-bef1-32c3f8509cd4_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_6719e9e3-35fc-47d4-99da-3ef5b2337647_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_efa5b7cc-cb3b-46bb-895a-6dafccfd963a_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_e97762c6-c6e9-4535-8948-04fcb48c2709_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_bb75a7a4-6613-4199-9c16-1c867abed685_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_880508d9-1c29-4271-a11a-c74c76609b94_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_aae6fc47-08cd-48c5-8248-2e6c307773a4_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_dc4728b8-41b3-4f62-aceb-2ac756632b65_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_1fd1576c-03fa-4c7d-a37a-71142c6a435c_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_9b6784ad-91ed-40c9-ae1f-dd88f5446ae4_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_68532b54-d04c-4a13-b234-270390cfccad_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_823c1d5c-266d-40e1-884b-c453816c414c_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_e4aeb48a-0519-4a97-9cb9-2d80ced93f02_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_c7c8d32e-32fe-4252-9bdb-110b90d1a8b0_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_0a4dca11-8de0-445c-bdd5-814d7d441a52_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_5d164179-5514-4dad-9022-cc9b1a8d965b_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_551345b0-5fd5-49a9-be37-7968db46fe35_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_bf386a2b-58a1-471c-a8c1-8cb32eadbd0e_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_77715a4c-8f0c-4f9d-8858-953bf8df5e71_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_0e97e52d-075d-4268-a49c-5b8d09dd0ed8_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_57e6418d-85af-4fe9-8724-27c0a111ba90_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_ffabcd84-cc24-4437-8a9d-2e35d92a4bcd_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_ac86f380-e556-4e54-9cb0-7bd0dec8ee66_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_d30d2a65-bf06-432f-8b67-2b6a8162ef1e_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_0ffed9ae-31f1-487c-8a48-4a002e7e23e3_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_f1f886e6-4ec0-4ec3-8172-1e7389065e3c_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_3e635c93-daac-4e24-b903-b404b7509752_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_2eeb9032-97ff-4250-8e30-d4cdd89ec2d4_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_114bb944-5d51-46ce-a7d8-61f82c3ab175_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_01886ded-be08-491b-b1bd-0ee8f0385b17_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_9ef820a7-ad16-4309-803b-e42db1b5b31c_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_8f21b4d9-0e90-458e-8473-1ce91334cb98_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_d4da4a8e-8f39-4b61-beee-951107512676_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_833b7052-d974-4eee-b7a7-4449885a5239_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_15f782e6-c93c-41ed-8007-2f152c16052d_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_af95d389-de32-4c0f-b3c6-57ff032ee195_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_bf95e0ca-49a0-464e-ac6c-ec9d0f472c4f_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_42aae18f-4c1b-4c5b-bcdd-a81c404afeeb_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_44fdde52-068e-41ec-821d-406fe79f0523_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_5f81707d-fd53-4a36-baa4-e28df54ef880_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_3640c569-eaeb-4832-bc46-3157dbef0324_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_315eae8a-9965-46bb-9251-43a676b0d0d7_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_188317f7-f416-4415-b379-7cdc1d6608ad_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_23aba20d-6513-4ff7-9f63-1a59503ef94b_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_0c10f684-7d9e-4782-84d5-41830fe42dc0_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_be8ef207-296d-4d63-bb09-bc97b4a06485_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_00705c43-ee3a-47fa-aa66-2e4e6e805021_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_ea5c84ba-096b-42bf-8b45-a62c8694a78a_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_9431f38d-0094-45a5-a796-aab765bc99ad_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_e528ecbc-96de-4e2d-af75-292676895512_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_c45137bc-4fdc-4b54-a6ad-222e2a616a24_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_74c5af72-a7c9-4799-b6e6-caa58dd45fb7_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_36975099-517c-4ed5-bc49-550f2f3863ee_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_259838d1-cab8-47b1-9fd8-ac5802492521_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_23f5a73b-0991-47c4-a504-1838aafa4fdd_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_375a46d8-c017-446a-912c-b01178db0bbc_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_ee4d1024-778d-4a37-bf05-4da4ac760c33_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_e96cea58-82de-4854-bf56-8e510f1c0655_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_0809607c-6bd9-4f25-99c9-3dd1ae17628a_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_06a73fc7-2962-4e9c-8ade-110dfa5089b4_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_a088644d-de97-4b4f-b30e-d1b0ca8aa07e_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_396668e2-172e-4d04-ae06-ae8dbfd30493_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_f6a495a1-00e6-4d04-bb28-6fba81edd1bb_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_e8d3ea2c-bff8-4739-8499-44025cdfbbce_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_587af7c0-3976-4576-8b4c-a8952d0a8b7d_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_73139682-4289-48c3-a3f5-b6fe1c1e647f_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_a9457e5f-9f26-4cfe-a4a2-067ca0ad5f80_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_2a239756-4d3b-405a-8669-b451dfb7f3d0_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_e1529871-2342-4313-b90d-8ac1ed27654d_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_a71d85f4-c54e-4a96-953a-d8ada1d128a7_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_5b943fb6-a7db-4d62-8567-4119cc9c6217_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_610047d9-2899-4dda-9d53-d569884d3785_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_ec795e87-e3af-4931-ac3a-bb0e9a4fd0c4_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_7b85cb82-be19-4bfe-8e33-85dc74ed7771_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_ffd538f2-ac96-4715-9f2a-ba11b04ecbec_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_d60cc642-0f91-41b3-af4a-a1e8857f30cb_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_7cccf90f-1635-4ba2-ae21-dcd6654d6dbd_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_5ec93470-af0f-4ab2-84e9-014c6bf28421_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_d837eb36-1fbb-4324-b41c-5f9e7531ceab_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_9c8afc44-706e-4c08-b30e-cf532df63b33_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_382c25b8-22c7-45bf-8ac0-ce57da6b7443_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_0d5afc02-eda1-4d40-a8c0-e9076476c446_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_eb1c91d1-0b12-44a7-8586-29258c29288e_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_fd8b7f80-ce0f-4236-9f65-8f625a343312_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_92181542-c83d-49a9-adf6-0491c3428735_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_a6f09829-0d22-4c7b-9949-e2cdc50304f6_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_02d9675a-c0bf-47d1-87d1-84085ac0c42b_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_79b9cab8-2b59-41ed-baa5-8ffc38323248_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_b9087054-1bda-4c59-9ca8-fe65665da106_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_f7b998a5-e9fd-49ae-8f20-b6066ffcfb88_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_047eec5e-3d4a-4a2e-a568-6429f69752b1_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_e6904c9b-92a6-44e0-ae09-f1a55ff1ec5f_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_fff7c408-3cec-4e35-86f2-5cf9201ee2f6_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_27531ac1-a82e-4b99-8858-00c4d7d10a7f_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_8614a8bb-1791-46b8-aae6-0797fc8f05bf_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_c47feab9-ea3b-4ed9-924d-5cd10c4ee8e5_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_ec1692e3-84fc-4196-acb4-bb4eff3af3cc_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_369ae71a-ea6c-465e-8052-80a58a58a838_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_c6928bf7-814d-40cb-95fa-03b62393dee2_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_54d712c1-a76f-4260-b81b-db2eb9ddcff4_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vW8_6c825c1b-8bf9-4d07-a9e0-98f44de59aeb_2_poly"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_425bcd6f-6583-4c7c-bae2-dfbb2e8673d6_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>23.847</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>425bcd6f-6583-4c7c-bae2-dfbb2e8673d6</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854961_3035">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_425bcd6f-6583-4c7c-bae2-dfbb2e8673d6_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32167">
                      <gml:posList srsDimension="3">456574.8388 5428830.3316 112.44 456568.92 5428833.26 112.44 456574.87 5428835.47 112.44 456578.1917 5428831.5801 112.44 456574.8388 5428830.3316 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_753b12e9-4378-4a90-ab2a-f9c426c55b07_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>31.509</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>49.186</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>339.607</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>753b12e9-4378-4a90-ab2a-f9c426c55b07</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854961_3036">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_753b12e9-4378-4a90-ab2a-f9c426c55b07_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32168">
                      <gml:posList srsDimension="3">456574.87 5428835.47 128.13999999999996 456568.92 5428833.26 128.13999999999996 456574.8388 5428830.3316 132.29 456578.1917 5428831.5801 132.29 456574.87 5428835.47 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_fc89377f-607c-4fb5-90f2-ff931fc3f036_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>26.6</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>fc89377f-607c-4fb5-90f2-ff931fc3f036</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854961_3037">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_fc89377f-607c-4fb5-90f2-ff931fc3f036_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32169">
                      <gml:posList srsDimension="3">456578.1917 5428831.5801 112.44 456574.87 5428835.47 112.44 456580.42999999993 5428837.58 112.44 456582.9609 5428833.39 112.44 456578.1917 5428831.5801 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_dc09c106-ce84-4889-a8ef-31431f9beb32_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>35.116</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>49.245</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>339.218</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>dc09c106-ce84-4889-a8ef-31431f9beb32</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854961_3038">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_dc09c106-ce84-4889-a8ef-31431f9beb32_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32170">
                      <gml:posList srsDimension="3">456580.42999999993 5428837.58 128.13999999999996 456574.87 5428835.47 128.13999999999996 456578.1917 5428831.5801 132.29 456582.9609 5428833.39 132.29 456580.42999999993 5428837.58 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_e06b4134-9257-4e4c-b04f-84d69bcf1323_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>26.647</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e06b4134-9257-4e4c-b04f-84d69bcf1323</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854961_3039">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_e06b4134-9257-4e4c-b04f-84d69bcf1323_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32171">
                      <gml:posList srsDimension="3">456582.9609 5428833.39 112.44 456580.42999999993 5428837.58 112.44 456585.94 5428839.78 112.44 456587.6987 5428835.2817 112.44 456582.9609 5428833.39 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_9fb9c0ab-7e4d-4c1e-9490-8d42b450c0cb_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>35.133</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>49.329</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>338.234</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>9fb9c0ab-7e4d-4c1e-9490-8d42b450c0cb</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854961_3040">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_9fb9c0ab-7e4d-4c1e-9490-8d42b450c0cb_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32172">
                      <gml:posList srsDimension="3">456585.94 5428839.78 128.13999999999996 456580.42999999993 5428837.58 128.13999999999996 456582.9609 5428833.39 132.29 456587.6987 5428835.2817 132.29 456585.94 5428839.78 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_44640427-9a1c-4e16-9205-f16825a41e90_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>30.922</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>44640427-9a1c-4e16-9205-f16825a41e90</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854961_3041">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_44640427-9a1c-4e16-9205-f16825a41e90_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32173">
                      <gml:posList srsDimension="3">456587.6987 5428835.2817 112.44 456585.94 5428839.78 112.44 456591.54 5428842.119999999 112.44 456593.9165 5428837.8799 112.44 456592.7746 5428837.4027 112.44 456587.6987 5428835.2817 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_541e7de6-6553-42f5-9d4f-48cf50f23c6c_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>40.774</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>49.322</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>337.322</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>541e7de6-6553-42f5-9d4f-48cf50f23c6c</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854961_3042">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_541e7de6-6553-42f5-9d4f-48cf50f23c6c_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32174">
                      <gml:posList srsDimension="3">456591.54 5428842.119999999 128.13999999999996 456585.94 5428839.78 128.13999999999996 456587.6987 5428835.2817 132.29 456592.7746 5428837.4027 132.29 456593.9165 5428837.8799 132.29 456591.54 5428842.119999999 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_98a13dc4-1e5c-4b29-8f28-34cef9f5e450_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>28.297</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>98a13dc4-1e5c-4b29-8f28-34cef9f5e450</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854961_3043">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_98a13dc4-1e5c-4b29-8f28-34cef9f5e450_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32175">
                      <gml:posList srsDimension="3">456605.2681 5428842.960899999 112.44 456602.56999999995 5428847.05 112.44 456608.04 5428849.68 112.44 456610.30489999993 5428845.3826 112.44 456605.2681 5428842.960899999 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_8fdeb363-2d76-4e40-b552-fc26a02b9365_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>37.228</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>49.473</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>334.322</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>8fdeb363-2d76-4e40-b552-fc26a02b9365</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854961_3044">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_8fdeb363-2d76-4e40-b552-fc26a02b9365_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32176">
                      <gml:posList srsDimension="3">456608.04 5428849.68 128.13999999999996 456602.56999999995 5428847.05 128.13999999999996 456605.2681 5428842.960899999 132.29 456610.30489999993 5428845.3826 132.29 456608.04 5428849.68 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_55df0c8e-8a05-4d4e-8fb2-25a4f68103a5_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>29.069</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>55df0c8e-8a05-4d4e-8fb2-25a4f68103a5</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854961_3045">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_55df0c8e-8a05-4d4e-8fb2-25a4f68103a5_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32177">
                      <gml:posList srsDimension="3">456610.30489999993 5428845.3826 112.44 456608.04 5428849.68 112.44 456613.35 5428852.2 112.44 456615.8169 5428847.9984 112.44 456614.1915 5428847.2271 112.44 456610.30489999993 5428845.3826 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_0ea315d9-526a-42e6-819d-265701b84083_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>38.247</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>49.467</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>334.612</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>0ea315d9-526a-42e6-819d-265701b84083</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854961_3046">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_0ea315d9-526a-42e6-819d-265701b84083_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32178">
                      <gml:posList srsDimension="3">456613.35 5428852.2 128.13999999999996 456608.04 5428849.68 128.13999999999996 456610.30489999993 5428845.3826 132.29 456614.1915 5428847.2271 132.29 456615.8169 5428847.9984 132.29 456613.35 5428852.2 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_dc3f7cfd-6d68-4ed0-ac95-df586d9b7066_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>30.046</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>dc3f7cfd-6d68-4ed0-ac95-df586d9b7066</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854961_3047">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_dc3f7cfd-6d68-4ed0-ac95-df586d9b7066_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32179">
                      <gml:posList srsDimension="3">456599.5204 5428840.2399 112.44 456597.12 5428844.469999999 112.44 456602.56999999995 5428847.05 112.44 456605.2681 5428842.960899999 112.44 456599.5204 5428840.2399 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_732d8ffc-7a0e-455a-9dae-909fdf79dad7_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>39.543</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>49.45</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>334.667</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>732d8ffc-7a0e-455a-9dae-909fdf79dad7</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3048">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_732d8ffc-7a0e-455a-9dae-909fdf79dad7_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32180">
                      <gml:posList srsDimension="3">456602.56999999995 5428847.05 128.13999999999996 456597.12 5428844.469999999 128.13999999999996 456599.5204 5428840.2399 132.29 456605.2681 5428842.960899999 132.29 456602.56999999995 5428847.05 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_6727a964-585e-4549-9deb-f9f3bd597db5_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>29.307</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>6727a964-585e-4549-9deb-f9f3bd597db5</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3049">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_6727a964-585e-4549-9deb-f9f3bd597db5_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32181">
                      <gml:posList srsDimension="3">456593.9165 5428837.8799 112.44 456591.54 5428842.119999999 112.44 456597.12 5428844.469999999 112.44 456599.5204 5428840.2399 112.44 456593.9165 5428837.8799 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_81e12c30-bfb2-41a2-adc7-0be9a7578a7b_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>38.639</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>49.331</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>337.162</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>81e12c30-bfb2-41a2-adc7-0be9a7578a7b</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3050">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_81e12c30-bfb2-41a2-adc7-0be9a7578a7b_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32182">
                      <gml:posList srsDimension="3">456597.12 5428844.469999999 128.13999999999996 456591.54 5428842.119999999 128.13999999999996 456593.9165 5428837.8799 132.29 456599.5204 5428840.2399 132.29 456597.12 5428844.469999999 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_ce7efb2d-36db-44b6-a0b7-df3a6418aaac_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>30.714</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>ce7efb2d-36db-44b6-a0b7-df3a6418aaac</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3051">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_ce7efb2d-36db-44b6-a0b7-df3a6418aaac_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32183">
                      <gml:posList srsDimension="3">456615.8169 5428847.9984 112.44 456613.35 5428852.2 112.44 456618.65 5428854.89 112.44 456621.7647 5428850.9864 112.44 456615.8169 5428847.9984 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_aad9fa21-a17c-4aaa-8baa-b60fda9f8003_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>40.335</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>49.595</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>333.215</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>aad9fa21-a17c-4aaa-8baa-b60fda9f8003</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3052">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_aad9fa21-a17c-4aaa-8baa-b60fda9f8003_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32184">
                      <gml:posList srsDimension="3">456618.65 5428854.89 128.13999999999996 456613.35 5428852.2 128.13999999999996 456615.8169 5428847.9984 132.29 456621.7647 5428850.9864 132.29 456618.65 5428854.89 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_d0f8da80-074e-4819-8a42-482a4060ba31_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>15.606</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>d0f8da80-074e-4819-8a42-482a4060ba31</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3053">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_d0f8da80-074e-4819-8a42-482a4060ba31_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32185">
                      <gml:posList srsDimension="3">456621.7647 5428850.9864 112.44 456618.65 5428854.89 112.44 456624.3 5428857.83 112.44 456621.7647 5428850.9864 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_a0c50fd5-73e9-46cf-b625-6bbbc10378a3_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>20.45</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>49.741</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>332.51</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>a0c50fd5-73e9-46cf-b625-6bbbc10378a3</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3054">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_a0c50fd5-73e9-46cf-b625-6bbbc10378a3_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32186">
                      <gml:posList srsDimension="3">456618.65 5428854.89 128.13999999999996 456621.7647 5428850.9864 132.29 456624.3 5428857.83 128.13999999999996 456618.65 5428854.89 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_a53daa25-8bb8-41c8-8cf4-805abccfe50f_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>13.402</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>a53daa25-8bb8-41c8-8cf4-805abccfe50f</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3055">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_a53daa25-8bb8-41c8-8cf4-805abccfe50f_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32187">
                      <gml:posList srsDimension="3">456621.7647 5428850.9864 112.44 456624.3 5428857.83 112.44 456625.57 5428855.57 112.44 456622.8285 5428849.0135 112.44 456621.7647 5428850.9864 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_745ae98f-1288-4049-b619-e8b8ddeb9921_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>16.74</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>53.19</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>61.13</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>745ae98f-1288-4049-b619-e8b8ddeb9921</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3056">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_745ae98f-1288-4049-b619-e8b8ddeb9921_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32188">
                      <gml:posList srsDimension="3">456625.57 5428855.57 128.13999999999996 456624.3 5428857.83 128.13999999999996 456621.7647 5428850.9864 132.29 456622.8285 5428849.0135 132.29 456625.57 5428855.57 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_460d0923-f762-49f5-b52c-c3ca7e796410_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>11.315</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>460d0923-f762-49f5-b52c-c3ca7e796410</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3057">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_460d0923-f762-49f5-b52c-c3ca7e796410_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32189">
                      <gml:posList srsDimension="3">456575.5022 5428828.3009 112.44 456569.76 5428830.72 112.44 456568.92 5428833.26 112.44 456574.8388 5428830.3316 112.44 456575.5022 5428828.3009 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_05a45ed8-a361-4746-8196-02a05024394f_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>15.09</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>48.575</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>251.793</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>05a45ed8-a361-4746-8196-02a05024394f</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3058">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_05a45ed8-a361-4746-8196-02a05024394f_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32190">
                      <gml:posList srsDimension="3">456568.92 5428833.26 128.13999999999996 456569.76 5428830.72 128.13999999999996 456575.5022 5428828.3009 132.29 456574.8388 5428830.3316 132.29 456568.92 5428833.26 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_9432a605-8bbd-4699-b961-8bd9605e15b8_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>24.966</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>9432a605-8bbd-4699-b961-8bd9605e15b8</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3059">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_9432a605-8bbd-4699-b961-8bd9605e15b8_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32191">
                      <gml:posList srsDimension="3">456592.4785 5428787.2922 112.44 456592.88 5428782.26 112.44 456586.44 5428780.01 112.44 456589.2651 5428786.1694 112.44 456592.4785 5428787.2922 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_58c77241-5e75-4d6f-8e83-3a6e56c6c9c2_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>36.658</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>42.925</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>160.741</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>58c77241-5e75-4d6f-8e83-3a6e56c6c9c2</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3060">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_58c77241-5e75-4d6f-8e83-3a6e56c6c9c2_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32192">
                      <gml:posList srsDimension="3">456586.44 5428780.01 128.13999999999996 456592.88 5428782.26 128.13999999999996 456592.4785 5428787.2922 133.38999999999996 456589.2651 5428786.1694 133.38999999999996 456586.44 5428780.01 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_0dd8bb5e-3f88-4653-826e-3c7c75d32c29_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>22.395</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>0dd8bb5e-3f88-4653-826e-3c7c75d32c29</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3061">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_0dd8bb5e-3f88-4653-826e-3c7c75d32c29_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32193">
                      <gml:posList srsDimension="3">456596.2433 5428788.831599999 112.44 456597.64 5428784.06 112.44 456592.88 5428782.26 112.44 456592.4785 5428787.2922 112.44 456596.2433 5428788.831599999 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_cb458a9b-754b-4303-9b2a-72b81fcbedac_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>32.85</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>42.979</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>158.608</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>cb458a9b-754b-4303-9b2a-72b81fcbedac</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3062">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_cb458a9b-754b-4303-9b2a-72b81fcbedac_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32194">
                      <gml:posList srsDimension="3">456592.88 5428782.26 128.13999999999996 456597.64 5428784.06 128.13999999999996 456596.2433 5428788.831599999 133.38999999999996 456592.4785 5428787.2922 133.38999999999996 456592.88 5428782.26 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_32cffdb9-20b6-4fde-a5ce-3a4f5f9153c4_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>37.731</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>32cffdb9-20b6-4fde-a5ce-3a4f5f9153c4</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3063">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_32cffdb9-20b6-4fde-a5ce-3a4f5f9153c4_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32195">
                      <gml:posList srsDimension="3">456603.2692 5428791.6637 112.44 456604.76 5428786.93 112.44 456597.64 5428784.06 112.44 456596.2433 5428788.831599999 112.44 456603.2692 5428791.6637 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_7534da49-612a-46f0-844f-defc889e5fb3_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>55.014</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>43.302</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>158.046</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>7534da49-612a-46f0-844f-defc889e5fb3</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3064">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_7534da49-612a-46f0-844f-defc889e5fb3_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32196">
                      <gml:posList srsDimension="3">456597.64 5428784.06 128.13999999999996 456604.76 5428786.93 128.13999999999996 456603.2692 5428791.6637 133.38999999999996 456596.2433 5428788.831599999 133.38999999999996 456597.64 5428784.06 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_de373966-ee5e-4368-8fd3-ebd27337ad38_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>13.031</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>de373966-ee5e-4368-8fd3-ebd27337ad38</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3065">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_de373966-ee5e-4368-8fd3-ebd27337ad38_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32197">
                      <gml:posList srsDimension="3">456605.6768 5428792.6482 112.44 456607.23 5428787.94 112.44 456604.76 5428786.93 112.44 456603.2692 5428791.6637 112.44 456605.6768 5428792.6482 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_beae90d3-a764-429b-9240-f6df5343c316_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>19.004</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>43.291</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>157.76</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>beae90d3-a764-429b-9240-f6df5343c316</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3066">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_beae90d3-a764-429b-9240-f6df5343c316_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32198">
                      <gml:posList srsDimension="3">456604.76 5428786.93 128.13999999999996 456607.23 5428787.94 128.13999999999996 456605.6768 5428792.6482 133.38999999999996 456603.2692 5428791.6637 133.38999999999996 456604.76 5428786.93 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_052e543e-6e80-44b4-a447-76670abf9edd_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>7.355</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>052e543e-6e80-44b4-a447-76670abf9edd</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3067">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_052e543e-6e80-44b4-a447-76670abf9edd_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32199">
                      <gml:posList srsDimension="3">456607.0267 5428793.2613 112.44 456608.5999999999 5428788.489999999 112.44 456607.23 5428787.94 112.44 456605.6768 5428792.6482 112.44 456607.0267 5428793.2613 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_622e1ddb-2085-4c37-a0ea-0d9651426319_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>10.695</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>43.446</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>156.847</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>622e1ddb-2085-4c37-a0ea-0d9651426319</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3068">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_622e1ddb-2085-4c37-a0ea-0d9651426319_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32200">
                      <gml:posList srsDimension="3">456607.23 5428787.94 128.13999999999996 456608.5999999999 5428788.489999999 128.13999999999996 456607.0267 5428793.2613 133.38999999999996 456605.6768 5428792.6482 133.38999999999996 456607.23 5428787.94 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_8091cd59-bc43-4265-8423-38d6df9a0cb5_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>58.402</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>8091cd59-bc43-4265-8423-38d6df9a0cb5</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3069">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_8091cd59-bc43-4265-8423-38d6df9a0cb5_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32201">
                      <gml:posList srsDimension="3">456617.1687 5428797.9095 112.44 456619.7183 5428793.5816 112.44 456608.5999999999 5428788.489999999 112.44 456607.0267 5428793.2613 112.44 456617.1687 5428797.9095 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_53cbd62c-f7f4-4f89-a3c2-adfbe1b0edac_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>87.304</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>41.986</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.69</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>155.386</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>53cbd62c-f7f4-4f89-a3c2-adfbe1b0edac</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3070">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_53cbd62c-f7f4-4f89-a3c2-adfbe1b0edac_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32202">
                      <gml:posList srsDimension="3">456608.5999999999 5428788.489999999 128.13999999999996 456619.7183 5428793.5816 128.13999999999996 456617.1687 5428797.9095 133.69 456607.0267 5428793.2613 133.69 456608.5999999999 5428788.489999999 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_9f1354dc-bd51-4b80-a7f6-06e2c67ce6db_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>57.105</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>9f1354dc-bd51-4b80-a7f6-06e2c67ce6db</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3071">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_9f1354dc-bd51-4b80-a7f6-06e2c67ce6db_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32203">
                      <gml:posList srsDimension="3">456626.4639 5428802.505 112.44 456630.86 5428799.09 112.44 456619.7183 5428793.5816 112.44 456617.1687 5428797.9095 112.44 456626.4639 5428802.505 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_4f8c43ca-2acb-42ab-81cc-0c73a3de21d0_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>85.226</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>42.07</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.69</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>153.692</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>4f8c43ca-2acb-42ab-81cc-0c73a3de21d0</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3072">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_4f8c43ca-2acb-42ab-81cc-0c73a3de21d0_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32204">
                      <gml:posList srsDimension="3">456619.7183 5428793.5816 128.13999999999996 456630.86 5428799.09 128.13999999999996 456626.4639 5428802.505 133.69 456617.1687 5428797.9095 133.69 456619.7183 5428793.5816 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_18e220d1-75e0-47e2-95e9-31114f5b86e3_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>8.081</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>18e220d1-75e0-47e2-95e9-31114f5b86e3</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3073">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_18e220d1-75e0-47e2-95e9-31114f5b86e3_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32205">
                      <gml:posList srsDimension="3">456629.09599999996 5428803.816 112.44 456631.11 5428799.22 112.44 456630.86 5428799.09 112.44 456626.4639 5428802.505 112.44 456629.09599999996 5428803.816 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_60c50302-be5f-48a0-adba-9b90f415ceda_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>11.698</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>43.694</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>153.436</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>60c50302-be5f-48a0-adba-9b90f415ceda</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3074">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_60c50302-be5f-48a0-adba-9b90f415ceda_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32206">
                      <gml:posList srsDimension="3">456630.86 5428799.09 128.13999999999996 456631.11 5428799.22 128.13999999999996 456629.09599999996 5428803.816 133.38999999999996 456626.4639 5428802.505 133.38999999999996 456630.86 5428799.09 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_35369121-773b-4fa3-85dc-f24112a4a324_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>15.243</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>35369121-773b-4fa3-85dc-f24112a4a324</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3075">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_35369121-773b-4fa3-85dc-f24112a4a324_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32207">
                      <gml:posList srsDimension="3">456631.832 5428805.1788 112.44 456633.82 5428800.58 112.44 456631.11 5428799.22 112.44 456629.09599999996 5428803.816 112.44 456631.832 5428805.1788 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_1866e2ac-367b-4a71-b3b4-0673fd53404c_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>22.086</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>43.642</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>153.437</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>1866e2ac-367b-4a71-b3b4-0673fd53404c</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3076">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_1866e2ac-367b-4a71-b3b4-0673fd53404c_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32208">
                      <gml:posList srsDimension="3">456631.11 5428799.22 128.13999999999996 456633.82 5428800.58 128.13999999999996 456631.832 5428805.1788 133.38999999999996 456629.09599999996 5428803.816 133.38999999999996 456631.11 5428799.22 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_b594b7ce-29de-43f2-826d-e78b4f2ea787_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>32.985</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>b594b7ce-29de-43f2-826d-e78b4f2ea787</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3077">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_b594b7ce-29de-43f2-826d-e78b4f2ea787_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32209">
                      <gml:posList srsDimension="3">456637.5366 5428808.0202 112.44 456639.94 5428803.66 112.44 456633.82 5428800.58 112.44 456631.832 5428805.1788 112.44 456637.5366 5428808.0202 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_4d5a5dc9-1a51-45a4-ba02-e339399008fc_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>47.886</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>43.537</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>153.4</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>4d5a5dc9-1a51-45a4-ba02-e339399008fc</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3078">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_4d5a5dc9-1a51-45a4-ba02-e339399008fc_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32210">
                      <gml:posList srsDimension="3">456633.82 5428800.58 128.13999999999996 456639.94 5428803.66 128.13999999999996 456637.5366 5428808.0202 133.38999999999996 456631.832 5428805.1788 133.38999999999996 456633.82 5428800.58 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_dc4ec527-1c68-4b34-a96d-39ee650f6c7e_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>28.215</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>dc4ec527-1c68-4b34-a96d-39ee650f6c7e</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3079">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_dc4ec527-1c68-4b34-a96d-39ee650f6c7e_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32211">
                      <gml:posList srsDimension="3">456641.6408 5428810.0645 112.44 456646.09 5428807.05 112.44 456639.94 5428803.66 112.44 456637.5366 5428808.0202 112.44 456641.6408 5428810.0645 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_26839f82-449c-4bb4-bb79-5983b5b8c486_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>41.523</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>42.806</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>152.078</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>26839f82-449c-4bb4-bb79-5983b5b8c486</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3080">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_26839f82-449c-4bb4-bb79-5983b5b8c486_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32212">
                      <gml:posList srsDimension="3">456639.94 5428803.66 128.13999999999996 456646.09 5428807.05 128.13999999999996 456641.6408 5428810.0645 133.38999999999996 456637.5366 5428808.0202 133.38999999999996 456639.94 5428803.66 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_7e743c21-62dd-4c73-84ae-78f1832f0346_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>19.731</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>7e743c21-62dd-4c73-84ae-78f1832f0346</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3081">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_7e743c21-62dd-4c73-84ae-78f1832f0346_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32213">
                      <gml:posList srsDimension="3">456644.0189 5428811.249 112.44 456651.12 5428809.899999999 112.44 456646.09 5428807.05 112.44 456641.6408 5428810.0645 112.44 456644.0189 5428811.249 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_6d0bedb9-0bdb-4b57-97bb-b85231ceca2c_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>29.659</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>41.704</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>151.427</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>6d0bedb9-0bdb-4b57-97bb-b85231ceca2c</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3082">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_6d0bedb9-0bdb-4b57-97bb-b85231ceca2c_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32214">
                      <gml:posList srsDimension="3">456646.09 5428807.05 128.13999999999996 456651.12 5428809.899999999 128.13999999999996 456644.0189 5428811.249 133.38999999999996 456641.6408 5428810.0645 133.38999999999996 456646.09 5428807.05 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_3afe0639-0865-44e7-929b-cec8dcf177a7_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>51.15</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>3afe0639-0865-44e7-929b-cec8dcf177a7</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3083">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_3afe0639-0865-44e7-929b-cec8dcf177a7_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32215">
                      <gml:posList srsDimension="3">456578.1882 5428820.0784 112.44 456573.8671 5428818.2277 112.44 456569.76 5428830.72 112.44 456575.5022 5428828.3009 112.44 456578.1882 5428820.0784 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_a2a707e9-8504-41e7-8dc4-38dfca2457ef_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>68.283</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>48.511</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>251.844</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>a2a707e9-8504-41e7-8dc4-38dfca2457ef</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3084">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_a2a707e9-8504-41e7-8dc4-38dfca2457ef_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32216">
                      <gml:posList srsDimension="3">456569.76 5428830.72 128.13999999999996 456573.8671 5428818.2277 128.13999999999996 456578.1882 5428820.0784 132.29 456575.5022 5428828.3009 132.29 456569.76 5428830.72 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_8b709cc0-3fbc-48b5-acfb-6a440f51e831_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>56.61</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>8b709cc0-3fbc-48b5-acfb-6a440f51e831</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3085">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_8b709cc0-3fbc-48b5-acfb-6a440f51e831_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32217">
                      <gml:posList srsDimension="3">456581.914 5428808.673 112.44 456577.69 5428806.6 112.44 456573.8671 5428818.2277 112.44 456578.1882 5428820.0784 112.44 456578.6855 5428818.5559 112.44 456579.377 5428816.4392 112.44 456579.7888 5428815.1786 112.44 456580.3457 5428813.4739 112.44 456581.914 5428808.673 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_a52bb882-3c0d-4dd0-9820-10a514689fc0_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>85.164</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>41.66</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>251.854</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>a52bb882-3c0d-4dd0-9820-10a514689fc0</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3086">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_a52bb882-3c0d-4dd0-9820-10a514689fc0_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32218">
                      <gml:posList srsDimension="3">456573.8671 5428818.2277 128.13999999999996 456577.69 5428806.6 128.13999999999996 456581.914 5428808.673 133.38999999999996 456580.3457 5428813.4739 133.38999999999996 456579.7888 5428815.1786 133.38999999999996 456579.377 5428816.4392 133.38999999999996 456578.6855 5428818.5559 133.38999999999996 456578.1882 5428820.0784 133.38999999999996 456573.8671 5428818.2277 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_0545cb65-09fc-4e24-892f-1194bee8e2d7_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>81.95</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>0545cb65-09fc-4e24-892f-1194bee8e2d7</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3087">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_0545cb65-09fc-4e24-892f-1194bee8e2d7_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32219">
                      <gml:posList srsDimension="3">456582.7531 5428821.9319 112.44 456591.9467 5428825.6027 112.44 456595.1147999999 5428818.2665 112.44 456585.2528 5428814.3288 112.44 456584.89239999995 5428815.424999999 112.44 456584.3323 5428817.1287 112.44 456583.9573 5428818.2692 112.44 456583.2618 5428820.3846 112.44 456582.7531 5428821.9319 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_2c55036a-0f24-457e-b512-03fc3438f171_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>81.95</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>90.0</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>115.71</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>115.71</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>-1.0</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>2c55036a-0f24-457e-b512-03fc3438f171</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3088">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_2c55036a-0f24-457e-b512-03fc3438f171_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32220">
                      <gml:posList srsDimension="3">456585.2528 5428814.3288 115.71 456595.1147999999 5428818.2665 115.71 456591.9467 5428825.6027 115.71 456582.7531 5428821.9319 115.71 456583.2618 5428820.3846 115.71 456583.9573 5428818.2692 115.71 456584.3323 5428817.1287 115.71 456584.89239999995 5428815.424999999 115.71 456585.2528 5428814.3288 115.71</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_dc9a562c-239e-4f32-9995-ab65940c729b_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>53.255</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>dc9a562c-239e-4f32-9995-ab65940c729b</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3089">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_dc9a562c-239e-4f32-9995-ab65940c729b_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32221">
                      <gml:posList srsDimension="3">456582.7531 5428821.9319 112.44 456581.5521 5428824.7129 112.44 456580.6194 5428826.8726 112.44 456582.1204 5428827.4719 112.44 456584.30939999997 5428828.345999999 112.44 456587.426 5428829.5903 112.44 456589.813 5428830.5434 112.44 456591.9467 5428825.6027 112.44 456582.7531 5428821.9319 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_f775990d-d8f0-4aaf-8b55-ab3dae07a119_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>53.255</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>90.0</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>120.8</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>120.8</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>-1.0</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>f775990d-d8f0-4aaf-8b55-ab3dae07a119</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3090">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_f775990d-d8f0-4aaf-8b55-ab3dae07a119_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32222">
                      <gml:posList srsDimension="3">456591.9467 5428825.6027 120.8 456589.813 5428830.5434 120.8 456587.426 5428829.5903 120.8 456584.30939999997 5428828.345999999 120.8 456582.1204 5428827.4719 120.8 456580.6194 5428826.8726 120.8 456581.5521 5428824.7129 120.8 456582.7531 5428821.9319 120.8 456591.9467 5428825.6027 120.8</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_06cd3aad-da73-4489-89a7-3c7c82ac0f68_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>7.939</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>06cd3aad-da73-4489-89a7-3c7c82ac0f68</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3091">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_06cd3aad-da73-4489-89a7-3c7c82ac0f68_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32223">
                      <gml:posList srsDimension="3">456578.1882 5428820.0784 112.44 456582.7531 5428821.9319 112.44 456583.2618 5428820.3846 112.44 456582.3043 5428820.0023 112.44 456579.4663 5428818.8692 112.44 456578.6855 5428818.5559 112.44 456578.1882 5428820.0784 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_94f9c636-8ea8-4b08-bf2d-968defa7352b_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>11.614</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>43.122</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>71.855</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>94f9c636-8ea8-4b08-bf2d-968defa7352b</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3092">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_94f9c636-8ea8-4b08-bf2d-968defa7352b_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32224">
                      <gml:posList srsDimension="3">456583.2618 5428820.3846 128.13999999999996 456582.7531 5428821.9319 128.13999999999996 456578.1882 5428820.0784 133.38999999999996 456578.6855 5428818.5559 133.38999999999996 456579.4667 5428818.8681 132.4938 456579.5734 5428818.912 132.3709 456582.3043 5428820.0023 129.2384 456583.2618 5428820.3846 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_59c234f7-5390-4261-89d5-5c6502a9b15b_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>17.812</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>59c234f7-5390-4261-89d5-5c6502a9b15b</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3093">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_59c234f7-5390-4261-89d5-5c6502a9b15b_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32225">
                      <gml:posList srsDimension="3">456580.3457 5428813.4739 112.44 456581.1292 5428813.8111 112.44 456583.943 5428815.0179 112.44 456584.89239999995 5428815.424999999 112.44 456585.2528 5428814.3288 112.44 456582.87619999994 5428809.1175 112.44 456582.23089999997 5428807.7026 112.44 456581.914 5428808.673 112.44 456580.3457 5428813.4739 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_8a9a3a54-8e5f-4104-aa40-1235d958c20d_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>26.02</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>43.198</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>71.891</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>8a9a3a54-8e5f-4104-aa40-1235d958c20d</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3094">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_8a9a3a54-8e5f-4104-aa40-1235d958c20d_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32226">
                      <gml:posList srsDimension="3">456585.2528 5428814.3288 128.13999999999996 456584.89239999995 5428815.424999999 128.13999999999996 456583.943 5428815.0179 129.2361 456581.2181 5428813.8492 132.38219999999998 456581.1295999999 5428813.8103 132.4848 456580.3457 5428813.4739 133.38999999999996 456581.914 5428808.673 133.38999999999996 456582.23089999997 5428807.7026 133.38999999999996 456582.87619999994 5428809.1175 132.269 456585.2528 5428814.3288 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_ab1c2f02-6dda-47b5-b024-dd4348fe9c28_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>95.611</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>ab1c2f02-6dda-47b5-b024-dd4348fe9c28</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3095">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_ab1c2f02-6dda-47b5-b024-dd4348fe9c28_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32227">
                      <gml:posList srsDimension="3">456601.4209 5428817.3359 112.44 456595.1147999999 5428818.2665 112.44 456591.9467 5428825.6027 112.44 456589.813 5428830.5434 112.44 456592.7746 5428837.4027 112.44 456601.4209 5428817.3359 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_d224ae47-8b5c-47eb-ac65-772042802f8d_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>120.347</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>52.605</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>246.672</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>d224ae47-8b5c-47eb-ac65-772042802f8d</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3096">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_d224ae47-8b5c-47eb-ac65-772042802f8d_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32228">
                      <gml:posList srsDimension="3">456589.813 5428830.5434 128.13999999999996 456591.9467 5428825.6027 128.13999999999996 456595.1147999999 5428818.2665 128.13999999999996 456601.4209 5428817.3359 132.29 456592.7746 5428837.4027 132.29 456589.813 5428830.5434 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_ba89bae7-3fef-4d26-a825-7fb310ee810f_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>56.732</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>ba89bae7-3fef-4d26-a825-7fb310ee810f</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3097">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_ba89bae7-3fef-4d26-a825-7fb310ee810f_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32229">
                      <gml:posList srsDimension="3">456584.3918 5428809.2544 112.44 456585.2528 5428814.3288 112.44 456595.1147999999 5428818.2665 112.44 456601.4209 5428817.3359 112.44 456588.6736999999 5428811.2864 112.44 456584.3918 5428809.2544 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_8af374b6-8297-4f87-83c1-530b99d79ae8_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>83.391</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>42.868</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>335.917</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>8af374b6-8297-4f87-83c1-530b99d79ae8</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3098">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_8af374b6-8297-4f87-83c1-530b99d79ae8_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32230">
                      <gml:posList srsDimension="3">456595.1147999999 5428818.2665 128.13999999999996 456585.2528 5428814.3288 128.13999999999996 456584.3918 5428809.2544 132.29 456588.6736999999 5428811.2864 132.29 456601.4209 5428817.3359 132.29 456595.1147999999 5428818.2665 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_8e25cb9c-d516-42d5-9dcc-89bc6ecd832f_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>3.786</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>8e25cb9c-d516-42d5-9dcc-89bc6ecd832f</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3099">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_8e25cb9c-d516-42d5-9dcc-89bc6ecd832f_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32231">
                      <gml:posList srsDimension="3">456584.3918 5428809.2544 112.44 456582.87619999994 5428809.1175 112.44 456585.2528 5428814.3288 112.44 456584.3918 5428809.2544 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_ef849a8f-b05a-415b-bf52-05d61b75906d_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.93</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>50.174</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>354.839</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>ef849a8f-b05a-415b-bf52-05d61b75906d</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3100">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_ef849a8f-b05a-415b-bf52-05d61b75906d_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32232">
                      <gml:posList srsDimension="3">456584.3918 5428809.2544 132.29 456585.2528 5428814.3288 128.13999999999996 456582.87619999994 5428809.1175 132.29 456584.3918 5428809.2544 132.29</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_3d8dc3f6-d833-4301-bd67-9627b172ef85_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>1.028</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>3d8dc3f6-d833-4301-bd67-9627b172ef85</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3101">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_3d8dc3f6-d833-4301-bd67-9627b172ef85_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32233">
                      <gml:posList srsDimension="3">456582.23089999997 5428807.7026 112.44 456582.87619999994 5428809.1175 112.44 456584.3918 5428809.2544 112.44 456582.23089999997 5428807.7026 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_5ae032f4-395e-4016-865e-a7a9de62a7df_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>1.326</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>50.849</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>354.839</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>5ae032f4-395e-4016-865e-a7a9de62a7df</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3102">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_5ae032f4-395e-4016-865e-a7a9de62a7df_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32234">
                      <gml:posList srsDimension="3">456582.87619999994 5428809.1175 132.29 456582.23089999997 5428807.7026 133.38999999999996 456584.3918 5428809.2544 132.29 456582.87619999994 5428809.1175 132.29</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_a11d26b1-1413-45f7-963a-13c301698dc6_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>1.874</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>a11d26b1-1413-45f7-963a-13c301698dc6</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3103">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_a11d26b1-1413-45f7-963a-13c301698dc6_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32235">
                      <gml:posList srsDimension="3">456578.6855 5428818.5559 112.44 456579.4663 5428818.8692 112.44 456580.16179999994 5428816.7538 112.44 456579.377 5428816.4392 112.44 456578.6855 5428818.5559 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_e5fc3898-56ca-4b29-a06e-ee6c71e30481_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.826</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>41.539</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>132.44</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>71.854</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e5fc3898-56ca-4b29-a06e-ee6c71e30481</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3104">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_e5fc3898-56ca-4b29-a06e-ee6c71e30481_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32236">
                      <gml:posList srsDimension="3">456580.16179999994 5428816.7538 132.44 456579.4663 5428818.8692 132.44 456578.6855 5428818.5559 133.38999999999996 456579.377 5428816.4392 133.38999999999996 456580.16179999994 5428816.7538 132.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_0f05d1dc-8b58-4507-b97c-f68905a78f32_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>1.521</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>0f05d1dc-8b58-4507-b97c-f68905a78f32</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3105">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_0f05d1dc-8b58-4507-b97c-f68905a78f32_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32237">
                      <gml:posList srsDimension="3">456579.7888 5428815.1786 112.44 456580.5691 5428815.5148 112.44 456581.1292 5428813.8111 112.44 456580.3457 5428813.4739 112.44 456579.7888 5428815.1786 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_0b309a76-acfd-4ef0-9b21-cb9fd695a100_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.284</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>41.749</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>132.44</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>71.855</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>0b309a76-acfd-4ef0-9b21-cb9fd695a100</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3106">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_0b309a76-acfd-4ef0-9b21-cb9fd695a100_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32238">
                      <gml:posList srsDimension="3">456581.1292 5428813.8111 132.44 456580.5691 5428815.5148 132.44 456579.7888 5428815.1786 133.38999999999996 456580.3457 5428813.4739 133.38999999999996 456581.1292 5428813.8111 132.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_09bf8276-996a-4e2e-930c-ea32701ce502_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>6.792</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>09bf8276-996a-4e2e-930c-ea32701ce502</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3107">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_09bf8276-996a-4e2e-930c-ea32701ce502_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32239">
                      <gml:posList srsDimension="3">456579.4663 5428818.8692 112.44 456582.3043 5428820.0023 112.44 456582.99979999993 5428817.886899999 112.44 456580.16179999994 5428816.7538 112.44 456579.4663 5428818.8692 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_ba196c6d-1aff-4722-a25a-7417d97f8a3d_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>7.92</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>59.036</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.44</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.61</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>71.799</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>ba196c6d-1aff-4722-a25a-7417d97f8a3d</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854962_3108">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_ba196c6d-1aff-4722-a25a-7417d97f8a3d_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32240">
                      <gml:posList srsDimension="3">456582.99979999993 5428817.886899999 130.61 456582.3043 5428820.0023 130.61 456579.5734 5428818.912 132.3709 456579.4663 5428818.8692 132.44 456580.16179999994 5428816.7538 132.44 456580.2617 5428816.7937 132.3756 456582.99979999993 5428817.886899999 130.61</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_57344bb3-ffe4-431d-bb59-2a52b441e438_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.291</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>57344bb3-ffe4-431d-bb59-2a52b441e438</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3109">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_57344bb3-ffe4-431d-bb59-2a52b441e438_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32241">
                      <gml:posList srsDimension="3">456582.3043 5428820.0023 112.44 456583.2618 5428820.3846 112.44 456583.9573 5428818.2692 112.44 456582.99979999993 5428817.886899999 112.44 456582.3043 5428820.0023 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_d1d1b54d-a519-4e20-a2ee-2cefa2107058_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>3.436</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>41.822</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>129.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>71.8</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>d1d1b54d-a519-4e20-a2ee-2cefa2107058</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3110">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_d1d1b54d-a519-4e20-a2ee-2cefa2107058_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32242">
                      <gml:posList srsDimension="3">456583.9573 5428818.2692 128.13999999999996 456583.2618 5428820.3846 128.13999999999996 456582.3043 5428820.0023 129.29 456582.99979999993 5428817.886899999 129.29 456583.9573 5428818.2692 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_50dd73f5-a1dc-4c4f-813c-125f8f404e60_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>5.47</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>50dd73f5-a1dc-4c4f-813c-125f8f404e60</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3111">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_50dd73f5-a1dc-4c4f-813c-125f8f404e60_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32243">
                      <gml:posList srsDimension="3">456580.5691 5428815.5148 112.44 456583.38289999997 5428816.7215 112.44 456583.943 5428815.0179 112.44 456581.1292 5428813.8111 112.44 456580.5691 5428815.5148 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_8b9de822-56c2-4785-b833-9ba5ebee2ed8_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>6.379</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>59.036</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.44</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.61</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>71.8</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>8b9de822-56c2-4785-b833-9ba5ebee2ed8</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3112">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_8b9de822-56c2-4785-b833-9ba5ebee2ed8_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32244">
                      <gml:posList srsDimension="3">456583.943 5428815.0179 130.61 456583.38289999997 5428816.7215 130.61 456580.6638 5428815.5554 132.3784 456580.5691 5428815.5148 132.44 456581.1292 5428813.8111 132.44 456581.2181 5428813.8492 132.38219999999998 456583.943 5428815.0179 130.61</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_77a37421-7b99-4223-a670-b6ed40b6faa2_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>1.845</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>77a37421-7b99-4223-a670-b6ed40b6faa2</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3113">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_77a37421-7b99-4223-a670-b6ed40b6faa2_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32245">
                      <gml:posList srsDimension="3">456583.38289999997 5428816.7215 112.44 456584.3323 5428817.1287 112.44 456584.89239999995 5428815.424999999 112.44 456583.943 5428815.0179 112.44 456583.38289999997 5428816.7215 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_e5e5d20a-96e2-425d-80c6-a4ab3ea0d4e2_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.768</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>41.824</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>129.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>71.801</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e5e5d20a-96e2-425d-80c6-a4ab3ea0d4e2</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3114">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_e5e5d20a-96e2-425d-80c6-a4ab3ea0d4e2_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32246">
                      <gml:posList srsDimension="3">456584.89239999995 5428815.424999999 128.13999999999996 456584.3323 5428817.1287 128.13999999999996 456583.38289999997 5428816.7215 129.29 456583.943 5428815.0179 129.29 456584.89239999995 5428815.424999999 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_79a505b9-1f78-4294-ab13-e124aacaae14_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>6.22</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>79a505b9-1f78-4294-ab13-e124aacaae14</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3115">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_79a505b9-1f78-4294-ab13-e124aacaae14_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748907_32247">
                      <gml:posList srsDimension="3">456579.377 5428816.4392 112.44 456580.16179999994 5428816.7538 112.44 456582.99979999993 5428817.886899999 112.44 456583.9573 5428818.2692 112.44 456584.3323 5428817.1287 112.44 456583.38289999997 5428816.7215 112.44 456580.5691 5428815.5148 112.44 456579.7888 5428815.1786 112.44 456579.377 5428816.4392 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_4894b8a4-9172-4734-ae46-99325c079038_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>9.093</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>43.162</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>71.861</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>4894b8a4-9172-4734-ae46-99325c079038</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3116">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_4894b8a4-9172-4734-ae46-99325c079038_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32248">
                      <gml:posList srsDimension="3">456584.3323 5428817.1287 128.13999999999996 456583.9573 5428818.2692 128.13999999999996 456582.99979999993 5428817.886899999 129.2374 456580.2617 5428816.7937 132.3756 456580.1621 5428816.7529 132.49009999999998 456579.377 5428816.4392 133.38999999999996 456579.7888 5428815.1786 133.38999999999996 456580.5696 5428815.5137 132.4878 456580.6638 5428815.5554 132.3784 456583.38289999997 5428816.7215 129.2368 456584.3323 5428817.1287 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_85096926-04af-444c-83e4-bf75e844003b_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>23.63</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>85096926-04af-444c-83e4-bf75e844003b</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3117">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_85096926-04af-444c-83e4-bf75e844003b_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32249">
                      <gml:posList srsDimension="3">456589.2651 5428786.1694 112.44 456593.80489999993 5428795.6166 112.44 456596.6423 5428796.8385 112.44 456592.4785 5428787.2922 112.44 456589.2651 5428786.1694 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_94f22c2b-294b-41e7-be9d-d11e5817dbff_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>24.638</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>73.552</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>338.819</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>94f22c2b-294b-41e7-be9d-d11e5817dbff</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3118">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_94f22c2b-294b-41e7-be9d-d11e5817dbff_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32250">
                      <gml:posList srsDimension="3">456596.6423 5428796.8385 131.24 456593.80489999993 5428795.6166 131.24 456589.2651 5428786.1694 133.38999999999996 456592.4785 5428787.2922 133.38999999999996 456596.6423 5428796.8385 131.24</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_f7032497-10ee-4580-86ab-a2d9268b418b_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>25.3</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>f7032497-10ee-4580-86ab-a2d9268b418b</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3119">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_f7032497-10ee-4580-86ab-a2d9268b418b_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32251">
                      <gml:posList srsDimension="3">456592.4785 5428787.2922 112.44 456596.6423 5428796.8385 112.44 456599.33139999997 5428797.9966 112.44 456596.2433 5428788.831599999 112.44 456592.4785 5428787.2922 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_276fb58d-ebac-4eca-94ef-83b9b6f6bffe_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>26.394</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>73.447</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>337.317</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>276fb58d-ebac-4eca-94ef-83b9b6f6bffe</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3120">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_276fb58d-ebac-4eca-94ef-83b9b6f6bffe_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32252">
                      <gml:posList srsDimension="3">456599.33139999997 5428797.9966 131.24 456596.6423 5428796.8385 131.24 456592.4785 5428787.2922 133.38999999999996 456596.2433 5428788.831599999 133.38999999999996 456599.33139999997 5428797.9966 131.24</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_75e0303d-09e6-43b2-ac7f-1dfc0cd21998_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>35.901</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>75e0303d-09e6-43b2-ac7f-1dfc0cd21998</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3121">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_75e0303d-09e6-43b2-ac7f-1dfc0cd21998_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32253">
                      <gml:posList srsDimension="3">456596.2433 5428788.831599999 112.44 456599.33139999997 5428797.9966 112.44 456601.3436 5428798.8632 112.44 456603.2692 5428791.6637 112.44 456596.2433 5428788.831599999 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_fb7f2928-11c8-45aa-a96c-7d7e1df3ad58_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>37.404</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>73.7</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>337.744</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>fb7f2928-11c8-45aa-a96c-7d7e1df3ad58</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3122">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_fb7f2928-11c8-45aa-a96c-7d7e1df3ad58_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32254">
                      <gml:posList srsDimension="3">456601.3436 5428798.8632 131.24 456599.33139999997 5428797.9966 131.24 456596.2433 5428788.831599999 133.38999999999996 456603.2692 5428791.6637 133.38999999999996 456601.3436 5428798.8632 131.24</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_0c737a21-cd43-4af0-9ee8-9ebbb5f6321a_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>17.894</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>0c737a21-cd43-4af0-9ee8-9ebbb5f6321a</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3123">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_0c737a21-cd43-4af0-9ee8-9ebbb5f6321a_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32255">
                      <gml:posList srsDimension="3">456603.2692 5428791.6637 112.44 456601.3436 5428798.8632 112.44 456603.3926 5428799.7456 112.44 456605.6768 5428792.6482 112.44 456603.2692 5428791.6637 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_b62d9d07-11c0-411e-afa2-33fdb643b28e_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>18.632</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>73.813</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>337.271</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>b62d9d07-11c0-411e-afa2-33fdb643b28e</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3124">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_b62d9d07-11c0-411e-afa2-33fdb643b28e_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32256">
                      <gml:posList srsDimension="3">456603.3926 5428799.7456 131.24 456601.3436 5428798.8632 131.24 456603.2692 5428791.6637 133.38999999999996 456605.6768 5428792.6482 133.38999999999996 456603.3926 5428799.7456 131.24</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_6e8f9304-c711-4020-bfa3-8ac5159a6335_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>17.186</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>6e8f9304-c711-4020-bfa3-8ac5159a6335</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3125">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_6e8f9304-c711-4020-bfa3-8ac5159a6335_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32257">
                      <gml:posList srsDimension="3">456605.6768 5428792.6482 112.44 456603.3926 5428799.7456 112.44 456606.2984 5428800.997 112.44 456607.0267 5428793.2613 112.44 456605.6768 5428792.6482 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_0cb726e7-40ae-458f-979c-465d9e51823f_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>17.897</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>73.794</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>336.341</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>0cb726e7-40ae-458f-979c-465d9e51823f</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3126">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_0cb726e7-40ae-458f-979c-465d9e51823f_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32258">
                      <gml:posList srsDimension="3">456606.2984 5428800.997 131.24 456603.3926 5428799.7456 131.24 456605.6768 5428792.6482 133.38999999999996 456607.0267 5428793.2613 133.38999999999996 456606.2984 5428800.997 131.24</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_723a1f3e-dd88-4e0c-87fa-550db361e7bb_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>50.599</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>723a1f3e-dd88-4e0c-87fa-550db361e7bb</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3127">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_723a1f3e-dd88-4e0c-87fa-550db361e7bb_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32259">
                      <gml:posList srsDimension="3">456607.0267 5428793.2613 112.44 456606.2984 5428800.997 112.44 456608.79 5428802.07 112.44 456617.1687 5428797.9095 112.44 456607.0267 5428793.2613 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_bf833494-6f49-4083-9324-e26f766b5db7_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>53.375</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>71.44</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.69</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>335.636</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>bf833494-6f49-4083-9324-e26f766b5db7</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3128">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_bf833494-6f49-4083-9324-e26f766b5db7_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32260">
                      <gml:posList srsDimension="3">456608.79 5428802.07 131.24 456606.2984 5428800.997 131.24 456607.0267 5428793.2613 133.69 456617.1687 5428797.9095 133.69 456608.79 5428802.07 131.24</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_b55d2150-56e7-4550-a31d-5fdd637c62ee_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>119.321</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>b55d2150-56e7-4550-a31d-5fdd637c62ee</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3129">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_b55d2150-56e7-4550-a31d-5fdd637c62ee_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32261">
                      <gml:posList srsDimension="3">456583.0687 5428805.138 112.44 456589.7786 5428807.9418 112.44 456593.80489999993 5428795.6166 112.44 456589.2651 5428786.1694 112.44 456583.0687 5428805.138 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_620c996c-e831-46d5-8e1b-4992747ae270_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>124.459</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>73.48</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>71.909</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>620c996c-e831-46d5-8e1b-4992747ae270</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3130">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_620c996c-e831-46d5-8e1b-4992747ae270_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32262">
                      <gml:posList srsDimension="3">456593.80489999993 5428795.6166 131.24 456589.7786 5428807.9418 131.24 456583.0687 5428805.138 133.38999999999996 456589.2651 5428786.1694 133.38999999999996 456593.80489999993 5428795.6166 131.24</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_e11d88ea-3444-46e0-bf24-c34625ca9be7_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>98.94</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e11d88ea-3444-46e0-bf24-c34625ca9be7</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3131">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_e11d88ea-3444-46e0-bf24-c34625ca9be7_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32263">
                      <gml:posList srsDimension="3">456617.1687 5428797.9095 112.44 456608.79 5428802.07 112.44 456603.61 5428814.35 112.44 456610.22579999996 5428814.9233 112.44 456617.1687 5428797.9095 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_2ea12ae5-54e8-409e-89cd-75faf9a08595_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>106.289</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>68.569</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.69</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>247.518</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>2ea12ae5-54e8-409e-89cd-75faf9a08595</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3132">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_2ea12ae5-54e8-409e-89cd-75faf9a08595_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32264">
                      <gml:posList srsDimension="3">456603.61 5428814.35 131.24 456608.79 5428802.07 131.24 456617.1687 5428797.9095 133.69 456610.22579999996 5428814.9233 133.69 456603.61 5428814.35 131.24</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_c6317f6d-9ca5-45ee-9edf-c133e99bde11_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>42.846</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>c6317f6d-9ca5-45ee-9edf-c133e99bde11</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3133">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_c6317f6d-9ca5-45ee-9edf-c133e99bde11_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32265">
                      <gml:posList srsDimension="3">456617.1687 5428797.9095 112.44 456620.37 5428807.49 112.44 456621.7304 5428808.2399 112.44 456626.4639 5428802.505 112.44 456617.1687 5428797.9095 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_00d9379c-960c-4607-a39c-decb5e1cfbc2_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>45.665</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>69.763</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.69</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>333.359</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>00d9379c-960c-4607-a39c-decb5e1cfbc2</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3134">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_00d9379c-960c-4607-a39c-decb5e1cfbc2_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32266">
                      <gml:posList srsDimension="3">456621.7304 5428808.2399 131.04 456620.37 5428807.49 131.04 456617.1687 5428797.9095 133.69 456626.4639 5428802.505 133.69 456621.7304 5428808.2399 131.04</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_d495570e-3c26-45d3-a1d5-bdb2459c487f_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>103.233</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>d495570e-3c26-45d3-a1d5-bdb2459c487f</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3135">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_d495570e-3c26-45d3-a1d5-bdb2459c487f_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32267">
                      <gml:posList srsDimension="3">456610.22579999996 5428814.9233 112.44 456615.4644 5428819.5115 112.44 456620.37 5428807.49 112.44 456617.1687 5428797.9095 112.44 456610.22579999996 5428814.9233 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_17aabb24-6033-4cba-a49d-db8eb8acdabd_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>111.282</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>68.075</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.69</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>67.801</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>17aabb24-6033-4cba-a49d-db8eb8acdabd</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3136">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_17aabb24-6033-4cba-a49d-db8eb8acdabd_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32268">
                      <gml:posList srsDimension="3">456620.37 5428807.49 131.04 456615.4644 5428819.5115 131.04 456610.22579999996 5428814.9233 133.69 456617.1687 5428797.9095 133.69 456620.37 5428807.49 131.04</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_abd79283-7228-4944-ad9b-a3895cfb43dd_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>86.986</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>abd79283-7228-4944-ad9b-a3895cfb43dd</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3137">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_abd79283-7228-4944-ad9b-a3895cfb43dd_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32269">
                      <gml:posList srsDimension="3">456592.7746 5428837.4027 112.44 456599.012 5428835.027 112.44 456604.65 5428822.41 112.44 456601.4209 5428817.3359 112.44 456592.7746 5428837.4027 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_2e97c556-04a3-48ed-a110-6535279f896e_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>114.213</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>49.607</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>66.392</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>2e97c556-04a3-48ed-a110-6535279f896e</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3138">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_2e97c556-04a3-48ed-a110-6535279f896e_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32270">
                      <gml:posList srsDimension="3">456604.65 5428822.41 128.13999999999996 456599.012 5428835.027 128.13999999999996 456592.7746 5428837.4027 132.29 456601.4209 5428817.3359 132.29 456604.65 5428822.41 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_543426d2-a23f-403e-9dbd-cdbbf3401948_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>21.275</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>543426d2-a23f-403e-9dbd-cdbbf3401948</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3139">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_543426d2-a23f-403e-9dbd-cdbbf3401948_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32271">
                      <gml:posList srsDimension="3">456610.22579999996 5428814.9233 112.44 456603.61 5428814.35 112.44 456602.018 5428816.4788 112.44 456604.8982 5428817.3711 112.44 456608.7410999999 5428818.5617 112.44 456610.22579999996 5428814.9233 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_9a96e5ff-ddf0-4ea3-8f08-25fa1e519c9d_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>22.732</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>69.375</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.69</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>241.911</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>9a96e5ff-ddf0-4ea3-8f08-25fa1e519c9d</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3140">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_9a96e5ff-ddf0-4ea3-8f08-25fa1e519c9d_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32272">
                      <gml:posList srsDimension="3">456602.018 5428816.4788 131.24 456603.61 5428814.35 131.24 456610.22579999996 5428814.9233 133.69 456608.7410999999 5428818.5617 133.69 456604.8982 5428817.3711 132.29 456602.018 5428816.4788 131.24</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_b7ac7f3e-8a0b-4315-8c01-9a11e1ebcd67_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>1.501</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>b7ac7f3e-8a0b-4315-8c01-9a11e1ebcd67</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3141">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_b7ac7f3e-8a0b-4315-8c01-9a11e1ebcd67_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32273">
                      <gml:posList srsDimension="3">456602.018 5428816.4788 112.44 456601.4209 5428817.3359 112.44 456604.8982 5428817.3711 112.44 456602.018 5428816.4788 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_05b696fb-881c-4904-915b-ca0163d4c949_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.363</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>39.42</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>179.42</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>05b696fb-881c-4904-915b-ca0163d4c949</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3142">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_05b696fb-881c-4904-915b-ca0163d4c949_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32274">
                      <gml:posList srsDimension="3">456601.4209 5428817.3359 132.29 456602.018 5428816.4788 131.24 456604.8982 5428817.3711 132.29 456601.4209 5428817.3359 132.29</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_b86c06af-c27f-4f5d-9f36-32e916ba36b4_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.002</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>b86c06af-c27f-4f5d-9f36-32e916ba36b4</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3143">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_b86c06af-c27f-4f5d-9f36-32e916ba36b4_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32275">
                      <gml:posList srsDimension="3">456608.7410999999 5428818.5617 112.44 456604.8982 5428817.3711 112.44 456601.4209 5428817.3359 112.44 456608.7410999999 5428818.5617 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_94c9a397-fb32-48ad-ad87-efcdb46b9a16_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>3.152</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>39.441</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.69</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>179.42</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>94c9a397-fb32-48ad-ad87-efcdb46b9a16</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3144">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_94c9a397-fb32-48ad-ad87-efcdb46b9a16_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32276">
                      <gml:posList srsDimension="3">456604.8982 5428817.3711 132.29 456608.7410999999 5428818.5617 133.69 456601.4209 5428817.3359 132.29 456604.8982 5428817.3711 132.29</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_3b52d4d0-c23a-4c92-b77b-95d6b6fb878d_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>11.83</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>3b52d4d0-c23a-4c92-b77b-95d6b6fb878d</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3145">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_3b52d4d0-c23a-4c92-b77b-95d6b6fb878d_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32277">
                      <gml:posList srsDimension="3">456608.7410999999 5428818.5617 112.44 456601.4209 5428817.3359 112.44 456614.1611 5428822.7015 112.44 456608.7410999999 5428818.5617 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_fed7e289-94b7-4dd6-b7d1-aa87c32b7fb5_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>15.284</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>50.718</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.69</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>337.161</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>fed7e289-94b7-4dd6-b7d1-aa87c32b7fb5</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3146">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_fed7e289-94b7-4dd6-b7d1-aa87c32b7fb5_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32278">
                      <gml:posList srsDimension="3">456601.4209 5428817.3359 132.29 456608.7410999999 5428818.5617 133.69 456614.1611 5428822.7015 132.29 456601.4209 5428817.3359 132.29</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_e406c7bc-119d-4c1d-ad11-52190d87bba4_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>18.469</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e406c7bc-119d-4c1d-ad11-52190d87bba4</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3147">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_e406c7bc-119d-4c1d-ad11-52190d87bba4_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32279">
                      <gml:posList srsDimension="3">456614.1915 5428847.2271 112.44 456616.3072 5428843.2145 112.44 456612.826 5428841.5665 112.44 456610.30489999993 5428845.3826 112.44 456614.1915 5428847.2271 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_185fa9f1-1de9-4d88-a4ad-8d71ea12f7e1_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>25.047</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>47.508</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>154.638</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>185fa9f1-1de9-4d88-a4ad-8d71ea12f7e1</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3148">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_185fa9f1-1de9-4d88-a4ad-8d71ea12f7e1_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32280">
                      <gml:posList srsDimension="3">456612.826 5428841.5665 128.13999999999996 456616.3072 5428843.2145 128.13999999999996 456614.1915 5428847.2271 132.29 456610.30489999993 5428845.3826 132.29 456612.826 5428841.5665 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_1b98351b-6a45-4943-81f3-3165aa850c86_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>25.232</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>1b98351b-6a45-4943-81f3-3165aa850c86</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3149">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_1b98351b-6a45-4943-81f3-3165aa850c86_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32281">
                      <gml:posList srsDimension="3">456610.30489999993 5428845.3826 112.44 456612.826 5428841.5665 112.44 456607.7704999999 5428839.1732 112.44 456605.2681 5428842.960899999 112.44 456610.30489999993 5428845.3826 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_91ca428c-0a3d-42d7-912d-a96b1ffd2ad8_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>34.279</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>47.399</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>154.494</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>91ca428c-0a3d-42d7-912d-a96b1ffd2ad8</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3150">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_91ca428c-0a3d-42d7-912d-a96b1ffd2ad8_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32282">
                      <gml:posList srsDimension="3">456607.7704999999 5428839.1732 128.13999999999996 456612.826 5428841.5665 128.13999999999996 456610.30489999993 5428845.3826 132.29 456605.2681 5428842.960899999 132.29 456607.7704999999 5428839.1732 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_21943d11-ec64-45d4-a5bf-56d6884064fa_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>27.366</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>21943d11-ec64-45d4-a5bf-56d6884064fa</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3151">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_21943d11-ec64-45d4-a5bf-56d6884064fa_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32283">
                      <gml:posList srsDimension="3">456599.5204 5428840.2399 112.44 456603.2415 5428837.0293 112.44 456599.012 5428835.027 112.44 456592.7746 5428837.4027 112.44 456593.9165 5428837.8799 112.44 456599.5204 5428840.2399 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_f0a3d9ba-bc77-4e58-85fb-234ca13f9edb_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>36.992</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>47.713</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>156.205</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>f0a3d9ba-bc77-4e58-85fb-234ca13f9edb</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3152">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_f0a3d9ba-bc77-4e58-85fb-234ca13f9edb_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32284">
                      <gml:posList srsDimension="3">456599.012 5428835.027 128.13999999999996 456603.2415 5428837.0293 128.13999999999996 456599.5204 5428840.2399 132.29 456593.9165 5428837.8799 132.29 456592.7746 5428837.4027 132.29 456599.012 5428835.027 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_7a59a332-bf13-4afe-a3a3-4a28d24351e1_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>25.549</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>7a59a332-bf13-4afe-a3a3-4a28d24351e1</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3153">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_7a59a332-bf13-4afe-a3a3-4a28d24351e1_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32285">
                      <gml:posList srsDimension="3">456605.2681 5428842.960899999 112.44 456607.7704999999 5428839.1732 112.44 456603.2415 5428837.0293 112.44 456599.5204 5428840.2399 112.44 456605.2681 5428842.960899999 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_9f6ea118-5dff-46cc-b0c7-a3ec72445fce_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>34.776</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>47.28</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>154.668</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>9f6ea118-5dff-46cc-b0c7-a3ec72445fce</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3154">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_9f6ea118-5dff-46cc-b0c7-a3ec72445fce_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32286">
                      <gml:posList srsDimension="3">456603.2415 5428837.0293 128.13999999999996 456607.7704999999 5428839.1732 128.13999999999996 456605.2681 5428842.960899999 132.29 456599.5204 5428840.2399 132.29 456603.2415 5428837.0293 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_56674c2a-60ff-43d3-9947-dabf17971b52_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>19.071</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>56674c2a-60ff-43d3-9947-dabf17971b52</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3155">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_56674c2a-60ff-43d3-9947-dabf17971b52_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32287">
                      <gml:posList srsDimension="3">456616.3072 5428843.2145 112.44 456622.39509999997 5428846.6758 112.44 456622.0263 5428844.585799999 112.44 456621.6575 5428842.4958 112.44 456620.726 5428841.9274 112.44 456617.83 5428840.16 112.44 456616.3072 5428843.2145 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_30a56c9e-41dd-41cf-a3d7-2a4761990b50_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.914</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>69.468</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.05</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>243.502</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>30a56c9e-41dd-41cf-a3d7-2a4761990b50</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3156">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_30a56c9e-41dd-41cf-a3d7-2a4761990b50_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32288">
                      <gml:posList srsDimension="3">456619.4019 5428843.051399999 132.05 456616.3072 5428843.2145 131.04 456617.83 5428840.16 131.04 456619.4019 5428843.051399999 132.05</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_47041e8c-6185-4870-b24e-2afa101f77d4_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>7.235</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>58.322</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.05</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>149.042</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>47041e8c-6185-4870-b24e-2afa101f77d4</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3157">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_47041e8c-6185-4870-b24e-2afa101f77d4_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32289">
                      <gml:posList srsDimension="3">456621.6575 5428842.4958 131.04 456622.0263 5428844.585799999 132.05 456619.4019 5428843.051399999 132.05 456617.83 5428840.16 131.04 456620.726 5428841.9274 131.04 456621.6011 5428842.4614 131.04 456621.6575 5428842.4958 131.04</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_e45b6847-df8c-4f83-8dfc-c110fa20ea92_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>9.737</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>58.609</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.05</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>330.17</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e45b6847-df8c-4f83-8dfc-c110fa20ea92</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3158">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_e45b6847-df8c-4f83-8dfc-c110fa20ea92_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32290">
                      <gml:posList srsDimension="3">456622.0263 5428844.585799999 132.05 456622.39509999997 5428846.6758 131.04 456616.3072 5428843.2145 131.04 456619.4019 5428843.051399999 132.05 456622.0263 5428844.585799999 132.05</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_1d6764c6-426d-4c46-bce5-b7d4976f68e3_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>9.081</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>1d6764c6-426d-4c46-bce5-b7d4976f68e3</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3159">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_1d6764c6-426d-4c46-bce5-b7d4976f68e3_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32291">
                      <gml:posList srsDimension="3">456623.7168 5428847.4272 112.44 456624.4821 5428846.057 112.44 456625.2474 5428844.6867 112.44 456621.6575 5428842.4958 112.44 456622.0263 5428844.585799999 112.44 456622.39509999997 5428846.6758 112.44 456623.7168 5428847.4272 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_fd9990c3-c0b4-4830-8d05-d3f0a299e94a_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.021</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>53.947</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>248.593</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>fd9990c3-c0b4-4830-8d05-d3f0a299e94a</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3160">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_fd9990c3-c0b4-4830-8d05-d3f0a299e94a_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32292">
                      <gml:posList srsDimension="3">456624.0106 5428845.7745 132.05 456624.4821 5428846.057 132.29 456623.7168 5428847.4272 132.29 456622.39509999997 5428846.6758 131.04 456624.0106 5428845.7745 132.05</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_c44d9cba-e213-4409-8ce5-22232bc73ca0_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>3.923</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>72.354</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>248.594</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>c44d9cba-e213-4409-8ce5-22232bc73ca0</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3161">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_c44d9cba-e213-4409-8ce5-22232bc73ca0_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32293">
                      <gml:posList srsDimension="3">456625.2474 5428844.6867 132.29 456624.4821 5428846.057 132.29 456624.0106 5428845.7745 132.05 456621.6575 5428842.4958 131.04 456625.2474 5428844.6867 132.29</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_636e754c-b898-47f3-9991-49200199cdbe_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.192</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>57.792</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.05</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>329.076</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>636e754c-b898-47f3-9991-49200199cdbe</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3162">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_636e754c-b898-47f3-9991-49200199cdbe_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32294">
                      <gml:posList srsDimension="3">456622.0263 5428844.585799999 132.05 456624.0106 5428845.7745 132.05 456622.39509999997 5428846.6758 131.04 456622.0263 5428844.585799999 132.05</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_7cb2e356-a6f8-43e3-8d69-4291c14e84ca_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.192</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>57.792</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.05</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>149.076</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>7cb2e356-a6f8-43e3-8d69-4291c14e84ca</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3163">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_7cb2e356-a6f8-43e3-8d69-4291c14e84ca_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32295">
                      <gml:posList srsDimension="3">456624.0106 5428845.7745 132.05 456622.0263 5428844.585799999 132.05 456621.6575 5428842.4958 131.04 456624.0106 5428845.7745 132.05</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_95c4aac4-3a7a-4d83-8dd4-ff9f6a59dabc_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.077</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>95c4aac4-3a7a-4d83-8dd4-ff9f6a59dabc</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3164">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_95c4aac4-3a7a-4d83-8dd4-ff9f6a59dabc_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32296">
                      <gml:posList srsDimension="3">456614.1915 5428847.2271 112.44 456615.8169 5428847.9984 112.44 456616.3072 5428843.2145 112.44 456614.1915 5428847.2271 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_038731e8-df4f-4e27-8136-fbcf230cf6a9_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>5.528</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>47.52</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>154.614</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>038731e8-df4f-4e27-8136-fbcf230cf6a9</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3165">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_038731e8-df4f-4e27-8136-fbcf230cf6a9_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32297">
                      <gml:posList srsDimension="3">456614.1915 5428847.2271 132.29 456616.3072 5428843.2145 128.13999999999996 456615.8169 5428847.9984 132.29 456614.1915 5428847.2271 132.29</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_77f2d858-2937-4847-bde8-894f685c4aac_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>14.96</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>77f2d858-2937-4847-bde8-894f685c4aac</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3166">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_77f2d858-2937-4847-bde8-894f685c4aac_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32298">
                      <gml:posList srsDimension="3">456615.8169 5428847.9984 112.44 456621.7647 5428850.9864 112.44 456617.9363 5428845.5344 112.44 456616.3072 5428843.2145 112.44 456615.8169 5428847.9984 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_e899c3a8-5bfe-4669-af2d-68edec872143_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>20.385</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>47.209</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>153.245</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e899c3a8-5bfe-4669-af2d-68edec872143</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3167">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_e899c3a8-5bfe-4669-af2d-68edec872143_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32299">
                      <gml:posList srsDimension="3">456615.8169 5428847.9984 132.29 456616.3072 5428843.2145 128.13999999999996 456617.9363 5428845.5344 129.37 456621.7647 5428850.9864 132.29 456615.8169 5428847.9984 132.29</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_512d91bb-b49a-433b-bd71-a6b2a2370a91_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>6.677</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>512d91bb-b49a-433b-bd71-a6b2a2370a91</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3168">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_512d91bb-b49a-433b-bd71-a6b2a2370a91_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32300">
                      <gml:posList srsDimension="3">456621.7647 5428850.9864 112.44 456622.8285 5428849.0135 112.44 456620.72799999994 5428847.5197 112.44 456617.9363 5428845.5344 112.44 456621.7647 5428850.9864 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_a0157e80-46b7-46af-87cf-71c82bd56222_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>7.436</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>63.878</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.37</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>241.861</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>a0157e80-46b7-46af-87cf-71c82bd56222</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3169">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_a0157e80-46b7-46af-87cf-71c82bd56222_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32301">
                      <gml:posList srsDimension="3">456621.7647 5428850.9864 132.29 456617.9363 5428845.5344 129.37 456620.72799999994 5428847.5197 131.04 456622.8285 5428849.0135 132.29 456621.7647 5428850.9864 132.29</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_959920d8-930f-4e20-a3bd-24e5d92d4b1a_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.242</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>959920d8-930f-4e20-a3bd-24e5d92d4b1a</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854963_3170">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_959920d8-930f-4e20-a3bd-24e5d92d4b1a_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32302">
                      <gml:posList srsDimension="3">456622.39509999997 5428846.6758 112.44 456616.3072 5428843.2145 112.44 456617.9363 5428845.5344 112.44 456622.39509999997 5428846.6758 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_c9deb3ee-ed11-44cf-a6df-73f6ee48d61d_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>7.224</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>35.96</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.37</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>330.379</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>c9deb3ee-ed11-44cf-a6df-73f6ee48d61d</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3171">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_c9deb3ee-ed11-44cf-a6df-73f6ee48d61d_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32303">
                      <gml:posList srsDimension="3">456622.39509999997 5428846.6758 131.04 456617.9363 5428845.5344 129.37 456616.3072 5428843.2145 131.04 456622.39509999997 5428846.6758 131.04</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_ab788e64-e0f4-4ccd-ac6f-c8c1dafe08b5_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>3.514</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>ab788e64-e0f4-4ccd-ac6f-c8c1dafe08b5</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3172">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_ab788e64-e0f4-4ccd-ac6f-c8c1dafe08b5_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32304">
                      <gml:posList srsDimension="3">456623.7168 5428847.4272 112.44 456622.39509999997 5428846.6758 112.44 456620.72799999994 5428847.5197 112.44 456622.8285 5428849.0135 112.44 456623.7168 5428847.4272 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_3087dc75-c36b-40ae-a01f-a2bcb65e19ac_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.148</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>57.9</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>223.561</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>3087dc75-c36b-40ae-a01f-a2bcb65e19ac</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3173">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_3087dc75-c36b-40ae-a01f-a2bcb65e19ac_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32305">
                      <gml:posList srsDimension="3">456620.72799999994 5428847.5197 131.04 456622.39509999997 5428846.6758 131.04 456623.7168 5428847.4272 132.29 456622.8285 5428849.0135 132.29 456620.72799999994 5428847.5197 131.04</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_1bae7a54-f1b4-48d7-8e37-b266ad92f851_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.833</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>1bae7a54-f1b4-48d7-8e37-b266ad92f851</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3174">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_1bae7a54-f1b4-48d7-8e37-b266ad92f851_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32306">
                      <gml:posList srsDimension="3">456620.72799999994 5428847.5197 112.44 456622.39509999997 5428846.6758 112.44 456617.9363 5428845.5344 112.44 456620.72799999994 5428847.5197 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_a2f547e0-2e0c-445d-9087-217c2106fd49_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>3.234</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>61.155</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.37</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>206.849</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>a2f547e0-2e0c-445d-9087-217c2106fd49</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3175">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_a2f547e0-2e0c-445d-9087-217c2106fd49_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32307">
                      <gml:posList srsDimension="3">456620.72799999994 5428847.5197 131.04 456617.9363 5428845.5344 129.37 456622.39509999997 5428846.6758 131.04 456620.72799999994 5428847.5197 131.04</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_031f68ed-28a9-4034-89f5-c03fa3e86346_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>271.064</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>031f68ed-28a9-4034-89f5-c03fa3e86346</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3176">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_031f68ed-28a9-4034-89f5-c03fa3e86346_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32308">
                      <gml:posList srsDimension="3">456622.5273 5428830.4987 112.443 456612.99759999994 5428826.187 112.44 456604.65 5428822.41 112.44 456599.012 5428835.027 112.44 456603.2415 5428837.0293 112.44 456607.7704999999 5428839.1732 112.44 456612.826 5428841.5665 112.44 456616.3072 5428843.2145 112.44 456621.0 5428833.64 112.44 456622.5273 5428830.4987 112.443</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_896eda34-680a-4f6f-8088-a898e63e82d0_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>271.005</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>90.0</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>112.453</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>112.453</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>-1.0</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>896eda34-680a-4f6f-8088-a898e63e82d0</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3177">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_896eda34-680a-4f6f-8088-a898e63e82d0_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32309">
                      <gml:posList srsDimension="3">456616.3072 5428843.2145 112.453 456612.826 5428841.5665 112.453 456607.7704999999 5428839.1732 112.453 456603.2415 5428837.0293 112.453 456599.012 5428835.027 112.453 456604.65 5428822.41 112.453 456612.99759999994 5428826.187 112.453 456622.5273 5428830.4987 112.453 456616.3072 5428843.2145 112.453</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_bde9ac7d-c072-475e-943a-d56e3648d61d_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>26.127</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>bde9ac7d-c072-475e-943a-d56e3648d61d</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3178">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_bde9ac7d-c072-475e-943a-d56e3648d61d_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32310">
                      <gml:posList srsDimension="3">456624.2795 5428835.7949 112.44 456621.0 5428833.64 112.44 456617.83 5428840.16 112.44 456620.726 5428841.9274 112.44 456622.9386 5428838.109 112.44 456624.2795 5428835.7949 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_de24d6db-df80-4679-86bb-e6cb117caa64_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>26.127</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>90.0</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>128.5</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.5</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>-1.0</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>de24d6db-df80-4679-86bb-e6cb117caa64</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3179">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_de24d6db-df80-4679-86bb-e6cb117caa64_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32311">
                      <gml:posList srsDimension="3">456620.726 5428841.9274 128.5 456617.83 5428840.16 128.5 456621.0 5428833.64 128.5 456624.2795 5428835.7949 128.5 456622.9386 5428838.109 128.5 456620.726 5428841.9274 128.5</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_b950faf6-d8cd-4c59-92be-c404f74b3819_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>23.657</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>b950faf6-d8cd-4c59-92be-c404f74b3819</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3180">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_b950faf6-d8cd-4c59-92be-c404f74b3819_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32312">
                      <gml:posList srsDimension="3">456621.0 5428833.64 112.44 456624.2795 5428835.7949 112.44 456625.1316 5428836.3549 112.44 456626.3216 5428834.227399999 112.44 456627.5115 5428832.0999 112.44 456622.9635 5428829.6015 112.44 456622.5273 5428830.4987 112.443 456621.0 5428833.64 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_71a11872-302a-425e-8bb1-17f68ff5e37b_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>5.06</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>63.373</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.05</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>244.071</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>71a11872-302a-425e-8bb1-17f68ff5e37b</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3181">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_71a11872-302a-425e-8bb1-17f68ff5e37b_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32313">
                      <gml:posList srsDimension="3">456623.71549999993 5428832.6621 132.05 456621.0 5428833.64 131.04 456622.5273 5428830.4987 131.04 456622.9635 5428829.6015 131.04 456623.71549999993 5428832.6621 132.05</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_5ac2864a-8c08-4f93-a627-9828d6a0c3f3_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>10.574</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>66.863</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.05</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>150.402</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>5ac2864a-8c08-4f93-a627-9828d6a0c3f3</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3182">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_5ac2864a-8c08-4f93-a627-9828d6a0c3f3_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32314">
                      <gml:posList srsDimension="3">456627.5115 5428832.0999 131.04 456626.3216 5428834.227399999 132.05 456623.71549999993 5428832.6621 132.05 456622.9635 5428829.6015 131.04 456627.5115 5428832.0999 131.04</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_df6c088a-d65c-49aa-8e09-a2a3aae83002_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>10.237</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>66.81</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.05</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>327.574</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>df6c088a-d65c-49aa-8e09-a2a3aae83002</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3183">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_df6c088a-d65c-49aa-8e09-a2a3aae83002_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32315">
                      <gml:posList srsDimension="3">456626.3216 5428834.227399999 132.05 456625.1316 5428836.3549 131.04 456624.2795 5428835.7949 131.04 456621.0 5428833.64 131.04 456623.71549999993 5428832.6621 132.05 456626.3216 5428834.227399999 132.05</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_1f84bc6e-dce1-4841-b3e0-5bb5f8b82454_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>23.641</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>1f84bc6e-dce1-4841-b3e0-5bb5f8b82454</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3184">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_1f84bc6e-dce1-4841-b3e0-5bb5f8b82454_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32316">
                      <gml:posList srsDimension="3">456627.48179999995 5428840.695 112.44 456626.8013 5428840.307699999 112.44 456622.9386 5428838.109 112.44 456620.726 5428841.9274 112.44 456621.6575 5428842.4958 112.44 456625.2474 5428844.6867 112.44 456627.48179999995 5428840.695 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_1df80a5f-b7f1-4c4c-b9be-5da25269bb30_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>24.646</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>73.584</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.74</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>240.175</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>1df80a5f-b7f1-4c4c-b9be-5da25269bb30</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3185">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_1df80a5f-b7f1-4c4c-b9be-5da25269bb30_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32317">
                      <gml:posList srsDimension="3">456620.726 5428841.9274 130.74 456622.9386 5428838.109 130.74 456626.8013 5428840.307699999 132.05 456627.48179999995 5428840.695 132.29 456625.2474 5428844.6867 132.29 456621.6575 5428842.4958 131.0593 456621.6011 5428842.4614 131.04 456620.726 5428841.9274 130.74</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_5af367b3-22bd-4e54-8b53-38d05c896ca3_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>65.494</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>5af367b3-22bd-4e54-8b53-38d05c896ca3</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3186">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_5af367b3-22bd-4e54-8b53-38d05c896ca3_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32318">
                      <gml:posList srsDimension="3">456622.8285 5428849.0135 112.44 456625.57 5428855.57 112.44 456632.34929999994 5428843.4484 112.44 456627.48179999995 5428840.695 112.44 456625.2474 5428844.6867 112.44 456624.4821 5428846.057 112.44 456623.7168 5428847.4272 112.44 456622.8285 5428849.0135 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_1a030b9e-5141-44dd-9c5a-fdc8aec59393_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>81.554</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>53.425</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>60.781</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>1a030b9e-5141-44dd-9c5a-fdc8aec59393</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3187">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_1a030b9e-5141-44dd-9c5a-fdc8aec59393_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32319">
                      <gml:posList srsDimension="3">456632.34929999994 5428843.4484 128.13999999999996 456625.57 5428855.57 128.13999999999996 456622.8285 5428849.0135 132.29 456623.7168 5428847.4272 132.29 456624.4821 5428846.057 132.29 456625.2474 5428844.6867 132.29 456627.48179999995 5428840.695 132.29 456632.34929999994 5428843.4484 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_dd6e08cb-b53d-46bd-9867-7b95667462ff_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>11.212</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>dd6e08cb-b53d-46bd-9867-7b95667462ff</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3188">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_dd6e08cb-b53d-46bd-9867-7b95667462ff_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32320">
                      <gml:posList srsDimension="3">456627.9693 5428838.2194 112.44 456625.1316 5428836.3549 112.44 456624.2795 5428835.7949 112.44 456622.9386 5428838.109 112.44 456626.8013 5428840.307699999 112.44 456627.9693 5428838.2194 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_8ddeed19-b1dc-4adf-93a4-7dc8f71caf54_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>11.693</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>73.512</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.05</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.74</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>240.225</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>8ddeed19-b1dc-4adf-93a4-7dc8f71caf54</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3189">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_8ddeed19-b1dc-4adf-93a4-7dc8f71caf54_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32321">
                      <gml:posList srsDimension="3">456622.9386 5428838.109 130.74 456624.2795 5428835.7949 130.74 456625.1316 5428836.3549 131.04 456627.9693 5428838.2194 132.05 456626.8013 5428840.307699999 132.05 456622.9386 5428838.109 130.74</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_a081b24c-ca3a-46b4-afba-26523276babd_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>1.85</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>a081b24c-ca3a-46b4-afba-26523276babd</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3190">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_a081b24c-ca3a-46b4-afba-26523276babd_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32322">
                      <gml:posList srsDimension="3">456628.6228 5428838.6489 112.44 456627.9693 5428838.2194 112.44 456626.8013 5428840.307699999 112.44 456627.48179999995 5428840.695 112.44 456628.6228 5428838.6489 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_f8a12c93-5b95-4c97-8a94-9dbdc0fae1f1_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>3.673</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>30.25</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>132.05</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>240.817</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>f8a12c93-5b95-4c97-8a94-9dbdc0fae1f1</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3191">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_f8a12c93-5b95-4c97-8a94-9dbdc0fae1f1_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32323">
                      <gml:posList srsDimension="3">456626.8013 5428840.307699999 132.05 456627.9693 5428838.2194 132.05 456628.6228 5428838.6489 133.38999999999996 456627.48179999995 5428840.695 133.38999999999996 456626.8013 5428840.307699999 132.05</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_79857e8a-26c9-4bbb-b2fc-432701c9cf67_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>16.953</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>79857e8a-26c9-4bbb-b2fc-432701c9cf67</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3192">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_79857e8a-26c9-4bbb-b2fc-432701c9cf67_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32324">
                      <gml:posList srsDimension="3">456627.9693 5428838.2194 112.44 456629.2229 5428835.9781 112.44 456630.4765 5428833.7367 112.44 456627.5115 5428832.0999 112.44 456626.3216 5428834.227399999 112.44 456625.1316 5428836.3549 112.44 456627.9693 5428838.2194 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_18060a0e-ec7a-4303-b4b2-d05d0e2e747c_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.538</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>73.394</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.05</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>240.781</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>18060a0e-ec7a-4303-b4b2-d05d0e2e747c</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3193">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_18060a0e-ec7a-4303-b4b2-d05d0e2e747c_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32325">
                      <gml:posList srsDimension="3">456629.2229 5428835.9781 132.05 456627.9693 5428838.2194 132.05 456625.1316 5428836.3549 131.04 456629.2229 5428835.9781 132.05</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_78e02257-416f-453b-87e6-aeab19157ede_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.469</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>67.484</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.05</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>328.892</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>78e02257-416f-453b-87e6-aeab19157ede</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3194">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_78e02257-416f-453b-87e6-aeab19157ede_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32326">
                      <gml:posList srsDimension="3">456626.3216 5428834.227399999 132.05 456629.2229 5428835.9781 132.05 456625.1316 5428836.3549 131.04 456626.3216 5428834.227399999 132.05</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_b6749f02-227f-4bd0-b4cd-0a7b70008825_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.468</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>67.483</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.05</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>148.892</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>b6749f02-227f-4bd0-b4cd-0a7b70008825</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3195">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_b6749f02-227f-4bd0-b4cd-0a7b70008825_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32327">
                      <gml:posList srsDimension="3">456627.5115 5428832.0999 131.04 456629.2229 5428835.9781 132.05 456626.3216 5428834.227399999 132.05 456627.5115 5428832.0999 131.04</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_5e2f51a5-2c9d-4bca-b314-9204b2648582_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.538</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>73.394</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.05</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>240.782</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>5e2f51a5-2c9d-4bca-b314-9204b2648582</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3196">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_5e2f51a5-2c9d-4bca-b314-9204b2648582_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32328">
                      <gml:posList srsDimension="3">456630.4765 5428833.7367 132.05 456629.2229 5428835.9781 132.05 456627.5115 5428832.0999 131.04 456630.4765 5428833.7367 132.05</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_dba50201-4f12-49bd-a542-c7f7035ddd3f_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.044</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>dba50201-4f12-49bd-a542-c7f7035ddd3f</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3197">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_dba50201-4f12-49bd-a542-c7f7035ddd3f_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32329">
                      <gml:posList srsDimension="3">456631.16439999995 5428834.1162 112.44 456630.4765 5428833.7367 112.44 456629.2229 5428835.9781 112.44 456627.9693 5428838.2194 112.44 456628.6228 5428838.6489 112.44 456631.16439999995 5428834.1162 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_298ea433-d110-4c13-b05f-aa8846361fc1_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>8.018</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>30.293</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>132.05</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>240.75</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>298ea433-d110-4c13-b05f-aa8846361fc1</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3198">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_298ea433-d110-4c13-b05f-aa8846361fc1_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32330">
                      <gml:posList srsDimension="3">456627.9693 5428838.2194 132.05 456629.2229 5428835.9781 132.05 456630.4765 5428833.7367 132.05 456631.16439999995 5428834.1162 133.38999999999996 456628.6228 5428838.6489 133.38999999999996 456627.9693 5428838.2194 132.05</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_343142a4-7ce9-471e-8737-03c5a76bc275_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>124.983</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>343142a4-7ce9-471e-8737-03c5a76bc275</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3199">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_343142a4-7ce9-471e-8737-03c5a76bc275_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32331">
                      <gml:posList srsDimension="3">456626.4639 5428802.505 112.44 456621.7304 5428808.2399 112.44 456634.76 5428814.84 112.44 456644.0189 5428811.249 112.44 456641.6408 5428810.0645 112.44 456637.5366 5428808.0202 112.44 456631.832 5428805.1788 112.44 456629.09599999996 5428803.816 112.44 456626.4639 5428802.505 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_a60181c4-21ef-42b0-94a3-d786cb8bf75e_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>131.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>72.167</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>333.357</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>a60181c4-21ef-42b0-94a3-d786cb8bf75e</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3200">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_a60181c4-21ef-42b0-94a3-d786cb8bf75e_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32332">
                      <gml:posList srsDimension="3">456634.76 5428814.84 131.04 456621.7304 5428808.2399 131.04 456626.4639 5428802.505 133.38999999999996 456629.09599999996 5428803.816 133.38999999999996 456631.832 5428805.1788 133.38999999999996 456637.5366 5428808.0202 133.38999999999996 456641.6408 5428810.0645 133.38999999999996 456644.0189 5428811.249 133.38999999999996 456634.76 5428814.84 131.04</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_a996a81d-caed-4f81-8254-c1d114c7fb2f_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>40.781</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>a996a81d-caed-4f81-8254-c1d114c7fb2f</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3201">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_a996a81d-caed-4f81-8254-c1d114c7fb2f_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32333">
                      <gml:posList srsDimension="3">456614.1611 5428822.7015 112.44 456612.99759999994 5428826.187 112.44 456622.5273 5428830.4987 112.443 456622.9635 5428829.6015 112.44 456624.2666 5428826.9397 112.44 456614.1611 5428822.7015 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_f72cf0c5-9be4-4922-82b5-0051f7746e22_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>60.311</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>42.545</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>336.47</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>f72cf0c5-9be4-4922-82b5-0051f7746e22</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3202">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_f72cf0c5-9be4-4922-82b5-0051f7746e22_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32334">
                      <gml:posList srsDimension="3">456622.5273 5428830.4987 128.13999999999996 456612.99759999994 5428826.187 128.13999999999996 456614.1611 5428822.7015 132.29 456624.2666 5428826.9397 132.29 456622.9635 5428829.6015 129.1852 456622.5273 5428830.4987 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_dbab981e-6410-4260-a9f4-e8cfc260cbff_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>40.405</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>dbab981e-6410-4260-a9f4-e8cfc260cbff</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3203">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_dbab981e-6410-4260-a9f4-e8cfc260cbff_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32335">
                      <gml:posList srsDimension="3">456601.4209 5428817.3359 112.44 456604.65 5428822.41 112.44 456612.99759999994 5428826.187 112.44 456614.1611 5428822.7015 112.44 456601.4209 5428817.3359 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_e910b927-b837-442d-b25f-1b6cff3fb998_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>62.507</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>40.271</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>336.561</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e910b927-b837-442d-b25f-1b6cff3fb998</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3204">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_e910b927-b837-442d-b25f-1b6cff3fb998_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32336">
                      <gml:posList srsDimension="3">456612.99759999994 5428826.187 128.13999999999996 456604.65 5428822.41 128.13999999999996 456601.4209 5428817.3359 132.29 456614.1611 5428822.7015 132.29 456612.99759999994 5428826.187 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_0247c8fa-7aa7-4bcd-a95d-e9e72cf486b6_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>39.866</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>0247c8fa-7aa7-4bcd-a95d-e9e72cf486b6</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3205">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_0247c8fa-7aa7-4bcd-a95d-e9e72cf486b6_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32337">
                      <gml:posList srsDimension="3">456624.2666 5428826.9397 112.44 456625.5197 5428826.6152 112.44 456628.59 5428825.82 112.44 456615.4644 5428819.5115 112.44 456614.522 5428821.8181 112.44 456614.1611 5428822.7015 112.44 456624.2666 5428826.9397 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_e6575387-ece9-4800-90dc-d2ae8faaa077_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>66.271</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>36.981</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>155.582</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e6575387-ece9-4800-90dc-d2ae8faaa077</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3206">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_e6575387-ece9-4800-90dc-d2ae8faaa077_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32338">
                      <gml:posList srsDimension="3">456615.4644 5428819.5115 128.13999999999996 456628.59 5428825.82 128.13999999999996 456625.5197 5428826.6152 131.0872 456624.2666 5428826.9397 132.29 456614.1611 5428822.7015 132.29 456614.522 5428821.8181 131.1408 456614.55369999993 5428821.7406 131.04 456615.4644 5428819.5115 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_6751622d-ebb0-4562-9c34-a0b67bda5ad6_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>8.789</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>6751622d-ebb0-4562-9c34-a0b67bda5ad6</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3207">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_6751622d-ebb0-4562-9c34-a0b67bda5ad6_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32339">
                      <gml:posList srsDimension="3">456629.4661 5428829.8078 112.44 456624.2666 5428826.9397 112.44 456622.9635 5428829.6015 112.44 456626.8678 5428829.725399999 112.44 456629.4661 5428829.8078 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_6c565868-00fc-4b1c-bb8d-cad42220d9d0_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>12.816</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>43.297</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>331.039</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>6c565868-00fc-4b1c-bb8d-cad42220d9d0</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3208">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_6c565868-00fc-4b1c-bb8d-cad42220d9d0_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32340">
                      <gml:posList srsDimension="3">456629.4661 5428829.8078 132.29 456626.8678 5428829.725399999 131.04 456622.9635 5428829.6015 129.13999999999996 456624.2666 5428826.9397 132.29 456629.4661 5428829.8078 132.29</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_1143c020-88cd-4295-b019-7bc8921a0df3_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>102.581</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>1143c020-88cd-4295-b019-7bc8921a0df3</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3209">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_1143c020-88cd-4295-b019-7bc8921a0df3_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32341">
                      <gml:posList srsDimension="3">456644.0189 5428811.249 112.44 456634.76 5428814.84 112.44 456628.59 5428825.82 112.44 456631.6173 5428827.275 112.44 456634.3051 5428828.5668 112.44 456644.0189 5428811.249 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_67385e04-dd77-4218-8a5d-27c940286423_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>109.438</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>69.609</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>240.669</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>67385e04-dd77-4218-8a5d-27c940286423</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3210">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_67385e04-dd77-4218-8a5d-27c940286423_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32342">
                      <gml:posList srsDimension="3">456628.59 5428825.82 131.04 456634.76 5428814.84 131.04 456644.0189 5428811.249 133.38999999999996 456634.3051 5428828.5668 133.38999999999996 456631.6173 5428827.275 132.29 456628.59 5428825.82 131.04</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_5fb447b8-a1ae-4ddf-b58c-78b48a2e8dcf_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>195.648</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>5fb447b8-a1ae-4ddf-b58c-78b48a2e8dcf</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3211">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_5fb447b8-a1ae-4ddf-b58c-78b48a2e8dcf_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32343">
                      <gml:posList srsDimension="3">456628.59 5428825.82 112.44 456634.76 5428814.84 112.44 456621.7304 5428808.2399 112.44 456620.37 5428807.49 112.44 456615.4644 5428819.5115 112.44 456628.59 5428825.82 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_c95e0b61-adac-4816-8327-eeea8831ccba_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>195.648</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>90.0</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>112.45</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>112.45</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>-1.0</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>c95e0b61-adac-4816-8327-eeea8831ccba</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3212">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_c95e0b61-adac-4816-8327-eeea8831ccba_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32344">
                      <gml:posList srsDimension="3">456615.4644 5428819.5115 112.45 456620.37 5428807.49 112.45 456621.7304 5428808.2399 112.45 456634.76 5428814.84 112.45 456628.59 5428825.82 112.45 456615.4644 5428819.5115 112.45</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_3ac651b0-e593-4440-91e4-da0c8b2a2fb9_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>126.366</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>3ac651b0-e593-4440-91e4-da0c8b2a2fb9</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3213">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_3ac651b0-e593-4440-91e4-da0c8b2a2fb9_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32345">
                      <gml:posList srsDimension="3">456634.3051 5428828.5668 112.44 456638.5548 5428832.345099999 112.44 456651.12 5428809.899999999 112.44 456644.0189 5428811.249 112.44 456634.3051 5428828.5668 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_16afb51e-5b89-463c-9e86-e6baf6e9e23d_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>174.021</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>46.565</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>60.738</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>16afb51e-5b89-463c-9e86-e6baf6e9e23d</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3214">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_16afb51e-5b89-463c-9e86-e6baf6e9e23d_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32346">
                      <gml:posList srsDimension="3">456651.12 5428809.899999999 128.13999999999996 456638.5548 5428832.345099999 128.13999999999996 456634.3051 5428828.5668 133.38999999999996 456644.0189 5428811.249 133.38999999999996 456651.12 5428809.899999999 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_ca58e949-1043-455c-a7f0-414d3ff94420_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>10.332</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>ca58e949-1043-455c-a7f0-414d3ff94420</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3215">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_ca58e949-1043-455c-a7f0-414d3ff94420_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32347">
                      <gml:posList srsDimension="3">456633.3148 5428830.2093 112.44 456637.69 5428833.89 112.44 456638.5548 5428832.345099999 112.44 456634.3051 5428828.5668 112.44 456633.3148 5428830.2093 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_cc6cb384-a2e6-45dd-9a64-d788c3efb594_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>14.159</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>46.864</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>59.8</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>cc6cb384-a2e6-45dd-9a64-d788c3efb594</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3216">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_cc6cb384-a2e6-45dd-9a64-d788c3efb594_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32348">
                      <gml:posList srsDimension="3">456638.5548 5428832.345099999 128.13999999999996 456637.69 5428833.89 128.13999999999996 456633.3148 5428830.2093 133.38999999999996 456634.3051 5428828.5668 133.38999999999996 456638.5548 5428832.345099999 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_59024e7b-0ecb-411c-bc14-3579983f3261_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>8.153</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>59024e7b-0ecb-411c-bc14-3579983f3261</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3217">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_59024e7b-0ecb-411c-bc14-3579983f3261_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32349">
                      <gml:posList srsDimension="3">456634.3051 5428828.5668 112.44 456631.6173 5428827.275 112.44 456629.4661 5428829.8078 112.44 456633.3148 5428830.2093 112.44 456634.3051 5428828.5668 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_203a6bb6-5807-4182-8c92-e777b914cd44_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>8.645</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>70.583</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>233.042</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>203a6bb6-5807-4182-8c92-e777b914cd44</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3218">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_203a6bb6-5807-4182-8c92-e777b914cd44_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32350">
                      <gml:posList srsDimension="3">456629.4661 5428829.8078 132.29 456631.6173 5428827.275 132.29 456634.3051 5428828.5668 133.38999999999996 456633.3148 5428830.2093 133.38999999999996 456629.4661 5428829.8078 132.29</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_9ed90f66-de02-49e8-901b-57d0e9f94737_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.596</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>9ed90f66-de02-49e8-901b-57d0e9f94737</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3219">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_9ed90f66-de02-49e8-901b-57d0e9f94737_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32351">
                      <gml:posList srsDimension="3">456622.9635 5428829.6015 112.44 456627.5115 5428832.0999 112.44 456626.8678 5428829.725399999 112.44 456622.9635 5428829.6015 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_c0f3ec90-8b53-4b0b-a02e-a324ac8283f6_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>5.156</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>63.043</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>285.168</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>c0f3ec90-8b53-4b0b-a02e-a324ac8283f6</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3220">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_c0f3ec90-8b53-4b0b-a02e-a324ac8283f6_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32352">
                      <gml:posList srsDimension="3">456627.5115 5428832.0999 131.04 456622.9635 5428829.6015 129.13999999999996 456626.8678 5428829.725399999 131.04 456627.5115 5428832.0999 131.04</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_b4a3770d-7feb-4a42-a874-bdb1525c7a04_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>3.058</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>b4a3770d-7feb-4a42-a874-bdb1525c7a04</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3221">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_b4a3770d-7feb-4a42-a874-bdb1525c7a04_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32353">
                      <gml:posList srsDimension="3">456629.4661 5428829.8078 112.44 456626.8678 5428829.725399999 112.44 456627.5115 5428832.0999 112.44 456629.4661 5428829.8078 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_3b6b34ff-cba4-4521-a336-cba22a4cbe69_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>3.423</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>63.308</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>285.168</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>3b6b34ff-cba4-4521-a336-cba22a4cbe69</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3222">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_3b6b34ff-cba4-4521-a336-cba22a4cbe69_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32354">
                      <gml:posList srsDimension="3">456626.8678 5428829.725399999 131.04 456629.4661 5428829.8078 132.29 456627.5115 5428832.0999 131.04 456626.8678 5428829.725399999 131.04</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_53cf4b31-519b-4bbe-a278-358babd532c4_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.852</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>53cf4b31-519b-4bbe-a278-358babd532c4</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3223">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_53cf4b31-519b-4bbe-a278-358babd532c4_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32355">
                      <gml:posList srsDimension="3">456633.3148 5428830.2093 112.44 456629.4661 5428829.8078 112.44 456629.8694 5428831.3318 112.44 456633.3148 5428830.2093 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_0ef08a7c-eea4-4ad9-8a61-0bc0307d49d0_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.981</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>73.088</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>284.823</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>0ef08a7c-eea4-4ad9-8a61-0bc0307d49d0</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3224">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_0ef08a7c-eea4-4ad9-8a61-0bc0307d49d0_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32356">
                      <gml:posList srsDimension="3">456629.4661 5428829.8078 132.29 456633.3148 5428830.2093 133.38999999999996 456629.8694 5428831.3318 132.29 456629.4661 5428829.8078 132.29</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_ef1a3656-bc02-406b-bd11-c0b3a6758c05_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>1.952</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>ef1a3656-bc02-406b-bd11-c0b3a6758c05</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3225">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_ef1a3656-bc02-406b-bd11-c0b3a6758c05_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32357">
                      <gml:posList srsDimension="3">456627.5115 5428832.0999 112.44 456629.8694 5428831.3318 112.44 456629.4661 5428829.8078 112.44 456627.5115 5428832.0999 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_0ea49728-88e2-40f5-988f-c4c06b14740e_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.186</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>63.213</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>284.823</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>0ea49728-88e2-40f5-988f-c4c06b14740e</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3226">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_0ea49728-88e2-40f5-988f-c4c06b14740e_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32358">
                      <gml:posList srsDimension="3">456629.8694 5428831.3318 132.29 456627.5115 5428832.0999 131.04 456629.4661 5428829.8078 132.29 456629.8694 5428831.3318 132.29</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_4ca180b1-52bd-4f0c-bdf9-124a4012fb95_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.86</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>4ca180b1-52bd-4f0c-bdf9-124a4012fb95</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3227">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_4ca180b1-52bd-4f0c-bdf9-124a4012fb95_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32359">
                      <gml:posList srsDimension="3">456632.2079 5428832.2429 112.44 456633.3148 5428830.2093 112.44 456629.8694 5428831.3318 112.44 456627.5115 5428832.0999 112.44 456629.5468 5428832.1645 112.44 456632.2079 5428832.2429 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_728d6cc1-088a-4d2f-a608-6db88f10bdcc_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>5.567</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>60.819</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>241.843</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>728d6cc1-088a-4d2f-a608-6db88f10bdcc</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3228">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_728d6cc1-088a-4d2f-a608-6db88f10bdcc_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32360">
                      <gml:posList srsDimension="3">456632.2079 5428832.2429 133.38999999999996 456629.5468 5428832.1645 132.05 456627.5115 5428832.0999 131.04 456629.8694 5428831.3318 131.9948 456633.3148 5428830.2093 133.38999999999996 456632.2079 5428832.2429 133.38999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_8e7060a7-a891-4180-bb83-db3e0b7ff04b_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>1.57</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>8e7060a7-a891-4180-bb83-db3e0b7ff04b</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3229">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_8e7060a7-a891-4180-bb83-db3e0b7ff04b_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32361">
                      <gml:posList srsDimension="3">456630.4765 5428833.7367 112.44 456629.5468 5428832.1645 112.44 456627.5115 5428832.0999 112.44 456630.4765 5428833.7367 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_46ad65d8-e5a1-4e07-b0b9-340fe37f2490_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>1.821</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>59.564</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.05</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>300.597</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>46ad65d8-e5a1-4e07-b0b9-340fe37f2490</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3230">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_46ad65d8-e5a1-4e07-b0b9-340fe37f2490_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32362">
                      <gml:posList srsDimension="3">456630.4765 5428833.7367 132.05 456627.5115 5428832.0999 131.04 456629.5468 5428832.1645 132.05 456630.4765 5428833.7367 132.05</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_309dc7bf-ddd9-4912-8005-df9c6e8e4c3c_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.898</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>309dc7bf-ddd9-4912-8005-df9c6e8e4c3c</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3231">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_309dc7bf-ddd9-4912-8005-df9c6e8e4c3c_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32363">
                      <gml:posList srsDimension="3">456632.2079 5428832.2429 112.44 456629.5468 5428832.1645 112.44 456630.4765 5428833.7367 112.44 456631.16439999995 5428834.1162 112.44 456632.2079 5428832.2429 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_9ab3ff09-99ff-4985-8163-9c4e2eeeeb91_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>3.706</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>51.443</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>132.05</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>268.108</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>9ab3ff09-99ff-4985-8163-9c4e2eeeeb91</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3232">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_9ab3ff09-99ff-4985-8163-9c4e2eeeeb91_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32364">
                      <gml:posList srsDimension="3">456630.4765 5428833.7367 132.05 456629.5468 5428832.1645 132.05 456632.2079 5428832.2429 133.38999999999996 456631.16439999995 5428834.1162 133.38999999999996 456630.4765 5428833.7367 132.05</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_5239dec5-948f-4fc2-8683-b1c8d0041bf1_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>8.155</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>5239dec5-948f-4fc2-8683-b1c8d0041bf1</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3233">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_5239dec5-948f-4fc2-8683-b1c8d0041bf1_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32365">
                      <gml:posList srsDimension="3">456631.6173 5428827.275 112.44 456628.59 5428825.82 112.44 456627.6021 5428827.616 112.44 456629.4661 5428829.8078 112.44 456631.6173 5428827.275 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_409e34da-e4b0-46e2-b67c-98c72aea7aab_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>8.813</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>67.716</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>234.052</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>409e34da-e4b0-46e2-b67c-98c72aea7aab</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3234">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_409e34da-e4b0-46e2-b67c-98c72aea7aab_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32366">
                      <gml:posList srsDimension="3">456627.6021 5428827.616 131.04 456628.59 5428825.82 131.04 456631.6173 5428827.275 132.29 456629.4661 5428829.8078 132.29 456627.6021 5428827.616 131.04</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_868548bc-0fcf-41cf-8432-abbe1d4652bc_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>3.99</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>868548bc-0fcf-41cf-8432-abbe1d4652bc</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3235">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_868548bc-0fcf-41cf-8432-abbe1d4652bc_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32367">
                      <gml:posList srsDimension="3">456629.4661 5428829.8078 112.44 456627.6021 5428827.616 112.44 456625.5197 5428826.6152 112.44 456624.2666 5428826.9397 112.44 456629.4661 5428829.8078 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_b667b818-cf83-41bb-858e-7912d8286ded_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>6.518</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>37.747</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>152.018</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>b667b818-cf83-41bb-858e-7912d8286ded</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854964_3236">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_b667b818-cf83-41bb-858e-7912d8286ded_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32368">
                      <gml:posList srsDimension="3">456625.5197 5428826.6152 131.04 456627.6021 5428827.616 131.04 456629.4661 5428829.8078 132.29 456624.2666 5428826.9397 132.29 456625.5197 5428826.6152 131.04</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_29730b94-cabe-4843-baee-853db1a1de5b_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.364</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>29730b94-cabe-4843-baee-853db1a1de5b</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3237">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_29730b94-cabe-4843-baee-853db1a1de5b_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32369">
                      <gml:posList srsDimension="3">456625.5197 5428826.6152 112.44 456627.6021 5428827.616 112.44 456628.59 5428825.82 112.44 456625.5197 5428826.6152 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_cdddac81-dc42-4714-9c58-85b2add1ab35_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.1</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>35.213</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>154.331</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>cdddac81-dc42-4714-9c58-85b2add1ab35</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3238">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_cdddac81-dc42-4714-9c58-85b2add1ab35_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32370">
                      <gml:posList srsDimension="3">456625.5197 5428826.6152 131.04 456628.59 5428825.82 128.13999999999996 456627.6021 5428827.616 131.04 456625.5197 5428826.6152 131.04</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_66bab9aa-b3c6-425e-88d0-47555023333e_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>21.138</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>66bab9aa-b3c6-425e-88d0-47555023333e</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3239">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_66bab9aa-b3c6-425e-88d0-47555023333e_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32371">
                      <gml:posList srsDimension="3">456608.7410999999 5428818.5617 112.44 456611.8282 5428820.3007 112.44 456614.522 5428821.8181 112.44 456615.4644 5428819.5115 112.44 456610.22579999996 5428814.9233 112.44 456608.7410999999 5428818.5617 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_6cedc2f4-2942-4616-90aa-57b08763a26e_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>22.786</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>68.074</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.69</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>67.793</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>6cedc2f4-2942-4616-90aa-57b08763a26e</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3240">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_6cedc2f4-2942-4616-90aa-57b08763a26e_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32372">
                      <gml:posList srsDimension="3">456615.4644 5428819.5115 131.04 456614.55369999993 5428821.7406 131.04 456614.522 5428821.8181 131.04 456611.8282 5428820.3007 132.2749 456608.7410999999 5428818.5617 133.69 456610.22579999996 5428814.9233 133.69 456615.4644 5428819.5115 131.04</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_90572a47-1003-419d-bb37-dc92ef413f3c_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>1.677</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>90572a47-1003-419d-bb37-dc92ef413f3c</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3241">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_90572a47-1003-419d-bb37-dc92ef413f3c_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32373">
                      <gml:posList srsDimension="3">456608.7410999999 5428818.5617 112.44 456614.1611 5428822.7015 112.44 456611.8282 5428820.3007 112.44 456608.7410999999 5428818.5617 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_09c5c1b3-83c8-42d0-8923-051150f6bdb7_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.882</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>35.594</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.69</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>134.178</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>09c5c1b3-83c8-42d0-8923-051150f6bdb7</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3242">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_09c5c1b3-83c8-42d0-8923-051150f6bdb7_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32374">
                      <gml:posList srsDimension="3">456614.1611 5428822.7015 132.29 456608.7410999999 5428818.5617 133.69 456611.8282 5428820.3007 132.29 456614.1611 5428822.7015 132.29</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_ee9e63b9-a799-4529-943f-92bef2233939_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>1.464</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>ee9e63b9-a799-4529-943f-92bef2233939</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3243">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_ee9e63b9-a799-4529-943f-92bef2233939_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32375">
                      <gml:posList srsDimension="3">456614.522 5428821.8181 112.44 456611.8282 5428820.3007 112.44 456614.1611 5428822.7015 112.44 456614.522 5428821.8181 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_94a7216a-b8f5-48d3-aa58-2ca309b45f9c_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.553</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>34.976</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.04</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>134.178</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>94a7216a-b8f5-48d3-aa58-2ca309b45f9c</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3244">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_94a7216a-b8f5-48d3-aa58-2ca309b45f9c_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32376">
                      <gml:posList srsDimension="3">456611.8282 5428820.3007 132.29 456614.522 5428821.8181 131.04 456614.1611 5428822.7015 132.29 456611.8282 5428820.3007 132.29</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_19966186-46e0-4afb-9e22-30ef27bfb130_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>5.655</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>19966186-46e0-4afb-9e22-30ef27bfb130</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3245">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_19966186-46e0-4afb-9e22-30ef27bfb130_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32377">
                      <gml:posList srsDimension="3">456582.23089999997 5428807.7026 112.44 456578.1299 5428805.2633 112.44 456577.69 5428806.6 112.44 456581.914 5428808.673 112.44 456582.23089999997 5428807.7026 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_54754a1f-ee99-4402-afc3-6f21e3a5a667_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>8.521</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>41.583</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>251.839</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>54754a1f-ee99-4402-afc3-6f21e3a5a667</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3246">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_54754a1f-ee99-4402-afc3-6f21e3a5a667_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32378">
                      <gml:posList srsDimension="3">456577.69 5428806.6 128.13999999999996 456578.1299 5428805.2633 128.13999999999996 456582.23089999997 5428807.7026 133.38999999999996 456581.914 5428808.673 133.38999999999996 456577.69 5428806.6 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_05a051c8-194f-4a49-989d-0262055aa8cc_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>11.438</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>05a051c8-194f-4a49-989d-0262055aa8cc</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3247">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_05a051c8-194f-4a49-989d-0262055aa8cc_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32379">
                      <gml:posList srsDimension="3">456583.0687 5428805.138 112.44 456578.823 5428803.1571 112.44 456578.1299 5428805.2633 112.44 456582.23089999997 5428807.7026 112.44 456583.0687 5428805.138 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_037b4e83-df88-40d2-90a8-65c3dea14e2a_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>17.243</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>41.557</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>251.853</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>037b4e83-df88-40d2-90a8-65c3dea14e2a</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3248">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_037b4e83-df88-40d2-90a8-65c3dea14e2a_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32380">
                      <gml:posList srsDimension="3">456578.1299 5428805.2633 128.13999999999996 456578.823 5428803.1571 128.13999999999996 456583.0687 5428805.138 133.38999999999996 456582.23089999997 5428807.7026 133.38999999999996 456578.1299 5428805.2633 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_8cb8ec12-bbd2-458c-bad4-301de74c676e_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>102.559</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>8cb8ec12-bbd2-458c-bad4-301de74c676e</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3249">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_8cb8ec12-bbd2-458c-bad4-301de74c676e_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32381">
                      <gml:posList srsDimension="3">456589.2651 5428786.1694 112.44 456586.44 5428780.01 112.44 456578.823 5428803.1571 112.44 456583.0687 5428805.138 112.44 456589.2651 5428786.1694 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_f2bf61da-036f-4961-ba29-ed55329b7039_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>155.098</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>41.396</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>251.841</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>f2bf61da-036f-4961-ba29-ed55329b7039</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3250">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_f2bf61da-036f-4961-ba29-ed55329b7039_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32382">
                      <gml:posList srsDimension="3">456578.823 5428803.1571 128.13999999999996 456586.44 5428780.01 128.13999999999996 456589.2651 5428786.1694 133.38999999999996 456583.0687 5428805.138 133.38999999999996 456578.823 5428803.1571 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_a122a64c-6789-412a-98f6-d884604f1dd0_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>14.196</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>a122a64c-6789-412a-98f6-d884604f1dd0</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3251">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_a122a64c-6789-412a-98f6-d884604f1dd0_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32383">
                      <gml:posList srsDimension="3">456601.4209 5428817.3359 112.44 456602.018 5428816.4788 112.44 456588.9797 5428810.3673 112.44 456588.6736999999 5428811.2864 112.44 456601.4209 5428817.3359 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_58199ec4-9f66-4875-a305-b13857417c1c_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>20.629</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>43.484</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>154.75</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>58199ec4-9f66-4875-a305-b13857417c1c</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3252">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_58199ec4-9f66-4875-a305-b13857417c1c_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32384">
                      <gml:posList srsDimension="3">456588.9797 5428810.3673 131.24 456602.018 5428816.4788 131.24 456601.4209 5428817.3359 132.29 456588.6736999999 5428811.2864 132.29 456588.9797 5428810.3673 131.24</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_3ed1cea7-f95e-4e77-9d55-12c794469801_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>38.077</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>3ed1cea7-f95e-4e77-9d55-12c794469801</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3253">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_3ed1cea7-f95e-4e77-9d55-12c794469801_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32385">
                      <gml:posList srsDimension="3">456602.018 5428816.4788 112.44 456603.61 5428814.35 112.44 456599.2348 5428812.3229 112.44 456592.457 5428809.1827 112.44 456589.7786 5428807.9418 112.44 456588.9797 5428810.3673 112.44 456602.018 5428816.4788 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_9f122a76-e78c-4e98-8bb4-8736406c811f_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>56.87</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>42.032</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>155.017</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>9f122a76-e78c-4e98-8bb4-8736406c811f</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3254">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_9f122a76-e78c-4e98-8bb4-8736406c811f_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32386">
                      <gml:posList srsDimension="3">456589.7786 5428807.9418 128.38999999999996 456592.457 5428809.1827 128.38999999999996 456599.2348 5428812.3229 128.38999999999996 456603.61 5428814.35 128.38999999999996 456602.018 5428816.4788 131.24 456588.9797 5428810.3673 131.24 456589.7786 5428807.9418 128.38999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_93c31d16-7324-4767-8577-83ca28515c97_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>9.257</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>93c31d16-7324-4767-8577-83ca28515c97</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3255">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_93c31d16-7324-4767-8577-83ca28515c97_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32387">
                      <gml:posList srsDimension="3">456583.0687 5428805.138 112.44 456585.9969 5428807.7285 112.44 456588.9797 5428810.3673 112.44 456589.7786 5428807.9418 112.44 456583.0687 5428805.138 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_efc49de3-46d5-4ce5-9bbf-f1fe4a797085_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>9.656</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>73.483</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>71.767</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>efc49de3-46d5-4ce5-9bbf-f1fe4a797085</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3256">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_efc49de3-46d5-4ce5-9bbf-f1fe4a797085_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32388">
                      <gml:posList srsDimension="3">456588.9797 5428810.3673 131.24 456585.9969 5428807.7285 132.3249 456583.0687 5428805.138 133.38999999999996 456589.7786 5428807.9418 131.24 456588.9797 5428810.3673 131.24</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_65b01ffc-c112-4769-b056-5d2c49dbc300_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>7.734</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>65b01ffc-c112-4769-b056-5d2c49dbc300</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3257">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_65b01ffc-c112-4769-b056-5d2c49dbc300_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32389">
                      <gml:posList srsDimension="3">456582.23089999997 5428807.7026 112.44 456584.3918 5428809.2544 112.44 456585.9969 5428807.7285 112.44 456583.0687 5428805.138 112.44 456582.23089999997 5428807.7026 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_5a45c296-0190-41ac-9b15-b7b6bc2872d0_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>8.166</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>71.283</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>59.154</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>5a45c296-0190-41ac-9b15-b7b6bc2872d0</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3258">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_5a45c296-0190-41ac-9b15-b7b6bc2872d0_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32390">
                      <gml:posList srsDimension="3">456585.9969 5428807.7285 132.29 456584.3918 5428809.2544 132.29 456582.23089999997 5428807.7026 133.38999999999996 456583.0687 5428805.138 133.38999999999996 456585.9969 5428807.7285 132.29</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_743fc0da-1a00-438c-8fde-287d8c6c10f1_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.279</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>743fc0da-1a00-438c-8fde-287d8c6c10f1</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3259">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_743fc0da-1a00-438c-8fde-287d8c6c10f1_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32391">
                      <gml:posList srsDimension="3">456584.3918 5428809.2544 112.44 456588.6736999999 5428811.2864 112.44 456588.9797 5428810.3673 112.44 456584.3918 5428809.2544 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_389ba21e-73dd-4d67-a8d0-1284f29f2d23_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>3.374</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>42.482</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>154.613</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>389ba21e-73dd-4d67-a8d0-1284f29f2d23</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3260">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_389ba21e-73dd-4d67-a8d0-1284f29f2d23_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32392">
                      <gml:posList srsDimension="3">456584.3918 5428809.2544 132.29 456588.9797 5428810.3673 131.24 456588.6736999999 5428811.2864 132.29 456584.3918 5428809.2544 132.29</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_33ef9f57-1628-44fd-a797-3834ec9183ba_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.393</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>33ef9f57-1628-44fd-a797-3834ec9183ba</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3261">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_33ef9f57-1628-44fd-a797-3834ec9183ba_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32393">
                      <gml:posList srsDimension="3">456585.9969 5428807.7285 112.44 456584.3918 5428809.2544 112.44 456588.9797 5428810.3673 112.44 456585.9969 5428807.7285 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_e118ca77-9818-43a6-9442-9cd43b4c783d_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.545</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>75.177</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>131.24</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>43.551</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e118ca77-9818-43a6-9442-9cd43b4c783d</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3262">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_e118ca77-9818-43a6-9442-9cd43b4c783d_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32394">
                      <gml:posList srsDimension="3">456585.9969 5428807.7285 132.29 456588.9797 5428810.3673 131.24 456584.3918 5428809.2544 132.29 456585.9969 5428807.7285 132.29</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_3331561b-d589-4e6b-a0d7-a278d41a644c_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>190.478</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>3331561b-d589-4e6b-a0d7-a278d41a644c</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3263">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_3331561b-d589-4e6b-a0d7-a278d41a644c_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32395">
                      <gml:posList srsDimension="3">456608.79 5428802.07 112.44 456606.2984 5428800.997 112.44 456603.3926 5428799.7456 112.44 456601.3436 5428798.8632 112.44 456599.33139999997 5428797.9966 112.44 456596.6423 5428796.8385 112.44 456593.80489999993 5428795.6166 112.44 456589.7786 5428807.9418 112.44 456592.457 5428809.1827 112.44 456593.3099 5428807.1607 112.44 456600.0876999999 5428810.3009 112.44 456599.2348 5428812.3229 112.44 456603.61 5428814.35 112.44 456608.79 5428802.07 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_5698f1f9-ea4d-4818-8912-22e28fce4dde_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>190.478</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>90.0</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>113.32</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>113.32</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>-1.0</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>5698f1f9-ea4d-4818-8912-22e28fce4dde</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3264">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_5698f1f9-ea4d-4818-8912-22e28fce4dde_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32396">
                      <gml:posList srsDimension="3">456603.61 5428814.35 113.3203 456599.2348 5428812.3229 113.3203 456600.0876999999 5428810.3009 113.3203 456593.3099 5428807.1607 113.3203 456592.457 5428809.1827 113.3203 456589.7786 5428807.9418 113.3203 456593.80489999993 5428795.6166 113.3203 456596.6423 5428796.8385 113.3203 456599.33139999997 5428797.9966 113.3203 456601.3436 5428798.8632 113.3203 456603.3926 5428799.7456 113.3203 456606.2984 5428800.997 113.3203 456608.79 5428802.07 113.3203 456603.61 5428814.35 113.3203</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_cc174c83-09c7-47d0-9610-ea8f0128da99_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>16.383</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>cc174c83-09c7-47d0-9610-ea8f0128da99</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3265">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_cc174c83-09c7-47d0-9610-ea8f0128da99_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32397">
                      <gml:posList srsDimension="3">456600.0876999999 5428810.3009 112.44 456593.3099 5428807.1607 112.44 456592.457 5428809.1827 112.44 456599.2348 5428812.3229 112.44 456600.0876999999 5428810.3009 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_6768722b-a033-4984-8ec9-8aa0934a1726_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>16.383</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>90.0</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>114.22</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>114.22</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>-1.0</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>6768722b-a033-4984-8ec9-8aa0934a1726</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3266">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_6768722b-a033-4984-8ec9-8aa0934a1726_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32398">
                      <gml:posList srsDimension="3">456599.2348 5428812.3229 114.22 456592.457 5428809.1827 114.22 456593.3099 5428807.1607 114.22 456600.0876999999 5428810.3009 114.22 456599.2348 5428812.3229 114.22</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_f0090abb-6728-41e9-bada-39ac5ff36505_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>27.514</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>f0090abb-6728-41e9-bada-39ac5ff36505</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3267">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_f0090abb-6728-41e9-bada-39ac5ff36505_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32399">
                      <gml:posList srsDimension="3">456575.5022 5428828.3009 112.44 456581.5521 5428824.7129 112.44 456582.7531 5428821.9319 112.44 456578.1882 5428820.0784 112.44 456575.5022 5428828.3009 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_b83fdbfc-4b24-4a86-9349-bd43cbd83890_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>36.653</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>48.65</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>70.544</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>b83fdbfc-4b24-4a86-9349-bd43cbd83890</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3268">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_b83fdbfc-4b24-4a86-9349-bd43cbd83890_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32400">
                      <gml:posList srsDimension="3">456582.7531 5428821.9319 128.13999999999996 456581.5521 5428824.7129 128.13999999999996 456575.5022 5428828.3009 132.29 456578.1882 5428820.0784 132.29 456582.7531 5428821.9319 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_f533ad0b-a209-4563-a3e1-96af18d7c8c7_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>9.582</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>f533ad0b-a209-4563-a3e1-96af18d7c8c7</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3269">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_f533ad0b-a209-4563-a3e1-96af18d7c8c7_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32401">
                      <gml:posList srsDimension="3">456574.8388 5428830.3316 112.44 456580.6194 5428826.8726 112.44 456581.5521 5428824.7129 112.44 456575.5022 5428828.3009 112.44 456574.8388 5428830.3316 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_68afa420-50d0-463c-8140-1c2da58dc6e6_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>13.356</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>45.841</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>69.148</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>68afa420-50d0-463c-8140-1c2da58dc6e6</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3270">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_68afa420-50d0-463c-8140-1c2da58dc6e6_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32402">
                      <gml:posList srsDimension="3">456581.5521 5428824.7129 128.13999999999996 456580.6194 5428826.8726 128.13999999999996 456574.8388 5428830.3316 132.29 456575.5022 5428828.3009 132.29 456581.5521 5428824.7129 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_d385b3b6-b289-4050-9da7-3157cf351500_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>11.756</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>d385b3b6-b289-4050-9da7-3157cf351500</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3271">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_d385b3b6-b289-4050-9da7-3157cf351500_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32403">
                      <gml:posList srsDimension="3">456632.2079 5428832.2429 112.44 456636.7701 5428835.5363 112.44 456637.69 5428833.89 112.44 456633.3148 5428830.2093 112.44 456632.2079 5428832.2429 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_02778ef7-8c21-41d8-a467-7ae48bf37bff_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>16.119</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>46.83</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>61.155</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>02778ef7-8c21-41d8-a467-7ae48bf37bff</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3272">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_02778ef7-8c21-41d8-a467-7ae48bf37bff_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32404">
                      <gml:posList srsDimension="3">456637.69 5428833.89 128.13999999999996 456636.7701 5428835.5363 128.13999999999996 456632.2079 5428832.2429 133.38999999999996 456633.3148 5428830.2093 133.38999999999996 456637.69 5428833.89 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_193aec86-a730-431e-b1f5-46bae677fab2_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>10.659</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>193aec86-a730-431e-b1f5-46bae677fab2</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3273">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_193aec86-a730-431e-b1f5-46bae677fab2_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32405">
                      <gml:posList srsDimension="3">456631.16439999995 5428834.1162 112.44 456635.9551 5428836.995 112.44 456636.7701 5428835.5363 112.44 456632.2079 5428832.2429 112.44 456631.16439999995 5428834.1162 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_e898013b-0483-4819-b729-2635defacfca_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>14.626</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>46.783</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>60.848</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e898013b-0483-4819-b729-2635defacfca</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3274">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_e898013b-0483-4819-b729-2635defacfca_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32406">
                      <gml:posList srsDimension="3">456636.7701 5428835.5363 128.13999999999996 456635.9551 5428836.995 128.13999999999996 456631.16439999995 5428834.1162 133.38999999999996 456632.2079 5428832.2429 133.38999999999996 456636.7701 5428835.5363 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_defba096-4856-4756-b8bf-348d25f5f5ef_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>26.67</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>defba096-4856-4756-b8bf-348d25f5f5ef</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3275">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_defba096-4856-4756-b8bf-348d25f5f5ef_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32407">
                      <gml:posList srsDimension="3">456628.6228 5428838.6489 112.44 456633.8355 5428840.7884 112.44 456635.9551 5428836.995 112.44 456631.16439999995 5428834.1162 112.44 456628.6228 5428838.6489 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_b44dea41-b30f-4db7-b686-04179e27dfbb_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>36.588</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>46.797</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>60.759</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>b44dea41-b30f-4db7-b686-04179e27dfbb</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3276">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_b44dea41-b30f-4db7-b686-04179e27dfbb_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32408">
                      <gml:posList srsDimension="3">456635.9551 5428836.995 128.13999999999996 456633.8355 5428840.7884 128.13999999999996 456628.6228 5428838.6489 133.38999999999996 456631.16439999995 5428834.1162 133.38999999999996 456635.9551 5428836.995 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_d6209d8a-e9d9-41a9-a6b9-d551054a997d_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>15.073</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>d6209d8a-e9d9-41a9-a6b9-d551054a997d</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3277">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_d6209d8a-e9d9-41a9-a6b9-d551054a997d_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32409">
                      <gml:posList srsDimension="3">456627.48179999995 5428840.695 112.44 456632.34929999994 5428843.4484 112.44 456633.8355 5428840.7884 112.44 456628.6228 5428838.6489 112.44 456627.48179999995 5428840.695 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_a901b59c-b039-497b-8d0a-066c7bc962d1_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>20.673</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>46.813</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>60.827</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>a901b59c-b039-497b-8d0a-066c7bc962d1</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3278">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_a901b59c-b039-497b-8d0a-066c7bc962d1_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32410">
                      <gml:posList srsDimension="3">456633.8355 5428840.7884 128.13999999999996 456632.34929999994 5428843.4484 128.13999999999996 456627.48179999995 5428840.695 133.38999999999996 456628.6228 5428838.6489 133.38999999999996 456633.8355 5428840.7884 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_a9092e9e-b648-4fe0-a95b-90ec32b7cf67_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>20.931</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>a9092e9e-b648-4fe0-a95b-90ec32b7cf67</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3279">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_a9092e9e-b648-4fe0-a95b-90ec32b7cf67_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32411">
                      <gml:posList srsDimension="3">456592.7746 5428837.4027 112.44 456589.813 5428830.5434 112.44 456587.426 5428829.5903 112.44 456587.6987 5428835.2817 112.44 456592.7746 5428837.4027 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_26e14769-1fac-4622-b93f-7f705f670fcc_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>26.806</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>51.334</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>157.612</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>26e14769-1fac-4622-b93f-7f705f670fcc</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3280">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_26e14769-1fac-4622-b93f-7f705f670fcc_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32412">
                      <gml:posList srsDimension="3">456587.426 5428829.5903 128.13999999999996 456589.813 5428830.5434 128.13999999999996 456592.7746 5428837.4027 132.29 456587.6987 5428835.2817 132.29 456587.426 5428829.5903 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_343c71d7-1f35-4c9e-8a26-97878179456a_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>21.923</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>343c71d7-1f35-4c9e-8a26-97878179456a</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3281">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_343c71d7-1f35-4c9e-8a26-97878179456a_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32413">
                      <gml:posList srsDimension="3">456587.6987 5428835.2817 112.44 456587.426 5428829.5903 112.44 456584.30939999997 5428828.345999999 112.44 456582.9609 5428833.39 112.44 456587.6987 5428835.2817 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_a146f547-1728-4a45-9d68-69d4b27c1eb3_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>28.082</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>51.324</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>158.235</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>a146f547-1728-4a45-9d68-69d4b27c1eb3</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3282">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_a146f547-1728-4a45-9d68-69d4b27c1eb3_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32414">
                      <gml:posList srsDimension="3">456584.30939999997 5428828.345999999 128.13999999999996 456587.426 5428829.5903 128.13999999999996 456587.6987 5428835.2817 132.29 456582.9609 5428833.39 132.29 456584.30939999997 5428828.345999999 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_236c927b-9b94-49c0-8016-21912cb678b0_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>13.668</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>236c927b-9b94-49c0-8016-21912cb678b0</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3283">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_236c927b-9b94-49c0-8016-21912cb678b0_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32415">
                      <gml:posList srsDimension="3">456578.1917 5428831.5801 112.44 456582.1204 5428827.4719 112.44 456580.6194 5428826.8726 112.44 456574.8388 5428830.3316 112.44 456578.1917 5428831.5801 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_3651a99a-5de1-4cd2-8a23-0b6199545952_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>17.406</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>51.745</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>159.159</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>3651a99a-5de1-4cd2-8a23-0b6199545952</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3284">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_3651a99a-5de1-4cd2-8a23-0b6199545952_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748908_32416">
                      <gml:posList srsDimension="3">456580.6194 5428826.8726 128.13999999999996 456582.1204 5428827.4719 128.13999999999996 456578.1917 5428831.5801 132.29 456574.8388 5428830.3316 132.29 456580.6194 5428826.8726 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vW8_a341da3e-3598-42a2-a16f-c57e74745ef8_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>19.462</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>a341da3e-3598-42a2-a16f-c57e74745ef8</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3285">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_a341da3e-3598-42a2-a16f-c57e74745ef8_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32417">
                      <gml:posList srsDimension="3">456582.9609 5428833.39 112.44 456584.30939999997 5428828.345999999 112.44 456582.1204 5428827.4719 112.44 456578.1917 5428831.5801 112.44 456582.9609 5428833.39 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_0fef8ce4-7669-49a6-ae1e-1e656faa5c98_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>24.864</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>51.51</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.29</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>158.907</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>0fef8ce4-7669-49a6-ae1e-1e656faa5c98</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3286">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_0fef8ce4-7669-49a6-ae1e-1e656faa5c98_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32418">
                      <gml:posList srsDimension="3">456582.1204 5428827.4719 128.13999999999996 456584.30939999997 5428828.345999999 128.13999999999996 456582.9609 5428833.39 132.29 456578.1917 5428831.5801 132.29 456582.1204 5428827.4719 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_c32c66d6-2807-45f4-a4d2-d67528994761_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>99.651</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>c32c66d6-2807-45f4-a4d2-d67528994761</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3287">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_c32c66d6-2807-45f4-a4d2-d67528994761_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32419">
                      <gml:posList srsDimension="3">456568.92 5428833.26 112.44 456568.92 5428833.26 128.13999999999996 456574.87 5428835.47 128.13999999999996 456574.87 5428835.47 112.44 456568.92 5428833.26 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_4dedb1ed-56bb-4a53-a7dd-bdddffeeb39c_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>93.366</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>4dedb1ed-56bb-4a53-a7dd-bdddffeeb39c</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3288">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_4dedb1ed-56bb-4a53-a7dd-bdddffeeb39c_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32420">
                      <gml:posList srsDimension="3">456574.87 5428835.47 112.44 456574.87 5428835.47 128.13999999999996 456580.42999999993 5428837.58 128.13999999999996 456580.42999999993 5428837.58 112.44 456574.87 5428835.47 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_0745513b-2cb4-4d26-a20e-8ddfbb366e2c_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>93.148</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>0745513b-2cb4-4d26-a20e-8ddfbb366e2c</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3289">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_0745513b-2cb4-4d26-a20e-8ddfbb366e2c_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32421">
                      <gml:posList srsDimension="3">456580.42999999993 5428837.58 112.44 456580.42999999993 5428837.58 128.13999999999996 456585.94 5428839.78 128.13999999999996 456585.94 5428839.78 112.44 456580.42999999993 5428837.58 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_2e992f79-e3ad-432e-9fd5-47cf712ffc1e_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>95.287</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>2e992f79-e3ad-432e-9fd5-47cf712ffc1e</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3290">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_2e992f79-e3ad-432e-9fd5-47cf712ffc1e_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32422">
                      <gml:posList srsDimension="3">456585.94 5428839.78 112.44 456585.94 5428839.78 128.13999999999996 456591.54 5428842.119999999 128.13999999999996 456591.54 5428842.119999999 112.44 456585.94 5428839.78 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_e0527bee-27b3-473f-b200-289ac97c7eb6_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>95.29</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e0527bee-27b3-473f-b200-289ac97c7eb6</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3291">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_e0527bee-27b3-473f-b200-289ac97c7eb6_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32423">
                      <gml:posList srsDimension="3">456602.56999999995 5428847.05 112.44 456602.56999999995 5428847.05 128.13999999999996 456608.04 5428849.68 128.13999999999996 456608.04 5428849.68 112.44 456602.56999999995 5428847.05 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_7e0df6ea-b6bc-4841-aabb-056f9216aaf9_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>92.279</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>7e0df6ea-b6bc-4841-aabb-056f9216aaf9</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3292">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_7e0df6ea-b6bc-4841-aabb-056f9216aaf9_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32424">
                      <gml:posList srsDimension="3">456608.04 5428849.68 112.44 456608.04 5428849.68 128.13999999999996 456613.35 5428852.2 128.13999999999996 456613.35 5428852.2 112.44 456608.04 5428849.68 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_85a43622-f8d0-4fd5-be27-e92c731c8fd9_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>94.668</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>85a43622-f8d0-4fd5-be27-e92c731c8fd9</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3293">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_85a43622-f8d0-4fd5-be27-e92c731c8fd9_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32425">
                      <gml:posList srsDimension="3">456597.12 5428844.469999999 112.44 456597.12 5428844.469999999 128.13999999999996 456602.56999999995 5428847.05 128.13999999999996 456602.56999999995 5428847.05 112.44 456597.12 5428844.469999999 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_1c79002a-2662-44d9-85df-f1086856b1ee_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>95.058</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>1c79002a-2662-44d9-85df-f1086856b1ee</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3294">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_1c79002a-2662-44d9-85df-f1086856b1ee_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32426">
                      <gml:posList srsDimension="3">456591.54 5428842.119999999 112.44 456591.54 5428842.119999999 128.13999999999996 456597.12 5428844.469999999 128.13999999999996 456597.12 5428844.469999999 112.44 456591.54 5428842.119999999 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_04015ac1-0ea3-4bc0-b975-2f043a7323e0_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>93.314</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>04015ac1-0ea3-4bc0-b975-2f043a7323e0</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3295">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_04015ac1-0ea3-4bc0-b975-2f043a7323e0_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32427">
                      <gml:posList srsDimension="3">456613.35 5428852.2 112.44 456613.35 5428852.2 128.13999999999996 456618.65 5428854.89 128.13999999999996 456618.65 5428854.89 112.44 456613.35 5428852.2 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_37589b65-5d36-4857-ac98-2eccc305fd55_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>99.996</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>37589b65-5d36-4857-ac98-2eccc305fd55</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3296">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_37589b65-5d36-4857-ac98-2eccc305fd55_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32428">
                      <gml:posList srsDimension="3">456618.65 5428854.89 112.44 456618.65 5428854.89 128.13999999999996 456624.3 5428857.83 128.13999999999996 456624.3 5428857.83 112.44 456618.65 5428854.89 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_1ef82076-2bf8-4175-a830-46ed23db1eb9_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>40.701</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>1ef82076-2bf8-4175-a830-46ed23db1eb9</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3297">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_1ef82076-2bf8-4175-a830-46ed23db1eb9_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32429">
                      <gml:posList srsDimension="3">456624.3 5428857.83 112.44 456624.3 5428857.83 128.13999999999996 456625.57 5428855.57 128.13999999999996 456625.57 5428855.57 112.44 456624.3 5428857.83 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_7a0bc6f9-f0fb-4f12-9e72-8b7be9a39c8f_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>42.002</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>7a0bc6f9-f0fb-4f12-9e72-8b7be9a39c8f</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3298">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_7a0bc6f9-f0fb-4f12-9e72-8b7be9a39c8f_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32430">
                      <gml:posList srsDimension="3">456569.76 5428830.72 112.44 456569.76 5428830.72 128.13999999999996 456568.92 5428833.26 128.13999999999996 456568.92 5428833.26 112.44 456569.76 5428830.72 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_de190afa-dd55-48be-8dae-2ab1877a851e_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>107.101</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>de190afa-dd55-48be-8dae-2ab1877a851e</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3299">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_de190afa-dd55-48be-8dae-2ab1877a851e_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32431">
                      <gml:posList srsDimension="3">456592.88 5428782.26 112.44 456592.88 5428782.26 128.13999999999996 456586.44 5428780.01 128.13999999999996 456586.44 5428780.01 112.44 456592.88 5428782.26 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_20c54bc4-8310-4d4a-b48b-afe783d7414a_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>79.897</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>20c54bc4-8310-4d4a-b48b-afe783d7414a</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3300">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_20c54bc4-8310-4d4a-b48b-afe783d7414a_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32432">
                      <gml:posList srsDimension="3">456597.64 5428784.06 112.44 456597.64 5428784.06 128.13999999999996 456592.88 5428782.26 128.13999999999996 456592.88 5428782.26 112.44 456597.64 5428784.06 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_d91055e9-7799-4830-8444-492ad36d4100_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>120.524</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>d91055e9-7799-4830-8444-492ad36d4100</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854965_3301">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_d91055e9-7799-4830-8444-492ad36d4100_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32433">
                      <gml:posList srsDimension="3">456604.76 5428786.93 112.44 456604.76 5428786.93 128.13999999999996 456597.64 5428784.06 128.13999999999996 456597.64 5428784.06 112.44 456604.76 5428786.93 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_824bb9bc-6433-4cae-a224-fcd870e4cb43_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>41.896</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>824bb9bc-6433-4cae-a224-fcd870e4cb43</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3302">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_824bb9bc-6433-4cae-a224-fcd870e4cb43_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32434">
                      <gml:posList srsDimension="3">456607.23 5428787.94 112.44 456607.23 5428787.94 128.13999999999996 456604.76 5428786.93 128.13999999999996 456604.76 5428786.93 112.44 456607.23 5428787.94 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_af6459cd-a97d-4fc2-a036-07e6d162f836_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>23.178</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>af6459cd-a97d-4fc2-a036-07e6d162f836</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3303">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_af6459cd-a97d-4fc2-a036-07e6d162f836_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32435">
                      <gml:posList srsDimension="3">456608.5999999999 5428788.489999999 112.44 456608.5999999999 5428788.489999999 128.13999999999996 456607.23 5428787.94 128.13999999999996 456607.23 5428787.94 112.44 456608.5999999999 5428788.489999999 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_5b8cff76-b6cd-4945-8492-043582195706_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.754</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>5b8cff76-b6cd-4945-8492-043582195706</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3304">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_5b8cff76-b6cd-4945-8492-043582195706_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32436">
                      <gml:posList srsDimension="3">456608.5999999999 5428788.489999999 128.13999999999996 456607.0267 5428793.2613 133.69 456607.0267 5428793.2613 133.38999999999996 456608.5999999999 5428788.489999999 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_670badc8-253a-4a4d-9a23-4acf4c008ca4_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>191.991</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>670badc8-253a-4a4d-9a23-4acf4c008ca4</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3305">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_670badc8-253a-4a4d-9a23-4acf4c008ca4_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32437">
                      <gml:posList srsDimension="3">456619.7183 5428793.5816 112.44 456619.7183 5428793.5816 128.13999999999996 456608.5999999999 5428788.489999999 128.13999999999996 456608.5999999999 5428788.489999999 112.44 456619.7183 5428793.5816 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_4bbc9c82-477c-464d-86d2-07dfd4d960b1_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>195.135</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>4bbc9c82-477c-464d-86d2-07dfd4d960b1</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3306">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_4bbc9c82-477c-464d-86d2-07dfd4d960b1_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32438">
                      <gml:posList srsDimension="3">456630.86 5428799.09 112.44 456630.86 5428799.09 128.13999999999996 456619.7183 5428793.5816 128.13999999999996 456619.7183 5428793.5816 112.44 456630.86 5428799.09 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_dc06072d-a854-44a5-903d-8dd39efb1a99_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.835</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>dc06072d-a854-44a5-903d-8dd39efb1a99</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3307">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_dc06072d-a854-44a5-903d-8dd39efb1a99_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32439">
                      <gml:posList srsDimension="3">456626.4639 5428802.505 133.38999999999996 456626.4639 5428802.505 133.69 456630.86 5428799.09 128.13999999999996 456626.4639 5428802.505 133.38999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_e57a3726-ffac-4f8b-b0aa-a5e73651f9d3_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.424</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e57a3726-ffac-4f8b-b0aa-a5e73651f9d3</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3308">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_e57a3726-ffac-4f8b-b0aa-a5e73651f9d3_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32440">
                      <gml:posList srsDimension="3">456631.11 5428799.22 112.44 456631.11 5428799.22 128.13999999999996 456630.86 5428799.09 128.13999999999996 456630.86 5428799.09 112.44 456631.11 5428799.22 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_c5c4ff19-58de-42f0-92e6-80fe05d70637_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>47.604</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>c5c4ff19-58de-42f0-92e6-80fe05d70637</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3309">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_c5c4ff19-58de-42f0-92e6-80fe05d70637_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32441">
                      <gml:posList srsDimension="3">456633.82 5428800.58 112.44 456633.82 5428800.58 128.13999999999996 456631.11 5428799.22 128.13999999999996 456631.11 5428799.22 112.44 456633.82 5428800.58 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_2a9d7e94-4a57-471d-8cf7-01032b2f68c9_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>107.566</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>2a9d7e94-4a57-471d-8cf7-01032b2f68c9</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3310">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_2a9d7e94-4a57-471d-8cf7-01032b2f68c9_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32442">
                      <gml:posList srsDimension="3">456639.94 5428803.66 112.44 456639.94 5428803.66 128.13999999999996 456633.82 5428800.58 128.13999999999996 456633.82 5428800.58 112.44 456639.94 5428803.66 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_ecedbb4e-8971-4150-b4b3-faa906e40c75_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>110.252</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>ecedbb4e-8971-4150-b4b3-faa906e40c75</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3311">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_ecedbb4e-8971-4150-b4b3-faa906e40c75_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32443">
                      <gml:posList srsDimension="3">456646.09 5428807.05 112.44 456646.09 5428807.05 128.13999999999996 456639.94 5428803.66 128.13999999999996 456639.94 5428803.66 112.44 456646.09 5428807.05 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_e497bd3a-d561-46c9-93f6-40f6785b9b47_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>90.766</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e497bd3a-d561-46c9-93f6-40f6785b9b47</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3312">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_e497bd3a-d561-46c9-93f6-40f6785b9b47_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32444">
                      <gml:posList srsDimension="3">456651.12 5428809.899999999 112.44 456651.12 5428809.899999999 128.13999999999996 456646.09 5428807.05 128.13999999999996 456646.09 5428807.05 112.44 456651.12 5428809.899999999 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_1c2cc50c-3250-4adc-be29-61fec1ab9e0b_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>206.457</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>1c2cc50c-3250-4adc-be29-61fec1ab9e0b</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3313">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_1c2cc50c-3250-4adc-be29-61fec1ab9e0b_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32445">
                      <gml:posList srsDimension="3">456573.8671 5428818.2277 112.44 456573.8671 5428818.2277 128.13999999999996 456569.76 5428830.72 128.13999999999996 456569.76 5428830.72 112.44 456573.8671 5428818.2277 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_18338a57-8737-4902-a558-2e1991d94fe2_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.585</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>18338a57-8737-4902-a558-2e1991d94fe2</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3314">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_18338a57-8737-4902-a558-2e1991d94fe2_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32446">
                      <gml:posList srsDimension="3">456573.8671 5428818.2277 128.13999999999996 456578.1882 5428820.0784 133.38999999999996 456578.1882 5428820.0784 132.29 456573.8671 5428818.2277 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_b78bae04-c0e5-45c4-b23d-553e953f848e_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>192.168</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>b78bae04-c0e5-45c4-b23d-553e953f848e</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3315">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_b78bae04-c0e5-45c4-b23d-553e953f848e_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32447">
                      <gml:posList srsDimension="3">456577.69 5428806.6 112.44 456577.69 5428806.6 128.13999999999996 456573.8671 5428818.2277 128.13999999999996 456573.8671 5428818.2277 112.44 456577.69 5428806.6 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_c6bfc394-6276-413b-9a46-d4b2f0d8d577_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>50.388</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>c6bfc394-6276-413b-9a46-d4b2f0d8d577</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3316">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_c6bfc394-6276-413b-9a46-d4b2f0d8d577_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32448">
                      <gml:posList srsDimension="3">456591.9467 5428825.6027 115.71 456591.9467 5428825.6027 120.8 456582.7531 5428821.9319 120.8 456582.7531 5428821.9319 115.71 456591.9467 5428825.6027 115.71</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_971b9919-2cf6-4b91-9976-24fbd4543871_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>20.246</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>971b9919-2cf6-4b91-9976-24fbd4543871</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3317">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_971b9919-2cf6-4b91-9976-24fbd4543871_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32449">
                      <gml:posList srsDimension="3">456582.7531 5428821.9319 115.71 456582.7531 5428821.9319 120.8 456582.7531 5428821.9319 128.13999999999996 456583.2618 5428820.3846 128.13999999999996 456583.2618 5428820.3846 115.71 456582.7531 5428821.9319 115.71</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_52d19005-5883-49b4-a667-927c2a38342a_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.71</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>52d19005-5883-49b4-a667-927c2a38342a</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3318">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_52d19005-5883-49b4-a667-927c2a38342a_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32450">
                      <gml:posList srsDimension="3">456578.1882 5428820.0784 132.29 456578.1882 5428820.0784 133.38999999999996 456582.7531 5428821.9319 128.13999999999996 456578.1882 5428820.0784 132.29</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_17d66976-9aeb-434c-83e7-5f0a863f34a2_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>14.343</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>17d66976-9aeb-434c-83e7-5f0a863f34a2</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3319">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_17d66976-9aeb-434c-83e7-5f0a863f34a2_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32451">
                      <gml:posList srsDimension="3">456584.89239999995 5428815.424999999 115.71 456584.89239999995 5428815.424999999 128.13999999999996 456585.2528 5428814.3288 128.13999999999996 456585.2528 5428814.3288 115.71 456584.89239999995 5428815.424999999 115.71</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_88d6bb0b-395f-428c-95c5-3df4f990136d_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>131.995</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>88d6bb0b-395f-428c-95c5-3df4f990136d</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3320">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_88d6bb0b-395f-428c-95c5-3df4f990136d_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32452">
                      <gml:posList srsDimension="3">456585.2528 5428814.3288 115.71 456585.2528 5428814.3288 128.13999999999996 456595.1147999999 5428818.2665 128.13999999999996 456595.1147999999 5428818.2665 115.71 456585.2528 5428814.3288 115.71</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_99b77784-0b45-44ee-bef1-32c3f8509cd4_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.06</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>99b77784-0b45-44ee-bef1-32c3f8509cd4</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3321">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_99b77784-0b45-44ee-bef1-32c3f8509cd4_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32453">
                      <gml:posList srsDimension="3">456582.87619999994 5428809.1175 132.269 456582.87619999994 5428809.1175 132.29 456585.2528 5428814.3288 128.13999999999996 456582.87619999994 5428809.1175 132.269</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_6719e9e3-35fc-47d4-99da-3ef5b2337647_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.016</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>6719e9e3-35fc-47d4-99da-3ef5b2337647</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3322">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_6719e9e3-35fc-47d4-99da-3ef5b2337647_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32454">
                      <gml:posList srsDimension="3">456582.23089999997 5428807.7026 133.38999999999996 456582.87619999994 5428809.1175 132.29 456582.87619999994 5428809.1175 132.269 456582.23089999997 5428807.7026 133.38999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_efa5b7cc-cb3b-46bb-895a-6dafccfd963a_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.023</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>efa5b7cc-cb3b-46bb-895a-6dafccfd963a</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3323">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_efa5b7cc-cb3b-46bb-895a-6dafccfd963a_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32455">
                      <gml:posList srsDimension="3">456582.99979999993 5428817.886899999 129.2374 456582.99979999993 5428817.886899999 129.29 456582.99979999993 5428817.886899999 130.61 456580.2617 5428816.7937 132.3756 456582.99979999993 5428817.886899999 129.2374</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_e97762c6-c6e9-4535-8948-04fcb48c2709_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.939</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e97762c6-c6e9-4535-8948-04fcb48c2709</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3324">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_e97762c6-c6e9-4535-8948-04fcb48c2709_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32456">
                      <gml:posList srsDimension="3">456582.3043 5428820.0023 129.29 456582.3043 5428820.0023 130.61 456582.99979999993 5428817.886899999 130.61 456582.99979999993 5428817.886899999 129.29 456582.3043 5428820.0023 129.29</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_bb75a7a4-6613-4199-9c16-1c867abed685_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.017</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>bb75a7a4-6613-4199-9c16-1c867abed685</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3325">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_bb75a7a4-6613-4199-9c16-1c867abed685_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32457">
                      <gml:posList srsDimension="3">456579.5734 5428818.912 132.3709 456582.3043 5428820.0023 130.61 456582.3043 5428820.0023 129.29 456582.3043 5428820.0023 129.2384 456579.5734 5428818.912 132.3709</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_880508d9-1c29-4271-a11a-c74c76609b94_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.027</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>880508d9-1c29-4271-a11a-c74c76609b94</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3326">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_880508d9-1c29-4271-a11a-c74c76609b94_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32458">
                      <gml:posList srsDimension="3">456583.9573 5428818.2692 128.13999999999996 456582.99979999993 5428817.886899999 129.29 456582.99979999993 5428817.886899999 129.2374 456583.9573 5428818.2692 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_aae6fc47-08cd-48c5-8248-2e6c307773a4_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>27.679</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>aae6fc47-08cd-48c5-8248-2e6c307773a4</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3327">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_aae6fc47-08cd-48c5-8248-2e6c307773a4_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32459">
                      <gml:posList srsDimension="3">456583.2618 5428820.3846 115.71 456583.2618 5428820.3846 128.13999999999996 456583.9573 5428818.2692 128.13999999999996 456583.9573 5428818.2692 115.71 456583.2618 5428820.3846 115.71</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_dc4728b8-41b3-4f62-aceb-2ac756632b65_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.027</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>dc4728b8-41b3-4f62-aceb-2ac756632b65</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3328">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_dc4728b8-41b3-4f62-aceb-2ac756632b65_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32460">
                      <gml:posList srsDimension="3">456582.3043 5428820.0023 129.2384 456582.3043 5428820.0023 129.29 456583.2618 5428820.3846 128.13999999999996 456582.3043 5428820.0023 129.2384</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_1fd1576c-03fa-4c7d-a37a-71142c6a435c_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.037</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>1fd1576c-03fa-4c7d-a37a-71142c6a435c</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3329">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_1fd1576c-03fa-4c7d-a37a-71142c6a435c_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32461">
                      <gml:posList srsDimension="3">456583.943 5428815.0179 129.2361 456583.943 5428815.0179 129.29 456583.943 5428815.0179 130.61 456581.2181 5428813.8492 132.38219999999998 456583.943 5428815.0179 129.2361</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_9b6784ad-91ed-40c9-ae1f-dd88f5446ae4_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.367</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>9b6784ad-91ed-40c9-ae1f-dd88f5446ae4</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3330">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_9b6784ad-91ed-40c9-ae1f-dd88f5446ae4_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32462">
                      <gml:posList srsDimension="3">456583.38289999997 5428816.7215 129.29 456583.38289999997 5428816.7215 130.61 456583.943 5428815.0179 130.61 456583.943 5428815.0179 129.29 456583.38289999997 5428816.7215 129.29</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_68532b54-d04c-4a13-b234-270390cfccad_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.031</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>68532b54-d04c-4a13-b234-270390cfccad</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3331">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_68532b54-d04c-4a13-b234-270390cfccad_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32463">
                      <gml:posList srsDimension="3">456580.6638 5428815.5554 132.3784 456583.38289999997 5428816.7215 130.61 456583.38289999997 5428816.7215 129.29 456583.38289999997 5428816.7215 129.2368 456580.6638 5428815.5554 132.3784</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_823c1d5c-266d-40e1-884b-c453816c414c_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.028</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>823c1d5c-266d-40e1-884b-c453816c414c</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3332">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_823c1d5c-266d-40e1-884b-c453816c414c_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32464">
                      <gml:posList srsDimension="3">456584.89239999995 5428815.424999999 128.13999999999996 456583.943 5428815.0179 129.29 456583.943 5428815.0179 129.2361 456584.89239999995 5428815.424999999 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_e4aeb48a-0519-4a97-9cb9-2d80ced93f02_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>22.292</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e4aeb48a-0519-4a97-9cb9-2d80ced93f02</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3333">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_e4aeb48a-0519-4a97-9cb9-2d80ced93f02_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32465">
                      <gml:posList srsDimension="3">456584.3323 5428817.1287 115.71 456584.3323 5428817.1287 128.13999999999996 456584.89239999995 5428815.424999999 128.13999999999996 456584.89239999995 5428815.424999999 115.71 456584.3323 5428817.1287 115.71</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_c7c8d32e-32fe-4252-9bdb-110b90d1a8b0_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.027</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>c7c8d32e-32fe-4252-9bdb-110b90d1a8b0</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3334">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_c7c8d32e-32fe-4252-9bdb-110b90d1a8b0_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32466">
                      <gml:posList srsDimension="3">456583.38289999997 5428816.7215 129.2368 456583.38289999997 5428816.7215 129.29 456584.3323 5428817.1287 128.13999999999996 456583.38289999997 5428816.7215 129.2368</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_0a4dca11-8de0-445c-bdd5-814d7d441a52_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>14.923</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>0a4dca11-8de0-445c-bdd5-814d7d441a52</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3335">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_0a4dca11-8de0-445c-bdd5-814d7d441a52_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32467">
                      <gml:posList srsDimension="3">456583.9573 5428818.2692 115.71 456583.9573 5428818.2692 128.13999999999996 456584.3323 5428817.1287 128.13999999999996 456584.3323 5428817.1287 115.71 456583.9573 5428818.2692 115.71</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_5d164179-5514-4dad-9022-cc9b1a8d965b_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>55.36</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>5d164179-5514-4dad-9022-cc9b1a8d965b</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3336">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_5d164179-5514-4dad-9022-cc9b1a8d965b_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32468">
                      <gml:posList srsDimension="3">456593.80489999993 5428795.6166 113.3203 456593.80489999993 5428795.6166 131.24 456596.6423 5428796.8385 131.24 456596.6423 5428796.8385 113.3203 456593.80489999993 5428795.6166 113.3203</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_551345b0-5fd5-49a9-be37-7968db46fe35_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>52.467</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>551345b0-5fd5-49a9-be37-7968db46fe35</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3337">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_551345b0-5fd5-49a9-be37-7968db46fe35_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32469">
                      <gml:posList srsDimension="3">456596.6423 5428796.8385 113.3203 456596.6423 5428796.8385 131.24 456599.33139999997 5428797.9966 131.24 456599.33139999997 5428797.9966 113.3203 456596.6423 5428796.8385 113.3203</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_bf386a2b-58a1-471c-a8c1-8cb32eadbd0e_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>39.26</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>bf386a2b-58a1-471c-a8c1-8cb32eadbd0e</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3338">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_bf386a2b-58a1-471c-a8c1-8cb32eadbd0e_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32470">
                      <gml:posList srsDimension="3">456599.33139999997 5428797.9966 113.3203 456599.33139999997 5428797.9966 131.24 456601.3436 5428798.8632 131.24 456601.3436 5428798.8632 113.3203 456599.33139999997 5428797.9966 113.3203</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_77715a4c-8f0c-4f9d-8858-953bf8df5e71_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>39.978</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>77715a4c-8f0c-4f9d-8858-953bf8df5e71</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3339">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_77715a4c-8f0c-4f9d-8858-953bf8df5e71_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32471">
                      <gml:posList srsDimension="3">456601.3436 5428798.8632 113.3203 456601.3436 5428798.8632 131.24 456603.3926 5428799.7456 131.24 456603.3926 5428799.7456 113.3203 456601.3436 5428798.8632 113.3203</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_0e97e52d-075d-4268-a49c-5b8d09dd0ed8_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>56.694</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>0e97e52d-075d-4268-a49c-5b8d09dd0ed8</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3340">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_0e97e52d-075d-4268-a49c-5b8d09dd0ed8_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32472">
                      <gml:posList srsDimension="3">456603.3926 5428799.7456 113.3203 456603.3926 5428799.7456 131.24 456606.2984 5428800.997 131.24 456606.2984 5428800.997 113.3203 456603.3926 5428799.7456 113.3203</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_57e6418d-85af-4fe9-8724-27c0a111ba90_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>48.613</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>57e6418d-85af-4fe9-8724-27c0a111ba90</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3341">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_57e6418d-85af-4fe9-8724-27c0a111ba90_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32473">
                      <gml:posList srsDimension="3">456606.2984 5428800.997 113.3203 456606.2984 5428800.997 131.24 456608.79 5428802.07 131.24 456608.79 5428802.07 113.3203 456606.2984 5428800.997 113.3203</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_ffabcd84-cc24-4437-8a9d-2e35d92a4bcd_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>1.165</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>ffabcd84-cc24-4437-8a9d-2e35d92a4bcd</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3342">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_ffabcd84-cc24-4437-8a9d-2e35d92a4bcd_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32474">
                      <gml:posList srsDimension="3">456607.0267 5428793.2613 133.38999999999996 456607.0267 5428793.2613 133.69 456606.2984 5428800.997 131.24 456607.0267 5428793.2613 133.38999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_ac86f380-e556-4e54-9cb0-7bd0dec8ee66_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>232.35</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>ac86f380-e556-4e54-9cb0-7bd0dec8ee66</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3343">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_ac86f380-e556-4e54-9cb0-7bd0dec8ee66_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32475">
                      <gml:posList srsDimension="3">456589.7786 5428807.9418 113.3203 456589.7786 5428807.9418 128.38999999999996 456589.7786 5428807.9418 131.24 456593.80489999993 5428795.6166 131.24 456593.80489999993 5428795.6166 113.3203 456589.7786 5428807.9418 113.3203</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_d30d2a65-bf06-432f-8b67-2b6a8162ef1e_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>238.831</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>d30d2a65-bf06-432f-8b67-2b6a8162ef1e</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3344">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_d30d2a65-bf06-432f-8b67-2b6a8162ef1e_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32476">
                      <gml:posList srsDimension="3">456608.79 5428802.07 113.3203 456608.79 5428802.07 131.24 456603.61 5428814.35 131.24 456603.61 5428814.35 128.38999999999996 456603.61 5428814.35 113.3203 456608.79 5428802.07 113.3203</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_0ffed9ae-31f1-487c-8a48-4a002e7e23e3_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>1.115</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>0ffed9ae-31f1-487c-8a48-4a002e7e23e3</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3345">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_0ffed9ae-31f1-487c-8a48-4a002e7e23e3_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32477">
                      <gml:posList srsDimension="3">456621.7304 5428808.2399 131.04 456626.4639 5428802.505 133.69 456626.4639 5428802.505 133.38999999999996 456621.7304 5428808.2399 131.04</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_f1f886e6-4ec0-4ec3-8172-1e7389065e3c_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>28.878</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>f1f886e6-4ec0-4ec3-8172-1e7389065e3c</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3346">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_f1f886e6-4ec0-4ec3-8172-1e7389065e3c_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32478">
                      <gml:posList srsDimension="3">456620.37 5428807.49 112.45 456620.37 5428807.49 131.04 456621.7304 5428808.2399 131.04 456621.7304 5428808.2399 112.45 456620.37 5428807.49 112.45</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_3e635c93-daac-4e24-b903-b404b7509752_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>241.37</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>3e635c93-daac-4e24-b903-b404b7509752</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3347">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_3e635c93-daac-4e24-b903-b404b7509752_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32479">
                      <gml:posList srsDimension="3">456615.4644 5428819.5115 112.45 456615.4644 5428819.5115 128.13999999999996 456615.4644 5428819.5115 131.04 456620.37 5428807.49 131.04 456620.37 5428807.49 112.45 456615.4644 5428819.5115 112.45</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_2eeb9032-97ff-4250-8e30-d4cdd89ec2d4_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>216.785</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>2eeb9032-97ff-4250-8e30-d4cdd89ec2d4</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3348">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_2eeb9032-97ff-4250-8e30-d4cdd89ec2d4_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32480">
                      <gml:posList srsDimension="3">456599.012 5428835.027 112.453 456599.012 5428835.027 128.13999999999996 456604.65 5428822.41 128.13999999999996 456604.65 5428822.41 112.453 456599.012 5428835.027 112.453</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_114bb944-5d51-46ce-a7d8-61f82c3ab175_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>3.788</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>114bb944-5d51-46ce-a7d8-61f82c3ab175</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3349">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_114bb944-5d51-46ce-a7d8-61f82c3ab175_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32481">
                      <gml:posList srsDimension="3">456603.61 5428814.35 128.38999999999996 456603.61 5428814.35 131.24 456602.018 5428816.4788 131.24 456603.61 5428814.35 128.38999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_01886ded-be08-491b-b1bd-0ee8f0385b17_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>60.42</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>01886ded-be08-491b-b1bd-0ee8f0385b17</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3350">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_01886ded-be08-491b-b1bd-0ee8f0385b17_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32482">
                      <gml:posList srsDimension="3">456616.3072 5428843.2145 112.453 456616.3072 5428843.2145 128.13999999999996 456612.826 5428841.5665 128.13999999999996 456612.826 5428841.5665 112.453 456616.3072 5428843.2145 112.453</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_9ef820a7-ad16-4309-803b-e42db1b5b31c_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>87.743</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>9ef820a7-ad16-4309-803b-e42db1b5b31c</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3351">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_9ef820a7-ad16-4309-803b-e42db1b5b31c_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32483">
                      <gml:posList srsDimension="3">456612.826 5428841.5665 112.453 456612.826 5428841.5665 128.13999999999996 456607.7704999999 5428839.1732 128.13999999999996 456607.7704999999 5428839.1732 112.453 456612.826 5428841.5665 112.453</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_8f21b4d9-0e90-458e-8473-1ce91334cb98_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>73.408</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>8f21b4d9-0e90-458e-8473-1ce91334cb98</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3352">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_8f21b4d9-0e90-458e-8473-1ce91334cb98_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32484">
                      <gml:posList srsDimension="3">456603.2415 5428837.0293 112.453 456603.2415 5428837.0293 128.13999999999996 456599.012 5428835.027 128.13999999999996 456599.012 5428835.027 112.453 456603.2415 5428837.0293 112.453</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_d4da4a8e-8f39-4b61-beee-951107512676_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>78.604</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>d4da4a8e-8f39-4b61-beee-951107512676</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3353">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_d4da4a8e-8f39-4b61-beee-951107512676_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32485">
                      <gml:posList srsDimension="3">456607.7704999999 5428839.1732 112.453 456607.7704999999 5428839.1732 128.13999999999996 456603.2415 5428837.0293 128.13999999999996 456603.2415 5428837.0293 112.453 456607.7704999999 5428839.1732 112.453</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_833b7052-d974-4eee-b7a7-4449885a5239_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>63.483</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>833b7052-d974-4eee-b7a7-4449885a5239</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3354">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_833b7052-d974-4eee-b7a7-4449885a5239_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32486">
                      <gml:posList srsDimension="3">456617.83 5428840.16 112.44 456617.83 5428840.16 128.5 456617.83 5428840.16 131.04 456616.3072 5428843.2145 131.04 456616.3072 5428843.2145 128.13999999999996 456616.3072 5428843.2145 112.453 456616.3072 5428843.2145 112.44 456617.83 5428840.16 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_15f782e6-c93c-41ed-8007-2f152c16052d_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.11</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>15f782e6-c93c-41ed-8007-2f152c16052d</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3355">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_15f782e6-c93c-41ed-8007-2f152c16052d_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32487">
                      <gml:posList srsDimension="3">456616.3072 5428843.2145 128.13999999999996 456616.3072 5428843.2145 131.04 456617.9363 5428845.5344 129.37 456616.3072 5428843.2145 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_af95d389-de32-4c0f-b3c6-57ff032ee195_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.188</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>18.315</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>112.453</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>112.44</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>63.93</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>af95d389-de32-4c0f-b3c6-57ff032ee195</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3356">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_af95d389-de32-4c0f-b3c6-57ff032ee195_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32488">
                      <gml:posList srsDimension="3">456616.3072 5428843.2145 112.44 456616.3072 5428843.2145 112.453 456622.5273 5428830.4987 112.453 456622.5273 5428830.4987 112.443 456621.0 5428833.64 112.44 456616.3072 5428843.2145 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_bf95e0ca-49a0-464e-ac6c-ec9d0f472c4f_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>116.431</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>bf95e0ca-49a0-464e-ac6c-ec9d0f472c4f</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3357">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_bf95e0ca-49a0-464e-ac6c-ec9d0f472c4f_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32489">
                      <gml:posList srsDimension="3">456621.0 5428833.64 112.44 456621.0 5428833.64 128.5 456617.83 5428840.16 128.5 456617.83 5428840.16 112.44 456621.0 5428833.64 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_42aae18f-4c1b-4c5b-bcdd-a81c404afeeb_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.93</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>42aae18f-4c1b-4c5b-bcdd-a81c404afeeb</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3358">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_42aae18f-4c1b-4c5b-bcdd-a81c404afeeb_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32490">
                      <gml:posList srsDimension="3">456627.5115 5428832.0999 131.04 456622.9635 5428829.6015 131.04 456622.9635 5428829.6015 129.1852 456622.9635 5428829.6015 129.13999999999996 456627.5115 5428832.0999 131.04</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_44fdde52-068e-41ec-821d-406fe79f0523_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>9.885</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>44fdde52-068e-41ec-821d-406fe79f0523</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3359">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_44fdde52-068e-41ec-821d-406fe79f0523_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32491">
                      <gml:posList srsDimension="3">456622.9386 5428838.109 128.5 456622.9386 5428838.109 130.74 456620.726 5428841.9274 130.74 456620.726 5428841.9274 128.5 456622.9386 5428838.109 128.5</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_5f81707d-fd53-4a36-baa4-e28df54ef880_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>218.05</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>5f81707d-fd53-4a36-baa4-e28df54ef880</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3360">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_5f81707d-fd53-4a36-baa4-e28df54ef880_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32492">
                      <gml:posList srsDimension="3">456625.57 5428855.57 112.44 456625.57 5428855.57 128.13999999999996 456632.34929999994 5428843.4484 128.13999999999996 456632.34929999994 5428843.4484 112.44 456625.57 5428855.57 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_3640c569-eaeb-4832-bc46-3157dbef0324_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>5.991</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>3640c569-eaeb-4832-bc46-3157dbef0324</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854966_3361">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_3640c569-eaeb-4832-bc46-3157dbef0324_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32493">
                      <gml:posList srsDimension="3">456624.2795 5428835.7949 128.5 456624.2795 5428835.7949 130.74 456622.9386 5428838.109 130.74 456622.9386 5428838.109 128.5 456624.2795 5428835.7949 128.5</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_315eae8a-9965-46bb-9251-43a676b0d0d7_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.431</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>315eae8a-9965-46bb-9251-43a676b0d0d7</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3362">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_315eae8a-9965-46bb-9251-43a676b0d0d7_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32494">
                      <gml:posList srsDimension="3">456626.8013 5428840.307699999 132.05 456627.48179999995 5428840.695 133.38999999999996 456627.48179999995 5428840.695 132.29 456626.8013 5428840.307699999 132.05</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_188317f7-f416-4415-b379-7cdc1d6608ad_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>271.523</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>188317f7-f416-4415-b379-7cdc1d6608ad</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3363">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_188317f7-f416-4415-b379-7cdc1d6608ad_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32495">
                      <gml:posList srsDimension="3">456621.7304 5428808.2399 112.45 456621.7304 5428808.2399 131.04 456634.76 5428814.84 131.04 456634.76 5428814.84 112.45 456621.7304 5428808.2399 112.45</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_23aba20d-6513-4ff7-9f63-1a59503ef94b_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>164.082</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>23aba20d-6513-4ff7-9f63-1a59503ef94b</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3364">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_23aba20d-6513-4ff7-9f63-1a59503ef94b_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32496">
                      <gml:posList srsDimension="3">456612.99759999994 5428826.187 112.453 456612.99759999994 5428826.187 128.13999999999996 456622.5273 5428830.4987 128.13999999999996 456622.5273 5428830.4987 112.453 456612.99759999994 5428826.187 112.453</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_0c10f684-7d9e-4782-84d5-41830fe42dc0_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>143.729</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>0c10f684-7d9e-4782-84d5-41830fe42dc0</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3365">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_0c10f684-7d9e-4782-84d5-41830fe42dc0_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32497">
                      <gml:posList srsDimension="3">456604.65 5428822.41 112.453 456604.65 5428822.41 128.13999999999996 456612.99759999994 5428826.187 128.13999999999996 456612.99759999994 5428826.187 112.453 456604.65 5428822.41 112.453</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_be8ef207-296d-4d63-bb09-bc97b4a06485_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>228.492</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>be8ef207-296d-4d63-bb09-bc97b4a06485</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3366">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_be8ef207-296d-4d63-bb09-bc97b4a06485_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32498">
                      <gml:posList srsDimension="3">456628.59 5428825.82 112.45 456628.59 5428825.82 128.13999999999996 456615.4644 5428819.5115 128.13999999999996 456615.4644 5428819.5115 112.45 456628.59 5428825.82 112.45</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_00705c43-ee3a-47fa-aa66-2e4e6e805021_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>234.138</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>00705c43-ee3a-47fa-aa66-2e4e6e805021</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3367">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_00705c43-ee3a-47fa-aa66-2e4e6e805021_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32499">
                      <gml:posList srsDimension="3">456634.76 5428814.84 112.45 456634.76 5428814.84 131.04 456628.59 5428825.82 131.04 456628.59 5428825.82 128.13999999999996 456628.59 5428825.82 112.45 456634.76 5428814.84 112.45</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_ea5c84ba-096b-42bf-8b45-a62c8694a78a_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>403.849</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>ea5c84ba-096b-42bf-8b45-a62c8694a78a</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3368">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_ea5c84ba-096b-42bf-8b45-a62c8694a78a_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32500">
                      <gml:posList srsDimension="3">456638.5548 5428832.345099999 112.44 456638.5548 5428832.345099999 128.13999999999996 456651.12 5428809.899999999 128.13999999999996 456651.12 5428809.899999999 112.44 456638.5548 5428832.345099999 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_9431f38d-0094-45a5-a796-aab765bc99ad_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>27.797</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>9431f38d-0094-45a5-a796-aab765bc99ad</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3369">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_9431f38d-0094-45a5-a796-aab765bc99ad_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32501">
                      <gml:posList srsDimension="3">456637.69 5428833.89 112.44 456637.69 5428833.89 128.13999999999996 456638.5548 5428832.345099999 128.13999999999996 456638.5548 5428832.345099999 112.44 456637.69 5428833.89 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_e528ecbc-96de-4e2d-af75-292676895512_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.535</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e528ecbc-96de-4e2d-af75-292676895512</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3370">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_e528ecbc-96de-4e2d-af75-292676895512_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32502">
                      <gml:posList srsDimension="3">456629.8694 5428831.3318 131.9948 456629.8694 5428831.3318 132.29 456633.3148 5428830.2093 133.38999999999996 456629.8694 5428831.3318 131.9948</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_c45137bc-4fdc-4b54-a6ad-222e2a616a24_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.366</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>c45137bc-4fdc-4b54-a6ad-222e2a616a24</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3371">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_c45137bc-4fdc-4b54-a6ad-222e2a616a24_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32503">
                      <gml:posList srsDimension="3">456627.5115 5428832.0999 131.04 456629.8694 5428831.3318 132.29 456629.8694 5428831.3318 131.9948 456627.5115 5428832.0999 131.04</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_74c5af72-a7c9-4799-b6e6-caa58dd45fb7_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.972</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>74c5af72-a7c9-4799-b6e6-caa58dd45fb7</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3372">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_74c5af72-a7c9-4799-b6e6-caa58dd45fb7_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32504">
                      <gml:posList srsDimension="3">456628.59 5428825.82 128.13999999999996 456628.59 5428825.82 131.04 456627.6021 5428827.616 131.04 456628.59 5428825.82 128.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_36975099-517c-4ed5-bc49-550f2f3863ee_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>3.492</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>36975099-517c-4ed5-bc49-550f2f3863ee</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3373">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_36975099-517c-4ed5-bc49-550f2f3863ee_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32505">
                      <gml:posList srsDimension="3">456614.55369999993 5428821.7406 131.04 456615.4644 5428819.5115 131.04 456615.4644 5428819.5115 128.13999999999996 456614.55369999993 5428821.7406 131.04</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_259838d1-cab8-47b1-9fd8-ac5802492521_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.027</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>259838d1-cab8-47b1-9fd8-ac5802492521</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3374">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_259838d1-cab8-47b1-9fd8-ac5802492521_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32506">
                      <gml:posList srsDimension="3">456611.8282 5428820.3007 132.2749 456611.8282 5428820.3007 132.29 456608.7410999999 5428818.5617 133.69 456611.8282 5428820.3007 132.2749</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_23f5a73b-0991-47c4-a504-1838aafa4fdd_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.023</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>23f5a73b-0991-47c4-a504-1838aafa4fdd</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3375">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_23f5a73b-0991-47c4-a504-1838aafa4fdd_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32507">
                      <gml:posList srsDimension="3">456614.522 5428821.8181 131.04 456611.8282 5428820.3007 132.29 456611.8282 5428820.3007 132.2749 456614.522 5428821.8181 131.04</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_375a46d8-c017-446a-912c-b01178db0bbc_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>22.093</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>375a46d8-c017-446a-912c-b01178db0bbc</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3376">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_375a46d8-c017-446a-912c-b01178db0bbc_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32508">
                      <gml:posList srsDimension="3">456578.1299 5428805.2633 112.44 456578.1299 5428805.2633 128.13999999999996 456577.69 5428806.6 128.13999999999996 456577.69 5428806.6 112.44 456578.1299 5428805.2633 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_ee4d1024-778d-4a37-bf05-4da4ac760c33_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>34.812</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>ee4d1024-778d-4a37-bf05-4da4ac760c33</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3377">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_ee4d1024-778d-4a37-bf05-4da4ac760c33_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32509">
                      <gml:posList srsDimension="3">456578.823 5428803.1571 112.44 456578.823 5428803.1571 128.13999999999996 456578.1299 5428805.2633 128.13999999999996 456578.1299 5428805.2633 112.44 456578.823 5428803.1571 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_e96cea58-82de-4854-bf56-8e510f1c0655_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>382.58</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e96cea58-82de-4854-bf56-8e510f1c0655</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3378">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_e96cea58-82de-4854-bf56-8e510f1c0655_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32510">
                      <gml:posList srsDimension="3">456586.44 5428780.01 112.44 456586.44 5428780.01 128.13999999999996 456578.823 5428803.1571 128.13999999999996 456578.823 5428803.1571 112.44 456586.44 5428780.01 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_0809607c-6bd9-4f25-99c9-3dd1ae17628a_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>3.639</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>0809607c-6bd9-4f25-99c9-3dd1ae17628a</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3379">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_0809607c-6bd9-4f25-99c9-3dd1ae17628a_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32511">
                      <gml:posList srsDimension="3">456588.9797 5428810.3673 131.24 456589.7786 5428807.9418 131.24 456589.7786 5428807.9418 128.38999999999996 456588.9797 5428810.3673 131.24</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_06a73fc7-2962-4e9c-8ade-110dfa5089b4_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>1.974</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>06a73fc7-2962-4e9c-8ade-110dfa5089b4</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3380">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_06a73fc7-2962-4e9c-8ade-110dfa5089b4_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32512">
                      <gml:posList srsDimension="3">456593.3099 5428807.1607 113.3203 456593.3099 5428807.1607 114.22 456592.457 5428809.1827 114.22 456592.457 5428809.1827 113.3203 456593.3099 5428807.1607 113.3203</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_a088644d-de97-4b4f-b30e-d1b0ca8aa07e_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>6.721</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>a088644d-de97-4b4f-b30e-d1b0ca8aa07e</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3381">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_a088644d-de97-4b4f-b30e-d1b0ca8aa07e_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32513">
                      <gml:posList srsDimension="3">456600.0876999999 5428810.3009 113.3203 456600.0876999999 5428810.3009 114.22 456593.3099 5428807.1607 114.22 456593.3099 5428807.1607 113.3203 456600.0876999999 5428810.3009 113.3203</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_396668e2-172e-4d04-ae06-ae8dbfd30493_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>1.974</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>396668e2-172e-4d04-ae06-ae8dbfd30493</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3382">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_396668e2-172e-4d04-ae06-ae8dbfd30493_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32514">
                      <gml:posList srsDimension="3">456599.2348 5428812.3229 113.3203 456599.2348 5428812.3229 114.22 456600.0876999999 5428810.3009 114.22 456600.0876999999 5428810.3009 113.3203 456599.2348 5428812.3229 113.3203</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_f6a495a1-00e6-4d04-bb28-6fba81edd1bb_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>22.235</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>f6a495a1-00e6-4d04-bb28-6fba81edd1bb</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3383">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_f6a495a1-00e6-4d04-bb28-6fba81edd1bb_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32515">
                      <gml:posList srsDimension="3">456581.5521 5428824.7129 120.8 456581.5521 5428824.7129 128.13999999999996 456582.7531 5428821.9319 128.13999999999996 456582.7531 5428821.9319 120.8 456581.5521 5428824.7129 120.8</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_e8d3ea2c-bff8-4739-8499-44025cdfbbce_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>17.267</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e8d3ea2c-bff8-4739-8499-44025cdfbbce</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3384">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_e8d3ea2c-bff8-4739-8499-44025cdfbbce_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32516">
                      <gml:posList srsDimension="3">456580.6194 5428826.8726 120.8 456580.6194 5428826.8726 128.13999999999996 456581.5521 5428824.7129 128.13999999999996 456581.5521 5428824.7129 120.8 456580.6194 5428826.8726 120.8</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_587af7c0-3976-4576-8b4c-a8952d0a8b7d_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>29.608</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>587af7c0-3976-4576-8b4c-a8952d0a8b7d</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3385">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_587af7c0-3976-4576-8b4c-a8952d0a8b7d_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32517">
                      <gml:posList srsDimension="3">456636.7701 5428835.5363 112.44 456636.7701 5428835.5363 128.13999999999996 456637.69 5428833.89 128.13999999999996 456637.69 5428833.89 112.44 456636.7701 5428835.5363 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_73139682-4289-48c3-a3f5-b6fe1c1e647f_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>26.234</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>73139682-4289-48c3-a3f5-b6fe1c1e647f</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3386">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_73139682-4289-48c3-a3f5-b6fe1c1e647f_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32518">
                      <gml:posList srsDimension="3">456635.9551 5428836.995 112.44 456635.9551 5428836.995 128.13999999999996 456636.7701 5428835.5363 128.13999999999996 456636.7701 5428835.5363 112.44 456635.9551 5428836.995 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_a9457e5f-9f26-4cfe-a4a2-067ca0ad5f80_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>68.223</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>a9457e5f-9f26-4cfe-a4a2-067ca0ad5f80</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3387">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_a9457e5f-9f26-4cfe-a4a2-067ca0ad5f80_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32519">
                      <gml:posList srsDimension="3">456633.8355 5428840.7884 112.44 456633.8355 5428840.7884 128.13999999999996 456635.9551 5428836.995 128.13999999999996 456635.9551 5428836.995 112.44 456633.8355 5428840.7884 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_2a239756-4d3b-405a-8669-b451dfb7f3d0_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>47.838</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>2a239756-4d3b-405a-8669-b451dfb7f3d0</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3388">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_2a239756-4d3b-405a-8669-b451dfb7f3d0_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32520">
                      <gml:posList srsDimension="3">456632.34929999994 5428843.4484 112.44 456632.34929999994 5428843.4484 128.13999999999996 456633.8355 5428840.7884 128.13999999999996 456633.8355 5428840.7884 112.44 456632.34929999994 5428843.4484 112.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_e1529871-2342-4313-b90d-8ac1ed27654d_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>3.076</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e1529871-2342-4313-b90d-8ac1ed27654d</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3389">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_e1529871-2342-4313-b90d-8ac1ed27654d_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32521">
                      <gml:posList srsDimension="3">456627.48179999995 5428840.695 132.29 456627.48179999995 5428840.695 133.38999999999996 456632.34929999994 5428843.4484 128.13999999999996 456627.48179999995 5428840.695 132.29</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_a71d85f4-c54e-4a96-953a-d8ada1d128a7_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>18.866</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>a71d85f4-c54e-4a96-953a-d8ada1d128a7</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3390">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_a71d85f4-c54e-4a96-953a-d8ada1d128a7_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32522">
                      <gml:posList srsDimension="3">456589.813 5428830.5434 120.8 456589.813 5428830.5434 128.13999999999996 456587.426 5428829.5903 128.13999999999996 456587.426 5428829.5903 120.8 456589.813 5428830.5434 120.8</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_5b943fb6-a7db-4d62-8567-4119cc9c6217_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>24.632</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>5b943fb6-a7db-4d62-8567-4119cc9c6217</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3391">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_5b943fb6-a7db-4d62-8567-4119cc9c6217_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32523">
                      <gml:posList srsDimension="3">456587.426 5428829.5903 120.8 456587.426 5428829.5903 128.13999999999996 456584.30939999997 5428828.345999999 128.13999999999996 456584.30939999997 5428828.345999999 120.8 456587.426 5428829.5903 120.8</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_610047d9-2899-4dda-9d53-d569884d3785_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>11.863</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>610047d9-2899-4dda-9d53-d569884d3785</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3392">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_610047d9-2899-4dda-9d53-d569884d3785_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32524">
                      <gml:posList srsDimension="3">456582.1204 5428827.4719 120.8 456582.1204 5428827.4719 128.13999999999996 456580.6194 5428826.8726 128.13999999999996 456580.6194 5428826.8726 120.8 456582.1204 5428827.4719 120.8</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_ec795e87-e3af-4931-ac3a-bb0e9a4fd0c4_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>17.301</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>ec795e87-e3af-4931-ac3a-bb0e9a4fd0c4</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3393">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_ec795e87-e3af-4931-ac3a-bb0e9a4fd0c4_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32525">
                      <gml:posList srsDimension="3">456584.30939999997 5428828.345999999 120.8 456584.30939999997 5428828.345999999 128.13999999999996 456582.1204 5428827.4719 128.13999999999996 456582.1204 5428827.4719 120.8 456584.30939999997 5428828.345999999 120.8</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_7b85cb82-be19-4bfe-8e33-85dc74ed7771_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.023</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>-1.246</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>132.44</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>159.544</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>7b85cb82-be19-4bfe-8e33-85dc74ed7771</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3394">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_7b85cb82-be19-4bfe-8e33-85dc74ed7771_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32526">
                      <gml:posList srsDimension="3">456579.4663 5428818.8692 132.44 456579.4667 5428818.8681 132.4938 456578.6855 5428818.5559 133.38999999999996 456579.4663 5428818.8692 132.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_ffd538f2-ac96-4715-9f2a-ba11b04ecbec_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.019</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>1.14</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>132.44</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>337.984</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>ffd538f2-ac96-4715-9f2a-ba11b04ecbec</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3395">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_ffd538f2-ac96-4715-9f2a-ba11b04ecbec_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32527">
                      <gml:posList srsDimension="3">456580.3457 5428813.4739 133.38999999999996 456581.1295999999 5428813.8103 132.4848 456581.1292 5428813.8111 132.44 456580.3457 5428813.4739 133.38999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_d60cc642-0f91-41b3-af4a-a1e8857f30cb_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>99.329</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>d60cc642-0f91-41b3-af4a-a1e8857f30cb</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3396">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_d60cc642-0f91-41b3-af4a-a1e8857f30cb_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32528">
                      <gml:posList srsDimension="3">456595.1147999999 5428818.2665 115.71 456595.1147999999 5428818.2665 128.13999999999996 456591.9467 5428825.6027 128.13999999999996 456591.9467 5428825.6027 120.8 456591.9467 5428825.6027 115.71 456595.1147999999 5428818.2665 115.71</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_7cccf90f-1635-4ba2-ae21-dcd6654d6dbd_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>39.502</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>7cccf90f-1635-4ba2-ae21-dcd6654d6dbd</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3397">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_7cccf90f-1635-4ba2-ae21-dcd6654d6dbd_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32529">
                      <gml:posList srsDimension="3">456591.9467 5428825.6027 120.8 456591.9467 5428825.6027 128.13999999999996 456589.813 5428830.5434 128.13999999999996 456589.813 5428830.5434 120.8 456591.9467 5428825.6027 120.8</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_5ec93470-af0f-4ab2-84e9-014c6bf28421_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.02</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>-1.446</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>132.44</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>158.308</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>5ec93470-af0f-4ab2-84e9-014c6bf28421</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3398">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_5ec93470-af0f-4ab2-84e9-014c6bf28421_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32530">
                      <gml:posList srsDimension="3">456580.5691 5428815.5148 132.44 456580.5696 5428815.5137 132.4878 456579.7888 5428815.1786 133.38999999999996 456580.5691 5428815.5148 132.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_d837eb36-1fbb-4324-b41c-5f9e7531ceab_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.021</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>1.084</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>133.39</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>132.44</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>339.374</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>d837eb36-1fbb-4324-b41c-5f9e7531ceab</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3399">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_d837eb36-1fbb-4324-b41c-5f9e7531ceab_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32531">
                      <gml:posList srsDimension="3">456579.377 5428816.4392 133.38999999999996 456580.1621 5428816.7529 132.49009999999998 456580.16179999994 5428816.7538 132.44 456579.377 5428816.4392 133.38999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_9c8afc44-706e-4c08-b30e-cf532df63b33_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>8.617</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>9c8afc44-706e-4c08-b30e-cf532df63b33</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3400">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_9c8afc44-706e-4c08-b30e-cf532df63b33_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32532">
                      <gml:posList srsDimension="3">456620.726 5428841.9274 128.5 456620.726 5428841.9274 130.74 456620.726 5428841.9274 131.04 456617.83 5428840.16 131.04 456617.83 5428840.16 128.5 456620.726 5428841.9274 128.5</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_382c25b8-22c7-45bf-8ac0-ce57da6b7443_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.154</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>382c25b8-22c7-45bf-8ac0-ce57da6b7443</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3401">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_382c25b8-22c7-45bf-8ac0-ce57da6b7443_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32533">
                      <gml:posList srsDimension="3">456621.6011 5428842.4614 131.04 456620.726 5428841.9274 131.04 456620.726 5428841.9274 130.74 456621.6011 5428842.4614 131.04</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_0d5afc02-eda1-4d40-a8c0-e9076476c446_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>9.967</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>0d5afc02-eda1-4d40-a8c0-e9076476c446</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3402">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_0d5afc02-eda1-4d40-a8c0-e9076476c446_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32534">
                      <gml:posList srsDimension="3">456621.0 5428833.64 128.5 456621.0 5428833.64 131.04 456624.2795 5428835.7949 131.04 456624.2795 5428835.7949 130.74 456624.2795 5428835.7949 128.5 456621.0 5428833.64 128.5</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_eb1c91d1-0b12-44a7-8586-29258c29288e_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.153</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>eb1c91d1-0b12-44a7-8586-29258c29288e</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3403">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_eb1c91d1-0b12-44a7-8586-29258c29288e_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32535">
                      <gml:posList srsDimension="3">456624.2795 5428835.7949 130.74 456624.2795 5428835.7949 131.04 456625.1316 5428836.3549 131.04 456624.2795 5428835.7949 130.74</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_fd8b7f80-ce0f-4236-9f65-8f625a343312_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>64.963</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>fd8b7f80-ce0f-4236-9f65-8f625a343312</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3404">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_fd8b7f80-ce0f-4236-9f65-8f625a343312_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32536">
                      <gml:posList srsDimension="3">456622.5273 5428830.4987 112.443 456622.5273 5428830.4987 112.453 456622.5273 5428830.4987 128.13999999999996 456622.5273 5428830.4987 131.04 456621.0 5428833.64 131.04 456621.0 5428833.64 128.5 456621.0 5428833.64 112.44 456622.5273 5428830.4987 112.443</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_92181542-c83d-49a9-adf6-0491c3428735_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.372</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>92181542-c83d-49a9-adf6-0491c3428735</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3405">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_92181542-c83d-49a9-adf6-0491c3428735_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32537">
                      <gml:posList srsDimension="3">456622.9635 5428829.6015 129.1852 456622.9635 5428829.6015 131.04 456622.5273 5428830.4987 131.04 456622.5273 5428830.4987 128.13999999999996 456622.9635 5428829.6015 129.1852</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_a6f09829-0d22-4c7b-9949-e2cdc50304f6_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.041</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>a6f09829-0d22-4c7b-9949-e2cdc50304f6</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3406">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_a6f09829-0d22-4c7b-9949-e2cdc50304f6_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32538">
                      <gml:posList srsDimension="3">456621.6575 5428842.4958 131.04 456621.6575 5428842.4958 131.0593 456625.2474 5428844.6867 132.29 456621.6575 5428842.4958 131.04</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_02d9675a-c0bf-47d1-87d1-84085ac0c42b_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.001</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>02d9675a-c0bf-47d1-87d1-84085ac0c42b</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3407">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_02d9675a-c0bf-47d1-87d1-84085ac0c42b_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32539">
                      <gml:posList srsDimension="3">456621.6011 5428842.4614 131.04 456621.6575 5428842.4958 131.0593 456621.6575 5428842.4958 131.04 456621.6011 5428842.4614 131.04</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_79b9cab8-2b59-41ed-baa5-8ffc38323248_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.067</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>79b9cab8-2b59-41ed-baa5-8ffc38323248</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3408">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_79b9cab8-2b59-41ed-baa5-8ffc38323248_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32540">
                      <gml:posList srsDimension="3">456622.9635 5428829.6015 129.13999999999996 456622.9635 5428829.6015 129.1852 456624.2666 5428826.9397 132.29 456622.9635 5428829.6015 129.13999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_b9087054-1bda-4c59-9ca8-fe65665da106_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.004</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>b9087054-1bda-4c59-9ca8-fe65665da106</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3409">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_b9087054-1bda-4c59-9ca8-fe65665da106_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32541">
                      <gml:posList srsDimension="3">456614.55369999993 5428821.7406 131.04 456614.522 5428821.8181 131.1408 456614.522 5428821.8181 131.04 456614.55369999993 5428821.7406 131.04</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_f7b998a5-e9fd-49ae-8f20-b6066ffcfb88_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.048</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>f7b998a5-e9fd-49ae-8f20-b6066ffcfb88</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3410">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_f7b998a5-e9fd-49ae-8f20-b6066ffcfb88_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32542">
                      <gml:posList srsDimension="3">456614.522 5428821.8181 131.04 456614.522 5428821.8181 131.1408 456614.1611 5428822.7015 132.29 456614.522 5428821.8181 131.04</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_047eec5e-3d4a-4a2e-a568-6429f69752b1_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.031</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>047eec5e-3d4a-4a2e-a568-6429f69752b1</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3411">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_047eec5e-3d4a-4a2e-a568-6429f69752b1_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32543">
                      <gml:posList srsDimension="3">456624.2666 5428826.9397 132.29 456625.5197 5428826.6152 131.0872 456625.5197 5428826.6152 131.04 456624.2666 5428826.9397 132.29</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_e6904c9b-92a6-44e0-ae09-f1a55ff1ec5f_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.075</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e6904c9b-92a6-44e0-ae09-f1a55ff1ec5f</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3412">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_e6904c9b-92a6-44e0-ae09-f1a55ff1ec5f_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32544">
                      <gml:posList srsDimension="3">456625.5197 5428826.6152 131.04 456625.5197 5428826.6152 131.0872 456628.59 5428825.82 128.13999999999996 456625.5197 5428826.6152 131.04</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_fff7c408-3cec-4e35-86f2-5cf9201ee2f6_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>72.666</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>fff7c408-3cec-4e35-86f2-5cf9201ee2f6</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3413">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_fff7c408-3cec-4e35-86f2-5cf9201ee2f6_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32545">
                      <gml:posList srsDimension="3">456603.61 5428814.35 113.3203 456603.61 5428814.35 128.38999999999996 456599.2348 5428812.3229 128.38999999999996 456599.2348 5428812.3229 114.22 456599.2348 5428812.3229 113.3203 456603.61 5428814.35 113.3203</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_27531ac1-a82e-4b99-8858-00c4d7d10a7f_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.068</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>27531ac1-a82e-4b99-8858-00c4d7d10a7f</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3414">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_27531ac1-a82e-4b99-8858-00c4d7d10a7f_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32546">
                      <gml:posList srsDimension="3">456583.0687 5428805.138 133.38999999999996 456585.9969 5428807.7285 132.3249 456585.9969 5428807.7285 132.29 456583.0687 5428805.138 133.38999999999996</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_8614a8bb-1791-46b8-aae6-0797fc8f05bf_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.069</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>8614a8bb-1791-46b8-aae6-0797fc8f05bf</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3415">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_8614a8bb-1791-46b8-aae6-0797fc8f05bf_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32547">
                      <gml:posList srsDimension="3">456585.9969 5428807.7285 132.29 456585.9969 5428807.7285 132.3249 456588.9797 5428810.3673 131.24 456585.9969 5428807.7285 132.29</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_c47feab9-ea3b-4ed9-924d-5cd10c4ee8e5_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.003</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>-1.246</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.494</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>132.371</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>158.964</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>c47feab9-ea3b-4ed9-924d-5cd10c4ee8e5</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3416">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_c47feab9-ea3b-4ed9-924d-5cd10c4ee8e5_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32548">
                      <gml:posList srsDimension="3">456579.5734 5428818.912 132.3709 456579.4667 5428818.8681 132.4938 456579.4663 5428818.8692 132.44 456579.5734 5428818.912 132.3709</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_ec1692e3-84fc-4196-acb4-bb4eff3af3cc_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.002</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>1.141</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.485</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>132.382</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>337.483</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>ec1692e3-84fc-4196-acb4-bb4eff3af3cc</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3417">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_ec1692e3-84fc-4196-acb4-bb4eff3af3cc_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32549">
                      <gml:posList srsDimension="3">456581.1292 5428813.8111 132.44 456581.1295999999 5428813.8103 132.4848 456581.2181 5428813.8492 132.38219999999998 456581.1292 5428813.8111 132.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_369ae71a-ea6c-465e-8052-80a58a58a838_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.002</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>-1.447</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.488</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>132.378</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>157.659</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>369ae71a-ea6c-465e-8052-80a58a58a838</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3418">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_369ae71a-ea6c-465e-8052-80a58a58a838_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32550">
                      <gml:posList srsDimension="3">456580.6638 5428815.5554 132.3784 456580.5696 5428815.5137 132.4878 456580.5691 5428815.5148 132.44 456580.6638 5428815.5554 132.3784</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vW8_c6928bf7-814d-40cb-95fa-03b62393dee2_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.003</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>1.084</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>132.49</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>132.376</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>338.877</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>c6928bf7-814d-40cb-95fa-03b62393dee2</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3419">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_c6928bf7-814d-40cb-95fa-03b62393dee2_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32551">
                      <gml:posList srsDimension="3">456580.16179999994 5428816.7538 132.44 456580.1621 5428816.7529 132.49009999999998 456580.2617 5428816.7937 132.3756 456580.16179999994 5428816.7538 132.44</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_54d712c1-a76f-4260-b81b-db2eb9ddcff4_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>105.849</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>54d712c1-a76f-4260-b81b-db2eb9ddcff4</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3420">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_54d712c1-a76f-4260-b81b-db2eb9ddcff4_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32552">
                      <gml:posList srsDimension="3">456599.2348 5428812.3229 114.22 456599.2348 5428812.3229 128.38999999999996 456592.457 5428809.1827 128.38999999999996 456592.457 5428809.1827 114.22 456599.2348 5428812.3229 114.22</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vW8_6c825c1b-8bf9-4d07-a9e0-98f44de59aeb_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>44.484</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>6c825c1b-8bf9-4d07-a9e0-98f44de59aeb</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854967_3421">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vW8_6c825c1b-8bf9-4d07-a9e0-98f44de59aeb_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572941748909_32553">
                      <gml:posList srsDimension="3">456592.457 5428809.1827 113.3203 456592.457 5428809.1827 114.22 456592.457 5428809.1827 128.38999999999996 456589.7786 5428807.9418 128.38999999999996 456589.7786 5428807.9418 113.3203 456592.457 5428809.1827 113.3203</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:address xlink:href="#UUID_2099c54d-a1da-4aed-82ec-1590199462c7"/>
    </bldg:Building>
  </core:cityObjectMember>
</core:CityModel>