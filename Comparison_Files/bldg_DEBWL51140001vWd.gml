<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<core:CityModel xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:tex="http://www.opengis.net/citygml/texturedsurface/2.0" xmlns:core="http://www.opengis.net/citygml/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:frn="http://www.opengis.net/citygml/cityfurniture/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd http://www.opengis.net/citygml/appearance/2.0 http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/cityobjectgroup/2.0 http://schemas.opengis.net/citygml/cityobjectgroup/2.0/cityObjectGroup.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/cityfurniture/2.0 http://schemas.opengis.net/citygml/cityfurniture/2.0/cityFurniture.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd">
  <core:cityObjectMember>
    <bldg:Building gml:id="bldg_DEBWL51140001vWd">
      <gml:name/>
      <gml:boundedBy>
        <gml:Envelope srsName="urn:ogc:def:crs:EPSG::25832" srsDimension="3">
          <gml:lowerCorner>456484.33 5428532.34 114.7794</gml:lowerCorner>
          <gml:upperCorner>456520.12 5428609.42 131.8021</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <core:creationDate>2019-06-28</core:creationDate>
      <core:externalReference>
        <core:informationSystem>http://www.karlsruhe.de/b3/bauen/geodaten/3dgis</core:informationSystem>
        <core:externalObject>
          <core:uri>DEBWL51140001vWd</core:uri>
        </core:externalObject>
      </core:externalReference>
      <gen:doubleAttribute name="HoeheGrund">
        <gen:value>114.779</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="HoeheDach">
        <gen:value>131.802</gen:value>
      </gen:doubleAttribute>
      <gen:doubleAttribute name="NiedrigsteTraufeDesGebaeudes">
        <gen:value>115.01</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Dachtypen">
        <gen:value>PULTDACH;FLACHDACH;SATTELDACH;PULTDACH;PULTDACH</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Dachkodierungsliste">
        <gen:value>2100;1000;3100;2100;2100</gen:value>
      </gen:stringAttribute>
      <gen:doubleAttribute name="Volumen">
        <gen:value>23678.848</gen:value>
      </gen:doubleAttribute>
      <gen:stringAttribute name="Verfahren">
        <gen:value>BREC_2000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Objektart">
        <gen:value>Hauptgebaeude</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Strassenschluessel">
        <gen:value>04420</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Strassenname">
        <gen:value>Kreuzstraße</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Hausnummer">
        <gen:value>12</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="creationUser">
        <gen:value>Firma VCS</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Feinheit">
        <gen:value>Basis</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="AmtlicherGemeindeschluessel">
        <gen:value>08212000</gen:value>
      </gen:stringAttribute>
      <gen:intAttribute name="DatenquelleDachhoehe">
        <gen:value>1000</gen:value>
      </gen:intAttribute>
      <gen:intAttribute name="DatenquelleLage">
        <gen:value>1000</gen:value>
      </gen:intAttribute>
      <gen:intAttribute name="DatenquelleBodenhoehe">
        <gen:value>1100</gen:value>
      </gen:intAttribute>
      <bldg:function>31001_3010</bldg:function>
      <bldg:roofType>3100</bldg:roofType>
      <bldg:measuredHeight uom="urn:adv:uom:m">17.023</bldg:measuredHeight>
      <bldg:lod2Solid>
        <gml:Solid>
          <gml:exterior>
            <gml:CompositeSurface>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_d9bab671-645e-4cf4-939b-12708411c540_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_5b104f98-37b4-4911-9bac-4ec0a1511280_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_229de10b-d00a-4d22-8bbd-375bc2273402_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_7e4b179d-effe-45e5-837f-2a0f8a9bbc14_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_005b5b8a-76a5-44c5-abd1-044224096c69_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_aa1a68c2-95ff-424c-854d-e6a94267ee12_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_21a1ac3e-e270-4152-bbd2-6b1af7150340_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_6eeaf62d-0a83-4468-8c53-213e6967ee60_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_d7dca3ef-a0de-462e-888b-b7d03e953afc_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_a88df266-fb71-4173-b49f-6d9f35da3d38_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_84d22e0e-b3a6-4784-b757-748b830d5dda_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_0b4fda02-a88f-48ac-9052-603eb276f29b_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_b912427b-b68d-4161-acbc-72988b680f3d_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_0b06a9b8-a081-4dc6-8b84-34957ae116c6_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_fbad8000-bef4-4904-8435-d7a709d5403a_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_7a085f65-15df-49e8-bd54-ae3a04f92f89_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_36c8098d-61f6-4b16-af27-66058e513bbb_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_bc7071f3-55ff-4ced-a651-ccb1fc0b4fed_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_f942af93-1c3b-49cc-9b1d-9999e8881ac2_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_ea7efa7d-e827-49ab-9010-b63df9e0bf38_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_6c7c50b8-74b0-49ab-86ee-71ceb67b462a_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_3ca3e2a4-7efc-4a21-8794-546b21eb8ad3_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_7b3df0ca-98ed-4511-a004-3e618caf4db2_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_c66efb3e-b25b-4e0c-afab-9201f69c08e8_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_04423292-79b9-4834-9dd3-d3d8255b334d_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_dfe6480a-32f2-4d27-a0a2-bb1f9ca30f39_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_c582de0b-641d-4570-bc84-64ce0e4b59ae_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_bb71fb53-92d3-4cc3-b002-24d1fd393e9a_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_91cabe9d-4a6c-4bd5-9435-958f7b124635_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_84547eae-ac68-4c6f-9950-df3c19003dbe_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_e22c9e8a-e859-477d-82b6-f71baf77cad3_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_2a7555ff-59cf-4926-8795-c8d601aaedae_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_4a058aac-b05c-43ac-a207-9c521e6f0784_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_c95d23ce-6bed-430d-ae7a-89871a295ec1_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_e91d55dd-72dd-4601-8a09-fe49622190fd_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_80224a65-3c91-499b-9b42-daaf61d22180_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_80215475-3c6d-412d-a61b-a4ab0d63fc28_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_e221e38a-5a6a-4f7a-9e85-f92abb5a6297_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_dfcbad28-4c03-4fca-ba8c-d645118b4339_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_84de4ffe-4233-4d32-b50c-fd7bd4a47a9b_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_9dccb6d4-1730-4408-a2d7-ddec67823397_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_d050603c-3b44-4621-824e-9fcca1f9a0e0_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_6c905a43-0170-4195-b5fc-d704fd35c4f2_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_f306da72-9325-4eb0-899a-ead1579abbb6_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_16e6e85f-2401-4f6f-b494-418bc7fc37d0_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_168884f0-a651-41f0-ace7-b2fbde10518c_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_3120c040-cf45-4443-94c8-d62fb9a5d1a8_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_355ac522-adfb-43cf-9322-d8078125ff62_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_864c1e24-d019-4907-b642-f2b92745796a_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_98e4df3d-f7bc-4a90-8516-2bb72eab651b_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_409bfeda-ca2b-4daa-b967-8a829e9fa289_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_89da396c-80ad-42fb-a662-372c4351dfef_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_29d0aa9e-1592-4fd1-89d3-c5627dacb7c1_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_65843ec7-3703-429e-a7c4-56f9fb9e2768_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_18903252-cc34-4549-b5e4-2b174d65a793_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_c7462b43-07d9-4f8e-8761-6d18481339cd_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_bf89edc3-8d28-4f31-ba6d-4f70ebaad9fe_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_e196140f-eef5-4495-83fa-2154a0f7a756_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_248a8855-a721-456c-bc02-e66884300613_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_e4a848bf-2303-476e-953b-d39d840cc388_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_0c605b4b-4a39-4471-a5df-6b35d98fc98b_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_7baf9d7c-e034-4203-ab0f-fe3dec989e72_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_d7ecc8f0-eb74-49ad-bb48-196f97d531c9_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_edb20409-1b99-4cb6-bf45-f62e7ef70e5b_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_3ccae2ef-13da-44cf-a597-ec406257a8b1_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_19b240e3-066a-4ed0-b744-8c76165153ec_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_308dba52-07fd-408f-818a-54882557a788_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_cdf532ca-e7fe-447d-81ad-da2d9ca2d01f_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_8abac84d-e4f0-473c-b8f4-09561e544020_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_36a3241d-67f7-4b97-bcee-28054511d61d_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_a9091c9a-952a-4fb5-8210-ae3d6edfc08b_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_1e41c926-d1de-4c92-b52f-5934302b798d_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_47050ade-5dde-4ce9-965d-9778504373f8_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_4beae04c-d80c-4c07-96bd-6110e82d9d15_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_d8ad4044-ac3c-4335-80ec-95d4d956d2d8_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_3d2802e8-4856-4e44-a819-15fe60e6ecd5_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_c721551e-0a23-4e0b-9d6e-44731ef6a680_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_0462df14-e13f-40be-ba6c-54c2a8b12222_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_1b3737a2-ffbd-4322-b2a7-67c49f380504_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_c4f91d0c-0570-4021-aefc-e4c197e45cda_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_8498f146-0881-4cb8-805c-bbc5ad06f4e7_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_9b0f1eec-60d3-42a3-864c-92093c3df28a_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_8f35cb73-41f3-4a1b-8230-ca15f38ab4bd_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_9490334d-1abf-491d-bd26-317685209ef8_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_134a02a9-6ac4-4793-8c91-984d350f0d30_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_1daf13b1-8a48-41d4-83d3-2dd01bf5aae9_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_5d9a2ff7-2f8e-4ddd-a75b-aa2d00ea4dc3_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_e10686d8-04d1-410e-8cd4-8250b5588a61_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_87fb6bb8-5c2e-4b51-af40-5e2bb0358071_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_8b4644bc-eba1-4626-83fa-156e2b466fd7_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_e89c84c0-b799-4db4-a07c-af2eceb2a123_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_0534fc3d-7a63-448f-8d90-5816f951c6da_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_5b4ebb82-f28d-4fbc-94b5-54d3f183ac0a_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_372cb27c-2aee-4094-909f-5b0dff64a83f_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_a6890f89-76cd-43f2-b2ae-df7a67483b63_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_4c71c32c-3cee-49e3-8770-09d597cde81f_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_39e89252-8f30-4fc0-9d75-2580e7390358_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_fe11ce09-5617-4fb0-afe9-7aa3ed702992_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_614c5b36-b783-4936-aed9-5aab16023681_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_70e9d9bf-eb60-4aed-a151-def0e1b4d7b0_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_37e8a35d-6a72-4496-8af6-501e4f3616bb_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_af12b756-e8a7-406a-860c-729ff8b80803_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_0d6bcad7-dd57-4d59-a1f8-9b8efa2f4b52_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_b5841488-3bc9-4b80-a57c-69d9e1d87f60_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_41ba9404-4141-4c9a-848b-ea18ba66334c_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_46e0dfbd-b052-4c7e-916d-3f2a0b3e06c8_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_5bb93174-8ae0-46ba-8045-ea191aedcba5_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_78de3743-c8ba-498f-8647-903b4ab83320_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_279d49a7-5009-4b55-842c-bd615d10f4ea_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_da55189e-6a90-45ce-b4f4-7a6cf088c36c_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_b30d19ea-a305-4dce-a0f3-084b012e0aaf_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_520f1425-3619-429d-84e5-4095e4cadea9_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_6169a1d1-8e91-4c64-82d6-2e802fc8f73d_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_c9cb86ac-2e5b-4140-af9d-1be5e17c3c2a_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_86d34e33-2284-4586-b4b9-10fe22dd9e71_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_02db7db3-60d0-402a-8186-2c5993c3de35_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_7b176e8d-8e1b-4efa-aad8-b04dbbf9748e_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_82303869-c70b-4e65-8907-20684e6beae6_2_poly"/>
              <gml:surfaceMember xlink:href="#bldg_DEBWL51140001vWd_c961d04e-9d6f-48d6-8f9c-83cca2cdad74_2_poly"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_d9bab671-645e-4cf4-939b-12708411c540_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>53.725</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>50.664</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>127.32</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>274.464</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>d9bab671-645e-4cf4-939b-12708411c540</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854941_1831">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_d9bab671-645e-4cf4-939b-12708411c540_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32860">
                      <gml:posList srsDimension="3">456490.0366 5428539.5696 131.79 456485.5158 5428551.7407 127.32 456484.33 5428536.55 127.32 456490.0366 5428539.5696 131.79</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_5b104f98-37b4-4911-9bac-4ec0a1511280_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>8.242</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>54.631</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>127.32</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>229.37</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>5b104f98-37b4-4911-9bac-4ec0a1511280</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854941_1832">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_5b104f98-37b4-4911-9bac-4ec0a1511280_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32861">
                      <gml:posList srsDimension="3">456485.72 5428534.93 127.32 456490.0366 5428539.5696 131.79 456484.33 5428536.55 127.32 456485.72 5428534.93 127.32</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_229de10b-d00a-4d22-8bbd-375bc2273402_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>109.731</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>47.827</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>127.32</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>184.507</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>229de10b-d00a-4d22-8bbd-375bc2273402</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854941_1833">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_229de10b-d00a-4d22-8bbd-375bc2273402_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32862">
                      <gml:posList srsDimension="3">456518.57999999996 5428532.34 127.32 456512.0982 5428537.8006 131.79 456485.72 5428534.93 127.32 456518.57999999996 5428532.34 127.32</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_7e4b179d-effe-45e5-837f-2a0f8a9bbc14_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>3.686</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>71.759</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>130.17</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.451</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>234.678</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>7e4b179d-effe-45e5-837f-2a0f8a9bbc14</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854941_1834">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_7e4b179d-effe-45e5-837f-2a0f8a9bbc14_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32863">
                      <gml:posList srsDimension="3">456490.1485 5428574.5631 130.1693 456488.2949 5428577.1826 130.17 456487.308 5428574.8004 129.45069999999998 456490.1485 5428574.5631 130.1693</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_005b5b8a-76a5-44c5-abd1-044224096c69_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>61.824</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>60.382</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.791</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.17</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>274.461</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>005b5b8a-76a5-44c5-abd1-044224096c69</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854941_1835">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_005b5b8a-76a5-44c5-abd1-044224096c69_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32864">
                      <gml:posList srsDimension="3">456491.1326 5428576.953599999 131.7892 456492.5166 5428594.6409 131.7908 456489.8475 5428597.101 130.17 456488.2949 5428577.1826 130.17 456491.1326 5428576.953599999 131.7892</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_aa1a68c2-95ff-424c-854d-e6a94267ee12_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.363</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>53.442</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.789</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.169</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>234.699</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>aa1a68c2-95ff-424c-854d-e6a94267ee12</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854941_1836">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_aa1a68c2-95ff-424c-854d-e6a94267ee12_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32865">
                      <gml:posList srsDimension="3">456490.1485 5428574.5631 130.1693 456491.1326 5428576.953599999 131.7892 456488.2949 5428577.1826 130.17 456490.1485 5428574.5631 130.1693</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_21a1ac3e-e270-4152-bbd2-6b1af7150340_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>26.534</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>57.035</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.17</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>274.464</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>21a1ac3e-e270-4152-bbd2-6b1af7150340</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854941_1837">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_21a1ac3e-e270-4152-bbd2-6b1af7150340_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32866">
                      <gml:posList srsDimension="3">456490.9027 5428551.6969 131.79 456489.78599999996 5428569.4877 130.17 456488.3988 5428551.7173 130.17 456490.9027 5428551.6969 131.79</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_6eeaf62d-0a83-4468-8c53-213e6967ee60_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>72.014</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>30.66</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.791</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>125.63</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>274.475</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>6eeaf62d-0a83-4468-8c53-213e6967ee60</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854941_1838">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_6eeaf62d-0a83-4468-8c53-213e6967ee60_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32867">
                      <gml:posList srsDimension="3">456489.8475 5428597.101 126.9778 456492.5166 5428594.6409 131.7908 456493.1754 5428603.061599999 131.79 456490.01 5428609.42 125.6301 456489.0999999999 5428597.79 125.63 456489.8475 5428597.101 126.9778</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_d7dca3ef-a0de-462e-888b-b7d03e953afc_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>150.207</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>44.619</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>125.63</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>4.686</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>d7dca3ef-a0de-462e-888b-b7d03e953afc</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854941_1839">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_d7dca3ef-a0de-462e-888b-b7d03e953afc_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32868">
                      <gml:posList srsDimension="3">456493.1754 5428603.061599999 131.79 456505.8129 5428602.0258 131.79 456511.97 5428607.62 125.63 456490.01 5428609.42 125.6301 456493.1754 5428603.061599999 131.79</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_a88df266-fb71-4173-b49f-6d9f35da3d38_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>31.452</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>20.77</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.684</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>94.085</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>a88df266-fb71-4173-b49f-6d9f35da3d38</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854941_1840">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_a88df266-fb71-4173-b49f-6d9f35da3d38_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32869">
                      <gml:posList srsDimension="3">456492.3765 5428545.147799999 126.6836 456492.23 5428545.159999999 127.07119999999999 456492.2664 5428545.7328 127.0831 456490.8509 5428550.9725 131.79 456490.0366 5428539.5696 131.79 456492.3765 5428545.147799999 126.6836</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_84d22e0e-b3a6-4784-b757-748b830d5dda_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>56.214</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>48.342</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.68</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>4.573</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>84d22e0e-b3a6-4784-b757-748b830d5dda</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854941_1841">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_84d22e0e-b3a6-4784-b757-748b830d5dda_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32870">
                      <gml:posList srsDimension="3">456499.4941 5428538.8242 131.7851 456498.04019999993 5428544.6541 126.7149 456498.03 5428544.41 126.9322 456492.69 5428544.84 126.9297 456492.71 5428545.12 126.6798 456492.3765 5428545.147799999 126.6836 456490.0366 5428539.5696 131.79 456499.4941 5428538.8242 131.7851</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_0b4fda02-a88f-48ac-9052-603eb276f29b_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>3.772</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>50.983</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.169</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>241.719</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>0b4fda02-a88f-48ac-9052-603eb276f29b</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854941_1842">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_0b4fda02-a88f-48ac-9052-603eb276f29b_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32871">
                      <gml:posList srsDimension="3">456492.52209999994 5428574.373 131.79 456491.1326 5428576.953599999 131.7892 456490.1485 5428574.5631 130.1693 456492.52209999994 5428574.373 131.79</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_b912427b-b68d-4161-acbc-72988b680f3d_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>13.947</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>18.629</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.62</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>91.881</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>b912427b-b68d-4161-acbc-72988b680f3d</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854941_1843">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_b912427b-b68d-4161-acbc-72988b680f3d_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32872">
                      <gml:posList srsDimension="3">456492.2664 5428545.7328 127.0831 456492.59 5428550.83 126.61999999999999 456490.8509 5428550.9725 131.79 456492.2664 5428545.7328 127.0831</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_0b06a9b8-a081-4dc6-8b84-34957ae116c6_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>38.205</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>54.197</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.791</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.27</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>94.481</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>0b06a9b8-a081-4dc6-8b84-34957ae116c6</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854941_1844">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_0b06a9b8-a081-4dc6-8b84-34957ae116c6_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32873">
                      <gml:posList srsDimension="3">456494.87 5428579.94 129.27 456492.5166 5428594.6409 131.7908 456491.1326 5428576.953599999 131.7892 456494.87 5428579.94 129.27</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_fbad8000-bef4-4904-8435-d7a709d5403a_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>7.823</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>61.844</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.27</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>61.671</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>fbad8000-bef4-4904-8435-d7a709d5403a</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854941_1845">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_fbad8000-bef4-4904-8435-d7a709d5403a_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32874">
                      <gml:posList srsDimension="3">456492.52209999994 5428574.373 131.79 456494.87 5428579.94 129.27 456491.1326 5428576.953599999 131.7892 456492.52209999994 5428574.373 131.79</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_7a085f65-15df-49e8-bd54-ae3a04f92f89_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>84.759</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>69.072</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.277</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>94.333</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>7a085f65-15df-49e8-bd54-ae3a04f92f89</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854941_1846">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_7a085f65-15df-49e8-bd54-ae3a04f92f89_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32875">
                      <gml:posList srsDimension="3">456497.6928 5428555.3591 129.2773 456498.66 5428568.76 129.2957 456498.9 5428572.12 129.3012 456499.43239999993 5428579.5734 129.3135 456492.1599 5428569.3005 131.79 456497.6928 5428555.3591 129.2773</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_36c8098d-61f6-4b16-af27-66058e513bbb_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>14.329</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>65.984</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.27</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>2.274</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>36c8098d-61f6-4b16-af27-66058e513bbb</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854941_1847">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_36c8098d-61f6-4b16-af27-66058e513bbb_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32876">
                      <gml:posList srsDimension="3">456499.4386 5428579.6609 129.3135 456499.44 5428579.68 129.305 456494.87 5428579.94 129.27 456492.52209999994 5428574.373 131.79 456499.4386 5428579.6609 129.3135</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_bc7071f3-55ff-4ced-a651-ccb1fc0b4fed_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>12.141</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>68.839</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.115</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.27</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>94.333</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>bc7071f3-55ff-4ced-a651-ccb1fc0b4fed</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854941_1848">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_bc7071f3-55ff-4ced-a651-ccb1fc0b4fed_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32877">
                      <gml:posList srsDimension="3">456497.35 5428550.58 129.27 456497.709 5428555.3185 129.27 456492.59 5428550.83 131.1146 456497.35 5428550.58 129.27</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_f942af93-1c3b-49cc-9b1d-9999e8881ac2_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>60.154</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>90.0</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>115.01</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>115.01</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>-1.0</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>f942af93-1c3b-49cc-9b1d-9999e8881ac2</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854941_1849">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_f942af93-1c3b-49cc-9b1d-9999e8881ac2_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32878">
                      <gml:posList srsDimension="3">456500.12 5428550.21 115.00999999999999 456502.5 5428550.04 115.00999999999999 456509.28 5428549.57 115.00999999999999 456510.12 5428549.67 115.00999999999999 456509.58 5428554.64 115.00999999999999 456507.81 5428554.5 115.00999999999999 456503.36 5428554.86 115.00999999999999 456500.56 5428555.09 115.00999999999999 456497.709 5428555.3185 115.00999999999999 456497.35 5428550.58 115.00999999999999 456500.12 5428550.21 115.00999999999999</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_ea7efa7d-e827-49ab-9010-b63df9e0bf38_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>1.554</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>48.613</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.802</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.693</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>2.559</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>ea7efa7d-e827-49ab-9010-b63df9e0bf38</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854941_1850">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_ea7efa7d-e827-49ab-9010-b63df9e0bf38_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32879">
                      <gml:posList srsDimension="3">456499.4941 5428538.8242 131.7851 456499.89499999996 5428538.7926 131.8021 456498.041 5428544.6732 126.6931 456498.04019999993 5428544.6541 126.7149 456499.4941 5428538.8242 131.7851</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_6c7c50b8-74b0-49ab-86ee-71ceb67b462a_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.392</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>24.799</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.802</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.62</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>275.011</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>6c7c50b8-74b0-49ab-86ee-71ceb67b462a</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854941_1851">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_6c7c50b8-74b0-49ab-86ee-71ceb67b462a_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32880">
                      <gml:posList srsDimension="3">456498.04019999993 5428544.6541 126.7149 456499.89499999996 5428538.7926 131.8021 456498.08 5428545.61 126.61999999999999 456498.041 5428544.6732 126.6931 456498.04019999993 5428544.6541 126.7149</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_3ca3e2a4-7efc-4a21-8794-546b21eb8ad3_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>54.495</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>52.158</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.802</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.62</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>4.089</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>3ca3e2a4-7efc-4a21-8794-546b21eb8ad3</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854941_1852">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_3ca3e2a4-7efc-4a21-8794-546b21eb8ad3_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32881">
                      <gml:posList srsDimension="3">456499.89499999996 5428538.7926 131.8021 456510.95 5428544.69 126.61999999999999 456498.08 5428545.61 126.61999999999999 456499.89499999996 5428538.7926 131.8021</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_7b3df0ca-98ed-4511-a004-3e618caf4db2_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>23.573</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>49.816</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.616</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>223.129</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>7b3df0ca-98ed-4511-a004-3e618caf4db2</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854941_1853">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_7b3df0ca-98ed-4511-a004-3e618caf4db2_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32882">
                      <gml:posList srsDimension="3">456503.83 5428589.79 126.6568 456505.2509 5428593.4659 129.7431 456506.1948 5428595.9025 131.79 456499.5012 5428593.7988 126.6284 456499.49999999994 5428593.78 126.61619999999999 456503.83 5428589.79 126.6568</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_c66efb3e-b25b-4e0c-afab-9201f69c08e8_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>7.059</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>51.806</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.628</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>273.317</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>c66efb3e-b25b-4e0c-afab-9201f69c08e8</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854941_1854">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_c66efb3e-b25b-4e0c-afab-9201f69c08e8_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32883">
                      <gml:posList srsDimension="3">456506.1948 5428595.9025 131.79 456503.1281 5428596.5359 129.3525 456499.50839999993 5428593.915599999 126.6287 456499.5012 5428593.7988 126.6284 456506.1948 5428595.9025 131.79</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_04423292-79b9-4834-9dd3-d3d8255b334d_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>11.312</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>50.169</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.27</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>266.432</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>04423292-79b9-4834-9dd3-d3d8255b334d</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854941_1855">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_04423292-79b9-4834-9dd3-d3d8255b334d_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32884">
                      <gml:posList srsDimension="3">456506.1708 5428596.2865 131.79 456505.8129 5428602.0258 131.79 456503.1281 5428596.5359 129.27 456506.1708 5428596.2865 131.79</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_dfe6480a-32f2-4d27-a0a2-bb1f9ca30f39_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.747</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>51.104</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.352</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>266.424</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>dfe6480a-32f2-4d27-a0a2-bb1f9ca30f39</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854941_1856">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_dfe6480a-32f2-4d27-a0a2-bb1f9ca30f39_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32885">
                      <gml:posList srsDimension="3">456506.1948 5428595.9025 131.79 456506.1708 5428596.2865 131.79 456503.1281 5428596.5359 129.3525 456506.1948 5428595.9025 131.79</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_c582de0b-641d-4570-bc84-64ce0e4b59ae_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.071</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>49.738</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>129.72</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.657</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>177.736</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>c582de0b-641d-4570-bc84-64ce0e4b59ae</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854941_1857">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_c582de0b-641d-4570-bc84-64ce0e4b59ae_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32886">
                      <gml:posList srsDimension="3">456505.5879 5428589.9649 126.746 456505.2509 5428593.4659 129.72 456503.83 5428589.79 126.6568 456505.5879 5428589.9649 126.746</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_bb71fb53-92d3-4cc3-b002-24d1fd393e9a_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>6.793</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>47.254</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.72</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>170.167</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>bb71fb53-92d3-4cc3-b002-24d1fd393e9a</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854941_1858">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_bb71fb53-92d3-4cc3-b002-24d1fd393e9a_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32887">
                      <gml:posList srsDimension="3">456509.6436 5428594.2355 129.7275 456506.1948 5428595.9025 131.79 456505.2509 5428593.4659 129.72 456509.6436 5428594.2355 129.7275</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_91cabe9d-4a6c-4bd5-9435-958f7b124635_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>11.28</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>49.703</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>129.727</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.545</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>170.176</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>91cabe9d-4a6c-4bd5-9435-958f7b124635</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854941_1859">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_91cabe9d-4a6c-4bd5-9435-958f7b124635_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32888">
                      <gml:posList srsDimension="3">456505.5879 5428589.9649 126.746 456505.74 5428589.9799999995 126.7366 456505.7644 5428589.7554 126.5454 456509.6436 5428594.2355 129.7275 456505.2509 5428593.4659 129.72 456505.5879 5428589.9649 126.746</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_84547eae-ac68-4c6f-9950-df3c19003dbe_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>16.282</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>51.339</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>129.727</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.545</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>268.729</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>84547eae-ac68-4c6f-9950-df3c19003dbe</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854941_1860">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_84547eae-ac68-4c6f-9950-df3c19003dbe_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32889">
                      <gml:posList srsDimension="3">456505.7887 5428589.5321 126.5608 456509.7805 5428588.059399999 129.7275 456509.6436 5428594.2355 129.7275 456505.7644 5428589.7554 126.5454 456505.7887 5428589.5321 126.5608</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_e22c9e8a-e859-477d-82b6-f71baf77cad3_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.077</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>49.367</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>129.727</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.478</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>260.401</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e22c9e8a-e859-477d-82b6-f71baf77cad3</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854941_1861">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_e22c9e8a-e859-477d-82b6-f71baf77cad3_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32890">
                      <gml:posList srsDimension="3">456505.96419999993 5428587.9172 126.4782 456509.7805 5428588.059399999 129.7275 456505.7887 5428589.5321 126.5608 456505.96419999993 5428587.9172 126.4782</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_2a7555ff-59cf-4926-8795-c8d601aaedae_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>47.265</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>47.232</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>125.63</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>84.56</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>2a7555ff-59cf-4926-8795-c8d601aaedae</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854941_1862">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_2a7555ff-59cf-4926-8795-c8d601aaedae_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32891">
                      <gml:posList srsDimension="3">456509.6436 5428594.2355 128.9459 456511.97 5428607.62 125.63 456505.8129 5428602.0258 131.79 456509.6436 5428594.2355 128.9459</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_4a058aac-b05c-43ac-a207-9c521e6f0784_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>10.971</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>58.149</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>129.59</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.457</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>353.171</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>4a058aac-b05c-43ac-a207-9c521e6f0784</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854941_1863">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_4a058aac-b05c-43ac-a207-9c521e6f0784_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32892">
                      <gml:posList srsDimension="3">456507.69 5428572.04 126.9145 456508.04 5428568.8 128.939 456508.1533 5428567.7583 129.59 456511.8225 5428568.1975 129.59 456507.6104 5428572.7728 126.4567 456507.69 5428572.04 126.9145</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_c95d23ce-6bed-430d-ae7a-89871a295ec1_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>11.026</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>58.149</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>129.59</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.441</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>173.174</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>c95d23ce-6bed-430d-ae7a-89871a295ec1</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854941_1864">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_c95d23ce-6bed-430d-ae7a-89871a295ec1_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32893">
                      <gml:posList srsDimension="3">456508.70139999996 5428562.7189 126.4409 456511.8225 5428568.1975 129.59 456508.1533 5428567.7583 129.59 456508.70139999996 5428562.7189 126.4409</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_e91d55dd-72dd-4601-8a09-fe49622190fd_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>38.789</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>48.765</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>128.946</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>125.63</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>83.691</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e91d55dd-72dd-4601-8a09-fe49622190fd</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854941_1865">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_e91d55dd-72dd-4601-8a09-fe49622190fd_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32894">
                      <gml:posList srsDimension="3">456513.6646 5428592.2921 125.63 456511.97 5428607.62 125.63 456509.6436 5428594.2355 128.9459 456513.6646 5428592.2921 125.63</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_80224a65-3c91-499b-9b42-daaf61d22180_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>17.463</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>44.702</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>129.727</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>125.709</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>88.73</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>80224a65-3c91-499b-9b42-daaf61d22180</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854941_1866">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_80224a65-3c91-499b-9b42-daaf61d22180_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32895">
                      <gml:posList srsDimension="3">456509.7805 5428588.059399999 129.7275 456513.6646 5428592.2921 125.709 456509.6436 5428594.2355 129.7275 456509.7805 5428588.059399999 129.7275</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_80215475-3c6d-412d-a61b-a4ab0d63fc28_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>11.694</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>47.108</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>129.727</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>125.709</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>83.693</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>80215475-3c6d-412d-a61b-a4ab0d63fc28</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854941_1867">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_80215475-3c6d-412d-a61b-a4ab0d63fc28_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32896">
                      <gml:posList srsDimension="3">456514.0998 5428588.3548 125.709 456513.6646 5428592.2921 125.709 456509.7805 5428588.059399999 129.7275 456514.0998 5428588.3548 125.709</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_e221e38a-5a6a-4f7a-9e85-f92abb5a6297_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>6.664</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>49.024</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>129.72</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.429</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>260.56</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e221e38a-5a6a-4f7a-9e85-f92abb5a6297</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1868">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_e221e38a-5a6a-4f7a-9e85-f92abb5a6297_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32897">
                      <gml:posList srsDimension="3">456510.5736 5428546.9481 126.42999999999999 456514.1509 5428548.5278 129.72 456510.13699999993 5428549.5683 126.4292 456510.5736 5428546.9481 126.42999999999999</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_dfcbad28-4c03-4fca-ba8c-d645118b4339_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>3.042</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>46.056</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>129.72</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.43</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>275.374</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>dfcbad28-4c03-4fca-ba8c-d645118b4339</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1869">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_dfcbad28-4c03-4fca-ba8c-d645118b4339_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32898">
                      <gml:posList srsDimension="3">456514.0307 5428547.2501 129.72 456514.1509 5428548.5278 129.72 456510.5736 5428546.9481 126.42999999999999 456514.0307 5428547.2501 129.72</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_84de4ffe-4233-4d32-b50c-fd7bd4a47a9b_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.953</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>43.514</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.72</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>355.008</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>84de4ffe-4233-4d32-b50c-fd7bd4a47a9b</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1870">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_84de4ffe-4233-4d32-b50c-fd7bd4a47a9b_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32899">
                      <gml:posList srsDimension="3">456513.4858 5428545.2297 131.79 456514.0307 5428547.2501 129.72 456510.5736 5428546.9481 129.72 456513.4858 5428545.2297 131.79</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_9dccb6d4-1730-4408-a2d7-ddec67823397_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>3.795</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>51.367</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.72</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>260.536</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>9dccb6d4-1730-4408-a2d7-ddec67823397</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1871">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_9dccb6d4-1730-4408-a2d7-ddec67823397_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32900">
                      <gml:posList srsDimension="3">456510.95 5428544.69 129.72 456513.4858 5428545.2297 131.79 456510.5736 5428546.9481 129.72 456510.95 5428544.69 129.72</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_d050603c-3b44-4621-824e-9fcca1f9a0e0_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>12.546</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>51.132</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.72</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>265.839</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>d050603c-3b44-4621-824e-9fcca1f9a0e0</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1872">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_d050603c-3b44-4621-824e-9fcca1f9a0e0_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32901">
                      <gml:posList srsDimension="3">456514.0378 5428537.6429 131.79 456513.4858 5428545.2297 131.79 456510.95 5428544.69 129.72 456514.0378 5428537.6429 131.79</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_6c905a43-0170-4195-b5fc-d704fd35c4f2_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>5.872</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>42.457</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.72</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>260.655</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>6c905a43-0170-4195-b5fc-d704fd35c4f2</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1873">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_6c905a43-0170-4195-b5fc-d704fd35c4f2_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32902">
                      <gml:posList srsDimension="3">456511.63849999994 5428540.558999999 129.72 456511.6406 5428540.5465 129.7294 456514.0378 5428537.6429 131.79 456510.95 5428544.69 129.72 456511.63849999994 5428540.558999999 129.72</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_f306da72-9325-4eb0-899a-ead1579abbb6_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>6.466</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>47.728</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>127.32</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>184.648</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>f306da72-9325-4eb0-899a-ead1579abbb6</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1874">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_f306da72-9325-4eb0-899a-ead1579abbb6_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32903">
                      <gml:posList srsDimension="3">456518.57999999996 5428532.34 127.32 456514.0378 5428537.6429 131.79 456512.0982 5428537.8006 131.79 456518.57999999996 5428532.34 127.32</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_16e6e85f-2401-4f6f-b494-418bc7fc37d0_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>12.944</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>52.935</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>129.47</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>125.628</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>353.173</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>16e6e85f-2401-4f6f-b494-418bc7fc37d0</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1875">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_16e6e85f-2401-4f6f-b494-418bc7fc37d0_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32904">
                      <gml:posList srsDimension="3">456516.2694 5428568.7299 129.47 456515.7105 5428573.7857 125.6281 456512.23689999996 5428568.247099999 129.47 456516.2694 5428568.7299 129.47</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_168884f0-a651-41f0-ace7-b2fbde10518c_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>12.942</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>52.949</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>129.47</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>125.63</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>173.173</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>168884f0-a651-41f0-ace7-b2fbde10518c</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1876">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_168884f0-a651-41f0-ace7-b2fbde10518c_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32905">
                      <gml:posList srsDimension="3">456516.8284 5428563.6741 125.63 456516.2694 5428568.7299 129.47 456512.23689999996 5428568.247099999 129.47 456516.8284 5428563.6741 125.63</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_3120c040-cf45-4443-94c8-d62fb9a5d1a8_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.428</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>43.481</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.72</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>354.828</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>3120c040-cf45-4443-94c8-d62fb9a5d1a8</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1877">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_3120c040-cf45-4443-94c8-d62fb9a5d1a8_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32906">
                      <gml:posList srsDimension="3">456515.7247 5428547.3981 129.7256 456514.0307 5428547.2501 129.72 456513.4858 5428545.2297 131.79 456515.7247 5428547.3981 129.7256</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_355ac522-adfb-43cf-9322-d8078125ff62_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>12.013</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>49.185</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.726</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>85.839</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>355ac522-adfb-43cf-9322-d8078125ff62</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1878">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_355ac522-adfb-43cf-9322-d8078125ff62_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32907">
                      <gml:posList srsDimension="3">456514.0378 5428537.6429 131.79 456515.7247 5428547.3981 129.7256 456513.4858 5428545.2297 131.79 456514.0378 5428537.6429 131.79</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_864c1e24-d019-4907-b642-f2b92745796a_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.115</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>43.264</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>129.72</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>127.32</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>95.374</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>864c1e24-d019-4907-b642-f2b92745796a</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1879">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_864c1e24-d019-4907-b642-f2b92745796a_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32908">
                      <gml:posList srsDimension="3">456516.4937 5428549.3151 127.32 456514.1509 5428548.5278 129.72 456514.0307 5428547.2501 129.72 456516.4937 5428549.3151 127.32</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_98e4df3d-f7bc-4a90-8516-2bb72eab651b_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.579</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>37.418</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>129.726</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>127.32</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>354.863</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>98e4df3d-f7bc-4a90-8516-2bb72eab651b</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1880">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_98e4df3d-f7bc-4a90-8516-2bb72eab651b_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32909">
                      <gml:posList srsDimension="3">456515.7247 5428547.3981 129.7256 456516.4937 5428549.3151 127.32 456514.0307 5428547.2501 129.72 456515.7247 5428547.3981 129.7256</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_409bfeda-ca2b-4daa-b967-8a829e9fa289_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>41.742</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>51.844</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>127.32</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>84.419</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>409bfeda-ca2b-4daa-b967-8a829e9fa289</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1881">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_409bfeda-ca2b-4daa-b967-8a829e9fa289_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32910">
                      <gml:posList srsDimension="3">456520.12 5428533.9 127.32 456515.7247 5428547.3981 129.7256 456514.0378 5428537.6429 131.79 456520.12 5428533.9 127.32</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_89da396c-80ad-42fb-a662-372c4351dfef_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>9.064</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>57.282</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>127.32</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>134.63</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>89da396c-80ad-42fb-a662-372c4351dfef</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1882">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_89da396c-80ad-42fb-a662-372c4351dfef_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32911">
                      <gml:posList srsDimension="3">456518.57999999996 5428532.34 127.32 456520.12 5428533.9 127.32 456514.0378 5428537.6429 131.79 456518.57999999996 5428532.34 127.32</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_29d0aa9e-1592-4fd1-89d3-c5627dacb7c1_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>2.809</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>33.167</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>129.726</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>127.32</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>341.428</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>29d0aa9e-1592-4fd1-89d3-c5627dacb7c1</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1883">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_29d0aa9e-1592-4fd1-89d3-c5627dacb7c1_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32912">
                      <gml:posList srsDimension="3">456518.347 5428549.9378 127.32 456516.6131 5428549.3552 127.32 456516.4937 5428549.3151 127.32 456515.7247 5428547.3981 129.7256 456518.347 5428549.9378 127.32</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_65843ec7-3703-429e-a7c4-56f9fb9e2768_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>30.308</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>50.182</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>129.726</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>127.32</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>83.691</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>65843ec7-3703-429e-a7c4-56f9fb9e2768</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1884">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_65843ec7-3703-429e-a7c4-56f9fb9e2768_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32913">
                      <gml:posList srsDimension="3">456520.12 5428533.9 127.32 456518.347 5428549.9378 127.32 456515.7247 5428547.3981 129.7256 456520.12 5428533.9 127.32</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_18903252-cc34-4549-b5e4-2b174d65a793_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>26.769</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>18903252-cc34-4549-b5e4-2b174d65a793</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1885">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_18903252-cc34-4549-b5e4-2b174d65a793_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32914">
                      <gml:posList srsDimension="3">456485.72 5428534.93 114.7794 456485.72 5428534.93 127.32 456484.33 5428536.55 127.32 456484.33 5428536.55 114.7794 456485.72 5428534.93 114.7794</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_c7462b43-07d9-4f8e-8761-6d18481339cd_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>413.362</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>c7462b43-07d9-4f8e-8761-6d18481339cd</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1886">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_c7462b43-07d9-4f8e-8761-6d18481339cd_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32915">
                      <gml:posList srsDimension="3">456518.57999999996 5428532.34 114.7794 456518.57999999996 5428532.34 127.32 456485.72 5428534.93 127.32 456485.72 5428534.93 114.7794 456518.57999999996 5428532.34 114.7794</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_bf89edc3-8d28-4f31-ba6d-4f70ebaad9fe_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>48.553</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>bf89edc3-8d28-4f31-ba6d-4f70ebaad9fe</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1887">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_bf89edc3-8d28-4f31-ba6d-4f70ebaad9fe_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32916">
                      <gml:posList srsDimension="3">456486.92 5428569.7299999995 114.7794 456486.92 5428569.7299999995 129.45 456487.17 5428573.03 129.4509 456487.17 5428573.03 114.7794 456486.92 5428569.7299999995 114.7794</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_e196140f-eef5-4495-83fa-2154a0f7a756_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>126.579</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e196140f-eef5-4495-83fa-2154a0f7a756</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1888">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_e196140f-eef5-4495-83fa-2154a0f7a756_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32917">
                      <gml:posList srsDimension="3">456489.0999999999 5428597.79 114.7794 456489.0999999999 5428597.79 125.63 456490.01 5428609.42 125.6301 456490.01 5428609.42 114.7794 456489.0999999999 5428597.79 114.7794</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_248a8855-a721-456c-bc02-e66884300613_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>239.079</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>248a8855-a721-456c-bc02-e66884300613</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1889">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_248a8855-a721-456c-bc02-e66884300613_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32918">
                      <gml:posList srsDimension="3">456490.01 5428609.42 114.7794 456490.01 5428609.42 125.6301 456511.97 5428607.62 125.63 456511.97 5428607.62 114.7794 456490.01 5428609.42 114.7794</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_e4a848bf-2303-476e-953b-d39d840cc388_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>3.376</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e4a848bf-2303-476e-953b-d39d840cc388</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1890">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_e4a848bf-2303-476e-953b-d39d840cc388_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32919">
                      <gml:posList srsDimension="3">456492.71 5428545.12 114.7794 456492.71 5428545.12 126.6798 456492.69 5428544.84 126.9297 456492.69 5428544.84 114.7794 456492.71 5428545.12 114.7794</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_0c605b4b-4a39-4471-a5df-6b35d98fc98b_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>65.099</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>0c605b4b-4a39-4471-a5df-6b35d98fc98b</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1891">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_0c605b4b-4a39-4471-a5df-6b35d98fc98b_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32920">
                      <gml:posList srsDimension="3">456492.69 5428544.84 114.7794 456492.69 5428544.84 126.9297 456498.03 5428544.41 126.9322 456498.03 5428544.41 114.7794 456492.69 5428544.84 114.7794</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_7baf9d7c-e034-4203-ab0f-fe3dec989e72_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>3.921</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>7baf9d7c-e034-4203-ab0f-fe3dec989e72</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1892">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_7baf9d7c-e034-4203-ab0f-fe3dec989e72_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32921">
                      <gml:posList srsDimension="3">456492.59 5428550.83 126.61999999999999 456492.59 5428550.83 131.1146 456490.8509 5428550.9725 131.79 456492.59 5428550.83 126.61999999999999</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_d7ecc8f0-eb74-49ad-bb48-196f97d531c9_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>66.409</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>d7ecc8f0-eb74-49ad-bb48-196f97d531c9</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1893">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_d7ecc8f0-eb74-49ad-bb48-196f97d531c9_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32922">
                      <gml:posList srsDimension="3">456494.87 5428579.94 114.7794 456494.87 5428579.94 129.27 456499.44 5428579.68 129.305 456499.44 5428579.68 114.7794 456494.87 5428579.94 114.7794</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_edb20409-1b99-4cb6-bf45-f62e7ef70e5b_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>67.765</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>edb20409-1b99-4cb6-bf45-f62e7ef70e5b</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1894">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_edb20409-1b99-4cb6-bf45-f62e7ef70e5b_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32923">
                      <gml:posList srsDimension="3">456497.709 5428555.3185 115.00999999999999 456497.709 5428555.3185 129.27 456497.35 5428550.58 129.27 456497.35 5428550.58 115.00999999999999 456497.709 5428555.3185 115.00999999999999</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_3ccae2ef-13da-44cf-a597-ec406257a8b1_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>73.467</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>3ccae2ef-13da-44cf-a597-ec406257a8b1</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1895">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_3ccae2ef-13da-44cf-a597-ec406257a8b1_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32924">
                      <gml:posList srsDimension="3">456497.35 5428550.58 114.7794 456497.35 5428550.58 115.00999999999999 456497.35 5428550.58 129.27 456492.59 5428550.83 131.1146 456492.59 5428550.83 126.61999999999999 456492.59 5428550.83 114.7794 456497.35 5428550.58 114.7794</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_19b240e3-066a-4ed0-b744-8c76165153ec_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.648</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>19b240e3-066a-4ed0-b744-8c76165153ec</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1896">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_19b240e3-066a-4ed0-b744-8c76165153ec_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32925">
                      <gml:posList srsDimension="3">456500.56 5428555.09 114.7794 456500.56 5428555.09 115.00999999999999 456503.36 5428554.86 115.00999999999999 456503.36 5428554.86 114.7794 456500.56 5428555.09 114.7794</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_308dba52-07fd-408f-818a-54882557a788_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>1.03</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>308dba52-07fd-408f-818a-54882557a788</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1897">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_308dba52-07fd-408f-818a-54882557a788_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32926">
                      <gml:posList srsDimension="3">456503.36 5428554.86 114.7794 456503.36 5428554.86 115.00999999999999 456507.81 5428554.5 115.00999999999999 456507.81 5428554.5 114.7794 456503.36 5428554.86 114.7794</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_cdf532ca-e7fe-447d-81ad-da2d9ca2d01f_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.409</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>cdf532ca-e7fe-447d-81ad-da2d9ca2d01f</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1898">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_cdf532ca-e7fe-447d-81ad-da2d9ca2d01f_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32927">
                      <gml:posList srsDimension="3">456507.81 5428554.5 114.7794 456507.81 5428554.5 115.00999999999999 456509.58 5428554.64 115.00999999999999 456509.58 5428554.64 114.7794 456507.81 5428554.5 114.7794</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_8abac84d-e4f0-473c-b8f4-09561e544020_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.195</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>8abac84d-e4f0-473c-b8f4-09561e544020</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1899">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_8abac84d-e4f0-473c-b8f4-09561e544020_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32928">
                      <gml:posList srsDimension="3">456510.12 5428549.67 114.7794 456510.12 5428549.67 115.00999999999999 456509.28 5428549.57 115.00999999999999 456509.28 5428549.57 114.7794 456510.12 5428549.67 114.7794</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_36a3241d-67f7-4b97-bcee-28054511d61d_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>1.567</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>36a3241d-67f7-4b97-bcee-28054511d61d</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1900">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_36a3241d-67f7-4b97-bcee-28054511d61d_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32929">
                      <gml:posList srsDimension="3">456509.28 5428549.57 114.7794 456509.28 5428549.57 115.00999999999999 456502.5 5428550.04 115.00999999999999 456502.5 5428550.04 114.7794 456509.28 5428549.57 114.7794</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_a9091c9a-952a-4fb5-8210-ae3d6edfc08b_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.55</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>a9091c9a-952a-4fb5-8210-ae3d6edfc08b</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1901">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_a9091c9a-952a-4fb5-8210-ae3d6edfc08b_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32930">
                      <gml:posList srsDimension="3">456502.5 5428550.04 114.7794 456502.5 5428550.04 115.00999999999999 456500.12 5428550.21 115.00999999999999 456500.12 5428550.21 114.7794 456502.5 5428550.04 114.7794</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_1e41c926-d1de-4c92-b52f-5934302b798d_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.644</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>1e41c926-d1de-4c92-b52f-5934302b798d</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1902">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_1e41c926-d1de-4c92-b52f-5934302b798d_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32931">
                      <gml:posList srsDimension="3">456500.12 5428550.21 114.7794 456500.12 5428550.21 115.00999999999999 456497.35 5428550.58 115.00999999999999 456497.35 5428550.58 114.7794 456500.12 5428550.21 114.7794</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_47050ade-5dde-4ce9-965d-9778504373f8_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>152.777</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>47050ade-5dde-4ce9-965d-9778504373f8</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1903">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_47050ade-5dde-4ce9-965d-9778504373f8_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32932">
                      <gml:posList srsDimension="3">456498.08 5428545.61 114.7794 456498.08 5428545.61 126.61999999999999 456510.95 5428544.69 126.61999999999999 456510.95 5428544.69 114.7794 456498.08 5428545.61 114.7794</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_4beae04c-d80c-4c07-96bd-6110e82d9d15_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.03</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>4beae04c-d80c-4c07-96bd-6110e82d9d15</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1904">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_4beae04c-d80c-4c07-96bd-6110e82d9d15_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32933">
                      <gml:posList srsDimension="3">456506.1948 5428595.9025 131.79 456505.2509 5428593.4659 129.7431 456505.2509 5428593.4659 129.72 456506.1948 5428595.9025 131.79</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_d8ad4044-ac3c-4335-80ec-95d4d956d2d8_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.046</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>d8ad4044-ac3c-4335-80ec-95d4d956d2d8</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1905">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_d8ad4044-ac3c-4335-80ec-95d4d956d2d8_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32934">
                      <gml:posList srsDimension="3">456505.2509 5428593.4659 129.72 456505.2509 5428593.4659 129.7431 456503.83 5428589.79 126.6568 456505.2509 5428593.4659 129.72</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_3d2802e8-4856-4e44-a819-15fe60e6ecd5_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>69.815</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>3d2802e8-4856-4e44-a819-15fe60e6ecd5</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1906">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_3d2802e8-4856-4e44-a819-15fe60e6ecd5_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32935">
                      <gml:posList srsDimension="3">456503.83 5428589.79 114.7794 456503.83 5428589.79 126.6568 456499.49999999994 5428593.78 126.61619999999999 456499.49999999994 5428593.78 114.7794 456503.83 5428589.79 114.7794</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_c721551e-0a23-4e0b-9d6e-44731ef6a680_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.477</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>c721551e-0a23-4e0b-9d6e-44731ef6a680</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1907">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_c721551e-0a23-4e0b-9d6e-44731ef6a680_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32936">
                      <gml:posList srsDimension="3">456503.0251 5428596.5443 129.27 456499.6874 5428596.8179 129.27 456499.6874 5428596.8179 126.59609999999999 456503.0251 5428596.5443 129.27</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_0462df14-e13f-40be-ba6c-54c2a8b12222_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>42.845</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>0462df14-e13f-40be-ba6c-54c2a8b12222</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1908">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_0462df14-e13f-40be-ba6c-54c2a8b12222_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32937">
                      <gml:posList srsDimension="3">456508.04 5428568.8 114.7794 456508.04 5428568.8 128.939 456507.69 5428572.04 126.9145 456507.69 5428572.04 114.7794 456508.04 5428568.8 114.7794</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_1b3737a2-ffbd-4322-b2a7-67c49f380504_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.167</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>1b3737a2-ffbd-4322-b2a7-67c49f380504</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1909">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_1b3737a2-ffbd-4322-b2a7-67c49f380504_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32938">
                      <gml:posList srsDimension="3">456514.0998 5428588.3548 125.6319 456514.0998 5428588.3548 125.709 456509.7805 5428588.059399999 129.7275 456514.0998 5428588.3548 125.6319</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_c4f91d0c-0570-4021-aefc-e4c197e45cda_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>5.709</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>c4f91d0c-0570-4021-aefc-e4c197e45cda</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1910">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_c4f91d0c-0570-4021-aefc-e4c197e45cda_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32939">
                      <gml:posList srsDimension="3">456510.5736 5428546.9481 126.42999999999999 456510.5736 5428546.9481 129.72 456514.0307 5428547.2501 129.72 456510.5736 5428546.9481 126.42999999999999</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_8498f146-0881-4cb8-805c-bbc5ad06f4e7_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>27.49</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>8498f146-0881-4cb8-805c-bbc5ad06f4e7</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1911">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_8498f146-0881-4cb8-805c-bbc5ad06f4e7_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32940">
                      <gml:posList srsDimension="3">456520.12 5428533.9 114.7794 456520.12 5428533.9 127.32 456518.57999999996 5428532.34 127.32 456518.57999999996 5428532.34 114.7794 456520.12 5428533.9 114.7794</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_9b0f1eec-60d3-42a3-864c-92093c3df28a_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>1.546</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>9b0f1eec-60d3-42a3-864c-92093c3df28a</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1912">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_9b0f1eec-60d3-42a3-864c-92093c3df28a_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32941">
                      <gml:posList srsDimension="3">456516.6131 5428549.3552 127.32 456518.347 5428549.9378 127.32 456518.347 5428549.9378 125.63 456516.6131 5428549.3552 127.32</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_8f35cb73-41f3-4a1b-8230-ca15f38ab4bd_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>55.476</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>52.639</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.802</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.62</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>4.717</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>8f35cb73-41f3-4a1b-8230-ca15f38ab4bd</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1913">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_8f35cb73-41f3-4a1b-8230-ca15f38ab4bd_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32942">
                      <gml:posList srsDimension="3">456512.0982 5428537.8006 131.79 456514.0378 5428537.6429 131.79 456511.6406 5428540.5465 129.7294 456511.63849999994 5428540.558999999 129.72 456510.95 5428544.69 126.61999999999999 456499.89499999996 5428538.7926 131.8021 456512.0982 5428537.8006 131.79</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_9490334d-1abf-491d-bd26-317685209ef8_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>173.298</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>49.599</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>129.727</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.424</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>263.69</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>9490334d-1abf-491d-bd26-317685209ef8</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1914">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_9490334d-1abf-491d-bd26-317685209ef8_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32943">
                      <gml:posList srsDimension="3">456507.6057 5428572.8154 126.4543 456507.6104 5428572.7728 126.4567 456511.8225 5428568.1975 129.59 456508.70139999996 5428562.7189 126.4409 456508.7033 5428562.7014 126.4409 456509.5719 5428554.7148 126.4297 456509.58 5428554.64 126.4307 456510.12 5428549.67 126.42439999999999 456510.13699999993 5428549.5683 126.4292 456514.1509 5428548.5278 129.72 456513.9235999999 5428550.5834 129.7208 456513.4377 5428554.9792 129.7203 456512.5332 5428563.1599 129.72 456511.97429999994 5428568.2157 129.72 456511.4154 5428573.2715 129.7225 456509.7805 5428588.059399999 129.7275 456505.96419999993 5428587.9172 126.4782 456507.6057 5428572.8154 126.4543</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_134a02a9-6ac4-4793-8c91-984d350f0d30_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>205.062</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>46.586</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>129.727</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>125.628</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>83.701</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>134a02a9-6ac4-4793-8c91-984d350f0d30</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1915">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_134a02a9-6ac4-4793-8c91-984d350f0d30_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32944">
                      <gml:posList srsDimension="3">456511.4154 5428573.2715 129.7225 456511.97429999994 5428568.2157 129.72 456512.5332 5428563.1599 129.72 456513.4377 5428554.9792 129.7203 456513.9235999999 5428550.5834 129.7208 456514.1509 5428548.5278 129.72 456516.4937 5428549.3151 127.43639999999999 456516.6131 5428549.3552 127.32 456518.347 5428549.9378 125.63 456516.8284 5428563.6741 125.63 456512.23689999996 5428568.247099999 129.47 456515.7105 5428573.7857 125.6281 456514.0998 5428588.3548 125.6319 456509.7805 5428588.059399999 129.7275 456511.4154 5428573.2715 129.7225</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_1daf13b1-8a48-41d4-83d3-2dd01bf5aae9_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>36.629</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>1daf13b1-8a48-41d4-83d3-2dd01bf5aae9</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1916">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_1daf13b1-8a48-41d4-83d3-2dd01bf5aae9_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32945">
                      <gml:posList srsDimension="3">456499.49999999994 5428593.78 126.61619999999999 456499.5012 5428593.7988 126.6284 456499.50839999993 5428593.915599999 126.6287 456499.6874 5428596.8179 126.59609999999999 456499.6874 5428596.8179 129.27 456499.69 5428596.859999999 129.2887 456499.69 5428596.859999999 114.7794 456499.6874 5428596.8179 114.7794 456499.50839999993 5428593.915599999 114.7794 456499.5012 5428593.7988 114.7794 456499.49999999994 5428593.78 114.7794 456499.49999999994 5428593.78 126.61619999999999</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_5d9a2ff7-2f8e-4ddd-a75b-aa2d00ea4dc3_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>211.204</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>5d9a2ff7-2f8e-4ddd-a75b-aa2d00ea4dc3</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1917">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_5d9a2ff7-2f8e-4ddd-a75b-aa2d00ea4dc3_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32946">
                      <gml:posList srsDimension="3">456505.7644 5428589.7554 114.7794 456505.7887 5428589.5321 114.7794 456505.96419999993 5428587.9172 114.7794 456507.6057 5428572.8154 114.7794 456507.6104 5428572.7728 114.7794 456507.69 5428572.04 114.7794 456507.69 5428572.04 126.9145 456507.6104 5428572.7728 126.4567 456507.6057 5428572.8154 126.4543 456505.96419999993 5428587.9172 126.4782 456505.7887 5428589.5321 126.5608 456505.7644 5428589.7554 126.5454 456505.74 5428589.9799999995 126.7366 456505.74 5428589.9799999995 114.7794 456505.7644 5428589.7554 114.7794</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_e10686d8-04d1-410e-8cd4-8250b5588a61_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>234.074</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e10686d8-04d1-410e-8cd4-8250b5588a61</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1918">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_e10686d8-04d1-410e-8cd4-8250b5588a61_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32947">
                      <gml:posList srsDimension="3">456508.1533 5428567.7583 114.7794 456508.70139999996 5428562.7189 114.7794 456508.7033 5428562.7014 114.7794 456509.5719 5428554.7148 114.7794 456509.58 5428554.64 114.7794 456509.58 5428554.64 115.00999999999999 456510.12 5428549.67 115.00999999999999 456510.12 5428549.67 126.42439999999999 456509.58 5428554.64 126.4307 456509.5719 5428554.7148 126.4297 456508.7033 5428562.7014 126.4409 456508.70139999996 5428562.7189 126.4409 456508.1533 5428567.7583 129.59 456508.04 5428568.8 128.939 456508.04 5428568.8 114.7794 456508.1533 5428567.7583 114.7794</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_87fb6bb8-5c2e-4b51-af40-5e2bb0358071_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>72.842</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>87fb6bb8-5c2e-4b51-af40-5e2bb0358071</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1919">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_87fb6bb8-5c2e-4b51-af40-5e2bb0358071_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32948">
                      <gml:posList srsDimension="3">456510.13699999993 5428549.5683 114.7794 456510.5736 5428546.9481 114.7794 456510.95 5428544.69 114.7794 456510.95 5428544.69 126.61999999999999 456511.63849999994 5428540.558999999 129.72 456510.95 5428544.69 129.72 456510.5736 5428546.9481 129.72 456510.5736 5428546.9481 126.42999999999999 456510.13699999993 5428549.5683 126.4292 456510.12 5428549.67 126.42439999999999 456510.12 5428549.67 115.00999999999999 456510.12 5428549.67 114.7794 456510.13699999993 5428549.5683 114.7794</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_8b4644bc-eba1-4626-83fa-156e2b466fd7_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>851.889</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>8b4644bc-eba1-4626-83fa-156e2b466fd7</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1920">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_8b4644bc-eba1-4626-83fa-156e2b466fd7_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32949">
                      <gml:posList srsDimension="3">456513.6646 5428592.2921 125.63 456513.6646 5428592.2921 125.709 456514.0998 5428588.3548 125.709 456514.0998 5428588.3548 125.6319 456515.7105 5428573.7857 125.6281 456516.2694 5428568.7299 129.47 456516.8284 5428563.6741 125.63 456518.347 5428549.9378 125.63 456518.347 5428549.9378 127.32 456520.12 5428533.9 127.32 456520.12 5428533.9 114.7794 456518.347 5428549.9378 114.7794 456516.8284 5428563.6741 114.7794 456516.2694 5428568.7299 114.7794 456515.7105 5428573.7857 114.7794 456514.0998 5428588.3548 114.7794 456513.6646 5428592.2921 114.7794 456511.97 5428607.62 114.7794 456511.97 5428607.62 125.63 456513.6646 5428592.2921 125.63</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_e89c84c0-b799-4db4-a07c-af2eceb2a123_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>159.02</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>e89c84c0-b799-4db4-a07c-af2eceb2a123</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1921">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_e89c84c0-b799-4db4-a07c-af2eceb2a123_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32950">
                      <gml:posList srsDimension="3">456498.66 5428568.76 114.7794 456498.9 5428572.12 114.7794 456499.43239999993 5428579.5734 114.7794 456499.4367 5428579.634 114.7794 456499.4386 5428579.6609 114.7794 456499.44 5428579.68 114.7794 456499.44 5428579.68 129.305 456499.4386 5428579.6609 129.3135 456499.4367 5428579.634 129.3135 456499.43239999993 5428579.5734 129.3135 456498.9 5428572.12 129.3012 456498.66 5428568.76 129.2957 456498.66 5428568.76 114.7794</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="bldg_DEBWL51140001vWd_0534fc3d-7a63-448f-8d90-5816f951c6da_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>1688.058</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>0534fc3d-7a63-448f-8d90-5816f951c6da</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854942_1922">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_0534fc3d-7a63-448f-8d90-5816f951c6da_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32951">
                      <gml:posList srsDimension="3">456518.57999999996 5428532.34 114.7794 456485.72 5428534.93 114.7794 456484.33 5428536.55 114.7794 456485.5158 5428551.7407 114.7794 456486.92 5428569.7299999995 114.7794 456487.17 5428573.03 114.7794 456487.308 5428574.8004 114.7794 456487.4991 5428577.2514 114.7794 456489.0999999999 5428597.79 114.7794 456490.01 5428609.42 114.7794 456511.97 5428607.62 114.7794 456513.6646 5428592.2921 114.7794 456514.0998 5428588.3548 114.7794 456515.7105 5428573.7857 114.7794 456516.2694 5428568.7299 114.7794 456516.8284 5428563.6741 114.7794 456518.347 5428549.9378 114.7794 456520.12 5428533.9 114.7794 456518.57999999996 5428532.34 114.7794</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                  <gml:interior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32952">
                      <gml:posList srsDimension="3">456510.5736 5428546.9481 114.7794 456510.13699999993 5428549.5683 114.7794 456510.12 5428549.67 114.7794 456509.28 5428549.57 114.7794 456502.5 5428550.04 114.7794 456500.12 5428550.21 114.7794 456497.35 5428550.58 114.7794 456492.59 5428550.83 114.7794 456492.2664 5428545.7328 114.7794 456492.23 5428545.159999999 114.7794 456492.3765 5428545.147799999 114.7794 456492.71 5428545.12 114.7794 456492.69 5428544.84 114.7794 456498.03 5428544.41 114.7794 456498.04019999993 5428544.6541 114.7794 456498.041 5428544.6732 114.7794 456498.08 5428545.61 114.7794 456510.95 5428544.69 114.7794 456510.5736 5428546.9481 114.7794</gml:posList>
                    </gml:LinearRing>
                  </gml:interior>
                  <gml:interior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32953">
                      <gml:posList srsDimension="3">456509.5719 5428554.7148 114.7794 456508.7033 5428562.7014 114.7794 456508.70139999996 5428562.7189 114.7794 456508.1533 5428567.7583 114.7794 456508.04 5428568.8 114.7794 456507.69 5428572.04 114.7794 456507.6104 5428572.7728 114.7794 456507.6057 5428572.8154 114.7794 456505.96419999993 5428587.9172 114.7794 456505.7887 5428589.5321 114.7794 456505.7644 5428589.7554 114.7794 456505.74 5428589.9799999995 114.7794 456505.5879 5428589.9649 114.7794 456503.83 5428589.79 114.7794 456499.49999999994 5428593.78 114.7794 456499.5012 5428593.7988 114.7794 456499.50839999993 5428593.915599999 114.7794 456499.6874 5428596.8179 114.7794 456499.69 5428596.859999999 114.7794 456499.5278 5428596.874099999 114.7794 456496.1391999999 5428597.1683 114.7794 456496.12 5428597.17 114.7794 456496.1125 5428597.0671 114.7794 456494.87 5428579.94 114.7794 456499.44 5428579.68 114.7794 456499.4386 5428579.6609 114.7794 456499.4367 5428579.634 114.7794 456499.43239999993 5428579.5734 114.7794 456498.9 5428572.12 114.7794 456498.66 5428568.76 114.7794 456497.6928 5428555.3591 114.7794 456497.69 5428555.32 114.7794 456497.709 5428555.3185 114.7794 456500.56 5428555.09 114.7794 456503.36 5428554.86 114.7794 456507.81 5428554.5 114.7794 456509.58 5428554.64 114.7794 456509.5719 5428554.7148 114.7794</gml:posList>
                    </gml:LinearRing>
                  </gml:interior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_5b4ebb82-f28d-4fbc-94b5-54d3f183ac0a_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>68.195</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>75.938</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>130.17</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.45</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>274.412</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>5b4ebb82-f28d-4fbc-94b5-54d3f183ac0a</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854943_1923">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_5b4ebb82-f28d-4fbc-94b5-54d3f183ac0a_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32954">
                      <gml:posList srsDimension="3">456488.3988 5428551.7173 130.17 456489.78599999996 5428569.4877 130.17 456490.0314 5428572.9238 130.16909999999996 456490.1485 5428574.5631 130.1693 456487.308 5428574.8004 129.45069999999998 456487.17 5428573.03 129.4509 456486.92 5428569.7299999995 129.45 456485.5158 5428551.7407 129.45 456488.3988 5428551.7173 130.17</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_372cb27c-2aee-4094-909f-5b0dff64a83f_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>42.504</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>50.259</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>127.32</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>274.084</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>372cb27c-2aee-4094-909f-5b0dff64a83f</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854943_1924">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_372cb27c-2aee-4094-909f-5b0dff64a83f_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32955">
                      <gml:posList srsDimension="3">456490.0366 5428539.5696 131.79 456490.8509 5428550.9725 131.79 456490.9027 5428551.6969 131.79 456488.3988 5428551.7173 129.7124 456485.5158 5428551.7407 127.32 456490.0366 5428539.5696 131.79</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_a6890f89-76cd-43f2-b2ae-df7a67483b63_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>40.066</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>55.768</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>130.169</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>274.087</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>a6890f89-76cd-43f2-b2ae-df7a67483b63</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854943_1925">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_a6890f89-76cd-43f2-b2ae-df7a67483b63_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32956">
                      <gml:posList srsDimension="3">456490.9027 5428551.6969 131.79 456492.1599 5428569.3005 131.79 456492.4107 5428572.8123 131.79 456492.52209999994 5428574.373 131.79 456490.1485 5428574.5631 130.1693 456490.0314 5428572.9238 130.16909999999996 456489.78599999996 5428569.4877 130.17 456490.9027 5428551.6969 131.79</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_4c71c32c-3cee-49e3-8770-09d597cde81f_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>68.89</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>68.897</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.27</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>94.101</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>4c71c32c-3cee-49e3-8770-09d597cde81f</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854943_1926">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_4c71c32c-3cee-49e3-8770-09d597cde81f_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32957">
                      <gml:posList srsDimension="3">456492.59 5428550.83 131.1146 456497.709 5428555.3185 129.27 456497.69 5428555.32 129.2773 456497.6928 5428555.3591 129.2773 456492.1599 5428569.3005 131.79 456490.9027 5428551.6969 131.79 456490.8509 5428550.9725 131.79 456492.59 5428550.83 131.1146</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_39e89252-8f30-4fc0-9d75-2580e7390358_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>364.348</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>39e89252-8f30-4fc0-9d75-2580e7390358</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854943_1927">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_39e89252-8f30-4fc0-9d75-2580e7390358_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32958">
                      <gml:posList srsDimension="3">456487.17 5428573.03 129.4509 456487.308 5428574.8004 129.45069999999998 456487.4991 5428577.2514 129.45 456489.0999999999 5428597.79 129.45 456489.0999999999 5428597.79 125.63 456489.0999999999 5428597.79 114.7794 456487.4991 5428577.2514 114.7794 456487.308 5428574.8004 114.7794 456487.17 5428573.03 114.7794 456487.17 5428573.03 129.4509</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_fe11ce09-5617-4fb0-afe9-7aa3ed702992_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>14.307</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>fe11ce09-5617-4fb0-afe9-7aa3ed702992</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854943_1928">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_fe11ce09-5617-4fb0-afe9-7aa3ed702992_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32959">
                      <gml:posList srsDimension="3">456498.03 5428544.41 126.9322 456498.04019999993 5428544.6541 126.7149 456498.041 5428544.6732 126.6931 456498.08 5428545.61 126.61999999999999 456498.08 5428545.61 114.7794 456498.041 5428544.6732 114.7794 456498.04019999993 5428544.6541 114.7794 456498.03 5428544.41 114.7794 456498.03 5428544.41 126.9322</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_614c5b36-b783-4936-aed9-5aab16023681_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>52.007</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>614c5b36-b783-4936-aed9-5aab16023681</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854943_1929">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_614c5b36-b783-4936-aed9-5aab16023681_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32960">
                      <gml:posList srsDimension="3">456496.1391999999 5428597.1683 114.7794 456499.5278 5428596.874099999 114.7794 456499.69 5428596.859999999 114.7794 456499.69 5428596.859999999 129.2887 456499.5278 5428596.874099999 129.289 456496.1391999999 5428597.1683 129.2963 456496.12 5428597.17 129.31039999999996 456496.12 5428597.17 114.7794 456496.1391999999 5428597.1683 114.7794</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_70e9d9bf-eb60-4aed-a151-def0e1b4d7b0_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.151</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>70e9d9bf-eb60-4aed-a151-def0e1b4d7b0</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854943_1930">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_70e9d9bf-eb60-4aed-a151-def0e1b4d7b0_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32961">
                      <gml:posList srsDimension="3">456516.4937 5428549.3151 127.32 456516.6131 5428549.3552 127.32 456516.4937 5428549.3151 127.43639999999999 456514.1509 5428548.5278 129.72 456516.4937 5428549.3151 127.32</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_37e8a35d-6a72-4496-8af6-501e4f3616bb_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>3.419</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>37e8a35d-6a72-4496-8af6-501e4f3616bb</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854943_1931">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_37e8a35d-6a72-4496-8af6-501e4f3616bb_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32962">
                      <gml:posList srsDimension="3">456509.6436 5428594.2355 129.7275 456513.6646 5428592.2921 125.709 456513.6646 5428592.2921 125.63 456509.6436 5428594.2355 128.9459 456506.1948 5428595.9025 131.79 456509.6436 5428594.2355 129.7275</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_af12b756-e8a7-406a-860c-729ff8b80803_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>22.89</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>af12b756-e8a7-406a-860c-729ff8b80803</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854943_1932">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_af12b756-e8a7-406a-860c-729ff8b80803_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32963">
                      <gml:posList srsDimension="3">456503.83 5428589.79 114.7794 456505.5879 5428589.9649 114.7794 456505.74 5428589.9799999995 114.7794 456505.74 5428589.9799999995 126.7366 456505.5879 5428589.9649 126.746 456503.83 5428589.79 126.6568 456503.83 5428589.79 114.7794</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_0d6bcad7-dd57-4d59-a1f8-9b8efa2f4b52_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.13</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>0d6bcad7-dd57-4d59-a1f8-9b8efa2f4b52</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854943_1933">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_0d6bcad7-dd57-4d59-a1f8-9b8efa2f4b52_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32964">
                      <gml:posList srsDimension="3">456503.1281 5428596.5359 129.3525 456506.1708 5428596.2865 131.79 456503.1281 5428596.5359 129.27 456503.0251 5428596.5443 129.27 456503.1281 5428596.5359 129.3525</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_b5841488-3bc9-4b80-a57c-69d9e1d87f60_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>250.677</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>b5841488-3bc9-4b80-a57c-69d9e1d87f60</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854943_1934">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_b5841488-3bc9-4b80-a57c-69d9e1d87f60_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32965">
                      <gml:posList srsDimension="3">456494.87 5428579.94 114.7794 456496.1125 5428597.0671 114.7794 456496.12 5428597.17 114.7794 456496.12 5428597.17 129.31039999999996 456496.1125 5428597.0671 129.31 456494.87 5428579.94 129.27 456494.87 5428579.94 114.7794</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_41ba9404-4141-4c9a-848b-ea18ba66334c_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.936</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>41ba9404-4141-4c9a-848b-ea18ba66334c</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854943_1935">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_41ba9404-4141-4c9a-848b-ea18ba66334c_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32966">
                      <gml:posList srsDimension="3">456497.709 5428555.3185 129.27 456497.709 5428555.3185 115.00999999999999 456500.56 5428555.09 115.00999999999999 456500.56 5428555.09 114.7794 456497.709 5428555.3185 114.7794 456497.69 5428555.32 114.7794 456497.69 5428555.32 129.2773 456497.709 5428555.3185 129.27</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_46e0dfbd-b052-4c7e-916d-3f2a0b3e06c8_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>195.482</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>46e0dfbd-b052-4c7e-916d-3f2a0b3e06c8</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854943_1936">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_46e0dfbd-b052-4c7e-916d-3f2a0b3e06c8_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32967">
                      <gml:posList srsDimension="3">456497.69 5428555.32 114.7794 456497.6928 5428555.3591 114.7794 456498.66 5428568.76 114.7794 456498.66 5428568.76 129.2957 456497.6928 5428555.3591 129.2773 456497.69 5428555.32 129.2773 456497.69 5428555.32 114.7794</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_5bb93174-8ae0-46ba-8045-ea191aedcba5_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>0.082</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>5bb93174-8ae0-46ba-8045-ea191aedcba5</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854943_1937">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_5bb93174-8ae0-46ba-8045-ea191aedcba5_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32968">
                      <gml:posList srsDimension="3">456499.4941 5428538.8242 131.8016 456499.89499999996 5428538.7926 131.8021 456499.4941 5428538.8242 131.7851 456490.0366 5428539.5696 131.79 456499.4941 5428538.8242 131.8016</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_78de3743-c8ba-498f-8647-903b4ab83320_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>5.762</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>78de3743-c8ba-498f-8647-903b4ab83320</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854943_1938">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_78de3743-c8ba-498f-8647-903b4ab83320_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32969">
                      <gml:posList srsDimension="3">456492.3765 5428545.147799999 126.6836 456492.71 5428545.12 126.6798 456492.71 5428545.12 114.7794 456492.3765 5428545.147799999 114.7794 456492.23 5428545.159999999 114.7794 456492.23 5428545.159999999 127.07119999999999 456492.3765 5428545.147799999 126.6836</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_279d49a7-5009-4b55-842c-bd615d10f4ea_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>68.716</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>279d49a7-5009-4b55-842c-bd615d10f4ea</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854943_1939">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_279d49a7-5009-4b55-842c-bd615d10f4ea_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32970">
                      <gml:posList srsDimension="3">456492.23 5428545.159999999 114.7794 456492.2664 5428545.7328 114.7794 456492.59 5428550.83 114.7794 456492.59 5428550.83 126.61999999999999 456492.2664 5428545.7328 127.0831 456492.23 5428545.159999999 127.07119999999999 456492.23 5428545.159999999 114.7794</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_da55189e-6a90-45ce-b4f4-7a6cf088c36c_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>9.358</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>da55189e-6a90-45ce-b4f4-7a6cf088c36c</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854943_1940">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_da55189e-6a90-45ce-b4f4-7a6cf088c36c_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32971">
                      <gml:posList srsDimension="3">456489.8475 5428597.101 130.17 456492.5166 5428594.6409 131.7908 456489.8475 5428597.101 126.9778 456489.0999999999 5428597.79 125.63 456489.0999999999 5428597.79 129.45 456489.8475 5428597.101 130.17</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_b30d19ea-a305-4dce-a0f3-084b012e0aaf_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>4.303</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>b30d19ea-a305-4dce-a0f3-084b012e0aaf</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854943_1941">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_b30d19ea-a305-4dce-a0f3-084b012e0aaf_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32972">
                      <gml:posList srsDimension="3">456488.3988 5428551.7173 129.7124 456490.9027 5428551.6969 131.79 456488.3988 5428551.7173 130.17 456485.5158 5428551.7407 129.45 456485.5158 5428551.7407 127.32 456488.3988 5428551.7173 129.7124</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="bldg_DEBWL51140001vWd_520f1425-3619-429d-84e5-4095e4cadea9_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>455.797</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>520f1425-3619-429d-84e5-4095e4cadea9</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854943_1942">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_520f1425-3619-429d-84e5-4095e4cadea9_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32973">
                      <gml:posList srsDimension="3">456484.33 5428536.55 127.32 456485.5158 5428551.7407 127.32 456485.5158 5428551.7407 129.45 456486.92 5428569.7299999995 129.45 456486.92 5428569.7299999995 114.7794 456485.5158 5428551.7407 114.7794 456484.33 5428536.55 114.7794 456484.33 5428536.55 127.32</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_6169a1d1-8e91-4c64-82d6-2e802fc8f73d_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>13.453</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>49.571</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>128.946</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>86.431</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>6169a1d1-8e91-4c64-82d6-2e802fc8f73d</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854943_1943">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_6169a1d1-8e91-4c64-82d6-2e802fc8f73d_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32974">
                      <gml:posList srsDimension="3">456506.1708 5428596.2865 131.79 456506.1948 5428595.9025 131.79 456509.6436 5428594.2355 128.9459 456505.8129 5428602.0258 131.79 456506.1708 5428596.2865 131.79</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_c9cb86ac-2e5b-4140-af9d-1be5e17c3c2a_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>6.422</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>51.394</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>129.352</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>126.596</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>274.333</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>c9cb86ac-2e5b-4140-af9d-1be5e17c3c2a</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854943_1944">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_c9cb86ac-2e5b-4140-af9d-1be5e17c3c2a_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32975">
                      <gml:posList srsDimension="3">456503.1281 5428596.5359 129.3525 456503.0251 5428596.5443 129.27 456499.6874 5428596.8179 126.59609999999999 456499.50839999993 5428593.915599999 126.6287 456503.1281 5428596.5359 129.3525</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_86d34e33-2284-4586-b4b9-10fe22dd9e71_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>60.987</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>66.116</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.27</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>184.686</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>86d34e33-2284-4586-b4b9-10fe22dd9e71</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854943_1945">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_86d34e33-2284-4586-b4b9-10fe22dd9e71_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32976">
                      <gml:posList srsDimension="3">456496.1391999999 5428597.1683 129.2963 456499.5278 5428596.874099999 129.289 456499.69 5428596.859999999 129.2887 456499.6874 5428596.8179 129.27 456503.0251 5428596.5443 129.27 456503.1281 5428596.5359 129.27 456505.8129 5428602.0258 131.79 456493.1754 5428603.061599999 131.79 456496.1391999999 5428597.1683 129.2963</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_02db7db3-60d0-402a-8186-2c5993c3de35_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>54.286</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>53.886</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.791</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.27</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>94.377</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>02db7db3-60d0-402a-8186-2c5993c3de35</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854943_1946">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_02db7db3-60d0-402a-8186-2c5993c3de35_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32977">
                      <gml:posList srsDimension="3">456494.87 5428579.94 129.27 456496.1125 5428597.0671 129.31 456496.12 5428597.17 129.31039999999996 456496.1391999999 5428597.1683 129.2963 456493.1754 5428603.061599999 131.79 456492.5166 5428594.6409 131.7908 456494.87 5428579.94 129.27</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_7b176e8d-8e1b-4efa-aad8-b04dbbf9748e_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>18.046</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>69.209</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.79</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.314</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>94.084</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>7b176e8d-8e1b-4efa-aad8-b04dbbf9748e</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854943_1947">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_7b176e8d-8e1b-4efa-aad8-b04dbbf9748e_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32978">
                      <gml:posList srsDimension="3">456499.43239999993 5428579.5734 129.3135 456499.4367 5428579.634 129.3135 456499.4386 5428579.6609 129.3135 456492.52209999994 5428574.373 131.79 456492.4107 5428572.8123 131.79 456492.1599 5428569.3005 131.79 456499.43239999993 5428579.5734 129.3135</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_82303869-c70b-4e65-8907-20684e6beae6_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>23.141</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>47.969</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>130.17</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>129.45</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>274.458</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>82303869-c70b-4e65-8907-20684e6beae6</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854943_1948">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_82303869-c70b-4e65-8907-20684e6beae6_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32979">
                      <gml:posList srsDimension="3">456488.2949 5428577.1826 130.17 456489.8475 5428597.101 130.17 456489.0999999999 5428597.79 129.45 456487.4991 5428577.2514 129.45 456487.308 5428574.8004 129.45069999999998 456488.2949 5428577.1826 130.17</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="bldg_DEBWL51140001vWd_c961d04e-9d6f-48d6-8f9c-83cca2cdad74_2">
          <gen:doubleAttribute name="Flaeche">
            <gen:value>74.17</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachneigung">
            <gen:value>48.031</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="FirsthoeheAbsolut">
            <gen:value>131.802</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="TraufhoeheAbsolut">
            <gen:value>127.32</gen:value>
          </gen:doubleAttribute>
          <gen:doubleAttribute name="Dachorientierung">
            <gen:value>184.584</gen:value>
          </gen:doubleAttribute>
          <gen:stringAttribute name="FaceUUID">
            <gen:value>c961d04e-9d6f-48d6-8f9c-83cca2cdad74</gen:value>
          </gen:stringAttribute>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1572940854943_1949">
              <gml:surfaceMember>
                <gml:Polygon gml:id="bldg_DEBWL51140001vWd_c961d04e-9d6f-48d6-8f9c-83cca2cdad74_2_poly">
                  <gml:exterior>
                    <gml:LinearRing gml:id="CityDoctor_1572943559556_32980">
                      <gml:posList srsDimension="3">456512.0982 5428537.8006 131.79 456499.89499999996 5428538.7926 131.8021 456499.4941 5428538.8242 131.8016 456490.0366 5428539.5696 131.79 456485.72 5428534.93 127.32 456512.0982 5428537.8006 131.79</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:address xlink:href="#UUID_153fd038-7a82-4271-8441-17d157ef67b5"/>
    </bldg:Building>
  </core:cityObjectMember>
</core:CityModel>