<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<core:CityModel xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:tex="http://www.opengis.net/citygml/texturedsurface/2.0" xmlns:core="http://www.opengis.net/citygml/2.0" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/2.0" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:frn="http://www.opengis.net/citygml/cityfurniture/2.0" xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:qual="https://transfer.hft-stuttgart.de/pages/citydoctor/qualityade/0.1.3" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd http://www.opengis.net/citygml/appearance/2.0 http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/cityobjectgroup/2.0 http://schemas.opengis.net/citygml/cityobjectgroup/2.0/cityObjectGroup.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/cityfurniture/2.0 http://schemas.opengis.net/citygml/cityfurniture/2.0/cityFurniture.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd https://transfer.hft-stuttgart.de/pages/citydoctor/qualityade/0.1.3 https://transfer.hft-stuttgart.de/pages/citydoctor/qualityade/0.1.3/qualityAde.xsd http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd">
  <core:cityObjectMember>
    <bldg:Building gml:id="_Simple_BD.1">
      <qual:validationResult result="ERROR">
        <qual:GE_S_SELF_INTERSECTION>
          <qual:geometryId>CityDoctor_1618569814351_0</qual:geometryId>
          <qual:polygonId1>_Simple_BD.1_PG.1</qual:polygonId1>
          <qual:polygonId2>_Simple_BD.1_PG.6</qual:polygonId2>
        </qual:GE_S_SELF_INTERSECTION>
      </qual:validationResult>
      <bldg:lod2Solid>
        <gml:Solid gml:id="CityDoctor_1618569814351_0">
          <gml:exterior>
            <gml:CompositeSurface>
              <gml:surfaceMember xlink:href="#_Simple_BD.1_PG.1"/>
              <gml:surfaceMember xlink:href="#_Simple_BD.1_PG.2"/>
              <gml:surfaceMember xlink:href="#_Simple_BD.1_PG.3"/>
              <gml:surfaceMember xlink:href="#_Simple_BD.1_PG.4"/>
              <gml:surfaceMember xlink:href="#_Simple_BD.1_PG.5"/>
              <gml:surfaceMember xlink:href="#_Simple_BD.1_PG.6"/>
              <gml:surfaceMember xlink:href="#_Simple_BD.1_PG.7"/>
              <gml:surfaceMember xlink:href="#_Simple_BD.1_PG.8"/>
              <gml:surfaceMember xlink:href="#_Simple_BD.1_PG.9"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="_Simple_BD.1_WallSurface_1">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1618569814352_1">
              <gml:surfaceMember>
                <gml:Polygon gml:id="_Simple_BD.1_PG.2">
                  <gml:exterior>
                    <gml:LinearRing gml:id="_Simple_BD.1_PG.2_LR.1">
                      <gml:posList srsDimension="3">13.0 15.0 0.0 13.0 15.0 3.0 13.0 10.0 3.0 13.0 10.0 0.0 13.0 15.0 0.0</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="_Simple_BD.1_WallSurface_2">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1618569814352_2">
              <gml:surfaceMember>
                <gml:Polygon gml:id="_Simple_BD.1_PG.3">
                  <gml:exterior>
                    <gml:LinearRing gml:id="_Simple_BD.1_PG.3_LR.1">
                      <gml:posList srsDimension="3">10.0 15.0 0.0 10.0 15.0 3.0 13.0 15.0 3.0 13.0 15.0 0.0 10.0 15.0 0.0</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="_Simple_BD.1_WallSurface_3">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1618569814353_3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="_Simple_BD.1_PG.4">
                  <gml:exterior>
                    <gml:LinearRing gml:id="_Simple_BD.1_PG.4_LR.1">
                      <gml:posList srsDimension="3">10.0 10.0 3.0 10.0 15.0 3.0 10.0 15.0 0.0 10.0 10.0 0.0 10.0 10.0 3.0</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="_Simple_BD.1_WallSurface_4">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1618569814353_4">
              <gml:surfaceMember>
                <gml:Polygon gml:id="_Simple_BD.1_PG.5">
                  <gml:exterior>
                    <gml:LinearRing gml:id="_Simple_BD.1_PG.5_LR.1">
                      <gml:posList srsDimension="3">13.0 10.0 0.0 13.0 10.0 3.0 10.0 10.0 3.0 10.0 10.0 0.0 13.0 10.0 0.0</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="_Simple_BD.1_RoofSurface_1">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1618569814353_5">
              <gml:surfaceMember>
                <gml:Polygon gml:id="_Simple_BD.1_PG.6">
                  <gml:exterior>
                    <gml:LinearRing gml:id="_Simple_BD.1_PG.6_LR.1">
                      <gml:posList srsDimension="3">10.0 10.0 3.0 13.0 10.0 3.0 11.5 12.5 -1.0 10.0 10.0 3.0</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="_Simple_BD.1_PG.7">
                  <gml:exterior>
                    <gml:LinearRing gml:id="_Simple_BD.1_PG.7_LR.1">
                      <gml:posList srsDimension="3">13.0 10.0 3.0 13.0 15.0 3.0 11.5 12.5 -1.0 13.0 10.0 3.0</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="_Simple_BD.1_PG.8">
                  <gml:exterior>
                    <gml:LinearRing gml:id="_Simple_BD.1_PG.8_LR.1">
                      <gml:posList srsDimension="3">13.0 15.0 3.0 10.0 15.0 3.0 11.5 12.5 -1.0 13.0 15.0 3.0</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
              <gml:surfaceMember>
                <gml:Polygon gml:id="_Simple_BD.1_PG.9">
                  <gml:exterior>
                    <gml:LinearRing gml:id="_Simple_BD.1_PG.9_LR.1">
                      <gml:posList srsDimension="3">10.0 15.0 3.0 10.0 10.0 3.0 11.5 12.5 -1.0 10.0 15.0 3.0</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="_Simple_BD.1_GroundSurface_1">
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="CityDoctor_1618569814353_6">
              <gml:surfaceMember>
                <gml:Polygon gml:id="_Simple_BD.1_PG.1">
                  <gml:exterior>
                    <gml:LinearRing gml:id="_Simple_BD.1_PG.1_LR.1">
                      <gml:posList srsDimension="3">10.0 10.0 0.0 10.0 15.0 0.0 13.0 15.0 0.0 13.0 10.0 0.0 10.0 10.0 0.0</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
    </bldg:Building>
  </core:cityObjectMember>
  <qual:validation>
    <qual:validationDate>2021-04-16T12:43:32.176+02:00</qual:validationDate>
    <qual:validationSoftware>CityDoctor 3.9.0-SNAPSHOT-79d9364</qual:validationSoftware>
    <qual:validationPlan>
      <qual:globalParameters>
        <qual:parameter name="numberOfRoundingPlaces">8</qual:parameter>
        <qual:parameter name="minVertexDistance" uom="m">1.0E-4</qual:parameter>
        <qual:parameter name="schematronFile"/>
      </qual:globalParameters>
      <qual:requirement name="R_GE_R_TOO_FEW_POINTS" enabled="true"/>
      <qual:requirement name="R_GE_R_NOT_CLOSED" enabled="true"/>
      <qual:requirement name="R_GE_R_CONSECUTIVE_POINTS_SAME" enabled="true"/>
      <qual:requirement name="R_GE_R_SELF_INTERSECTION" enabled="true"/>
      <qual:requirement name="R_GE_S_MULTIPLE_CONNECTED_COMPONENTS" enabled="true"/>
      <qual:requirement name="R_GE_P_INTERIOR_DISCONNECTED" enabled="true"/>
      <qual:requirement name="R_GE_P_INTERSECTING_RINGS" enabled="true"/>
      <qual:requirement name="R_GE_P_NON_PLANAR" enabled="true">
        <qual:parameter name="type">distance</qual:parameter>
        <qual:parameter name="distanceTolerance" uom="m">0.01</qual:parameter>
        <qual:parameter name="angleTolerance" uom="deg">1</qual:parameter>
        <qual:parameter name="degeneratedPolygonTolerance" uom="m">0.00000</qual:parameter>
      </qual:requirement>
      <qual:requirement name="R_GE_P_HOLE_OUTSIDE" enabled="true"/>
      <qual:requirement name="R_GE_P_ORIENTATION_RINGS_SAME" enabled="true"/>
      <qual:requirement name="R_GE_P_INNER_RINGS_NESTED" enabled="true"/>
      <qual:requirement name="R_GE_S_TOO_FEW_POLYGONS" enabled="true"/>
      <qual:requirement name="R_GE_S_NOT_CLOSED" enabled="true"/>
      <qual:requirement name="R_GE_S_NON_MANIFOLD_EDGE" enabled="true"/>
      <qual:requirement name="R_GE_S_POLYGON_WRONG_ORIENTATION" enabled="true"/>
      <qual:requirement name="R_GE_S_ALL_POLYGONS_WRONG_ORIENTATION" enabled="true"/>
      <qual:requirement name="R_GE_S_NON_MANIFOLD_VERTEX" enabled="true"/>
      <qual:requirement name="R_GE_S_SELF_INTERSECTION" enabled="true"/>
      <qual:requirement name="R_SE_ATTRIBUTES_EXISTING" enabled="false"/>
      <qual:requirement name="R_SE_ATTRIBUTES_CORRECT" enabled="false"/>
      <qual:filter>
        <qual:checking>BUILDING</qual:checking>
        <qual:checking>BRIDGE</qual:checking>
        <qual:checking>LAND</qual:checking>
        <qual:checking>TRANSPORTATION</qual:checking>
        <qual:checking>VEGETATION</qual:checking>
        <qual:checking>WATER</qual:checking>
      </qual:filter>
    </qual:validationPlan>
    <qual:statistics>
      <qual:numErrorBuildings numChecked="1">1</qual:numErrorBuildings>
      <qual:numErrorVegetation numChecked="0">0</qual:numErrorVegetation>
      <qual:numErrorLandObjects numChecked="0">0</qual:numErrorLandObjects>
      <qual:numErrorBridgeObjects numChecked="0">0</qual:numErrorBridgeObjects>
      <qual:numErrorWaterObjects numChecked="0">0</qual:numErrorWaterObjects>
      <qual:numErrorTransportation numChecked="0">0</qual:numErrorTransportation>
      <qual:errorStatistics>
        <qual:error name="GE_S_SELF_INTERSECTION">1</qual:error>
      </qual:errorStatistics>
    </qual:statistics>
  </qual:validation>
</core:CityModel>