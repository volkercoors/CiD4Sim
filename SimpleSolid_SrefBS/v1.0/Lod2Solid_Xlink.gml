<?xml version="1.0" encoding="UTF-8"?>
<core:CityModel xmlns:brid="http://www.opengis.net/citygml/bridge/2.0" xmlns:tran="http://www.opengis.net/citygml/transportation/2.0" xmlns:frn="http://www.opengis.net/citygml/cityfurniture/2.0" xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0" xmlns:sch="http://www.ascc.net/xml/schematron" xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0" xmlns:tex="http://www.opengis.net/citygml/texturedsurface/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:dem="http://www.opengis.net/citygml/relief/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:luse="http://www.opengis.net/citygml/landuse/2.0" xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:smil20lang="http://www.w3.org/2001/SMIL20/Language" xmlns:pbase="http://www.opengis.net/citygml/profiles/base/2.0" xmlns:smil20="http://www.w3.org/2001/SMIL20/" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:core="http://www.opengis.net/citygml/2.0" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/2.0">
	<gml:boundedBy>
		<gml:Envelope srsDimension="3">
			<gml:lowerCorner>10 10 0</gml:lowerCorner>
			<gml:upperCorner>13 15 4.5</gml:upperCorner>
		</gml:Envelope>
	</gml:boundedBy>
	<core:cityObjectMember>
		<bldg:Building gml:id="_Simple_BD.1">
			<bldg:lod2Solid>
				<gml:Solid srsDimension="3">
					<gml:exterior>
						<gml:CompositeSurface>
							<gml:surfaceMember xlink:href="_Simple_BD.1_PG.3"/>
							<gml:surfaceMember xlink:href="_Simple_BD.1_PG.7"/>
							<gml:surfaceMember xlink:href="_Simple_BD.1_PG.2"/>
							<gml:surfaceMember xlink:href="_Simple_BD.1_PG.6"/>
							<gml:surfaceMember xlink:href="_Simple_BD.1_PG.4"/>
							<gml:surfaceMember xlink:href="_Simple_BD.1_PG.5"/>
							<gml:surfaceMember xlink:href="_Simple_BD.1_PG.1"/>
						</gml:CompositeSurface>
					</gml:exterior>
				</gml:Solid>
			</bldg:lod2Solid>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="_Simple_BD.1_WallSurface_1">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon gml:id="_Simple_BD.1_PG.2">
									<gml:exterior>
										<gml:LinearRing gml:id="_Simple_BD.1_PG.2_LR.1">
											<gml:posList>13 15 0 13 15 3 13 10 3 13 10 0 13 15 0</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="_Simple_BD.1_WallSurface_2">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon gml:id="_Simple_BD.1_PG.3">
									<gml:exterior>
										<gml:LinearRing gml:id="_Simple_BD.1_PG.3_LR.1">
											<gml:posList>10 15 0 10 15 3 11.5 15 4.5 13 15 3 13 15 0 10 15 0</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="_Simple_BD.1_WallSurface_3">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon gml:id="_Simple_BD.1_PG.4">
									<gml:exterior>
										<gml:LinearRing gml:id="_Simple_BD.1_PG.4_LR.1">
											<gml:posList>10 10 3 10 15 3 10 15 0 10 10 0 10 10 3</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="_Simple_BD.1_WallSurface_4">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon gml:id="_Simple_BD.1_PG.5">
									<gml:exterior>
										<gml:LinearRing gml:id="_Simple_BD.1_PG.5_LR.1">
											<gml:posList>13 10 0 13 10 3 11.5 10 4.5 10 10 3 10 10 0 13 10 0</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="_Simple_BD.1_RoofSurface_1">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon gml:id="_Simple_BD.1_PG.6">
									<gml:exterior>
										<gml:LinearRing gml:id="_Simple_BD.1_PG.6_LR.1">
											<gml:posList>10 10 3 11.5 10 4.5 11.5 15 4.5 10 15 3 10 10 3</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
							<gml:surfaceMember>
								<gml:Polygon gml:id="_Simple_BD.1_PG.7">
									<gml:exterior>
										<gml:LinearRing gml:id="_Simple_BD.1_PG.7_LR.1">
											<gml:posList>11.5 10 4.5 13 10 3 13 15 3 11.5 15 4.5 11.5 10 4.5</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="_Simple_BD.1_GroundSurface_1">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface srsDimension="3">
							<gml:surfaceMember>
								<gml:Polygon gml:id="_Simple_BD.1_PG.1">
									<gml:exterior>
										<gml:LinearRing gml:id="_Simple_BD.1_PG.1_LR.1">
											<gml:posList>10 10 0 10 15 0 13 15 0 13 10 0 10 10 0</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
		</bldg:Building>
	</core:cityObjectMember>
</core:CityModel>
