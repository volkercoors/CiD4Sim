﻿<?xml version="1.0" encoding="utf-8"?>
<CityModel xmlns:xlink="http://www.w3.org/1999/xlink">
<!--
Einfaches Gebäude mit Grundriss 3m x 5m und Satteldach, Traufhöhe 3m, Firsthöhe 4,5m
Modelliert mit Begrenzungsflächen (eine Dachfläche, 4 Wandflächen, 1 Grundfläche), die Gebäudegeometrie als Solid, der auf die Polygone der Begrenzungsflächen referenziert 
CityGML 2.0

no name spaces due to some issues with namespaces in liquid-technology web XML Schematron validator. 
https://www.liquid-technologies.com/online-schematron-validator

Just a draft to test schematron validation

http://dh.obdurodon.org/schematron-intro.xhtml

Author: V. Coors
-->

	<cityObjectMember>
		<Building id="_Simple_BD.1">
			<lod2Solid>
				<Solid>
					<exterior>
						<CompositeSurface>
							<surfaceMember xlink:href="#_Simple_BD.1_PG.1"/>
							<surfaceMember xlink:href="#_Simple_BD.1_PG.2"/>
							<surfaceMember xlink:href="#_Simple_BD.1_PG.3"/>
							<surfaceMember xlink:href="#_Simple_BD.1_PG.4"/>
							<surfaceMember xlink:href="#_Simple_BD.1_PG.5"/>
							<surfaceMember xlink:href="#_Simple_BD.1_PG.6"/>
							<surfaceMember xlink:href="#_Simple_BD.1_PG.7"/>
						</CompositeSurface>
					</exterior>
				</Solid>
			</lod2Solid>
			<BoundedBy>
				<WallSurface id="_Simple_BD.1_WallSurface_1">
					<lod2MultiSurface>
						<MultiSurface>
							<surfaceMember>
								<Polygon id="_Simple_BD.1_PG.2">
									<exterior>
										<LinearRing id="_Simple_BD.1_PG.2_LR.1">
											<posList srsDimension="3">
												13.0 15.0 0.0
												13.0 15.0 3.0
												13.0 10.0 3.0
												13.0 10.0 0.0
												13.0 15.0 0.0
											</posList>
										</LinearRing>
									</exterior>
								</Polygon>
							</surfaceMember>
						</MultiSurface>
					</lod2MultiSurface>
				</WallSurface>
			</BoundedBy>
			<BoundedBy>
				<WallSurface id="_Simple_BD.1_WallSurface_2">
					<lod2MultiSurface>
						<MultiSurface>
							<surfaceMember>
								<Polygon id="_Simple_BD.1_PG.3">
									<exterior>
										<LinearRing id="_Simple_BD.1_PG.3_LR.1">
											<posList srsDimension="3">
												10.0 15.0 0.0
												10.0 15.0 3.0
												11.5 15.0 4.5
												13.0 15.0 3.0
												13.0 15.0 0.0
												10.0 15.0 0.0
											</posList>
										</LinearRing>
									</exterior>
								</Polygon>
							</surfaceMember>
						</MultiSurface>
					</lod2MultiSurface>
				</WallSurface>
			</BoundedBy>
			<BoundedBy>
				<WallSurface id="_Simple_BD.1_WallSurface_3">
					<lod2MultiSurface>
						<MultiSurface>
							<surfaceMember>
								<Polygon id="_Simple_BD.1_PG.4">
									<exterior>
										<LinearRing id="_Simple_BD.1_PG.4_LR.1">
											<posList srsDimension="3">
												10.0 10.0 3.0
												10.0 15.0 3.0
												10.0 15.0 0.0
												10.0 10.0 0.0
												10.0 10.0 3.0
											</posList>
										</LinearRing>
									</exterior>
								</Polygon>
							</surfaceMember>
						</MultiSurface>
					</lod2MultiSurface>
				</WallSurface>
			</BoundedBy>
			<BoundedBy>
				<WallSurface id="_Simple_BD.1_WallSurface_4">
					<lod2MultiSurface>
						<MultiSurface>
							<surfaceMember>
								<Polygon id="_Simple_BD.1_PG.5">
									<exterior>
										<LinearRing id="_Simple_BD.1_PG.5_LR.1">
											<posList srsDimension="3">
												13.0 10.0 0.0
												13.0 10.0 3.0
												11.5 10.0 4.5
												10.0 10.0 3.0
												10.0 10.0 0.0
												13.0 10.0 0.0
											</posList>
										</LinearRing>
									</exterior>
								</Polygon>
							</surfaceMember>
						</MultiSurface>
					</lod2MultiSurface>
				</WallSurface>
			</BoundedBy>
			<BoundedBy>
				<RoofSurface id="_Simple_BD.1_RoofSurface_1">
					<lod2MultiSurface>
						<MultiSurface>
							<surfaceMember>
								<Polygon id="_Simple_BD.1_PG.6">
									<exterior>
										<LinearRing id="_Simple_BD.1_PG.6_LR.1">
											<posList srsDimension="3">
												10.0 10.0 3.0
												11.5 10.0 4.5
												11.5 15.0 4.5
												10.0 15.0 3.0
												10.0 10.0 3.0
											</posList>
										</LinearRing>
									</exterior>
								</Polygon>
							</surfaceMember>
							<surfaceMember>
								<Polygon id="_Simple_BD.1_PG.7">
									<exterior>
										<LinearRing id="_Simple_BD.1_PG.7_LR.1">
											<posList srsDimension="3">
												11.5 10.0 4.5
												13.0 10.0 3.0
												13.0 15.0 3.0
												11.5 15.0 4.5
												11.5 10.0 4.5
											</posList>
										</LinearRing>
									</exterior>
								</Polygon>
							</surfaceMember>
						</MultiSurface>
					</lod2MultiSurface>
				</RoofSurface>
			</BoundedBy>
			<BoundedBy>
				<GroundSurface id="_Simple_BD.1_GroundSurface_1">
					<lod2MultiSurface>
						<MultiSurface>
							<surfaceMember>
								<Polygon id="_Simple_BD.1_PG.1">
									<exterior>
										<LinearRing id="_Simple_BD.1_PG.1_LR.1">
											<posList srsDimension="3">
												10.0 10.0 0.0
												10.0 15.0 0.0
												13.0 15.0 0.0
												13.0 10.0 0.0
												10.0 10.0 0.0
											</posList>
										</LinearRing>
									</exterior>
								</Polygon>
							</surfaceMember>
						</MultiSurface>
					</lod2MultiSurface>
				</GroundSurface>
			</BoundedBy>
		</Building>
	</cityObjectMember>
</CityModel>