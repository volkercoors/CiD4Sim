<?xml version="1.0" encoding="UTF-8"?>
<CityModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns="http://www.opengis.net/citygml/2.0"
	xmlns:gml="http://www.opengis.net/gml"
	xmlns:xlink="http://www.w3.org/1999/xlink"
	xmlns:bldg="http://www.opengis.net/citygml/building/2.0"
	xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0"
	xsi:schemaLocation="http://www.w3.org/2001/XMLSchema-instance http://schemas.opengis.net/citygml/profiles/base/2.0/CityGML.xsd
						  http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd ">

	<!-- dwagner, 15.06.2012 Simple building with saddle roof Local coordinates -->

	<cityObjectMember>
		<bldg:Building gml:id="Test-001-Solid-SimpleBldg">
			<bldg:boundedBy>
				<bldg:RoofSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface>
							<gml:surfaceMember xlink:href="#p_r_1">
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface>
							<gml:surfaceMember xlink:href="#p_r_2">
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>

			<bldg:boundedBy>
				<bldg:WallSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface>
							<gml:surfaceMember xlink:href="#p_w_1">
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface>
							<gml:surfaceMember xlink:href="#p_w_2">
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface>
							<gml:surfaceMember xlink:href="#p_w_3">
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface>
							<gml:surfaceMember xlink:href="#p_w_4">
							</gml:surfaceMember>
							<gml:surfaceMember xlink:href="#p_w_5">
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>

			<bldg:boundedBy>
				<bldg:GroundSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface>
							<gml:surfaceMember xlink:href="#p_g_1">
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>

			<bldg:function>1121</bldg:function>
			<bldg:yearOfConstruction>1978</bldg:yearOfConstruction>
			
			<bldg:lod2Solid>
				<gml:Solid>
					<gml:exterior>
						<gml:CompositeSurface>
							<!-- WallSurface -->
							<gml:surfaceMember>
								<gml:Polygon gml:id="p_w_1">
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>
												0.00 0.00 0.00
												2.00 0.00 0.00
												2.00 0.00 3.00
												1.00 0.00 3.50
												0.00 0.00 3.00
												0.00 0.00 0.00
											</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
							<gml:surfaceMember>
								<gml:Polygon gml:id="p_w_2">
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>
												2.00 0.00 0.00
												2.00 2.00 0.00
												2.00 2.00 3.00
												2.00 0.00 3.00
												2.00 0.00 0.00
											</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
							<gml:surfaceMember>
								<gml:Polygon gml:id="p_w_3">
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>
												2.00 2.00 0.00
												0.00 2.00 0.00
												0.00 2.00 3.00
												1.00 2.00 3.50
												2.00 2.00 3.00
												2.00 2.00 0.00
											</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
							<gml:surfaceMember>
								<gml:Polygon gml:id="p_w_4">
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>
												0.00 2.00 0.00
												0.00 0.00 0.00
												0.00 0.00 3.00
												0.00 2.00 0.00
											</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
							<gml:surfaceMember>
								<gml:Polygon gml:id="p_w_5">
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>
												0.00 2.00 0.00
												0.00 0.00 3.00
												0.00 2.00 3.00
												0.00 2.00 0.00
											</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
							<!-- GroundSurface -->
							<gml:surfaceMember>
								<gml:Polygon gml:id="p_g_1">
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>
												0.00 0.00 0.00
												0.00 2.00 0.00
												2.00 2.00 0.00
												2.00 0.00 0.00
												0.00 0.00 0.00
											</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
							<!-- RoofSurface -->
							<gml:surfaceMember>
								<gml:Polygon gml:id="p_r_1">
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>
												0.00 0.00 3.00
												1.00 0.00 3.50
												1.00 2.00 3.50
												0.00 2.00 3.00
												0.00 0.00 3.00
											</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
							<gml:surfaceMember>
								<gml:Polygon gml:id="p_r_2">
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>
												2.00 0.00 3.00
												2.00 2.00 3.00
												1.00 2.00 3.50
												1.00 0.00 3.50
												2.00 0.00 3.00
											</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:CompositeSurface>
					</gml:exterior>
				</gml:Solid>
			</bldg:lod2Solid>
		</bldg:Building>
</cityObjectMember>

<cityObjectMember>
		<bldg:Building gml:id="Test-002-Solid-SimpleBldg">
			<bldg:boundedBy>
				<bldg:RoofSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface>
							<gml:surfaceMember xlink:href="#b2_p_r_1">
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface>
							<gml:surfaceMember xlink:href="#b2_p_r_2">
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>

			<bldg:boundedBy>
				<bldg:WallSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface>
							<gml:surfaceMember xlink:href="#b2_p_w_1">
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface>
							<gml:surfaceMember xlink:href="#b2_p_w_2">
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface>
							<gml:surfaceMember xlink:href="#b2_p_w_3">
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface>
							<gml:surfaceMember xlink:href="#b2_p_w_4">
							</gml:surfaceMember>
							<gml:surfaceMember xlink:href="#b2_p_w_5">
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>

			<bldg:boundedBy>
				<bldg:GroundSurface>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface>
							<gml:surfaceMember xlink:href="#b2_p_g_1">
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>

			<bldg:function>3010</bldg:function>
			<bldg:yearOfConstruction>1948</bldg:yearOfConstruction>

			<bldg:lod2Solid>
				<gml:Solid>
					<gml:exterior>
						<gml:CompositeSurface>
							<!-- WallSurface -->
							<gml:surfaceMember>
								<gml:Polygon gml:id="b2_p_w_1">
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>
												2.00 0.00 0.00
												4.00 0.00 0.00
												4.00 0.00 3.00
												3.00 0.00 3.50
												2.00 0.00 3.00
												2.00 0.00 0.00
											</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
							<gml:surfaceMember>
								<gml:Polygon gml:id="b2_p_w_2">
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>
												4.00 0.00 0.00
												4.00 2.00 0.00
												4.00 2.00 3.00
												4.00 0.00 3.00
												4.00 0.00 0.00
											</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
							<gml:surfaceMember>
								<gml:Polygon gml:id="b2_p_w_3">
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>
												4.00 2.00 0.00
												2.00 2.00 0.00
												2.00 2.00 3.00
												3.00 2.00 3.50
												4.00 2.00 3.00
												4.00 2.00 0.00
											</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
							<gml:surfaceMember>
								<gml:Polygon gml:id="b2_p_w_4">
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>
												2.00 2.00 0.00
												2.00 0.00 0.00
												2.00 0.00 3.00
												2.00 2.00 0.00
											</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
							<gml:surfaceMember>
								<gml:Polygon gml:id="b2_p_w_5">
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>
												2.00 2.00 0.00
												2.00 0.00 3.00
												2.00 2.00 3.00
												2.00 2.00 0.00
											</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
							<!-- GroundSurface -->
							<gml:surfaceMember>
								<gml:Polygon gml:id="b2_p_g_1">
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>
												2.00 0.00 0.00
												2.00 2.00 0.00
												4.00 2.00 0.00
												4.00 0.00 0.00
												2.00 0.00 0.00
											</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
							<!-- RoofSurface -->
							<gml:surfaceMember>
								<gml:Polygon gml:id="b2_p_r_1">
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>
												2.00 0.00 3.00
												3.00 0.00 3.50
												3.00 2.00 3.50
												2.00 2.00 3.00
												2.00 0.00 3.00
											</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
							<gml:surfaceMember>
								<gml:Polygon gml:id="b2_p_r_2">
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList>
												4.00 0.00 3.00
												4.00 2.00 3.00
												3.00 2.00 3.50
												3.00 0.00 3.50
												4.00 0.00 3.00
											</gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:CompositeSurface>
					</gml:exterior>
				</gml:Solid>
			</bldg:lod2Solid>
		</bldg:Building>

	</cityObjectMember>

</CityModel>
