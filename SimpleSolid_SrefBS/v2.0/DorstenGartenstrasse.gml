<?xml version="1.0" encoding="UTF-8"?>
<!--SGJ3D Exporter 3.3.8 (http://www.cpa-redev.de) -->
<core:CityModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
           xmlns:core="http://www.opengis.net/citygml/2.0"
           xmlns:gml="http://www.opengis.net/gml"
           xmlns:xlink="http://www.w3.org/1999/xlink"
           xmlns:tran="http://www.opengis.net/citygml/transportation/2.0"
           xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0"
           xmlns:smil20lang="http://www.w3.org/2001/SMIL20/Language"
           xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/2.0"
           xmlns:luse="http://www.opengis.net/citygml/landuse/2.0"
           xmlns:frn="http://www.opengis.net/citygml/cityfurniture/2.0"
           xmlns:app="http://www.opengis.net/citygml/appearance/2.0"
           xmlns:tex="http://www.opengis.net/citygml/texturedsurface/2.0"
           xmlns:smil20="http://www.w3.org/2001/SMIL20/"
           xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0"
           xmlns:bldg="http://www.opengis.net/citygml/building/2.0"
           xmlns:dem="http://www.opengis.net/citygml/relief/2.0"
           xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0"
           xmlns:brid="http://www.opengis.net/citygml/bridge/2.0"
           xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0"
           xmlns:gen="http://www.opengis.net/citygml/generics/2.0"
           xmlns:wfs="http://www.opengis.net/wfs"
           xsi:schemaLocation="http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd http://www.opengis.net/citygml/cityfurniture/2.0 http://schemas.opengis.net/citygml/cityfurniture/2.0/cityFurniture.xsd http://www.opengis.net/citygml/appearance/2.0 http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/cityobjectgroup/2.0 http://schemas.opengis.net/citygml/cityobjectgroup/2.0/cityObjectGroup.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd">
<gml:description>D:\Programme\Apache Software Foundation\Tomcat 8.5\temp\export8777433398796633783\export.gml</gml:description>
<gml:name>D:\Programme\Apache Software Foundation\Tomcat 8.5\temp\export8777433398796633783\export.gml</gml:name>
<gml:boundedBy>
	<gml:Envelope srsDimension="3" srsName="urn:ogc:def:crs:EPSG::25832">
		<gml:lowerCorner>359012.59 5726387.27 0.00</gml:lowerCorner>
		<gml:upperCorner>359058.14 5726426.28 0.00</gml:upperCorner>
	</gml:Envelope>
</gml:boundedBy>
<core:cityObjectMember>
<bldg:Building gml:id="DENW_198da89b-0766-49a8-b0ed-45e022201d70">
	<core:creationDate>2018-09-19</core:creationDate>
	<core:externalReference>
		<core:informationSystem>http://www.adv-online.de/fdv/art.htm#_9100</core:informationSystem>
		<core:externalObject>
			<core:name>DENW52AL000hnBCF</core:name>
		</core:externalObject>
	</core:externalReference>
	<gen:stringAttribute name="DatenquelleDachhoehe">
		<gen:value>1000</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="DatenquelleLage">
		<gen:value>1000</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="DatenquelleBodenhoehe">
		<gen:value>1100</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="Gemeindeschluessel">
		<gen:value>05562012</gen:value>
	</gen:stringAttribute>
	<bldg:measuredHeight uom="urn:ogc:def:uom:UCUM::m">2.583</bldg:measuredHeight>
	<bldg:lod2Solid>
		<gml:Solid gml:id="IDGeo65fd6f78-8dc1-4eac-a4ae-43a031bc07c4">
			<gml:exterior>
				<gml:CompositeSurface>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596253"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596254"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596255"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596256"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596257"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596258"/>
				</gml:CompositeSurface>
			</gml:exterior>
		</gml:Solid>
	</bldg:lod2Solid>
	<bldg:boundedBy>
		<bldg:WallSurface>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoe3a3a917-4553-4429-b382-fea91a566525">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596253">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596253">
									<gml:posList srsDimension="3">359043.469000 5726401.843000 30.710000 359044.347000 5726397.980000 30.710000 359044.347000 5726397.980000 28.127000 359043.469000 5726401.843000 28.127000 359043.469000 5726401.843000 30.710000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo9dab43cd-f0c2-45ab-93c5-83d51cdee8e0">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596254">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596254">
									<gml:posList srsDimension="3">359044.347000 5726397.980000 30.710000 359037.913000 5726396.354000 30.710000 359037.913000 5726396.354000 28.127000 359044.347000 5726397.980000 28.127000 359044.347000 5726397.980000 30.710000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoed098498-9970-4ee2-bca8-7fa2f8dae332">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596255">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596255">
									<gml:posList srsDimension="3">359037.913000 5726396.354000 30.710000 359037.022000 5726400.213000 30.710000 359037.022000 5726400.213000 28.127000 359037.913000 5726396.354000 28.127000 359037.913000 5726396.354000 30.710000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoa2a4956b-e610-4fc6-ab28-02dc33237342">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596256">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596256">
									<gml:posList srsDimension="3">359037.022000 5726400.213000 30.710000 359043.469000 5726401.843000 30.710000 359043.469000 5726401.843000 28.127000 359037.022000 5726400.213000 28.127000 359037.022000 5726400.213000 30.710000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo41361770-de6c-49b3-bf40-fcd1b1d6bb95">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596257">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596257">
									<gml:posList srsDimension="3">359037.022000 5726400.213000 30.710000 359037.913000 5726396.354000 30.710000 359044.347000 5726397.980000 30.710000 359043.469000 5726401.843000 30.710000 359037.022000 5726400.213000 30.710000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:GroundSurface>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoa3cf281c-bc3f-46b6-b866-f1ddeaa7b157">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596258">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596258">
									<gml:posList srsDimension="3">359043.469000 5726401.843000 28.127000 359044.347000 5726397.980000 28.127000 359037.913000 5726396.354000 28.127000 359037.022000 5726400.213000 28.127000 359043.469000 5726401.843000 28.127000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:GroundSurface>
	</bldg:boundedBy>
	<bldg:address>
		<core:Address>
			<core:xalAddress>
				<xAL:AddressDetails>
					<xAL:Country>
						<xAL:CountryName>
							Germany
						</xAL:CountryName>
						<xAL:Locality  Type="Town">
							<xAL:LocalityName>
								Dorsten
							</xAL:LocalityName>
							<xAL:Thoroughfare>
								<xAL:ThoroughfareNumber>
									14
								</xAL:ThoroughfareNumber>
								<xAL:ThoroughfareName>
									Gartenstra�e
								</xAL:ThoroughfareName>
							</xAL:Thoroughfare>
							<xAL:PostalCode>
								<xAL:PostalCodeNumber>
									46284
								</xAL:PostalCodeNumber>
							</xAL:PostalCode>
						</xAL:Locality>
					</xAL:Country>
				</xAL:AddressDetails>
			</core:xalAddress>
			<core:multiPoint>
				<gml:MultiPoint>
					<gml:pointMember>
						<gml:Point>
							<gml:pos>359040.688 5726399.0 0.0</gml:pos>
						</gml:Point>
					</gml:pointMember>
				</gml:MultiPoint>
			</core:multiPoint>
		</core:Address>
	</bldg:address>
</bldg:Building>
</core:cityObjectMember>
<core:cityObjectMember>
<bldg:Building gml:id="DENW_09c4ab00-514f-4a98-a609-31f3b8798d56">
	<core:creationDate>2018-09-19</core:creationDate>
	<core:externalReference>
		<core:informationSystem>http://www.adv-online.de/fdv/art.htm#_9100</core:informationSystem>
		<core:externalObject>
			<core:name>DENW52AL000hnFwq</core:name>
		</core:externalObject>
	</core:externalReference>
	<gen:stringAttribute name="DatenquelleDachhoehe">
		<gen:value>6000</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="DatenquelleLage">
		<gen:value>1000</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="DatenquelleBodenhoehe">
		<gen:value>1100</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="Gemeindeschluessel">
		<gen:value>05562012</gen:value>
	</gen:stringAttribute>
	<bldg:measuredHeight uom="urn:ogc:def:uom:UCUM::m">2.466</bldg:measuredHeight>
	<bldg:lod2Solid>
		<gml:Solid gml:id="IDGeo831579ef-f32e-4328-b85f-6fb81c78fe5f">
			<gml:exterior>
				<gml:CompositeSurface>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596291"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596292"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596293"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596294"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596295"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596296"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596297"/>
				</gml:CompositeSurface>
			</gml:exterior>
		</gml:Solid>
	</bldg:lod2Solid>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_124_31459_247754">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo0469cf5a-04e9-4ee6-8505-204dcd44f37d">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596291">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596291">
									<gml:posList srsDimension="3">359017.529000 5726423.726000 29.780000 359018.011000 5726421.804000 29.780000 359018.011000 5726421.804000 27.985000 359017.529000 5726423.726000 27.985000 359017.529000 5726423.726000 29.780000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_263_460785_381231">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo7fd59acb-030f-4fdf-b048-4ef9da78d428">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596292">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596292">
									<gml:posList srsDimension="3">359015.566000 5726421.185000 29.780000 359015.089000 5726422.966000 29.780000 359015.089000 5726422.966000 27.985000 359015.566000 5726421.185000 27.985000 359015.566000 5726421.185000 29.780000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:GroundSurface gml:id="UUID_GroundSurface_1930_250125_251125">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo9c65dd7d-5774-4346-99dc-6e3918e2a086">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596293">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596293">
									<gml:posList srsDimension="3">359017.529000 5726423.726000 27.985000 359018.011000 5726421.804000 27.985000 359015.566000 5726421.185000 27.985000 359015.089000 5726422.966000 27.985000 359017.529000 5726423.726000 27.985000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:GroundSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_276_227474_198644">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo467f4394-7899-4550-98ba-eff2e098baec">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596294">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596294">
									<gml:posList srsDimension="3">359015.089000 5726422.966000 27.985000 359015.089000 5726422.966000 29.780000 359016.189090 5726423.308651 30.451000 359017.529000 5726423.726000 29.780000 359017.529000 5726423.726000 27.985000 359015.089000 5726422.966000 27.985000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1568_162150_336599">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo2cba40eb-ac23-48a0-9c4e-66f14ba55bfb">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596295">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596295">
									<gml:posList srsDimension="3">359018.011000 5726421.804000 27.985000 359018.011000 5726421.804000 29.780000 359016.668867 5726421.464213 30.442998 359015.566000 5726421.185000 29.780000 359015.566000 5726421.185000 27.985000 359018.011000 5726421.804000 27.985000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_79_555077_252154">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoe02bfb7d-05a3-4d0c-b90d-b923e64755dd">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596296">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596296">
									<gml:posList srsDimension="3">359016.668867 5726421.464213 30.442998 359016.189090 5726423.308651 30.451000 359015.089000 5726422.966000 29.780000 359015.566000 5726421.185000 29.780000 359016.668867 5726421.464213 30.442998 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_334_871675_383567">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoadf01a9f-9b25-4864-a26c-d4c636a79c3b">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596297">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596297">
									<gml:posList srsDimension="3">359016.189090 5726423.308651 30.451000 359016.668867 5726421.464213 30.442998 359018.011000 5726421.804000 29.780000 359017.529000 5726423.726000 29.780000 359016.189090 5726423.308651 30.451000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:address>
		<core:Address>
			<core:xalAddress>
				<xAL:AddressDetails>
					<xAL:Country>
						<xAL:CountryName>
							Germany
						</xAL:CountryName>
						<xAL:Locality  Type="Town">
							<xAL:LocalityName>
								Dorsten
							</xAL:LocalityName>
							<xAL:Thoroughfare>
								<xAL:ThoroughfareNumber>
									10
								</xAL:ThoroughfareNumber>
								<xAL:ThoroughfareName>
									Gartenstra�e
								</xAL:ThoroughfareName>
							</xAL:Thoroughfare>
							<xAL:PostalCode>
								<xAL:PostalCodeNumber>
									46284
								</xAL:PostalCodeNumber>
							</xAL:PostalCode>
						</xAL:Locality>
					</xAL:Country>
				</xAL:AddressDetails>
			</core:xalAddress>
			<core:multiPoint>
				<gml:MultiPoint>
					<gml:pointMember>
						<gml:Point>
							<gml:pos>359016.531 5726422.5 0.0</gml:pos>
						</gml:Point>
					</gml:pointMember>
				</gml:MultiPoint>
			</core:multiPoint>
		</core:Address>
	</bldg:address>
</bldg:Building>
</core:cityObjectMember>
<core:cityObjectMember>
<bldg:Building gml:id="DENW_f33fb41b-f067-418e-a39c-138f49856bdc">
	<core:creationDate>2018-09-19</core:creationDate>
	<core:externalReference>
		<core:informationSystem>http://www.adv-online.de/fdv/art.htm#_9100</core:informationSystem>
		<core:externalObject>
			<core:name>DENW52AL000hnBBx</core:name>
		</core:externalObject>
	</core:externalReference>
	<gen:stringAttribute name="DatenquelleDachhoehe">
		<gen:value>6000</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="DatenquelleLage">
		<gen:value>1000</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="DatenquelleBodenhoehe">
		<gen:value>1100</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="Gemeindeschluessel">
		<gen:value>05562012</gen:value>
	</gen:stringAttribute>
	<bldg:measuredHeight uom="urn:ogc:def:uom:UCUM::m">5.114</bldg:measuredHeight>
	<bldg:lod2Solid>
		<gml:Solid gml:id="IDGeo0f231454-8089-4472-bc15-30395107552d">
			<gml:exterior>
				<gml:CompositeSurface>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596327"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596328"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596329"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596330"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596331"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596332"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596333"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596334"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596335"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596336"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596337"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596338"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596339"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596340"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596341"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596342"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596343"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596344"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596345"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596346"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596347"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596348"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596349"/>
				</gml:CompositeSurface>
			</gml:exterior>
		</gml:Solid>
	</bldg:lod2Solid>
	<bldg:boundedBy>
		<bldg:WallSurface>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo6bd74b49-f732-4e78-8f73-205d5cb0f178">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596327">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596327">
									<gml:posList srsDimension="3">359035.678000 5726422.002000 32.097000 359041.159000 5726423.334000 32.097000 359041.159000 5726423.334000 27.976000 359035.678000 5726422.002000 27.976000 359035.678000 5726422.002000 32.097000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeob2a79f36-2f56-4d5e-ba54-d281bfcaf534">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596328">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596328">
									<gml:posList srsDimension="3">359041.159000 5726423.334000 32.097000 359041.274000 5726423.362000 32.058000 359041.274000 5726423.362000 27.976000 359041.159000 5726423.334000 27.976000 359041.159000 5726423.334000 32.097000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeobf5cc86a-371e-410b-9acc-68584e8814d9">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596329">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596329">
									<gml:posList srsDimension="3">359041.274000 5726423.362000 32.058000 359041.772000 5726421.443000 32.058000 359041.772000 5726421.443000 27.976000 359041.274000 5726423.362000 27.976000 359041.274000 5726423.362000 32.058000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo4a9f3c13-8db0-4a9e-821a-7bef5072ee04">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596330">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596330">
									<gml:posList srsDimension="3">359043.433000 5726414.537000 32.097000 359037.924000 5726413.143000 32.097000 359037.924000 5726413.143000 27.976000 359043.433000 5726414.537000 27.976000 359043.433000 5726414.537000 32.097000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoc72543c0-4ef8-452f-a475-ba7a43a181c9">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596331">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596331">
									<gml:posList srsDimension="3">359037.924000 5726413.143000 27.976000 359037.924000 5726413.143000 32.097000 359035.678000 5726422.002000 32.097000 359035.678000 5726422.002000 27.976000 359037.924000 5726413.143000 27.976000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo2c86bff2-cdfa-48b5-b90f-0e5a793662e9">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596332">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596332">
									<gml:posList srsDimension="3">359039.867000 5726416.281000 33.090000 359037.924000 5726413.143000 32.097000 359043.433000 5726414.537000 32.097000 359039.867000 5726416.281000 33.090000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_668_675841_257126">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo062a8056-7f3d-41f0-9c5b-8565c0692561">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596333">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596333">
									<gml:posList srsDimension="3">359041.159000 5726423.334000 32.097000 359041.655000 5726421.414000 32.097000 359041.274000 5726423.362000 32.058000 359041.159000 5726423.334000 32.097000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_64_101834_317518">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo0ec87854-5d53-44ed-b3e5-8dbf5eed82b7">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596334">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596334">
									<gml:posList srsDimension="3">359041.772000 5726421.443000 32.058000 359041.274000 5726423.362000 32.058000 359041.655000 5726421.414000 32.097000 359041.772000 5726421.443000 32.058000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_496_216002_171654">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoeb7063e4-19cb-4596-8509-f8e56ebeaad3">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596335">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596335">
									<gml:posList srsDimension="3">359038.862000 5726420.206000 31.590000 359041.774511 5726420.951751 31.590000 359041.774511 5726420.951751 33.090000 359038.862000 5726420.206000 33.090000 359038.862000 5726420.206000 31.590000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:GroundSurface>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeob2bfb938-b6d7-4071-9feb-44e1896b7bea">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596336">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596336">
									<gml:posList srsDimension="3">359041.772000 5726421.443000 27.976000 359041.655000 5726421.414000 27.976000 359043.433000 5726414.537000 27.976000 359037.924000 5726413.143000 27.976000 359035.678000 5726422.002000 27.976000 359041.159000 5726423.334000 27.976000 359041.274000 5726423.362000 27.976000 359041.772000 5726421.443000 27.976000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:GroundSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_109_350718_262221">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo20b0623b-d412-4879-b6ea-38971b9cc6f7">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596337">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596337">
									<gml:posList srsDimension="3">359038.862000 5726420.206000 33.090000 359039.867000 5726416.281000 33.090000 359039.867000 5726416.281000 31.590000 359038.862000 5726420.206000 31.590000 359038.862000 5726420.206000 33.090000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1642_12654_247506">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo46bcf64c-2866-43b4-8e6f-64553cc8c089">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596338">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596338">
									<gml:posList srsDimension="3">359042.788685 5726417.029100 33.090000 359042.788685 5726417.029100 31.590000 359039.867000 5726416.281000 31.590000 359039.867000 5726416.281000 33.090000 359042.788685 5726417.029100 33.090000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_616_197797_41545">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo9ed77265-da64-4d83-bcb1-c73a84af60f5">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596339">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596339">
									<gml:posList srsDimension="3">359043.433000 5726414.537000 32.097000 359042.788685 5726417.029100 33.090000 359039.867000 5726416.281000 33.090000 359043.433000 5726414.537000 32.097000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1905_872864_334738">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoe6d60dff-b7a1-405c-8c11-f3aadc5c24f1">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596340">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596340">
									<gml:posList srsDimension="3">359041.774511 5726420.951751 31.590000 359038.862000 5726420.206000 31.590000 359039.867000 5726416.281000 31.590000 359042.788685 5726417.029100 31.590000 359041.774511 5726420.951751 31.590000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo2d419a8e-06f7-453a-80cb-e3fe3b9249d5">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596341">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596341">
									<gml:posList srsDimension="3">359041.772000 5726421.443000 32.058000 359041.655000 5726421.414000 32.097000 359041.655000 5726421.414000 27.976000 359041.772000 5726421.443000 27.976000 359041.772000 5726421.443000 32.058000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1184_771856_400453">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo05fa9ae6-9654-487a-bac0-346b9c29b6c9">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596342">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596342">
									<gml:posList srsDimension="3">359041.655000 5726421.414000 32.097000 359041.749480 5726421.048568 32.097000 359041.749480 5726421.048568 33.090000 359041.774511 5726420.951751 33.090000 359041.774511 5726420.951751 31.590000 359042.788685 5726417.029100 31.590000 359042.788685 5726417.029100 33.090000 359043.433000 5726414.537000 32.097000 359043.433000 5726414.537000 27.976000 359041.655000 5726421.414000 27.976000 359041.655000 5726421.414000 32.097000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1223_394259_42160">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeofe3ceb87-2d6e-4f96-9978-c99889a8d079">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596343">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596343">
									<gml:posList srsDimension="3">359038.862000 5726420.206000 33.090000 359041.774511 5726420.951751 33.090000 359041.749480 5726421.048568 33.090000 359038.837195 5726420.302875 33.090000 359038.862000 5726420.206000 33.090000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1444_132121_17189">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo0fc7d245-7df5-430a-97ce-fd54f9450f7e">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596344">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596344">
									<gml:posList srsDimension="3">359038.837195 5726420.302875 33.090000 359038.837195 5726420.302875 33.050237 359038.862000 5726420.206000 33.090000 359038.837195 5726420.302875 33.090000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoea9f718b-d53a-4205-a3dd-1afc51723359">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596345">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596345">
									<gml:posList srsDimension="3">359035.678000 5726422.002000 32.097000 359038.862000 5726420.206000 33.090000 359038.837195 5726420.302875 33.050237 359038.955356 5726420.333130 33.049642 359041.159000 5726423.334000 32.097000 359035.678000 5726422.002000 32.097000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_225_40951_20539">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoc6e5eef2-d8fe-401f-9d65-49960d5ab280">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596346">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596346">
									<gml:posList srsDimension="3">359041.749480 5726421.048568 32.097000 359038.955356 5726420.333130 33.049642 359038.837195 5726420.302875 33.050237 359038.837195 5726420.302875 33.090000 359041.749480 5726421.048568 33.090000 359041.749480 5726421.048568 32.097000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_702_735619_125396">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeod190f92c-2368-42fc-ae71-e4d366be41ca">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596347">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596347">
									<gml:posList srsDimension="3">359041.159000 5726423.334000 32.097000 359038.955356 5726420.333130 33.049642 359041.749480 5726421.048568 32.097000 359041.655000 5726421.414000 32.097000 359041.159000 5726423.334000 32.097000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_484_302811_140088">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo70e9d00d-7546-4ef2-90c6-61340bd722e8">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596348">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596348">
									<gml:posList srsDimension="3">359039.867000 5726416.281000 33.090000 359035.678000 5726422.002000 32.097000 359037.924000 5726413.143000 32.097000 359039.867000 5726416.281000 33.090000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1787_109703_92173">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo24d50e59-b6ab-4730-a49c-ac205dfff54a">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596349">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596349">
									<gml:posList srsDimension="3">359035.678000 5726422.002000 32.097000 359039.867000 5726416.281000 33.090000 359038.862000 5726420.206000 33.090000 359035.678000 5726422.002000 32.097000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:address>
		<core:Address>
			<core:xalAddress>
				<xAL:AddressDetails>
					<xAL:Country>
						<xAL:CountryName>
							Germany
						</xAL:CountryName>
						<xAL:Locality  Type="Town">
							<xAL:LocalityName>
								Dorsten
							</xAL:LocalityName>
							<xAL:Thoroughfare>
								<xAL:ThoroughfareNumber>
									12
								</xAL:ThoroughfareNumber>
								<xAL:ThoroughfareName>
									Gartenstra�e
								</xAL:ThoroughfareName>
							</xAL:Thoroughfare>
							<xAL:PostalCode>
								<xAL:PostalCodeNumber>
									46284
								</xAL:PostalCodeNumber>
							</xAL:PostalCode>
						</xAL:Locality>
					</xAL:Country>
				</xAL:AddressDetails>
			</core:xalAddress>
			<core:multiPoint>
				<gml:MultiPoint>
					<gml:pointMember>
						<gml:Point>
							<gml:pos>359039.563 5726418.25 0.0</gml:pos>
						</gml:Point>
					</gml:pointMember>
				</gml:MultiPoint>
			</core:multiPoint>
		</core:Address>
	</bldg:address>
</bldg:Building>
</core:cityObjectMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596333</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596332</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596334</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596345</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596347</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596349</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596348</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596339</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<core:cityObjectMember>
<bldg:Building gml:id="DENW52AL000hnBCf">
	<core:creationDate>2018-02-02</core:creationDate>
	<core:externalReference>
		<core:informationSystem>http://repository.gdi-de.org/schemas/adv/citygml/fdv/art.htm#_9100</core:informationSystem>
		<core:externalObject>
			<core:name>DENW52AL000hnBCf</core:name>
		</core:externalObject>
	</core:externalReference>
	<gen:stringAttribute name="DatenquelleDachhoehe">
		<gen:value>1000</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="DatenquelleLage">
		<gen:value>1000</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="Gemeindeschluessel">
		<gen:value>05562012</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="DatenquelleBodenhoehe">
		<gen:value>1100</gen:value>
	</gen:stringAttribute>
	<bldg:measuredHeight uom="urn:adv:uom:m">12.214</bldg:measuredHeight>
	<bldg:lod2Solid>
		<gml:Solid gml:id="IDGeoc1ea4d15-717d-43a0-85af-38a3a95d69b5">
			<gml:exterior>
				<gml:CompositeSurface>
					<gml:surfaceMember xlink:href="#PolyIDGeo870394"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo870391"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo870397"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo870398"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo870392"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo870396"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo870390"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo870393"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo870395"/>
				</gml:CompositeSurface>
			</gml:exterior>
		</gml:Solid>
	</bldg:lod2Solid>
	<bldg:lod2TerrainIntersection>
		<gml:MultiCurve>
			<gml:curveMember>
				<gml:LineString>
					<gml:pos>359060.233 5726389.495 28.402</gml:pos>
					<gml:pos>359060.285 5726389.285 28.407</gml:pos>
					<gml:pos>359060.357 5726389.0 28.41</gml:pos>
					<gml:pos>359060.485 5726388.485 28.41</gml:pos>
					<gml:pos>359060.607 5726388.0 28.416</gml:pos>
					<gml:pos>359060.685 5726387.685 28.42</gml:pos>
					<gml:pos>359060.857 5726387.0 28.42</gml:pos>
					<gml:pos>359060.885 5726386.885 28.422</gml:pos>
					<gml:pos>359061.0 5726386.427 28.426</gml:pos>
					<gml:pos>359061.085 5726386.085 28.428</gml:pos>
					<gml:pos>359061.107 5726386.0 28.432</gml:pos>
					<gml:pos>359061.285 5726385.285 28.443</gml:pos>
					<gml:pos>359061.357 5726385.0 28.44</gml:pos>
					<gml:pos>359061.485 5726384.485 28.44</gml:pos>
					<gml:pos>359061.607 5726384.0 28.44</gml:pos>
					<gml:pos>359061.685 5726383.685 28.434</gml:pos>
					<gml:pos>359061.857 5726383.0 28.454</gml:pos>
					<gml:pos>359061.885 5726382.885 28.458</gml:pos>
					<gml:pos>359062.0 5726382.427 28.454</gml:pos>
					<gml:pos>359062.085 5726382.085 28.453</gml:pos>
					<gml:pos>359062.107 5726382.0 28.451</gml:pos>
					<gml:pos>359062.285 5726381.285 28.417</gml:pos>
					<gml:pos>359062.304 5726381.211 28.41</gml:pos>
				</gml:LineString>
			</gml:curveMember>
			<gml:curveMember>
				<gml:LineString>
					<gml:pos>359050.758 5726378.339 28.481</gml:pos>
					<gml:pos>359051.0 5726378.399 28.476</gml:pos>
					<gml:pos>359051.531 5726378.531 28.476</gml:pos>
					<gml:pos>359052.0 5726378.648 28.479</gml:pos>
					<gml:pos>359052.862 5726378.862 28.443</gml:pos>
					<gml:pos>359053.0 5726378.897 28.441</gml:pos>
					<gml:pos>359053.415 5726379.0 28.444</gml:pos>
					<gml:pos>359054.0 5726379.145 28.45</gml:pos>
					<gml:pos>359054.194 5726379.194 28.446</gml:pos>
					<gml:pos>359055.0 5726379.394 28.43</gml:pos>
					<gml:pos>359055.525 5726379.525 28.435</gml:pos>
					<gml:pos>359056.0 5726379.643 28.433</gml:pos>
					<gml:pos>359056.856 5726379.856 28.317</gml:pos>
					<gml:pos>359057.0 5726379.892 28.302</gml:pos>
					<gml:pos>359057.436 5726380.0 28.357</gml:pos>
					<gml:pos>359058.0 5726380.14 28.431</gml:pos>
					<gml:pos>359058.187 5726380.187 28.432</gml:pos>
					<gml:pos>359059.0 5726380.389 28.416</gml:pos>
					<gml:pos>359059.518 5726380.518 28.4</gml:pos>
					<gml:pos>359060.0 5726380.638 28.4</gml:pos>
					<gml:pos>359060.849 5726380.849 28.392</gml:pos>
					<gml:pos>359061.0 5726380.887 28.389</gml:pos>
					<gml:pos>359061.456 5726381.0 28.395</gml:pos>
					<gml:pos>359062.0 5726381.135 28.407</gml:pos>
					<gml:pos>359062.18 5726381.18 28.411</gml:pos>
					<gml:pos>359062.304 5726381.211 28.41</gml:pos>
				</gml:LineString>
			</gml:curveMember>
			<gml:curveMember>
				<gml:LineString>
					<gml:pos>359048.702 5726386.633 28.574</gml:pos>
					<gml:pos>359048.859 5726386.0 28.58</gml:pos>
					<gml:pos>359048.887 5726385.887 28.581</gml:pos>
					<gml:pos>359049.0 5726385.431 28.586</gml:pos>
					<gml:pos>359049.086 5726385.086 28.587</gml:pos>
					<gml:pos>359049.107 5726385.0 28.589</gml:pos>
					<gml:pos>359049.284 5726384.284 28.594</gml:pos>
					<gml:pos>359049.355 5726384.0 28.589</gml:pos>
					<gml:pos>359049.483 5726383.483 28.575</gml:pos>
					<gml:pos>359049.603 5726383.0 28.586</gml:pos>
					<gml:pos>359049.682 5726382.682 28.593</gml:pos>
					<gml:pos>359049.85 5726382.0 28.583</gml:pos>
					<gml:pos>359049.88 5726381.88 28.58</gml:pos>
					<gml:pos>359050.0 5726381.397 28.586</gml:pos>
					<gml:pos>359050.079 5726381.079 28.589</gml:pos>
					<gml:pos>359050.098 5726381.0 28.588</gml:pos>
					<gml:pos>359050.277 5726380.277 28.584</gml:pos>
					<gml:pos>359050.346 5726380.0 28.576</gml:pos>
					<gml:pos>359050.476 5726379.476 28.56</gml:pos>
					<gml:pos>359050.594 5726379.0 28.528</gml:pos>
					<gml:pos>359050.675 5726378.675 28.497</gml:pos>
					<gml:pos>359050.758 5726378.339 28.481</gml:pos>
				</gml:LineString>
			</gml:curveMember>
			<gml:curveMember>
				<gml:LineString>
					<gml:pos>359048.702 5726386.633 28.574</gml:pos>
					<gml:pos>359049.0 5726386.707 28.573</gml:pos>
					<gml:pos>359049.94 5726386.94 28.552</gml:pos>
					<gml:pos>359050.0 5726386.955 28.55</gml:pos>
					<gml:pos>359050.181 5726387.0 28.546</gml:pos>
					<gml:pos>359051.0 5726387.203 28.528</gml:pos>
					<gml:pos>359051.271 5726387.271 28.514</gml:pos>
					<gml:pos>359052.0 5726387.452 28.492</gml:pos>
					<gml:pos>359052.601 5726387.601 28.474</gml:pos>
					<gml:pos>359053.0 5726387.7 28.465</gml:pos>
					<gml:pos>359053.931 5726387.931 28.444</gml:pos>
					<gml:pos>359054.0 5726387.948 28.443</gml:pos>
					<gml:pos>359054.21 5726388.0 28.438</gml:pos>
					<gml:pos>359055.0 5726388.196 28.426</gml:pos>
					<gml:pos>359055.261 5726388.261 28.425</gml:pos>
					<gml:pos>359056.0 5726388.444 28.421</gml:pos>
					<gml:pos>359056.591 5726388.591 28.43</gml:pos>
					<gml:pos>359057.0 5726388.693 28.418</gml:pos>
					<gml:pos>359057.921 5726388.921 28.39</gml:pos>
					<gml:pos>359058.0 5726388.941 28.39</gml:pos>
					<gml:pos>359058.239 5726389.0 28.392</gml:pos>
					<gml:pos>359059.0 5726389.189 28.396</gml:pos>
					<gml:pos>359059.251 5726389.251 28.397</gml:pos>
					<gml:pos>359060.0 5726389.437 28.401</gml:pos>
					<gml:pos>359060.233 5726389.495 28.402</gml:pos>
				</gml:LineString>
			</gml:curveMember>
		</gml:MultiCurve>
	</bldg:lod2TerrainIntersection>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_dd48d632-60ca-45cd-bfe7-a5f91d00b677">
			<core:creationDate>2018-02-02</core:creationDate>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeod5fa94ac-2e99-4d56-8bdd-9a1825a6e870">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo870390">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID870390">
									<gml:posList srsDimension="3">359060.233000 5726389.495000 34.578000 359062.304000 5726381.211000 34.578000 359062.304000 5726381.211000 28.302000 359060.233000 5726389.495000 28.302000 359060.233000 5726389.495000 34.578000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_33aed640-c52e-4ca6-8d76-ee69363a4bd8">
			<core:creationDate>2018-02-02</core:creationDate>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo6ae7ab54-6a76-4578-963a-09c100b7ccab">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo870391">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID870391">
									<gml:posList srsDimension="3">359062.304000 5726381.211000 34.578000 359050.758000 5726378.339000 34.578000 359050.758000 5726378.339000 28.302000 359062.304000 5726381.211000 28.302000 359062.304000 5726381.211000 34.578000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_400cc057-527c-4831-bf81-44f0c4fa7ec1">
			<core:creationDate>2018-02-02</core:creationDate>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoa06464f2-90a9-42bf-8819-b86870da7a0d">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo870392">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID870392">
									<gml:posList srsDimension="3">359050.758000 5726378.339000 34.578000 359048.702000 5726386.633000 34.578000 359048.702000 5726386.633000 28.302000 359050.758000 5726378.339000 28.302000 359050.758000 5726378.339000 34.578000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_3f880271-1db8-441c-a8b8-7c203ad7d745">
			<core:creationDate>2018-02-02</core:creationDate>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeodb767d69-e0a9-4c7d-877d-7133c851d266">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo870393">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID870393">
									<gml:posList srsDimension="3">359048.702000 5726386.633000 34.578000 359060.233000 5726389.495000 34.578000 359060.233000 5726389.495000 28.302000 359048.702000 5726386.633000 28.302000 359048.702000 5726386.633000 34.578000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_283bfc7d-7f0f-4580-9acf-b1aee02e8e28">
			<core:creationDate>2018-02-02</core:creationDate>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeod213a9f6-cdbc-4172-8de2-cf40566e35a9">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo870394">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID870394">
									<gml:posList srsDimension="3">359050.758000 5726378.339000 34.578000 359062.304000 5726381.211000 34.578000 359056.531000 5726379.775000 40.517000 359050.758000 5726378.339000 34.578000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_0954c957-1a07-4327-8a05-4bade9562e29">
			<core:creationDate>2018-02-02</core:creationDate>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeode5f4bc7-088b-4b65-8e21-c855f3f4835d">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo870395">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID870395">
									<gml:posList srsDimension="3">359060.233000 5726389.495000 34.578000 359048.702000 5726386.633000 34.578000 359054.468000 5726388.064000 40.517000 359060.233000 5726389.495000 34.578000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_6b4b71e8-1fde-428d-9f0e-ebdbfbff91f5">
			<core:creationDate>2018-02-02</core:creationDate>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeod65d0f36-7c19-42cd-a783-23b751e55da7">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo870396">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID870396">
									<gml:posList srsDimension="3">359062.304000 5726381.211000 34.578000 359060.233000 5726389.495000 34.578000 359054.468000 5726388.064000 40.517000 359056.531000 5726379.775000 40.517000 359062.304000 5726381.211000 34.578000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_3dc6f9f1-5998-4b03-8a17-9448a86cf296">
			<core:creationDate>2018-02-02</core:creationDate>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeocd958e08-e1f3-4756-ae99-9bcbeb6fa003">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo870397">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID870397">
									<gml:posList srsDimension="3">359048.702000 5726386.633000 34.578000 359050.758000 5726378.339000 34.578000 359056.531000 5726379.775000 40.517000 359054.468000 5726388.064000 40.517000 359048.702000 5726386.633000 34.578000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:GroundSurface gml:id="UUID_4effa616-65a3-4cdd-b60b-db4b2bda5db3">
			<core:creationDate>2018-02-02</core:creationDate>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeode5a65a6-4d9a-401d-b106-03fea6faa4c9">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo870398">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID870398">
									<gml:posList srsDimension="3">359060.233000 5726389.495000 28.302000 359062.304000 5726381.211000 28.302000 359050.758000 5726378.339000 28.302000 359048.702000 5726386.633000 28.302000 359060.233000 5726389.495000 28.302000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:GroundSurface>
	</bldg:boundedBy>
	<bldg:address>
		<core:Address>
			<core:xalAddress>
				<xAL:AddressDetails>
					<xAL:Country>
						<xAL:CountryName>
							Germany
						</xAL:CountryName>
						<xAL:Locality  Type="Town">
							<xAL:LocalityName>
								Dorsten
							</xAL:LocalityName>
							<xAL:Thoroughfare  Type="Street">
								<xAL:ThoroughfareNumber>
									18
								</xAL:ThoroughfareNumber>
								<xAL:ThoroughfareName>
									Gartenstraße
								</xAL:ThoroughfareName>
							</xAL:Thoroughfare>
						</xAL:Locality>
					</xAL:Country>
				</xAL:AddressDetails>
			</core:xalAddress>
		</core:Address>
	</bldg:address>
</bldg:Building>
</core:cityObjectMember>
<core:cityObjectMember>
<bldg:Building gml:id="DENW_6b83a8d9-7cba-4e34-9757-c8cee8baeb17">
	<core:creationDate>2018-09-19</core:creationDate>
	<core:externalReference>
		<core:informationSystem>http://www.adv-online.de/fdv/art.htm#_9100</core:informationSystem>
		<core:externalObject>
			<core:name>DENW52AL000hnBCp</core:name>
		</core:externalObject>
	</core:externalReference>
	<gen:stringAttribute name="DatenquelleDachhoehe">
		<gen:value>6000</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="DatenquelleLage">
		<gen:value>1000</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="DatenquelleBodenhoehe">
		<gen:value>1100</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="Gemeindeschluessel">
		<gen:value>05562012</gen:value>
	</gen:stringAttribute>
	<bldg:measuredHeight uom="urn:ogc:def:uom:UCUM::m">10.84</bldg:measuredHeight>
	<bldg:lod2Solid>
		<gml:Solid gml:id="IDGeo1521efb4-7882-4cfe-9e83-0af02079bc8d">
			<gml:exterior>
				<gml:CompositeSurface>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596299"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596300"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596301"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596302"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596303"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596304"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596305"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596306"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596307"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596308"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596309"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596310"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596311"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596312"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596313"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596314"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596315"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596316"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596317"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596318"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596319"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596320"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596321"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596322"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596323"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596324"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596325"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596326"/>
				</gml:CompositeSurface>
			</gml:exterior>
		</gml:Solid>
	</bldg:lod2Solid>
	<bldg:boundedBy>
		<bldg:GroundSurface>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo778322fc-b463-4c3e-8f25-d70629c45e6e">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596299">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596299">
									<gml:posList srsDimension="3">359054.767000 5726404.213000 28.000000 359046.631000 5726402.118000 28.000000 359044.878000 5726408.907000 28.000000 359046.162000 5726409.234000 28.000000 359045.418000 5726412.088000 28.000000 359054.077000 5726414.335000 28.000000 359056.074000 5726406.581000 28.000000 359054.275000 5726406.115000 28.000000 359054.767000 5726404.213000 28.000000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:GroundSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_104_757623_117608">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo13f746bd-4fe2-4974-a42b-f09f6e3d7041">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596300">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596300">
									<gml:posList srsDimension="3">359046.162000 5726409.234000 28.000000 359046.162000 5726409.234000 32.000000 359046.288128 5726408.750170 35.000000 359045.418000 5726412.088000 35.000000 359045.418000 5726412.088000 28.000000 359046.162000 5726409.234000 28.000000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_952_491067_429685">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo105457d1-3a8c-4ca9-bb3d-47c5c1d3bb4a">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596301">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596301">
									<gml:posList srsDimension="3">359054.767000 5726404.213000 28.000000 359054.275000 5726406.115000 28.000000 359054.275000 5726406.115000 32.000000 359054.767000 5726404.213000 32.000000 359054.767000 5726404.213000 28.000000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1358_304688_96263">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeob64d01e3-0e31-4030-9ff3-e08aaee10654">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596302">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596302">
									<gml:posList srsDimension="3">359054.282794 5726404.088318 35.000000 359047.929859 5726402.452453 35.000000 359047.445654 5726402.327772 32.000000 359046.631000 5726402.118000 32.000000 359046.631000 5726402.118000 28.000000 359054.767000 5726404.213000 28.000000 359054.767000 5726404.213000 32.000000 359054.282794 5726404.088318 35.000000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1907_117460_284234">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeof902626a-a517-4090-aebc-47c4ea2c679c">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596303">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596303">
									<gml:posList srsDimension="3">359054.275000 5726406.115000 28.000000 359056.074000 5726406.581000 28.000000 359056.074000 5726406.581000 32.000000 359054.275000 5726406.115000 32.000000 359054.275000 5726406.115000 28.000000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_162_313031_113680">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeod79d28d8-e765-43a8-b7d2-848bd42c9ff9">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596304">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596304">
									<gml:posList srsDimension="3">359054.329023 5726411.337372 35.000000 359054.941380 5726410.978765 32.000000 359054.816677 5726411.462964 35.000000 359054.329023 5726411.337372 35.000000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_885_463896_285149">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoeb29663a-7b25-4e74-a350-801526f7da1e">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596305">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596305">
									<gml:posList srsDimension="3">359055.462290 5726406.937096 35.000000 359056.074000 5726406.581000 32.000000 359054.941380 5726410.978765 32.000000 359054.329023 5726411.337372 35.000000 359055.462290 5726406.937096 35.000000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_103_623350_143722">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoec68dad1-6d1f-4f23-918c-1de02260d88a">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596306">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596306">
									<gml:posList srsDimension="3">359054.275000 5726406.115000 32.000000 359053.665584 5726406.474364 35.000000 359054.282794 5726404.088318 35.000000 359054.767000 5726404.213000 32.000000 359054.275000 5726406.115000 32.000000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1024_471722_389764">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeobee35574-caa6-4ae7-ab17-1fa10af403bb">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596307">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596307">
									<gml:posList srsDimension="3">359053.665584 5726406.474364 35.000000 359049.820872 5726408.785989 37.893508 359050.362966 5726406.698098 37.891718 359050.516588 5726406.106418 37.891210 359054.282794 5726404.088318 35.000000 359053.665584 5726406.474364 35.000000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_840_879338_279432">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo320dd5d9-8d96-4aeb-8ea7-8f2fcd42d420">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596308">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596308">
									<gml:posList srsDimension="3">359054.282794 5726404.088318 35.000000 359050.516588 5726406.106418 37.891210 359047.929859 5726402.452453 35.000000 359054.282794 5726404.088318 35.000000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_762_34735_330933">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoe2e00d25-900a-4af7-aabf-efbc54fb3bb2">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596309">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596309">
									<gml:posList srsDimension="3">359055.462290 5726406.937096 35.000000 359050.579347 5726409.978023 38.766951 359049.820872 5726408.785989 37.893508 359053.665584 5726406.474364 35.000000 359055.462290 5726406.937096 35.000000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1528_841221_188072">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo2be821ae-b28d-4e95-b2ec-d18e68745d48">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596310">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596310">
									<gml:posList srsDimension="3">359045.418000 5726412.088000 35.000000 359046.288128 5726408.750170 35.000000 359050.362966 5726406.698098 37.891718 359049.820872 5726408.785989 37.893508 359050.579347 5726409.978023 38.766951 359049.747500 5726413.211500 38.776000 359045.418000 5726412.088000 35.000000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1301_626088_344132">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo3f334991-de16-443f-baaf-2c3945fe1271">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596311">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596311">
									<gml:posList srsDimension="3">359045.004105 5726408.418620 35.000000 359045.754996 5726405.510577 37.889543 359050.362966 5726406.698098 37.891718 359046.288128 5726408.750170 35.000000 359045.004105 5726408.418620 35.000000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1204_726574_208835">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo6cb6adce-4cb0-4d49-90eb-dbf6dbf22be5">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596312">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596312">
									<gml:posList srsDimension="3">359054.329023 5726411.337372 35.000000 359050.579347 5726409.978023 38.766951 359055.462290 5726406.937096 35.000000 359054.329023 5726411.337372 35.000000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1760_720312_114485">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo6c389248-5fa5-4fb6-93e9-a123f751bf64">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596313">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596313">
									<gml:posList srsDimension="3">359055.462290 5726406.937096 35.000000 359054.275000 5726406.115000 32.000000 359056.074000 5726406.581000 32.000000 359055.462290 5726406.937096 35.000000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_853_687382_200294">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoae50ce68-2bb0-451b-9f42-3d432809b952">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596314">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596314">
									<gml:posList srsDimension="3">359054.275000 5726406.115000 32.000000 359055.462290 5726406.937096 35.000000 359053.665584 5726406.474364 35.000000 359054.275000 5726406.115000 32.000000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1842_683592_55905">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo5967e89a-e0a2-4457-b7f6-b3744fa501f5">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596315">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596315">
									<gml:posList srsDimension="3">359046.288128 5726408.750170 35.000000 359046.162000 5726409.234000 32.000000 359044.878000 5726408.907000 31.973602 359045.004105 5726408.418620 35.000000 359046.288128 5726408.750170 35.000000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_891_281569_363282">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo67aa67d6-a0b7-42f7-86da-7ef578a5a56b">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596316">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596316">
									<gml:posList srsDimension="3">359054.077000 5726414.335000 38.006789 359050.538920 5726413.416873 38.000000 359054.329023 5726411.337372 35.000000 359054.816677 5726411.462964 35.000000 359054.077000 5726414.335000 38.006789 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1638_755067_56517">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo7debf07e-c4a0-40c9-b2f9-3376d67b70cf">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596317">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596317">
									<gml:posList srsDimension="3">359046.162000 5726409.234000 32.000000 359046.162000 5726409.234000 28.000000 359044.878000 5726408.907000 28.000000 359044.878000 5726408.907000 31.973602 359046.162000 5726409.234000 32.000000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1771_130404_76101">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo1ac03866-4e3f-4e1f-a0dd-3d874ae550c6">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596318">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596318">
									<gml:posList srsDimension="3">359046.631000 5726402.118000 32.000000 359046.505951 5726402.602287 35.001028 359045.754996 5726405.510577 37.889543 359045.004105 5726408.418620 35.000000 359044.878000 5726408.907000 31.973602 359044.878000 5726408.907000 28.000000 359046.631000 5726402.118000 28.000000 359046.631000 5726402.118000 32.000000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_670_399341_383354">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo5616c1e3-f9c6-401b-a820-4e62d116f6e9">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596319">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596319">
									<gml:posList srsDimension="3">359054.329023 5726411.337372 35.000000 359050.538920 5726413.416873 38.000000 359049.747500 5726413.211500 38.776000 359050.579347 5726409.978023 38.766951 359054.329023 5726411.337372 35.000000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1476_78866_155855">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo9b926a02-1112-411b-b747-a5898af850cc">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596320">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596320">
									<gml:posList srsDimension="3">359046.505951 5726402.602287 35.001028 359047.803731 5726402.936286 35.000000 359045.754996 5726405.510577 37.889543 359046.505951 5726402.602287 35.001028 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1510_157962_370242">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoee5ad8bd-f461-4c1b-8f28-e3ab4c2a735f">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596321">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596321">
									<gml:posList srsDimension="3">359045.754996 5726405.510577 37.889543 359047.803731 5726402.936286 35.000000 359050.362966 5726406.698098 37.891718 359045.754996 5726405.510577 37.889543 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1396_489947_335218">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo77dbe9fd-cd0b-4e93-8965-bbc6b36ba303">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596322">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596322">
									<gml:posList srsDimension="3">359047.803731 5726402.936286 35.000000 359047.445654 5726402.327772 32.000000 359047.929859 5726402.452453 35.000000 359047.803731 5726402.936286 35.000000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1870_493936_398845">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeof77370b2-704f-43d6-ad73-06ca56ff09c4">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596323">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596323">
									<gml:posList srsDimension="3">359050.362966 5726406.698098 37.891718 359047.803731 5726402.936286 35.000000 359047.929859 5726402.452453 35.000000 359050.516588 5726406.106418 37.891210 359050.362966 5726406.698098 37.891718 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_42_169519_321262">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo90ed0042-0af5-441e-ac24-29d6744d8570">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596324">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596324">
									<gml:posList srsDimension="3">359047.445654 5726402.327772 32.000000 359047.803731 5726402.936286 35.000000 359046.505951 5726402.602287 35.001028 359046.631000 5726402.118000 32.000000 359047.445654 5726402.327772 32.000000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_428_407564_214408">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo64bb4e4f-32ce-412a-a4ee-49bb919ee472">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596325">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596325">
									<gml:posList srsDimension="3">359056.074000 5726406.581000 28.000000 359054.077000 5726414.335000 28.000000 359054.077000 5726414.335000 38.006789 359054.816677 5726411.462964 35.000000 359054.941380 5726410.978765 32.000000 359056.074000 5726406.581000 32.000000 359056.074000 5726406.581000 28.000000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_132_131496_72043">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeob175c7b1-98a9-4827-89ab-8ecadcca4fd2">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596326">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596326">
									<gml:posList srsDimension="3">359049.747500 5726413.211500 38.776000 359050.538920 5726413.416873 38.000000 359054.077000 5726414.335000 38.006789 359054.077000 5726414.335000 28.000000 359045.418000 5726412.088000 28.000000 359045.418000 5726412.088000 35.000000 359049.747500 5726413.211500 38.776000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
</bldg:Building>
</core:cityObjectMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596321</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596320</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596323</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596322</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Frontgiebel.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992596325">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992596325">0.0 0.0 1.0 0.0 1.0 0.9998 0.6296 0.6994 0.5672 0.3996 0.0 0.3996 0.0 0.0 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596324</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596326</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Seite_vorne_blank.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992596301">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992596301">0.0 0.0 1.0 0.0 1.0 1.0 0.0 1.0 0.0 0.0 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Seite_vorne_blank.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992596303">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992596303">0.0 0.0 1.0 0.0 1.0 1.0 0.0 1.0 0.0 0.0 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Gartenstr14Seite_Eingang.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992596302">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992596302">0.9405 0.9979 0.1596 0.9979 0.1001 0.5702 0.0 0.5702 0.0 0.0 1.0 0.0 1.0 0.5702 0.9405 0.9979 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596313</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596312</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596315</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596314</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Seite_vorne_blank.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992596317">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992596317">0.0 1.0 0.0 0.0 1.0 0.0 1.0 0.9934 0.0 1.0 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596316</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596319</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Gartengiebel.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992596318">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992596318">1.0 0.4037 0.9287 0.7066 0.5003 0.9981 0.0719 0.7065 0.0 0.401 0.0 0.0 1.0 0.0 1.0 0.4037 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596305</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596304</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596307</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596306</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596309</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596308</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596311</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596310</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<core:cityObjectMember>
<bldg:Building gml:id="DENW_6222307c-e41d-4920-bdd8-6104cf7dfd0e">
	<core:creationDate>2014-10-18</core:creationDate>
	<core:externalReference>
		<core:informationSystem>http://www.adv-online.de/fdv/art.htm#_9100</core:informationSystem>
		<core:externalObject>
			<core:name>DENW52AL000pbbsn</core:name>
		</core:externalObject>
	</core:externalReference>
	<gen:stringAttribute name="QualitaetDacherkennung">
		<gen:value>92.4590163934426</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="Dachneigung">
		<gen:value>90</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="Dachorientierung">
		<gen:value>0</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="Dachflaeche">
		<gen:value>26.517</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="GebaeudeHoehe">
		<gen:value>2.494</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="Firsthoehe">
		<gen:value>30.8000511299326</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="MittlereTraufHoehe">
		<gen:value>30.8</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="AbsoluteHoehe">
		<gen:value>28.306</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="DatenquelleDachhoehe">
		<gen:value>1000</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="DatenquelleLage">
		<gen:value>1000</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="DatenquelleBodenhoehe">
		<gen:value>1100</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="Gemeindeschluessel">
		<gen:value>05562012</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="GTA_Dachtyp">
		<gen:value>PolyFlatRoof</gen:value>
	</gen:stringAttribute>
	<bldg:measuredHeight uom="urn:ogc:def:uom:UCUM::m">2.494</bldg:measuredHeight>
	<bldg:lod2Solid>
		<gml:Solid gml:id="IDGeo6bb6dba1-6bb5-41e5-87bd-2f01cd4c0c10">
			<gml:exterior>
				<gml:CompositeSurface>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596403"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596404"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596405"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596406"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596407"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596408"/>
				</gml:CompositeSurface>
			</gml:exterior>
		</gml:Solid>
	</bldg:lod2Solid>
	<bldg:lod2TerrainIntersection>
		<gml:MultiCurve>
			<gml:curveMember>
				<gml:LineString>
					<gml:pos>359044.363 5726397.911 28.309</gml:pos>
					<gml:pos>359044.474 5726397.474 0.0</gml:pos>
					<gml:pos>359044.593 5726397.0 0.0</gml:pos>
					<gml:pos>359044.675 5726396.675 0.0</gml:pos>
					<gml:pos>359044.846 5726396.0 0.0</gml:pos>
					<gml:pos>359044.877 5726395.877 0.0</gml:pos>
					<gml:pos>359045.0 5726395.391 0.0</gml:pos>
					<gml:pos>359045.079 5726395.079 0.0</gml:pos>
					<gml:pos>359045.099 5726395.0 0.0</gml:pos>
					<gml:pos>359045.281 5726394.281 0.0</gml:pos>
					<gml:pos>359045.344 5726394.03 28.408</gml:pos>
				</gml:LineString>
			</gml:curveMember>
			<gml:curveMember>
				<gml:LineString>
					<gml:pos>359038.88 5726392.472 28.442</gml:pos>
					<gml:pos>359039.0 5726392.501 0.0</gml:pos>
					<gml:pos>359039.66 5726392.66 0.0</gml:pos>
					<gml:pos>359040.0 5726392.742 0.0</gml:pos>
					<gml:pos>359040.978 5726392.978 0.0</gml:pos>
					<gml:pos>359041.0 5726392.983 0.0</gml:pos>
					<gml:pos>359041.071 5726393.0 0.0</gml:pos>
					<gml:pos>359042.0 5726393.224 0.0</gml:pos>
					<gml:pos>359042.295 5726393.295 0.0</gml:pos>
					<gml:pos>359043.0 5726393.465 0.0</gml:pos>
					<gml:pos>359043.613 5726393.613 0.0</gml:pos>
					<gml:pos>359044.0 5726393.706 0.0</gml:pos>
					<gml:pos>359044.93 5726393.93 0.0</gml:pos>
					<gml:pos>359045.0 5726393.947 0.0</gml:pos>
					<gml:pos>359045.22 5726394.0 0.0</gml:pos>
					<gml:pos>359045.344 5726394.03 28.408</gml:pos>
				</gml:LineString>
			</gml:curveMember>
			<gml:curveMember>
				<gml:LineString>
					<gml:pos>359037.916 5726396.334 28.605</gml:pos>
					<gml:pos>359037.999 5726396.0 0.0</gml:pos>
					<gml:pos>359037.999 5726395.999 0.0</gml:pos>
					<gml:pos>359038.0 5726395.997 0.0</gml:pos>
					<gml:pos>359038.199 5726395.199 0.0</gml:pos>
					<gml:pos>359038.249 5726395.0 0.0</gml:pos>
					<gml:pos>359038.399 5726394.399 0.0</gml:pos>
					<gml:pos>359038.499 5726394.0 0.0</gml:pos>
					<gml:pos>359038.599 5726393.599 0.0</gml:pos>
					<gml:pos>359038.748 5726393.0 0.0</gml:pos>
					<gml:pos>359038.799 5726392.799 0.0</gml:pos>
					<gml:pos>359038.88 5726392.472 28.442</gml:pos>
				</gml:LineString>
			</gml:curveMember>
			<gml:curveMember>
				<gml:LineString>
					<gml:pos>359037.916 5726396.334 28.605</gml:pos>
					<gml:pos>359038.0 5726396.355 0.0</gml:pos>
					<gml:pos>359038.469 5726396.469 0.0</gml:pos>
					<gml:pos>359039.0 5726396.599 0.0</gml:pos>
					<gml:pos>359039.793 5726396.793 0.0</gml:pos>
					<gml:pos>359040.0 5726396.844 0.0</gml:pos>
					<gml:pos>359040.639 5726397.0 0.0</gml:pos>
					<gml:pos>359041.0 5726397.088 0.0</gml:pos>
					<gml:pos>359041.117 5726397.117 0.0</gml:pos>
					<gml:pos>359042.0 5726397.333 0.0</gml:pos>
					<gml:pos>359042.441 5726397.441 0.0</gml:pos>
					<gml:pos>359043.0 5726397.578 0.0</gml:pos>
					<gml:pos>359043.765 5726397.765 0.0</gml:pos>
					<gml:pos>359044.0 5726397.822 0.0</gml:pos>
					<gml:pos>359044.363 5726397.911 28.309</gml:pos>
				</gml:LineString>
			</gml:curveMember>
		</gml:MultiCurve>
	</bldg:lod2TerrainIntersection>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="IDGeoe5ec5a11-c182-4f53-973e-10d2dfc97124">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeocec6bd55-2c9a-41d0-a583-7c9771a6d59d">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596403">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596403">
									<gml:posList srsDimension="3">359044.363000 5726397.911000 30.800000 359045.344000 5726394.030000 30.800000 359045.344000 5726394.030000 28.306000 359044.363000 5726397.911000 28.306000 359044.363000 5726397.911000 30.800000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="IDGeo071b9ee9-78d4-402e-8929-8da3abf959f3">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo29f25d17-e8a5-495e-8966-d9b63975888e">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596404">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596404">
									<gml:posList srsDimension="3">359045.344000 5726394.030000 30.800000 359038.880000 5726392.472000 30.800000 359038.880000 5726392.472000 28.306000 359045.344000 5726394.030000 28.306000 359045.344000 5726394.030000 30.800000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="IDGeocdf45aec-fba3-4f0c-a832-c4b9b7fc525c">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeob83848b1-efd8-4406-8683-dc08454bcbe7">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596405">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596405">
									<gml:posList srsDimension="3">359038.880000 5726392.472000 30.800000 359037.916000 5726396.334000 30.800000 359037.916000 5726396.334000 28.306000 359038.880000 5726392.472000 28.306000 359038.880000 5726392.472000 30.800000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="IDGeo5fefb3d6-2fa8-4cb0-be1e-6530c9c2e132">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoa8c7361c-e8dd-499e-b744-5b69b17c9344">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596406">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596406">
									<gml:posList srsDimension="3">359037.916000 5726396.334000 30.800000 359044.363000 5726397.911000 30.800000 359044.363000 5726397.911000 28.306000 359037.916000 5726396.334000 28.306000 359037.916000 5726396.334000 30.800000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="IDGeo819f72a9-3ae2-4c7a-9bf0-402143c00a7f">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeob0f33961-8495-4ce6-953a-4237f556b4b4">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596407">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596407">
									<gml:posList srsDimension="3">359037.916000 5726396.334000 30.800000 359038.880000 5726392.472000 30.800000 359045.344000 5726394.030000 30.800000 359044.363000 5726397.911000 30.800000 359037.916000 5726396.334000 30.800000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:GroundSurface gml:id="IDGeoa6dd7ea3-7c8c-4ab8-af2c-1bc154852fc9">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeofa7169d5-4809-4308-996f-7dbf5766746a">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596408">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596408">
									<gml:posList srsDimension="3">359044.363000 5726397.911000 28.306000 359045.344000 5726394.030000 28.306000 359038.880000 5726392.472000 28.306000 359037.916000 5726396.334000 28.306000 359044.363000 5726397.911000 28.306000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:GroundSurface>
	</bldg:boundedBy>
	<bldg:address>
		<core:Address>
			<core:xalAddress>
				<xAL:AddressDetails>
					<xAL:Country>
						<xAL:CountryName>
							Germany
						</xAL:CountryName>
						<xAL:Locality  Type="Town">
							<xAL:LocalityName>
								Dorsten
							</xAL:LocalityName>
							<xAL:Thoroughfare>
								<xAL:ThoroughfareNumber>
									16
								</xAL:ThoroughfareNumber>
								<xAL:ThoroughfareName>
									Gartenstraße
								</xAL:ThoroughfareName>
							</xAL:Thoroughfare>
							<xAL:PostalCode>
								<xAL:PostalCodeNumber>
									46284
								</xAL:PostalCodeNumber>
							</xAL:PostalCode>
						</xAL:Locality>
					</xAL:Country>
				</xAL:AddressDetails>
			</core:xalAddress>
			<core:multiPoint>
				<gml:MultiPoint>
					<gml:pointMember>
						<gml:Point>
							<gml:pos>359041.625 5726395.0 0.0</gml:pos>
						</gml:Point>
					</gml:pointMember>
				</gml:MultiPoint>
			</core:multiPoint>
		</core:Address>
	</bldg:address>
</bldg:Building>
</core:cityObjectMember>
<core:cityObjectMember>
<bldg:Building gml:id="DENW_47f1adda-7c3f-4198-a31c-4b7ee49234be">
	<core:creationDate>2018-09-19</core:creationDate>
	<core:externalReference>
		<core:informationSystem>http://www.adv-online.de/fdv/art.htm#_9100</core:informationSystem>
		<core:externalObject>
			<core:name>DENW52AL000hnChE</core:name>
		</core:externalObject>
	</core:externalReference>
	<gen:stringAttribute name="DatenquelleDachhoehe">
		<gen:value>6000</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="DatenquelleLage">
		<gen:value>1000</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="DatenquelleBodenhoehe">
		<gen:value>1100</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="Gemeindeschluessel">
		<gen:value>05562012</gen:value>
	</gen:stringAttribute>
	<bldg:measuredHeight uom="urn:ogc:def:uom:UCUM::m">4.3</bldg:measuredHeight>
	<bldg:lod2Solid>
		<gml:Solid gml:id="IDGeoec29c54e-62a7-41fc-882b-1fa201f11576">
			<gml:exterior>
				<gml:CompositeSurface>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596283"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596284"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596285"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596286"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596287"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596288"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596289"/>
				</gml:CompositeSurface>
			</gml:exterior>
		</gml:Solid>
	</bldg:lod2Solid>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1907_661497_97415">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo52810b2c-2c89-438e-b7f6-808eece51e9b">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596283">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596283">
									<gml:posList srsDimension="3">359021.675000 5726403.018000 32.531000 359021.013000 5726405.463000 32.531000 359021.013000 5726405.463000 28.184000 359021.675000 5726403.018000 28.184000 359021.675000 5726403.018000 32.531000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:GroundSurface>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeobbf6fc03-0856-4b94-a9ba-086a07161166">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596284">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596284">
									<gml:posList srsDimension="3">359021.013000 5726405.463000 28.184000 359023.443000 5726406.124000 28.184000 359021.675000 5726403.018000 28.184000 359021.013000 5726405.463000 28.184000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596285">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596285">
									<gml:posList srsDimension="3">359024.092000 5726403.658000 28.193891 359021.675000 5726403.018000 28.184000 359023.443000 5726406.124000 28.184000 359024.092000 5726403.658000 28.193891 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:GroundSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo70825630-e8e4-46d4-9f30-5295c151071e">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596286">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596286">
									<gml:posList srsDimension="3">359024.092000 5726403.658000 28.193891 359024.092000 5726403.658000 31.159891 359021.675000 5726403.018000 32.531000 359021.675000 5726403.018000 28.184000 359024.092000 5726403.658000 28.193891 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo49b00473-fdbc-4237-a9b8-fd85b8d788b9">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596287">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596287">
									<gml:posList srsDimension="3">359021.013000 5726405.463000 28.184000 359021.013000 5726405.463000 32.531000 359023.443000 5726406.124000 31.150000 359023.443000 5726406.124000 28.184000 359021.013000 5726405.463000 28.184000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_738_95978_406493">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo4ad52aaa-c6ec-4a11-931a-c7f1d7bbcb2f">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596288">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596288">
									<gml:posList srsDimension="3">359023.443000 5726406.124000 31.150000 359024.092000 5726403.658000 31.159891 359024.092000 5726403.658000 28.193891 359023.443000 5726406.124000 28.184000 359023.443000 5726406.124000 31.150000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_647_537926_201139">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeod35c2e19-d423-41a3-a42a-11ad03668fb5">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596289">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596289">
									<gml:posList srsDimension="3">359024.092000 5726403.658000 31.159891 359023.443000 5726406.124000 31.150000 359021.013000 5726405.463000 32.531000 359021.675000 5726403.018000 32.531000 359024.092000 5726403.658000 31.159891 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:address>
		<core:Address>
			<core:xalAddress>
				<xAL:AddressDetails>
					<xAL:Country>
						<xAL:CountryName>
							Germany
						</xAL:CountryName>
						<xAL:Locality  Type="Town">
							<xAL:LocalityName>
								Dorsten
							</xAL:LocalityName>
							<xAL:Thoroughfare>
								<xAL:ThoroughfareNumber>
									14
								</xAL:ThoroughfareNumber>
								<xAL:ThoroughfareName>
									Gartenstra�e
								</xAL:ThoroughfareName>
							</xAL:Thoroughfare>
							<xAL:PostalCode>
								<xAL:PostalCodeNumber>
									46284
								</xAL:PostalCodeNumber>
							</xAL:PostalCode>
						</xAL:Locality>
					</xAL:Country>
				</xAL:AddressDetails>
			</core:xalAddress>
			<core:multiPoint>
				<gml:MultiPoint>
					<gml:pointMember>
						<gml:Point>
							<gml:pos>359022.547 5726404.5 0.0</gml:pos>
						</gml:Point>
					</gml:pointMember>
				</gml:MultiPoint>
			</core:multiPoint>
		</core:Address>
	</bldg:address>
</bldg:Building>
</core:cityObjectMember>
<core:cityObjectMember>
<bldg:Building gml:id="DENW_ac3ca340-8b3a-4c92-b0a1-33c539b2b38d">
	<core:creationDate>2018-09-19</core:creationDate>
	<core:externalReference>
		<core:informationSystem>http://www.adv-online.de/fdv/art.htm#_9100</core:informationSystem>
		<core:externalObject>
			<core:name>DENW52AL000hnChx</core:name>
		</core:externalObject>
	</core:externalReference>
	<gen:stringAttribute name="DatenquelleDachhoehe">
		<gen:value>6000</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="DatenquelleLage">
		<gen:value>1000</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="DatenquelleBodenhoehe">
		<gen:value>1100</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="Gemeindeschluessel">
		<gen:value>05562012</gen:value>
	</gen:stringAttribute>
	<bldg:measuredHeight uom="urn:ogc:def:uom:UCUM::m">4.37</bldg:measuredHeight>
	<bldg:lod2Solid>
		<gml:Solid gml:id="IDGeo6bf66f7e-ede9-47cc-9b89-d089d2f1e363">
			<gml:exterior>
				<gml:CompositeSurface>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596260"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596261"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596262"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596263"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596264"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596265"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596266"/>
				</gml:CompositeSurface>
			</gml:exterior>
		</gml:Solid>
	</bldg:lod2Solid>
	<bldg:boundedBy>
		<bldg:WallSurface>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeod58e86ff-4965-4866-a250-661d6eb98bd6">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596260">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596260">
									<gml:posList srsDimension="3">359020.301000 5726408.038000 28.171000 359020.301000 5726408.038000 32.543072 359021.013000 5726405.463000 32.531000 359021.013000 5726405.463000 28.171000 359020.301000 5726408.038000 28.171000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo36855e3f-71e0-4cfb-b6a8-adfef679d94e">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596261">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596261">
									<gml:posList srsDimension="3">359021.013000 5726405.463000 28.171000 359021.013000 5726405.463000 32.531000 359017.801000 5726404.588000 31.157113 359017.801000 5726404.588000 28.171000 359021.013000 5726405.463000 28.171000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_536_599152_206871">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo4c7a7a8b-2f0e-4c1d-b8a1-3fb25f1a3797">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596262">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596262">
									<gml:posList srsDimension="3">359021.013000 5726405.463000 32.531000 359020.301000 5726408.038000 32.543072 359017.106000 5726407.190000 31.179014 359017.801000 5726404.588000 31.157113 359021.013000 5726405.463000 32.531000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:GroundSurface>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo9b3f26e4-d963-46cc-abe1-15135aa0a08d">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596263">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596263">
									<gml:posList srsDimension="3">359017.106000 5726407.190000 28.171176 359020.301000 5726408.038000 28.171000 359017.801000 5726404.588000 28.171000 359017.106000 5726407.190000 28.171176 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596264">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596264">
									<gml:posList srsDimension="3">359021.013000 5726405.463000 28.171000 359017.801000 5726404.588000 28.171000 359020.301000 5726408.038000 28.171000 359021.013000 5726405.463000 28.171000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:GroundSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoa29add8a-100d-455f-bb1c-dbc7d50edc69">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596265">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596265">
									<gml:posList srsDimension="3">359017.106000 5726407.190000 28.171176 359017.106000 5726407.190000 31.179014 359020.301000 5726408.038000 32.543072 359020.301000 5726408.038000 28.171000 359017.106000 5726407.190000 28.171176 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1858_599196_297279">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo07a51949-6df2-41e0-97bd-739360a41357">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596266">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596266">
									<gml:posList srsDimension="3">359017.801000 5726404.588000 31.157113 359017.106000 5726407.190000 31.179014 359017.106000 5726407.190000 28.171176 359017.801000 5726404.588000 28.171000 359017.801000 5726404.588000 31.157113 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:address>
		<core:Address>
			<core:xalAddress>
				<xAL:AddressDetails>
					<xAL:Country>
						<xAL:CountryName>
							Germany
						</xAL:CountryName>
						<xAL:Locality  Type="Town">
							<xAL:LocalityName>
								Dorsten
							</xAL:LocalityName>
							<xAL:Thoroughfare>
								<xAL:ThoroughfareNumber>
									12
								</xAL:ThoroughfareNumber>
								<xAL:ThoroughfareName>
									Gartenstra�e
								</xAL:ThoroughfareName>
							</xAL:Thoroughfare>
							<xAL:PostalCode>
								<xAL:PostalCodeNumber>
									46284
								</xAL:PostalCodeNumber>
							</xAL:PostalCode>
						</xAL:Locality>
					</xAL:Country>
				</xAL:AddressDetails>
			</core:xalAddress>
			<core:multiPoint>
				<gml:MultiPoint>
					<gml:pointMember>
						<gml:Point>
							<gml:pos>359019.063 5726406.5 0.0</gml:pos>
						</gml:Point>
					</gml:pointMember>
				</gml:MultiPoint>
			</core:multiPoint>
		</core:Address>
	</bldg:address>
</bldg:Building>
</core:cityObjectMember>
<core:cityObjectMember>
<bldg:Building gml:id="DENW52AL000hnBBF">
	<core:creationDate>2018-02-02</core:creationDate>
	<core:externalReference>
		<core:informationSystem>http://repository.gdi-de.org/schemas/adv/citygml/fdv/art.htm#_9100</core:informationSystem>
		<core:externalObject>
			<core:name>DENW52AL000hnBBF</core:name>
		</core:externalObject>
	</core:externalReference>
	<gen:stringAttribute name="DatenquelleDachhoehe">
		<gen:value>1000</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="DatenquelleLage">
		<gen:value>1000</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="Gemeindeschluessel">
		<gen:value>05562012</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="DatenquelleBodenhoehe">
		<gen:value>1100</gen:value>
	</gen:stringAttribute>
	<bldg:measuredHeight uom="urn:adv:uom:m">12.314</bldg:measuredHeight>
	<bldg:lod2Solid>
		<gml:Solid gml:id="IDGeo324acc0a-279b-4700-801d-ec588e518fa1">
			<gml:exterior>
				<gml:CompositeSurface>
					<gml:surfaceMember xlink:href="#PolyIDGeo858637"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo858636"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo858641"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo858639"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo858635"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo858638"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo858640"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo858642"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo858643"/>
				</gml:CompositeSurface>
			</gml:exterior>
		</gml:Solid>
	</bldg:lod2Solid>
	<bldg:lod2TerrainIntersection>
		<gml:MultiCurve>
			<gml:curveMember>
				<gml:LineString>
					<gml:pos>359058.143 5726397.853 28.172</gml:pos>
					<gml:pos>359058.285 5726397.285 28.227</gml:pos>
					<gml:pos>359058.356 5726397.0 28.234</gml:pos>
					<gml:pos>359058.485 5726396.485 28.235</gml:pos>
					<gml:pos>359058.606 5726396.0 28.254</gml:pos>
					<gml:pos>359058.685 5726395.685 28.276</gml:pos>
					<gml:pos>359058.857 5726395.0 28.299</gml:pos>
					<gml:pos>359058.885 5726394.885 28.298</gml:pos>
					<gml:pos>359059.0 5726394.426 28.294</gml:pos>
					<gml:pos>359059.085 5726394.085 28.293</gml:pos>
					<gml:pos>359059.107 5726394.0 28.287</gml:pos>
					<gml:pos>359059.285 5726393.285 28.303</gml:pos>
					<gml:pos>359059.357 5726393.0 28.331</gml:pos>
					<gml:pos>359059.485 5726392.485 28.371</gml:pos>
					<gml:pos>359059.607 5726392.0 28.414</gml:pos>
					<gml:pos>359059.685 5726391.685 28.424</gml:pos>
					<gml:pos>359059.857 5726391.0 28.376</gml:pos>
					<gml:pos>359059.886 5726390.886 28.371</gml:pos>
					<gml:pos>359060.0 5726390.428 28.381</gml:pos>
					<gml:pos>359060.086 5726390.086 28.388</gml:pos>
					<gml:pos>359060.107 5726390.0 28.391</gml:pos>
					<gml:pos>359060.226 5726389.524 28.402</gml:pos>
				</gml:LineString>
			</gml:curveMember>
			<gml:curveMember>
				<gml:LineString>
					<gml:pos>359048.697 5726386.654 28.573</gml:pos>
					<gml:pos>359049.0 5726386.729 28.573</gml:pos>
					<gml:pos>359049.971 5726386.971 28.551</gml:pos>
					<gml:pos>359050.0 5726386.978 28.55</gml:pos>
					<gml:pos>359050.087 5726387.0 28.548</gml:pos>
					<gml:pos>359051.0 5726387.227 28.528</gml:pos>
					<gml:pos>359051.303 5726387.303 28.512</gml:pos>
					<gml:pos>359052.0 5726387.476 28.491</gml:pos>
					<gml:pos>359052.634 5726387.634 28.472</gml:pos>
					<gml:pos>359053.0 5726387.725 28.464</gml:pos>
					<gml:pos>359053.966 5726387.966 28.442</gml:pos>
					<gml:pos>359054.0 5726387.974 28.442</gml:pos>
					<gml:pos>359054.104 5726388.0 28.439</gml:pos>
					<gml:pos>359055.0 5726388.223 28.426</gml:pos>
					<gml:pos>359055.297 5726388.297 28.424</gml:pos>
					<gml:pos>359056.0 5726388.472 28.421</gml:pos>
					<gml:pos>359056.628 5726388.628 28.43</gml:pos>
					<gml:pos>359057.0 5726388.721 28.419</gml:pos>
					<gml:pos>359057.96 5726388.96 28.39</gml:pos>
					<gml:pos>359058.0 5726388.97 28.39</gml:pos>
					<gml:pos>359058.121 5726389.0 28.391</gml:pos>
					<gml:pos>359059.0 5726389.219 28.396</gml:pos>
					<gml:pos>359059.291 5726389.291 28.397</gml:pos>
					<gml:pos>359060.0 5726389.468 28.401</gml:pos>
					<gml:pos>359060.226 5726389.524 28.402</gml:pos>
				</gml:LineString>
			</gml:curveMember>
			<gml:curveMember>
				<gml:LineString>
					<gml:pos>359046.636 5726394.967 28.346</gml:pos>
					<gml:pos>359046.702 5726394.702 28.367</gml:pos>
					<gml:pos>359046.876 5726394.0 28.404</gml:pos>
					<gml:pos>359046.9 5726393.9 28.406</gml:pos>
					<gml:pos>359047.0 5726393.499 28.41</gml:pos>
					<gml:pos>359047.099 5726393.099 28.413</gml:pos>
					<gml:pos>359047.124 5726393.0 28.418</gml:pos>
					<gml:pos>359047.298 5726392.298 28.435</gml:pos>
					<gml:pos>359047.372 5726392.0 28.443</gml:pos>
					<gml:pos>359047.496 5726391.496 28.465</gml:pos>
					<gml:pos>359047.62 5726391.0 28.512</gml:pos>
					<gml:pos>359047.695 5726390.695 28.532</gml:pos>
					<gml:pos>359047.867 5726390.0 28.534</gml:pos>
					<gml:pos>359047.894 5726389.894 28.533</gml:pos>
					<gml:pos>359048.0 5726389.465 28.541</gml:pos>
					<gml:pos>359048.092 5726389.092 28.546</gml:pos>
					<gml:pos>359048.115 5726389.0 28.549</gml:pos>
					<gml:pos>359048.291 5726388.291 28.583</gml:pos>
					<gml:pos>359048.363 5726388.0 28.596</gml:pos>
					<gml:pos>359048.49 5726387.49 28.585</gml:pos>
					<gml:pos>359048.611 5726387.0 28.574</gml:pos>
					<gml:pos>359048.688 5726386.688 28.573</gml:pos>
					<gml:pos>359048.697 5726386.654 28.573</gml:pos>
				</gml:LineString>
			</gml:curveMember>
			<gml:curveMember>
				<gml:LineString>
					<gml:pos>359046.636 5726394.967 28.346</gml:pos>
					<gml:pos>359046.768 5726395.0 28.342</gml:pos>
					<gml:pos>359047.0 5726395.058 28.338</gml:pos>
					<gml:pos>359047.078 5726395.078 28.338</gml:pos>
					<gml:pos>359048.0 5726395.309 28.331</gml:pos>
					<gml:pos>359048.413 5726395.413 28.323</gml:pos>
					<gml:pos>359049.0 5726395.56 28.318</gml:pos>
					<gml:pos>359049.747 5726395.747 28.325</gml:pos>
					<gml:pos>359050.0 5726395.811 28.32</gml:pos>
					<gml:pos>359050.755 5726396.0 28.275</gml:pos>
					<gml:pos>359051.0 5726396.062 28.258</gml:pos>
					<gml:pos>359051.082 5726396.082 28.257</gml:pos>
					<gml:pos>359052.0 5726396.312 28.241</gml:pos>
					<gml:pos>359052.417 5726396.417 28.237</gml:pos>
					<gml:pos>359053.0 5726396.563 28.224</gml:pos>
					<gml:pos>359053.752 5726396.752 28.215</gml:pos>
					<gml:pos>359054.0 5726396.814 28.21</gml:pos>
					<gml:pos>359054.742 5726397.0 28.188</gml:pos>
					<gml:pos>359055.0 5726397.065 28.177</gml:pos>
					<gml:pos>359055.086 5726397.086 28.175</gml:pos>
					<gml:pos>359056.0 5726397.316 28.154</gml:pos>
					<gml:pos>359056.421 5726397.421 28.153</gml:pos>
					<gml:pos>359057.0 5726397.566 28.156</gml:pos>
					<gml:pos>359057.756 5726397.756 28.16</gml:pos>
					<gml:pos>359058.0 5726397.817 28.165</gml:pos>
					<gml:pos>359058.143 5726397.853 28.172</gml:pos>
				</gml:LineString>
			</gml:curveMember>
		</gml:MultiCurve>
	</bldg:lod2TerrainIntersection>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_e7433fbd-6cb0-428b-9871-6f0728a58301">
			<core:creationDate>2018-02-02</core:creationDate>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo6837a83e-4697-4eb0-8b16-c3acf0043594">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo858635">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID858635">
									<gml:posList srsDimension="3">359058.143000 5726397.853000 34.542000 359060.226000 5726389.524000 34.542000 359060.226000 5726389.524000 28.153000 359058.143000 5726397.853000 28.153000 359058.143000 5726397.853000 34.542000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_7bf8d8c4-9ce4-4117-b2d8-d002385b9836">
			<core:creationDate>2018-02-02</core:creationDate>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo4ff40974-bec4-4fe0-a409-9f5ad326de82">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo858636">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID858636">
									<gml:posList srsDimension="3">359060.226000 5726389.524000 34.542000 359048.697000 5726386.654000 34.542000 359048.697000 5726386.654000 28.153000 359060.226000 5726389.524000 28.153000 359060.226000 5726389.524000 34.542000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_a83df9b1-233d-41cd-9519-c8426acfa5ea">
			<core:creationDate>2018-02-02</core:creationDate>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo7a131537-f1cd-400e-8a4d-ea19db5c9282">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo858637">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID858637">
									<gml:posList srsDimension="3">359048.697000 5726386.654000 34.542000 359046.636000 5726394.967000 34.542000 359046.636000 5726394.967000 28.153000 359048.697000 5726386.654000 28.153000 359048.697000 5726386.654000 34.542000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_aae0cdfe-9999-4876-ab6b-78099199c074">
			<core:creationDate>2018-02-02</core:creationDate>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo893f3e60-ecef-4795-9a6e-eb8d62242e3f">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo858638">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID858638">
									<gml:posList srsDimension="3">359046.636000 5726394.967000 34.542000 359058.143000 5726397.853000 34.542000 359058.143000 5726397.853000 28.153000 359046.636000 5726394.967000 28.153000 359046.636000 5726394.967000 34.542000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_5380600e-c11b-4554-aeb7-4736d6af6c1c">
			<core:creationDate>2018-02-02</core:creationDate>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo11bc0584-e6a5-4028-b88e-c9a3480880c3">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo858639">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID858639">
									<gml:posList srsDimension="3">359048.697000 5726386.654000 34.542000 359060.226000 5726389.524000 34.542000 359054.462000 5726388.089000 40.468000 359048.697000 5726386.654000 34.542000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_95b88cb7-05db-439a-baff-3ed04a858c66">
			<core:creationDate>2018-02-02</core:creationDate>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo18f8d258-19d3-4afa-afa3-f1e0157309a6">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo858640">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID858640">
									<gml:posList srsDimension="3">359058.143000 5726397.853000 34.542000 359046.636000 5726394.967000 34.542000 359052.390000 5726396.410000 40.468000 359058.143000 5726397.853000 34.542000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_c9bc4669-5807-4d11-8ac7-5b5c0ea93010">
			<core:creationDate>2018-02-02</core:creationDate>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo610c5908-8b18-46bf-bbce-d4bbff598ff0">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo858641">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID858641">
									<gml:posList srsDimension="3">359060.226000 5726389.524000 34.542000 359058.143000 5726397.853000 34.542000 359052.390000 5726396.410000 40.468000 359054.462000 5726388.089000 40.468000 359060.226000 5726389.524000 34.542000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_b7df8709-f855-4c78-81b8-07c22b5365ad">
			<core:creationDate>2018-02-02</core:creationDate>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo2098ea00-08a1-42db-8f2e-ee6155cc3141">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo858642">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID858642">
									<gml:posList srsDimension="3">359046.636000 5726394.967000 34.542000 359048.697000 5726386.654000 34.542000 359054.462000 5726388.089000 40.468000 359052.390000 5726396.410000 40.468000 359046.636000 5726394.967000 34.542000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:GroundSurface gml:id="UUID_83ade68c-abff-4d29-81ce-5f4272c7d476">
			<core:creationDate>2018-02-02</core:creationDate>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoa0ac2691-f5ca-4346-bc7a-de88ce7a8812">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo858643">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID858643">
									<gml:posList srsDimension="3">359058.143000 5726397.853000 28.153000 359060.226000 5726389.524000 28.153000 359048.697000 5726386.654000 28.153000 359046.636000 5726394.967000 28.153000 359058.143000 5726397.853000 28.153000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:GroundSurface>
	</bldg:boundedBy>
	<bldg:address>
		<core:Address>
			<core:xalAddress>
				<xAL:AddressDetails>
					<xAL:Country>
						<xAL:CountryName>
							Germany
						</xAL:CountryName>
						<xAL:Locality  Type="Town">
							<xAL:LocalityName>
								Dorsten
							</xAL:LocalityName>
							<xAL:Thoroughfare  Type="Street">
								<xAL:ThoroughfareNumber>
									16
								</xAL:ThoroughfareNumber>
								<xAL:ThoroughfareName>
									Gartenstraße
								</xAL:ThoroughfareName>
							</xAL:Thoroughfare>
						</xAL:Locality>
					</xAL:Country>
				</xAL:AddressDetails>
			</core:xalAddress>
		</core:Address>
	</bldg:address>
</bldg:Building>
</core:cityObjectMember>
<core:cityObjectMember>
<bldg:Building gml:id="DENW_e916da52-a640-4c67-ab2a-19b61e626728">
	<core:creationDate>2018-09-19</core:creationDate>
	<core:externalReference>
		<core:informationSystem>http://www.adv-online.de/fdv/art.htm#_9100</core:informationSystem>
		<core:externalObject>
			<core:name>DENW52AL000hnChq</core:name>
		</core:externalObject>
	</core:externalReference>
	<gen:stringAttribute name="DatenquelleDachhoehe">
		<gen:value>6000</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="DatenquelleLage">
		<gen:value>1000</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="DatenquelleBodenhoehe">
		<gen:value>1100</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="Gemeindeschluessel">
		<gen:value>05562012</gen:value>
	</gen:stringAttribute>
	<bldg:measuredHeight uom="urn:ogc:def:uom:UCUM::m">4.37</bldg:measuredHeight>
	<bldg:lod2Solid>
		<gml:Solid gml:id="IDGeo016457d0-2872-4fd5-b130-fa930050db5b">
			<gml:exterior>
				<gml:CompositeSurface>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596268"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596269"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596270"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596271"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596272"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596273"/>
				</gml:CompositeSurface>
			</gml:exterior>
		</gml:Solid>
	</bldg:lod2Solid>
	<bldg:boundedBy>
		<bldg:WallSurface>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeod7fe21c1-e1fc-46ae-9e44-d84e4ef5d947">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596268">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596268">
									<gml:posList srsDimension="3">359022.757000 5726408.687000 28.148000 359022.757000 5726408.687000 31.150000 359023.443000 5726406.124000 31.150000 359023.443000 5726406.124000 28.148000 359022.757000 5726408.687000 28.148000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:GroundSurface>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeobf246875-fbbe-4558-bd43-046bb887f428">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596269">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596269">
									<gml:posList srsDimension="3">359020.301000 5726408.038000 28.148000 359022.757000 5726408.687000 28.148000 359023.443000 5726406.124000 28.148000 359021.013000 5726405.463000 28.148000 359020.301000 5726408.038000 28.148000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:GroundSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo9e3b30b5-a72e-4ef8-9dc5-1a7afe43aeb0">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596270">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596270">
									<gml:posList srsDimension="3">359023.443000 5726406.124000 28.148000 359023.443000 5726406.124000 31.150000 359021.013000 5726405.463000 32.531000 359021.013000 5726405.463000 28.148000 359023.443000 5726406.124000 28.148000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoeb71cbea-edc3-4d00-98e0-f15d277c6e75">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596271">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596271">
									<gml:posList srsDimension="3">359021.013000 5726405.463000 28.148000 359021.013000 5726405.463000 32.531000 359020.301000 5726408.038000 32.543072 359020.301000 5726408.038000 28.148000 359021.013000 5726405.463000 28.148000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoffa04b8e-5d20-4709-9600-acfc436984ac">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596272">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596272">
									<gml:posList srsDimension="3">359020.301000 5726408.038000 28.148000 359020.301000 5726408.038000 32.543072 359022.757000 5726408.687000 31.150000 359022.757000 5726408.687000 28.148000 359020.301000 5726408.038000 28.148000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1554_664926_299639">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo6ac447ca-de15-42d5-a614-4389cd3310f0">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596273">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596273">
									<gml:posList srsDimension="3">359021.013000 5726405.463000 32.531000 359023.443000 5726406.124000 31.150000 359022.757000 5726408.687000 31.150000 359020.301000 5726408.038000 32.543072 359021.013000 5726405.463000 32.531000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:address>
		<core:Address>
			<core:xalAddress>
				<xAL:AddressDetails>
					<xAL:Country>
						<xAL:CountryName>
							Germany
						</xAL:CountryName>
						<xAL:Locality  Type="Town">
							<xAL:LocalityName>
								Dorsten
							</xAL:LocalityName>
							<xAL:Thoroughfare>
								<xAL:ThoroughfareNumber>
									12
								</xAL:ThoroughfareNumber>
								<xAL:ThoroughfareName>
									Gartenstra�e
								</xAL:ThoroughfareName>
							</xAL:Thoroughfare>
							<xAL:PostalCode>
								<xAL:PostalCodeNumber>
									46284
								</xAL:PostalCodeNumber>
							</xAL:PostalCode>
						</xAL:Locality>
					</xAL:Country>
				</xAL:AddressDetails>
			</core:xalAddress>
			<core:multiPoint>
				<gml:MultiPoint>
					<gml:pointMember>
						<gml:Point>
							<gml:pos>359021.875 5726407.0 0.0</gml:pos>
						</gml:Point>
					</gml:pointMember>
				</gml:MultiPoint>
			</core:multiPoint>
		</core:Address>
	</bldg:address>
</bldg:Building>
</core:cityObjectMember>
<core:cityObjectMember>
<bldg:Building gml:id="DENW_8b279c9f-bfd4-4a79-8f2e-089585107b85">
	<core:creationDate>2018-09-19</core:creationDate>
	<core:externalReference>
		<core:informationSystem>http://www.adv-online.de/fdv/art.htm#_9100</core:informationSystem>
		<core:externalObject>
			<core:name>DENW52AL000hnCjd</core:name>
		</core:externalObject>
	</core:externalReference>
	<gen:stringAttribute name="DatenquelleDachhoehe">
		<gen:value>6000</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="DatenquelleLage">
		<gen:value>1000</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="DatenquelleBodenhoehe">
		<gen:value>1100</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="Gemeindeschluessel">
		<gen:value>05562012</gen:value>
	</gen:stringAttribute>
	<bldg:measuredHeight uom="urn:ogc:def:uom:UCUM::m">4.3</bldg:measuredHeight>
	<bldg:lod2Solid>
		<gml:Solid gml:id="IDGeo69dacd00-e349-4559-9547-f10f043662ef">
			<gml:exterior>
				<gml:CompositeSurface>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596275"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596276"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596277"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596278"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596279"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596280"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992596281"/>
				</gml:CompositeSurface>
			</gml:exterior>
		</gml:Solid>
	</bldg:lod2Solid>
	<bldg:boundedBy>
		<bldg:WallSurface>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeof0924d79-9e12-448d-aaf0-10bd44311d58">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596275">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596275">
									<gml:posList srsDimension="3">359021.675000 5726403.018000 28.231000 359021.675000 5726403.018000 32.531000 359018.440000 5726402.162000 31.150000 359018.440000 5726402.162000 28.231000 359021.675000 5726403.018000 28.231000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo700f7337-a7d1-497b-9109-67e15cf237ff">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596276">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596276">
									<gml:posList srsDimension="3">359021.675000 5726403.018000 28.231000 359021.013000 5726405.463000 28.231000 359021.013000 5726405.463000 32.531000 359021.675000 5726403.018000 32.531000 359021.675000 5726403.018000 28.231000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:GroundSurface>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo5d8b85f5-faf9-44be-95f0-7e1b561071fe">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596277">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596277">
									<gml:posList srsDimension="3">359017.801000 5726404.588000 28.238113 359021.013000 5726405.463000 28.231000 359018.440000 5726402.162000 28.231000 359017.801000 5726404.588000 28.238113 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596278">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596278">
									<gml:posList srsDimension="3">359021.675000 5726403.018000 28.231000 359018.440000 5726402.162000 28.231000 359021.013000 5726405.463000 28.231000 359021.675000 5726403.018000 28.231000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:GroundSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo48c88323-9cbc-4943-bd71-985a361b2560">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596279">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596279">
									<gml:posList srsDimension="3">359017.801000 5726404.588000 28.238113 359017.801000 5726404.588000 31.157113 359021.013000 5726405.463000 32.531000 359021.013000 5726405.463000 28.231000 359017.801000 5726404.588000 28.238113 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface>
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo15165da4-e122-4276-ba36-0075f5692810">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596280">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596280">
									<gml:posList srsDimension="3">359018.440000 5726402.162000 31.150000 359017.801000 5726404.588000 31.157113 359017.801000 5726404.588000 28.238113 359018.440000 5726402.162000 28.231000 359018.440000 5726402.162000 31.150000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_104_533470_236306">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeof21936e3-37a5-43b3-a542-61cf468f1fb9">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992596281">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992596281">
									<gml:posList srsDimension="3">359021.675000 5726403.018000 32.531000 359021.013000 5726405.463000 32.531000 359017.801000 5726404.588000 31.157113 359018.440000 5726402.162000 31.150000 359021.675000 5726403.018000 32.531000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:address>
		<core:Address>
			<core:xalAddress>
				<xAL:AddressDetails>
					<xAL:Country>
						<xAL:CountryName>
							Germany
						</xAL:CountryName>
						<xAL:Locality  Type="Town">
							<xAL:LocalityName>
								Dorsten
							</xAL:LocalityName>
							<xAL:Thoroughfare>
								<xAL:ThoroughfareNumber>
									14
								</xAL:ThoroughfareNumber>
								<xAL:ThoroughfareName>
									Gartenstra�e
								</xAL:ThoroughfareName>
							</xAL:Thoroughfare>
							<xAL:PostalCode>
								<xAL:PostalCodeNumber>
									46284
								</xAL:PostalCodeNumber>
							</xAL:PostalCode>
						</xAL:Locality>
					</xAL:Country>
				</xAL:AddressDetails>
			</core:xalAddress>
			<core:multiPoint>
				<gml:MultiPoint>
					<gml:pointMember>
						<gml:Point>
							<gml:pos>359019.734 5726404.0 0.0</gml:pos>
						</gml:Point>
					</gml:pointMember>
				</gml:MultiPoint>
			</core:multiPoint>
		</core:Address>
	</bldg:address>
</bldg:Building>
</core:cityObjectMember>
<core:cityObjectMember>
<bldg:Building gml:id="DENW_fb86ba19-6fe3-4bca-84af-6091453e6b55">
	<core:creationDate>2018-09-19</core:creationDate>
	<core:externalReference>
		<core:informationSystem>http://www.adv-online.de/fdv/art.htm#_9100</core:informationSystem>
		<core:externalObject>
			<core:name>DENW52AL000hnBCx</core:name>
		</core:externalObject>
	</core:externalReference>
	<gen:stringAttribute name="DatenquelleDachhoehe">
		<gen:value>6000</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="DatenquelleLage">
		<gen:value>1000</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="DatenquelleBodenhoehe">
		<gen:value>1100</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="Gemeindeschluessel">
		<gen:value>05562012</gen:value>
	</gen:stringAttribute>
	<bldg:measuredHeight uom="urn:ogc:def:uom:UCUM::m">10.84</bldg:measuredHeight>
	<bldg:consistsOfBuildingPart>
		<bldg:BuildingPart gml:id="UUID_BuildingPart_1352_107604_142412">
			<bldg:lod2Solid>
				<gml:Solid gml:id="IDGeo1183006e-fb24-4b69-b1d1-f99eb8c414ff">
					<gml:exterior>
						<gml:CompositeSurface>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596351"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596352"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596353"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596354"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596355"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596356"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596357"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596358"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596359"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596360"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596361"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596362"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596363"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596364"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596365"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596366"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596367"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596368"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596369"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596370"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596371"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596372"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596373"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596374"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596375"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596376"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596377"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596378"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596379"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596380"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596381"/>
						</gml:CompositeSurface>
					</gml:exterior>
				</gml:Solid>
			</bldg:lod2Solid>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_WallSurface_1841_741517_315291">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeoac17e9fb-7138-4703-8601-4e707edfcd73">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596351">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596351">
											<gml:posList srsDimension="3">359044.673000 5726414.942000 27.937000 359045.418000 5726412.088000 27.937000 359045.418000 5726412.088000 35.000000 359044.546713 5726415.425789 35.000000 359044.673000 5726414.942000 32.070000 359044.673000 5726414.942000 27.937000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_RoofSurface_1497_802021_154244">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo5271f99e-0484-4c99-bbc5-4d3b398e1d26">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596352">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596352">
											<gml:posList srsDimension="3">359053.332848 5726417.210263 35.070000 359053.207569 5726417.694314 32.070000 359052.848797 5726417.084985 35.070000 359053.332848 5726417.210263 35.070000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_RoofSurface_1835_340370_296229">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo9aefaea4-929e-41f9-8a18-dd5c4ce711d7">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596353">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596353">
											<gml:posList srsDimension="3">359053.207569 5726417.694314 32.070000 359051.977000 5726422.449000 32.070000 359051.620682 5726421.830185 35.070000 359052.848797 5726417.084985 35.070000 359053.207569 5726417.694314 32.070000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_WallSurface_994_77934_389405">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeoe4a45778-7917-409e-bd20-036b53f28b85">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596354">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596354">
											<gml:posList srsDimension="3">359044.673000 5726414.942000 27.937000 359044.673000 5726414.942000 32.070000 359043.410000 5726414.624000 32.070000 359043.410000 5726414.624000 27.937000 359044.673000 5726414.942000 27.937000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_RoofSurface_915_555911_397920">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeobdfdb2e6-25f5-4022-949a-f48b9d9a26f9">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596355">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596355">
											<gml:posList srsDimension="3">359044.673000 5726414.942000 32.070000 359044.546713 5726415.425789 35.000000 359043.284477 5726415.109640 35.009374 359043.410000 5726414.624000 32.070000 359044.673000 5726414.942000 32.070000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_RoofSurface_235_481155_430036">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo5ef91812-3fb0-48c9-89ee-5ff744e19ed2">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596356">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596356">
											<gml:posList srsDimension="3">359048.905000 5726416.473000 38.736682 359050.538920 5726413.416873 38.000000 359052.848797 5726417.084985 35.070000 359048.905000 5726416.473000 38.736682 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_RoofSurface_31_696378_2110">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo6b4965e0-b888-406b-90e9-ce32c1e10a08">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596357">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596357">
											<gml:posList srsDimension="3">359050.538920 5726413.416873 38.000000 359048.905000 5726416.473000 38.736682 359049.747500 5726413.211500 38.776000 359050.538920 5726413.416873 38.000000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_RoofSurface_1101_462285_72287">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeoa64278ff-0da3-4699-9529-c52fd37bfb4c">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596358">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596358">
											<gml:posList srsDimension="3">359050.178002 5726421.982992 32.070000 359049.821226 5726421.366389 35.056662 359051.620682 5726421.830185 35.070000 359051.977000 5726422.449000 32.070000 359051.492976 5726422.323617 32.070000 359050.178002 5726421.982992 32.070000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_RoofSurface_1287_166324_341374">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeoab4663ed-9080-4f97-85e8-8fa1f39607ec">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596359">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596359">
											<gml:posList srsDimension="3">359042.532497 5726418.018999 37.889543 359047.115626 5726419.203598 37.889541 359044.536309 5726420.521534 36.078796 359042.052002 5726419.878001 36.080089 359042.532497 5726418.018999 37.889543 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_RoofSurface_953_36956_381427">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo40f1355a-59c1-4e5e-ac67-5df8ea69c97a">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596360">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596360">
											<gml:posList srsDimension="3">359045.085848 5726420.662157 36.552619 359047.115626 5726419.203598 37.889541 359046.989827 5726419.690304 37.889541 359045.085848 5726420.662157 36.552619 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_RoofSurface_435_66122_372169">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo7eea26bf-85ee-4ba9-ac43-44482625f9d3">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596361">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596361">
											<gml:posList srsDimension="3">359045.085848 5726420.662157 36.552619 359045.083603 5726420.663304 36.551043 359044.536309 5726420.521534 36.078796 359047.115626 5726419.203598 37.889541 359045.085848 5726420.662157 36.552619 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_WallSurface_116_570905_385597">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo2db2eb2f-4616-49bd-a54f-91c46aaa32e4">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596362">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596362">
											<gml:posList srsDimension="3">359049.693484 5726421.857471 35.056662 359050.178002 5726421.982992 32.070000 359051.492976 5726422.323617 32.070000 359051.977000 5726422.449000 32.070000 359051.977000 5726422.449000 27.937000 359042.052000 5726419.878000 27.937000 359042.052002 5726419.878001 36.080089 359044.536309 5726420.521534 36.078796 359045.083603 5726420.663304 36.551043 359048.081520 5726421.439893 36.548439 359049.693484 5726421.857471 35.056662 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_RoofSurface_539_91268_242182">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo757c7bec-4032-4c8a-a11d-f145b9bbd44e">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596363">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596363">
											<gml:posList srsDimension="3">359050.178002 5726421.982992 32.070000 359049.693484 5726421.857471 35.056662 359049.821226 5726421.366389 35.056662 359050.178002 5726421.982992 32.070000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_RoofSurface_632_489818_103416">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo91cd8380-8978-4c36-80fc-41ecd494ab03">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596364">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596364">
											<gml:posList srsDimension="3">359047.513516 5726417.664188 37.889541 359051.620682 5726421.830185 35.070000 359049.821226 5726421.366389 35.056662 359047.513516 5726417.664188 37.889541 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_RoofSurface_382_402893_425985">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo8d0a95c3-37ab-4e9f-aec0-a4d6d95f3789">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596365">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596365">
											<gml:posList srsDimension="3">359048.905000 5726416.473000 38.736682 359045.418000 5726412.088000 35.000000 359049.747500 5726413.211500 38.776000 359048.905000 5726416.473000 38.736682 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_RoofSurface_667_89246_173344">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeod95352c5-91bb-4d4d-a0d9-d6f8e4f5a944">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596366">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596366">
											<gml:posList srsDimension="3">359045.418000 5726412.088000 35.000000 359048.905000 5726416.473000 38.736682 359047.513516 5726417.664188 37.889541 359045.418000 5726412.088000 35.000000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="UUID_GroundSurface_1737_152761_326048">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo27441c29-f179-4065-aafe-dfd1fdee523d">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596367">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596367">
											<gml:posList srsDimension="3">359044.673000 5726414.942000 27.937000 359043.410000 5726414.624000 27.937000 359042.052000 5726419.878000 27.937000 359051.977000 5726422.449000 27.937000 359054.077000 5726414.335000 27.937000 359045.418000 5726412.088000 27.937000 359044.673000 5726414.942000 27.937000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_RoofSurface_940_797983_277104">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo737ec9f0-9e60-45a4-9127-e5143bf4e0d0">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596368">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596368">
											<gml:posList srsDimension="3">359044.546713 5726415.425789 35.000000 359047.513516 5726417.664188 37.889541 359047.115626 5726419.203598 37.889541 359044.546713 5726415.425789 35.000000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_RoofSurface_554_630492_387593">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo9fd3cd3f-9558-4b8c-8aa1-1b78ce6d94e3">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596369">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596369">
											<gml:posList srsDimension="3">359047.513516 5726417.664188 37.889541 359044.546713 5726415.425789 35.000000 359045.418000 5726412.088000 35.000000 359047.513516 5726417.664188 37.889541 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_WallSurface_1367_2099_253245">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeodec94474-3e5f-4c8b-a45c-50f2d45ba6e0">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596370">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596370">
											<gml:posList srsDimension="3">359043.410000 5726414.624000 32.070000 359043.284477 5726415.109640 35.009374 359042.532497 5726418.018999 37.889543 359042.052002 5726419.878001 36.080089 359042.052000 5726419.878000 27.937000 359043.410000 5726414.624000 27.937000 359043.410000 5726414.624000 32.070000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_RoofSurface_226_10424_155951">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo5c5bea4e-08fc-40e4-beab-64215a230616">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596371">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596371">
											<gml:posList srsDimension="3">359048.905000 5726416.473000 38.736682 359052.848797 5726417.084985 35.070000 359051.620682 5726421.830185 35.070000 359048.905000 5726416.473000 38.736682 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_RoofSurface_358_559865_341102">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo58d2a6b6-d744-474c-a698-87baa66d5b2f">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596372">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596372">
											<gml:posList srsDimension="3">359051.620682 5726421.830185 35.070000 359047.513516 5726417.664188 37.889541 359048.905000 5726416.473000 38.736682 359051.620682 5726421.830185 35.070000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_RoofSurface_953_805112_324562">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo90eb59a6-92e0-45a1-be84-0c01812abfdf">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596373">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596373">
											<gml:posList srsDimension="3">359048.081520 5726421.439893 36.548439 359045.083603 5726420.663304 36.551043 359045.085848 5726420.662157 36.552619 359046.989827 5726419.690304 37.889541 359048.081520 5726421.439893 36.548439 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_RoofSurface_657_723210_44787">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo6d8c982f-8d47-465e-b1de-ce3a5c26e5f9">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596374">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596374">
											<gml:posList srsDimension="3">359048.081520 5726421.439893 36.548439 359049.821226 5726421.366389 35.056662 359049.693484 5726421.857471 35.056662 359048.081520 5726421.439893 36.548439 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_RoofSurface_1556_851394_303550">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo59b86986-64fa-4d6e-ac51-056cc67d8e24">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596375">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596375">
											<gml:posList srsDimension="3">359049.821226 5726421.366389 35.056662 359047.115626 5726419.203598 37.889541 359047.513516 5726417.664188 37.889541 359049.821226 5726421.366389 35.056662 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_RoofSurface_843_100344_20548">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeoc5ff7681-92c3-4695-9c82-bdc4e9d4e2cb">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596376">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596376">
											<gml:posList srsDimension="3">359047.115626 5726419.203598 37.889541 359048.081520 5726421.439893 36.548439 359046.989827 5726419.690304 37.889541 359047.115626 5726419.203598 37.889541 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_RoofSurface_1326_25746_24579">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo6c62653a-d760-454d-b03b-b09bbf6ba3b9">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596377">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596377">
											<gml:posList srsDimension="3">359048.081520 5726421.439893 36.548439 359047.115626 5726419.203598 37.889541 359049.821226 5726421.366389 35.056662 359048.081520 5726421.439893 36.548439 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_RoofSurface_1050_316190_266257">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo149db4cc-4b1d-4596-b697-25a478c47fb7">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596378">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596378">
											<gml:posList srsDimension="3">359054.077000 5726414.335000 37.997681 359053.332848 5726417.210263 35.070000 359052.848797 5726417.084985 35.070000 359050.538920 5726413.416873 38.000000 359054.077000 5726414.335000 37.997681 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_WallSurface_188_279185_112425">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeoe3e84b9e-7dea-4c7b-9ba2-8f3308311987">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596379">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596379">
											<gml:posList srsDimension="3">359054.077000 5726414.335000 27.937000 359054.077000 5726414.335000 37.997681 359050.538920 5726413.416873 38.000000 359049.747500 5726413.211500 38.776000 359045.418000 5726412.088000 35.000000 359045.418000 5726412.088000 27.937000 359054.077000 5726414.335000 27.937000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_WallSurface_1076_720553_101597">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeoca2b04b3-6f57-4315-89d1-0de5b096e42a">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596380">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596380">
											<gml:posList srsDimension="3">359054.077000 5726414.335000 27.937000 359051.977000 5726422.449000 27.937000 359051.977000 5726422.449000 32.070000 359053.207569 5726417.694314 32.070000 359053.332848 5726417.210263 35.070000 359054.077000 5726414.335000 37.997681 359054.077000 5726414.335000 27.937000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_RoofSurface_1075_187913_323108">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo3bc6aba4-d763-486e-b512-d14b79645dae">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596381">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596381">
											<gml:posList srsDimension="3">359047.115626 5726419.203598 37.889541 359042.532497 5726418.018999 37.889543 359043.284477 5726415.109640 35.009374 359044.546713 5726415.425789 35.000000 359047.115626 5726419.203598 37.889541 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
		</bldg:BuildingPart>
	</bldg:consistsOfBuildingPart>
	<bldg:consistsOfBuildingPart>
		<bldg:BuildingPart gml:id="UUID_BuildingPart_1030_849965_208139">
			<bldg:lod2Solid>
				<gml:Solid gml:id="IDGeo30750765-41d3-427c-b1c7-4abf4547bebe">
					<gml:exterior>
						<gml:CompositeSurface>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596382"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596383"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596384"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596385"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596386"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596387"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596388"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596389"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596390"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596391"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596392"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596393"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596394"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596395"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596396"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo992596397"/>
						</gml:CompositeSurface>
					</gml:exterior>
				</gml:Solid>
			</bldg:lod2Solid>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_WallSurface_864_726144_88439">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeoedb2b3b2-7186-40e4-a689-7d538e1af9d4">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596382">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596382">
											<gml:posList srsDimension="3">359049.780000 5726423.511000 27.935000 359049.780000 5726423.511000 32.070000 359050.178002 5726421.982992 32.070000 359050.178000 5726421.983000 27.935000 359049.780000 5726423.511000 27.935000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="UUID_GroundSurface_83_286040_365103">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeob2349fa6-c041-44c2-bc80-a23369ce8ab9">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596383">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596383">
											<gml:posList srsDimension="3">359050.178000 5726421.983000 27.935000 359042.052000 5726419.878000 27.935000 359041.655000 5726421.414000 27.935000 359049.780000 5726423.511000 27.935000 359050.178000 5726421.983000 27.935000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_WallSurface_1875_654422_289195">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo07057a6e-67ae-49d5-bc02-26a257cdb6d9">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596384">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596384">
											<gml:posList srsDimension="3">359041.655000 5726421.414000 32.070000 359042.471245 5726421.624667 32.070000 359042.955380 5726421.749618 35.056663 359049.295865 5726423.386048 35.056662 359049.780000 5726423.511000 32.070000 359049.780000 5726423.511000 27.935000 359041.655000 5726421.414000 27.935000 359041.655000 5726421.414000 32.070000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_WallSurface_1129_723026_353491">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo90b3d9c4-b8a1-49ad-a05d-5b68f34ccedc">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596385">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596385">
											<gml:posList srsDimension="3">359042.052000 5726419.878000 27.935000 359042.052002 5726419.878001 36.080089 359041.780120 5726420.929908 35.000000 359041.655000 5726421.414000 32.070000 359041.655000 5726421.414000 27.935000 359042.052000 5726419.878000 27.935000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_RoofSurface_1229_586241_222804">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo4f89ca50-b0ed-4e56-be30-b19b64988996">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596386">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596386">
											<gml:posList srsDimension="3">359042.955380 5726421.749618 35.056663 359042.471245 5726421.624667 32.070000 359043.080331 5726421.265485 35.056663 359042.955380 5726421.749618 35.056663 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_RoofSurface_1085_856955_15982">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo16e3d35f-2349-41e6-80b5-be7f95247a5c">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596387">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596387">
											<gml:posList srsDimension="3">359043.080331 5726421.265485 35.056663 359041.655000 5726421.414000 32.070000 359041.780120 5726420.929908 35.000000 359043.080331 5726421.265485 35.056663 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_RoofSurface_1623_290484_316547">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo2a711957-cae5-4e7a-901d-5716dccb2874">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596388">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596388">
											<gml:posList srsDimension="3">359041.655000 5726421.414000 32.070000 359043.080331 5726421.265485 35.056663 359042.471245 5726421.624667 32.070000 359041.655000 5726421.414000 32.070000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_RoofSurface_688_503747_262718">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeoec930993-6ef2-4bb4-8626-b2e366375b71">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596389">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596389">
											<gml:posList srsDimension="3">359044.536309 5726420.521534 36.078796 359042.955380 5726421.749618 35.056663 359043.080331 5726421.265485 35.056663 359044.536309 5726420.521534 36.078796 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_RoofSurface_1905_754907_84310">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo7b7d8629-8a06-4426-9c6e-80029d7bffe0">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596390">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596390">
											<gml:posList srsDimension="3">359045.083603 5726420.663304 36.551043 359042.955380 5726421.749618 35.056663 359044.536309 5726420.521534 36.078796 359045.083603 5726420.663304 36.551043 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_RoofSurface_313_636687_335935">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeoa5639b02-ca41-4372-865a-49ceb6a1f84e">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596391">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596391">
											<gml:posList srsDimension="3">359042.955380 5726421.749618 35.056663 359045.083603 5726420.663304 36.551043 359048.081520 5726421.439893 36.548439 359049.295865 5726423.386048 35.056662 359042.955380 5726421.749618 35.056663 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_RoofSurface_482_343205_29158">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeoee282b1f-fffa-47da-ac75-23521a71066f">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596392">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596392">
											<gml:posList srsDimension="3">359043.080331 5726421.265485 35.056663 359042.052002 5726419.878001 36.080089 359044.536309 5726420.521534 36.078796 359043.080331 5726421.265485 35.056663 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_RoofSurface_1551_221684_151213">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo4dc5499e-d0c6-4f43-bae3-d48203f2fae1">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596393">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596393">
											<gml:posList srsDimension="3">359042.052002 5726419.878001 36.080089 359043.080331 5726421.265485 35.056663 359041.780120 5726420.929908 35.000000 359042.052002 5726419.878001 36.080089 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_RoofSurface_770_358238_296512">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo844a11e9-d026-4971-92c9-c2c1a638c937">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596394">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596394">
											<gml:posList srsDimension="3">359049.295865 5726423.386048 35.056662 359050.178002 5726421.982992 32.070000 359049.780000 5726423.511000 32.070000 359049.295865 5726423.386048 35.056662 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_RoofSurface_1943_142764_139522">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo99b28db0-a073-4bdf-959c-e3240dcad1b7">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596395">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596395">
											<gml:posList srsDimension="3">359050.178002 5726421.982992 32.070000 359049.295865 5726423.386048 35.056662 359049.693484 5726421.857471 35.056662 359050.178002 5726421.982992 32.070000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_RoofSurface_235_55963_158029">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeoa021c452-3fac-47da-a900-84e673cddbad">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596396">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596396">
											<gml:posList srsDimension="3">359049.295865 5726423.386048 35.056662 359048.081520 5726421.439893 36.548439 359049.693484 5726421.857471 35.056662 359049.295865 5726423.386048 35.056662 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_WallSurface_1978_468076_230196">
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeoe9fe3993-3711-492c-a231-e8b00a403005">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo992596397">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID992596397">
											<gml:posList srsDimension="3">359050.178000 5726421.983000 27.935000 359050.178002 5726421.982992 32.070000 359049.693484 5726421.857471 35.056662 359048.081520 5726421.439893 36.548439 359045.083603 5726420.663304 36.551043 359044.536309 5726420.521534 36.078796 359042.052002 5726419.878001 36.080089 359042.052000 5726419.878000 27.935000 359050.178000 5726421.983000 27.935000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
		</bldg:BuildingPart>
	</bldg:consistsOfBuildingPart>
	<bldg:address>
		<core:Address>
			<core:xalAddress>
				<xAL:AddressDetails>
					<xAL:Country>
						<xAL:CountryName>
							Germany
						</xAL:CountryName>
						<xAL:Locality  Type="Town">
							<xAL:LocalityName>
								Dorsten
							</xAL:LocalityName>
							<xAL:Thoroughfare>
								<xAL:ThoroughfareNumber>
									12
								</xAL:ThoroughfareNumber>
								<xAL:ThoroughfareName>
									Gartenstra�e
								</xAL:ThoroughfareName>
							</xAL:Thoroughfare>
							<xAL:PostalCode>
								<xAL:PostalCodeNumber>
									46284
								</xAL:PostalCodeNumber>
							</xAL:PostalCode>
						</xAL:Locality>
					</xAL:Country>
				</xAL:AddressDetails>
			</core:xalAddress>
			<core:multiPoint>
				<gml:MultiPoint>
					<gml:pointMember>
						<gml:Point>
							<gml:pos>359047.891 5726418.0 0.0</gml:pos>
						</gml:Point>
					</gml:pointMember>
				</gml:MultiPoint>
			</core:multiPoint>
		</core:Address>
	</bldg:address>
</bldg:Building>
</core:cityObjectMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596393</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596392</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596395</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596394</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596396</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596387</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596386</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596389</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596388</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596391</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596390</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596361</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596360</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596363</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596365</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596364</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596366</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596353</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596352</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596355</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596357</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596356</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596359</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596358</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596377</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596376</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596378</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596381</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596369</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596368</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596371</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596373</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596372</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596375</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.694 0.133 0.133</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992596374</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<core:cityObjectMember>
<bldg:Building gml:id="DENW52AL000hnCjC">
	<core:creationDate>2018-02-02</core:creationDate>
	<core:externalReference>
		<core:informationSystem>http://repository.gdi-de.org/schemas/adv/citygml/fdv/art.htm#_9100</core:informationSystem>
		<core:externalObject>
			<core:name>DENW52AL000hnCjC</core:name>
		</core:externalObject>
	</core:externalReference>
	<gen:stringAttribute name="Gemeindeschluessel">
		<gen:value>05562012</gen:value>
	</gen:stringAttribute>
	<bldg:consistsOfBuildingPart>
		<bldg:BuildingPart gml:id="UUID_cc5dc3a7-a7dd-4998-96f7-db5719b89b3b">
			<core:creationDate>2018-02-02</core:creationDate>
			<gen:stringAttribute name="DatenquelleDachhoehe">
				<gen:value>1000</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="DatenquelleLage">
				<gen:value>1000</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="DatenquelleBodenhoehe">
				<gen:value>1100</gen:value>
			</gen:stringAttribute>
			<bldg:measuredHeight uom="urn:adv:uom:m">7.232</bldg:measuredHeight>
			<bldg:lod2Solid>
				<gml:Solid gml:id="IDGeo93af67cb-d27c-4483-be10-8e0d01332edf">
					<gml:exterior>
						<gml:CompositeSurface>
							<gml:surfaceMember xlink:href="#PolyIDGeo851464"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo851477"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo851469"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo851481"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo851476"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo851475"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo851472"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo851471"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo851466"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo851467"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo851468"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo851465"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo851480"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo851463"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo851478"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo851473"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo851470"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo851474"/>
						</gml:CompositeSurface>
					</gml:exterior>
				</gml:Solid>
			</bldg:lod2Solid>
			<bldg:lod2TerrainIntersection>
				<gml:MultiCurve>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>358999.797 5726382.922 28.008</gml:pos>
							<gml:pos>358999.824 5726382.824 28.014</gml:pos>
							<gml:pos>359000.0 5726382.171 27.945</gml:pos>
							<gml:pos>359000.036 5726382.036 27.948</gml:pos>
							<gml:pos>359000.046 5726382.0 27.951</gml:pos>
							<gml:pos>359000.249 5726381.249 28.087</gml:pos>
							<gml:pos>359000.317 5726381.0 28.104</gml:pos>
							<gml:pos>359000.462 5726380.462 28.184</gml:pos>
							<gml:pos>359000.587 5726380.0 28.276</gml:pos>
							<gml:pos>359000.675 5726379.675 28.486</gml:pos>
							<gml:pos>359000.843 5726379.055 28.794</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>358998.751 5726386.788 28.563</gml:pos>
							<gml:pos>358998.759 5726386.759 28.555</gml:pos>
							<gml:pos>358998.964 5726386.0 28.196</gml:pos>
							<gml:pos>358998.972 5726385.972 28.192</gml:pos>
							<gml:pos>358999.0 5726385.868 28.173</gml:pos>
							<gml:pos>358999.185 5726385.185 28.102</gml:pos>
							<gml:pos>358999.235 5726385.0 28.044</gml:pos>
							<gml:pos>358999.398 5726384.398 28.038</gml:pos>
							<gml:pos>358999.505 5726384.0 28.025</gml:pos>
							<gml:pos>358999.611 5726383.611 28.035</gml:pos>
							<gml:pos>358999.776 5726383.0 28.004</gml:pos>
							<gml:pos>358999.797 5726382.922 28.008</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>358998.751 5726386.788 28.563</gml:pos>
							<gml:pos>358998.802 5726386.802 28.566</gml:pos>
							<gml:pos>358999.0 5726386.855 28.558</gml:pos>
							<gml:pos>358999.536 5726387.0 28.695</gml:pos>
							<gml:pos>359000.0 5726387.125 28.768</gml:pos>
							<gml:pos>359000.171 5726387.171 28.758</gml:pos>
							<gml:pos>359001.0 5726387.395 28.726</gml:pos>
							<gml:pos>359001.541 5726387.541 28.748</gml:pos>
							<gml:pos>359002.0 5726387.665 28.763</gml:pos>
							<gml:pos>359002.911 5726387.911 28.894</gml:pos>
							<gml:pos>359003.0 5726387.935 28.903</gml:pos>
							<gml:pos>359003.242 5726388.0 28.91</gml:pos>
							<gml:pos>359004.0 5726388.205 28.906</gml:pos>
							<gml:pos>359004.28 5726388.28 28.899</gml:pos>
							<gml:pos>359005.0 5726388.475 28.886</gml:pos>
							<gml:pos>359005.65 5726388.65 28.868</gml:pos>
							<gml:pos>359006.0 5726388.744 28.863</gml:pos>
							<gml:pos>359006.947 5726389.0 28.878</gml:pos>
							<gml:pos>359007.0 5726389.014 28.88</gml:pos>
							<gml:pos>359007.02 5726389.02 28.881</gml:pos>
							<gml:pos>359008.0 5726389.284 28.864</gml:pos>
							<gml:pos>359008.389 5726389.389 28.791</gml:pos>
							<gml:pos>359009.0 5726389.554 28.743</gml:pos>
							<gml:pos>359009.759 5726389.759 28.714</gml:pos>
							<gml:pos>359010.0 5726389.824 28.707</gml:pos>
							<gml:pos>359010.652 5726390.0 28.726</gml:pos>
							<gml:pos>359011.0 5726390.094 28.73</gml:pos>
							<gml:pos>359011.129 5726390.129 28.721</gml:pos>
							<gml:pos>359011.167 5726390.139 28.715</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>359011.167 5726390.139 28.715</gml:pos>
							<gml:pos>359011.205 5726390.0 28.709</gml:pos>
							<gml:pos>359011.374 5726389.374 28.621</gml:pos>
							<gml:pos>359011.475 5726389.0 28.621</gml:pos>
							<gml:pos>359011.587 5726388.587 28.617</gml:pos>
							<gml:pos>359011.745 5726388.0 28.603</gml:pos>
							<gml:pos>359011.8 5726387.8 28.602</gml:pos>
							<gml:pos>359012.0 5726387.058 28.59</gml:pos>
							<gml:pos>359012.012 5726387.012 28.59</gml:pos>
							<gml:pos>359012.016 5726387.0 28.59</gml:pos>
							<gml:pos>359012.174 5726386.415 28.598</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>359012.174 5726386.415 28.598</gml:pos>
							<gml:pos>359012.503 5726386.503 28.59</gml:pos>
							<gml:pos>359013.0 5726386.636 28.574</gml:pos>
							<gml:pos>359013.63 5726386.804 28.566</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>359013.63 5726386.804 28.566</gml:pos>
							<gml:pos>359013.667 5726386.667 28.567</gml:pos>
							<gml:pos>359013.848 5726386.0 28.555</gml:pos>
							<gml:pos>359013.88 5726385.88 28.554</gml:pos>
							<gml:pos>359014.0 5726385.438 28.544</gml:pos>
							<gml:pos>359014.093 5726385.093 28.538</gml:pos>
							<gml:pos>359014.118 5726385.0 28.538</gml:pos>
							<gml:pos>359014.306 5726384.306 28.527</gml:pos>
							<gml:pos>359014.389 5726384.0 28.526</gml:pos>
							<gml:pos>359014.519 5726383.519 28.53</gml:pos>
							<gml:pos>359014.66 5726383.0 28.533</gml:pos>
							<gml:pos>359014.715 5726382.798 28.535</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>359014.498 5726382.739 28.538</gml:pos>
							<gml:pos>359014.715 5726382.798 28.535</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>359013.249 5726382.408 28.576</gml:pos>
							<gml:pos>359013.465 5726382.465 28.567</gml:pos>
							<gml:pos>359014.0 5726382.607 28.544</gml:pos>
							<gml:pos>359014.498 5726382.739 28.538</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>359013.249 5726382.408 28.576</gml:pos>
							<gml:pos>359013.25 5726382.404 28.576</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>359013.25 5726382.404 28.576</gml:pos>
							<gml:pos>359013.25 5726382.403 28.576</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>359005.787 5726380.389 28.92</gml:pos>
							<gml:pos>359006.0 5726380.446 28.916</gml:pos>
							<gml:pos>359006.611 5726380.611 28.896</gml:pos>
							<gml:pos>359007.0 5726380.716 28.883</gml:pos>
							<gml:pos>359007.981 5726380.981 28.841</gml:pos>
							<gml:pos>359008.0 5726380.986 28.84</gml:pos>
							<gml:pos>359008.051 5726381.0 28.838</gml:pos>
							<gml:pos>359009.0 5726381.256 28.8</gml:pos>
							<gml:pos>359009.351 5726381.351 28.768</gml:pos>
							<gml:pos>359010.0 5726381.526 28.71</gml:pos>
							<gml:pos>359010.72 5726381.72 28.66</gml:pos>
							<gml:pos>359011.0 5726381.796 28.64</gml:pos>
							<gml:pos>359011.758 5726382.0 28.632</gml:pos>
							<gml:pos>359012.0 5726382.065 28.63</gml:pos>
							<gml:pos>359012.09 5726382.09 28.626</gml:pos>
							<gml:pos>359013.0 5726382.335 28.587</gml:pos>
							<gml:pos>359013.25 5726382.403 28.576</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>359000.843 5726379.055 28.794</gml:pos>
							<gml:pos>359001.0 5726379.097 28.783</gml:pos>
							<gml:pos>359001.133 5726379.133 28.825</gml:pos>
							<gml:pos>359002.0 5726379.367 28.904</gml:pos>
							<gml:pos>359002.503 5726379.503 28.98</gml:pos>
							<gml:pos>359003.0 5726379.637 29.008</gml:pos>
							<gml:pos>359003.872 5726379.872 28.979</gml:pos>
							<gml:pos>359004.0 5726379.907 28.973</gml:pos>
							<gml:pos>359004.345 5726380.0 28.96</gml:pos>
							<gml:pos>359005.0 5726380.177 28.938</gml:pos>
							<gml:pos>359005.242 5726380.242 28.933</gml:pos>
							<gml:pos>359005.787 5726380.389 28.92</gml:pos>
						</gml:LineString>
					</gml:curveMember>
				</gml:MultiCurve>
			</bldg:lod2TerrainIntersection>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_2e785b85-8078-492e-903c-eda7e66431f1">
					<core:creationDate>2018-02-02</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo77a24c78-c5ec-4a58-99e0-46f383ec3557">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo851463">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID851463">
											<gml:posList srsDimension="3">359000.843000 5726379.055000 32.658000 358999.797000 5726382.922000 35.177000 358999.797000 5726382.922000 27.945000 359000.843000 5726379.055000 27.945000 359000.843000 5726379.055000 32.658000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_7c370555-818f-4cb4-8bc0-5f555cec3db9">
					<core:creationDate>2018-02-02</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo14f644e7-ab6b-4d97-8e50-816c1a6eca11">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo851464">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID851464">
											<gml:posList srsDimension="3">358999.797000 5726382.922000 35.177000 358998.751000 5726386.788000 32.658000 358998.751000 5726386.788000 27.945000 358999.797000 5726382.922000 27.945000 358999.797000 5726382.922000 35.177000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_551b4f7f-899f-4fba-b2ee-fb29be2f90e3">
					<core:creationDate>2018-02-02</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo0745c559-8eda-496f-a2a9-18cb9b453874">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo851465">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID851465">
											<gml:posList srsDimension="3">358998.751000 5726386.788000 32.658000 359011.167000 5726390.139000 32.658000 359011.167000 5726390.139000 27.945000 358998.751000 5726386.788000 27.945000 358998.751000 5726386.788000 32.658000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_77c40bf6-00dd-47b9-bcf1-31b36a1937b0">
					<core:creationDate>2018-02-02</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo51e51340-2d5c-4a5e-a28c-6c2f5a6b7a5d">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo851466">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID851466">
											<gml:posList srsDimension="3">359011.167000 5726390.139000 32.658000 359012.174000 5726386.415000 32.656000 359012.174000 5726386.415000 27.945000 359011.167000 5726390.139000 27.945000 359011.167000 5726390.139000 32.658000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_1544b722-91b4-4f09-926b-78d327509bc2">
					<core:creationDate>2018-02-02</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo3a0ebea0-4875-4a7d-878d-ffc1691e2f0f">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo851467">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID851467">
											<gml:posList srsDimension="3">359012.174000 5726386.415000 32.656000 359013.630000 5726386.804000 31.674000 359013.630000 5726386.804000 27.945000 359012.174000 5726386.415000 27.945000 359012.174000 5726386.415000 32.656000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_f6836790-ff8d-4114-991d-7fe6601e5ecc">
					<core:creationDate>2018-02-02</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo16ce1dd7-ecf9-412c-9614-3e7cf4db3b5e">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo851468">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID851468">
											<gml:posList srsDimension="3">359013.630000 5726386.804000 31.674000 359014.715000 5726382.798000 31.671000 359014.715000 5726382.798000 27.945000 359013.630000 5726386.804000 27.945000 359013.630000 5726386.804000 31.674000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_e04efc98-1134-4c41-86af-e26cf19cd16e">
					<core:creationDate>2018-02-02</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo240b7555-4103-4c37-bfd2-b1aaf113a0a0">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo851469">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID851469">
											<gml:posList srsDimension="3">359014.715000 5726382.798000 31.671000 359014.498000 5726382.739000 31.817000 359014.498000 5726382.739000 27.945000 359014.715000 5726382.798000 27.945000 359014.715000 5726382.798000 31.671000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_ef63eabd-358e-49af-8dbe-4b50dab120fd">
					<core:creationDate>2018-02-02</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo12335558-c88c-42a8-919d-48baab066ddf">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo851470">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID851470">
											<gml:posList srsDimension="3">359014.498000 5726382.739000 31.817000 359013.249000 5726382.408000 32.659000 359013.249000 5726382.408000 27.945000 359014.498000 5726382.739000 27.945000 359014.498000 5726382.739000 31.817000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_e65367cd-49e5-407f-b651-0a14f688d855">
					<core:creationDate>2018-02-02</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo1ffb5791-0afc-4fcf-83d3-deb3936d265a">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo851471">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID851471">
											<gml:posList srsDimension="3">359013.249000 5726382.408000 32.659000 359013.250000 5726382.404000 32.659000 359013.250000 5726382.404000 27.945000 359013.249000 5726382.408000 27.945000 359013.249000 5726382.408000 32.659000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_f8de8d23-908e-4d90-a683-1ca8782544b3">
					<core:creationDate>2018-02-02</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo0ee46416-ce8d-4054-932f-3ca5826bdce6">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo851472">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID851472">
											<gml:posList srsDimension="3">359013.250000 5726382.404000 32.659000 359013.250000 5726382.403000 32.658000 359013.250000 5726382.403000 27.945000 359013.250000 5726382.404000 27.945000 359013.250000 5726382.404000 32.659000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_805792ef-94dc-4e03-ac40-17a03a29adcd">
					<core:creationDate>2018-02-02</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo0929c38d-6220-42bd-8a89-42c7e92c9079">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo851473">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID851473">
											<gml:posList srsDimension="3">359005.787000 5726380.389000 32.658000 359000.843000 5726379.055000 32.658000 359000.843000 5726379.055000 27.945000 359005.787000 5726380.389000 27.945000 359005.787000 5726380.389000 32.658000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_cad3dff1-c8d6-4325-9db6-d1c617e29a8e">
					<core:creationDate>2018-02-02</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo4bacb752-f7b0-4174-b00c-2cadd5292092">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo851474">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID851474">
											<gml:posList srsDimension="3">359008.476000 5726385.264000 35.177000 359013.250000 5726382.404000 32.659000 359013.249000 5726382.408000 32.659000 359014.498000 5726382.739000 31.817000 359014.715000 5726382.798000 31.671000 359013.630000 5726386.804000 31.674000 359012.174000 5726386.415000 32.656000 359011.167000 5726390.139000 32.658000 359008.476000 5726385.264000 35.177000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_00da7be3-608e-4d3e-baaf-b7a9bb33f027">
					<core:creationDate>2018-02-02</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo0885b79f-8d82-46c5-83fd-6c0d874be6a3">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo851475">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID851475">
											<gml:posList srsDimension="3">359008.476000 5726385.264000 35.177000 359011.167000 5726390.139000 32.658000 358998.751000 5726386.788000 32.658000 358999.797000 5726382.922000 35.177000 359008.476000 5726385.264000 35.177000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="UUID_a706cddc-aba3-4faf-8c43-75753791cabf">
					<core:creationDate>2018-02-02</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo4e70201c-4b5c-4171-a6ff-a97b79cf32e3">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo851476">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID851476">
											<gml:posList srsDimension="3">359000.843000 5726379.055000 27.945000 358999.797000 5726382.922000 27.945000 358998.751000 5726386.788000 27.945000 359011.167000 5726390.139000 27.945000 359012.174000 5726386.415000 27.945000 359013.630000 5726386.804000 27.945000 359014.715000 5726382.798000 27.945000 359014.498000 5726382.739000 27.945000 359013.249000 5726382.408000 27.945000 359013.250000 5726382.404000 27.945000 359013.250000 5726382.403000 27.945000 359005.787000 5726380.389000 27.945000 359000.843000 5726379.055000 27.945000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:ClosureSurface>
					<core:creationDate>2018-02-02</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeoe46b3a40-1066-4454-80cc-13313faff089">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo851477">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID851477">
											<gml:posList srsDimension="3">359013.250000 5726382.403000 32.658000 359005.787000 5726380.389000 32.658000 359005.787000 5726380.389000 27.945000 359013.250000 5726382.403000 27.945000 359013.250000 5726382.403000 32.658000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:ClosureSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_f6a67705-fabe-425f-8d58-e5a2e2f0b090">
					<core:creationDate>2018-02-02</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeodad4a547-5325-457e-8d0b-3b379384269a">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo851478">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID851478">
											<gml:posList srsDimension="3">359008.476000 5726385.264000 35.177000 358999.797000 5726382.922000 35.177000 359000.843000 5726379.055000 32.658000 359005.787000 5726380.389000 32.658000 359008.476000 5726385.264000 35.177000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo851480">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID851480">
											<gml:posList srsDimension="3">359013.250000 5726382.403000 32.658000 359013.250000 5726382.404000 32.659000 359008.476000 5726385.264000 35.177000 359013.250000 5726382.403000 32.658000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:ClosureSurface>
					<core:creationDate>2018-02-02</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo3cbc2278-cc2d-460d-a098-15610d7b32b2">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo851481">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID851481">
											<gml:posList srsDimension="3">359008.476000 5726385.264000 35.177000 359005.787000 5726380.389000 32.658000 359013.250000 5726382.403000 32.658000 359008.476000 5726385.264000 35.177000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:ClosureSurface>
			</bldg:boundedBy>
		</bldg:BuildingPart>
	</bldg:consistsOfBuildingPart>
	<bldg:consistsOfBuildingPart>
		<bldg:BuildingPart gml:id="GUID_1479134954866_42991539">
			<core:creationDate>2018-02-02</core:creationDate>
			<gen:stringAttribute name="DatenquelleDachhoehe">
				<gen:value>1000</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="DatenquelleLage">
				<gen:value>1000</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="DatenquelleBodenhoehe">
				<gen:value>1100</gen:value>
			</gen:stringAttribute>
			<bldg:measuredHeight uom="urn:adv:uom:m">7.232</bldg:measuredHeight>
			<bldg:lod2Solid>
				<gml:Solid gml:id="IDGeobeb2a7fc-2cda-4063-b581-7b627a7a12ec">
					<gml:exterior>
						<gml:CompositeSurface>
							<gml:surfaceMember xlink:href="#PolyIDGeo851493"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo851488"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo851490"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo851494"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo851489"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo851498"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo851495"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo851496"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo851497"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo851492"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo851491"/>
						</gml:CompositeSurface>
					</gml:exterior>
				</gml:Solid>
			</bldg:lod2Solid>
			<bldg:lod2TerrainIntersection>
				<gml:MultiCurve>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>359007.062 5726375.663 29.124</gml:pos>
							<gml:pos>359007.886 5726375.886 29.117</gml:pos>
							<gml:pos>359008.0 5726375.917 29.117</gml:pos>
							<gml:pos>359008.245 5726375.983 29.102</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>359005.787 5726380.389 28.92</gml:pos>
							<gml:pos>359005.892 5726380.0 28.922</gml:pos>
							<gml:pos>359005.915 5726379.915 28.925</gml:pos>
							<gml:pos>359006.0 5726379.599 28.932</gml:pos>
							<gml:pos>359006.127 5726379.127 28.942</gml:pos>
							<gml:pos>359006.162 5726379.0 28.942</gml:pos>
							<gml:pos>359006.34 5726378.34 28.946</gml:pos>
							<gml:pos>359006.432 5726378.0 28.948</gml:pos>
							<gml:pos>359006.552 5726377.552 28.969</gml:pos>
							<gml:pos>359006.701 5726377.0 29.016</gml:pos>
							<gml:pos>359006.765 5726376.765 29.034</gml:pos>
							<gml:pos>359006.971 5726376.0 29.1</gml:pos>
							<gml:pos>359006.977 5726375.977 29.102</gml:pos>
							<gml:pos>359007.0 5726375.893 29.108</gml:pos>
							<gml:pos>359007.062 5726375.663 29.124</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>359005.787 5726380.389 28.92</gml:pos>
							<gml:pos>359006.0 5726380.446 28.916</gml:pos>
							<gml:pos>359006.611 5726380.611 28.896</gml:pos>
							<gml:pos>359007.0 5726380.716 28.883</gml:pos>
							<gml:pos>359007.981 5726380.981 28.841</gml:pos>
							<gml:pos>359008.0 5726380.986 28.84</gml:pos>
							<gml:pos>359008.051 5726381.0 28.838</gml:pos>
							<gml:pos>359009.0 5726381.256 28.8</gml:pos>
							<gml:pos>359009.351 5726381.351 28.768</gml:pos>
							<gml:pos>359010.0 5726381.526 28.71</gml:pos>
							<gml:pos>359010.72 5726381.72 28.66</gml:pos>
							<gml:pos>359011.0 5726381.796 28.64</gml:pos>
							<gml:pos>359011.758 5726382.0 28.632</gml:pos>
							<gml:pos>359012.0 5726382.065 28.63</gml:pos>
							<gml:pos>359012.09 5726382.09 28.626</gml:pos>
							<gml:pos>359013.0 5726382.335 28.587</gml:pos>
							<gml:pos>359013.25 5726382.403 28.576</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>359013.25 5726382.403 28.576</gml:pos>
							<gml:pos>359013.283 5726382.283 28.576</gml:pos>
							<gml:pos>359013.359 5726382.0 28.576</gml:pos>
							<gml:pos>359013.495 5726381.495 28.575</gml:pos>
							<gml:pos>359013.629 5726381.0 28.569</gml:pos>
							<gml:pos>359013.708 5726380.708 28.562</gml:pos>
							<gml:pos>359013.899 5726380.0 28.572</gml:pos>
							<gml:pos>359013.92 5726379.92 28.569</gml:pos>
							<gml:pos>359014.0 5726379.624 28.566</gml:pos>
							<gml:pos>359014.133 5726379.133 28.557</gml:pos>
							<gml:pos>359014.168 5726379.0 28.56</gml:pos>
							<gml:pos>359014.345 5726378.345 28.553</gml:pos>
							<gml:pos>359014.438 5726378.0 28.55</gml:pos>
							<gml:pos>359014.524 5726377.682 28.54</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>359010.793 5726376.673 28.558</gml:pos>
							<gml:pos>359011.0 5726376.729 28.555</gml:pos>
							<gml:pos>359011.999 5726376.999 28.59</gml:pos>
							<gml:pos>359012.0 5726376.999 28.59</gml:pos>
							<gml:pos>359012.003 5726377.0 28.59</gml:pos>
							<gml:pos>359013.0 5726377.27 28.555</gml:pos>
							<gml:pos>359013.37 5726377.37 28.55</gml:pos>
							<gml:pos>359014.0 5726377.54 28.536</gml:pos>
							<gml:pos>359014.524 5726377.682 28.54</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>359008.245 5726375.983 29.102</gml:pos>
							<gml:pos>359008.308 5726376.0 29.098</gml:pos>
							<gml:pos>359009.0 5726376.187 29.033</gml:pos>
							<gml:pos>359009.257 5726376.257 28.949</gml:pos>
							<gml:pos>359010.0 5726376.458 28.578</gml:pos>
							<gml:pos>359010.628 5726376.628 28.56</gml:pos>
							<gml:pos>359010.793 5726376.673 28.558</gml:pos>
						</gml:LineString>
					</gml:curveMember>
				</gml:MultiCurve>
			</bldg:lod2TerrainIntersection>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_e9d7bd29-84ab-4e63-be96-e6fdf21cc691">
					<core:creationDate>2018-02-02</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeob1f847c2-b73b-46ac-9d16-5e6f6b9a6793">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo851488">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID851488">
											<gml:posList srsDimension="3">359008.245000 5726375.983000 33.457000 359007.062000 5726375.663000 32.658000 359007.062000 5726375.663000 27.945000 359008.245000 5726375.983000 27.945000 359008.245000 5726375.983000 33.457000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_0593bdd7-8739-40e6-9208-258b0c29fc39">
					<core:creationDate>2018-02-02</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeoe0a8b8db-f54b-452a-bb7a-db0d4f9518ed">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo851489">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID851489">
											<gml:posList srsDimension="3">359007.062000 5726375.663000 32.658000 359005.787000 5726380.389000 32.658000 359005.787000 5726380.389000 27.945000 359007.062000 5726375.663000 27.945000 359007.062000 5726375.663000 32.658000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_4d8e9a87-75e1-4953-a42e-142b69a2b0e7">
					<core:creationDate>2018-02-02</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeoe3000289-91af-46da-961e-0542e926c42e">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo851490">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID851490">
											<gml:posList srsDimension="3">359013.250000 5726382.403000 32.659000 359014.524000 5726377.682000 32.658000 359014.524000 5726377.682000 27.945000 359013.250000 5726382.403000 27.945000 359013.250000 5726382.403000 32.659000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_2badb616-21b8-4e26-b765-d6cf8d6a0aa5">
					<core:creationDate>2018-02-02</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeofada1a5e-803f-432e-8cac-f4e2029f513f">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo851491">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID851491">
											<gml:posList srsDimension="3">359014.524000 5726377.682000 32.658000 359010.793000 5726376.673000 35.177000 359010.793000 5726376.673000 27.945000 359014.524000 5726377.682000 27.945000 359014.524000 5726377.682000 32.658000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_174c101e-efca-44ae-add6-ac371dbaf800">
					<core:creationDate>2018-02-02</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo13260808-0276-4ca7-94f5-e96aa549932a">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo851492">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID851492">
											<gml:posList srsDimension="3">359010.793000 5726376.673000 35.177000 359008.245000 5726375.983000 33.457000 359008.245000 5726375.983000 27.945000 359010.793000 5726376.673000 27.945000 359010.793000 5726376.673000 35.177000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_4c68632e-b460-4733-894c-c3e46f1dd9e3">
					<core:creationDate>2018-02-02</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeoe333ef07-916b-4d3b-953f-7d21e3f8e3e7">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo851493">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID851493">
											<gml:posList srsDimension="3">359008.476000 5726385.264000 35.177000 359010.793000 5726376.673000 35.177000 359014.524000 5726377.682000 32.658000 359013.250000 5726382.403000 32.659000 359008.476000 5726385.264000 35.177000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_eb46717e-88eb-4d23-b7c8-19a450660ba9">
					<core:creationDate>2018-02-02</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo496a02a2-5e1a-48e4-ae83-6a0a239acf83">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo851494">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID851494">
											<gml:posList srsDimension="3">359008.476000 5726385.264000 35.177000 359005.787000 5726380.389000 32.658000 359007.062000 5726375.663000 32.658000 359008.245000 5726375.983000 33.457000 359010.793000 5726376.673000 35.177000 359008.476000 5726385.264000 35.177000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="UUID_f3acbe31-e3f5-4557-8e26-b38dc5d0d80d">
					<core:creationDate>2018-02-02</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo1e612d52-bdbe-421d-9812-236f4dd41305">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo851495">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID851495">
											<gml:posList srsDimension="3">359008.245000 5726375.983000 27.945000 359007.062000 5726375.663000 27.945000 359005.787000 5726380.389000 27.945000 359013.250000 5726382.403000 27.945000 359014.524000 5726377.682000 27.945000 359010.793000 5726376.673000 27.945000 359008.245000 5726375.983000 27.945000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_3eb3884f-d745-4f11-af06-e72df2ead84d">
					<core:creationDate>2018-02-02</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo0d62ef6d-9dcf-4a7f-b5f9-20c9322b5d32">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo851496">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID851496">
											<gml:posList srsDimension="3">359005.787000 5726380.389000 32.658000 359013.250000 5726382.403000 32.659000 359013.250000 5726382.403000 29.124000 359013.250000 5726382.403000 32.658000 359005.787000 5726380.389000 32.658000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:ClosureSurface>
					<core:creationDate>2018-02-02</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeob1e7a080-56b3-48cb-823e-7b345d024c9c">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo851497">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID851497">
											<gml:posList srsDimension="3">359013.250000 5726382.403000 29.124000 359013.250000 5726382.403000 27.945000 359006.616000 5726380.613000 27.945000 359005.787000 5726380.389000 27.945000 359005.787000 5726380.389000 32.658000 359013.250000 5726382.403000 32.658000 359013.250000 5726382.403000 29.124000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:ClosureSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:ClosureSurface>
					<core:creationDate>2018-02-02</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo7f6654a0-41f2-46d0-bfb8-ab8ea1e71105">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo851498">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID851498">
											<gml:posList srsDimension="3">359008.476000 5726385.264000 35.177000 359013.250000 5726382.403000 32.659000 359005.787000 5726380.389000 32.658000 359008.476000 5726385.264000 35.177000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:ClosureSurface>
			</bldg:boundedBy>
		</bldg:BuildingPart>
	</bldg:consistsOfBuildingPart>
	<bldg:address>
		<core:Address>
			<core:xalAddress>
				<xAL:AddressDetails>
					<xAL:Country>
						<xAL:CountryName>
							Germany
						</xAL:CountryName>
						<xAL:Locality  Type="Town">
							<xAL:LocalityName>
								Dorsten
							</xAL:LocalityName>
							<xAL:Thoroughfare  Type="Street">
								<xAL:ThoroughfareNumber>
									4
								</xAL:ThoroughfareNumber>
								<xAL:ThoroughfareName>
									Parkweg
								</xAL:ThoroughfareName>
							</xAL:Thoroughfare>
						</xAL:Locality>
					</xAL:Country>
				</xAL:AddressDetails>
			</core:xalAddress>
		</core:Address>
	</bldg:address>
</bldg:Building>
</core:cityObjectMember>
</core:CityModel>
