<?xml version="1.0" encoding="UTF-8"?>
<!--SGJ3D Exporter 3.3.8 (http://www.cpa-redev.de) -->
<core:CityModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
           xmlns:core="http://www.opengis.net/citygml/2.0"
           xmlns:gml="http://www.opengis.net/gml"
           xmlns:xlink="http://www.w3.org/1999/xlink"
           xmlns:tran="http://www.opengis.net/citygml/transportation/2.0"
           xmlns:wtr="http://www.opengis.net/citygml/waterbody/2.0"
           xmlns:smil20lang="http://www.w3.org/2001/SMIL20/Language"
           xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/2.0"
           xmlns:luse="http://www.opengis.net/citygml/landuse/2.0"
           xmlns:frn="http://www.opengis.net/citygml/cityfurniture/2.0"
           xmlns:app="http://www.opengis.net/citygml/appearance/2.0"
           xmlns:tex="http://www.opengis.net/citygml/texturedsurface/2.0"
           xmlns:smil20="http://www.w3.org/2001/SMIL20/"
           xmlns:xAL="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0"
           xmlns:bldg="http://www.opengis.net/citygml/building/2.0"
           xmlns:dem="http://www.opengis.net/citygml/relief/2.0"
           xmlns:veg="http://www.opengis.net/citygml/vegetation/2.0"
           xmlns:brid="http://www.opengis.net/citygml/bridge/2.0"
           xmlns:tun="http://www.opengis.net/citygml/tunnel/2.0"
           xmlns:gen="http://www.opengis.net/citygml/generics/2.0"
           xmlns:wfs="http://www.opengis.net/wfs"
           xsi:schemaLocation="http://www.opengis.net/citygml/landuse/2.0 http://schemas.opengis.net/citygml/landuse/2.0/landUse.xsd http://www.opengis.net/citygml/cityfurniture/2.0 http://schemas.opengis.net/citygml/cityfurniture/2.0/cityFurniture.xsd http://www.opengis.net/citygml/appearance/2.0 http://schemas.opengis.net/citygml/appearance/2.0/appearance.xsd http://www.opengis.net/citygml/texturedsurface/2.0 http://schemas.opengis.net/citygml/texturedsurface/2.0/texturedSurface.xsd http://www.opengis.net/citygml/transportation/2.0 http://schemas.opengis.net/citygml/transportation/2.0/transportation.xsd http://www.opengis.net/citygml/waterbody/2.0 http://schemas.opengis.net/citygml/waterbody/2.0/waterBody.xsd http://www.opengis.net/citygml/building/2.0 http://schemas.opengis.net/citygml/building/2.0/building.xsd http://www.opengis.net/citygml/relief/2.0 http://schemas.opengis.net/citygml/relief/2.0/relief.xsd http://www.opengis.net/citygml/vegetation/2.0 http://schemas.opengis.net/citygml/vegetation/2.0/vegetation.xsd http://www.opengis.net/citygml/cityobjectgroup/2.0 http://schemas.opengis.net/citygml/cityobjectgroup/2.0/cityObjectGroup.xsd http://www.opengis.net/citygml/bridge/2.0 http://schemas.opengis.net/citygml/bridge/2.0/bridge.xsd http://www.opengis.net/citygml/tunnel/2.0 http://schemas.opengis.net/citygml/tunnel/2.0/tunnel.xsd http://www.opengis.net/citygml/generics/2.0 http://schemas.opengis.net/citygml/generics/2.0/generics.xsd">
<gml:description>D:\Programme\Apache Software Foundation\Tomcat 8.5\temp\export2843413203417119045\export.gml</gml:description>
<gml:name>D:\Programme\Apache Software Foundation\Tomcat 8.5\temp\export2843413203417119045\export.gml</gml:name>
<gml:boundedBy>
	<gml:Envelope srsDimension="3" srsName="urn:ogc:def:crs:EPSG::25832">
		<gml:lowerCorner>388879.97 5720402.57 0.00</gml:lowerCorner>
		<gml:upperCorner>388937.35 5720436.08 0.00</gml:upperCorner>
	</gml:Envelope>
</gml:boundedBy>
<core:cityObjectMember>
<bldg:Building gml:id="DENW_0c99f568-78ae-4546-bd33-5bf75ec9618f">
	<gml:name>St.Petrus-Kirche</gml:name>
	<core:creationDate>2017-08-30</core:creationDate>
	<core:externalReference>
		<core:informationSystem>http://www.adv-online.de/fdv/art.htm#_9100</core:informationSystem>
		<core:externalObject>
			<core:name>DENW52AL00bvcpdE</core:name>
		</core:externalObject>
	</core:externalReference>
	<gen:stringAttribute name="DatenquelleDachhoehe">
		<gen:value>1000</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="DatenquelleLage">
		<gen:value>1000</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="DatenquelleBodenhoehe">
		<gen:value>1100</gen:value>
	</gen:stringAttribute>
	<gen:stringAttribute name="Gemeindeschluessel">
		<gen:value>05562036</gen:value>
	</gen:stringAttribute>
	<bldg:measuredHeight uom="urn:ogc:def:uom:UCUM::m">43.51</bldg:measuredHeight>
	<bldg:lod2Solid>
		<gml:Solid gml:id="IDGeo3dc5df42-a950-4394-a5a9-d0f4bbd2ec48">
			<gml:exterior>
				<gml:CompositeSurface>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600442">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600442">
									<gml:posList srsDimension="3">388907.500000 5720434.471000 66.415000 388907.682979 5720434.310000 66.415000 388907.357893 5720434.310000 66.415000 388907.500000 5720434.471000 66.415000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600443"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600444"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600445"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600446"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600447"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600448"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600449"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600450"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600451"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600452"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600453"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600454"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600455"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600456"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600457"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600458"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600459"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600460"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600461"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600462"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600463"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600464"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600465"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600466"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600467"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600468"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600469"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600470"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600471"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600472"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600473"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600474"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600475"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600476"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600477"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600478"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600479"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600480"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600481"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600482"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600483"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600484"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600485"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600486"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600487"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600488"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600489"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600490"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600491"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600492"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600493"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600494"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600495"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600496"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600497"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600498"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600499"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600500"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600501"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600502"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600503"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600504"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600505"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600506"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600507"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600508"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600509"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600510"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600511"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600512"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600513"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600514"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600515"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600516"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600517"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600518"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600519"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600520"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600521"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600522"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600523"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600524"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600525"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600526"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600527"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600528"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600529"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600530"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600531"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600532"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600533"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600534"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600535"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600536"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600537"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600538"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600539"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600540"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600541"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600542"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600543"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600544"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600545"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600546"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600547"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600548"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600549"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600550"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600551"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600552"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600553"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600554"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600555"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600556"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600557"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600558"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600559"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600560"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600561"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600562"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600563"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600564"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600565"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600566"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600567"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600568"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600569"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600570"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600571"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600572"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600573"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600574"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600575"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600576"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600577"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600578"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600579"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600580"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600581"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600582"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600583"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600584"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600585"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600586"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600587"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600588"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600589"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600590"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600591"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600592"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600593"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600594"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600595"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600596"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600597"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600598"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600599"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600600"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600601"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600602"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600603"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600604"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600605"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600606"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600607"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600608"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600609"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600610"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600611"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600612"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600613"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600614"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600615"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600616"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600617"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600618"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600619"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600620"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600621"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600622"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600623"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600624"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600625"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600626"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600627"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600628"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600629"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600630"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600631"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600632"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600633"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600634"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600635"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600636"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600637"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600638"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600639"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600640"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600641"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600642"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600643"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600644"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600645"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600646"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600647"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600648"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600649"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600650"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600651"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600652"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600653"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600654"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600655"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600656"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600657"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600658"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600659"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600660"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600661"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600662"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600663"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600664"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600665"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600666"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600667"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600668"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600669"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600670"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600671"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600672"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600673"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600674"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600675"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600676"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600677"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600678"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600679"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600680"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600681"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600682"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600683"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600684"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600685"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600686"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600687"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600688"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600689"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600690"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600691"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600692"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600693"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600694"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600695"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600696"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600697"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600698"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600699"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600700"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600701"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600702"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600703"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600704"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600705"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600706"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600707"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600708"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600709"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600710"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600711"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600712"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600713"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600714"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600715"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600716"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600717"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600718"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600719"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600720"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600721"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600722"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600723"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600724"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600725"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600726"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600727"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600728"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600729"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600730"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600731"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600732"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600733"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600734"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600735"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600736"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600737"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600738"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600739"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600740"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600741"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600742"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600743"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600744"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600745"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600746"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600747"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600748"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600749"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600750"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600751"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600752"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600753"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600754"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600755"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600756"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600757"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600758"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600759"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600760"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600761"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600762"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600763"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600764"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600765"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600766"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600767"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600768"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600769"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600770"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600771"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600772"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600773"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600774"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600775"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600776"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600777"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600778"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600779"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600780"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600781"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600782"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600783"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600784"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600785"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600786"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600787"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600788"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600789"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600790"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600791"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600792"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600793"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600794"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600795"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600796"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600797"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600798"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600799"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600800"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600801"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600802"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600803"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600804"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600805"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600806"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600807"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600808"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600809"/>
					<gml:surfaceMember xlink:href="#PolyIDGeo992600810"/>
				</gml:CompositeSurface>
			</gml:exterior>
		</gml:Solid>
	</bldg:lod2Solid>
	<bldg:outerBuildingInstallation>
		<bldg:BuildingInstallation gml:id="UUID_BuildingInstallation_1114_470551_145149">
			<bldg:lod2Geometry>
				<gml:CompositeSurface>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600811">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600811">
									<gml:posList srsDimension="3">388915.067664 5720420.420627 93.890999 388914.877685 5720420.613309 94.090999 388914.687708 5720420.805991 93.890999 388914.687706 5720420.805993 92.111000 388914.497728 5720420.998675 92.111000 388914.497729 5720420.998673 94.390999 388915.257642 5720420.227946 94.390999 388915.257640 5720420.227948 92.111000 388915.067662 5720420.420630 92.111000 388915.067664 5720420.420627 93.890999 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600812">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600812">
									<gml:posList srsDimension="3">388913.060899 5720420.440101 94.091002 388912.916861 5720420.216988 93.890999 388912.916860 5720420.216990 92.111002 388912.772822 5720419.993877 92.111000 388912.772824 5720419.993875 94.390999 388913.348975 5720420.886326 94.391005 388913.348973 5720420.886328 92.111006 388913.204936 5720420.663215 92.111005 388913.204937 5720420.663213 93.890999 388913.060899 5720420.440101 94.091002 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600813">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600813">
									<gml:posList srsDimension="3">388912.817053 5720419.548619 94.091002 388912.839168 5720419.325991 93.890999 388912.839166 5720419.325993 92.111000 388912.861281 5720419.103365 92.111000 388912.861283 5720419.103362 94.390999 388912.772824 5720419.993875 94.390999 388912.772822 5720419.993877 92.111000 388912.794937 5720419.771249 92.111000 388912.794938 5720419.771247 93.890999 388912.817053 5720419.548619 94.091002 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600814">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600814">
									<gml:posList srsDimension="3">388913.567541 5720418.340682 94.390999 388913.666636 5720418.623843 94.390999 388913.132864 5720419.230812 94.390999 388912.861283 5720419.103362 94.390999 388913.567541 5720418.340682 94.390999 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600815">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600815">
									<gml:posList srsDimension="3">388912.861283 5720419.103362 94.390999 388913.132864 5720419.230812 94.390999 388913.064128 5720419.922171 94.390999 388912.772824 5720419.993875 94.390999 388912.861283 5720419.103362 94.390999 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600816">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600816">
									<gml:posList srsDimension="3">388912.772824 5720419.993875 94.390999 388913.064128 5720419.922171 94.390999 388913.499862 5720420.627032 94.391004 388913.348975 5720420.886326 94.391005 388912.772824 5720419.993875 94.390999 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600817">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600817">
									<gml:posList srsDimension="3">388915.257642 5720420.227946 94.390999 388914.986060 5720420.100496 94.390999 388915.073138 5720419.427658 94.390999 388915.364443 5720419.355954 94.390999 388915.257642 5720420.227946 94.390999 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600818">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600818">
									<gml:posList srsDimension="3">388914.497729 5720420.998673 94.390999 388914.398635 5720420.715512 94.390999 388914.986060 5720420.100496 94.390999 388915.257642 5720420.227946 94.390999 388914.497729 5720420.998673 94.390999 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600819">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600819">
									<gml:posList srsDimension="3">388915.364443 5720419.355954 94.390999 388915.073138 5720419.427658 94.390999 388914.611443 5720418.716825 94.390999 388914.762330 5720418.457531 94.390999 388915.364443 5720419.355954 94.390999 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600820">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600820">
									<gml:posList srsDimension="3">388913.567541 5720418.340682 94.390999 388912.861283 5720419.103362 94.390999 388912.861281 5720419.103365 92.111000 388913.037846 5720418.912695 92.111000 388913.037847 5720418.912692 93.890999 388913.214412 5720418.722023 94.091002 388913.390977 5720418.531352 93.890999 388913.390975 5720418.531354 92.111000 388913.567540 5720418.340684 92.111000 388913.567541 5720418.340682 94.390999 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600821">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600821">
									<gml:posList srsDimension="3">388913.923352 5720420.942500 94.091002 388913.636163 5720420.914413 93.890999 388913.636162 5720420.914415 92.111005 388913.348973 5720420.886328 92.111006 388913.348975 5720420.886326 94.391005 388914.497729 5720420.998673 94.390999 388914.497728 5720420.998675 92.111000 388914.210539 5720420.970588 92.111002 388914.210541 5720420.970586 93.890999 388913.923352 5720420.942500 94.091002 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600822">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600822">
									<gml:posList srsDimension="3">388913.348975 5720420.886326 94.391005 388913.499862 5720420.627032 94.391004 388914.398635 5720420.715512 94.390999 388914.497729 5720420.998673 94.390999 388913.348975 5720420.886326 94.391005 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600823">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600823">
									<gml:posList srsDimension="3">388915.337742 5720419.573954 93.890999 388915.311042 5720419.791950 94.090999 388915.284342 5720420.009950 93.890999 388915.284341 5720420.009950 92.111000 388915.257640 5720420.227948 92.111000 388915.257642 5720420.227946 94.390999 388915.364443 5720419.355954 94.390999 388915.364442 5720419.355956 92.111000 388915.337742 5720419.573954 92.111000 388915.337742 5720419.573954 93.890999 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600824">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600824">
									<gml:posList srsDimension="3">388914.463633 5720418.428319 93.890999 388914.463632 5720418.428321 92.111000 388914.762329 5720418.457533 92.111000 388914.762330 5720418.457531 94.390999 388913.567541 5720418.340682 94.390999 388913.567540 5720418.340684 92.111000 388913.866237 5720418.369897 92.111000 388913.866239 5720418.369894 93.890999 388914.164936 5720418.399107 94.091002 388914.463633 5720418.428319 93.890999 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600825">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600825">
									<gml:posList srsDimension="3">388914.762330 5720418.457531 94.390999 388914.611443 5720418.716825 94.390999 388913.666636 5720418.623843 94.390999 388913.567541 5720418.340682 94.390999 388914.762330 5720418.457531 94.390999 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600826">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600826">
									<gml:posList srsDimension="3">388914.064046 5720419.670286 99.221000 388913.132864 5720419.230812 94.390999 388913.666636 5720418.623843 94.390999 388914.064046 5720419.670286 99.221000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600827">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600827">
									<gml:posList srsDimension="3">388914.611443 5720418.716825 94.390999 388914.064046 5720419.670286 99.221000 388913.666636 5720418.623843 94.390999 388914.611443 5720418.716825 94.390999 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600828">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600828">
									<gml:posList srsDimension="3">388915.073138 5720419.427658 94.390999 388914.064046 5720419.670286 99.221000 388914.611443 5720418.716825 94.390999 388915.073138 5720419.427658 94.390999 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600829">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600829">
									<gml:posList srsDimension="3">388914.986060 5720420.100496 94.390999 388914.064046 5720419.670286 99.221000 388915.073138 5720419.427658 94.390999 388914.986060 5720420.100496 94.390999 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600830">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600830">
									<gml:posList srsDimension="3">388914.398635 5720420.715512 94.390999 388914.064046 5720419.670286 99.221000 388914.986060 5720420.100496 94.390999 388914.398635 5720420.715512 94.390999 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600831">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600831">
									<gml:posList srsDimension="3">388913.499862 5720420.627032 94.391004 388914.064046 5720419.670286 99.221000 388914.398635 5720420.715512 94.390999 388913.499862 5720420.627032 94.391004 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600832">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600832">
									<gml:posList srsDimension="3">388914.064046 5720419.670286 99.221000 388913.064128 5720419.922171 94.390999 388913.132864 5720419.230812 94.390999 388914.064046 5720419.670286 99.221000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600833">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600833">
									<gml:posList srsDimension="3">388913.064128 5720419.922171 94.390999 388914.064046 5720419.670286 99.221000 388913.499862 5720420.627032 94.391004 388913.064128 5720419.922171 94.390999 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600834">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600834">
									<gml:posList srsDimension="3">388915.364443 5720419.355954 94.390999 388914.762330 5720418.457531 94.390999 388914.762329 5720418.457533 92.111000 388914.912857 5720418.682139 92.111000 388914.912859 5720418.682137 93.890999 388915.063387 5720418.906743 94.091002 388915.213915 5720419.131348 93.890999 388915.213914 5720419.131350 92.111000 388915.364442 5720419.355956 92.111000 388915.364443 5720419.355954 94.390999 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:CompositeSurface>
			</bldg:lod2Geometry>
		</bldg:BuildingInstallation>
	</bldg:outerBuildingInstallation>
	<bldg:outerBuildingInstallation>
		<bldg:BuildingInstallation gml:id="UUID_BuildingInstallation_440_389670_193387">
			<bldg:lod2Geometry>
				<gml:CompositeSurface>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600835">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600835">
									<gml:posList srsDimension="3">388895.433134 5720426.795325 79.534233 388895.433134 5720426.795325 78.734233 388895.480585 5720426.245687 79.534233 388895.433134 5720426.795325 79.534233 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600836">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600836">
									<gml:posList srsDimension="3">388896.030911 5720426.846932 79.534233 388896.078361 5720426.297293 79.534233 388896.030911 5720426.846932 78.734233 388896.030911 5720426.846932 79.534233 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600837">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600837">
									<gml:posList srsDimension="3">388895.732207 5720426.821144 80.034233 388895.809315 5720425.927982 80.034233 388896.177991 5720426.305894 79.534233 388896.130540 5720426.855533 79.534233 388896.031280 5720426.846964 79.658827 388895.732207 5720426.821144 80.034233 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600838">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600838">
									<gml:posList srsDimension="3">388895.712524 5720427.047502 79.807091 388895.333874 5720426.786756 79.534233 388895.732207 5720426.821144 80.658827 388895.712524 5720427.047502 79.807091 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600839">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600839">
									<gml:posList srsDimension="3">388895.712524 5720427.047502 79.807091 388895.689017 5720427.319276 79.534233 388895.333874 5720426.786756 79.534233 388895.712524 5720427.047502 79.807091 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600840">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600840">
									<gml:posList srsDimension="3">388895.712583 5720427.047475 79.807053 388895.712524 5720427.047502 79.807091 388895.732207 5720426.821144 80.658827 388896.130540 5720426.855533 79.534233 388895.712583 5720427.047475 79.807053 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600841">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600841">
									<gml:posList srsDimension="3">388901.681448 5720427.379735 78.627069 388901.685585 5720427.336287 79.425878 388902.282883 5720427.393163 79.425878 388902.278746 5720427.436610 78.627069 388901.681448 5720427.379735 78.627069 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600842">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600842">
									<gml:posList srsDimension="3">388902.283896 5720427.386431 79.550287 388902.382433 5720427.402642 79.425878 388901.990234 5720427.303667 80.548797 388901.586404 5720427.326843 79.425878 388901.987004 5720427.337588 79.925133 388902.283896 5720427.386431 79.550287 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600843">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600843">
									<gml:posList srsDimension="3">388901.964242 5720427.575738 79.710686 388901.964184 5720427.575762 79.710726 388901.990234 5720427.303667 80.548797 388902.382433 5720427.402642 79.425878 388901.964242 5720427.575738 79.710686 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600844">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600844">
									<gml:posList srsDimension="3">388902.282883 5720427.393163 79.425878 388902.335101 5720426.844782 79.395781 388902.278746 5720427.436610 78.627069 388902.282883 5720427.393163 79.425878 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600845">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600845">
									<gml:posList srsDimension="3">388901.964184 5720427.575762 79.710726 388901.936908 5720427.861732 79.453155 388901.586404 5720427.326843 79.425878 388901.964184 5720427.575762 79.710726 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600846">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600846">
									<gml:posList srsDimension="3">388901.964184 5720427.575762 79.710726 388901.586404 5720427.326843 79.425878 388901.990234 5720427.303667 80.548797 388901.964184 5720427.575762 79.710726 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600847">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600847">
									<gml:posList srsDimension="3">388901.964184 5720427.575762 79.710726 388901.964242 5720427.575738 79.710686 388902.382433 5720427.402642 79.425878 388901.936908 5720427.861732 79.453155 388901.964184 5720427.575762 79.710726 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600848">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600848">
									<gml:posList srsDimension="3">388901.685585 5720427.336287 79.425878 388901.681448 5720427.379735 78.627069 388901.737803 5720426.787906 79.395781 388901.685585 5720427.336287 79.425878 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600849">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600849">
									<gml:posList srsDimension="3">388901.987004 5720427.337588 79.925133 388902.071858 5720426.446469 79.876225 388902.434651 5720426.854261 79.395781 388902.382433 5720427.402642 79.425878 388902.283896 5720427.386431 79.550287 388901.987004 5720427.337588 79.925133 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600850">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600850">
									<gml:posList srsDimension="3">388901.638622 5720426.778462 79.395781 388902.071858 5720426.446469 79.876225 388901.987004 5720427.337588 79.925133 388901.586404 5720427.326843 79.425878 388901.638622 5720426.778462 79.395781 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600851">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600851">
									<gml:posList srsDimension="3">388896.031280 5720426.846964 79.658827 388896.130540 5720426.855533 79.534233 388895.732207 5720426.821144 80.658827 388895.333874 5720426.786756 79.534233 388895.732207 5720426.821144 80.034233 388896.031280 5720426.846964 79.658827 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600852">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600852">
									<gml:posList srsDimension="3">388895.712524 5720427.047502 79.807091 388895.712583 5720427.047475 79.807053 388896.130540 5720426.855533 79.534233 388895.689017 5720427.319276 79.534233 388895.712524 5720427.047502 79.807091 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600853">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600853">
									<gml:posList srsDimension="3">388896.030911 5720426.846932 78.734233 388895.433134 5720426.795325 78.734233 388895.433134 5720426.795325 79.534233 388896.030911 5720426.846932 79.534233 388896.030911 5720426.846932 78.734233 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600854">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600854">
									<gml:posList srsDimension="3">388888.970562 5720426.515515 79.836119 388888.970621 5720426.515488 79.836081 388889.388578 5720426.323546 79.563260 388888.947055 5720426.787289 79.563260 388888.970562 5720426.515515 79.836119 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600855">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600855">
									<gml:posList srsDimension="3">388888.970562 5720426.515515 79.836119 388888.591912 5720426.254770 79.563260 388888.990245 5720426.289158 80.687855 388888.970562 5720426.515515 79.836119 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600856">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600856">
									<gml:posList srsDimension="3">388888.970562 5720426.515515 79.836119 388888.947055 5720426.787289 79.563260 388888.591912 5720426.254770 79.563260 388888.970562 5720426.515515 79.836119 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600857">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600857">
									<gml:posList srsDimension="3">388889.288948 5720426.314945 78.763251 388888.691172 5720426.263339 78.763269 388888.691172 5720426.263339 79.563260 388889.288948 5720426.314945 79.563260 388889.288948 5720426.314945 78.763251 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600858">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600858">
									<gml:posList srsDimension="3">388889.330749 5720425.830755 79.563260 388889.288948 5720426.314945 78.763251 388889.288948 5720426.314945 79.563260 388889.330749 5720425.830755 79.563260 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600859">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600859">
									<gml:posList srsDimension="3">388888.691172 5720426.263339 78.763269 388888.732972 5720425.779159 79.563260 388888.691172 5720426.263339 79.563260 388888.691172 5720426.263339 78.763269 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600860">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600860">
									<gml:posList srsDimension="3">388890.855381 5720408.280597 79.886404 388890.855440 5720408.280570 79.886443 388890.835757 5720408.506927 80.738178 388890.437424 5720408.472539 79.613584 388890.855381 5720408.280597 79.886404 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600861">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600861">
									<gml:posList srsDimension="3">388890.855440 5720408.280570 79.886443 388891.234090 5720408.541316 79.613584 388890.835757 5720408.506927 80.738178 388890.855440 5720408.280570 79.886443 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600862">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600862">
									<gml:posList srsDimension="3">388890.536684 5720408.481108 79.738178 388890.437424 5720408.472539 79.613584 388890.835757 5720408.506927 80.738178 388891.234090 5720408.541316 79.613584 388890.835757 5720408.506927 80.113584 388890.536684 5720408.481108 79.738178 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600863">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600863">
									<gml:posList srsDimension="3">388890.855440 5720408.280570 79.886443 388890.855381 5720408.280597 79.886404 388890.437424 5720408.472539 79.613584 388890.878947 5720408.008796 79.613584 388890.855440 5720408.280570 79.886443 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600864">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600864">
									<gml:posList srsDimension="3">388890.855440 5720408.280570 79.886443 388890.878947 5720408.008796 79.613584 388891.234090 5720408.541316 79.613584 388890.855440 5720408.280570 79.886443 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600865">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600865">
									<gml:posList srsDimension="3">388891.234090 5720408.541316 79.613584 388891.201029 5720408.924278 79.613584 388890.782933 5720409.118813 80.113584 388890.835757 5720408.506927 80.113584 388891.234090 5720408.541316 79.613584 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600866">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600866">
									<gml:posList srsDimension="3">388890.835757 5720408.506927 80.113584 388890.782933 5720409.118813 80.113584 388890.405470 5720408.842670 79.613584 388890.437424 5720408.472539 79.613584 388890.536684 5720408.481108 79.738178 388890.835757 5720408.506927 80.113584 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600867">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600867">
									<gml:posList srsDimension="3">388890.504961 5720408.852876 79.613584 388890.537054 5720408.481140 78.823799 388890.537054 5720408.481140 79.613584 388890.504961 5720408.852876 79.613584 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600868">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600868">
									<gml:posList srsDimension="3">388891.134830 5720408.532747 78.803344 388891.101907 5720408.914110 79.613584 388891.134830 5720408.532747 79.613584 388891.134830 5720408.532747 78.803344 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600869">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600869">
									<gml:posList srsDimension="3">388888.591912 5720426.254770 79.563260 388888.633711 5720425.770592 79.563260 388889.058170 5720425.502357 80.063260 388888.990245 5720426.289158 80.063260 388888.591912 5720426.254770 79.563260 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600870">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600870">
									<gml:posList srsDimension="3">388888.970621 5720426.515488 79.836081 388888.970562 5720426.515515 79.836119 388888.990245 5720426.289158 80.687855 388889.388578 5720426.323546 79.563260 388888.970621 5720426.515488 79.836081 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600871">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600871">
									<gml:posList srsDimension="3">388888.990245 5720426.289158 80.063260 388889.058170 5720425.502357 80.063260 388889.430379 5720425.839354 79.563260 388889.388578 5720426.323546 79.563260 388889.289318 5720426.314977 79.687855 388888.990245 5720426.289158 80.063260 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600872">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600872">
									<gml:posList srsDimension="3">388889.289318 5720426.314977 79.687855 388889.388578 5720426.323546 79.563260 388888.990245 5720426.289158 80.687855 388888.591912 5720426.254770 79.563260 388888.990245 5720426.289158 80.063260 388889.289318 5720426.314977 79.687855 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600873">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600873">
									<gml:posList srsDimension="3">388897.420463 5720408.912642 79.908745 388897.799114 5720409.173388 79.635886 388897.400781 5720409.138999 80.760481 388897.420463 5720408.912642 79.908745 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600874">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600874">
									<gml:posList srsDimension="3">388897.420463 5720408.912642 79.908745 388897.420404 5720408.912669 79.908707 388897.002447 5720409.104611 79.635886 388897.443971 5720408.640868 79.635886 388897.420463 5720408.912642 79.908745 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600875">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600875">
									<gml:posList srsDimension="3">388897.420404 5720408.912669 79.908707 388897.420463 5720408.912642 79.908745 388897.400781 5720409.138999 80.760481 388897.002447 5720409.104611 79.635886 388897.420404 5720408.912669 79.908707 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600876">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600876">
									<gml:posList srsDimension="3">388897.101708 5720409.113180 79.760481 388897.002447 5720409.104611 79.635886 388897.400781 5720409.138999 80.760481 388897.799114 5720409.173388 79.635886 388897.400781 5720409.138999 80.135886 388897.101708 5720409.113180 79.760481 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600877">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600877">
									<gml:posList srsDimension="3">388897.420463 5720408.912642 79.908745 388897.443971 5720408.640868 79.635886 388897.799114 5720409.173388 79.635886 388897.420463 5720408.912642 79.908745 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600878">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600878">
									<gml:posList srsDimension="3">388897.073926 5720409.439294 79.635886 388897.102077 5720409.113212 78.839870 388897.102077 5720409.113212 79.635886 388897.073926 5720409.439294 79.635886 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600879">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600879">
									<gml:posList srsDimension="3">388897.400781 5720409.138999 80.135886 388897.354806 5720409.671538 80.135886 388896.974344 5720409.430147 79.635886 388897.002447 5720409.104611 79.635886 388897.101708 5720409.113180 79.760481 388897.400781 5720409.138999 80.135886 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600880">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600880">
									<gml:posList srsDimension="3">388897.699853 5720409.164819 78.831878 388897.671420 5720409.494174 79.635886 388897.699853 5720409.164819 79.635886 388897.699853 5720409.164819 78.831878 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600881">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600881">
									<gml:posList srsDimension="3">388897.102077 5720409.113212 78.839870 388897.699853 5720409.164819 78.831878 388897.699853 5720409.164819 79.635886 388897.102077 5720409.113212 79.635886 388897.102077 5720409.113212 78.839870 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600882">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600882">
									<gml:posList srsDimension="3">388897.799114 5720409.173388 79.635886 388897.770633 5720409.503287 79.635886 388897.354806 5720409.671538 80.135886 388897.400781 5720409.138999 80.135886 388897.799114 5720409.173388 79.635886 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600883">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600883">
									<gml:posList srsDimension="3">388903.785318 5720409.718064 79.642619 388903.758167 5720410.032556 79.642619 388903.343358 5720410.189015 80.142619 388903.386985 5720409.683675 80.142619 388903.785318 5720409.718064 79.642619 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600884">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600884">
									<gml:posList srsDimension="3">388903.406667 5720409.457318 79.915478 388903.785318 5720409.718064 79.642619 388903.386985 5720409.683675 80.767214 388903.406667 5720409.457318 79.915478 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600885">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600885">
									<gml:posList srsDimension="3">388903.087912 5720409.657856 79.767214 388902.988652 5720409.649287 79.642619 388903.386985 5720409.683675 80.767214 388903.785318 5720409.718064 79.642619 388903.386985 5720409.683675 80.142619 388903.087912 5720409.657856 79.767214 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600886">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600886">
									<gml:posList srsDimension="3">388903.406608 5720409.457345 79.915440 388903.406667 5720409.457318 79.915478 388903.386985 5720409.683675 80.767214 388902.988652 5720409.649287 79.642619 388903.406608 5720409.457345 79.915440 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600887">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600887">
									<gml:posList srsDimension="3">388903.406667 5720409.457318 79.915478 388903.406608 5720409.457345 79.915440 388902.988652 5720409.649287 79.642619 388903.430175 5720409.185544 79.642619 388903.406667 5720409.457318 79.915478 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600888">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600888">
									<gml:posList srsDimension="3">388903.406667 5720409.457318 79.915478 388903.430175 5720409.185544 79.642619 388903.785318 5720409.718064 79.642619 388903.406667 5720409.457318 79.915478 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600889">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600889">
									<gml:posList srsDimension="3">388903.386985 5720409.683675 80.142619 388903.343358 5720410.189015 80.142619 388902.962108 5720409.956754 79.642619 388902.988652 5720409.649287 79.642619 388903.087912 5720409.657856 79.767214 388903.386985 5720409.683675 80.142619 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600890">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600890">
									<gml:posList srsDimension="3">388903.061661 5720409.966234 79.642619 388903.088281 5720409.657888 78.849387 388903.088281 5720409.657888 79.642619 388903.061661 5720409.966234 79.642619 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600891">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600891">
									<gml:posList srsDimension="3">388903.686057 5720409.709494 78.835826 388903.658983 5720410.023112 79.642619 388903.686057 5720409.709494 79.642619 388903.686057 5720409.709494 78.835826 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600892">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600892">
									<gml:posList srsDimension="3">388903.088281 5720409.657888 78.849387 388903.686057 5720409.709494 78.835826 388903.686057 5720409.709494 79.642619 388903.088281 5720409.657888 79.642619 388903.088281 5720409.657888 78.849387 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600893">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600893">
									<gml:posList srsDimension="3">388927.061404 5720412.306829 80.105367 388926.804320 5720412.774471 80.074879 388926.567769 5720412.375162 79.587419 388926.724922 5720412.089297 79.606056 388926.808770 5720412.143504 79.730478 388927.061404 5720412.306829 80.105367 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600894">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600894">
									<gml:posList srsDimension="3">388927.177111 5720412.096794 79.891554 388927.177048 5720412.096793 79.891514 388926.724922 5720412.089297 79.606056 388927.315924 5720411.844561 79.634699 388927.177111 5720412.096794 79.891554 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600895">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600895">
									<gml:posList srsDimension="3">388927.177111 5720412.096794 79.891554 388927.425638 5720412.474529 79.606305 388927.044071 5720412.337953 80.728944 388927.177111 5720412.096794 79.891554 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600896">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600896">
									<gml:posList srsDimension="3">388926.834643 5720412.097807 78.811366 388927.360645 5720412.386467 78.803575 388927.338333 5720412.426531 79.606274 388926.812552 5720412.137473 79.606087 388926.834643 5720412.097807 78.811366 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600897">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600897">
									<gml:posList srsDimension="3">388927.177048 5720412.096793 79.891514 388927.177111 5720412.096794 79.891554 388927.044071 5720412.337953 80.728944 388926.724922 5720412.089297 79.606056 388927.177048 5720412.096793 79.891514 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600898">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600898">
									<gml:posList srsDimension="3">388926.808770 5720412.143504 79.730478 388926.724922 5720412.089297 79.606056 388927.044071 5720412.337953 80.728944 388927.425638 5720412.474529 79.606305 388927.061404 5720412.306829 80.105367 388926.808770 5720412.143504 79.730478 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600899">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600899">
									<gml:posList srsDimension="3">388927.425638 5720412.474529 79.606305 388927.266379 5720412.764226 79.587419 388926.804320 5720412.774471 80.074879 388927.061404 5720412.306829 80.105367 388927.425638 5720412.474529 79.606305 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600900">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600900">
									<gml:posList srsDimension="3">388927.360645 5720412.386467 78.803575 388927.179336 5720412.715751 79.587419 388927.338333 5720412.426531 79.606274 388927.360645 5720412.386467 78.803575 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600901">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600901">
									<gml:posList srsDimension="3">388926.655135 5720412.423818 79.587419 388926.834643 5720412.097807 78.811366 388926.812552 5720412.137473 79.606087 388926.655135 5720412.423818 79.587419 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600902">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600902">
									<gml:posList srsDimension="3">388927.177111 5720412.096794 79.891554 388927.315924 5720411.844561 79.634699 388927.425638 5720412.474529 79.606305 388927.177111 5720412.096794 79.891554 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600903">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600903">
									<gml:posList srsDimension="3">388930.765180 5720416.275656 83.198147 388930.719988 5720416.799129 83.198147 388930.292787 5720417.099122 83.698147 388930.366847 5720416.241268 83.698147 388930.765180 5720416.275656 83.198147 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600904">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600904">
									<gml:posList srsDimension="3">388930.067774 5720416.215448 83.322741 388929.968513 5720416.206879 83.198147 388930.366847 5720416.241268 84.322741 388930.765180 5720416.275656 83.198147 388930.366847 5720416.241268 83.698147 388930.067774 5720416.215448 83.322741 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600905">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600905">
									<gml:posList srsDimension="3">388930.366847 5720416.241268 83.698147 388930.292787 5720417.099122 83.698147 388929.922555 5720416.739235 83.198147 388929.968513 5720416.206879 83.198147 388930.067774 5720416.215448 83.322741 388930.366847 5720416.241268 83.698147 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600906">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600906">
									<gml:posList srsDimension="3">388930.386529 5720416.014910 83.471006 388930.765180 5720416.275656 83.198147 388930.366847 5720416.241268 84.322741 388930.386529 5720416.014910 83.471006 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600907">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600907">
									<gml:posList srsDimension="3">388930.386470 5720416.014937 83.470967 388930.386529 5720416.014910 83.471006 388930.366847 5720416.241268 84.322741 388929.968513 5720416.206879 83.198147 388930.386470 5720416.014937 83.470967 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600908">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600908">
									<gml:posList srsDimension="3">388930.386529 5720416.014910 83.471006 388930.386470 5720416.014937 83.470967 388929.968513 5720416.206879 83.198147 388930.410037 5720415.743137 83.198147 388930.386529 5720416.014910 83.471006 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600909">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600909">
									<gml:posList srsDimension="3">388930.386529 5720416.014910 83.471006 388930.410037 5720415.743137 83.198147 388930.765180 5720416.275656 83.198147 388930.386529 5720416.014910 83.471006 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600910">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600910">
									<gml:posList srsDimension="3">388930.022280 5720416.746725 83.198147 388930.068143 5720416.215480 82.393084 388930.068143 5720416.215480 83.198147 388930.022280 5720416.746725 83.198147 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600911">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600911">
									<gml:posList srsDimension="3">388930.665919 5720416.267087 82.403184 388930.620632 5720416.791667 83.198147 388930.665919 5720416.267087 83.198147 388930.665919 5720416.267087 82.403184 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600912">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600912">
									<gml:posList srsDimension="3">388930.068143 5720416.215480 82.393084 388930.665919 5720416.267087 82.403184 388930.665919 5720416.267087 83.198147 388930.068143 5720416.215480 83.198147 388930.068143 5720416.215480 82.393084 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600913">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600913">
									<gml:posList srsDimension="3">388928.929791 5720426.079709 83.468345 388928.906283 5720426.351483 83.195486 388928.551140 5720425.818963 83.195486 388928.929791 5720426.079709 83.468345 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600914">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600914">
									<gml:posList srsDimension="3">388928.929791 5720426.079709 83.468345 388928.929850 5720426.079682 83.468307 388929.347806 5720425.887740 83.195486 388928.906283 5720426.351483 83.195486 388928.929791 5720426.079709 83.468345 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600915">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600915">
									<gml:posList srsDimension="3">388928.929850 5720426.079682 83.468307 388928.929791 5720426.079709 83.468345 388928.949473 5720425.853352 84.320081 388929.347806 5720425.887740 83.195486 388928.929850 5720426.079682 83.468307 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600916">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600916">
									<gml:posList srsDimension="3">388928.551140 5720425.818963 83.195486 388928.597693 5720425.279722 83.195486 388929.024291 5720424.986717 83.695486 388928.949473 5720425.853352 83.695486 388928.551140 5720425.818963 83.195486 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600917">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600917">
									<gml:posList srsDimension="3">388928.929791 5720426.079709 83.468345 388928.551140 5720425.818963 83.195486 388928.949473 5720425.853352 84.320081 388928.929791 5720426.079709 83.468345 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600918">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600918">
									<gml:posList srsDimension="3">388929.293835 5720425.350263 83.195486 388929.248177 5720425.879139 82.402133 388929.248177 5720425.879139 83.195486 388929.293835 5720425.350263 83.195486 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600919">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600919">
									<gml:posList srsDimension="3">388928.650400 5720425.827532 82.388798 388928.696826 5720425.289767 83.195486 388928.650400 5720425.827532 83.195486 388928.650400 5720425.827532 82.388798 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600920">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600920">
									<gml:posList srsDimension="3">388929.248177 5720425.879139 82.402133 388928.650400 5720425.827532 82.388798 388928.650400 5720425.827532 83.195486 388929.248177 5720425.879139 83.195486 388929.248177 5720425.879139 82.402133 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600921">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600921">
									<gml:posList srsDimension="3">388934.707912 5720421.535447 83.546395 388934.707939 5720421.535506 83.546433 388934.481582 5720421.515824 84.398169 388934.515970 5720421.117490 83.273574 388934.707912 5720421.535447 83.546395 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600922">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600922">
									<gml:posList srsDimension="3">388934.707939 5720421.535506 83.546433 388934.447193 5720421.914157 83.273574 388934.481582 5720421.515824 84.398169 388934.707939 5720421.535506 83.546433 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600923">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600923">
									<gml:posList srsDimension="3">388934.707939 5720421.535506 83.546433 388934.707912 5720421.535447 83.546395 388934.515970 5720421.117490 83.273574 388934.979713 5720421.559014 83.273574 388934.707939 5720421.535506 83.546433 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600924">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600924">
									<gml:posList srsDimension="3">388934.447193 5720421.914157 83.273574 388934.079473 5720421.882411 83.273574 388933.888075 5720421.464586 83.773574 388934.481582 5720421.515824 83.773574 388934.447193 5720421.914157 83.273574 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600925">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600925">
									<gml:posList srsDimension="3">388934.507401 5720421.216751 83.398169 388934.515970 5720421.117490 83.273574 388934.481582 5720421.515824 84.398169 388934.447193 5720421.914157 83.273574 388934.481582 5720421.515824 83.773574 388934.507401 5720421.216751 83.398169 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600926">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600926">
									<gml:posList srsDimension="3">388934.707939 5720421.535506 83.546433 388934.979713 5720421.559014 83.273574 388934.447193 5720421.914157 83.273574 388934.707939 5720421.535506 83.546433 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600927">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600927">
									<gml:posList srsDimension="3">388934.481582 5720421.515824 83.773574 388933.888075 5720421.464586 83.773574 388934.153072 5720421.086161 83.273574 388934.515970 5720421.117490 83.273574 388934.507401 5720421.216751 83.398169 388934.481582 5720421.515824 83.773574 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600928">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600928">
									<gml:posList srsDimension="3">388934.143868 5720421.185738 83.273574 388934.507369 5720421.217120 82.477111 388934.507369 5720421.217120 83.273574 388934.143868 5720421.185738 83.273574 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600929">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600929">
									<gml:posList srsDimension="3">388934.455763 5720421.814896 82.469183 388934.088643 5720421.783203 83.273574 388934.455763 5720421.814896 83.273574 388934.455763 5720421.814896 82.469183 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600930">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600930">
									<gml:posList srsDimension="3">388934.507369 5720421.217120 82.477111 388934.455763 5720421.814896 82.469183 388934.455763 5720421.814896 83.273574 388934.507369 5720421.217120 83.273574 388934.507369 5720421.217120 82.477111 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600931">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600931">
									<gml:posList srsDimension="3">388925.775681 5720429.712894 79.873025 388925.931585 5720429.936740 79.600166 388925.317864 5720429.754945 79.600166 388925.775681 5720429.712894 79.873025 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600932">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600932">
									<gml:posList srsDimension="3">388925.775681 5720429.712894 79.873025 388925.775708 5720429.712835 79.872987 388925.973954 5720429.297831 79.600166 388925.931585 5720429.936740 79.600166 388925.775681 5720429.712894 79.873025 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600933">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600933">
									<gml:posList srsDimension="3">388925.892209 5720429.354785 79.724761 388925.973954 5720429.297831 79.600166 388925.645909 5720429.526388 80.724761 388925.317864 5720429.754945 79.600166 388925.645909 5720429.526388 80.100166 388925.892209 5720429.354785 79.724761 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600934">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600934">
									<gml:posList srsDimension="3">388925.775708 5720429.712835 79.872987 388925.775681 5720429.712894 79.873025 388925.645909 5720429.526388 80.724761 388925.973954 5720429.297831 79.600166 388925.775708 5720429.712835 79.872987 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600935">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600935">
									<gml:posList srsDimension="3">388925.775681 5720429.712894 79.873025 388925.317864 5720429.754945 79.600166 388925.645909 5720429.526388 80.724761 388925.775681 5720429.712894 79.873025 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600936">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600936">
									<gml:posList srsDimension="3">388925.645909 5720429.526388 80.100166 388925.265038 5720428.979729 80.100166 388925.742747 5720428.965983 79.600166 388925.973954 5720429.297831 79.600166 388925.892209 5720429.354785 79.724761 388925.645909 5720429.526388 80.100166 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600937">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600937">
									<gml:posList srsDimension="3">388925.317864 5720429.754945 79.600166 388925.080251 5720429.413902 79.600166 388925.265038 5720428.979729 80.100166 388925.645909 5720429.526388 80.100166 388925.317864 5720429.754945 79.600166 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600938">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600938">
									<gml:posList srsDimension="3">388925.399609 5720429.697991 78.791708 388925.162794 5720429.358093 79.600166 388925.399609 5720429.697991 79.600166 388925.399609 5720429.697991 78.791708 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600939">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600939">
									<gml:posList srsDimension="3">388925.659897 5720429.021999 79.600166 388925.891905 5720429.354997 78.808118 388925.891905 5720429.354997 79.600166 388925.659897 5720429.021999 79.600166 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600940">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600940">
									<gml:posList srsDimension="3">388925.891905 5720429.354997 78.808118 388925.399609 5720429.697991 78.791708 388925.399609 5720429.697991 79.600166 388925.891905 5720429.354997 79.600166 388925.891905 5720429.354997 78.808118 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600941">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600941">
									<gml:posList srsDimension="3">388929.347806 5720425.887740 83.195486 388929.248177 5720425.879139 83.195486 388928.650400 5720425.827532 83.195486 388928.551140 5720425.818963 83.195486 388928.906283 5720426.351483 83.195486 388929.347806 5720425.887740 83.195486 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600942">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600942">
									<gml:posList srsDimension="3">388925.399609 5720429.697991 79.600166 388925.317864 5720429.754945 79.600166 388925.931585 5720429.936740 79.600166 388925.973954 5720429.297831 79.600166 388925.891905 5720429.354997 79.600166 388925.399609 5720429.697991 79.600166 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600943">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600943">
									<gml:posList srsDimension="3">388901.685585 5720427.336287 79.425878 388901.586404 5720427.326843 79.425878 388901.936908 5720427.861732 79.453155 388902.382433 5720427.402642 79.425878 388902.282883 5720427.393163 79.425878 388901.685585 5720427.336287 79.425878 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600944">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600944">
									<gml:posList srsDimension="3">388895.381325 5720426.237117 79.534233 388895.809315 5720425.927982 80.034233 388895.732207 5720426.821144 80.034233 388895.333874 5720426.786756 79.534233 388895.381325 5720426.237117 79.534233 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600945">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600945">
									<gml:posList srsDimension="3">388895.433134 5720426.795325 79.534233 388895.333874 5720426.786756 79.534233 388895.689017 5720427.319276 79.534233 388896.130540 5720426.855533 79.534233 388896.030911 5720426.846932 79.534233 388895.433134 5720426.795325 79.534233 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600946">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600946">
									<gml:posList srsDimension="3">388888.691172 5720426.263339 79.563260 388888.591912 5720426.254770 79.563260 388888.947055 5720426.787289 79.563260 388889.388578 5720426.323546 79.563260 388889.288948 5720426.314945 79.563260 388888.691172 5720426.263339 79.563260 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600947">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600947">
									<gml:posList srsDimension="3">388890.537054 5720408.481140 78.823799 388891.134830 5720408.532747 78.803344 388891.134830 5720408.532747 79.613584 388890.537054 5720408.481140 79.613584 388890.537054 5720408.481140 78.823799 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600948">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600948">
									<gml:posList srsDimension="3">388890.537054 5720408.481140 79.613584 388891.134830 5720408.532747 79.613584 388891.234090 5720408.541316 79.613584 388890.878947 5720408.008796 79.613584 388890.437424 5720408.472539 79.613584 388890.537054 5720408.481140 79.613584 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600949">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600949">
									<gml:posList srsDimension="3">388897.102077 5720409.113212 79.635886 388897.699853 5720409.164819 79.635886 388897.799114 5720409.173388 79.635886 388897.443971 5720408.640868 79.635886 388897.002447 5720409.104611 79.635886 388897.102077 5720409.113212 79.635886 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600950">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600950">
									<gml:posList srsDimension="3">388903.686057 5720409.709494 79.642619 388903.785318 5720409.718064 79.642619 388903.430175 5720409.185544 79.642619 388902.988652 5720409.649287 79.642619 388903.088281 5720409.657888 79.642619 388903.686057 5720409.709494 79.642619 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600951">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600951">
									<gml:posList srsDimension="3">388926.724922 5720412.089297 79.606056 388926.812552 5720412.137473 79.606087 388927.338333 5720412.426531 79.606274 388927.425638 5720412.474529 79.606305 388927.315924 5720411.844561 79.634699 388926.724922 5720412.089297 79.606056 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600952">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600952">
									<gml:posList srsDimension="3">388930.665919 5720416.267087 83.198147 388930.765180 5720416.275656 83.198147 388930.410037 5720415.743137 83.198147 388929.968513 5720416.206879 83.198147 388930.068143 5720416.215480 83.198147 388930.665919 5720416.267087 83.198147 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600953">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600953">
									<gml:posList srsDimension="3">388934.515970 5720421.117490 83.273574 388934.507369 5720421.217120 83.273574 388934.455763 5720421.814896 83.273574 388934.447193 5720421.914157 83.273574 388934.979713 5720421.559014 83.273574 388934.515970 5720421.117490 83.273574 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600954">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600954">
									<gml:posList srsDimension="3">388928.949473 5720425.853352 83.695486 388929.024291 5720424.986717 83.695486 388929.393337 5720425.360346 83.195486 388929.347806 5720425.887740 83.195486 388929.248546 5720425.879171 83.320081 388928.949473 5720425.853352 83.695486 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600955">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600955">
									<gml:posList srsDimension="3">388929.248546 5720425.879171 83.320081 388929.347806 5720425.887740 83.195486 388928.949473 5720425.853352 84.320081 388928.551140 5720425.818963 83.195486 388928.949473 5720425.853352 83.695486 388929.248546 5720425.879171 83.320081 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:CompositeSurface>
			</bldg:lod2Geometry>
		</bldg:BuildingInstallation>
	</bldg:outerBuildingInstallation>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1870_351205_267803">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo1f96e285-a597-4bea-922e-ae492a0d6973">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600443">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600443">
									<gml:posList srsDimension="3">388934.460000 5720413.233000 69.511000 388934.329000 5720414.937000 69.511000 388933.199000 5720414.848000 69.511000 388933.335000 5720413.133000 69.511000 388934.460000 5720413.233000 69.511000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1217_605682_322347">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo3d0e1812-f056-4aa3-99e0-79d7286acb5e">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600444">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600444">
									<gml:posList srsDimension="3">388894.928000 5720408.530000 77.911000 388899.947000 5720408.991000 77.911000 388897.184602 5720411.380620 84.313068 388894.928000 5720408.530000 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1421_735416_69372">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeod239de8e-8610-4e70-ab28-6f74251ed0ef">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600445">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600445">
									<gml:posList srsDimension="3">388902.585069 5720418.004764 84.287822 388902.207189 5720417.432271 83.081167 388901.147415 5720409.105306 77.911000 388903.182570 5720411.806224 84.308097 388902.585069 5720418.004764 84.287822 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_231_165615_102908">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo5134fa24-0798-47cb-9579-b8e136d3ed67">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600446">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600446">
									<gml:posList srsDimension="3">388901.147415 5720409.105306 77.911000 388905.696673 5720409.538494 77.911000 388905.556072 5720410.956941 81.564421 388903.182570 5720411.806224 84.308097 388901.147415 5720409.105306 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1688_390609_326955">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeob3cb6faf-f3a1-43e4-9939-106bab59454b">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600447">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600447">
									<gml:posList srsDimension="3">388884.238000 5720413.203000 98.782158 388884.143477 5720416.903632 109.921000 388881.179000 5720412.724000 94.841000 388884.238000 5720413.203000 98.782158 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1116_432296_64952">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo8cc03455-e99a-473c-ab71-31030c45849c">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600448">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600448">
									<gml:posList srsDimension="3">388884.143477 5720416.903632 109.921000 388884.238000 5720413.203000 98.782158 388887.297000 5720413.682000 94.841000 388884.143477 5720416.903632 109.921000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1267_116485_2075">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo00b87548-c10b-47c5-97f5-e18cafbae81d">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600449">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600449">
									<gml:posList srsDimension="3">388886.727500 5720417.308500 98.782158 388884.143477 5720416.903632 109.921000 388887.297000 5720413.682000 94.841000 388886.727500 5720417.308500 98.782158 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_366_453044_49361">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo5ca0d7e4-15a3-4cb5-b864-963a1e596778">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600450">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600450">
									<gml:posList srsDimension="3">388884.143477 5720416.903632 109.921000 388886.727500 5720417.308500 98.782158 388886.158000 5720420.935000 94.841000 388884.143477 5720416.903632 109.921000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1469_650562_415555">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo346a401d-c66d-4629-979e-58c311a2ec7e">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600451">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600451">
									<gml:posList srsDimension="3">388883.101000 5720420.456500 98.782158 388884.143477 5720416.903632 109.921000 388886.158000 5720420.935000 94.841000 388883.101000 5720420.456500 98.782158 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1376_765916_230100">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo9d5ba9fe-d99c-4fcb-abbd-2326a7f571dd">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600452">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600452">
									<gml:posList srsDimension="3">388884.143477 5720416.903632 109.921000 388883.101000 5720420.456500 98.782158 388880.044000 5720419.978000 94.841000 388884.143477 5720416.903632 109.921000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1831_69054_90476">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo70cead5c-3858-4e7e-998f-b84ae36029ed">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600453">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600453">
									<gml:posList srsDimension="3">388880.611500 5720416.351000 98.782158 388884.143477 5720416.903632 109.921000 388880.044000 5720419.978000 94.841000 388880.611500 5720416.351000 98.782158 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_881_692262_386263">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeobe3d8c48-b070-4628-9452-c35ffc203df3">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600454">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600454">
									<gml:posList srsDimension="3">388884.143477 5720416.903632 109.921000 388880.611500 5720416.351000 98.782158 388881.179000 5720412.724000 94.841000 388884.143477 5720416.903632 109.921000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1559_211713_39388">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoef798146-3952-49e8-8b67-cdbcba35a81a">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600455">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600455">
									<gml:posList srsDimension="3">388910.151977 5720409.980118 81.564421 388907.614365 5720412.695398 87.881000 388905.696673 5720409.538494 81.564421 388910.151977 5720409.980118 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_400_431608_111499">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeob6e7d505-77e9-4024-b62d-e165ede98267">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600456">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600456">
									<gml:posList srsDimension="3">388907.614365 5720412.695398 87.881000 388910.151977 5720409.980118 81.564421 388910.278274 5720417.080724 87.881000 388907.614365 5720412.695398 87.881000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_512_336789_418059">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoc6734bf7-b698-4327-9c4c-4ba3d69afd42">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600457">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600457">
									<gml:posList srsDimension="3">388921.658485 5720414.116173 87.881000 388918.111154 5720417.626385 87.881000 388919.471757 5720410.891582 81.564421 388921.658485 5720414.116173 87.881000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_569_861887_359994">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo3cb68842-48ed-42e9-a246-d2a9338f9f17">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600458">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600458">
									<gml:posList srsDimension="3">388924.427393 5720411.376238 81.564421 388921.658485 5720414.116173 87.881000 388919.471757 5720410.891582 81.564421 388924.427393 5720411.376238 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_812_202407_85894">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo0910d3ec-f8b2-4a46-8128-26edc5366b61">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600459">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600459">
									<gml:posList srsDimension="3">388920.175996 5720426.935924 87.881000 388917.652894 5720429.526100 81.564421 388917.424608 5720422.713744 87.881000 388920.175996 5720426.935924 87.881000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_9_75864_277755">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoee004734-8df5-4984-90c6-334c833eaeee">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600460">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600460">
									<gml:posList srsDimension="3">388902.403767 5720419.673752 84.323510 388903.648973 5720420.507691 82.721185 388904.388738 5720422.717946 81.564421 388904.130097 5720425.330937 81.564421 388902.083225 5720423.000783 84.312921 388902.403767 5720419.673752 84.323510 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_676_402287_183639">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeodc23eea8-bb52-4b52-9218-8ace13e22f64">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600461">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600461">
									<gml:posList srsDimension="3">388906.307332 5720425.644213 87.881000 388904.388738 5720422.717946 81.564421 388909.965877 5720421.920415 87.881000 388906.307332 5720425.644213 87.881000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1356_744665_321887">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeofb091517-8254-43a5-846b-9d58670b11de">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600462">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600462">
									<gml:posList srsDimension="3">388904.388738 5720422.717946 81.564421 388906.307332 5720425.644213 87.881000 388903.851783 5720428.142688 81.564421 388904.130097 5720425.330937 81.564421 388904.388738 5720422.717946 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_905_639119_101077">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo40a347e0-d082-4e6e-9d8a-f42889234d5d">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600463">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600463">
									<gml:posList srsDimension="3">388907.614365 5720412.695398 87.881000 388905.113576 5720415.421026 81.564421 388905.556072 5720410.956941 81.564421 388905.696673 5720409.538494 81.564421 388907.614365 5720412.695398 87.881000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1425_454300_277327">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo962c00df-e3f3-4daa-ac1f-9bb490bdae5b">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600464">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600464">
									<gml:posList srsDimension="3">388907.614365 5720412.695398 87.881000 388910.278274 5720417.080724 87.881000 388905.113576 5720415.421026 81.564421 388907.614365 5720412.695398 87.881000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1543_725794_210179">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo4aa7f7d9-9103-46af-b5d1-29ffb9e0eb0b">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600465">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600465">
									<gml:posList srsDimension="3">388913.567540 5720418.340684 92.081000 388910.151977 5720409.980118 81.564421 388914.398478 5720416.238879 89.220681 388919.471757 5720410.891582 81.564421 388914.762329 5720418.457533 92.081000 388913.567540 5720418.340684 92.081000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_613_467936_404629">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoe587affc-9e95-4c78-b30b-9d2fb0ab4b96">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600466">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600466">
									<gml:posList srsDimension="3">388914.398478 5720416.238879 89.220681 388915.439231 5720405.978917 89.154559 388919.979980 5720405.684766 81.564421 388919.471757 5720410.891582 81.564421 388914.398478 5720416.238879 89.220681 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1787_548005_190890">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoeac72c0d-922f-4e28-ae74-c8b4e8d8e0ca">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600467">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600467">
									<gml:posList srsDimension="3">388910.151977 5720409.980118 81.564421 388910.694489 5720404.841180 81.564421 388915.439231 5720405.978917 89.154559 388914.398478 5720416.238879 89.220681 388910.151977 5720409.980118 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_723_631326_171200">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo8ffad949-60ae-42e5-b6a2-682e328d94da">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600468">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600468">
									<gml:posList srsDimension="3">388910.628000 5720405.471000 77.911000 388910.151977 5720409.980118 81.564421 388908.285489 5720407.697559 81.564421 388910.628000 5720405.471000 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_664_605901_261898">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo475427e9-1e41-4844-a8f8-c2baafbdee82">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600469">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600469">
									<gml:posList srsDimension="3">388910.151977 5720409.980118 81.564421 388905.696673 5720409.538494 77.911000 388908.285489 5720407.697559 81.564421 388910.151977 5720409.980118 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_645_166192_210366">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo6b750725-9d99-4bdf-828d-dc3330a395ad">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600470">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600470">
									<gml:posList srsDimension="3">388906.027000 5720406.206000 77.911000 388908.285489 5720407.697559 81.564421 388905.696673 5720409.538494 77.911000 388906.027000 5720406.206000 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_35_333432_344649">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeod574fccb-d1d6-4d1d-a942-7526bb4b1eee">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600471">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600471">
									<gml:posList srsDimension="3">388887.297000 5720413.682000 77.911000 388882.264995 5720412.894053 75.861000 388882.300141 5720412.534650 75.861000 388882.672000 5720408.732000 75.861000 388887.297000 5720413.682000 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1187_250450_404891">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo9781a2cd-9b0a-490c-ad41-5d2bf8dbc75f">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600472">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600472">
									<gml:posList srsDimension="3">388887.719000 5720409.370000 75.861000 388887.297000 5720413.682000 77.911000 388882.672000 5720408.732000 75.861000 388887.719000 5720409.370000 75.861000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_946_546708_67581">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo4380f91a-008b-46a9-a836-e32b6259ac82">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600473">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600473">
									<gml:posList srsDimension="3">388890.080442 5720418.335733 84.622329 388889.762751 5720422.984658 84.292126 388886.040000 5720425.213000 77.911000 388886.158000 5720420.935000 77.911000 388890.080442 5720418.335733 84.622329 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1291_148676_124018">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo42ba0c5c-f2e1-48d2-b1e1-cf19f11ca1c7">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600474">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600474">
									<gml:posList srsDimension="3">388886.158000 5720420.935000 77.911000 388880.468000 5720424.332000 75.861000 388881.130000 5720420.148000 75.861000 388886.158000 5720420.935000 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1526_802645_12284">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo264c144a-212b-4bed-94be-cbd9836f7f40">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600475">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600475">
									<gml:posList srsDimension="3">388880.468000 5720424.332000 75.861000 388886.158000 5720420.935000 77.911000 388886.040000 5720425.213000 75.861000 388880.468000 5720424.332000 75.861000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_331_81374_24821">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoccf48d3b-b7e8-439d-8a62-c755d90f5986">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600476">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600476">
									<gml:posList srsDimension="3">388934.034000 5720407.055000 68.121000 388935.033000 5720407.169000 68.121000 388934.741000 5720410.208000 68.121000 388933.686000 5720410.113000 68.121000 388934.034000 5720407.055000 68.121000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1741_588184_58979">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo5fac2ad8-45b1-4ed7-bafd-7d8b50d849de">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600477">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600477">
									<gml:posList srsDimension="3">388929.179558 5720415.598376 82.731000 388928.485546 5720415.546249 82.731000 388928.537672 5720414.852237 82.731000 388929.231685 5720414.904363 82.731000 388929.179558 5720415.598376 82.731000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1589_592590_215964">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo761c2676-4e7b-47e1-bdaa-fcb68c9c7342">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600478">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600478">
									<gml:posList srsDimension="3">388930.635000 5720426.583000 81.564421 388927.181000 5720426.233000 81.564421 388927.585853 5720420.842806 89.641000 388931.184523 5720421.159989 89.711601 388930.635000 5720426.583000 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_382_816883_223681">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo2981ccde-2b1d-44c1-82ce-f99fe1f8de93">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600479">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600479">
									<gml:posList srsDimension="3">388931.184523 5720421.159989 89.711601 388931.650000 5720426.272000 81.564421 388930.635000 5720426.583000 81.564421 388931.184523 5720421.159989 89.711601 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1511_809438_397415">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoc638d358-f446-49bd-a6f8-b211de9bc84b">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600480">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600480">
									<gml:posList srsDimension="3">388931.184523 5720421.159989 89.711601 388934.230000 5720424.173000 81.564421 388931.650000 5720426.272000 81.564421 388931.184523 5720421.159989 89.711601 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1719_232043_332823">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoc938e10e-4692-4880-93d2-a507fcdb7b97">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600481">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600481">
									<gml:posList srsDimension="3">388931.184523 5720421.159989 89.711601 388934.741000 5720423.232000 81.564421 388934.230000 5720424.173000 81.564421 388931.184523 5720421.159989 89.711601 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_933_124418_276261">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo7a40ab1a-2750-4195-bcac-5a0fb2addf5d">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600482">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600482">
									<gml:posList srsDimension="3">388931.184523 5720421.159989 89.711601 388935.050000 5720419.889000 81.564421 388934.741000 5720423.232000 81.564421 388931.184523 5720421.159989 89.711601 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1227_29008_203640">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo9a475bf4-fdcd-4df1-8de8-2b107d5ecb6d">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600483">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600483">
									<gml:posList srsDimension="3">388931.184523 5720421.159989 89.711601 388934.770000 5720418.899000 81.564421 388935.050000 5720419.889000 81.564421 388931.184523 5720421.159989 89.711601 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_721_489394_41116">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeob4fe7cc9-d455-45ab-9745-0183c8627129">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600484">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600484">
									<gml:posList srsDimension="3">388932.563000 5720416.275000 81.564421 388931.184523 5720421.159989 89.711601 388931.649711 5720415.783907 81.564421 388932.563000 5720416.275000 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1571_692338_226188">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoef43bc25-3231-4630-9dab-5643ae036e3d">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600485">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600485">
									<gml:posList srsDimension="3">388931.184523 5720421.159989 89.711601 388932.563000 5720416.275000 81.564421 388934.770000 5720418.899000 81.564421 388931.184523 5720421.159989 89.711601 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_259_273743_152619">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo1dd1c3a9-2cf0-49a5-aadb-b9492fba0a43">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600486">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600486">
									<gml:posList srsDimension="3">388915.364442 5720419.355956 92.081000 388919.695261 5720418.246896 86.624773 388917.154273 5720420.059300 89.641000 388919.365872 5720422.407006 86.390471 388915.257640 5720420.227948 92.081000 388915.364442 5720419.355956 92.081000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1946_599684_87772">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeof4863de3-cc19-4b81-befe-9acacbb5dcbf">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600487">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600487">
									<gml:posList srsDimension="3">388923.128136 5720416.336449 83.360061 388920.333597 5720417.464452 85.374486 388918.111154 5720417.626385 87.881000 388921.658485 5720414.116173 87.881000 388923.128136 5720416.336449 83.360061 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1232_527961_291086">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo1087832a-aad0-4f16-b666-f8346e6710f7">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600488">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600488">
									<gml:posList srsDimension="3">388924.427393 5720411.376238 81.564421 388924.095903 5720414.082719 81.564421 388923.958298 5720415.206212 81.564421 388923.128136 5720416.336449 83.360061 388921.658485 5720414.116173 87.881000 388924.427393 5720411.376238 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_422_382364_308927">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoe91b1ad5-e341-4a2e-b0f0-ed1cf19e120c">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600489">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600489">
									<gml:posList srsDimension="3">388919.365872 5720422.407006 86.390471 388919.962818 5720423.434818 84.925858 388917.424608 5720422.713744 87.881000 388917.652894 5720429.526100 81.564421 388914.497728 5720420.998675 92.081000 388915.257640 5720420.227948 92.081000 388919.365872 5720422.407006 86.390471 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1696_391408_180125">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeof4c6df4d-68f4-4c1b-95a3-c7cac97e6826">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600490">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600490">
									<gml:posList srsDimension="3">388896.600829 5720417.428620 84.305755 388897.137153 5720416.932720 83.070625 388902.207189 5720417.432271 83.081167 388902.585069 5720418.004764 84.287822 388903.644005 5720417.567835 83.070625 388906.039130 5720417.801617 83.070625 388908.297660 5720419.413641 86.205656 388886.727860 5720417.306207 86.201000 388887.297000 5720413.682000 77.911000 388890.378696 5720416.829294 84.323776 388890.938632 5720416.308777 83.027995 388896.106414 5720416.752144 82.890466 388896.600829 5720417.428620 84.305755 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_8_556242_320703">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo1017d55e-4cc8-4497-98e2-4c15363038aa">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600491">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600491">
									<gml:posList srsDimension="3">388889.762751 5720422.984658 84.292126 388885.829904 5720426.536039 77.911000 388886.040000 5720425.213000 77.911000 388889.762751 5720422.984658 84.292126 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1332_418426_271979">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo137c97a0-88b0-4ba1-aef2-477398a4f582">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600492">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600492">
									<gml:posList srsDimension="3">388903.453187 5720432.457419 77.911000 388905.972133 5720430.374637 81.564421 388907.739000 5720432.837000 77.911000 388903.453187 5720432.457419 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1051_485561_16500">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoaed339f2-e9e6-483e-be76-d005a3515f5f">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600493">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600493">
									<gml:posList srsDimension="3">388906.146886 5720405.085615 77.911000 388908.285489 5720407.697559 81.564421 388906.027000 5720406.206000 77.911000 388906.146886 5720405.085615 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_852_343898_178155">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo8feebeab-2a6e-43af-8bda-6f52e76caebe">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600494">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600494">
									<gml:posList srsDimension="3">388908.285489 5720407.697559 81.564421 388906.146886 5720405.085615 77.911000 388910.628000 5720405.471000 77.911000 388908.285489 5720407.697559 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1580_423105_371491">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo2cf1f1ad-8270-47d3-b515-6d9abbf481e3">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600495">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600495">
									<gml:posList srsDimension="3">388887.921349 5720407.779382 77.911000 388890.932496 5720411.129769 84.316223 388887.719000 5720409.370000 77.911000 388887.921349 5720407.779382 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_19_380884_387147">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo90fe7f0a-c28e-4e43-8cbc-9074f83c7197">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600496">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600496">
									<gml:posList srsDimension="3">388887.719000 5720409.370000 77.911000 388890.932496 5720411.129769 84.316223 388890.378696 5720416.829294 84.323776 388887.297000 5720413.682000 77.911000 388887.719000 5720409.370000 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_198_865797_414645">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeod453cca9-b343-4d3c-8245-243cd6d21ce3">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600497">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600497">
									<gml:posList srsDimension="3">388905.972133 5720430.374637 81.564421 388903.453187 5720432.457419 77.911000 388903.540000 5720431.296000 77.911000 388905.972133 5720430.374637 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_984_392538_240737">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo77744895-6ef2-4a10-89fa-8dc4c9ef4c62">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600498">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600498">
									<gml:posList srsDimension="3">388903.852558 5720428.142770 77.911000 388905.972133 5720430.374637 81.564421 388903.540000 5720431.296000 77.911000 388903.852558 5720428.142770 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_515_55436_27052">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoed8fa76c-89ae-477b-8061-63e6fd55ab0b">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600499">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600499">
									<gml:posList srsDimension="3">388917.178286 5720434.388529 81.564421 388912.663708 5720433.340614 89.330896 388913.700327 5720423.121406 89.265037 388917.652894 5720429.526100 81.564421 388917.178286 5720434.388529 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_152_129061_241802">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoee3fca60-d8bb-4029-a03a-8256bc8b91b0">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600500">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600500">
									<gml:posList srsDimension="3">388905.556072 5720410.956941 81.564421 388905.113576 5720415.421026 81.564421 388903.644005 5720417.567835 83.070625 388902.585069 5720418.004764 84.287822 388903.182570 5720411.806224 84.308097 388905.556072 5720410.956941 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1349_792514_19479">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo99a94fa3-25a1-469d-81e6-4480506c694a">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600501">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600501">
									<gml:posList srsDimension="3">388901.853000 5720405.758000 71.791000 388906.027000 5720406.206000 71.791000 388905.834618 5720408.146839 73.850883 388904.041495 5720407.967034 73.864170 388901.853000 5720405.758000 71.791000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_434_710835_21377">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeocfd39c02-a8a2-435a-a20b-61b4b34ea9ce">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600502">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600502">
									<gml:posList srsDimension="3">388904.041495 5720407.967034 73.864170 388905.834618 5720408.146839 73.850883 388905.696673 5720409.538494 71.791000 388901.537972 5720409.142495 71.791000 388904.041495 5720407.967034 73.864170 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_800_342872_32139">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeof0bb5f4d-64f7-42e7-bcf4-226abea3d9da">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600503">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600503">
									<gml:posList srsDimension="3">388904.041495 5720407.967034 73.864170 388901.537972 5720409.142495 71.791000 388901.623000 5720408.229000 71.791000 388901.853000 5720405.758000 71.791000 388904.041495 5720407.967034 73.864170 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_347_494957_212329">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoca9db520-90fa-4abf-b7e1-e25ce42d96e5">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600504">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600504">
									<gml:posList srsDimension="3">388901.444113 5720429.347078 73.532631 388903.713711 5720429.543522 73.507225 388903.540000 5720431.296000 71.681000 388899.341000 5720430.978000 71.681000 388901.444113 5720429.347078 73.532631 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_485_522353_289281">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeod9a9d46c-ef33-40e5-8c66-8c620cb2ee80">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600505">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600505">
									<gml:posList srsDimension="3">388908.524000 5720433.570000 80.621000 388907.500000 5720434.471000 78.875264 388906.808000 5720433.687000 79.004833 388907.739000 5720432.837000 80.621000 388907.670144 5720433.489242 79.947456 388908.524000 5720433.570000 80.621000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_450_585332_331354">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeodd4a64d0-9f18-4b27-811f-4df5744c2a4c">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600506">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600506">
									<gml:posList srsDimension="3">388917.237000 5720433.787000 80.621000 388918.000000 5720434.717000 79.169075 388917.196000 5720435.364000 78.985850 388916.348000 5720434.310000 80.621000 388917.178286 5720434.388529 80.023974 388917.237000 5720433.787000 80.621000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_927_537125_16371">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo54e46b0b-68d1-4721-a004-3fdf32ffa60a">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600507">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600507">
									<gml:posList srsDimension="3">388920.175996 5720426.935924 87.881000 388921.887949 5720429.563025 82.404409 388921.531881 5720429.905461 81.564421 388917.652894 5720429.526100 81.564421 388920.175996 5720426.935924 87.881000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1108_131594_169918">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo90c9cb9e-e96a-41e9-b579-54f5d7941e09">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600508">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600508">
									<gml:posList srsDimension="3">388897.184602 5720411.380620 84.313068 388899.947000 5720408.991000 77.911000 388897.137153 5720416.932720 83.070625 388896.600829 5720417.428620 84.305755 388897.184602 5720411.380620 84.313068 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_404_495586_300434">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo78bd4b95-dfa6-4d4d-b382-626b07816f98">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600509">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600509">
									<gml:posList srsDimension="3">388906.039130 5720417.801617 83.070625 388903.644005 5720417.567835 83.070625 388905.113576 5720415.421026 81.564421 388906.039130 5720417.801617 83.070625 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1358_260684_134746">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeod3673dc2-c8bf-4d46-8988-8d0c7b586255">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600510">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600510">
									<gml:posList srsDimension="3">388890.080442 5720418.335733 84.622329 388890.804555 5720419.320159 82.566753 388891.912000 5720427.061000 77.911000 388889.762751 5720422.984658 84.292126 388890.080442 5720418.335733 84.622329 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1443_184500_236166">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo8b03d0ff-3334-44b3-ba06-e661248134d9">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600511">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600511">
									<gml:posList srsDimension="3">388896.063228 5720422.984658 84.318222 388893.138000 5720427.167000 77.911000 388895.659686 5720419.815416 82.520786 388896.439089 5720419.090664 84.322945 388896.166627 5720421.913430 84.319521 388896.063228 5720422.984658 84.318222 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_348_545583_5221">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo49d559c2-31ed-44c3-a046-2d99b5a56dff">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600512">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600512">
									<gml:posList srsDimension="3">388890.804555 5720419.320159 82.566753 388895.659686 5720419.815416 82.520786 388893.138000 5720427.167000 77.911000 388891.912000 5720427.061000 77.911000 388890.804555 5720419.320159 82.566753 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1597_474910_239134">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo2472005d-fc17-458a-80ac-77db154b8683">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600513">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600513">
									<gml:posList srsDimension="3">388902.207189 5720417.432271 83.081167 388897.137153 5720416.932720 83.070625 388899.947000 5720408.991000 77.911000 388901.147415 5720409.105306 77.911000 388902.207189 5720417.432271 83.081167 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_495_856511_72981">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoef625414-767e-452e-8cdf-cca780845560">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600514">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600514">
									<gml:posList srsDimension="3">388908.297660 5720419.413641 86.205656 388905.479568 5720420.688563 82.717042 388903.648973 5720420.507691 82.721185 388902.403767 5720419.673752 84.323510 388901.778063 5720420.322837 82.725419 388896.991079 5720419.849860 82.736252 388896.439089 5720419.090664 84.322945 388895.659686 5720419.815416 82.520786 388890.804555 5720419.320159 82.566753 388890.080442 5720418.335733 84.622329 388886.158000 5720420.935000 77.911000 388886.727860 5720417.306207 86.201000 388908.297660 5720419.413641 86.205656 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_858_726305_145553">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo311b4eae-c606-4629-906c-f2d2e12cc49f">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600515">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600515">
									<gml:posList srsDimension="3">388912.772822 5720419.993877 92.081000 388904.388738 5720422.717946 81.564421 388905.479568 5720420.688563 82.717042 388908.297660 5720419.413641 86.205656 388906.039130 5720417.801617 83.070625 388905.113576 5720415.421026 81.564421 388912.861281 5720419.103365 92.081000 388912.772822 5720419.993877 92.081000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1008_839626_170218">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo2a7f5c37-3fa3-4424-985f-49feadf66997">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600516">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600516">
									<gml:posList srsDimension="3">388905.479568 5720420.688563 82.717042 388904.388738 5720422.717946 81.564421 388903.648973 5720420.507691 82.721185 388905.479568 5720420.688563 82.717042 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_900_463850_419978">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeofb309662-946e-405d-a893-81f69e309ced">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600517">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600517">
									<gml:posList srsDimension="3">388927.097343 5720406.721350 75.763030 388926.103986 5720407.585161 74.663666 388925.526490 5720406.846457 73.861792 388924.990244 5720406.780777 73.198546 388925.019000 5720406.546000 73.202857 388925.249755 5720404.002806 73.151000 388927.097343 5720406.721350 75.763030 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1853_402302_373971">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo45e6d484-3ff3-4dbf-b355-4cbc66548525">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600518">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600518">
									<gml:posList srsDimension="3">388927.097343 5720406.721350 75.763030 388932.403283 5720407.166680 75.753698 388933.753842 5720409.516848 73.151000 388925.930323 5720408.872054 73.151000 388926.060613 5720408.770198 73.281892 388926.191971 5720407.697707 74.541260 388926.103986 5720407.585161 74.663666 388927.097343 5720406.721350 75.763030 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_951_894159_391560">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeocc833a54-8350-4cdc-95ac-316b2090e0db">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600519">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600519">
									<gml:posList srsDimension="3">388934.034000 5720407.055000 73.150572 388933.753842 5720409.516848 73.151000 388932.403283 5720407.166680 75.753698 388934.292681 5720404.777373 73.151000 388934.034000 5720407.055000 73.150572 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1769_718076_192790">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo87fd9fc7-c98e-4302-991d-578b53ebb448">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600520">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600520">
									<gml:posList srsDimension="3">388931.535000 5720404.541000 73.150831 388934.292681 5720404.777373 73.151000 388932.403283 5720407.166680 75.753698 388927.097343 5720406.721350 75.763030 388925.249755 5720404.002806 73.151000 388928.013000 5720404.240000 73.151520 388928.641614 5720404.293053 73.150713 388930.896000 5720404.486000 73.150559 388931.535000 5720404.541000 73.150831 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_604_804685_27174">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo0a7ffd11-85f7-404c-8d73-45b498e7c518">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600521">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600521">
									<gml:posList srsDimension="3">388924.293143 5720427.886041 82.931000 388927.181000 5720426.233000 77.911000 388927.447650 5720428.857988 77.911000 388924.293143 5720427.886041 82.931000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1328_29709_370206">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo05513595-c864-4f1e-afd4-766d56269b10">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600522">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600522">
									<gml:posList srsDimension="3">388924.426071 5720430.900901 77.911000 388924.293143 5720427.886041 82.931000 388927.447650 5720428.857988 77.911000 388924.426071 5720430.900901 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_877_96232_282728">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo4f0e34a9-7483-401c-a4da-13cb2827f8f0">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600523">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600523">
									<gml:posList srsDimension="3">388924.426071 5720430.900901 77.911000 388923.568000 5720430.639000 77.911000 388923.546613 5720430.079657 78.844041 388922.989657 5720429.457426 79.607326 388922.817369 5720429.457426 79.518572 388924.293143 5720427.886041 82.931000 388924.426071 5720430.900901 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1518_272528_419983">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo4ee385a1-054e-48fc-8348-f23f68c93563">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600524">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600524">
									<gml:posList srsDimension="3">388922.649425 5720425.892638 82.931000 388922.505884 5720427.064589 81.564421 388922.649425 5720425.892638 81.564421 388922.649425 5720425.892638 82.931000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_675_868185_252647">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo86180259-0d86-4f31-a292-44d360da45ef">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600525">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600525">
									<gml:posList srsDimension="3">388923.883041 5720425.985293 81.564421 388922.649425 5720425.892638 82.931000 388922.649425 5720425.892638 81.564421 388923.883041 5720425.985293 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1838_435960_209897">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeobaf92e1f-e492-4f3c-8870-d51e15056db6">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600526">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600526">
									<gml:posList srsDimension="3">388928.528744 5720412.547054 71.911000 388926.193128 5720411.246324 71.911000 388926.343980 5720409.761652 71.911000 388925.319837 5720409.657593 71.911000 388925.168985 5720411.142265 71.911000 388924.427393 5720411.376238 71.911000 388924.673117 5720409.370000 71.911000 388925.234209 5720409.416255 71.911000 388925.930323 5720408.872054 71.911000 388933.753842 5720409.516848 71.911000 388933.686000 5720410.113000 71.911000 388934.741000 5720410.208000 71.911000 388934.728000 5720410.328000 71.911000 388934.859000 5720410.340000 71.911000 388934.603000 5720413.104000 71.911000 388934.480375 5720413.092767 71.911000 388934.471403 5720413.099014 71.911000 388934.460000 5720413.233000 71.911000 388933.335000 5720413.133000 71.911000 388933.199000 5720414.848000 71.911000 388933.176000 5720415.135000 71.911000 388932.262711 5720414.643907 71.911000 388931.649711 5720415.783907 71.911000 388929.179558 5720415.598376 71.911000 388929.231685 5720414.904363 71.911000 388928.642142 5720414.860083 71.911000 388928.952804 5720413.498832 71.911000 388930.322198 5720412.888705 71.911000 388929.898137 5720411.936927 71.911000 388928.528744 5720412.547054 71.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_870_99994_32500">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo12a65059-6a91-4a74-837f-613bd03abf53">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600527">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600527">
									<gml:posList srsDimension="3">388925.653477 5720413.783632 83.081000 388928.485546 5720415.546249 77.911000 388925.286330 5720415.305959 81.564421 388923.958298 5720415.206212 83.081000 388925.653477 5720413.783632 83.081000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1182_280657_87677">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo970cfbc5-c565-4eb7-a324-88e62934249e">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600528">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600528">
									<gml:posList srsDimension="3">388925.653477 5720413.783632 83.081000 388924.427393 5720411.376238 77.911000 388925.703477 5720410.973632 77.911000 388925.653477 5720413.783632 83.081000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_832_337009_68276">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoe4478e5b-aabd-42e3-a0d3-cba25e543f6a">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600529">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600529">
									<gml:posList srsDimension="3">388929.097704 5720412.863914 77.911000 388928.642142 5720414.860083 77.911000 388928.537672 5720414.852237 78.080854 388928.485546 5720415.546249 77.911000 388925.653477 5720413.783632 83.081000 388929.097704 5720412.863914 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1030_604595_81281">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo25491f2f-cb6f-450d-ae40-60c71e166179">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600530">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600530">
									<gml:posList srsDimension="3">388923.958298 5720415.206212 83.081000 388924.095903 5720414.082719 81.564421 388924.427393 5720411.376238 77.911000 388925.653477 5720413.783632 83.081000 388923.958298 5720415.206212 83.081000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_143_744335_94541">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoff0fff48-6a9a-4c77-a74f-188692708bcd">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600531">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600531">
									<gml:posList srsDimension="3">388917.237000 5720433.787000 77.911000 388919.691727 5720431.846002 81.621000 388921.730560 5720434.165904 77.911000 388917.237000 5720433.787000 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_387_57146_142477">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeob490e950-2375-4d63-9809-425cb71529ec">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600532">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600532">
									<gml:posList srsDimension="3">388921.350751 5720430.924402 79.001491 388921.951000 5720431.595000 77.911000 388921.730560 5720434.165904 77.911000 388919.691727 5720431.846002 81.621000 388921.350751 5720430.542097 79.055839 388921.350751 5720430.924402 79.001491 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_850_821054_244404">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo4dd7b6b0-5035-48a9-be69-938ce2b28d7d">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600533">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600533">
									<gml:posList srsDimension="3">388921.350751 5720430.542097 79.055839 388919.691727 5720431.846002 81.621000 388917.652894 5720429.526100 81.564421 388921.531881 5720429.905461 78.371392 388921.350751 5720430.079657 78.677500 388921.350751 5720430.542097 79.055839 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_513_342925_312379">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo0389e9c9-bb28-4845-b2c0-0f1fd1c7cfa5">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600534">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600534">
									<gml:posList srsDimension="3">388917.237000 5720433.787000 77.911000 388917.652894 5720429.526100 81.564421 388919.691727 5720431.846002 81.621000 388917.237000 5720433.787000 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_722_893014_86502">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo083c3a9b-1c2b-4c7b-ba61-cf385e015209">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600535">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600535">
									<gml:posList srsDimension="3">388925.653477 5720413.783632 83.081000 388925.703477 5720410.973632 77.911000 388929.097704 5720412.863914 77.911000 388925.653477 5720413.783632 83.081000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1633_892465_85732">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeobeddae86-e15d-4281-86cd-6ca29a87d63d">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600536">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600536">
									<gml:posList srsDimension="3">388919.471757 5720410.891582 81.564421 388919.917000 5720406.330000 77.911000 388922.187096 5720408.868885 81.564421 388919.471757 5720410.891582 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_892_11297_212053">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo98b603df-7e2f-456f-be77-0a8c9359664d">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600537">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600537">
									<gml:posList srsDimension="3">388924.427393 5720411.376238 77.911000 388919.471757 5720410.891582 81.564421 388922.187096 5720408.868885 81.564421 388924.427393 5720411.376238 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1778_281763_273971">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoc7e4fbff-fa35-4912-8b7b-492fd14fe98a">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600538">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600538">
									<gml:posList srsDimension="3">388919.917000 5720406.330000 77.911000 388924.433287 5720406.731289 77.911000 388923.602749 5720407.380579 79.041311 388923.544287 5720407.857893 79.795556 388922.187096 5720408.868885 81.564421 388919.917000 5720406.330000 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_667_797698_390491">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoab642621-080d-4fd2-a42a-33f90a76fa10">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600539">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600539">
									<gml:posList srsDimension="3">388922.187096 5720408.868885 81.564421 388923.544287 5720407.857893 79.795556 388923.471390 5720408.453070 79.795556 388924.136871 5720409.304321 78.691609 388924.673117 5720409.370000 77.911000 388924.427393 5720411.376238 77.911000 388922.187096 5720408.868885 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1706_727761_70016">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo466bc736-5e20-4f9a-8c82-e5e3b1fd18c0">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600540">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600540">
									<gml:posList srsDimension="3">388924.831681 5720408.075389 84.886702 388925.526490 5720406.846457 79.886702 388926.191971 5720407.697707 79.886702 388924.831681 5720408.075389 84.886702 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1377_545757_357752">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo2a4ce1d8-a6e9-497c-b2cd-9f7b93f8b40c">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600541">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600541">
									<gml:posList srsDimension="3">388924.831681 5720408.075389 84.886702 388924.990244 5720406.780777 79.886702 388925.526490 5720406.846457 79.886702 388924.831681 5720408.075389 84.886702 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1554_315770_226510">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoddf925f9-c4d7-4dc1-96d1-ca4d8163f66a">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600542">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600542">
									<gml:posList srsDimension="3">388924.831681 5720408.075389 84.886702 388924.433287 5720406.731289 79.886702 388924.990244 5720406.780777 79.886702 388924.831681 5720408.075389 84.886702 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1254_282760_37313">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo7dd9b1ba-424a-4419-a565-bcf9841a8b64">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600543">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600543">
									<gml:posList srsDimension="3">388924.831681 5720408.075389 84.886702 388923.602749 5720407.380579 79.886702 388924.433287 5720406.731289 79.886702 388924.831681 5720408.075389 84.886702 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_391_582485_386491">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo5dbba808-00f5-4cdd-8686-a06db2b97a8e">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600544">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600544">
									<gml:posList srsDimension="3">388924.831681 5720408.075389 84.886702 388923.471390 5720408.453070 79.886702 388923.602749 5720407.380579 79.886702 388924.831681 5720408.075389 84.886702 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1823_807103_264258">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo49fa95bb-6de5-4b63-bb8c-b0ac9bc62d99">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600545">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600545">
									<gml:posList srsDimension="3">388924.831681 5720408.075389 84.886702 388924.136871 5720409.304321 79.886702 388923.843460 5720408.929004 79.886702 388923.471390 5720408.453070 79.886702 388924.831681 5720408.075389 84.886702 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_608_320626_327202">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo1f0a8d01-c6d3-4526-aea9-d0af474ca953">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600546">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600546">
									<gml:posList srsDimension="3">388924.673117 5720409.370000 79.886702 388924.831681 5720408.075389 84.886702 388925.234209 5720409.416255 79.886702 388924.673117 5720409.370000 79.886702 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_737_582646_38212">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo185759c4-c1c7-4835-bf05-5c1f1c744453">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600547">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600547">
									<gml:posList srsDimension="3">388924.831681 5720408.075389 84.886702 388924.673117 5720409.370000 79.886702 388924.136871 5720409.304321 79.886702 388924.831681 5720408.075389 84.886702 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1244_47724_55358">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo838085b8-0fc9-441b-b458-50876c9be66a">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600548">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600548">
									<gml:posList srsDimension="3">388926.060613 5720408.770198 79.886702 388924.831681 5720408.075389 84.886702 388926.191971 5720407.697707 79.886702 388926.060613 5720408.770198 79.886702 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_646_671767_285982">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo11f598da-0367-47ba-97cf-0e484a9ba286">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600549">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600549">
									<gml:posList srsDimension="3">388924.831681 5720408.075389 84.886702 388926.060613 5720408.770198 79.886702 388925.234209 5720409.416255 79.886702 388924.831681 5720408.075389 84.886702 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1765_364644_315991">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo46f75a80-2d71-41fc-869b-611c8ae5c381">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600550">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600550">
									<gml:posList srsDimension="3">388896.600829 5720417.428620 84.305755 388896.106414 5720416.752144 82.890466 388894.928000 5720408.530000 77.911000 388897.184602 5720411.380620 84.313068 388896.600829 5720417.428620 84.305755 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_878_750904_88138">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoaaae7529-6047-476e-80f3-e14e1c04d941">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600551">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600551">
									<gml:posList srsDimension="3">388894.928000 5720408.530000 77.911000 388896.106414 5720416.752144 82.890466 388890.938632 5720416.308777 83.027995 388893.718000 5720408.374000 77.911000 388894.928000 5720408.530000 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_387_608984_369854">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeob4059b83-e647-4ee3-b233-18752b4a1286">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600552">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600552">
									<gml:posList srsDimension="3">388890.932496 5720411.129769 84.316223 388893.718000 5720408.374000 77.911000 388890.938632 5720416.308777 83.027995 388890.378696 5720416.829294 84.323776 388890.932496 5720411.129769 84.316223 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1050_302285_6896">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo16260c27-e188-418a-b7d0-ad079cf8b44e">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600553">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600553">
									<gml:posList srsDimension="3">388921.699000 5720434.921000 75.415000 388921.032000 5720434.107000 76.891000 388921.730560 5720434.165904 76.210875 388921.788000 5720433.496000 76.891000 388922.442000 5720434.344000 75.389372 388921.699000 5720434.921000 75.415000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1679_701526_405925">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoe7000377-14e2-4376-a06e-31a50db9f3bf">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600554">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600554">
									<gml:posList srsDimension="3">388924.809000 5720430.642000 76.891000 388924.835000 5720431.613000 75.415000 388923.905000 5720431.696000 75.392907 388923.817000 5720430.715000 76.891000 388924.426071 5720430.900901 76.540973 388924.809000 5720430.642000 76.891000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_91_20606_129244">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo131b4676-00f9-4f2e-aa6b-8b56ae20ca17">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600555">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600555">
									<gml:posList srsDimension="3">388927.393000 5720428.320000 76.891000 388928.255000 5720428.680000 75.475631 388927.899000 5720429.533000 75.415000 388926.998000 5720429.162000 76.891000 388927.447650 5720428.857988 76.469456 388927.393000 5720428.320000 76.891000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_554_863690_398614">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoef969549-1e89-4abc-b240-59bd805b9fb3">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600556">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600556">
									<gml:posList srsDimension="3">388930.635000 5720426.583000 80.564421 388931.650000 5720426.272000 80.564421 388932.076000 5720427.521000 79.064421 388931.022000 5720427.806000 79.105685 388930.635000 5720426.583000 80.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1157_492891_381602">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoaee5baa0-92fe-4ca8-b9b9-2009c3598774">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600557">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600557">
									<gml:posList srsDimension="3">388934.230000 5720424.173000 80.564421 388934.741000 5720423.232000 80.564421 388935.893000 5720423.817000 79.064421 388935.433000 5720424.765000 79.008489 388934.230000 5720424.173000 80.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1151_242181_219236">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo29ac6d05-c4e0-4166-bf91-bea5b9c59f0f">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600558">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600558">
									<gml:posList srsDimension="3">388935.050000 5720419.889000 80.564421 388934.770000 5720418.899000 80.564421 388935.954277 5720418.504574 79.120893 388936.280000 5720419.477000 79.064421 388935.050000 5720419.889000 80.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1618_449119_417902">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo49a3ba1f-68e4-4721-b2b3-9c16028167bd">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600559">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600559">
									<gml:posList srsDimension="3">388931.649711 5720415.783907 80.564421 388932.262711 5720414.643907 79.064421 388933.176000 5720415.135000 79.064421 388932.563000 5720416.275000 80.564421 388931.649711 5720415.783907 80.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1631_660441_223474">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo00269362-fa69-472a-ba18-9f9196c5d072">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600560">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600560">
									<gml:posList srsDimension="3">388926.343980 5720409.761652 75.411000 388926.193128 5720411.246324 76.911000 388925.703477 5720410.973632 76.688060 388925.168985 5720411.142265 76.911000 388925.319837 5720409.657593 75.411000 388926.343980 5720409.761652 75.411000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_331_68978_356581">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoe32e2e25-9aec-4eef-a5b1-a1ae89d097e9">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600561">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600561">
									<gml:posList srsDimension="3">388933.810000 5720404.736000 72.150919 388934.233000 5720404.402000 71.150919 388934.637000 5720404.860000 71.176786 388934.244000 5720405.206000 72.150919 388934.292681 5720404.777373 71.543226 388933.810000 5720404.736000 72.150919 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_333_391815_337873">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoace8fa88-5a28-4492-b72e-39dbb1a20569">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600562">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600562">
									<gml:posList srsDimension="3">388931.535000 5720404.541000 72.150919 388930.896000 5720404.486000 72.150919 388930.948000 5720403.888000 71.150777 388931.586000 5720403.943000 71.150919 388931.535000 5720404.541000 72.150919 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1439_692489_134047">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo4320bd30-2b76-4d26-a374-203edcfd32c7">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600563">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600563">
									<gml:posList srsDimension="3">388928.641614 5720404.293053 72.150919 388928.013000 5720404.240000 72.150919 388928.065000 5720403.631000 71.150919 388928.694000 5720403.685000 71.152411 388928.641614 5720404.293053 72.150919 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_608_614403_209923">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo5e526a7a-360a-4b85-b2c7-9c5f263e1d55">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600564">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600564">
									<gml:posList srsDimension="3">388925.208000 5720404.463000 72.150919 388924.858000 5720404.030000 71.141036 388925.345000 5720403.636000 71.150919 388925.718000 5720404.043000 72.150919 388925.249755 5720404.002806 71.554699 388925.208000 5720404.463000 72.150919 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1620_284708_367326">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo56f9b290-e9d2-47a3-8426-b6c071f86a7b">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600565">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600565">
									<gml:posList srsDimension="3">388924.433287 5720406.731289 76.911000 388923.721000 5720406.668000 76.911000 388924.555000 5720405.982000 75.911000 388925.019000 5720406.546000 76.598763 388924.990244 5720406.780777 76.911000 388924.433287 5720406.731289 76.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_530_282596_298548">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo5108a82a-65bc-4994-b3c1-628e41dab19a">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600566">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600566">
									<gml:posList srsDimension="3">388910.543000 5720403.758000 79.064421 388911.452000 5720404.910000 80.564421 388910.694489 5720404.841180 80.069282 388910.628000 5720405.471000 80.564421 388909.729000 5720404.464000 79.193009 388910.543000 5720403.758000 79.064421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_564_223163_364802">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo1b5da27a-5b14-48cd-8b97-0077e0fcb722">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600567">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600567">
									<gml:posList srsDimension="3">388920.932000 5720405.462000 79.200832 388919.917000 5720406.330000 80.564421 388919.979980 5720405.684766 80.030168 388919.124000 5720405.607000 80.564421 388920.225000 5720404.638000 79.064421 388920.932000 5720405.462000 79.200832 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_644_335465_424747">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoe778810a-f176-4256-932e-0c23bbad908c">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600568">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600568">
									<gml:posList srsDimension="3">388906.009000 5720404.345000 75.415000 388906.756000 5720405.138000 76.911000 388906.146886 5720405.085615 76.322966 388906.082000 5720405.692000 76.911000 388905.394000 5720404.880000 75.446361 388906.009000 5720404.345000 75.415000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_390_891128_381543">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo820b9139-123e-4e4a-a0e2-99ee016b7906">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600569">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600569">
									<gml:posList srsDimension="3">388891.912000 5720427.061000 76.911000 388893.138000 5720427.167000 76.911000 388893.028000 5720428.443000 75.411000 388891.802000 5720428.337000 75.411000 388891.912000 5720427.061000 76.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_102_833995_111168">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeofdec956e-5a4b-4cd5-918a-98da6481be47">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600570">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600570">
									<gml:posList srsDimension="3">388884.644000 5720426.873000 75.620164 388885.929000 5720425.912000 76.911000 388885.829904 5720426.536039 76.448438 388886.849000 5720426.624000 76.911000 388885.413000 5720427.785000 75.411000 388884.644000 5720426.873000 75.620164 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_723_392036_85022">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo23b7eee7-82d0-42df-8c48-168c2b77544f">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600571">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600571">
									<gml:posList srsDimension="3">388930.322198 5720412.888705 75.411000 388928.952804 5720413.498832 76.911000 388929.097704 5720412.863914 76.520027 388928.528744 5720412.547054 76.911000 388929.898137 5720411.936927 75.411000 388930.322198 5720412.888705 75.411000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1756_758454_411033">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo451405a3-387b-4a19-8308-adc58cedae02">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600572">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600572">
									<gml:posList srsDimension="3">388887.798000 5720408.749000 76.911000 388886.682000 5720407.302000 75.332927 388887.646000 5720406.535000 75.411000 388888.756000 5720407.865000 76.911000 388887.921349 5720407.779382 76.365735 388887.798000 5720408.749000 76.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_318_96831_214437">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo9973eaca-caab-4d40-a4cf-21ee566b4586">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600573">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600573">
									<gml:posList srsDimension="3">388894.928000 5720408.530000 76.911000 388893.718000 5720408.374000 76.911000 388893.828000 5720407.164000 75.464918 388895.043000 5720407.275000 75.411000 388894.928000 5720408.530000 76.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1848_862587_396939">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo3587a56c-be6d-41d0-946c-b1bdc8404ee1">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600574">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600574">
									<gml:posList srsDimension="3">388901.147415 5720409.105306 76.911000 388899.947000 5720408.991000 76.911000 388900.062000 5720407.736000 75.411000 388901.267000 5720407.846000 75.405381 388901.236000 5720408.175000 75.798704 388901.147415 5720409.105306 76.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1136_357378_196244">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeocd973cc7-6211-48d5-a247-79a1bc2d7f96">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600575">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600575">
									<gml:posList srsDimension="3">388896.439089 5720419.090664 84.322945 388896.991079 5720419.849860 82.736252 388898.142000 5720427.599000 77.911000 388896.063228 5720422.984658 84.318222 388896.166627 5720421.913430 84.319521 388896.439089 5720419.090664 84.322945 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1234_836111_233339">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo84ff75a0-0b66-4a2a-8809-638ca4c39705">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600576">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600576">
									<gml:posList srsDimension="3">388903.852558 5720428.142770 77.911000 388899.375230 5720427.716430 77.911000 388902.083225 5720423.000783 84.312921 388904.130097 5720425.330937 81.564421 388903.852558 5720428.142770 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1864_453693_10460">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeod0091324-38a8-461e-83e4-ab25745e7d0a">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600577">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600577">
									<gml:posList srsDimension="3">388902.083225 5720423.000783 84.312921 388899.375230 5720427.716430 77.911000 388901.778063 5720420.322837 82.725419 388902.403767 5720419.673752 84.323510 388902.083225 5720423.000783 84.312921 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1978_773048_426506">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo025a8eca-5255-4f8c-9909-1981f79aad8b">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600578">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600578">
									<gml:posList srsDimension="3">388902.695000 5720432.484000 75.415610 388903.503000 5720431.791000 76.891000 388903.453187 5720432.457419 76.296480 388904.013000 5720432.507000 76.891000 388903.178000 5720433.162000 75.415686 388903.149533 5720433.122040 75.415681 388902.695000 5720432.484000 75.415610 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_256_816006_415760">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo65ebb415-121a-4abd-9d79-a897a8620a8e">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600579">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600579">
									<gml:posList srsDimension="3">388896.991079 5720419.849860 82.736252 388901.778063 5720420.322837 82.725419 388899.375230 5720427.716430 77.911000 388898.142000 5720427.599000 77.911000 388896.991079 5720419.849860 82.736252 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_538_260608_1131">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo2fc72064-7703-4baa-b4aa-e23ab6a940eb">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600580">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600580">
									<gml:posList srsDimension="3">388899.678638 5720427.745321 71.681000 388903.852558 5720428.142770 71.681000 388903.713711 5720429.543522 73.507225 388901.444113 5720429.347078 73.532631 388899.678638 5720427.745321 71.681000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1210_431373_308124">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoac93d9b6-9123-48d9-8465-dcb66253260b">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600581">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600581">
									<gml:posList srsDimension="3">388899.375230 5720427.716430 71.681000 388899.678638 5720427.745321 71.681000 388899.583000 5720428.661000 71.681000 388899.289000 5720428.622000 71.681000 388899.375230 5720427.716430 71.681000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_936_187247_216881">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo239aa9b9-5cfc-4357-8cc3-972c9ff355f3">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600582">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600582">
									<gml:posList srsDimension="3">388899.678638 5720427.745321 71.681000 388901.444113 5720429.347078 73.532631 388899.341000 5720430.978000 71.681000 388899.583000 5720428.661000 71.681000 388899.678638 5720427.745321 71.681000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1722_511395_123298">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoe2305aeb-1881-49ef-bd52-c1abebf46638">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600583">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600583">
									<gml:posList srsDimension="3">388893.138000 5720427.167000 77.911000 388896.063228 5720422.984658 84.318222 388898.142000 5720427.599000 77.911000 388893.138000 5720427.167000 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1874_238865_422622">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoa0a424cb-b59f-49e4-840c-6b08fa18298c">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600584">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600584">
									<gml:posList srsDimension="3">388885.829904 5720426.536039 77.911000 388889.762751 5720422.984658 84.292126 388891.912000 5720427.061000 77.911000 388885.829904 5720426.536039 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_247_269308_366365">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoc6de30a9-aed0-4919-8920-1082f0836f9c">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600585">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600585">
									<gml:posList srsDimension="3">388887.921349 5720407.779382 77.911000 388893.718000 5720408.374000 77.911000 388890.932496 5720411.129769 84.316223 388887.921349 5720407.779382 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1315_320672_401013">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo6ba80447-b867-43c4-866d-f5af316cba89">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600586">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600586">
									<gml:posList srsDimension="3">388920.175996 5720426.935924 87.881000 388917.424608 5720422.713744 87.881000 388919.962818 5720423.434818 84.925858 388922.229415 5720424.877214 83.030376 388920.175996 5720426.935924 87.881000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_619_721505_74435">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo405933ec-3d65-4b51-a27c-ab20f92af6f6">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600587">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600587">
									<gml:posList srsDimension="3">388922.229415 5720424.877214 83.030376 388919.962818 5720423.434818 84.925858 388919.365872 5720422.407006 86.390471 388917.154273 5720420.059300 89.641000 388927.585853 5720420.842806 89.641000 388927.181000 5720426.233000 81.564421 388923.883041 5720425.985293 81.564421 388922.649425 5720425.892638 81.564421 388922.229415 5720424.877214 83.030376 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_994_671865_183124">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo06e7a833-54c0-4794-9a6f-293b17f5a25d">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600588">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600588">
									<gml:posList srsDimension="3">388922.422376 5720430.546713 89.971000 388923.581000 5720430.979000 84.831000 388922.847000 5720431.636000 84.831000 388922.422376 5720430.546713 89.971000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_857_671720_395015">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeobab806e2-5f77-4ec0-a549-fe902f4d718b">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600589">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600589">
									<gml:posList srsDimension="3">388922.989657 5720429.457426 84.831000 388922.422376 5720430.546713 89.971000 388921.997751 5720429.457426 84.831000 388922.989657 5720429.457426 84.831000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1581_588042_275116">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo6b15d624-c8f0-4818-9285-44957b65ed26">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600590">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600590">
									<gml:posList srsDimension="3">388923.546613 5720430.079657 84.831000 388922.422376 5720430.546713 89.971000 388922.989657 5720429.457426 84.831000 388923.546613 5720430.079657 84.831000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1330_118276_26660">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeofcac5463-1374-4466-8c20-8dbc4ccc524a">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600591">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600591">
									<gml:posList srsDimension="3">388923.581000 5720430.979000 84.831000 388922.422376 5720430.546713 89.971000 388923.546613 5720430.079657 84.831000 388923.581000 5720430.979000 84.831000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1961_585964_166138">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo00b07b2d-f8e4-471d-934c-987b16d96488">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600592">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600592">
									<gml:posList srsDimension="3">388921.951000 5720431.595000 84.831000 388922.422376 5720430.546713 89.971000 388922.847000 5720431.636000 84.831000 388921.951000 5720431.595000 84.831000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1210_766082_15511">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo8c47631b-425c-4710-b8fd-2e98ac95fd43">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600593">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600593">
									<gml:posList srsDimension="3">388921.350751 5720430.924402 84.831000 388922.422376 5720430.546713 89.971000 388921.951000 5720431.595000 84.831000 388921.350751 5720430.924402 84.831000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_457_757660_228357">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo2828f06f-d91f-4bdc-a2da-dfc76148036d">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600594">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600594">
									<gml:posList srsDimension="3">388921.350751 5720430.079657 84.831000 388922.422376 5720430.546713 89.971000 388921.350751 5720430.924402 84.831000 388921.350751 5720430.079657 84.831000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_852_438889_353502">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo12f4c17f-d070-4d26-8d0b-9fe00e5be34d">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600595">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600595">
									<gml:posList srsDimension="3">388921.997751 5720429.457426 84.831000 388922.422376 5720430.546713 89.971000 388921.350751 5720430.079657 84.831000 388921.997751 5720429.457426 84.831000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1341_320736_234052">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeod596b71b-7e66-43e3-a2b7-8f6e5a658a1b">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600596">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600596">
									<gml:posList srsDimension="3">388922.505884 5720427.064589 81.564421 388922.212810 5720429.457426 81.564421 388921.997751 5720429.457426 82.143552 388921.887949 5720429.563025 82.404409 388920.175996 5720426.935924 87.881000 388922.229415 5720424.877214 83.030376 388922.649425 5720425.892638 81.564421 388922.505884 5720427.064589 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1104_728581_344636">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo334dc11d-e925-4496-8d6e-d712b27dc007">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600597">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600597">
									<gml:posList srsDimension="3">388927.181000 5720426.233000 77.911000 388924.293143 5720427.886041 82.931000 388922.649425 5720425.892638 82.931000 388923.883041 5720425.985293 81.564421 388927.181000 5720426.233000 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_364_464236_31137">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo417106b2-2d31-4151-a841-0e3ddd024d58">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600598">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600598">
									<gml:posList srsDimension="3">388914.497728 5720420.998675 92.081000 388913.348973 5720420.886328 92.081006 388912.772822 5720419.993877 92.081000 388912.861281 5720419.103365 92.081000 388913.567540 5720418.340684 92.081000 388914.762329 5720418.457533 92.081000 388915.364442 5720419.355956 92.081000 388915.257640 5720420.227948 92.081000 388914.497728 5720420.998675 92.081000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_686_619185_74422">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo2eaf9dec-631a-43cf-a28d-d870063d83e4">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600599">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600599">
									<gml:posList srsDimension="3">388924.293143 5720427.886041 82.931000 388922.817369 5720429.457426 79.518572 388922.212810 5720429.457426 78.774205 388922.505884 5720427.064589 81.564421 388922.649425 5720425.892638 82.931000 388924.293143 5720427.886041 82.931000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:GroundSurface gml:id="UUID_GroundSurface_633_518907_146470">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeodd936791-b747-41d6-9889-0f11819da0e6">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600600">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600600">
									<gml:posList srsDimension="3">388905.394000 5720404.880000 66.415000 388906.082000 5720405.692000 66.415000 388906.027000 5720406.206000 66.415000 388901.853000 5720405.758000 66.415000 388901.623000 5720408.229000 66.415000 388901.236000 5720408.175000 66.415000 388901.267000 5720407.846000 66.415000 388900.062000 5720407.736000 66.415000 388899.947000 5720408.991000 66.415000 388894.928000 5720408.530000 66.415000 388895.043000 5720407.275000 66.415000 388893.828000 5720407.164000 66.415000 388893.718000 5720408.374000 66.415000 388888.756000 5720407.865000 66.415000 388887.646000 5720406.535000 66.415000 388886.682000 5720407.302000 66.415000 388887.798000 5720408.749000 66.415000 388887.719000 5720409.370000 66.415000 388882.672000 5720408.732000 66.415000 388882.265000 5720412.894000 66.415000 388882.264995 5720412.894053 66.415000 388881.179000 5720412.724000 66.415000 388880.044000 5720419.978000 66.415000 388881.130000 5720420.148000 66.415000 388880.468000 5720424.332000 66.415000 388886.040000 5720425.213000 66.415000 388885.929000 5720425.912000 66.415000 388884.644000 5720426.873000 66.415000 388885.413000 5720427.785000 66.415000 388886.849000 5720426.624000 66.415000 388891.912000 5720427.061000 66.415000 388891.802000 5720428.337000 66.415000 388893.028000 5720428.443000 66.415000 388893.138000 5720427.167000 66.415000 388898.142000 5720427.599000 66.415000 388898.032000 5720428.875000 66.415000 388899.258000 5720428.981000 66.415000 388899.289000 5720428.622000 66.415000 388899.583000 5720428.661000 66.415000 388899.341000 5720430.978000 66.415000 388903.540000 5720431.296000 66.415000 388903.503000 5720431.791000 66.415000 388902.695000 5720432.484000 66.415000 388903.178000 5720433.162000 66.415000 388904.013000 5720432.507000 66.415000 388907.739000 5720432.837000 66.415000 388906.808000 5720433.687000 66.415000 388907.357893 5720434.310000 66.415000 388907.682979 5720434.310000 66.415000 388908.524000 5720433.570000 66.415000 388916.348000 5720434.310000 66.415000 388917.196000 5720435.364000 66.415000 388918.000000 5720434.717000 66.415000 388917.237000 5720433.787000 66.415000 388921.032000 5720434.107000 66.415000 388921.699000 5720434.921000 66.415000 388922.442000 5720434.344000 66.415000 388921.788000 5720433.496000 66.415000 388921.951000 5720431.595000 66.415000 388922.847000 5720431.636000 66.415000 388923.581000 5720430.979000 66.415000 388923.568000 5720430.639000 66.415000 388923.817000 5720430.715000 66.415000 388923.905000 5720431.696000 66.415000 388924.835000 5720431.613000 66.415000 388924.809000 5720430.642000 66.415000 388926.998000 5720429.162000 66.415000 388927.899000 5720429.533000 66.415000 388928.255000 5720428.680000 66.415000 388927.393000 5720428.320000 66.415000 388927.181000 5720426.233000 66.415000 388930.635000 5720426.583000 66.415000 388931.022000 5720427.806000 66.415000 388932.076000 5720427.521000 66.415000 388931.650000 5720426.272000 66.415000 388934.230000 5720424.173000 66.415000 388935.433000 5720424.765000 66.415000 388935.893000 5720423.817000 66.415000 388934.741000 5720423.232000 66.415000 388935.050000 5720419.889000 66.415000 388936.280000 5720419.477000 66.415000 388935.954277 5720418.504574 66.415000 388934.770000 5720418.899000 66.415000 388932.563000 5720416.275000 66.415000 388933.176000 5720415.135000 66.415000 388933.199000 5720414.848000 66.415000 388934.329000 5720414.937000 66.415000 388934.460000 5720413.233000 66.415000 388934.471403 5720413.099014 66.415000 388934.480375 5720413.092767 66.415000 388934.603000 5720413.104000 66.415000 388934.859000 5720410.340000 66.415000 388934.728000 5720410.328000 66.415000 388934.741000 5720410.208000 66.415000 388935.033000 5720407.169000 66.415000 388934.034000 5720407.055000 66.415000 388934.244000 5720405.206000 66.415000 388934.637000 5720404.860000 66.415000 388934.233000 5720404.402000 66.415000 388933.810000 5720404.736000 66.415000 388931.535000 5720404.541000 66.415000 388931.586000 5720403.943000 66.415000 388930.948000 5720403.888000 66.415000 388930.896000 5720404.486000 66.415000 388928.641614 5720404.293053 66.415000 388928.694000 5720403.685000 66.415000 388928.065000 5720403.631000 66.415000 388928.013000 5720404.240000 66.415000 388925.718000 5720404.043000 66.415000 388925.345000 5720403.636000 66.415000 388924.858000 5720404.030000 66.415000 388925.208000 5720404.463000 66.415000 388925.019000 5720406.546000 66.415000 388924.555000 5720405.982000 66.415000 388923.721000 5720406.668000 66.415000 388919.917000 5720406.330000 66.415000 388920.932000 5720405.462000 66.415000 388920.225000 5720404.638000 66.415000 388919.124000 5720405.607000 66.415000 388916.348000 5720405.354801 66.415000 388911.452000 5720404.910000 66.415000 388910.543000 5720403.758000 66.415000 388909.729000 5720404.464000 66.415000 388910.628000 5720405.471000 66.415000 388906.756000 5720405.138000 66.415000 388906.009000 5720404.345000 66.415000 388905.394000 5720404.880000 66.415000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:GroundSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_23_777665_357085">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo4366ba0e-36d0-4f23-8bb1-7584f53835e8">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600601">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600601">
									<gml:posList srsDimension="3">388887.297000 5720413.682000 77.911000 388886.727860 5720417.306207 86.201000 388886.158000 5720420.935000 77.911000 388886.158000 5720420.935000 94.841000 388886.727500 5720417.308500 98.782158 388887.297000 5720413.682000 94.841000 388887.297000 5720413.682000 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_71_699752_380807">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo34c79f7c-4635-40f0-8ef3-fbb7c7fdbc2c">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600602">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600602">
									<gml:posList srsDimension="3">388905.696673 5720409.538494 81.564421 388905.556072 5720410.956941 81.564421 388905.696673 5720409.538494 77.911000 388905.696673 5720409.538494 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_234_844790_112576">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo82d18641-3b22-4961-a7b7-21f6d65ac2b2">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600603">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600603">
									<gml:posList srsDimension="3">388917.178286 5720434.388529 81.564421 388917.652894 5720429.526100 81.564421 388917.237000 5720433.787000 77.911000 388917.237000 5720433.787000 80.621000 388917.178286 5720434.388529 80.023974 388917.178286 5720434.388529 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1935_495746_287550">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo898447b6-b825-4e45-865f-fbd84e0e02f5">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600604">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600604">
									<gml:posList srsDimension="3">388910.694489 5720404.841180 81.564421 388919.979980 5720405.684766 81.564421 388915.439231 5720405.978917 89.154559 388910.694489 5720404.841180 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1356_288045_301377">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeof5f156b7-71e9-4bd3-b93c-5ec31f5ca8bb">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600605">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600605">
									<gml:posList srsDimension="3">388903.851783 5720428.142688 81.564421 388903.852558 5720428.142770 77.911000 388904.130097 5720425.330937 81.564421 388903.851783 5720428.142688 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1021_383590_315449">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo56bdc9fa-e309-4138-a158-c854b7ddf34d">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600606">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600606">
									<gml:posList srsDimension="3">388910.628000 5720405.471000 77.911000 388910.628000 5720405.471000 80.564421 388910.694489 5720404.841180 80.069282 388910.694489 5720404.841180 81.564421 388910.151977 5720409.980118 81.564421 388910.628000 5720405.471000 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_194_483374_177907">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo9a698ba4-daa6-477e-ad8a-124f646a8523">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600607">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600607">
									<gml:posList srsDimension="3">388905.696673 5720409.538494 81.564421 388905.696673 5720409.538494 77.911000 388910.151977 5720409.980118 81.564421 388905.696673 5720409.538494 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1697_380329_127553">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo85d572a3-4656-4dfc-a462-8b92232fdf8d">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600608">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600608">
									<gml:posList srsDimension="3">388886.040000 5720425.213000 77.911000 388886.040000 5720425.213000 75.861000 388886.158000 5720420.935000 77.911000 388886.040000 5720425.213000 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1478_337852_414870">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo01db672e-c4b3-43bb-8ca1-3de6fc5a3d4b">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600609">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600609">
									<gml:posList srsDimension="3">388887.719000 5720409.370000 77.911000 388887.297000 5720413.682000 77.911000 388887.719000 5720409.370000 75.861000 388887.719000 5720409.370000 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1328_770708_335996">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo4b730c1d-a48b-4596-a350-784b860f04e8">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600610">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600610">
									<gml:posList srsDimension="3">388934.460000 5720413.233000 69.511000 388933.335000 5720413.133000 69.511000 388933.335000 5720413.133000 71.911000 388934.460000 5720413.233000 71.911000 388934.460000 5720413.233000 69.511000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1089_895781_228185">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeobcb68b13-6ae3-4343-ac0b-a8c1ead5ce36">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600611">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600611">
									<gml:posList srsDimension="3">388934.741000 5720410.208000 68.121000 388934.741000 5720410.208000 71.911000 388933.686000 5720410.113000 71.911000 388933.686000 5720410.113000 68.121000 388934.741000 5720410.208000 68.121000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_670_344452_265337">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeob921b8b4-79c9-4755-a395-d625eb912a74">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600612">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600612">
									<gml:posList srsDimension="3">388933.335000 5720413.133000 69.511000 388933.199000 5720414.848000 69.511000 388933.199000 5720414.848000 71.911000 388933.335000 5720413.133000 71.911000 388933.335000 5720413.133000 69.511000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1496_120733_396640">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo1a631607-d560-4e9e-b881-7447d3445e7f">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600613">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600613">
									<gml:posList srsDimension="3">388935.050000 5720419.889000 80.564421 388935.050000 5720419.889000 81.564421 388934.770000 5720418.899000 81.564421 388934.770000 5720418.899000 80.564421 388935.050000 5720419.889000 80.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_165_550146_104279">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo78972f6e-0824-4eae-9b13-d0af50bc91ae">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600614">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600614">
									<gml:posList srsDimension="3">388932.262711 5720414.643907 71.911000 388932.262711 5720414.643907 79.064421 388931.649711 5720415.783907 80.564421 388931.649711 5720415.783907 71.911000 388932.262711 5720414.643907 71.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1731_865685_10400">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo9718a5d7-ca27-4322-8001-cdc88ceb9500">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600615">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600615">
									<gml:posList srsDimension="3">388933.176000 5720415.135000 71.911000 388933.176000 5720415.135000 79.064421 388932.262711 5720414.643907 79.064421 388932.262711 5720414.643907 71.911000 388933.176000 5720415.135000 71.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1226_120591_390587">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeod22c4641-b767-4544-83e7-caa14b69f91a">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600616">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600616">
									<gml:posList srsDimension="3">388928.485546 5720415.546249 81.564421 388925.286330 5720415.305959 81.564421 388928.485546 5720415.546249 77.911000 388928.485546 5720415.546249 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1593_525093_195251">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo44ae5330-922b-4441-8c54-4e7c41c772c7">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600617">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600617">
									<gml:posList srsDimension="3">388932.563000 5720416.275000 80.564421 388932.563000 5720416.275000 81.564421 388931.649711 5720415.783907 81.564421 388931.649711 5720415.783907 80.564421 388932.563000 5720416.275000 80.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1406_28946_161362">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoed773136-f12f-4d22-9dcb-8210714369da">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600618">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600618">
									<gml:posList srsDimension="3">388931.649711 5720415.783907 80.564421 388931.649711 5720415.783907 81.564421 388929.179558 5720415.598376 81.564421 388929.179558 5720415.598376 71.911000 388931.649711 5720415.783907 71.911000 388931.649711 5720415.783907 80.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_211_812185_313474">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo9a482bc7-6c6a-4e47-805d-e9b90a82c540">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600619">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600619">
									<gml:posList srsDimension="3">388928.485546 5720415.546249 81.564421 388928.485546 5720415.546249 77.911000 388928.537672 5720414.852237 78.080854 388928.537672 5720414.852237 82.731000 388928.485546 5720415.546249 82.731000 388928.485546 5720415.546249 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1079_877462_112678">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo3ce7e121-46ec-4260-8ee8-48b3ad987c97">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600620">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600620">
									<gml:posList srsDimension="3">388928.537672 5720414.852237 82.731000 388928.537672 5720414.852237 78.080854 388928.642142 5720414.860083 77.911000 388928.642142 5720414.860083 71.911000 388929.231685 5720414.904363 71.911000 388929.231685 5720414.904363 82.731000 388928.537672 5720414.852237 82.731000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_40_645472_165028">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoa6443ab4-fe0c-4a4f-9c35-23fbb03d2b5d">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600621">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600621">
									<gml:posList srsDimension="3">388929.231685 5720414.904363 71.911000 388929.179558 5720415.598376 71.911000 388929.179558 5720415.598376 81.564421 388929.179558 5720415.598376 82.731000 388929.231685 5720414.904363 82.731000 388929.231685 5720414.904363 71.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_56_697012_418887">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoaaed1525-ea12-446a-84ac-85f93bd15538">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600622">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600622">
									<gml:posList srsDimension="3">388929.179558 5720415.598376 81.564421 388928.485546 5720415.546249 81.564421 388928.485546 5720415.546249 82.731000 388929.179558 5720415.598376 82.731000 388929.179558 5720415.598376 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1038_452025_420510">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeob7ceee56-8171-49f4-afc6-8675b3df7dde">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600623">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600623">
									<gml:posList srsDimension="3">388912.663708 5720433.340614 89.330896 388917.178286 5720434.388529 81.564421 388907.670144 5720433.489242 81.564421 388912.663708 5720433.340614 89.330896 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1572_129179_45285">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo9a3e60eb-fdc1-462d-b74a-0b5c910e47b9">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600624">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600624">
									<gml:posList srsDimension="3">388903.540000 5720431.296000 77.911000 388903.540000 5720431.296000 71.681000 388903.713711 5720429.543522 73.507225 388903.852558 5720428.142770 71.681000 388903.852558 5720428.142770 77.911000 388903.540000 5720431.296000 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1956_664666_135145">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo27702efb-8611-4bdc-94cf-1c81845ab920">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600625">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600625">
									<gml:posList srsDimension="3">388905.696673 5720409.538494 77.911000 388905.696673 5720409.538494 71.791000 388905.834618 5720408.146839 73.850883 388906.027000 5720406.206000 71.791000 388906.027000 5720406.206000 77.911000 388905.696673 5720409.538494 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_961_420425_202563">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo2b68fd5e-8f65-49e7-8439-cf1070312f58">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600626">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600626">
									<gml:posList srsDimension="3">388921.531881 5720429.905461 81.564421 388921.531881 5720429.905461 78.371392 388917.652894 5720429.526100 81.564421 388921.531881 5720429.905461 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_942_403721_294344">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoe1ee71aa-47b4-462d-aea4-0661b4ae721a">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600627">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600627">
									<gml:posList srsDimension="3">388925.234209 5720409.416255 79.886702 388925.234209 5720409.416255 71.911000 388924.673117 5720409.370000 71.911000 388924.673117 5720409.370000 77.911000 388924.673117 5720409.370000 79.886702 388925.234209 5720409.416255 79.886702 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_40_838223_403534">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo519a1a43-83f0-4618-983c-ea259c8604a3">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600628">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600628">
									<gml:posList srsDimension="3">388924.433287 5720406.731289 79.886702 388924.433287 5720406.731289 77.911000 388924.433287 5720406.731289 76.911000 388924.990244 5720406.780777 76.911000 388924.990244 5720406.780777 79.886702 388924.433287 5720406.731289 79.886702 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1690_765261_426908">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo6c484ffe-7859-401f-b260-4309a020c1bf">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600629">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600629">
									<gml:posList srsDimension="3">388923.958298 5720415.206212 83.081000 388925.286330 5720415.305959 81.564421 388923.958298 5720415.206212 81.564421 388923.958298 5720415.206212 83.081000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_349_893411_319540">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo8d7c2868-cb6b-425d-8f71-d14c66b1c56d">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600630">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600630">
									<gml:posList srsDimension="3">388931.535000 5720404.541000 72.150919 388931.535000 5720404.541000 73.150831 388930.896000 5720404.486000 73.150559 388930.896000 5720404.486000 72.150919 388931.535000 5720404.541000 72.150919 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_334_132434_214064">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo1da90326-5ec2-403c-9943-424288e17b58">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600631">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600631">
									<gml:posList srsDimension="3">388925.526490 5720406.846457 73.861792 388926.103986 5720407.585161 74.663666 388926.191971 5720407.697707 74.541260 388926.191971 5720407.697707 79.886702 388925.526490 5720406.846457 79.886702 388925.526490 5720406.846457 73.861792 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1680_798929_21040">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoc846d73c-83bd-4f75-8ee5-a66eb12514f7">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600632">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600632">
									<gml:posList srsDimension="3">388926.191971 5720407.697707 74.541260 388926.060613 5720408.770198 73.281892 388926.060613 5720408.770198 79.886702 388926.191971 5720407.697707 79.886702 388926.191971 5720407.697707 74.541260 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_821_409927_425151">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo19bc67f1-778c-4d2a-95ba-2aa2bc0c1127">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600633">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600633">
									<gml:posList srsDimension="3">388924.990244 5720406.780777 79.886702 388924.990244 5720406.780777 76.911000 388924.990244 5720406.780777 73.198546 388925.526490 5720406.846457 73.861792 388925.526490 5720406.846457 79.886702 388924.990244 5720406.780777 79.886702 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_821_814225_424190">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo21ed8016-7c8e-4a33-b20a-18f7386e868e">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600634">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600634">
									<gml:posList srsDimension="3">388933.753842 5720409.516848 71.911000 388925.930323 5720408.872054 71.911000 388925.930323 5720408.872054 73.151000 388933.753842 5720409.516848 73.151000 388933.753842 5720409.516848 71.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_738_427527_46691">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo88d3616a-02f4-494c-8745-34d36979d5f0">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600635">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600635">
									<gml:posList srsDimension="3">388926.060613 5720408.770198 79.886702 388926.060613 5720408.770198 73.281892 388925.930323 5720408.872054 73.151000 388925.930323 5720408.872054 71.911000 388925.234209 5720409.416255 71.911000 388925.234209 5720409.416255 79.886702 388926.060613 5720408.770198 79.886702 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_356_202809_409710">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo8d59ff01-7351-4240-878d-3d4fe58b3ff6">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600636">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600636">
									<gml:posList srsDimension="3">388928.641614 5720404.293053 72.150919 388928.641614 5720404.293053 73.150713 388928.013000 5720404.240000 73.151520 388928.013000 5720404.240000 72.150919 388928.641614 5720404.293053 72.150919 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1168_784151_177658">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo85d0c7b5-5780-44e1-ba21-ddb9f85d0545">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600637">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600637">
									<gml:posList srsDimension="3">388933.753842 5720409.516848 71.911000 388933.753842 5720409.516848 73.151000 388934.034000 5720407.055000 73.150572 388934.034000 5720407.055000 68.121000 388933.686000 5720410.113000 68.121000 388933.686000 5720410.113000 71.911000 388933.753842 5720409.516848 71.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1268_287235_169021">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeode7f22ff-aad9-458a-a28b-a7cf423389c4">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600638">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600638">
									<gml:posList srsDimension="3">388927.181000 5720426.233000 81.564421 388927.181000 5720426.233000 77.911000 388923.883041 5720425.985293 81.564421 388927.181000 5720426.233000 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_981_116757_33071">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo4be6fd38-e47e-4abb-9f69-5302394970c3">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600639">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600639">
									<gml:posList srsDimension="3">388923.958298 5720415.206212 83.081000 388923.958298 5720415.206212 81.564421 388924.095903 5720414.082719 81.564421 388923.958298 5720415.206212 83.081000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1610_211871_142803">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoa2871162-dc75-450e-9381-44fb27c67fae">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600640">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600640">
									<gml:posList srsDimension="3">388924.673117 5720409.370000 77.911000 388924.673117 5720409.370000 71.911000 388924.427393 5720411.376238 71.911000 388924.427393 5720411.376238 77.911000 388924.673117 5720409.370000 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1981_399459_79429">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoe17fb5be-6b3e-4da2-bd97-4758327fe463">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600641">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600641">
									<gml:posList srsDimension="3">388924.427393 5720411.376238 77.911000 388924.095903 5720414.082719 81.564421 388924.427393 5720411.376238 81.564421 388924.427393 5720411.376238 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1512_146076_187216">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeof6cc63e7-13cd-4d3a-9809-308f73872341">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600642">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600642">
									<gml:posList srsDimension="3">388924.427393 5720411.376238 81.564421 388919.471757 5720410.891582 81.564421 388924.427393 5720411.376238 77.911000 388924.427393 5720411.376238 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1957_673435_108874">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoa8214afd-9f0a-453a-998d-03d80fb17d7a">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600643">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600643">
									<gml:posList srsDimension="3">388923.602749 5720407.380579 79.886702 388923.602749 5720407.380579 79.041311 388924.433287 5720406.731289 77.911000 388924.433287 5720406.731289 79.886702 388923.602749 5720407.380579 79.886702 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_489_110187_364388">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo3aed8ade-e561-406a-81ca-c7dcbc0ae32d">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600644">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600644">
									<gml:posList srsDimension="3">388923.471390 5720408.453070 79.886702 388923.471390 5720408.453070 79.795556 388923.544287 5720407.857893 79.795556 388923.602749 5720407.380579 79.041311 388923.602749 5720407.380579 79.886702 388923.471390 5720408.453070 79.886702 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1609_296222_7965">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoaadaec65-5192-4eaf-be16-89a104ac60e9">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600645">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600645">
									<gml:posList srsDimension="3">388924.136871 5720409.304321 79.886702 388924.136871 5720409.304321 78.691609 388923.471390 5720408.453070 79.795556 388923.471390 5720408.453070 79.886702 388923.843460 5720408.929004 79.886702 388924.136871 5720409.304321 79.886702 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1954_80050_116359">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo266e3549-55a2-4e64-9508-dcccaa2527ed">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600646">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600646">
									<gml:posList srsDimension="3">388924.673117 5720409.370000 79.886702 388924.673117 5720409.370000 77.911000 388924.136871 5720409.304321 78.691609 388924.136871 5720409.304321 79.886702 388924.673117 5720409.370000 79.886702 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_811_135354_160680">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo0051a0f8-0af8-4d9a-a291-a887ac69be49">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600647">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600647">
									<gml:posList srsDimension="3">388934.230000 5720424.173000 81.564421 388934.741000 5720423.232000 81.564421 388934.741000 5720423.232000 80.564421 388934.230000 5720424.173000 80.564421 388934.230000 5720424.173000 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_137_440620_234502">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo6feaaa63-1793-4c4e-b673-8c9d19ceb704">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600648">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600648">
									<gml:posList srsDimension="3">388930.635000 5720426.583000 81.564421 388931.650000 5720426.272000 81.564421 388931.650000 5720426.272000 80.564421 388930.635000 5720426.583000 80.564421 388930.635000 5720426.583000 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_171_557365_333097">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo9f719aef-e50a-4c42-aae7-cfad376f50dc">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600649">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600649">
									<gml:posList srsDimension="3">388929.898137 5720411.936927 71.911000 388929.898137 5720411.936927 75.411000 388928.528744 5720412.547054 76.911000 388928.528744 5720412.547054 71.911000 388929.898137 5720411.936927 71.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_991_300718_300531">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo232daff2-a230-4470-84d2-4c321c710aac">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600650">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600650">
									<gml:posList srsDimension="3">388930.322198 5720412.888705 71.911000 388930.322198 5720412.888705 75.411000 388929.898137 5720411.936927 75.411000 388929.898137 5720411.936927 71.911000 388930.322198 5720412.888705 71.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_195_78954_44861">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo58edbe1f-479c-4ac5-b162-87814e550a8d">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600651">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600651">
									<gml:posList srsDimension="3">388928.952804 5720413.498832 71.911000 388928.952804 5720413.498832 76.911000 388930.322198 5720412.888705 75.411000 388930.322198 5720412.888705 71.911000 388928.952804 5720413.498832 71.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_4_132298_413267">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeod5948f55-2fb9-46ba-9166-b762133b834a">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600652">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600652">
									<gml:posList srsDimension="3">388925.168985 5720411.142265 76.911000 388925.703477 5720410.973632 76.688060 388925.703477 5720410.973632 77.911000 388924.427393 5720411.376238 77.911000 388924.427393 5720411.376238 71.911000 388925.168985 5720411.142265 71.911000 388925.168985 5720411.142265 76.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_736_813650_5432">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoff581a83-9b3a-477a-a970-9aa0ed75201a">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600653">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600653">
									<gml:posList srsDimension="3">388925.319837 5720409.657593 71.911000 388925.319837 5720409.657593 75.411000 388925.168985 5720411.142265 76.911000 388925.168985 5720411.142265 71.911000 388925.319837 5720409.657593 71.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1679_134419_279830">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoa25e95e9-aff7-450f-bcdf-e592d7b2b5f9">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600654">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600654">
									<gml:posList srsDimension="3">388926.343980 5720409.761652 71.911000 388926.343980 5720409.761652 75.411000 388925.319837 5720409.657593 75.411000 388925.319837 5720409.657593 71.911000 388926.343980 5720409.761652 71.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1515_315969_82283">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo12978cfd-ea1f-458f-a870-d7863c5bd737">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600655">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600655">
									<gml:posList srsDimension="3">388926.193128 5720411.246324 71.911000 388926.193128 5720411.246324 76.911000 388926.343980 5720409.761652 75.411000 388926.343980 5720409.761652 71.911000 388926.193128 5720411.246324 71.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1264_70770_262698">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo2074df2f-f44c-45da-8ffd-254aeffbb8da">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600656">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600656">
									<gml:posList srsDimension="3">388924.990244 5720406.780777 73.198546 388924.990244 5720406.780777 76.911000 388925.019000 5720406.546000 76.598763 388925.019000 5720406.546000 75.765753 388925.019000 5720406.546000 73.202857 388924.990244 5720406.780777 73.198546 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_48_444669_192047">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo8d31deeb-4a2e-43df-ae8f-4352e5219ff5">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600657">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600657">
									<gml:posList srsDimension="3">388919.979980 5720405.684766 81.564421 388919.979980 5720405.684766 80.030168 388919.917000 5720406.330000 80.564421 388919.917000 5720406.330000 77.911000 388919.471757 5720410.891582 81.564421 388919.979980 5720405.684766 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1511_844578_375315">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo4f793dff-a8b4-45cb-9a97-cbe27b76ded1">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600658">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600658">
									<gml:posList srsDimension="3">388898.142000 5720427.599000 77.911000 388899.375230 5720427.716430 77.911000 388899.375230 5720427.716430 76.911000 388898.142000 5720427.599000 76.911000 388898.142000 5720427.599000 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1900_29713_347525">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeofaca951b-ca8b-4b2f-9f27-5f5c46b8be73">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600659">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600659">
									<gml:posList srsDimension="3">388891.912000 5720427.061000 77.911000 388893.138000 5720427.167000 77.911000 388893.138000 5720427.167000 76.911000 388891.912000 5720427.061000 76.911000 388891.912000 5720427.061000 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1943_501784_151899">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeob536fbd3-d343-4787-8f42-2d198dd5bdca">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600660">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600660">
									<gml:posList srsDimension="3">388894.928000 5720408.530000 77.911000 388893.718000 5720408.374000 77.911000 388893.718000 5720408.374000 76.911000 388894.928000 5720408.530000 76.911000 388894.928000 5720408.530000 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1015_579491_69936">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo3e7fdb7a-3f07-42be-9214-77c289cf1b37">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600661">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600661">
									<gml:posList srsDimension="3">388901.147415 5720409.105306 77.911000 388899.947000 5720408.991000 77.911000 388899.947000 5720408.991000 76.911000 388901.147415 5720409.105306 76.911000 388901.147415 5720409.105306 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1727_790887_81516">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeod85b50ad-cc5c-4f93-bfc3-a5d4291bb6e9">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600662">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600662">
									<gml:posList srsDimension="3">388899.375230 5720427.716430 77.911000 388903.852558 5720428.142770 77.911000 388903.852558 5720428.142770 71.681000 388899.678638 5720427.745321 71.681000 388899.375230 5720427.716430 71.681000 388899.375230 5720427.716430 76.911000 388899.375230 5720427.716430 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_295_35769_150726">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeob7dfcd9d-9f0d-49d7-bee0-dca85038bfbc">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600663">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600663">
									<gml:posList srsDimension="3">388901.147415 5720409.105306 71.791000 388901.147415 5720409.105306 76.911000 388901.236000 5720408.175000 75.798704 388901.236000 5720408.175000 71.791000 388901.147415 5720409.105306 71.791000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_696_812561_400604">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo8d8f90c4-4ad0-48e0-a449-5c9caccd45f6">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600664">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600664">
									<gml:posList srsDimension="3">388901.236000 5720408.175000 71.791000 388901.623000 5720408.229000 71.791000 388901.537972 5720409.142495 71.791000 388901.147415 5720409.105306 71.791000 388901.236000 5720408.175000 71.791000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1929_215837_8715">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo84c522a7-d987-4ddc-90cc-61dfb4ab522b">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600665">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600665">
									<gml:posList srsDimension="3">388905.696673 5720409.538494 77.911000 388901.147415 5720409.105306 77.911000 388901.147415 5720409.105306 76.911000 388901.147415 5720409.105306 71.791000 388901.537972 5720409.142495 71.791000 388905.696673 5720409.538494 71.791000 388905.696673 5720409.538494 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_408_349844_345779">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo1d6be7d6-0bee-48a8-8e84-43188da79e2a">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600666">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600666">
									<gml:posList srsDimension="3">388922.989657 5720429.457426 84.831000 388922.989657 5720429.457426 79.607326 388923.546613 5720430.079657 78.844041 388923.546613 5720430.079657 84.831000 388922.989657 5720429.457426 84.831000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_908_694042_234490">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo3e51900f-e729-482a-9951-8caa2f9203cc">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600667">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600667">
									<gml:posList srsDimension="3">388921.951000 5720431.595000 84.831000 388921.951000 5720431.595000 77.911000 388921.350751 5720430.924402 79.001491 388921.350751 5720430.924402 84.831000 388921.951000 5720431.595000 84.831000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_201_515164_124053">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeof1c850da-8a38-461c-8346-551a2650ccab">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600668">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600668">
									<gml:posList srsDimension="3">388921.350751 5720430.924402 84.831000 388921.350751 5720430.924402 79.001491 388921.350751 5720430.542097 79.055839 388921.350751 5720430.079657 78.677500 388921.350751 5720430.079657 84.831000 388921.350751 5720430.924402 84.831000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_625_276647_241061">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoa8c6165b-f789-4475-89dc-c491c22fc68e">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600669">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600669">
									<gml:posList srsDimension="3">388921.350751 5720430.079657 84.831000 388921.350751 5720430.079657 78.677500 388921.531881 5720429.905461 78.371392 388921.531881 5720429.905461 81.564421 388921.887949 5720429.563025 82.404409 388921.997751 5720429.457426 82.143552 388921.997751 5720429.457426 84.831000 388921.350751 5720430.079657 84.831000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1139_344432_55421">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo31939df9-255e-471f-a9c7-99a5c97b3cb5">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600670">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600670">
									<gml:posList srsDimension="3">388930.948000 5720403.888000 71.150777 388930.948000 5720403.888000 66.415000 388931.586000 5720403.943000 66.415000 388931.586000 5720403.943000 71.150919 388930.948000 5720403.888000 71.150777 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1162_613631_181017">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo050d382e-4fdf-4c88-ad33-931d14d5ca60">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600671">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600671">
									<gml:posList srsDimension="3">388919.917000 5720406.330000 77.911000 388919.917000 5720406.330000 80.564421 388920.932000 5720405.462000 79.200832 388920.932000 5720405.462000 66.415000 388919.917000 5720406.330000 66.415000 388919.917000 5720406.330000 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_845_319010_54239">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo8080a217-ee41-4fa0-904d-a87c3259839f">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600672">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600672">
									<gml:posList srsDimension="3">388910.543000 5720403.758000 79.064421 388910.543000 5720403.758000 66.415000 388911.452000 5720404.910000 66.415000 388911.452000 5720404.910000 80.564421 388910.543000 5720403.758000 79.064421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1198_727406_362205">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo1e1ed44b-c180-435c-a796-870993d1738d">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600673">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600673">
									<gml:posList srsDimension="3">388909.729000 5720404.464000 79.193009 388909.729000 5720404.464000 66.415000 388910.543000 5720403.758000 66.415000 388910.543000 5720403.758000 79.064421 388909.729000 5720404.464000 79.193009 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1268_735934_238886">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoc412222d-98ba-4b9f-85ae-071266c6e8c7">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600674">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600674">
									<gml:posList srsDimension="3">388901.623000 5720408.229000 71.791000 388901.623000 5720408.229000 66.415000 388901.853000 5720405.758000 66.415000 388901.853000 5720405.758000 71.791000 388901.623000 5720408.229000 71.791000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1018_79726_160357">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo0de39cbf-da65-45cd-b535-cf8e885d215b">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600675">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600675">
									<gml:posList srsDimension="3">388887.646000 5720406.535000 75.411000 388887.646000 5720406.535000 66.415000 388888.756000 5720407.865000 66.415000 388888.756000 5720407.865000 76.911000 388887.646000 5720406.535000 75.411000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1523_145192_237662">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo94906e08-ab05-4371-8509-a140fa187e21">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600676">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600676">
									<gml:posList srsDimension="3">388882.672000 5720408.732000 75.861000 388882.672000 5720408.732000 66.415000 388887.719000 5720409.370000 66.415000 388887.719000 5720409.370000 75.861000 388882.672000 5720408.732000 75.861000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1005_722870_315964">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoc65e7f83-aa1d-45fd-af80-9281783fd9ca">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600677">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600677">
									<gml:posList srsDimension="3">388880.468000 5720424.332000 75.861000 388880.468000 5720424.332000 66.415000 388881.130000 5720420.148000 66.415000 388881.130000 5720420.148000 75.861000 388880.468000 5720424.332000 75.861000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_533_847296_104352">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo55c69d82-d029-45e1-8e66-072ae31f5d33">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600678">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600678">
									<gml:posList srsDimension="3">388886.040000 5720425.213000 75.861000 388886.040000 5720425.213000 66.415000 388880.468000 5720424.332000 66.415000 388880.468000 5720424.332000 75.861000 388886.040000 5720425.213000 75.861000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_823_655213_91333">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo24f02213-840b-4d55-bf0b-a2e0afcd81c0">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600679">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600679">
									<gml:posList srsDimension="3">388885.413000 5720427.785000 75.411000 388885.413000 5720427.785000 66.415000 388884.644000 5720426.873000 66.415000 388884.644000 5720426.873000 75.620164 388885.413000 5720427.785000 75.411000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1184_455205_56979">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoa75a655c-b299-4b4a-93a6-64e0935ad59d">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600680">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600680">
									<gml:posList srsDimension="3">388927.447650 5720428.857988 77.911000 388927.447650 5720428.857988 76.469456 388926.998000 5720429.162000 76.891000 388926.998000 5720429.162000 66.415000 388924.809000 5720430.642000 66.415000 388924.809000 5720430.642000 76.891000 388924.426071 5720430.900901 76.540973 388924.426071 5720430.900901 77.911000 388927.447650 5720428.857988 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_433_238119_56100">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoe25fc642-0959-495e-aeb0-a8e6bbcd2cf7">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600681">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600681">
									<gml:posList srsDimension="3">388927.181000 5720426.233000 77.911000 388927.181000 5720426.233000 66.415000 388927.393000 5720428.320000 66.415000 388927.393000 5720428.320000 76.891000 388927.447650 5720428.857988 76.469456 388927.447650 5720428.857988 77.911000 388927.181000 5720426.233000 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1033_225834_57120">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoa6f1a367-c2ec-4cab-8e73-cd40bbee381d">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600682">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600682">
									<gml:posList srsDimension="3">388927.181000 5720426.233000 77.911000 388927.181000 5720426.233000 81.564421 388930.635000 5720426.583000 81.564421 388930.635000 5720426.583000 80.564421 388930.635000 5720426.583000 66.415000 388927.181000 5720426.233000 66.415000 388927.181000 5720426.233000 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1669_22645_8685">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo76909990-a48f-4578-bab3-0f3541c7aff3">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600683">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600683">
									<gml:posList srsDimension="3">388934.230000 5720424.173000 80.564421 388934.230000 5720424.173000 66.415000 388931.650000 5720426.272000 66.415000 388931.650000 5720426.272000 80.564421 388931.650000 5720426.272000 81.564421 388934.230000 5720424.173000 81.564421 388934.230000 5720424.173000 80.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_476_712741_69400">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo221d72e7-dd46-4bc8-aa00-4406a082a42c">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600684">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600684">
									<gml:posList srsDimension="3">388934.770000 5720418.899000 80.564421 388934.770000 5720418.899000 81.564421 388932.563000 5720416.275000 81.564421 388932.563000 5720416.275000 80.564421 388932.563000 5720416.275000 66.415000 388934.770000 5720418.899000 66.415000 388934.770000 5720418.899000 80.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_761_204205_332482">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoa59e6af4-ae0b-4fd3-b957-291cbd779934">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600685">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600685">
									<gml:posList srsDimension="3">388933.176000 5720415.135000 71.911000 388933.176000 5720415.135000 66.415000 388932.563000 5720416.275000 66.415000 388932.563000 5720416.275000 80.564421 388933.176000 5720415.135000 79.064421 388933.176000 5720415.135000 71.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_892_264590_128273">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoab9ad1f2-2367-4cbc-9ee6-c73baf238c71">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600686">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600686">
									<gml:posList srsDimension="3">388933.199000 5720414.848000 69.511000 388933.199000 5720414.848000 66.415000 388933.176000 5720415.135000 66.415000 388933.176000 5720415.135000 71.911000 388933.199000 5720414.848000 71.911000 388933.199000 5720414.848000 69.511000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1677_486955_2642">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo9f9a5e66-48c9-47e7-a53c-42026bd65e96">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600687">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600687">
									<gml:posList srsDimension="3">388934.471403 5720413.099014 71.911000 388934.471403 5720413.099014 66.415000 388934.460000 5720413.233000 66.415000 388934.460000 5720413.233000 69.511000 388934.460000 5720413.233000 71.911000 388934.471403 5720413.099014 71.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1078_606978_173635">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoe2c27367-f8a6-442a-bb35-29abd8fdf9fe">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600688">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600688">
									<gml:posList srsDimension="3">388934.480375 5720413.092767 71.911000 388934.480375 5720413.092767 66.415000 388934.471403 5720413.099014 66.415000 388934.471403 5720413.099014 71.911000 388934.480375 5720413.092767 71.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1589_544012_422745">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoc58c504d-18cc-4ef6-afd3-fc45bf40728c">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600689">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600689">
									<gml:posList srsDimension="3">388934.603000 5720413.104000 71.911000 388934.603000 5720413.104000 66.415000 388934.480375 5720413.092767 66.415000 388934.480375 5720413.092767 71.911000 388934.603000 5720413.104000 71.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_37_119885_173531">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo0a56fb92-7d90-4b0b-8d21-3ef78fa642de">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600690">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600690">
									<gml:posList srsDimension="3">388934.859000 5720410.340000 71.911000 388934.859000 5720410.340000 66.415000 388934.603000 5720413.104000 66.415000 388934.603000 5720413.104000 71.911000 388934.859000 5720410.340000 71.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1661_844137_157254">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo18399cad-29e6-4f10-ab95-cb8585ca1062">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600691">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600691">
									<gml:posList srsDimension="3">388934.728000 5720410.328000 71.911000 388934.728000 5720410.328000 66.415000 388934.859000 5720410.340000 66.415000 388934.859000 5720410.340000 71.911000 388934.728000 5720410.328000 71.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_768_460550_241383">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo05e32073-572d-456f-9ff5-da864492771c">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600692">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600692">
									<gml:posList srsDimension="3">388934.741000 5720410.208000 68.121000 388934.741000 5720410.208000 66.415000 388934.728000 5720410.328000 66.415000 388934.728000 5720410.328000 71.911000 388934.741000 5720410.208000 71.911000 388934.741000 5720410.208000 68.121000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1494_15334_193369">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo69d969e0-1a8d-4e03-88f7-5924c2feb704">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600693">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600693">
									<gml:posList srsDimension="3">388934.460000 5720413.233000 69.511000 388934.460000 5720413.233000 66.415000 388934.329000 5720414.937000 66.415000 388934.329000 5720414.937000 69.511000 388934.460000 5720413.233000 69.511000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_575_191505_288892">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoe4e38524-20bc-498b-a4fc-709c56411f51">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600694">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600694">
									<gml:posList srsDimension="3">388934.329000 5720414.937000 69.511000 388934.329000 5720414.937000 66.415000 388933.199000 5720414.848000 66.415000 388933.199000 5720414.848000 69.511000 388934.329000 5720414.937000 69.511000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1654_495201_24117">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo1d3ea47b-e65c-44e3-927e-7367bd0c5b3e">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600695">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600695">
									<gml:posList srsDimension="3">388935.050000 5720419.889000 80.564421 388935.050000 5720419.889000 66.415000 388934.741000 5720423.232000 66.415000 388934.741000 5720423.232000 80.564421 388934.741000 5720423.232000 81.564421 388935.050000 5720419.889000 81.564421 388935.050000 5720419.889000 80.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1043_745341_40964">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo63d5dfc6-5fc7-479d-9ede-511fbf951dbe">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600696">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600696">
									<gml:posList srsDimension="3">388882.264995 5720412.894053 66.415000 388882.265000 5720412.894000 66.415000 388882.672000 5720408.732000 66.415000 388882.672000 5720408.732000 75.861000 388882.300141 5720412.534650 75.861000 388882.264995 5720412.894053 75.861000 388882.264995 5720412.894053 66.415000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_387_601801_75520">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo0847f771-9562-4643-a75c-ec9d7400577a">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600697">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600697">
									<gml:posList srsDimension="3">388882.264995 5720412.894053 66.415000 388882.264995 5720412.894053 75.861000 388887.297000 5720413.682000 77.911000 388887.297000 5720413.682000 94.841000 388884.238000 5720413.203000 98.782158 388881.179000 5720412.724000 94.841000 388881.179000 5720412.724000 66.415000 388882.264995 5720412.894053 66.415000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_599_40408_277174">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo31354c70-ce72-4971-9f52-f5e0310a2b7e">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600698">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600698">
									<gml:posList srsDimension="3">388935.033000 5720407.169000 68.121000 388935.033000 5720407.169000 66.415000 388934.741000 5720410.208000 66.415000 388934.741000 5720410.208000 68.121000 388935.033000 5720407.169000 68.121000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_172_629773_335961">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo9485406c-f7f6-481b-9bab-157ae8a927f4">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600699">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600699">
									<gml:posList srsDimension="3">388935.033000 5720407.169000 68.121000 388934.034000 5720407.055000 68.121000 388934.034000 5720407.055000 66.415000 388935.033000 5720407.169000 66.415000 388935.033000 5720407.169000 68.121000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1285_705644_373304">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo20132e63-75fd-471c-b8a6-4a9c2badca48">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600700">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600700">
									<gml:posList srsDimension="3">388908.524000 5720433.570000 80.621000 388908.524000 5720433.570000 66.415000 388907.682979 5720434.310000 66.415000 388907.500000 5720434.471000 66.415000 388907.500000 5720434.471000 78.875264 388908.524000 5720433.570000 80.621000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1012_694123_87757">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo5ff741d4-ddb6-44e6-9550-f8914867f807">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600701">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600701">
									<gml:posList srsDimension="3">388907.500000 5720434.471000 78.875264 388907.500000 5720434.471000 66.415000 388907.357893 5720434.310000 66.415000 388906.808000 5720433.687000 66.415000 388906.808000 5720433.687000 79.004833 388907.500000 5720434.471000 78.875264 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1648_612277_366828">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo24b295eb-cc9f-4111-bb12-8da25e7e3b12">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600702">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600702">
									<gml:posList srsDimension="3">388906.808000 5720433.687000 79.004833 388906.808000 5720433.687000 66.415000 388907.739000 5720432.837000 66.415000 388907.739000 5720432.837000 77.911000 388907.739000 5720432.837000 80.621000 388906.808000 5720433.687000 79.004833 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_560_723463_46326">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo743d743a-efbe-435a-8d8c-26d40f92394d">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600703">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600703">
									<gml:posList srsDimension="3">388917.237000 5720433.787000 77.911000 388917.237000 5720433.787000 66.415000 388918.000000 5720434.717000 66.415000 388918.000000 5720434.717000 79.169075 388917.237000 5720433.787000 80.621000 388917.237000 5720433.787000 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1072_98491_13633">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeob36b3129-35eb-4bcb-8872-bc82de3b4b76">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600704">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600704">
									<gml:posList srsDimension="3">388918.000000 5720434.717000 79.169075 388918.000000 5720434.717000 66.415000 388917.196000 5720435.364000 66.415000 388917.196000 5720435.364000 78.985850 388918.000000 5720434.717000 79.169075 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_553_403658_344384">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoc5389923-f1e5-4e0d-a0e7-59e3a6bc3a43">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600705">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600705">
									<gml:posList srsDimension="3">388917.196000 5720435.364000 78.985850 388917.196000 5720435.364000 66.415000 388916.348000 5720434.310000 66.415000 388916.348000 5720434.310000 80.621000 388917.196000 5720435.364000 78.985850 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1969_144084_92972">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeod5525f21-b7f4-4897-ace5-30e82a1f9667">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600706">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600706">
									<gml:posList srsDimension="3">388917.178286 5720434.388529 81.564421 388917.178286 5720434.388529 80.023974 388916.348000 5720434.310000 80.621000 388916.348000 5720434.310000 66.415000 388908.524000 5720433.570000 66.415000 388908.524000 5720433.570000 80.621000 388907.670144 5720433.489242 79.947456 388907.670144 5720433.489242 81.564421 388917.178286 5720434.388529 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_565_896116_317908">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoa8b29195-62d2-4820-92d8-b0555b847b3b">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600707">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600707">
									<gml:posList srsDimension="3">388891.802000 5720428.337000 75.411000 388891.802000 5720428.337000 66.415000 388891.912000 5720427.061000 66.415000 388891.912000 5720427.061000 76.911000 388891.802000 5720428.337000 75.411000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1165_447306_357066">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo744719b0-0bb2-41ed-b82c-8e6f21ae5d83">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600708">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600708">
									<gml:posList srsDimension="3">388891.802000 5720428.337000 66.415000 388891.802000 5720428.337000 75.411000 388893.028000 5720428.443000 75.411000 388893.028000 5720428.443000 66.415000 388891.802000 5720428.337000 66.415000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_162_232783_316988">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeof931d355-6b02-4661-bdc2-5d387d21b78e">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600709">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600709">
									<gml:posList srsDimension="3">388884.644000 5720426.873000 75.620164 388884.644000 5720426.873000 66.415000 388885.929000 5720425.912000 66.415000 388885.929000 5720425.912000 76.911000 388884.644000 5720426.873000 75.620164 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1159_868173_396117">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo4cb20020-3aaf-4ed7-8296-2d23631f0130">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600710">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600710">
									<gml:posList srsDimension="3">388885.829904 5720426.536039 76.448438 388885.929000 5720425.912000 76.911000 388885.929000 5720425.912000 66.415000 388886.040000 5720425.213000 66.415000 388886.040000 5720425.213000 75.861000 388886.040000 5720425.213000 77.911000 388885.829904 5720426.536039 77.911000 388885.829904 5720426.536039 76.448438 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_934_132050_138464">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo43afe6d9-a22c-4524-871f-6ae1b9e16ab8">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600711">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600711">
									<gml:posList srsDimension="3">388885.829904 5720426.536039 76.448438 388885.829904 5720426.536039 77.911000 388891.912000 5720427.061000 77.911000 388891.912000 5720427.061000 76.911000 388891.912000 5720427.061000 66.415000 388886.849000 5720426.624000 66.415000 388886.849000 5720426.624000 76.911000 388885.829904 5720426.536039 76.448438 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1769_419477_165034">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoa9cc7323-7897-4f7f-a676-e16a5b767293">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600712">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600712">
									<gml:posList srsDimension="3">388887.719000 5720409.370000 75.861000 388887.719000 5720409.370000 66.415000 388887.798000 5720408.749000 66.415000 388887.798000 5720408.749000 76.911000 388887.921349 5720407.779382 76.365735 388887.921349 5720407.779382 77.911000 388887.719000 5720409.370000 77.911000 388887.719000 5720409.370000 75.861000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_433_91081_136346">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo062cde95-1fcc-4eb6-b5b4-c65204c5ad77">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600713">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600713">
									<gml:posList srsDimension="3">388887.921349 5720407.779382 77.911000 388887.921349 5720407.779382 76.365735 388888.756000 5720407.865000 76.911000 388888.756000 5720407.865000 66.415000 388893.718000 5720408.374000 66.415000 388893.718000 5720408.374000 76.911000 388893.718000 5720408.374000 77.911000 388887.921349 5720407.779382 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1835_578879_249768">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo8b95897d-5788-4421-ab50-a3797a64e63f">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600714">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600714">
									<gml:posList srsDimension="3">388893.828000 5720407.164000 75.464918 388893.828000 5720407.164000 66.415000 388895.043000 5720407.275000 66.415000 388895.043000 5720407.275000 75.411000 388893.828000 5720407.164000 75.464918 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_516_216890_277870">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo5b36bfe1-4e5f-4a13-a9d3-1dafde0131d9">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600715">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600715">
									<gml:posList srsDimension="3">388900.062000 5720407.736000 75.411000 388900.062000 5720407.736000 66.415000 388901.267000 5720407.846000 66.415000 388901.267000 5720407.846000 75.405381 388900.062000 5720407.736000 75.411000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_281_884321_49693">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeod95aec40-25c7-4dfc-a739-ab0155eafad4">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600716">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600716">
									<gml:posList srsDimension="3">388931.535000 5720404.541000 72.150919 388931.535000 5720404.541000 66.415000 388933.810000 5720404.736000 66.415000 388933.810000 5720404.736000 72.150919 388934.292681 5720404.777373 71.543226 388934.292681 5720404.777373 73.151000 388931.535000 5720404.541000 73.150831 388931.535000 5720404.541000 72.150919 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1899_806222_158380">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeof2c6d0b0-5b2c-43c0-adb3-c3b3abd8a3b7">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600717">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600717">
									<gml:posList srsDimension="3">388930.896000 5720404.486000 72.150919 388930.896000 5720404.486000 73.150559 388928.641614 5720404.293053 73.150713 388928.641614 5720404.293053 72.150919 388928.641614 5720404.293053 66.415000 388930.896000 5720404.486000 66.415000 388930.896000 5720404.486000 72.150919 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1904_838168_236440">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo225f27d3-eba0-4da8-8ee1-45dfbee563ea">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600718">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600718">
									<gml:posList srsDimension="3">388925.249755 5720404.002806 73.151000 388925.249755 5720404.002806 71.554699 388925.718000 5720404.043000 72.150919 388925.718000 5720404.043000 66.415000 388928.013000 5720404.240000 66.415000 388928.013000 5720404.240000 72.150919 388928.013000 5720404.240000 73.151520 388925.249755 5720404.002806 73.151000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1947_104393_383242">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo0b1faa3e-e1c4-4a57-b40d-7e752aec9249">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600719">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600719">
									<gml:posList srsDimension="3">388925.019000 5720406.546000 73.202857 388925.019000 5720406.546000 66.415000 388925.208000 5720404.463000 66.415000 388925.208000 5720404.463000 72.150919 388925.249755 5720404.002806 71.554699 388925.249755 5720404.002806 73.151000 388925.019000 5720406.546000 73.202857 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1129_877540_203476">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoa1c440f7-6faf-4dd3-b46c-cc6685ddbc69">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600720">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600720">
									<gml:posList srsDimension="3">388934.292681 5720404.777373 73.151000 388934.292681 5720404.777373 71.543226 388934.244000 5720405.206000 72.150919 388934.244000 5720405.206000 66.415000 388934.034000 5720407.055000 66.415000 388934.034000 5720407.055000 68.121000 388934.034000 5720407.055000 73.150572 388934.292681 5720404.777373 73.151000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_42_313783_28306">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo475c5549-e884-462c-95cc-5799b83dce9a">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600721">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600721">
									<gml:posList srsDimension="3">388895.043000 5720407.275000 75.411000 388895.043000 5720407.275000 66.415000 388894.928000 5720408.530000 66.415000 388894.928000 5720408.530000 76.911000 388895.043000 5720407.275000 75.411000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1476_832717_342983">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoc54ffb59-f243-4683-905e-594b39536b0b">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600722">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600722">
									<gml:posList srsDimension="3">388894.928000 5720408.530000 76.911000 388894.928000 5720408.530000 66.415000 388899.947000 5720408.991000 66.415000 388899.947000 5720408.991000 76.911000 388899.947000 5720408.991000 77.911000 388894.928000 5720408.530000 77.911000 388894.928000 5720408.530000 76.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1158_443863_215750">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo72d740cf-db69-44a0-b81c-3ad16ffe54ce">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600723">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600723">
									<gml:posList srsDimension="3">388922.442000 5720434.344000 75.389372 388922.442000 5720434.344000 66.415000 388921.699000 5720434.921000 66.415000 388921.699000 5720434.921000 75.415000 388922.442000 5720434.344000 75.389372 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_996_621568_103418">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeob5e30ede-a209-4d4c-ac0c-93d44e978eb4">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600724">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600724">
									<gml:posList srsDimension="3">388921.699000 5720434.921000 75.415000 388921.699000 5720434.921000 66.415000 388921.032000 5720434.107000 66.415000 388921.032000 5720434.107000 76.891000 388921.699000 5720434.921000 75.415000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1727_294618_381805">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoe10314d9-069d-4d04-9088-873f67c9775c">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600725">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600725">
									<gml:posList srsDimension="3">388921.730560 5720434.165904 77.911000 388921.730560 5720434.165904 76.210875 388921.032000 5720434.107000 76.891000 388921.032000 5720434.107000 66.415000 388917.237000 5720433.787000 66.415000 388917.237000 5720433.787000 77.911000 388921.730560 5720434.165904 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_848_709820_408746">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo6318e418-bdc6-4157-9b22-4b3145f2cc64">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600726">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600726">
									<gml:posList srsDimension="3">388924.809000 5720430.642000 76.891000 388924.809000 5720430.642000 66.415000 388924.835000 5720431.613000 66.415000 388924.835000 5720431.613000 75.415000 388924.809000 5720430.642000 76.891000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1648_649221_89189">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoefe949a6-7e62-4d16-ab4e-eb8f6dd102e6">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600727">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600727">
									<gml:posList srsDimension="3">388924.835000 5720431.613000 75.415000 388924.835000 5720431.613000 66.415000 388923.905000 5720431.696000 66.415000 388923.905000 5720431.696000 75.392907 388924.835000 5720431.613000 75.415000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1669_487297_350538">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo5ab16c15-5dd6-4c1e-aa6e-348d13608480">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600728">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600728">
									<gml:posList srsDimension="3">388923.905000 5720431.696000 75.392907 388923.905000 5720431.696000 66.415000 388923.817000 5720430.715000 66.415000 388923.817000 5720430.715000 76.891000 388923.905000 5720431.696000 75.392907 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_135_450919_428839">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeocc69a380-131a-44ae-8292-fb795efeabf6">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600729">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600729">
									<gml:posList srsDimension="3">388924.426071 5720430.900901 77.911000 388924.426071 5720430.900901 76.540973 388923.817000 5720430.715000 76.891000 388923.817000 5720430.715000 66.415000 388923.568000 5720430.639000 66.415000 388923.568000 5720430.639000 77.911000 388924.426071 5720430.900901 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_845_487082_182395">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeof9804c6d-0f9c-40f0-b774-124deb641347">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600730">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600730">
									<gml:posList srsDimension="3">388927.393000 5720428.320000 76.891000 388927.393000 5720428.320000 66.415000 388928.255000 5720428.680000 66.415000 388928.255000 5720428.680000 75.475631 388927.393000 5720428.320000 76.891000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1733_664110_302491">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeof7af7669-9216-4d25-806b-b52809ac1ca0">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600731">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600731">
									<gml:posList srsDimension="3">388928.255000 5720428.680000 75.475631 388928.255000 5720428.680000 66.415000 388927.899000 5720429.533000 66.415000 388927.899000 5720429.533000 75.415000 388928.255000 5720428.680000 75.475631 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1365_477860_289534">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeob232e128-73f6-462e-82a1-23d95a4def9d">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600732">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600732">
									<gml:posList srsDimension="3">388927.899000 5720429.533000 75.415000 388927.899000 5720429.533000 66.415000 388926.998000 5720429.162000 66.415000 388926.998000 5720429.162000 76.891000 388927.899000 5720429.533000 75.415000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_739_109386_324040">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeodbfe17a3-71de-4f39-8151-17d7a6748362">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600733">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600733">
									<gml:posList srsDimension="3">388931.650000 5720426.272000 80.564421 388931.650000 5720426.272000 66.415000 388932.076000 5720427.521000 66.415000 388932.076000 5720427.521000 79.064421 388931.650000 5720426.272000 80.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1824_896078_160479">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoeeaaf98c-6f29-41c8-852a-842bf38e1829">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600734">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600734">
									<gml:posList srsDimension="3">388932.076000 5720427.521000 79.064421 388932.076000 5720427.521000 66.415000 388931.022000 5720427.806000 66.415000 388931.022000 5720427.806000 79.105685 388932.076000 5720427.521000 79.064421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_280_274525_200372">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeofebb9574-03ac-460c-80dd-8b078fa7532a">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600735">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600735">
									<gml:posList srsDimension="3">388931.022000 5720427.806000 79.105685 388931.022000 5720427.806000 66.415000 388930.635000 5720426.583000 66.415000 388930.635000 5720426.583000 80.564421 388931.022000 5720427.806000 79.105685 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1939_343537_152340">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo4647a56e-fc34-4a33-b573-e746a12adc58">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600736">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600736">
									<gml:posList srsDimension="3">388934.741000 5720423.232000 80.564421 388934.741000 5720423.232000 66.415000 388935.893000 5720423.817000 66.415000 388935.893000 5720423.817000 79.064421 388934.741000 5720423.232000 80.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1234_121122_225864">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo86ad54a2-2bab-475a-b1b1-33bd362eecb1">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600737">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600737">
									<gml:posList srsDimension="3">388935.893000 5720423.817000 79.064421 388935.893000 5720423.817000 66.415000 388935.433000 5720424.765000 66.415000 388935.433000 5720424.765000 79.008489 388935.893000 5720423.817000 79.064421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1928_89911_251794">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeofa6164cc-f709-417d-8a71-b69820c6aed9">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600738">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600738">
									<gml:posList srsDimension="3">388935.433000 5720424.765000 79.008489 388935.433000 5720424.765000 66.415000 388934.230000 5720424.173000 66.415000 388934.230000 5720424.173000 80.564421 388935.433000 5720424.765000 79.008489 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_735_679338_54522">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoc564d035-a99d-4a85-9cb0-f419a1ad1e8d">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600739">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600739">
									<gml:posList srsDimension="3">388934.770000 5720418.899000 80.564421 388934.770000 5720418.899000 66.415000 388935.954277 5720418.504574 66.415000 388935.954277 5720418.504574 79.120893 388934.770000 5720418.899000 80.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1251_14184_155812">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoc564ba5f-fc8f-42a8-ae1f-34158a743092">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600740">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600740">
									<gml:posList srsDimension="3">388935.954277 5720418.504574 79.120893 388935.954277 5720418.504574 66.415000 388936.280000 5720419.477000 66.415000 388936.280000 5720419.477000 79.064421 388935.954277 5720418.504574 79.120893 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_63_288454_2932">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo41c3cb2b-83d4-4bc8-8164-2fbd1008b415">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600741">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600741">
									<gml:posList srsDimension="3">388936.280000 5720419.477000 79.064421 388936.280000 5720419.477000 66.415000 388935.050000 5720419.889000 66.415000 388935.050000 5720419.889000 80.564421 388936.280000 5720419.477000 79.064421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1905_432820_146982">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeof8159498-e8f7-47d4-893c-2f10030923e1">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600742">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600742">
									<gml:posList srsDimension="3">388933.810000 5720404.736000 72.150919 388933.810000 5720404.736000 66.415000 388934.233000 5720404.402000 66.415000 388934.233000 5720404.402000 71.150919 388933.810000 5720404.736000 72.150919 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1884_370246_113602">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoa9665bc2-4a15-4f19-82c6-05d62476b523">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600743">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600743">
									<gml:posList srsDimension="3">388934.233000 5720404.402000 71.150919 388934.233000 5720404.402000 66.415000 388934.637000 5720404.860000 66.415000 388934.637000 5720404.860000 71.176786 388934.233000 5720404.402000 71.150919 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1137_83132_91871">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo52228d81-eb48-46c1-85d6-f20be9975f93">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600744">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600744">
									<gml:posList srsDimension="3">388934.244000 5720405.206000 66.415000 388934.244000 5720405.206000 72.150919 388934.637000 5720404.860000 71.176786 388934.637000 5720404.860000 66.415000 388934.244000 5720405.206000 66.415000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1425_308879_208692">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo08d9b739-2277-49fc-81fa-ed27ffb884c1">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600745">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600745">
									<gml:posList srsDimension="3">388930.896000 5720404.486000 72.150919 388930.896000 5720404.486000 66.415000 388930.948000 5720403.888000 66.415000 388930.948000 5720403.888000 71.150777 388930.896000 5720404.486000 72.150919 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_343_391057_261162">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoe64e0481-7dea-46ac-a3a6-ad9564515198">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600746">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600746">
									<gml:posList srsDimension="3">388931.586000 5720403.943000 71.150919 388931.586000 5720403.943000 66.415000 388931.535000 5720404.541000 66.415000 388931.535000 5720404.541000 72.150919 388931.586000 5720403.943000 71.150919 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_886_2768_27568">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo91602d04-ebb8-4ed7-b73d-0e9781e0a5d4">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600747">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600747">
									<gml:posList srsDimension="3">388928.013000 5720404.240000 72.150919 388928.013000 5720404.240000 66.415000 388928.065000 5720403.631000 66.415000 388928.065000 5720403.631000 71.150919 388928.013000 5720404.240000 72.150919 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_237_353599_106934">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo04a7bc10-6b29-42f3-9e63-247ea3187d28">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600748">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600748">
									<gml:posList srsDimension="3">388928.065000 5720403.631000 71.150919 388928.065000 5720403.631000 66.415000 388928.694000 5720403.685000 66.415000 388928.694000 5720403.685000 71.152411 388928.065000 5720403.631000 71.150919 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_569_160659_312609">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo6d4c03ca-ed93-49bf-877e-8d1628a7d20e">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600749">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600749">
									<gml:posList srsDimension="3">388928.694000 5720403.685000 71.152411 388928.694000 5720403.685000 66.415000 388928.641614 5720404.293053 66.415000 388928.641614 5720404.293053 72.150919 388928.694000 5720403.685000 71.152411 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1113_276672_139614">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeocf9f0574-8620-44d4-a92f-bfcb61a74c74">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600750">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600750">
									<gml:posList srsDimension="3">388925.208000 5720404.463000 72.150919 388925.208000 5720404.463000 66.415000 388924.858000 5720404.030000 66.415000 388924.858000 5720404.030000 71.141036 388925.208000 5720404.463000 72.150919 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1477_46539_176551">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo8f82a424-f447-4f3c-b4b0-208a288b26cd">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600751">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600751">
									<gml:posList srsDimension="3">388924.858000 5720404.030000 71.141036 388924.858000 5720404.030000 66.415000 388925.345000 5720403.636000 66.415000 388925.345000 5720403.636000 71.150919 388924.858000 5720404.030000 71.141036 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_944_199261_115107">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo409e4a78-5c31-4b07-87dc-e9ad6b0b46ca">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600752">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600752">
									<gml:posList srsDimension="3">388925.345000 5720403.636000 71.150919 388925.345000 5720403.636000 66.415000 388925.718000 5720404.043000 66.415000 388925.718000 5720404.043000 72.150919 388925.345000 5720403.636000 71.150919 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1404_779786_286668">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeodd9355f1-44f1-4d24-9f65-40b965ce2339">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600753">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600753">
									<gml:posList srsDimension="3">388924.433287 5720406.731289 77.911000 388919.917000 5720406.330000 77.911000 388919.917000 5720406.330000 66.415000 388923.721000 5720406.668000 66.415000 388923.721000 5720406.668000 76.911000 388924.433287 5720406.731289 76.911000 388924.433287 5720406.731289 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_888_459846_374497">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeob8ed488c-a04f-4f4d-b8b4-ee60854361fe">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600754">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600754">
									<gml:posList srsDimension="3">388923.721000 5720406.668000 76.911000 388923.721000 5720406.668000 66.415000 388924.555000 5720405.982000 66.415000 388924.555000 5720405.982000 75.911000 388923.721000 5720406.668000 76.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1751_151272_371222">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoa92cc85f-40e9-49ed-99ed-36013053db6e">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600755">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600755">
									<gml:posList srsDimension="3">388925.019000 5720406.546000 75.765753 388925.019000 5720406.546000 76.598763 388924.555000 5720405.982000 75.911000 388924.555000 5720405.982000 66.415000 388925.019000 5720406.546000 66.415000 388925.019000 5720406.546000 73.202857 388925.019000 5720406.546000 75.765753 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_518_71304_206940">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo6983022f-21e4-4dda-8dc4-985b22a2e50b">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600756">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600756">
									<gml:posList srsDimension="3">388910.694489 5720404.841180 81.564421 388910.694489 5720404.841180 80.069282 388911.452000 5720404.910000 80.564421 388911.452000 5720404.910000 66.415000 388916.348000 5720405.354801 66.415000 388919.124000 5720405.607000 66.415000 388919.124000 5720405.607000 80.564421 388919.979980 5720405.684766 80.030168 388919.979980 5720405.684766 81.564421 388910.694489 5720404.841180 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1801_608446_239352">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoade55876-25fb-4b34-bc98-f1b7024962dc">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600757">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600757">
									<gml:posList srsDimension="3">388919.124000 5720405.607000 80.564421 388919.124000 5720405.607000 66.415000 388920.225000 5720404.638000 66.415000 388920.225000 5720404.638000 79.064421 388919.124000 5720405.607000 80.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1344_597368_128568">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeobf4ceb99-660a-4e09-acea-32629fe3c721">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600758">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600758">
									<gml:posList srsDimension="3">388910.628000 5720405.471000 77.911000 388910.628000 5720405.471000 66.415000 388909.729000 5720404.464000 66.415000 388909.729000 5720404.464000 79.193009 388910.628000 5720405.471000 80.564421 388910.628000 5720405.471000 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1221_501902_148853">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo1d8899cd-a4f2-4c84-a78b-1cf588bac2c7">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600759">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600759">
									<gml:posList srsDimension="3">388893.138000 5720427.167000 76.911000 388893.138000 5720427.167000 66.415000 388893.028000 5720428.443000 66.415000 388893.028000 5720428.443000 75.411000 388893.138000 5720427.167000 76.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1410_475860_150646">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo1a842a27-1589-4929-ad20-3ac2aefc1e14">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600760">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600760">
									<gml:posList srsDimension="3">388886.849000 5720426.624000 76.911000 388886.849000 5720426.624000 66.415000 388885.413000 5720427.785000 66.415000 388885.413000 5720427.785000 75.411000 388886.849000 5720426.624000 76.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1173_455990_253483">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeof1bb5a1d-67fa-485d-860b-7a2abc37963c">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600761">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600761">
									<gml:posList srsDimension="3">388887.798000 5720408.749000 76.911000 388887.798000 5720408.749000 66.415000 388886.682000 5720407.302000 66.415000 388886.682000 5720407.302000 75.332927 388887.798000 5720408.749000 76.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_269_205100_30039">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo0ad79ea4-82da-44ae-bc5d-3ce11945bd71">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600762">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600762">
									<gml:posList srsDimension="3">388886.682000 5720407.302000 75.332927 388886.682000 5720407.302000 66.415000 388887.646000 5720406.535000 66.415000 388887.646000 5720406.535000 75.411000 388886.682000 5720407.302000 75.332927 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_140_434983_410366">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoe7615557-0cd3-4b56-a4d6-18571f091537">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600763">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600763">
									<gml:posList srsDimension="3">388921.788000 5720433.496000 76.891000 388921.788000 5720433.496000 66.415000 388922.442000 5720434.344000 66.415000 388922.442000 5720434.344000 75.389372 388921.788000 5720433.496000 76.891000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1358_304_58329">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeof43ca2f1-3a74-472a-91a8-fe979328e0ff">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600764">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600764">
									<gml:posList srsDimension="3">388921.951000 5720431.595000 77.911000 388921.951000 5720431.595000 66.415000 388921.788000 5720433.496000 66.415000 388921.788000 5720433.496000 76.891000 388921.730560 5720434.165904 76.210875 388921.730560 5720434.165904 77.911000 388921.951000 5720431.595000 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_869_383347_260512">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo99541ffc-cacb-4c1a-8037-12b0ed7c9b0c">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600765">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600765">
									<gml:posList srsDimension="3">388893.718000 5720408.374000 76.911000 388893.718000 5720408.374000 66.415000 388893.828000 5720407.164000 66.415000 388893.828000 5720407.164000 75.464918 388893.718000 5720408.374000 76.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_494_597916_345003">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo37a92c0b-ceb3-465c-8356-7ce8eca45708">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600766">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600766">
									<gml:posList srsDimension="3">388900.062000 5720407.736000 66.415000 388900.062000 5720407.736000 75.411000 388899.947000 5720408.991000 76.911000 388899.947000 5720408.991000 66.415000 388900.062000 5720407.736000 66.415000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1027_727400_212314">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeob2bb87aa-a95c-49e9-94ab-c01f6e6dbc61">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600767">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600767">
									<gml:posList srsDimension="3">388901.853000 5720405.758000 71.791000 388901.853000 5720405.758000 66.415000 388906.027000 5720406.206000 66.415000 388906.027000 5720406.206000 71.791000 388901.853000 5720405.758000 71.791000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_752_286641_63615">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeod928700b-fb93-4e27-a5fc-c671afded5c3">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600768">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600768">
									<gml:posList srsDimension="3">388906.146886 5720405.085615 77.911000 388906.146886 5720405.085615 76.322966 388906.756000 5720405.138000 76.911000 388906.756000 5720405.138000 66.415000 388910.628000 5720405.471000 66.415000 388910.628000 5720405.471000 77.911000 388906.146886 5720405.085615 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1367_125428_296019">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoc66c7d71-f1d7-4681-bee8-5b3238c0daf1">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600769">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600769">
									<gml:posList srsDimension="3">388906.009000 5720404.345000 75.415000 388906.009000 5720404.345000 66.415000 388906.756000 5720405.138000 66.415000 388906.756000 5720405.138000 76.911000 388906.009000 5720404.345000 75.415000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_165_328106_123925">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoccedbc42-392f-4ade-bd1b-9eb5b670831a">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600770">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600770">
									<gml:posList srsDimension="3">388905.394000 5720404.880000 75.446361 388905.394000 5720404.880000 66.415000 388906.009000 5720404.345000 66.415000 388906.009000 5720404.345000 75.415000 388905.394000 5720404.880000 75.446361 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1879_641245_51566">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo08cdbe10-6146-4c0d-8b7c-949b0bd6d552">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600771">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600771">
									<gml:posList srsDimension="3">388906.082000 5720405.692000 76.911000 388906.082000 5720405.692000 66.415000 388905.394000 5720404.880000 66.415000 388905.394000 5720404.880000 75.446361 388906.082000 5720405.692000 76.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1240_672955_223069">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo977e3e1c-dc80-455c-9bc1-5f170cb09bc4">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600772">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600772">
									<gml:posList srsDimension="3">388906.027000 5720406.206000 71.791000 388906.027000 5720406.206000 66.415000 388906.082000 5720405.692000 66.415000 388906.082000 5720405.692000 76.911000 388906.146886 5720405.085615 76.322966 388906.146886 5720405.085615 77.911000 388906.027000 5720406.206000 77.911000 388906.027000 5720406.206000 71.791000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_578_229527_294842">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoa95b2b7f-975b-404e-b221-d39bec8b2ce5">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600773">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600773">
									<gml:posList srsDimension="3">388898.142000 5720427.599000 76.911000 388898.142000 5720427.599000 66.415000 388893.138000 5720427.167000 66.415000 388893.138000 5720427.167000 76.911000 388893.138000 5720427.167000 77.911000 388898.142000 5720427.599000 77.911000 388898.142000 5720427.599000 76.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_734_266875_173230">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo56ec786a-6ba4-4e24-96b5-90b7a328bde9">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600774">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600774">
									<gml:posList srsDimension="3">388904.013000 5720432.507000 76.891000 388904.013000 5720432.507000 66.415000 388903.178000 5720433.162000 66.415000 388903.178000 5720433.162000 75.415686 388904.013000 5720432.507000 76.891000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_271_435490_179001">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo3ba12138-0e5c-41c8-acc2-5ca9b5f43a66">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600775">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600775">
									<gml:posList srsDimension="3">388902.695000 5720432.484000 75.415610 388902.695000 5720432.484000 66.415000 388903.503000 5720431.791000 66.415000 388903.503000 5720431.791000 76.891000 388902.695000 5720432.484000 75.415610 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_197_234315_143536">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo4dae98c4-94ee-43b8-82f9-9c0d384a1b80">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600776">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600776">
									<gml:posList srsDimension="3">388903.178000 5720433.162000 75.415686 388903.178000 5720433.162000 66.415000 388902.695000 5720432.484000 66.415000 388902.695000 5720432.484000 75.415610 388903.149533 5720433.122040 75.415681 388903.178000 5720433.162000 75.415686 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1375_624859_219354">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoaaa49cd2-1c5f-46c0-88f8-24cc4c9948dc">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600777">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600777">
									<gml:posList srsDimension="3">388903.453187 5720432.457419 77.911000 388907.739000 5720432.837000 77.911000 388907.739000 5720432.837000 66.415000 388904.013000 5720432.507000 66.415000 388904.013000 5720432.507000 76.891000 388903.453187 5720432.457419 76.296480 388903.453187 5720432.457419 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_795_4412_188662">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoa16b12d5-4343-4391-8edd-df93dd2f3d63">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600778">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600778">
									<gml:posList srsDimension="3">388898.032000 5720428.875000 75.411000 388898.032000 5720428.875000 66.415000 388898.142000 5720427.599000 66.415000 388898.142000 5720427.599000 76.911000 388898.032000 5720428.875000 75.411000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1189_606199_208151">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo647bba78-e475-488a-8ac2-d3488d5ddd01">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600779">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600779">
									<gml:posList srsDimension="3">388901.236000 5720408.175000 71.791000 388901.236000 5720408.175000 66.415000 388901.623000 5720408.229000 66.415000 388901.623000 5720408.229000 71.791000 388901.236000 5720408.175000 71.791000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_659_763765_255611">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo3d35af89-56f9-4d1e-82ad-a5d8cbca3d66">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600780">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600780">
									<gml:posList srsDimension="3">388901.236000 5720408.175000 71.791000 388901.236000 5720408.175000 75.798704 388901.267000 5720407.846000 75.405381 388901.267000 5720407.846000 66.415000 388901.236000 5720408.175000 66.415000 388901.236000 5720408.175000 71.791000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_803_149638_109826">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo5babd9f7-2515-4f0f-b2f0-fa7320cf925a">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600781">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600781">
									<gml:posList srsDimension="3">388923.581000 5720430.979000 84.831000 388923.581000 5720430.979000 66.415000 388922.847000 5720431.636000 66.415000 388922.847000 5720431.636000 84.831000 388923.581000 5720430.979000 84.831000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_133_729600_204563">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoc0cc7e4e-e670-4b1d-a1ae-fc578320e8e2">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600782">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600782">
									<gml:posList srsDimension="3">388923.546613 5720430.079657 84.831000 388923.546613 5720430.079657 78.844041 388923.568000 5720430.639000 77.911000 388923.568000 5720430.639000 66.415000 388923.581000 5720430.979000 66.415000 388923.581000 5720430.979000 84.831000 388923.546613 5720430.079657 84.831000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1455_209022_208561">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeocc270981-f2d0-41bc-ba8a-0a4cedf56a27">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600783">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600783">
									<gml:posList srsDimension="3">388922.847000 5720431.636000 84.831000 388922.847000 5720431.636000 66.415000 388921.951000 5720431.595000 66.415000 388921.951000 5720431.595000 77.911000 388921.951000 5720431.595000 84.831000 388922.847000 5720431.636000 84.831000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1337_118663_330226">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo70c6c548-f61c-4630-8bd9-0cf520349f94">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600784">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600784">
									<gml:posList srsDimension="3">388881.130000 5720420.148000 75.861000 388881.130000 5720420.148000 66.415000 388880.044000 5720419.978000 66.415000 388880.044000 5720419.978000 94.841000 388883.101000 5720420.456500 98.782158 388886.158000 5720420.935000 94.841000 388886.158000 5720420.935000 77.911000 388881.130000 5720420.148000 75.861000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1841_552522_359531">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo42a346d5-77cf-4e08-8f21-966411ff152c">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600785">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600785">
									<gml:posList srsDimension="3">388920.225000 5720404.638000 79.064421 388920.225000 5720404.638000 66.415000 388920.932000 5720405.462000 66.415000 388920.932000 5720405.462000 79.200832 388920.225000 5720404.638000 79.064421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_188_245735_57043">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo0e9d234f-4d7b-4e65-9763-c7be83d94c32">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600786">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600786">
									<gml:posList srsDimension="3">388903.453187 5720432.457419 76.296480 388903.503000 5720431.791000 76.891000 388903.503000 5720431.791000 66.415000 388903.540000 5720431.296000 66.415000 388903.540000 5720431.296000 71.681000 388903.540000 5720431.296000 77.911000 388903.453187 5720432.457419 77.911000 388903.453187 5720432.457419 76.296480 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1994_125446_243498">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo175d8cc7-046c-43d7-a043-7f451fb3620c">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600787">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600787">
									<gml:posList srsDimension="3">388903.540000 5720431.296000 71.681000 388903.540000 5720431.296000 66.415000 388899.341000 5720430.978000 66.415000 388899.341000 5720430.978000 71.681000 388903.540000 5720431.296000 71.681000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_306_480494_74882">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo24028815-cef3-4216-9d4c-51b496006175">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600788">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600788">
									<gml:posList srsDimension="3">388922.212810 5720429.457426 81.564421 388922.505884 5720427.064589 81.564421 388922.212810 5720429.457426 78.774205 388922.212810 5720429.457426 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1306_504500_228460">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeof585e3e1-44e5-435b-8268-ca925af59001">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600789">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600789">
									<gml:posList srsDimension="3">388922.212810 5720429.457426 81.564421 388922.212810 5720429.457426 78.774205 388922.817369 5720429.457426 79.518572 388922.989657 5720429.457426 79.607326 388922.989657 5720429.457426 84.831000 388921.997751 5720429.457426 84.831000 388921.997751 5720429.457426 82.143552 388922.212810 5720429.457426 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1063_469874_229190">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeof11e8371-1d29-487b-a7a3-7dda89f1aef3">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600790">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600790">
									<gml:posList srsDimension="3">388929.097704 5720412.863914 77.911000 388929.097704 5720412.863914 76.520027 388928.952804 5720413.498832 76.911000 388928.952804 5720413.498832 71.911000 388928.642142 5720414.860083 71.911000 388928.642142 5720414.860083 77.911000 388929.097704 5720412.863914 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_840_776728_136529">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo29ec53d0-aa6b-47db-86fc-7bf35dbc36ce">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600791">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600791">
									<gml:posList srsDimension="3">388928.528744 5720412.547054 71.911000 388928.528744 5720412.547054 76.911000 388929.097704 5720412.863914 76.520027 388929.097704 5720412.863914 77.911000 388925.703477 5720410.973632 77.911000 388925.703477 5720410.973632 76.688060 388926.193128 5720411.246324 76.911000 388926.193128 5720411.246324 71.911000 388928.528744 5720412.547054 71.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_145_204960_37082">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo3d331108-e8c0-4864-99ed-e6c60fba7a69">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600792">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600792">
									<gml:posList srsDimension="3">388928.485546 5720415.546249 81.564421 388929.179558 5720415.598376 81.564421 388931.649711 5720415.783907 81.564421 388931.184523 5720421.159989 89.711601 388927.585853 5720420.842806 89.641000 388917.154273 5720420.059300 89.641000 388919.695261 5720418.246896 86.624773 388920.333597 5720417.464452 85.374486 388923.128136 5720416.336449 83.360061 388923.958298 5720415.206212 81.564421 388925.286330 5720415.305959 81.564421 388928.485546 5720415.546249 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1793_501161_340880">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo5b5a4935-e01b-4171-9944-a6dfee16bc91">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600793">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600793">
									<gml:posList srsDimension="3">388914.762329 5720418.457533 92.081000 388919.471757 5720410.891582 81.564421 388918.111154 5720417.626385 87.881000 388920.333597 5720417.464452 85.374486 388919.695261 5720418.246896 86.624773 388915.364442 5720419.355956 92.081000 388914.762329 5720418.457533 92.081000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_225_775400_93309">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoa30e61be-0ebc-41f8-a86c-228b6bb3b000">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600794">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600794">
									<gml:posList srsDimension="3">388912.861281 5720419.103365 92.081000 388905.113576 5720415.421026 81.564421 388910.278274 5720417.080724 87.881000 388910.151977 5720409.980118 81.564421 388913.567540 5720418.340684 92.081000 388912.861281 5720419.103365 92.081000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1751_881534_267696">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo9eb2ab5f-34a7-409a-92ef-0f780ad5c8c2">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600795">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600795">
									<gml:posList srsDimension="3">388908.186267 5720428.600275 81.564421 388907.739000 5720432.837000 77.911000 388905.972133 5720430.374637 81.564421 388908.186267 5720428.600275 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1905_6889_68644">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo85e50f5c-5c8d-4d45-8688-0f053b5496ef">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600796">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600796">
									<gml:posList srsDimension="3">388903.852558 5720428.142770 77.911000 388908.186267 5720428.600275 81.564421 388905.972133 5720430.374637 81.564421 388903.852558 5720428.142770 77.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_712_46358_227499">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeod1bfd549-9cb0-43e8-8714-823d391511d2">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600797">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600797">
									<gml:posList srsDimension="3">388903.851783 5720428.142688 81.564421 388908.186267 5720428.600275 81.564421 388903.852558 5720428.142770 77.911000 388903.851783 5720428.142688 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1583_57945_157656">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeof689b742-5987-4170-a4b8-0fc92659ac0f">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600798">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600798">
									<gml:posList srsDimension="3">388907.670144 5720433.489242 81.564421 388908.186267 5720428.600275 81.564421 388913.700327 5720423.121406 89.265037 388912.663708 5720433.340614 89.330896 388907.670144 5720433.489242 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_240_590700_251993">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo61192490-0fd4-4efd-a6c3-b4d6ed089a02">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600799">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600799">
									<gml:posList srsDimension="3">388908.186267 5720428.600275 81.564421 388907.670144 5720433.489242 81.564421 388907.670144 5720433.489242 79.947456 388907.739000 5720432.837000 80.621000 388907.739000 5720432.837000 77.911000 388908.186267 5720428.600275 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_553_242527_91587">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo3f3493e0-6b92-4815-a020-22fc4cef4375">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600800">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600800">
									<gml:posList srsDimension="3">388906.307332 5720425.644213 87.881000 388908.186267 5720428.600275 81.564421 388903.851783 5720428.142688 81.564421 388906.307332 5720425.644213 87.881000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_295_648866_147309">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeob75180d1-f7f6-4fbd-adab-16180087bd7d">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600801">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600801">
									<gml:posList srsDimension="3">388909.965877 5720421.920415 87.881000 388908.186267 5720428.600275 81.564421 388906.307332 5720425.644213 87.881000 388909.965877 5720421.920415 87.881000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1037_484813_349158">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo42e4625e-2717-42a6-92ab-3601b9bc9f3d">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600802">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600802">
									<gml:posList srsDimension="3">388908.186267 5720428.600275 81.564421 388909.965877 5720421.920415 87.881000 388904.388738 5720422.717946 81.564421 388912.772822 5720419.993877 92.081000 388913.348973 5720420.886328 92.081006 388908.186267 5720428.600275 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_598_809860_19738">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo81584f35-c0a8-4a46-96aa-39329dbf0670">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600803">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600803">
									<gml:posList srsDimension="3">388917.652894 5720429.526100 81.564421 388913.700327 5720423.121406 89.265037 388908.186267 5720428.600275 81.564421 388913.348973 5720420.886328 92.081006 388914.497728 5720420.998675 92.081000 388917.652894 5720429.526100 81.564421 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_932_83877_388055">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeoc7180845-ea08-4384-b32e-8a299ebb167f">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600804">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600804">
									<gml:posList srsDimension="3">388899.583000 5720428.661000 71.681000 388899.583000 5720428.661000 66.415000 388899.289000 5720428.622000 66.415000 388899.289000 5720428.622000 71.681000 388899.583000 5720428.661000 71.681000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:RoofSurface gml:id="UUID_RoofSurface_1923_728537_270208">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo316a7a4d-2435-41ce-902a-201fb6e8c48e">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600805">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600805">
									<gml:posList srsDimension="3">388898.142000 5720427.599000 76.911000 388899.375230 5720427.716430 76.911000 388899.289000 5720428.622000 75.845553 388899.258000 5720428.981000 75.423525 388898.032000 5720428.875000 75.411000 388898.142000 5720427.599000 76.911000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:RoofSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_920_167131_216702">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo1294c6c3-1ec9-49e3-82a9-54744c0d8c86">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600806">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600806">
									<gml:posList srsDimension="3">388899.289000 5720428.622000 71.681000 388899.289000 5720428.622000 75.845553 388899.375230 5720427.716430 76.911000 388899.375230 5720427.716430 71.681000 388899.289000 5720428.622000 71.681000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_857_211428_74596">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo8c1517be-3623-48db-a0f9-60cd7ea91473">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600807">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600807">
									<gml:posList srsDimension="3">388899.289000 5720428.622000 71.681000 388899.289000 5720428.622000 66.415000 388899.258000 5720428.981000 66.415000 388899.258000 5720428.981000 75.423525 388899.289000 5720428.622000 75.845553 388899.289000 5720428.622000 71.681000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_1287_424238_297525">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo2ba88d31-09d4-4869-aa94-bfdbae44f766">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600808">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600808">
									<gml:posList srsDimension="3">388899.258000 5720428.981000 75.423525 388899.258000 5720428.981000 66.415000 388898.032000 5720428.875000 66.415000 388898.032000 5720428.875000 75.411000 388899.258000 5720428.981000 75.423525 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_646_263304_305758">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo6eb7e35d-9ba2-4a90-99d8-8503aea68d92">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600809">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600809">
									<gml:posList srsDimension="3">388899.341000 5720430.978000 71.681000 388899.341000 5720430.978000 66.415000 388899.583000 5720428.661000 66.415000 388899.583000 5720428.661000 71.681000 388899.341000 5720430.978000 71.681000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:boundedBy>
		<bldg:WallSurface gml:id="UUID_WallSurface_689_637213_2129">
			<bldg:lod2MultiSurface>
				<gml:MultiSurface gml:id="IDGeo40b5cae0-0d86-4b1d-a450-6812e7e5f19a">
					<gml:surfaceMember>
						<gml:Polygon gml:id="PolyIDGeo992600810">
							<gml:exterior>
								<gml:LinearRing gml:id="ringID992600810">
									<gml:posList srsDimension="3">388880.044000 5720419.978000 94.841000 388880.044000 5720419.978000 66.415000 388881.179000 5720412.724000 66.415000 388881.179000 5720412.724000 94.841000 388880.611500 5720416.351000 98.782158 388880.044000 5720419.978000 94.841000 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</bldg:lod2MultiSurface>
		</bldg:WallSurface>
	</bldg:boundedBy>
	<bldg:address>
		<core:Address>
			<core:xalAddress>
				<xAL:AddressDetails>
					<xAL:Country>
						<xAL:CountryName>
							Germany
						</xAL:CountryName>
						<xAL:Locality  Type="Town">
							<xAL:LocalityName>
								Waltrop
							</xAL:LocalityName>
							<xAL:Thoroughfare>
								<xAL:ThoroughfareNumber>
									2
								</xAL:ThoroughfareNumber>
								<xAL:ThoroughfareName>
									Kirchplatz
								</xAL:ThoroughfareName>
							</xAL:Thoroughfare>
							<xAL:PostalCode>
								<xAL:PostalCodeNumber>
									45731
								</xAL:PostalCodeNumber>
							</xAL:PostalCode>
						</xAL:Locality>
					</xAL:Country>
				</xAL:AddressDetails>
			</core:xalAddress>
			<core:multiPoint>
				<gml:MultiPoint>
					<gml:pointMember>
						<gml:Point>
							<gml:pos>388908.188 5720419.5 0.0</gml:pos>
						</gml:Point>
					</gml:pointMember>
				</gml:MultiPoint>
			</core:multiPoint>
		</core:Address>
	</bldg:address>
</bldg:Building>
</core:cityObjectMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600873">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600873">54.036 38.0545 55.5322 37.1391 54.4832 40.9119 54.036 38.0545 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600872">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600872">36.6212 43.5461 36.948 43.1373 35.6363 46.827 34.3246 43.1373 35.6363 44.7778 36.6212 43.5461 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600875">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600875">39.2143 52.6555 39.2145 52.6557 38.7669 55.5129 37.7175 51.7403 39.2143 52.6555 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600874">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600874">26.7606 65.748 26.7604 65.7479 25.3588 64.6926 27.4595 64.6926 26.7606 65.748 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600877">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600877">46.0475 17.5273 45.3484 16.4719 47.4484 16.4719 46.0475 17.5273 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600876">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600876">-57.3032 43.7844 -56.9764 43.3756 -58.2881 47.0652 -59.5998 43.3756 -58.2881 45.016 -57.3032 43.7844 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600879">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600879">-13.106 71.56 -14.8597 71.56 -14.178 69.4596 -13.106 69.4596 -13.106 69.983 -13.106 71.56 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600865">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600865">12.8925 10.1114 14.1537 10.1114 14.9075 12.2117 12.8925 12.2117 12.8925 10.1114 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600864">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600864">32.3716 26.3456 31.6726 25.2902 33.7726 25.2902 32.3716 26.3456 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600866">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600866">-12.8925 57.9899 -14.9075 57.9899 -14.1114 55.8895 -12.8925 55.8895 -12.8925 56.4129 -12.8925 57.9899 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600869">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600869">-71.5378 55.127 -69.9434 55.127 -68.9468 57.2274 -71.5378 57.2274 -71.5378 55.127 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600871">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600871">71.5378 12.7163 68.9468 12.7163 69.9433 10.616 71.5378 10.616 71.5378 11.1394 71.5378 12.7163 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600870">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600870">14.8611 26.5393 14.8613 26.5395 14.4136 29.3968 13.3643 25.6241 14.8611 26.5393 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600889">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600889">-13.1971 83.8932 -14.8613 83.8932 -14.2097 81.7928 -13.1971 81.7928 -13.1971 82.3162 -13.1971 83.8932 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600888">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600888">58.4311 9.4181 57.7321 8.3627 59.8321 8.3627 58.4311 9.4181 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600893">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600893">42.5391 129.226 40.7901 129.0981 41.6232 127.053 42.6923 127.1312 42.6541 127.6532 42.5391 129.226 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600895">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600895">87.9046 9.8041 89.3677 8.8367 88.4518 12.644 87.9046 9.8041 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600894">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600894">129.6247 86.2748 129.6245 86.2746 128.2818 85.1453 130.3795 85.2586 129.6247 86.2748 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600883">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600883">13.1971 -15.6431 14.2328 -15.6431 14.8613 -13.5427 13.1971 -13.5427 13.1971 -15.6431 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600882">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600882">13.106 -3.3444 14.1924 -3.3444 14.8597 -1.244 13.106 -1.244 13.106 -3.3444 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600885">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600885">-77.0239 43.8065 -76.6971 43.3977 -78.0088 47.0873 -79.3205 43.3977 -78.0088 45.0381 -77.0239 43.8065 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600884">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600884">69.862 35.62 71.3582 34.7046 70.3092 38.4774 69.862 35.62 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600887">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600887">39.0088 73.9529 39.0086 73.9528 37.607 72.8975 39.7078 72.8975 39.0088 73.9529 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600886">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600886">54.9362 55.1608 54.9364 55.1609 54.4887 58.0182 53.4394 54.2456 54.9362 55.1608 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600840">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600840">-2.9871 23.6963 -2.9869 23.6964 -3.4345 26.5537 -4.4839 22.7811 -2.9871 23.6963 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600843">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600843">-19.5856 15.9485 -19.5853 15.9487 -20.1293 18.7892 -21.0506 14.9832 -19.5856 15.9485 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600842">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600842">80.0249 39.1752 80.3518 38.7664 79.0401 42.456 77.7283 38.7664 79.0401 40.4068 80.0249 39.1752 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600845">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600845">-104.5919 47.4177 -105.3442 46.3996 -103.247 46.2918 -104.5919 47.4177 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600847">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600847">9.3355 -22.9698 9.3353 -22.97 7.9897 -24.0957 10.0877 -23.9879 9.3355 -22.9698 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600846">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600846">-106.1406 40.0107 -104.6762 39.0452 -105.597 42.8512 -106.1406 40.0107 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600833">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600833">-102.4274 102.713 -103.4495 118.8843 -105.1462 102.7131 -102.4274 102.713 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600832">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600832">-41.3218 128.4197 -42.4687 112.2568 -40.1893 112.2568 -41.3218 128.4197 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.682 0.929 0.929</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992600834</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600837">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600837">71.3742 -1.2145 68.4329 -1.2145 69.5642 -3.3149 71.3742 -3.3149 71.3742 -2.7915 71.3742 -1.2145 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600839">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600839">-92.4378 37.3452 -93.1368 36.2899 -91.0368 36.2899 -92.4378 37.3452 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600838">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600838">-89.5406 38.1765 -88.0444 37.2611 -89.0935 41.0338 -89.5406 38.1765 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600856">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600856">-78.7131 28.1934 -79.4121 27.138 -77.3121 27.138 -78.7131 28.1934 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600861">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600861">36.6038 40.6588 38.1 39.7435 37.0509 43.5162 36.6038 40.6588 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600860">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600860">22.0325 49.84 22.0327 49.8401 21.5851 52.6974 20.5357 48.9248 22.0325 49.84 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600863">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600863">13.4105 56.6666 13.4103 56.6665 12.0087 55.6112 14.1095 55.6112 13.4105 56.6666 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600862">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600862">-35.6659 43.7112 -35.339 43.3024 -36.6507 46.9921 -37.9625 43.3024 -36.6507 44.9429 -35.6659 43.7112 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600849">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600849">73.9807 -12.672 71.0466 -12.8772 72.3217 -14.8935 74.1272 -14.7672 74.0907 -14.2451 73.9807 -12.672 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600851">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600851">58.8086 43.4509 59.1355 43.0421 57.8237 46.7317 56.512 43.0421 57.8237 44.6825 58.8086 43.4509 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600850">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600850">-65.4322 83.5897 -64.1571 85.6061 -67.0912 85.8113 -67.2377 83.7161 -65.4322 83.5897 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600852">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600852">20.194 -10.5072 20.1938 -10.5074 18.7922 -11.5626 20.893 -11.5626 20.194 -10.5072 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600855">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600855">-71.8948 35.4619 -70.3986 34.5465 -71.4476 38.3192 -71.8948 35.4619 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600854">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600854">34.1822 -1.3044 34.182 -1.3045 32.7804 -2.3598 34.8811 -2.3598 34.1822 -1.3044 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600937">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600937">-155.235 79.2984 -153.8713 79.2984 -153.0491 81.3988 -155.235 81.3988 -155.235 79.2984 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600936">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600936">155.235 -11.2659 153.0491 -11.2659 153.908 -13.3663 155.235 -13.3663 155.235 -12.8429 155.235 -11.2659 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600931">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600931">-168.1632 16.5711 -168.8622 15.5157 -166.7622 15.5157 -168.1632 16.5711 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600933">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600933">75.1737 43.6672 75.5006 43.2584 74.1889 46.948 72.8771 43.2584 74.1889 44.8988 75.1737 43.6672 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600932">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600932">75.4551 -44.8383 75.4549 -44.8384 74.0533 -45.8936 76.1541 -45.8936 75.4551 -44.8383 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600935">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600935">-152.9747 26.4397 -151.4785 25.5243 -152.5275 29.297 -152.9747 26.4397 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600934">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600934">34.2373 7.8635 34.2375 7.8636 33.7899 10.7209 32.7405 6.9483 34.2373 7.8635 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600955">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600955">167.1122 55.4629 167.439 55.0541 166.1273 58.7437 164.8156 55.0541 166.1273 56.6945 167.1122 55.4629 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600954">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600954">58.8372 -59.4706 55.9834 -59.4706 57.1005 -61.571 58.8372 -61.571 58.8372 -61.0476 58.8372 -59.4706 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600944">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600944">-69.5642 68.9091 -68.4329 71.0095 -71.3742 71.0095 -71.3742 68.9091 -69.5642 68.9091 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600905">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600905">-27.0184 149.2348 -29.8434 149.2348 -28.7715 147.1344 -27.0184 147.1344 -27.0184 147.6578 -27.0184 149.2348 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600904">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600904">-167.063 55.4716 -166.7361 55.0628 -168.0478 58.7524 -169.3596 55.0628 -168.0478 56.7032 -167.063 55.4716 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600907">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600907">118.6746 80.1462 118.6748 80.1463 118.2272 83.0036 117.1778 79.231 118.6746 80.1462 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600906">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600906">150.1669 38.0521 151.6631 37.1367 150.6141 40.9094 150.1669 38.0521 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600909">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600909">125.4428 -13.3644 124.7438 -14.4198 126.8438 -14.4198 125.4428 -13.3644 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600908">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600908">84.4632 125.6585 84.463 125.6584 83.0614 124.6031 85.1621 124.6031 84.4632 125.6585 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600897">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600897">148.796 55.5245 148.7962 55.5247 148.247 58.3642 147.3327 54.5566 148.796 55.5245 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600899">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600899">-56.2593 -58.3557 -55.1758 -58.435 -54.357 -56.3889 -56.106 -56.261 -56.2593 -58.3557 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600898">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600898">-147.9046 40.795 -147.5778 40.3861 -148.8884 44.0761 -150.2013 40.3869 -148.889 42.0269 -147.9046 40.795 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600903">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600903">27.0184 -62.7636 28.7423 -62.7636 29.8434 -60.6632 27.0184 -60.6632 27.0184 -62.7636 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600902">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600902">49.4514 -46.7231 48.6969 -47.7395 50.7939 -47.8519 49.4514 -46.7231 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600921">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600921">144.7176 29.6996 144.7178 29.6998 144.2701 32.557 143.2207 28.7844 144.7176 29.6996 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600923">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600923">170.3937 4.7098 170.3935 4.7097 168.9918 3.6545 171.0926 3.6545 170.3937 4.7098 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600922">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600922">-75.8369 18.8955 -74.3407 17.9802 -75.3897 21.7529 -75.8369 18.8955 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600925">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600925">-42.1133 55.7191 -41.7864 55.3103 -43.0981 58.9999 -44.4099 55.3103 -43.0981 56.9507 -42.1133 55.7191 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600924">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600924">-182.986 15.4631 -181.7751 15.4631 -181.0316 17.5635 -182.986 17.5635 -182.986 15.4631 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600927">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600927">182.986 71.3947 181.0316 71.3947 181.791 69.2943 182.986 69.2943 182.986 69.8177 182.986 71.3947 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600926">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600926">-116.6138 -30.9007 -117.3128 -31.956 -115.2128 -31.956 -116.6138 -30.9007 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600913">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600913">-150.263 96.4828 -150.9621 95.4274 -148.8621 95.4274 -150.263 96.4828 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600915">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600915">-97.1521 23.9799 -97.1519 23.98 -97.5995 26.8373 -98.6489 23.0647 -97.1521 23.9799 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600914">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600914">-57.2524 -40.9622 -57.2526 -40.9623 -58.6542 -42.0176 -56.5534 -42.0176 -57.2524 -40.9622 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600917">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600917">-168.6279 65.5889 -167.1317 64.6735 -168.1808 68.4462 -168.6279 65.5889 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600916">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600916">-58.8372 145.9282 -57.0615 145.9282 -55.9834 148.0286 -58.8372 148.0286 -58.8372 145.9282 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600585">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600585">27.0982 38.7595 46.2159 38.7595 38.0474 62.0006 27.0982 38.7595 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600584">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600584">-25.3745 -5.7892 -37.2278 18.7065 -45.403 -5.7892 -25.3745 -5.7892 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600587">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600587">-143.2356 12.4074 -135.4658 19.8904 -133.2602 25.6724 -125.4478 38.5049 -159.7685 38.5049 -159.7685 6.62 -148.918 6.62 -144.8593 6.62 -143.2356 12.4074 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600586">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600586">135.9436 26.0679 119.4097 26.0679 125.9382 14.8286 133.9628 7.6195 135.9436 26.0679 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600589">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600589">140.8978 76.6739 139.0367 93.9119 137.6435 76.6739 140.8978 76.6739 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600588">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600588">-44.7022 42.5618 -46.5887 25.32 -43.3567 25.32 -44.7022 42.5618 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600591">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600591">95.1164 28.7881 93.5539 46.0623 92.1636 28.7881 95.1164 28.7881 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600590">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600590">159.8455 48.3881 158.5272 65.6679 157.1057 48.3881 159.8455 48.3881 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600577">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600577">-56.4088 81.2539 -72.6526 58.9921 -47.7573 75.7336 -45.4428 81.2908 -56.4088 81.2539 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600576">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600576">-85.3835 -14.6116 -70.6277 -14.6116 -78.0056 11.9406 -85.4155 0.5411 -85.3835 -14.6116 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.682 0.929 0.929</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992600579</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600578">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600578">-120.2167 27.2541 -119.9028 33.2147 -121.5888 30.8128 -122.7869 33.2147 -122.9478 27.2544 -122.7869 27.2543 -120.2167 27.2541 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600581">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600581">63.4227 79.0204 64.4181 79.1152 64.1043 82.1194 63.1398 81.9915 63.4227 79.0204 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600580">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600580">71.6276 58.0367 85.3835 58.0367 85.3657 65.6015 77.8919 65.7067 71.6276 58.0367 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600583">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600583">-49.4417 -10.5273 -57.8231 15.0075 -65.9201 -10.5273 -49.4417 -10.5273 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600582">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600582">-71.9954 64.0493 -76.6204 72.8061 -82.659 64.0493 -75.016 64.0493 -71.9954 64.0493 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600593">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600593">-157.1057 67.4567 -158.5272 84.6687 -160.0584 67.4567 -157.1057 67.4567 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600592">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600592">-141.5402 41.7769 -142.9279 59.0011 -144.4829 41.7769 -141.5402 41.7769 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600595">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600595">40.4747 91.2919 39.0015 108.5233 37.5296 91.2919 40.4747 91.2919 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600594">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600594">-86.7738 86.8077 -88.3061 104.0339 -89.5453 86.8077 -86.7738 86.8077 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600597">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600597">155.5979 -7.8823 153.7546 11.7909 145.2779 11.7909 148.0873 6.4353 155.5979 -7.8823 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600596">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600596">59.3754 -4.4197 67.2846 -4.4197 67.3704 -2.3947 67.758 -1.4826 59.8857 17.6669 52.3625 0.7062 55.5017 -4.4197 59.3754 -4.4197 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600599">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600599">-153.7546 78.5089 -154.6519 65.2968 -153.3901 62.4148 -147.9448 73.2178 -145.2779 78.5089 -153.7546 78.5089 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600598">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600598">113.0371 56.9806 109.2682 56.6121 107.378 53.6841 107.6682 50.7624 109.9853 48.2602 113.9052 48.6436 115.8807 51.5911 115.5303 54.452 113.0371 56.9806 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600664">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600664">69.5276 14.9081 70.7972 15.0853 70.5183 18.0823 69.2369 17.9603 69.5276 14.9081 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.682 0.929 0.929</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992600811</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.682 0.929 0.929</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992600813</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.682 0.929 0.929</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992600812</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600815">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600815">107.6683 50.7623 108.5593 51.1805 108.3338 53.4487 107.3781 53.684 107.6683 50.7623 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600814">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600814">109.9854 48.2601 110.3105 49.1892 108.5593 51.1805 107.6683 50.7624 109.9854 48.2601 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600801">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600801">-25.9966 8.6973 -6.2717 -14.8565 -8.8696 8.6973 -25.9966 8.6973 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600800">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600800">-93.2719 39.5996 -100.4205 17.0069 -86.1207 17.0069 -93.2719 39.5996 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600803">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600803">-131.0722 -5.3062 -116.1208 26.7008 -99.8655 -5.3062 -114.2597 38.4051 -118.0466 38.4051 -131.0722 -5.3062 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600802">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600802">-118.9018 59.4794 -103.6565 86.1521 -95.9306 59.4794 -103.3412 103.8872 -106.8263 103.8872 -118.9018 59.4794 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600805">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600805">-66.5633 -20.9858 -70.6277 -20.9858 -70.6277 -25.5821 -70.6381 -27.4027 -66.6009 -27.4568 -66.5633 -20.9858 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600825">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600825">113.9053 48.6436 113.4102 49.4943 110.3105 49.1892 109.9854 48.2602 113.9053 48.6436 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.682 0.929 0.929</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992600824</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600827">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600827">117.7125 97.6222 116.2315 113.8064 114.5977 97.6222 117.7125 97.6222 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600826">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600826">34.1915 129.2366 33.2568 113.061 35.9086 113.061 34.1915 129.2366 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600829">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600829">38.8731 66.2478 37.8615 82.4104 36.6472 66.2478 38.8731 66.2478 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600828">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600828">106.0629 76.4273 104.9271 92.5956 103.282 76.4273 106.0629 76.4273 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600831">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600831">-114.6982 80.7162 -116.2328 96.9037 -117.6612 80.7162 -114.6982 80.7162 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600830">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600830">-37.317 66.482 -39.0386 82.6409 -40.1073 66.482 -37.317 66.482 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600817">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600817">115.5303 54.4519 114.6393 54.0338 114.925 51.8263 115.8807 51.5911 115.5303 54.4519 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600816">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600816">107.3782 53.6845 108.3339 53.4492 109.7635 55.7618 109.2685 56.6125 107.3782 53.6845 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600819">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600819">115.8807 51.5911 114.9249 51.8263 113.4102 49.4942 113.9052 48.6435 115.8807 51.5911 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600818">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600818">113.0371 56.9805 112.712 56.0515 114.6393 54.0338 115.5303 54.4519 113.0371 56.9805 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.682 0.929 0.929</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992600821</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.682 0.929 0.929</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992600820</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.682 0.929 0.929</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992600823</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600822">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600822">109.2678 56.6121 109.7628 55.7614 112.7116 56.0517 113.0367 56.9807 109.2678 56.6121 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600793">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600793">103.8218 25.3476 91.8036 -18.252 107.6734 7.9352 111.2914 -2.4563 112.2579 2.7272 107.3702 25.3476 103.8218 25.3476 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600792">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600792">161.4105 56.3909 163.6938 56.3909 171.8208 56.3909 171.62 88.4511 159.7685 88.1733 125.4478 88.1733 133.3156 76.304 135.2117 71.384 144.0772 63.457 146.5155 56.3909 150.8848 56.3909 161.4105 56.3909 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600795">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600795">-20.8204 -39.3374 -10.9829 -54.902 -11.5114 -39.3374 -20.8204 -39.3374 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600794">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600794">35.9091 136.1597 27.5026 92.4347 35.0202 118.6973 51.8315 92.4347 39.3194 136.1597 35.9091 136.1597 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600796">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600796">10.6639 100.6439 20.8204 116.2941 11.5114 116.2941 10.6639 100.6439 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600798">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600798">-87.9032 95.8885 -71.7741 95.8885 -51.9989 125.8488 -85.6983 126.105 -87.9032 95.8885 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600443">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600443">178.5302 31.5026 178.1004 37.0932 174.393 36.8012 174.8392 31.1745 178.5302 31.5026 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600445">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600445">-39.9354 81.4667 -38.1831 77.2635 -11.3163 59.2541 -19.5047 81.5373 -39.9354 81.4667 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600444">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600444">50.0974 39.271 66.6333 39.271 58.3254 61.9813 50.0974 39.271 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.682 0.929 0.929</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992600447</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600446">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600446">70.6277 39.2501 85.6206 39.2501 85.6025 52.1163 78.1146 61.7787 70.6277 39.2501 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600489">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600489">-46.711 -31.6919 -45.6848 -37.7946 -41.5227 -25.4812 -26.1333 -51.801 -38.7876 -7.9807 -42.3386 -7.9807 -46.711 -31.6919 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600488">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600488">7.5198 -6.2994 16.4657 -6.2994 20.1792 -6.2994 24.191 0.0078 17.5468 15.8876 7.5198 -6.2994 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600491">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600491">-57.7098 71.1706 -71.2407 47.5601 -66.8456 47.5601 -57.7098 71.1706 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600490">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600490">58.4609 69.7645 60.0542 65.3344 76.7687 65.3723 78.185 69.7002 81.5035 65.3344 89.3989 65.3344 97.2875 76.579 26.1835 76.5623 26.8868 46.8282 37.9527 69.8292 39.6151 65.1815 56.6309 64.6883 58.4609 69.7645 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600493">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600493">4.3665 78.5975 -3.4077 92.9466 0.6697 78.5975 4.3665 78.5975 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600492">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600492">-84.8459 -14.6041 -92.4751 -0.4458 -98.962 -14.6041 -84.8459 -14.6041 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600495">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600495">-10.2399 46.1475 -19.8974 69.9543 -15.5005 46.1475 -10.2399 46.1475 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600494">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600494">93.4582 44.3971 85.7333 30.0214 100.4894 30.0214 93.4582 44.3971 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600481">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600481">-29.5302 -10.6084 -29.1246 -40.5528 -25.6115 -40.5528 -29.5302 -10.6084 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600480">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600480">-93.8576 -1.9895 -95.3698 -32.1513 -84.4578 -32.1513 -93.8576 -1.9895 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600483">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600483">101.0019 3.291 97.0654 -26.5817 100.4408 -26.5817 101.0019 3.291 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600482">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600482">41.8229 -2.2957 36.5035 -31.6963 47.5181 -31.6963 41.8229 -2.2957 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600485">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600485">152.0108 26.0697 142.6565 -4.0016 153.9057 -4.0016 152.0108 26.0697 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600484">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600484">171.4039 19.0459 175.0108 50.3313 168.0018 19.0459 171.4039 19.0459 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600487">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600487">-71.1554 -16.422 -62.0352 -8.7893 -56.4787 0.708 -72.8517 0.708 -71.1554 -16.422 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600486">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600486">37.1206 -6.644 31.7816 -29.1622 38.6972 -16.714 45.4604 -30.1291 40.0029 -6.644 37.1206 -6.644 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600505">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600505">-135.3312 31.8693 -134.8931 24.6142 -131.4782 25.1526 -131.8075 31.8693 -133.1028 29.0701 -135.3312 31.8693 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600504">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600504">-76.3812 -37.8987 -83.8548 -38.0145 -83.7207 -46.337 -69.905 -46.337 -76.3812 -37.8987 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600507">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600507">-138.4836 38.4719 -144.9125 18.7908 -143.8592 15.7721 -131.0722 15.7721 -138.4836 38.4719 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600506">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600506">-55.0066 -57.1389 -55.6171 -63.2947 -52.2672 -64.0716 -51.6227 -57.1389 -53.8399 -59.6701 -55.0066 -57.1389 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.682 0.929 0.929</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992600509</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600508">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600508">19.877 33.2524 11.1987 10.6811 38.0225 28.872 39.8118 33.2266 19.877 33.2524 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600511">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600511">-58.1807 75.9963 -72.757 53.4174 -47.9571 69.6622 -45.3458 76.013 -54.6498 76.0009 -58.1807 75.9963 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600510">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600510">44.8526 41.7287 47.8388 34.4734 72.768 18.0406 60.1345 40.5632 44.8526 41.7287 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600497">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600497">-81.1567 91.2956 -88.587 77.0319 -84.7659 77.0319 -81.1567 91.2956 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600496">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600496">-16.2866 45.901 -21.0058 69.646 -39.7931 69.674 -30.5012 45.901 -16.2866 45.901 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600499">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600499">88.5978 -23.9585 86.6149 5.6478 52.9155 5.3967 72.5692 -23.9585 88.5978 -23.9585 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600498">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600498">-72.322 77.8561 -78.9228 92.0715 -82.718 77.8561 -72.322 77.8561 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600501">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600501">71.8879 12.3284 85.6608 12.3284 85.7128 21.6351 79.8005 21.6951 71.8879 12.3284 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600500">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600500">15.6618 -16.4844 30.3795 -16.4844 37.8641 -10.0613 39.6333 -4.8707 19.2027 -4.7843 15.6618 -16.4844 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600503">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600503">-6.8678 76.1851 -11.4689 65.8202 -8.4589 65.8202 -0.317 65.8202 -6.8678 76.1851 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600502">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600502">-79.7259 16.4575 -85.6383 16.4048 -85.6206 8.2364 -71.9148 8.2364 -79.7259 16.4575 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600457">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600457">72.8517 122.6911 56.4787 122.6911 75.1934 98.4552 72.8517 122.6911 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600456">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600456">72.3783 30.78 69.087 6.9617 89.2124 30.78 72.3783 30.78 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600459">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600459">-135.9436 94.9215 -138.5439 71.1845 -119.4097 94.9215 -135.9436 94.9215 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600458">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600458">147.3967 49.671 139.2305 72.6082 131.0605 49.671 147.3967 49.671 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600461">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600461">8.8696 115.2906 11.3065 91.7253 25.9966 115.2906 8.8696 115.2906 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600460">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600460">45.1518 -4.4119 47.4721 -11.2257 54.4493 -16.1448 63.064 -16.1448 56.1178 -4.457 45.1518 -4.4119 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600463">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600463">-20.6714 97.2586 -30.3795 75.2926 -15.6618 75.2926 -10.9853 75.2926 -20.6714 97.2586 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600462">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600462">-54.4493 75.0862 -63.3832 97.0282 -72.334 75.0862 -63.064 75.0862 -54.4493 75.0862 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.682 0.929 0.929</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992600449</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.682 0.929 0.929</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992600448</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.682 0.929 0.929</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992600451</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.682 0.929 0.929</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992600450</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.682 0.929 0.929</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992600453</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.682 0.929 0.929</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992600452</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600455">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600455">100.3524 49.6787 92.9463 72.5543 85.6637 49.6787 100.3524 49.6787 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.682 0.929 0.929</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992600454</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600473">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600473">-47.3176 68.8555 -62.5929 67.6066 -70.2378 43.4721 -56.197 43.4721 -47.3176 68.8555 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600472">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600472">27.343 27.2887 27.7437 43.0091 10.6528 27.2887 27.343 27.2887 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600475">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600475">-11.9807 -46.73 -28.6791 -31.267 -30.4886 -46.73 -11.9807 -46.73 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600474">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600474">-52.9393 40.6996 -66.8648 22.699 -52.967 22.699 -52.9393 40.6996 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600477">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600477">161.2059 39.263 158.929 39.092 159.1 36.8151 161.3769 36.9861 161.2059 39.263 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600476">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600476">177.1325 11.2336 180.4101 11.6076 179.4521 21.5781 175.9908 21.2664 177.1325 11.2336 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600479">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600479">-143.5742 10.3949 -140.1209 -21.0084 -136.638 -21.0084 -143.5742 10.3949 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600478">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600478">-172.7269 8.9555 -161.3369 8.9555 -160.8755 40.837 -172.7269 41.1157 -172.7269 8.9555 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600465">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600465">114.1605 89.6321 100.3379 46.3035 116.2025 77.8475 131.0605 46.3035 118.0991 89.6321 114.1605 89.6321 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600464">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600464">-72.3783 91.7122 -89.2124 91.7122 -75.7614 67.9359 -72.3783 91.7122 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600467">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600467">-10.3449 95.8885 6.6088 95.8885 4.531 125.419 -29.3028 125.6762 -10.3449 95.8885 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600466">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600466">30.2194 5.2276 -3.6144 4.9756 -6.0221 -23.9585 11.1418 -23.9585 30.2194 5.2276 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600469">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600469">-78.6552 79.0129 -68.2805 63.1447 -68.9815 79.0129 -78.6552 79.0129 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600468">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600468">68.1915 -20.5745 78.6552 -4.5909 68.9815 -4.5909 68.1915 -20.5745 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600471">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600471">-30.5032 39.0445 -29.5371 21.0573 -28.3524 21.0573 -15.817 21.0573 -30.5032 39.0445 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600470">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600470">0.0017 78.5039 -4.1371 92.8354 -10.9853 78.5039 0.0017 78.5039 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600553">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600553">-41.7616 -72.1774 -41.7383 -66.2302 -43.3993 -68.9706 -44.9274 -66.2302 -44.8474 -72.2807 -41.7616 -72.1774 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600552">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600552">21.0517 40.8648 11.1733 18.2974 37.9625 36.326 39.839 40.8914 21.0517 40.8648 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600555">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600555">7.3561 -67.6643 7.2242 -73.2265 10.2539 -73.4648 10.4074 -67.6643 8.8779 -69.3209 7.3561 -67.6643 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600554">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600554">-139.967 -25.749 -139.8183 -31.5442 -136.7553 -31.6309 -136.7036 -25.749 -138.6517 -27.1233 -139.967 -25.749 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600557">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600557">-25.6115 -87.742 -29.1246 -87.742 -29.2416 -94.2361 -25.7881 -94.4783 -25.6115 -87.742 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600556">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600556">-136.638 -44.7923 -140.1209 -44.7923 -140.2567 -51.3456 -136.6765 -51.1653 -136.638 -44.7923 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600559">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600559">168.0018 5.7033 168.0018 -0.7969 171.4039 -0.7969 171.4039 5.7033 168.0018 5.7033 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600558">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600558">100.4408 -68.8963 97.0654 -68.8963 96.8776 -75.1545 100.2384 -75.3993 100.4408 -68.8963 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600545">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600545">-101.9883 85.5937 -103.7608 68.6405 -102.1978 68.6405 -100.2159 68.6405 -101.9883 85.5937 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600544">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600544">3.3907 95.9023 1.6182 78.9491 5.1632 78.9491 3.3907 95.9023 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600547">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600547">-147.624 59.4962 -147.624 42.5431 -145.8515 42.5431 -147.624 59.4962 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600546">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600546">-147.4729 41.0707 -147.6424 58.023 -149.32 41.0707 -147.4729 41.0707 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600549">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600549">-106.7835 32.8975 -108.556 15.9443 -105.1145 15.9443 -106.7835 32.8975 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600548">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600548">-1.6182 4.4254 -3.3907 21.3785 -5.1632 4.4254 -1.6182 4.4254 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.682 0.929 0.929</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992600551</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600550">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600550">-39.8671 76.0024 -37.8129 71.0126 -11.3306 53.4568 -19.9324 76.0281 -39.8671 76.0024 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600569">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600569">-45.4138 -21.3625 -49.4511 -21.3625 -49.4521 -27.8336 -45.4148 -27.8336 -45.4138 -21.3625 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600568">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600568">64.3217 56.7471 64.5629 62.814 63.1282 60.4293 61.7005 62.814 61.6484 56.8743 64.3217 56.7471 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600571">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600571">94.8769 -76.9003 94.8769 -69.9426 93.1676 -71.7561 91.4583 -69.9426 91.4583 -76.9003 94.8769 -76.9003 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600570">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600570">-58.6048 -19.8512 -60.0091 -13.2423 -61.0051 -15.6106 -63.8258 -13.2423 -62.4313 -20.9221 -58.6048 -19.8512 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600573">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600573">50.4864 32.608 46.4837 32.608 46.334 26.4131 50.3341 26.1821 50.4864 32.608 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600572">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600572">7.309 44.9166 7.8376 37.0129 11.8685 37.4039 11.5857 44.9166 9.7638 42.1857 7.309 44.9166 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600575">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600575">45.2904 33.3437 47.5948 27.7515 72.5351 10.7451 58.1254 33.3271 54.5945 33.3317 45.2904 33.3437 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600574">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600574">70.6277 33.6448 66.6715 33.6448 66.6568 27.2172 70.6266 27.1931 70.6277 28.8785 70.6277 33.6448 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600561">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600561">122.3334 -43.6031 122.4698 -47.3276 124.473 -47.2313 124.4322 -43.6031 123.5074 -45.8665 122.3334 -43.6031 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600560">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600560">153.1579 24.2068 153.1579 31.1487 151.4693 30.117 149.7806 31.1487 149.7806 24.2068 153.1579 24.2068 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600563">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600563">159.0588 10.1929 156.9891 10.1929 156.9911 6.3477 159.0623 6.3535 159.0588 10.1929 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600562">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600562">168.5674 10.2113 166.4632 10.2113 166.465 6.3844 168.5659 6.385 168.5674 10.2113 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600565">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600565">145.9629 25.7898 143.6168 25.7898 146.1431 21.6747 147.8232 24.5049 147.7974 25.7898 145.9629 25.7898 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600564">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600564">112.6462 62.9745 112.6628 59.1911 114.718 59.2281 114.8137 62.9745 113.7117 60.7408 112.6462 62.9745 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600567">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600567">103.1778 -29.8157 102.6357 -23.5772 101.3621 -26.0214 99.1149 -23.5772 99.6423 -30.4398 103.1778 -29.8157 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600566">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600566">82.478 69.2721 82.8161 76.1484 80.8888 73.8786 79.5456 76.1484 78.9669 69.8616 82.478 69.2721 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600521">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600521">93.841 -24.3022 89.4029 -43.5568 98.0594 -43.5568 93.841 -24.3022 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600520">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600520">168.5722 7.8155 177.6529 7.8163 172.1457 19.7519 154.6766 19.7947 147.8759 7.8163 156.975 7.8186 159.0447 7.8149 166.468 7.8142 168.5722 7.8155 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600523">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600523">-165.3859 11.5828 -162.4425 11.5828 -161.8397 15.1013 -159.496 17.9796 -158.9554 17.645 -162.0812 30.5132 -165.3859 11.5828 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600522">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600522">-70.5156 -37.4188 -75.6945 -18.9131 -82.4821 -37.4188 -70.5156 -37.4188 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600527">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600527">-93.2117 -17.0175 -96.6117 -36.9153 -89.0783 -22.8544 -85.9512 -17.0175 -93.2117 -17.0175 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600526">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600526">159.0707 29.2521 151.4079 24.9847 151.9028 20.1137 148.5428 19.7723 148.0479 24.6433 145.6148 25.4109 146.421 18.8287 148.2618 18.9805 150.5457 17.1951 176.2134 19.3105 175.9908 21.2664 179.4521 21.5781 179.4094 21.9718 179.8392 22.0112 178.9993 31.0794 178.597 31.0425 178.5676 31.063 178.5302 31.5026 174.8392 31.1745 174.393 36.8012 174.3176 37.7428 171.3212 36.1316 169.3101 39.8717 161.2059 39.263 161.3769 36.9861 159.4427 36.8408 160.462 32.3748 164.9547 30.373 163.5634 27.2504 159.0707 29.2521 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.682 0.929 0.929</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992600513</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.682 0.929 0.929</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992600512</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600515">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600515">-42.8069 135.0923 -54.4194 91.5938 -47.4402 96.3612 -42.3639 110.7908 -37.8335 97.8237 -30.3615 91.5938 -39.8709 135.0923 -42.8069 135.0923 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600514">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600514">-97.2956 42.1824 -88.5009 29.6671 -82.4658 29.682 -78.1336 35.4303 -76.2978 29.6971 -60.516 29.736 -58.4711 35.4282 -56.1576 28.963 -40.1462 29.1279 -37.4674 36.5023 -25.4897 12.4255 -26.1916 42.1657 -97.2956 42.1824 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600517">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600517">6.4098 121.3955 3.2439 116.7437 5.4512 113.3507 5.4775 110.5443 6.2534 110.5625 14.6304 110.3431 6.4098 121.3955 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:X3DMaterial>
				<app:diffuseColor>0.682 0.929 0.929</app:diffuseColor>
				<app:transparency>0.0</app:transparency>
				<app:target>#PolyIDGeo992600516</app:target>
			</app:X3DMaterial>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600519">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600519">-8.8479 -74.345 -0.7189 -74.3433 -7.8795 -64.3071 -16.3685 -74.3433 -8.8479 -74.345 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600518">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600518">-154.6856 24.9604 -172.1548 24.9201 -177.2041 13.6726 -151.4494 13.6726 -151.8479 14.2383 -151.9884 19.6806 -151.6704 20.2095 -154.6856 24.9604 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600537">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600537">-101.5961 -44.7495 -89.5073 -28.489 -100.6159 -28.489 -101.5961 -44.7495 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600536">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600536">89.5073 102.0015 99.6192 85.6452 100.6159 102.0015 89.5073 102.0015 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600539">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600539">0.2482 -38.421 -3.5855 -45.4786 -1.6182 -45.4786 0.8885 -49.8833 0.8885 -52.9979 7.5198 -52.9979 0.2482 -38.421 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600538">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600538">131.0873 30.3258 145.9629 30.3258 143.4372 34.723 143.3848 37.6573 139.2431 44.5387 131.0873 30.3258 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600541">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600541">147.624 57.7845 147.624 40.8314 149.3965 40.8314 147.624 57.7845 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600540">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600540">101.9883 31.6871 100.2159 14.734 103.7608 14.734 101.9883 31.6871 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600543">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600543">106.7835 84.3833 105.011 67.4302 108.4697 67.4302 106.7835 84.3833 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600542">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600542">147.6551 59.0257 145.9629 42.0731 147.7974 42.0731 147.6551 59.0257 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600529">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600529">-6.2758 -53.0099 0.4417 -53.0099 0.4928 -52.3571 2.7508 -53.0099 -0.8198 -33.142 -6.2758 -53.0099 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600528">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600528">132.6814 83.783 131.2216 64.7005 135.6117 64.7005 132.6814 83.783 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600531">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600531">-129.9058 -11.4732 -137.3958 2.5792 -144.7008 -11.4732 -129.9058 -11.4732 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600530">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600530">85.9512 103.9468 88.6664 98.3633 95.2076 84.9129 93.2117 103.9468 85.9512 103.9468 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600533">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600533">156.4183 5.9472 155.9346 16.8339 145.8029 16.5938 155.2692 3.0424 155.2938 4.3416 156.4183 5.9472 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600532">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600532">77.6403 -38.0979 79.6641 -42.2719 88.1298 -42.2719 81.1178 -28.0711 76.3906 -37.8898 77.6403 -38.0979 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600535">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600535">146.9378 30.4042 142.5956 11.5927 155.3419 11.5927 146.9378 30.4042 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<app:appearanceMember>
	<app:Appearance>
		<app:surfaceDataMember>
			<app:ParameterizedTexture>
				<app:imageURI>texturen/Granite 01.jpg</app:imageURI>
				<app:textureType>unknown</app:textureType>
				<app:wrapMode>wrap</app:wrapMode>
				<app:borderColor>0 0 0 1</app:borderColor>
				<app:target uri="#PolyIDGeo992600534">
					<app:TexCoordList>
						<app:textureCoordinates ring="#ringID992600534">-154.4436 47.3573 -144.6866 63.0339 -154.8183 63.2767 -154.4436 47.3573 </app:textureCoordinates>
					</app:TexCoordList>
				</app:target>
			</app:ParameterizedTexture>
		</app:surfaceDataMember>
	</app:Appearance>
</app:appearanceMember>
<core:cityObjectMember>
<bldg:Building gml:id="DENW52AL00bvcpDb">
	<core:creationDate>2018-02-11</core:creationDate>
	<core:externalReference>
		<core:informationSystem>http://repository.gdi-de.org/schemas/adv/citygml/fdv/art.htm#_9100</core:informationSystem>
		<core:externalObject>
			<core:name>DENW52AL00bvcpDb</core:name>
		</core:externalObject>
	</core:externalReference>
	<gen:stringAttribute name="Gemeindeschluessel">
		<gen:value>05562036</gen:value>
	</gen:stringAttribute>
	<bldg:consistsOfBuildingPart>
		<bldg:BuildingPart gml:id="UUID_d55adef6-348d-460e-bb6f-8e3163fb4d73">
			<core:creationDate>2018-02-11</core:creationDate>
			<gen:stringAttribute name="DatenquelleDachhoehe">
				<gen:value>1000</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="DatenquelleLage">
				<gen:value>1000</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="DatenquelleBodenhoehe">
				<gen:value>1100</gen:value>
			</gen:stringAttribute>
			<bldg:measuredHeight uom="urn:adv:uom:m">11.466</bldg:measuredHeight>
			<bldg:lod2Solid>
				<gml:Solid gml:id="IDGeo9ead94b3-6fc9-4e87-9cdd-c8514910fbcb">
					<gml:exterior>
						<gml:CompositeSurface>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459399"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459415"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459409"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459405"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459411"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459412"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459403"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459407"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459408"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459400"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459414"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459406"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459401"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459410"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459402"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459416"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459404"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459398"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459413"/>
						</gml:CompositeSurface>
					</gml:exterior>
				</gml:Solid>
			</bldg:lod2Solid>
			<bldg:lod2TerrainIntersection>
				<gml:MultiCurve>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388871.913 5720393.237 68.79</gml:pos>
							<gml:pos>388872.0 5720393.094 68.783</gml:pos>
							<gml:pos>388872.035 5720393.035 68.78</gml:pos>
							<gml:pos>388872.057 5720393.0 68.778</gml:pos>
							<gml:pos>388872.413 5720392.413 68.746</gml:pos>
							<gml:pos>388872.664 5720392.0 68.723</gml:pos>
							<gml:pos>388872.791 5720391.791 68.708</gml:pos>
							<gml:pos>388873.0 5720391.446 68.688</gml:pos>
							<gml:pos>388873.169 5720391.169 68.672</gml:pos>
							<gml:pos>388873.271 5720391.0 68.662</gml:pos>
							<gml:pos>388873.546 5720390.546 68.631</gml:pos>
							<gml:pos>388873.878 5720390.0 68.594</gml:pos>
							<gml:pos>388873.924 5720389.924 68.587</gml:pos>
							<gml:pos>388874.0 5720389.799 68.578</gml:pos>
							<gml:pos>388874.302 5720389.302 68.542</gml:pos>
							<gml:pos>388874.485 5720389.0 68.525</gml:pos>
							<gml:pos>388874.68 5720388.68 68.504</gml:pos>
							<gml:pos>388874.992 5720388.165 68.478</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388871.57 5720393.047 68.794</gml:pos>
							<gml:pos>388871.913 5720393.237 68.79</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388871.21 5720393.65 68.823</gml:pos>
							<gml:pos>388871.374 5720393.374 68.81</gml:pos>
							<gml:pos>388871.57 5720393.047 68.794</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388871.21 5720393.65 68.823</gml:pos>
							<gml:pos>388871.537 5720393.845 68.819</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388870.975 5720394.77 68.856</gml:pos>
							<gml:pos>388871.0 5720394.729 68.855</gml:pos>
							<gml:pos>388871.275 5720394.275 68.837</gml:pos>
							<gml:pos>388871.443 5720394.0 68.827</gml:pos>
							<gml:pos>388871.537 5720393.845 68.819</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388870.705 5720395.214 68.873</gml:pos>
							<gml:pos>388870.835 5720395.0 68.865</gml:pos>
							<gml:pos>388870.897 5720394.897 68.861</gml:pos>
							<gml:pos>388870.975 5720394.77 68.856</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388866.492 5720402.144 69.045</gml:pos>
							<gml:pos>388866.58 5720402.0 69.045</gml:pos>
							<gml:pos>388866.739 5720401.739 69.036</gml:pos>
							<gml:pos>388867.0 5720401.308 69.013</gml:pos>
							<gml:pos>388867.117 5720401.117 69.007</gml:pos>
							<gml:pos>388867.188 5720401.0 69.004</gml:pos>
							<gml:pos>388867.495 5720400.495 68.99</gml:pos>
							<gml:pos>388867.795 5720400.0 68.968</gml:pos>
							<gml:pos>388867.873 5720399.873 68.965</gml:pos>
							<gml:pos>388868.0 5720399.664 68.963</gml:pos>
							<gml:pos>388868.251 5720399.251 68.957</gml:pos>
							<gml:pos>388868.403 5720399.0 68.958</gml:pos>
							<gml:pos>388868.629 5720398.629 68.959</gml:pos>
							<gml:pos>388869.0 5720398.019 68.95</gml:pos>
							<gml:pos>388869.007 5720398.007 68.95</gml:pos>
							<gml:pos>388869.011 5720398.0 68.95</gml:pos>
							<gml:pos>388869.385 5720397.385 68.936</gml:pos>
							<gml:pos>388869.619 5720397.0 68.921</gml:pos>
							<gml:pos>388869.763 5720396.763 68.915</gml:pos>
							<gml:pos>388870.0 5720396.374 68.91</gml:pos>
							<gml:pos>388870.141 5720396.141 68.906</gml:pos>
							<gml:pos>388870.227 5720396.0 68.903</gml:pos>
							<gml:pos>388870.519 5720395.519 68.885</gml:pos>
							<gml:pos>388870.705 5720395.214 68.873</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388866.492 5720402.144 69.045</gml:pos>
							<gml:pos>388866.577 5720402.18 69.038</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388866.559 5720402.211 69.038</gml:pos>
							<gml:pos>388866.577 5720402.18 69.038</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388866.559 5720402.211 69.038</gml:pos>
							<gml:pos>388867.0 5720402.42 69.003</gml:pos>
							<gml:pos>388867.796 5720402.796 68.932</gml:pos>
							<gml:pos>388868.0 5720402.893 68.918</gml:pos>
							<gml:pos>388868.227 5720403.0 68.912</gml:pos>
							<gml:pos>388869.0 5720403.366 68.909</gml:pos>
							<gml:pos>388869.694 5720403.694 68.871</gml:pos>
							<gml:pos>388870.0 5720403.839 68.852</gml:pos>
							<gml:pos>388870.341 5720404.0 68.84</gml:pos>
							<gml:pos>388871.0 5720404.312 68.823</gml:pos>
							<gml:pos>388871.592 5720404.592 68.808</gml:pos>
							<gml:pos>388872.0 5720404.785 68.798</gml:pos>
							<gml:pos>388872.455 5720405.0 68.791</gml:pos>
							<gml:pos>388873.0 5720405.258 68.783</gml:pos>
							<gml:pos>388873.489 5720405.489 68.77</gml:pos>
							<gml:pos>388874.0 5720405.731 68.76</gml:pos>
							<gml:pos>388874.569 5720406.0 68.754</gml:pos>
							<gml:pos>388875.0 5720406.204 68.754</gml:pos>
							<gml:pos>388875.387 5720406.387 68.746</gml:pos>
							<gml:pos>388876.0 5720406.677 68.737</gml:pos>
							<gml:pos>388876.683 5720407.0 68.733</gml:pos>
							<gml:pos>388877.0 5720407.15 68.728</gml:pos>
							<gml:pos>388877.285 5720407.285 68.727</gml:pos>
							<gml:pos>388878.0 5720407.623 68.72</gml:pos>
							<gml:pos>388878.562 5720407.889 68.698</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388878.562 5720407.889 68.698</gml:pos>
							<gml:pos>388878.684 5720407.684 68.693</gml:pos>
							<gml:pos>388879.0 5720407.156 68.714</gml:pos>
							<gml:pos>388879.058 5720407.058 68.716</gml:pos>
							<gml:pos>388879.093 5720407.0 68.714</gml:pos>
							<gml:pos>388879.432 5720406.432 68.694</gml:pos>
							<gml:pos>388879.691 5720406.0 68.72</gml:pos>
							<gml:pos>388879.806 5720405.806 68.718</gml:pos>
							<gml:pos>388880.0 5720405.482 68.72</gml:pos>
							<gml:pos>388880.18 5720405.18 68.706</gml:pos>
							<gml:pos>388880.288 5720405.0 68.714</gml:pos>
							<gml:pos>388880.554 5720404.554 68.713</gml:pos>
							<gml:pos>388880.885 5720404.0 68.73</gml:pos>
							<gml:pos>388880.928 5720403.928 68.73</gml:pos>
							<gml:pos>388881.0 5720403.808 68.732</gml:pos>
							<gml:pos>388881.302 5720403.302 68.707</gml:pos>
							<gml:pos>388881.483 5720403.0 68.726</gml:pos>
							<gml:pos>388881.676 5720402.676 68.716</gml:pos>
							<gml:pos>388882.0 5720402.134 68.736</gml:pos>
							<gml:pos>388882.05 5720402.05 68.732</gml:pos>
							<gml:pos>388882.08 5720402.0 68.735</gml:pos>
							<gml:pos>388882.424 5720401.424 68.703</gml:pos>
							<gml:pos>388882.432 5720401.412 68.703</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388882.432 5720401.412 68.703</gml:pos>
							<gml:pos>388882.678 5720401.0 68.706</gml:pos>
							<gml:pos>388882.798 5720400.798 68.698</gml:pos>
							<gml:pos>388883.0 5720400.46 68.711</gml:pos>
							<gml:pos>388883.172 5720400.172 68.711</gml:pos>
							<gml:pos>388883.275 5720400.0 68.714</gml:pos>
							<gml:pos>388883.546 5720399.546 68.705</gml:pos>
							<gml:pos>388883.872 5720399.0 68.71</gml:pos>
							<gml:pos>388883.92 5720398.92 68.708</gml:pos>
							<gml:pos>388884.0 5720398.787 68.71</gml:pos>
							<gml:pos>388884.294 5720398.294 68.695</gml:pos>
							<gml:pos>388884.47 5720398.0 68.701</gml:pos>
							<gml:pos>388884.668 5720397.668 68.687</gml:pos>
							<gml:pos>388885.0 5720397.113 68.699</gml:pos>
							<gml:pos>388885.042 5720397.042 68.697</gml:pos>
							<gml:pos>388885.067 5720397.0 68.697</gml:pos>
							<gml:pos>388885.416 5720396.416 68.666</gml:pos>
							<gml:pos>388885.665 5720396.0 68.663</gml:pos>
							<gml:pos>388885.79 5720395.79 68.656</gml:pos>
							<gml:pos>388886.0 5720395.439 68.66</gml:pos>
							<gml:pos>388886.164 5720395.164 68.658</gml:pos>
							<gml:pos>388886.262 5720395.0 68.66</gml:pos>
							<gml:pos>388886.301 5720394.935 68.657</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388885.424 5720394.41 68.619</gml:pos>
							<gml:pos>388886.0 5720394.755 68.65</gml:pos>
							<gml:pos>388886.301 5720394.935 68.657</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388874.992 5720388.165 68.478</gml:pos>
							<gml:pos>388875.0 5720388.17 68.478</gml:pos>
							<gml:pos>388875.423 5720388.423 68.487</gml:pos>
							<gml:pos>388876.0 5720388.768 68.501</gml:pos>
							<gml:pos>388876.387 5720389.0 68.51</gml:pos>
							<gml:pos>388877.0 5720389.367 68.521</gml:pos>
							<gml:pos>388877.915 5720389.915 68.528</gml:pos>
							<gml:pos>388878.0 5720389.966 68.529</gml:pos>
							<gml:pos>388878.057 5720390.0 68.53</gml:pos>
							<gml:pos>388879.0 5720390.564 68.553</gml:pos>
							<gml:pos>388879.728 5720391.0 68.57</gml:pos>
							<gml:pos>388880.0 5720391.163 68.575</gml:pos>
							<gml:pos>388880.406 5720391.406 68.586</gml:pos>
							<gml:pos>388881.0 5720391.762 68.603</gml:pos>
							<gml:pos>388881.398 5720392.0 68.618</gml:pos>
							<gml:pos>388882.0 5720392.36 68.634</gml:pos>
							<gml:pos>388882.898 5720392.898 68.612</gml:pos>
							<gml:pos>388883.0 5720392.959 68.609</gml:pos>
							<gml:pos>388883.069 5720393.0 68.608</gml:pos>
							<gml:pos>388884.0 5720393.558 68.597</gml:pos>
							<gml:pos>388884.739 5720394.0 68.595</gml:pos>
							<gml:pos>388885.0 5720394.156 68.598</gml:pos>
							<gml:pos>388885.389 5720394.389 68.617</gml:pos>
							<gml:pos>388885.424 5720394.41 68.619</gml:pos>
						</gml:LineString>
					</gml:curveMember>
				</gml:MultiCurve>
			</bldg:lod2TerrainIntersection>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_91a1c26d-0ffd-45fe-b263-55af5e8258ff">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo517f8fb8-e7e9-4c55-af12-a983c1c54e35">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459398">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459398">
											<gml:posList srsDimension="3">388874.992000 5720388.165000 76.377000 388871.913000 5720393.237000 78.109000 388871.913000 5720393.237000 67.319000 388874.992000 5720388.165000 67.319000 388874.992000 5720388.165000 76.377000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_2213f287-227e-4b12-ad77-813e6b736aa9">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo0148e609-09b8-4b3d-a5e4-81d43b064e62">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459399">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459399">
											<gml:posList srsDimension="3">388871.913000 5720393.237000 78.109000 388871.570000 5720393.047000 78.107000 388871.570000 5720393.047000 67.319000 388871.913000 5720393.237000 67.319000 388871.913000 5720393.237000 78.109000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_99138cfb-e523-4595-9e5b-cc60e74fb2f9">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo57e2918b-58b7-43d3-858c-81f51ceb8bbb">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459400">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459400">
											<gml:posList srsDimension="3">388871.570000 5720393.047000 78.107000 388871.210000 5720393.650000 78.312000 388871.210000 5720393.650000 67.319000 388871.570000 5720393.047000 67.319000 388871.570000 5720393.047000 78.107000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_842ab0f0-f241-4aef-8607-1b68297dfabb">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeobdf68546-4e68-4f32-9e60-ea9e78549c93">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459401">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459401">
											<gml:posList srsDimension="3">388871.210000 5720393.650000 78.312000 388871.537000 5720393.845000 78.318000 388871.537000 5720393.845000 67.319000 388871.210000 5720393.650000 67.319000 388871.210000 5720393.650000 78.312000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_d7ccee1b-3a58-4515-bb4a-4ca1c4dd7709">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeod9e9ccc5-d338-47ca-b6ff-d2aed7b09d8d">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459402">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459402">
											<gml:posList srsDimension="3">388871.537000 5720393.845000 78.318000 388870.975000 5720394.770000 78.633000 388870.975000 5720394.770000 67.319000 388871.537000 5720393.845000 67.319000 388871.537000 5720393.845000 78.318000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_79b8d6a4-b8d2-441a-a231-1de853b82581">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeoa99a25c8-22a0-4c83-9f38-35df6e4f0053">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459403">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459403">
											<gml:posList srsDimension="3">388866.492000 5720402.144000 76.418000 388866.577000 5720402.180000 76.419000 388866.577000 5720402.180000 67.319000 388866.492000 5720402.144000 67.319000 388866.492000 5720402.144000 76.418000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_d0d0e9ff-f1c6-49f7-8259-5ad9feafe2f0">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo78e2de3e-dd2a-4c9d-98a1-a144e91fe9f3">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459404">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459404">
											<gml:posList srsDimension="3">388866.577000 5720402.180000 76.419000 388866.559000 5720402.211000 76.409000 388866.559000 5720402.211000 67.319000 388866.577000 5720402.180000 67.319000 388866.577000 5720402.180000 76.419000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_b739ea7a-56da-4feb-a0bd-e96c638d0d74">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo8c7600a6-2c46-4572-8098-1adb46e417bb">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459405">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459405">
											<gml:posList srsDimension="3">388866.559000 5720402.211000 76.409000 388878.562000 5720407.889000 76.377000 388878.562000 5720407.889000 67.319000 388866.559000 5720402.211000 67.319000 388866.559000 5720402.211000 76.409000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_dfade2cb-800e-4193-8000-45f10206c60a">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo6c6635fd-4e9f-43f5-968e-0cb7ae61e6cb">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459406">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459406">
											<gml:posList srsDimension="3">388878.562000 5720407.889000 76.377000 388882.432000 5720401.412000 78.785000 388882.432000 5720401.412000 67.319000 388878.562000 5720407.889000 67.319000 388878.562000 5720407.889000 76.377000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_bc1ef48e-d725-41e5-831a-0b4b4246aae6">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeoda1c305c-9e54-4991-b077-6fd2530f3aae">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459407">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459407">
											<gml:posList srsDimension="3">388882.432000 5720401.412000 78.785000 388886.301000 5720394.935000 76.377000 388886.301000 5720394.935000 67.319000 388882.432000 5720401.412000 67.319000 388882.432000 5720401.412000 78.785000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_c0f49fed-04fb-4f37-95cc-c72b003e2780">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeoc5b9fa46-c679-41e2-bfea-cd0f9ef7d255">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459408">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459408">
											<gml:posList srsDimension="3">388886.301000 5720394.935000 76.377000 388885.424000 5720394.410000 76.377000 388885.424000 5720394.410000 67.319000 388886.301000 5720394.935000 67.319000 388886.301000 5720394.935000 76.377000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_3f100dac-85dc-47f4-bf92-2c4398ab7e96">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeoa1a8cdd4-11da-4423-b338-ccb0613dd578">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459409">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459409">
											<gml:posList srsDimension="3">388882.432000 5720401.412000 78.785000 388878.562000 5720407.889000 76.377000 388866.559000 5720402.211000 76.409000 388866.577000 5720402.180000 76.419000 388866.492000 5720402.144000 76.418000 388870.705000 5720395.214000 78.785000 388882.432000 5720401.412000 78.785000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_31001423-1d47-49b7-bdbf-18c0de76dca9">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeodb4888a1-f2f5-41a1-b4a5-cfb28db03e8c">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459410">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459410">
											<gml:posList srsDimension="3">388870.705000 5720395.214000 78.785000 388870.975000 5720394.770000 78.633000 388871.537000 5720393.845000 78.318000 388871.210000 5720393.650000 78.312000 388871.570000 5720393.047000 78.107000 388871.913000 5720393.237000 78.109000 388874.992000 5720388.165000 76.377000 388885.424000 5720394.410000 76.377000 388886.301000 5720394.935000 76.377000 388882.432000 5720401.412000 78.785000 388870.705000 5720395.214000 78.785000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="UUID_421a3fc5-b86c-4f01-ae69-6ac994542cc1">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeoa298f615-2d6f-4bc1-afb4-b8b0ed30eabf">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459411">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459411">
											<gml:posList srsDimension="3">388874.992000 5720388.165000 67.319000 388871.913000 5720393.237000 67.319000 388871.570000 5720393.047000 67.319000 388871.210000 5720393.650000 67.319000 388871.537000 5720393.845000 67.319000 388870.975000 5720394.770000 67.319000 388870.705000 5720395.214000 67.319000 388866.492000 5720402.144000 67.319000 388866.577000 5720402.180000 67.319000 388866.559000 5720402.211000 67.319000 388878.562000 5720407.889000 67.319000 388882.432000 5720401.412000 67.319000 388886.301000 5720394.935000 67.319000 388885.424000 5720394.410000 67.319000 388874.992000 5720388.165000 67.319000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:ClosureSurface>
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeod11220f3-44b1-48a3-91c3-f611e790d313">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459412">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459412">
											<gml:posList srsDimension="3">388870.975000 5720394.770000 78.633000 388870.705000 5720395.214000 78.785000 388870.705000 5720395.214000 67.319000 388870.975000 5720394.770000 67.319000 388870.975000 5720394.770000 78.633000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:ClosureSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:ClosureSurface>
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeof979000b-597d-434e-aaa6-225c07f45c7a">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459413">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459413">
											<gml:posList srsDimension="3">388870.705000 5720395.214000 78.785000 388866.492000 5720402.144000 76.418000 388866.492000 5720402.144000 67.319000 388870.705000 5720395.214000 67.319000 388870.705000 5720395.214000 78.785000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:ClosureSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_f2dc8a19-639d-48ab-8662-a9aeb8d32fd9">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo48d52d9c-89c8-46e9-a0f0-32cf8919d585">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459414">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459414">
											<gml:posList srsDimension="3">388885.424000 5720394.410000 76.377000 388874.992000 5720388.165000 76.377000 388874.992000 5720388.165000 73.476000 388880.207000 5720391.287000 76.127000 388885.424000 5720394.410000 73.475000 388885.424000 5720394.410000 76.377000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:ClosureSurface>
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeoaa65e4b8-f046-4c42-8bd5-9279a981ca52">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459415">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459415">
											<gml:posList srsDimension="3">388874.992000 5720388.165000 73.476000 388874.992000 5720388.165000 67.319000 388880.207000 5720391.287000 67.319000 388880.207000 5720391.287000 76.127000 388874.992000 5720388.165000 73.476000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459416">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459416">
											<gml:posList srsDimension="3">388880.207000 5720391.287000 76.127000 388880.207000 5720391.287000 67.319000 388885.424000 5720394.410000 67.319000 388885.424000 5720394.410000 73.475000 388880.207000 5720391.287000 76.127000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:ClosureSurface>
			</bldg:boundedBy>
		</bldg:BuildingPart>
	</bldg:consistsOfBuildingPart>
	<bldg:consistsOfBuildingPart>
		<bldg:BuildingPart gml:id="GUID_1479134954866_51097139">
			<core:creationDate>2018-02-11</core:creationDate>
			<gen:stringAttribute name="DatenquelleDachhoehe">
				<gen:value>1000</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="DatenquelleLage">
				<gen:value>1000</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="DatenquelleBodenhoehe">
				<gen:value>1100</gen:value>
			</gen:stringAttribute>
			<bldg:measuredHeight uom="urn:adv:uom:m">8.808</bldg:measuredHeight>
			<bldg:lod2Solid>
				<gml:Solid gml:id="IDGeo2d0974d0-2d1b-46f0-922d-b4396a1a963c">
					<gml:exterior>
						<gml:CompositeSurface>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459440"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459436"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459437"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459428"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459430"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459438"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459433"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459435"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459431"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459434"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459427"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459426"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459439"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459429"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459432"/>
						</gml:CompositeSurface>
					</gml:exterior>
				</gml:Solid>
			</bldg:lod2Solid>
			<bldg:lod2TerrainIntersection>
				<gml:MultiCurve>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388876.072 5720386.386 68.389</gml:pos>
							<gml:pos>388876.615 5720386.615 68.401</gml:pos>
							<gml:pos>388877.0 5720386.778 68.409</gml:pos>
							<gml:pos>388877.527 5720387.0 68.42</gml:pos>
							<gml:pos>388878.0 5720387.2 68.43</gml:pos>
							<gml:pos>388878.345 5720387.345 68.434</gml:pos>
							<gml:pos>388879.0 5720387.622 68.445</gml:pos>
							<gml:pos>388879.897 5720388.0 68.46</gml:pos>
							<gml:pos>388880.0 5720388.043 68.461</gml:pos>
							<gml:pos>388880.075 5720388.075 68.463</gml:pos>
							<gml:pos>388881.0 5720388.465 68.473</gml:pos>
							<gml:pos>388881.726 5720388.772 68.489</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388874.992 5720388.165 68.478</gml:pos>
							<gml:pos>388875.0 5720388.152 68.478</gml:pos>
							<gml:pos>388875.057 5720388.057 68.472</gml:pos>
							<gml:pos>388875.092 5720388.0 68.47</gml:pos>
							<gml:pos>388875.435 5720387.435 68.442</gml:pos>
							<gml:pos>388875.699 5720387.0 68.42</gml:pos>
							<gml:pos>388875.813 5720386.813 68.409</gml:pos>
							<gml:pos>388876.0 5720386.505 68.395</gml:pos>
							<gml:pos>388876.072 5720386.386 68.389</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388874.992 5720388.165 68.478</gml:pos>
							<gml:pos>388875.0 5720388.17 68.478</gml:pos>
							<gml:pos>388875.423 5720388.423 68.487</gml:pos>
							<gml:pos>388876.0 5720388.768 68.501</gml:pos>
							<gml:pos>388876.387 5720389.0 68.51</gml:pos>
							<gml:pos>388877.0 5720389.367 68.521</gml:pos>
							<gml:pos>388877.915 5720389.915 68.528</gml:pos>
							<gml:pos>388878.0 5720389.966 68.529</gml:pos>
							<gml:pos>388878.057 5720390.0 68.53</gml:pos>
							<gml:pos>388879.0 5720390.564 68.553</gml:pos>
							<gml:pos>388879.728 5720391.0 68.57</gml:pos>
							<gml:pos>388880.0 5720391.163 68.575</gml:pos>
							<gml:pos>388880.207 5720391.287 68.581</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388880.207 5720391.287 68.581</gml:pos>
							<gml:pos>388880.406 5720391.406 68.586</gml:pos>
							<gml:pos>388881.0 5720391.762 68.603</gml:pos>
							<gml:pos>388881.398 5720392.0 68.618</gml:pos>
							<gml:pos>388882.0 5720392.36 68.634</gml:pos>
							<gml:pos>388882.898 5720392.898 68.612</gml:pos>
							<gml:pos>388883.0 5720392.959 68.609</gml:pos>
							<gml:pos>388883.069 5720393.0 68.608</gml:pos>
							<gml:pos>388884.0 5720393.558 68.597</gml:pos>
							<gml:pos>388884.739 5720394.0 68.595</gml:pos>
							<gml:pos>388885.0 5720394.156 68.598</gml:pos>
							<gml:pos>388885.389 5720394.389 68.617</gml:pos>
							<gml:pos>388885.424 5720394.41 68.619</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388885.424 5720394.41 68.619</gml:pos>
							<gml:pos>388885.67 5720394.0 68.61</gml:pos>
							<gml:pos>388885.794 5720393.794 68.608</gml:pos>
							<gml:pos>388886.0 5720393.452 68.587</gml:pos>
							<gml:pos>388886.17 5720393.17 68.57</gml:pos>
							<gml:pos>388886.271 5720393.0 68.568</gml:pos>
							<gml:pos>388886.545 5720392.545 68.554</gml:pos>
							<gml:pos>388886.873 5720392.0 68.536</gml:pos>
							<gml:pos>388886.92 5720391.92 68.535</gml:pos>
							<gml:pos>388887.0 5720391.788 68.529</gml:pos>
							<gml:pos>388887.265 5720391.347 68.518</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388887.265 5720391.347 68.518</gml:pos>
							<gml:pos>388887.469 5720391.469 68.532</gml:pos>
							<gml:pos>388888.0 5720391.786 68.574</gml:pos>
							<gml:pos>388888.133 5720391.866 68.576</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388888.133 5720391.866 68.576</gml:pos>
							<gml:pos>388888.245 5720391.678 68.57</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388887.379 5720391.157 68.517</gml:pos>
							<gml:pos>388888.0 5720391.53 68.566</gml:pos>
							<gml:pos>388888.245 5720391.678 68.57</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388881.726 5720388.772 68.489</gml:pos>
							<gml:pos>388881.805 5720388.805 68.49</gml:pos>
							<gml:pos>388882.0 5720388.887 68.494</gml:pos>
							<gml:pos>388882.267 5720389.0 68.492</gml:pos>
							<gml:pos>388883.0 5720389.309 68.489</gml:pos>
							<gml:pos>388883.535 5720389.535 68.475</gml:pos>
							<gml:pos>388884.0 5720389.731 68.467</gml:pos>
							<gml:pos>388884.637 5720390.0 68.461</gml:pos>
							<gml:pos>388885.0 5720390.153 68.456</gml:pos>
							<gml:pos>388885.265 5720390.265 68.458</gml:pos>
							<gml:pos>388886.0 5720390.575 68.463</gml:pos>
							<gml:pos>388886.995 5720390.995 68.49</gml:pos>
							<gml:pos>388887.0 5720390.997 68.49</gml:pos>
							<gml:pos>388887.007 5720391.0 68.49</gml:pos>
							<gml:pos>388887.379 5720391.157 68.517</gml:pos>
						</gml:LineString>
					</gml:curveMember>
				</gml:MultiCurve>
			</bldg:lod2TerrainIntersection>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_142b7390-7753-4dba-84ac-e2c12f375fe1">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo4e742b5c-a8dc-4d5e-890d-587fe1041514">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459426">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459426">
											<gml:posList srsDimension="3">388876.072000 5720386.386000 73.475000 388874.992000 5720388.165000 73.476000 388874.992000 5720388.165000 67.319000 388876.072000 5720386.386000 67.319000 388876.072000 5720386.386000 73.475000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_20bca33c-fac3-43b7-945f-ba5a1a9182ba">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo9b5a8bb4-3f58-4d7a-a3b1-01e2ebbdcf8a">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459427">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459427">
											<gml:posList srsDimension="3">388885.424000 5720394.410000 73.475000 388887.265000 5720391.347000 73.475000 388887.265000 5720391.347000 67.319000 388885.424000 5720394.410000 67.319000 388885.424000 5720394.410000 73.475000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_36317a65-be1c-42d9-be39-33a6a0bb6e39">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo443794f2-150e-4eb7-a4f1-4b1d44fd6449">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459428">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459428">
											<gml:posList srsDimension="3">388887.265000 5720391.347000 73.475000 388888.133000 5720391.866000 73.034000 388888.133000 5720391.866000 67.319000 388887.265000 5720391.347000 67.319000 388887.265000 5720391.347000 73.475000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_045743b0-8bb3-40df-ae55-e7821ea66758">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo937d0a78-1ebf-4a8e-af40-6fc8780da06b">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459429">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459429">
											<gml:posList srsDimension="3">388888.133000 5720391.866000 73.034000 388888.245000 5720391.678000 73.034000 388888.245000 5720391.678000 67.319000 388888.133000 5720391.866000 67.319000 388888.133000 5720391.866000 73.034000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_a8fe2721-25ab-4e60-aa22-100f64c1a020">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo94ffb6d6-5fe6-4901-8b27-fb5d1a3836fe">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459430">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459430">
											<gml:posList srsDimension="3">388880.207000 5720391.287000 76.127000 388874.992000 5720388.165000 73.476000 388876.072000 5720386.386000 73.475000 388881.726000 5720388.772000 76.127000 388880.207000 5720391.287000 76.127000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_13d76773-0b25-4c60-9bcd-8217c8ca57ed">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo22f6880c-fef9-4060-bb59-e8d1ee4290c0">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459431">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459431">
											<gml:posList srsDimension="3">388881.726000 5720388.772000 76.127000 388887.379000 5720391.157000 73.475000 388888.245000 5720391.678000 73.034000 388888.133000 5720391.866000 73.034000 388887.265000 5720391.347000 73.475000 388885.424000 5720394.410000 73.475000 388880.207000 5720391.287000 76.127000 388881.726000 5720388.772000 76.127000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="UUID_fc9f5c41-06be-4717-ac4e-04eddab22a02">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo3231f349-e5e8-422d-a3ab-2802ee1f8408">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459432">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459432">
											<gml:posList srsDimension="3">388881.726000 5720388.772000 67.319000 388876.072000 5720386.386000 67.319000 388874.992000 5720388.165000 67.319000 388880.207000 5720391.287000 67.319000 388885.424000 5720394.410000 67.319000 388887.265000 5720391.347000 67.319000 388888.133000 5720391.866000 67.319000 388888.245000 5720391.678000 67.319000 388887.379000 5720391.157000 67.319000 388881.726000 5720388.772000 67.319000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_1c3f03bd-b50d-4b3e-b14a-06dad86fcd47">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo5e02424c-88ce-46ce-971a-3ce19183d1bf">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459433">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459433">
											<gml:posList srsDimension="3">388881.726000 5720388.772000 76.127000 388876.072000 5720386.386000 73.475000 388876.072000 5720386.386000 73.183000 388881.726000 5720388.772000 73.183000 388881.726000 5720388.772000 76.127000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:ClosureSurface>
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo5efd1d09-1a0e-4fcf-a6e8-9c1f2a3d5dab">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459434">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459434">
											<gml:posList srsDimension="3">388876.072000 5720386.386000 73.183000 388876.072000 5720386.386000 67.319000 388881.726000 5720388.772000 67.319000 388881.726000 5720388.772000 73.183000 388876.072000 5720386.386000 73.183000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:ClosureSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:ClosureSurface>
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeoe3fce241-5997-4051-8290-4a9691264fd7">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459435">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459435">
											<gml:posList srsDimension="3">388874.992000 5720388.165000 73.476000 388880.207000 5720391.287000 76.127000 388880.207000 5720391.287000 67.319000 388874.992000 5720388.165000 67.319000 388874.992000 5720388.165000 73.476000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:ClosureSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:ClosureSurface>
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo0c271df4-2fcf-42a5-b8f8-cc190b4eddd2">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459436">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459436">
											<gml:posList srsDimension="3">388880.207000 5720391.287000 76.127000 388885.424000 5720394.410000 73.475000 388885.424000 5720394.410000 67.319000 388880.207000 5720391.287000 67.319000 388880.207000 5720391.287000 76.127000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:ClosureSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_8dbff929-f770-4620-9353-226f9ca1a33b">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo1bb1f2f9-4856-4570-90d2-39ff5d62b264">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459437">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459437">
											<gml:posList srsDimension="3">388887.952000 5720391.502000 73.183000 388887.379000 5720391.157000 73.475000 388887.379000 5720391.157000 73.183000 388887.952000 5720391.502000 73.183000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:ClosureSurface>
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeobceded85-7d7a-4b53-be04-b05621b81408">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459438">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459438">
											<gml:posList srsDimension="3">388888.245000 5720391.678000 73.034000 388887.952000 5720391.502000 73.183000 388887.379000 5720391.157000 73.183000 388887.379000 5720391.157000 67.319000 388888.245000 5720391.678000 67.319000 388888.245000 5720391.678000 72.450000 388888.245000 5720391.678000 73.034000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:ClosureSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_0c7ccafe-a1ef-4fbd-ab62-25f40dd8d126">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo6b0e1fc5-9b71-4726-bc59-875087bcafbd">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459439">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459439">
											<gml:posList srsDimension="3">388887.379000 5720391.157000 73.475000 388881.726000 5720388.772000 76.127000 388881.726000 5720388.772000 67.319000 388887.379000 5720391.157000 67.319000 388887.379000 5720391.157000 73.475000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:ClosureSurface>
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo45e9142a-5a02-4ac1-a032-d8ed9ecda198">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459440">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459440">
											<gml:posList srsDimension="3">388887.379000 5720391.157000 73.475000 388881.726000 5720388.772000 76.127000 388881.726000 5720388.772000 67.319000 388887.379000 5720391.157000 67.319000 388887.379000 5720391.157000 73.475000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:ClosureSurface>
			</bldg:boundedBy>
		</bldg:BuildingPart>
	</bldg:consistsOfBuildingPart>
	<bldg:consistsOfBuildingPart>
		<bldg:BuildingPart gml:id="GUID_1479134954866_51097140">
			<core:creationDate>2018-02-11</core:creationDate>
			<gen:stringAttribute name="DatenquelleDachhoehe">
				<gen:value>1000</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="DatenquelleLage">
				<gen:value>1000</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="DatenquelleBodenhoehe">
				<gen:value>1100</gen:value>
			</gen:stringAttribute>
			<bldg:measuredHeight uom="urn:adv:uom:m">5.864</bldg:measuredHeight>
			<bldg:lod2Solid>
				<gml:Solid gml:id="IDGeo19a7bd0b-3807-45ad-86ca-2322b732146f">
					<gml:exterior>
						<gml:CompositeSurface>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459463"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459455"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459470"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459462"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459465"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459471"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459472"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459456"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459459"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459461"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459460"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459458"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459466"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459468"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459467"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459464"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459457"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459469"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459473"/>
						</gml:CompositeSurface>
					</gml:exterior>
				</gml:Solid>
			</bldg:lod2Solid>
			<bldg:lod2TerrainIntersection>
				<gml:MultiCurve>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388875.708 5720370.044 67.866</gml:pos>
							<gml:pos>388876.0 5720370.162 67.84</gml:pos>
							<gml:pos>388876.271 5720370.271 67.807</gml:pos>
							<gml:pos>388877.0 5720370.565 67.737</gml:pos>
							<gml:pos>388877.948 5720370.948 67.637</gml:pos>
							<gml:pos>388878.0 5720370.969 67.633</gml:pos>
							<gml:pos>388878.078 5720371.0 67.636</gml:pos>
							<gml:pos>388879.0 5720371.372 67.71</gml:pos>
							<gml:pos>388879.624 5720371.624 67.729</gml:pos>
							<gml:pos>388880.0 5720371.776 67.724</gml:pos>
							<gml:pos>388880.556 5720372.0 67.718</gml:pos>
							<gml:pos>388881.0 5720372.179 67.695</gml:pos>
							<gml:pos>388881.3 5720372.3 67.691</gml:pos>
							<gml:pos>388882.0 5720372.582 67.657</gml:pos>
							<gml:pos>388882.958 5720372.969 67.669</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388875.514 5720369.993 67.883</gml:pos>
							<gml:pos>388875.541 5720370.0 67.881</gml:pos>
							<gml:pos>388875.708 5720370.044 67.866</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388875.49 5720370.047 67.886</gml:pos>
							<gml:pos>388875.511 5720370.0 67.884</gml:pos>
							<gml:pos>388875.514 5720369.993 67.883</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388874.309 5720372.816 67.86</gml:pos>
							<gml:pos>388874.461 5720372.461 67.853</gml:pos>
							<gml:pos>388874.657 5720372.0 67.877</gml:pos>
							<gml:pos>388874.76 5720371.76 67.887</gml:pos>
							<gml:pos>388875.0 5720371.196 67.87</gml:pos>
							<gml:pos>388875.059 5720371.059 67.866</gml:pos>
							<gml:pos>388875.084 5720371.0 67.867</gml:pos>
							<gml:pos>388875.358 5720370.358 67.898</gml:pos>
							<gml:pos>388875.49 5720370.047 67.886</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388869.671 5720383.685 68.054</gml:pos>
							<gml:pos>388869.675 5720383.675 68.053</gml:pos>
							<gml:pos>388869.963 5720383.0 68.012</gml:pos>
							<gml:pos>388869.974 5720382.974 68.01</gml:pos>
							<gml:pos>388870.0 5720382.914 68.001</gml:pos>
							<gml:pos>388870.273 5720382.273 67.937</gml:pos>
							<gml:pos>388870.39 5720382.0 67.918</gml:pos>
							<gml:pos>388870.572 5720381.572 67.913</gml:pos>
							<gml:pos>388870.817 5720381.0 67.874</gml:pos>
							<gml:pos>388870.872 5720380.872 67.864</gml:pos>
							<gml:pos>388871.0 5720380.571 67.853</gml:pos>
							<gml:pos>388871.171 5720380.171 67.835</gml:pos>
							<gml:pos>388871.243 5720380.0 67.815</gml:pos>
							<gml:pos>388871.47 5720379.47 67.791</gml:pos>
							<gml:pos>388871.67 5720379.0 67.79</gml:pos>
							<gml:pos>388871.769 5720378.769 67.789</gml:pos>
							<gml:pos>388872.0 5720378.227 67.757</gml:pos>
							<gml:pos>388872.068 5720378.068 67.751</gml:pos>
							<gml:pos>388872.097 5720378.0 67.75</gml:pos>
							<gml:pos>388872.367 5720377.367 67.775</gml:pos>
							<gml:pos>388872.524 5720377.0 67.774</gml:pos>
							<gml:pos>388872.666 5720376.666 67.773</gml:pos>
							<gml:pos>388872.95 5720376.0 67.771</gml:pos>
							<gml:pos>388872.965 5720375.965 67.773</gml:pos>
							<gml:pos>388873.0 5720375.884 67.776</gml:pos>
							<gml:pos>388873.264 5720375.264 67.788</gml:pos>
							<gml:pos>388873.377 5720375.0 67.801</gml:pos>
							<gml:pos>388873.563 5720374.563 67.827</gml:pos>
							<gml:pos>388873.804 5720374.0 67.852</gml:pos>
							<gml:pos>388873.862 5720373.862 67.854</gml:pos>
							<gml:pos>388874.0 5720373.54 67.858</gml:pos>
							<gml:pos>388874.162 5720373.162 67.867</gml:pos>
							<gml:pos>388874.23 5720373.0 67.864</gml:pos>
							<gml:pos>388874.309 5720372.816 67.86</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388869.671 5720383.685 68.054</gml:pos>
							<gml:pos>388869.695 5720383.695 68.053</gml:pos>
							<gml:pos>388869.815 5720383.744 68.049</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388869.815 5720383.744 68.049</gml:pos>
							<gml:pos>388870.0 5720383.822 68.043</gml:pos>
							<gml:pos>388870.421 5720384.0 68.067</gml:pos>
							<gml:pos>388871.0 5720384.244 68.112</gml:pos>
							<gml:pos>388871.423 5720384.423 68.132</gml:pos>
							<gml:pos>388872.0 5720384.667 68.153</gml:pos>
							<gml:pos>388872.79 5720385.0 68.214</gml:pos>
							<gml:pos>388873.0 5720385.089 68.228</gml:pos>
							<gml:pos>388873.154 5720385.154 68.238</gml:pos>
							<gml:pos>388874.0 5720385.511 68.296</gml:pos>
							<gml:pos>388874.885 5720385.885 68.347</gml:pos>
							<gml:pos>388875.0 5720385.933 68.354</gml:pos>
							<gml:pos>388875.158 5720386.0 68.362</gml:pos>
							<gml:pos>388876.0 5720386.356 68.388</gml:pos>
							<gml:pos>388876.072 5720386.386 68.389</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388876.072 5720386.386 68.389</gml:pos>
							<gml:pos>388876.615 5720386.615 68.401</gml:pos>
							<gml:pos>388877.0 5720386.778 68.409</gml:pos>
							<gml:pos>388877.527 5720387.0 68.42</gml:pos>
							<gml:pos>388878.0 5720387.2 68.43</gml:pos>
							<gml:pos>388878.345 5720387.345 68.434</gml:pos>
							<gml:pos>388879.0 5720387.622 68.445</gml:pos>
							<gml:pos>388879.897 5720388.0 68.46</gml:pos>
							<gml:pos>388880.0 5720388.043 68.461</gml:pos>
							<gml:pos>388880.075 5720388.075 68.463</gml:pos>
							<gml:pos>388881.0 5720388.465 68.473</gml:pos>
							<gml:pos>388881.805 5720388.805 68.49</gml:pos>
							<gml:pos>388882.0 5720388.887 68.494</gml:pos>
							<gml:pos>388882.267 5720389.0 68.492</gml:pos>
							<gml:pos>388883.0 5720389.309 68.489</gml:pos>
							<gml:pos>388883.535 5720389.535 68.475</gml:pos>
							<gml:pos>388884.0 5720389.731 68.467</gml:pos>
							<gml:pos>388884.637 5720390.0 68.461</gml:pos>
							<gml:pos>388885.0 5720390.153 68.456</gml:pos>
							<gml:pos>388885.265 5720390.265 68.458</gml:pos>
							<gml:pos>388886.0 5720390.575 68.463</gml:pos>
							<gml:pos>388886.995 5720390.995 68.49</gml:pos>
							<gml:pos>388887.0 5720390.997 68.49</gml:pos>
							<gml:pos>388887.007 5720391.0 68.49</gml:pos>
							<gml:pos>388887.379 5720391.157 68.517</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388887.379 5720391.157 68.517</gml:pos>
							<gml:pos>388888.0 5720391.53 68.566</gml:pos>
							<gml:pos>388888.245 5720391.678 68.57</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388888.245 5720391.678 68.57</gml:pos>
							<gml:pos>388888.407 5720391.407 68.562</gml:pos>
							<gml:pos>388888.65 5720391.0 68.543</gml:pos>
							<gml:pos>388888.781 5720390.781 68.527</gml:pos>
							<gml:pos>388889.0 5720390.415 68.522</gml:pos>
							<gml:pos>388889.155 5720390.155 68.508</gml:pos>
							<gml:pos>388889.248 5720390.0 68.495</gml:pos>
							<gml:pos>388889.529 5720389.529 68.459</gml:pos>
							<gml:pos>388889.846 5720389.0 68.538</gml:pos>
							<gml:pos>388889.903 5720388.903 68.535</gml:pos>
							<gml:pos>388890.0 5720388.741 68.503</gml:pos>
							<gml:pos>388890.277 5720388.277 68.367</gml:pos>
							<gml:pos>388890.443 5720388.0 68.343</gml:pos>
							<gml:pos>388890.651 5720387.651 68.313</gml:pos>
							<gml:pos>388891.0 5720387.068 68.273</gml:pos>
							<gml:pos>388891.025 5720387.025 68.27</gml:pos>
							<gml:pos>388891.041 5720387.0 68.269</gml:pos>
							<gml:pos>388891.399 5720386.399 68.234</gml:pos>
							<gml:pos>388891.466 5720386.288 68.229</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388891.466 5720386.288 68.229</gml:pos>
							<gml:pos>388891.593 5720386.0 68.218</gml:pos>
							<gml:pos>388891.718 5720385.718 68.202</gml:pos>
							<gml:pos>388892.0 5720385.08 68.146</gml:pos>
							<gml:pos>388892.025 5720385.025 68.14</gml:pos>
							<gml:pos>388892.036 5720385.0 68.137</gml:pos>
							<gml:pos>388892.331 5720384.331 68.037</gml:pos>
							<gml:pos>388892.478 5720384.0 68.011</gml:pos>
							<gml:pos>388892.638 5720383.638 67.99</gml:pos>
							<gml:pos>388892.92 5720383.0 67.962</gml:pos>
							<gml:pos>388892.944 5720382.944 67.959</gml:pos>
							<gml:pos>388893.0 5720382.819 67.946</gml:pos>
							<gml:pos>388893.251 5720382.251 67.865</gml:pos>
							<gml:pos>388893.362 5720382.0 67.844</gml:pos>
							<gml:pos>388893.558 5720381.558 67.784</gml:pos>
							<gml:pos>388893.804 5720381.0 67.726</gml:pos>
							<gml:pos>388893.864 5720380.864 67.709</gml:pos>
							<gml:pos>388894.0 5720380.558 67.679</gml:pos>
							<gml:pos>388894.171 5720380.171 67.631</gml:pos>
							<gml:pos>388894.247 5720380.0 67.615</gml:pos>
							<gml:pos>388894.478 5720379.478 67.54</gml:pos>
							<gml:pos>388894.689 5720379.0 67.492</gml:pos>
							<gml:pos>388894.784 5720378.784 67.451</gml:pos>
							<gml:pos>388894.832 5720378.676 67.431</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388894.832 5720378.676 67.431</gml:pos>
							<gml:pos>388895.0 5720378.292 67.364</gml:pos>
							<gml:pos>388895.086 5720378.096 67.327</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388894.881 5720377.762 67.321</gml:pos>
							<gml:pos>388895.0 5720377.956 67.319</gml:pos>
							<gml:pos>388895.027 5720378.0 67.319</gml:pos>
							<gml:pos>388895.07 5720378.07 67.324</gml:pos>
							<gml:pos>388895.086 5720378.096 67.327</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388882.958 5720372.969 67.669</gml:pos>
							<gml:pos>388882.976 5720372.976 67.669</gml:pos>
							<gml:pos>388883.0 5720372.986 67.669</gml:pos>
							<gml:pos>388883.035 5720373.0 67.669</gml:pos>
							<gml:pos>388884.0 5720373.388 67.642</gml:pos>
							<gml:pos>388884.649 5720373.649 67.624</gml:pos>
							<gml:pos>388885.0 5720373.79 67.605</gml:pos>
							<gml:pos>388885.523 5720374.0 67.583</gml:pos>
							<gml:pos>388886.0 5720374.192 67.558</gml:pos>
							<gml:pos>388886.321 5720374.321 67.553</gml:pos>
							<gml:pos>388887.0 5720374.594 67.54</gml:pos>
							<gml:pos>388887.993 5720374.993 67.5</gml:pos>
							<gml:pos>388888.0 5720374.996 67.5</gml:pos>
							<gml:pos>388888.01 5720375.0 67.5</gml:pos>
							<gml:pos>388889.0 5720375.398 67.49</gml:pos>
							<gml:pos>388889.665 5720375.665 67.47</gml:pos>
							<gml:pos>388890.0 5720375.8 67.458</gml:pos>
							<gml:pos>388890.498 5720376.0 67.435</gml:pos>
							<gml:pos>388891.0 5720376.202 67.41</gml:pos>
							<gml:pos>388891.338 5720376.338 67.407</gml:pos>
							<gml:pos>388892.0 5720376.604 67.408</gml:pos>
							<gml:pos>388892.985 5720377.0 67.39</gml:pos>
							<gml:pos>388893.0 5720377.006 67.39</gml:pos>
							<gml:pos>388893.01 5720377.01 67.39</gml:pos>
							<gml:pos>388894.0 5720377.408 67.362</gml:pos>
							<gml:pos>388894.682 5720377.682 67.33</gml:pos>
							<gml:pos>388894.881 5720377.762 67.321</gml:pos>
						</gml:LineString>
					</gml:curveMember>
				</gml:MultiCurve>
			</bldg:lod2TerrainIntersection>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_d0b7902c-027d-4120-80e1-c1d8b33c01c5">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo33d33d2b-779e-4e01-9670-2df6648d36c5">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459455">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459455">
											<gml:posList srsDimension="3">388882.958000 5720372.969000 73.183000 388875.708000 5720370.044000 73.183000 388875.708000 5720370.044000 67.319000 388882.958000 5720372.969000 67.319000 388882.958000 5720372.969000 73.183000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_87bde015-b21b-4947-9120-0557352a7d72">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo2a632611-c831-4af0-90ed-6411ff03f364">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459456">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459456">
											<gml:posList srsDimension="3">388875.708000 5720370.044000 73.183000 388875.514000 5720369.993000 73.183000 388875.514000 5720369.993000 67.319000 388875.708000 5720370.044000 67.319000 388875.708000 5720370.044000 73.183000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_6b4d75e8-fd18-42e2-9d81-0eb2134bce08">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo0c995c56-3b67-4e56-8453-accedf4f84bc">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459457">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459457">
											<gml:posList srsDimension="3">388875.514000 5720369.993000 73.183000 388875.490000 5720370.047000 73.183000 388875.490000 5720370.047000 67.319000 388875.514000 5720369.993000 67.319000 388875.514000 5720369.993000 73.183000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_25fe0d3c-8e7e-4051-91c3-42c4bd36e81a">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo9ee783cc-0550-4a01-9c67-261b68b573df">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459458">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459458">
											<gml:posList srsDimension="3">388875.490000 5720370.047000 73.183000 388874.309000 5720372.816000 73.183000 388874.309000 5720372.816000 67.319000 388875.490000 5720370.047000 67.319000 388875.490000 5720370.047000 73.183000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_4ef24c3b-ca07-4dd8-a9bd-b1c320f017ae">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo53f2ebd2-8d13-4146-8ca7-083bfa9bda96">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459459">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459459">
											<gml:posList srsDimension="3">388874.309000 5720372.816000 73.183000 388869.671000 5720383.685000 73.183000 388869.671000 5720383.685000 67.319000 388874.309000 5720372.816000 67.319000 388874.309000 5720372.816000 73.183000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_3a6d72a9-1677-41d5-a861-9cf8f97441bf">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo7b87276c-61fc-482f-a7e7-8ddfe3664f65">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459460">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459460">
											<gml:posList srsDimension="3">388869.671000 5720383.685000 73.183000 388869.815000 5720383.744000 73.183000 388869.815000 5720383.744000 67.319000 388869.671000 5720383.685000 67.319000 388869.671000 5720383.685000 73.183000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_8496627a-382d-4f69-a05d-3dd53e1c5235">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo611ece7e-9de7-4cd7-bfee-6f3afc5368e9">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459461">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459461">
											<gml:posList srsDimension="3">388869.815000 5720383.744000 73.183000 388876.072000 5720386.386000 73.183000 388876.072000 5720386.386000 67.319000 388869.815000 5720383.744000 67.319000 388869.815000 5720383.744000 73.183000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_1af8addd-a4f1-4edb-b559-da8c2e1ef588">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo23fac495-1952-4aa5-8576-f599f188e2ec">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459462">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459462">
											<gml:posList srsDimension="3">388888.245000 5720391.678000 73.183000 388891.466000 5720386.288000 73.183000 388891.466000 5720386.288000 67.319000 388888.245000 5720391.678000 67.319000 388888.245000 5720391.678000 73.183000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_a1ca2a69-a224-410c-bb86-7550c4e230ab">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeof4417f79-e152-491a-a977-2818def3520c">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459463">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459463">
											<gml:posList srsDimension="3">388891.466000 5720386.288000 73.183000 388894.832000 5720378.676000 73.183000 388894.832000 5720378.676000 67.319000 388891.466000 5720386.288000 67.319000 388891.466000 5720386.288000 73.183000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_513ff525-3ea5-42d8-a429-2280d68d7e56">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo28bd0348-53a8-4aa5-b75b-0530764b82cb">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459464">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459464">
											<gml:posList srsDimension="3">388894.832000 5720378.676000 73.183000 388895.086000 5720378.096000 73.183000 388895.086000 5720378.096000 67.319000 388894.832000 5720378.676000 67.319000 388894.832000 5720378.676000 73.183000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_3cb00bd3-55a9-4161-b8c9-99c46cffaa65">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo54029ffc-9c3e-4bd6-b1bf-d7855db72ca5">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459465">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459465">
											<gml:posList srsDimension="3">388895.086000 5720378.096000 73.183000 388894.881000 5720377.762000 73.183000 388894.881000 5720377.762000 67.319000 388895.086000 5720378.096000 67.319000 388895.086000 5720378.096000 73.183000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_234c3fc1-d1c8-4e14-a819-810126ad4563">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeof8310644-6b6c-4920-92d7-a6c1811353c7">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459466">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459466">
											<gml:posList srsDimension="3">388894.881000 5720377.762000 73.183000 388882.958000 5720372.969000 73.183000 388882.958000 5720372.969000 67.319000 388894.881000 5720377.762000 67.319000 388894.881000 5720377.762000 73.183000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_ce7a07bc-42e6-4891-abcd-a80d15e7d92f">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeoca2359eb-e5ac-4df6-8edf-c13966916f54">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459467">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459467">
											<gml:posList srsDimension="3">388894.881000 5720377.762000 73.183000 388895.086000 5720378.096000 73.183000 388894.832000 5720378.676000 73.183000 388891.466000 5720386.288000 73.183000 388888.245000 5720391.678000 73.183000 388887.379000 5720391.157000 73.183000 388876.072000 5720386.386000 73.183000 388869.815000 5720383.744000 73.183000 388869.671000 5720383.685000 73.183000 388874.309000 5720372.816000 73.183000 388875.490000 5720370.047000 73.183000 388875.514000 5720369.993000 73.183000 388875.708000 5720370.044000 73.183000 388882.958000 5720372.969000 73.183000 388894.881000 5720377.762000 73.183000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="UUID_d67869af-36ab-49bc-9c7c-9bb6399811f7">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo1b0d041e-12f1-4d96-bcea-f7933fb02fb6">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459468">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459468">
											<gml:posList srsDimension="3">388882.958000 5720372.969000 67.319000 388875.708000 5720370.044000 67.319000 388875.514000 5720369.993000 67.319000 388875.490000 5720370.047000 67.319000 388874.309000 5720372.816000 67.319000 388869.671000 5720383.685000 67.319000 388869.815000 5720383.744000 67.319000 388876.072000 5720386.386000 67.319000 388887.379000 5720391.157000 67.319000 388888.245000 5720391.678000 67.319000 388891.466000 5720386.288000 67.319000 388894.832000 5720378.676000 67.319000 388895.086000 5720378.096000 67.319000 388894.881000 5720377.762000 67.319000 388882.958000 5720372.969000 67.319000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_89bc90d4-566f-4eac-851d-7d0bcc815a51">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo839c9140-5eab-4a76-aeb0-1c6b6ba2f3a6">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459469">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459469">
											<gml:posList srsDimension="3">388887.379000 5720391.157000 67.319000 388876.072000 5720386.386000 67.319000 388881.726000 5720388.772000 67.319000 388887.379000 5720391.157000 67.319000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:ClosureSurface>
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeob7674f1e-131a-482b-889f-d13e2837ae6c">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459470">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459470">
											<gml:posList srsDimension="3">388876.072000 5720386.386000 73.183000 388881.726000 5720388.772000 73.183000 388881.726000 5720388.772000 67.319000 388876.072000 5720386.386000 67.319000 388876.072000 5720386.386000 72.449000 388876.072000 5720386.386000 73.183000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459471">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459471">
											<gml:posList srsDimension="3">388881.726000 5720388.772000 73.183000 388887.379000 5720391.157000 73.183000 388887.379000 5720391.157000 67.319000 388881.726000 5720388.772000 67.319000 388881.726000 5720388.772000 73.183000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:ClosureSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_e55e46ae-6495-4a5d-b4ca-096ee1c1d3b1">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo2de1c0a6-e9d3-4a74-8664-892898a1f8e0">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459472">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459472">
											<gml:posList srsDimension="3">388887.952000 5720391.502000 73.183000 388888.245000 5720391.678000 73.183000 388888.245000 5720391.678000 67.319000 388888.245000 5720391.678000 73.034000 388887.952000 5720391.502000 73.183000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:ClosureSurface>
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo4236b9f4-9f71-40e3-a3cb-b6084d35cda5">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459473">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459473">
											<gml:posList srsDimension="3">388887.379000 5720391.157000 73.183000 388887.952000 5720391.502000 73.183000 388888.245000 5720391.678000 73.034000 388888.245000 5720391.678000 67.319000 388887.379000 5720391.157000 67.319000 388887.379000 5720391.157000 73.183000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:ClosureSurface>
			</bldg:boundedBy>
		</bldg:BuildingPart>
	</bldg:consistsOfBuildingPart>
	<bldg:consistsOfBuildingPart>
		<bldg:BuildingPart gml:id="GUID_1479134954866_51097141">
			<core:creationDate>2018-02-11</core:creationDate>
			<gen:stringAttribute name="DatenquelleDachhoehe">
				<gen:value>1000</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="DatenquelleLage">
				<gen:value>1000</gen:value>
			</gen:stringAttribute>
			<gen:stringAttribute name="DatenquelleBodenhoehe">
				<gen:value>1100</gen:value>
			</gen:stringAttribute>
			<bldg:measuredHeight uom="urn:adv:uom:m">11.703</bldg:measuredHeight>
			<bldg:lod2Solid>
				<gml:Solid gml:id="IDGeode46b366-9024-486d-aba8-9120ec03181b">
					<gml:exterior>
						<gml:CompositeSurface>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459485"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459493"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459489"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459494"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459486"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459490"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459495"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459487"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459496"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459491"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459483"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459492"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459484"/>
							<gml:surfaceMember xlink:href="#PolyIDGeo7459488"/>
						</gml:CompositeSurface>
					</gml:exterior>
				</gml:Solid>
			</bldg:lod2Solid>
			<bldg:lod2TerrainIntersection>
				<gml:MultiCurve>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388870.429 5720394.959 68.876</gml:pos>
							<gml:pos>388870.628 5720394.628 68.864</gml:pos>
							<gml:pos>388870.656 5720394.58 68.862</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388867.467 5720393.733 68.925</gml:pos>
							<gml:pos>388867.921 5720393.921 68.92</gml:pos>
							<gml:pos>388868.0 5720393.954 68.919</gml:pos>
							<gml:pos>388868.112 5720394.0 68.917</gml:pos>
							<gml:pos>388869.0 5720394.368 68.897</gml:pos>
							<gml:pos>388869.627 5720394.627 68.89</gml:pos>
							<gml:pos>388870.0 5720394.781 68.886</gml:pos>
							<gml:pos>388870.429 5720394.959 68.876</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388867.152 5720393.824 68.933</gml:pos>
							<gml:pos>388867.467 5720393.733 68.925</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388863.221 5720400.782 69.103</gml:pos>
							<gml:pos>388863.424 5720400.424 69.094</gml:pos>
							<gml:pos>388863.663 5720400.0 69.083</gml:pos>
							<gml:pos>388863.785 5720399.785 69.078</gml:pos>
							<gml:pos>388864.0 5720399.403 69.068</gml:pos>
							<gml:pos>388864.146 5720399.146 69.06</gml:pos>
							<gml:pos>388864.228 5720399.0 69.055</gml:pos>
							<gml:pos>388864.507 5720398.507 69.055</gml:pos>
							<gml:pos>388864.793 5720398.0 69.054</gml:pos>
							<gml:pos>388864.868 5720397.868 69.05</gml:pos>
							<gml:pos>388865.0 5720397.633 69.043</gml:pos>
							<gml:pos>388865.229 5720397.229 69.032</gml:pos>
							<gml:pos>388865.358 5720397.0 69.026</gml:pos>
							<gml:pos>388865.59 5720396.59 69.024</gml:pos>
							<gml:pos>388865.923 5720396.0 69.012</gml:pos>
							<gml:pos>388865.951 5720395.951 69.01</gml:pos>
							<gml:pos>388866.0 5720395.863 69.007</gml:pos>
							<gml:pos>388866.312 5720395.312 68.987</gml:pos>
							<gml:pos>388866.488 5720395.0 68.975</gml:pos>
							<gml:pos>388866.673 5720394.673 68.963</gml:pos>
							<gml:pos>388867.0 5720394.093 68.942</gml:pos>
							<gml:pos>388867.034 5720394.034 68.94</gml:pos>
							<gml:pos>388867.053 5720394.0 68.939</gml:pos>
							<gml:pos>388867.152 5720393.824 68.933</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388863.221 5720400.782 69.103</gml:pos>
							<gml:pos>388863.543 5720400.917 69.103</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388863.543 5720400.917 69.103</gml:pos>
							<gml:pos>388863.742 5720401.0 69.103</gml:pos>
							<gml:pos>388864.0 5720401.107 69.099</gml:pos>
							<gml:pos>388864.183 5720401.183 69.094</gml:pos>
							<gml:pos>388865.0 5720401.523 69.075</gml:pos>
							<gml:pos>388865.896 5720401.896 69.08</gml:pos>
							<gml:pos>388866.0 5720401.939 69.08</gml:pos>
							<gml:pos>388866.134 5720401.995 69.072</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388866.134 5720401.995 69.072</gml:pos>
							<gml:pos>388866.146 5720402.0 69.071</gml:pos>
							<gml:pos>388866.492 5720402.144 69.045</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388866.492 5720402.144 69.045</gml:pos>
							<gml:pos>388866.58 5720402.0 69.045</gml:pos>
							<gml:pos>388866.739 5720401.739 69.036</gml:pos>
							<gml:pos>388867.0 5720401.308 69.013</gml:pos>
							<gml:pos>388867.117 5720401.117 69.007</gml:pos>
							<gml:pos>388867.188 5720401.0 69.004</gml:pos>
							<gml:pos>388867.495 5720400.495 68.99</gml:pos>
							<gml:pos>388867.795 5720400.0 68.968</gml:pos>
							<gml:pos>388867.873 5720399.873 68.965</gml:pos>
							<gml:pos>388868.0 5720399.664 68.963</gml:pos>
							<gml:pos>388868.251 5720399.251 68.957</gml:pos>
							<gml:pos>388868.403 5720399.0 68.958</gml:pos>
							<gml:pos>388868.629 5720398.629 68.959</gml:pos>
							<gml:pos>388869.0 5720398.019 68.95</gml:pos>
							<gml:pos>388869.007 5720398.007 68.95</gml:pos>
							<gml:pos>388869.011 5720398.0 68.95</gml:pos>
							<gml:pos>388869.385 5720397.385 68.936</gml:pos>
							<gml:pos>388869.619 5720397.0 68.921</gml:pos>
							<gml:pos>388869.763 5720396.763 68.915</gml:pos>
							<gml:pos>388870.0 5720396.374 68.91</gml:pos>
							<gml:pos>388870.141 5720396.141 68.906</gml:pos>
							<gml:pos>388870.227 5720396.0 68.903</gml:pos>
							<gml:pos>388870.519 5720395.519 68.885</gml:pos>
							<gml:pos>388870.835 5720395.0 68.865</gml:pos>
							<gml:pos>388870.897 5720394.897 68.861</gml:pos>
							<gml:pos>388870.975 5720394.77 68.856</gml:pos>
						</gml:LineString>
					</gml:curveMember>
					<gml:curveMember>
						<gml:LineString>
							<gml:pos>388870.656 5720394.58 68.862</gml:pos>
							<gml:pos>388870.975 5720394.77 68.856</gml:pos>
						</gml:LineString>
					</gml:curveMember>
				</gml:MultiCurve>
			</bldg:lod2TerrainIntersection>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_f2f4a11e-d077-4581-a63e-48eaebe92d7f">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeof9a27db5-a3d4-4aba-888c-1c29dc3beead">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459483">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459483">
											<gml:posList srsDimension="3">388870.656000 5720394.580000 79.022000 388870.429000 5720394.959000 79.022000 388870.429000 5720394.959000 67.319000 388870.656000 5720394.580000 67.319000 388870.656000 5720394.580000 79.022000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_6fa7aa04-a635-4642-9df4-8fdbb9fd53c2">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeoa2b336a2-1b0a-4e2f-91ed-60b7f645f529">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459484">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459484">
											<gml:posList srsDimension="3">388870.429000 5720394.959000 79.022000 388867.467000 5720393.733000 79.022000 388867.467000 5720393.733000 67.319000 388870.429000 5720394.959000 67.319000 388870.429000 5720394.959000 79.022000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_7ed83dbd-2f21-445d-b9fc-70b8ab98c672">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo47c489ba-7dc9-4a8b-ab3d-ec1a54d4f179">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459485">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459485">
											<gml:posList srsDimension="3">388867.467000 5720393.733000 79.022000 388867.152000 5720393.824000 79.022000 388867.152000 5720393.824000 67.319000 388867.467000 5720393.733000 67.319000 388867.467000 5720393.733000 79.022000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_4122aed3-1bbe-4a1f-b7ea-8355864416ea">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo7b661b57-fc50-49b7-8bc6-711438f2b16f">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459486">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459486">
											<gml:posList srsDimension="3">388867.152000 5720393.824000 79.022000 388863.221000 5720400.782000 79.022000 388863.221000 5720400.782000 67.319000 388867.152000 5720393.824000 67.319000 388867.152000 5720393.824000 79.022000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_bf94adde-a6d7-44e5-84fa-efe8d6acaca6">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeoec1886b7-c2c6-4044-9550-b6d2b4117cfe">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459487">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459487">
											<gml:posList srsDimension="3">388863.221000 5720400.782000 79.022000 388863.543000 5720400.917000 79.022000 388863.543000 5720400.917000 67.319000 388863.221000 5720400.782000 67.319000 388863.221000 5720400.782000 79.022000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_beda63b5-263d-46c3-b695-7fab2f6e38e2">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo5b675568-5685-4099-9fe8-f31078437653">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459488">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459488">
											<gml:posList srsDimension="3">388863.543000 5720400.917000 79.022000 388866.134000 5720401.995000 79.022000 388866.134000 5720401.995000 67.319000 388863.543000 5720400.917000 67.319000 388863.543000 5720400.917000 79.022000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_2f4f5468-e780-4507-a133-6ed4109cdbe0">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo70632bb4-c9f4-491b-880c-89ab5a4700e3">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459489">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459489">
											<gml:posList srsDimension="3">388866.134000 5720401.995000 79.022000 388866.492000 5720402.144000 79.022000 388866.492000 5720402.144000 67.319000 388866.134000 5720401.995000 67.319000 388866.134000 5720401.995000 79.022000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_6564901f-c51a-4cd5-9367-9dcfa57b13e5">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo9040e398-a22c-419e-b756-f65144597eda">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459490">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459490">
											<gml:posList srsDimension="3">388870.975000 5720394.770000 79.022000 388870.656000 5720394.580000 79.022000 388870.656000 5720394.580000 67.319000 388870.975000 5720394.770000 67.319000 388870.975000 5720394.770000 79.022000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:RoofSurface gml:id="UUID_91f1bdd4-5703-4af8-b21b-24dda58725f5">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo639ab27d-efec-4333-bee1-073df4d716a8">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459491">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459491">
											<gml:posList srsDimension="3">388870.975000 5720394.770000 79.022000 388866.492000 5720402.144000 79.022000 388866.134000 5720401.995000 79.022000 388863.543000 5720400.917000 79.022000 388863.221000 5720400.782000 79.022000 388867.152000 5720393.824000 79.022000 388867.467000 5720393.733000 79.022000 388870.429000 5720394.959000 79.022000 388870.656000 5720394.580000 79.022000 388870.975000 5720394.770000 79.022000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:RoofSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:GroundSurface gml:id="UUID_bcaf2175-df73-46c0-9d15-c318bdb76480">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo39b2d864-4836-4dc3-a0d8-7ba1ca6a3ab1">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459492">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459492">
											<gml:posList srsDimension="3">388870.656000 5720394.580000 67.319000 388870.429000 5720394.959000 67.319000 388867.467000 5720393.733000 67.319000 388867.152000 5720393.824000 67.319000 388863.221000 5720400.782000 67.319000 388863.543000 5720400.917000 67.319000 388866.134000 5720401.995000 67.319000 388866.492000 5720402.144000 67.319000 388870.975000 5720394.770000 67.319000 388870.656000 5720394.580000 67.319000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:GroundSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:WallSurface gml:id="UUID_e7291ad5-810f-4a8c-9b46-b56a9be42867">
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeof2ff3ad3-9546-45c0-9a3a-872baf0f5726">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459493">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459493">
											<gml:posList srsDimension="3">388866.492000 5720402.144000 79.022000 388870.975000 5720394.770000 79.022000 388870.975000 5720394.770000 78.633000 388870.705000 5720395.214000 78.785000 388866.492000 5720402.144000 76.418000 388866.492000 5720402.144000 67.319000 388866.492000 5720402.144000 79.022000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459494">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459494">
											<gml:posList srsDimension="3">388870.705000 5720395.214000 67.319000 388870.975000 5720394.770000 67.319000 388866.492000 5720402.144000 67.319000 388870.705000 5720395.214000 67.319000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:WallSurface>
			</bldg:boundedBy>
			<bldg:boundedBy>
				<bldg:ClosureSurface>
					<core:creationDate>2018-02-11</core:creationDate>
					<bldg:lod2MultiSurface>
						<gml:MultiSurface gml:id="IDGeo6679598d-5726-4e48-9b66-d019c085afe4">
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459495">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459495">
											<gml:posList srsDimension="3">388870.975000 5720394.770000 78.633000 388870.975000 5720394.770000 67.319000 388870.705000 5720395.214000 67.319000 388870.705000 5720395.214000 78.785000 388870.975000 5720394.770000 78.633000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
							<gml:surfaceMember>
								<gml:Polygon gml:id="PolyIDGeo7459496">
									<gml:exterior>
										<gml:LinearRing gml:id="ringID7459496">
											<gml:posList srsDimension="3">388870.705000 5720395.214000 78.785000 388870.705000 5720395.214000 67.319000 388866.492000 5720402.144000 67.319000 388866.492000 5720402.144000 76.418000 388870.705000 5720395.214000 78.785000 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
							</gml:surfaceMember>
						</gml:MultiSurface>
					</bldg:lod2MultiSurface>
				</bldg:ClosureSurface>
			</bldg:boundedBy>
		</bldg:BuildingPart>
	</bldg:consistsOfBuildingPart>
	<bldg:address>
		<core:Address>
			<core:xalAddress>
				<xAL:AddressDetails>
					<xAL:Country>
						<xAL:CountryName>
							Germany
						</xAL:CountryName>
						<xAL:Locality  Type="Town">
							<xAL:LocalityName>
								Waltrop
							</xAL:LocalityName>
							<xAL:Thoroughfare  Type="Street">
								<xAL:ThoroughfareNumber>
									4
								</xAL:ThoroughfareNumber>
								<xAL:ThoroughfareName>
									Kirchplatz
								</xAL:ThoroughfareName>
							</xAL:Thoroughfare>
						</xAL:Locality>
					</xAL:Country>
				</xAL:AddressDetails>
			</core:xalAddress>
		</core:Address>
	</bldg:address>
	<bldg:address>
		<core:Address>
			<core:xalAddress>
				<xAL:AddressDetails>
					<xAL:Country>
						<xAL:CountryName>
							Germany
						</xAL:CountryName>
						<xAL:Locality  Type="Town">
							<xAL:LocalityName>
								Waltrop
							</xAL:LocalityName>
							<xAL:Thoroughfare  Type="Street">
								<xAL:ThoroughfareNumber>
									3
								</xAL:ThoroughfareNumber>
								<xAL:ThoroughfareName>
									Kirchplatz
								</xAL:ThoroughfareName>
							</xAL:Thoroughfare>
						</xAL:Locality>
					</xAL:Country>
				</xAL:AddressDetails>
			</core:xalAddress>
		</core:Address>
	</bldg:address>
</bldg:Building>
</core:cityObjectMember>
</core:CityModel>
